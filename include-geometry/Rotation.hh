#ifndef ROTATION_HH
#define ROTATION_HH

/*

  Author: Andrea Latina

*/

#include <valarray>
#include <cmath>

#include "Quaternion.hh"
#include "Matrix3d.hh"
#include "Axis.hh"

class Rotation {

  Quaternion q;

  Rotation(const Quaternion &_q ) : q(_q) {}

public:

  enum { X, Y, Z };
  enum { PHI_X = 0, THETA_Z = 1, PHI_Z = 2 };
  
  Rotation(double phi_x, double theta_z, double phi_z ) { q = rotation_to_quaternion(phi_z, Axis_Z) * rotation_to_quaternion(theta_z, Axis_Y) * rotation_to_quaternion(phi_x, Axis_Z); }
  Rotation(const std::valarray<double> &angles ) { q = rotation_to_quaternion(angles[PHI_Z], Axis_Z) * rotation_to_quaternion(angles[THETA_Z], Axis_Y) * rotation_to_quaternion(angles[PHI_X], Axis_Z); }
  Rotation(const Axis &axis, double angle ) { q = rotation_to_quaternion(angle, axis); }
  Rotation() : q(1., 0., 0., 0.) {}
  
  friend Rotation make_rotation(Axis s, Axis t ) // rotates s into t
  {	 
    Quaternion result;
    Vector3d u = s ^ t;
    double sin2phi = u.normalize();
    if (1. + sin2phi > 1.) {
      double cos2phi = s * t;
      /* 
       * calculates sinphi and cosphi using half-angle relations 
       */
      double sinphi = sqrt((1. - cos2phi) / 2.);
      double cosphi = sqrt((1. + cos2phi) / 2.);
      result.s = cosphi;
      result.v = sinphi * u;
    } else {
      result.s = 1.;
      result.v = Vector3d(0., 0., 0.);
    }
    return result;
  }
    
  void set_euler_angles(const std::valarray<double> &angles )
  {
    q = rotation_to_quaternion(angles[PHI_Z], Axis_Z) * rotation_to_quaternion(angles[THETA_Z], Axis_Y) * rotation_to_quaternion(angles[PHI_X], Axis_Z);
  }
  
  std::valarray<double> get_euler_angles() const 
  {
    std::valarray<double> result(3);
    Axis z = Axis(operator*(*this, Axis_Z));
    result[THETA_Z] = z.theta;
    result[PHI_Z] = z.phi;
    Quaternion _q = rotation_to_quaternion(-z.theta, Axis_Y) * rotation_to_quaternion(-z.phi, Axis_Z) * q;
    Axis x = Axis(operator*(Rotation(_q), Axis_X));
    result[PHI_X] = x.phi;
    return result;
  }
  
  Matrix3d get_row_matrix() const { return transpose(quaternion_to_matrix(normalize(q))); }
  Matrix3d get_column_matrix() const { return quaternion_to_matrix(normalize(q)); }
  void set_row_matrix(const Matrix3d &m ) { q = matrix_to_quaternion(transpose(m)); }
  void set_column_matrix(const Matrix3d &m ) { q = matrix_to_quaternion(m); }
  operator Matrix3d () const { return quaternion_to_matrix(normalize(q)); }
  
  friend Rotation inverse(const Rotation &a ) { return Rotation(conj(a.q)); }
  
  friend Vector3d operator * (const Rotation &a, const Vector3d &v ) { Quaternion t = normalize(a.q); return (t * Quaternion(1., v) * conj(t)).v; }
  friend Rotation operator * (const Rotation &a, const Rotation &b ) { return Rotation(a.q * b.q); }
  friend Rotation operator / (const Rotation &a, const Rotation &b ) { return Rotation(a.q / b.q); }
  
  friend std::ostream &operator << (std::ostream &stream, const Rotation &a ) { return stream << a.q; }
  //friend std::istream &operator >> (std::istream &stream, Rotation &a ) { return stream >> a.q; }
  
  friend Rotation rotate_x(double angle ) { return Rotation(rotation_to_quaternion(angle, Axis_X)); }
  friend Rotation rotate_y(double angle ) { return Rotation(rotation_to_quaternion(angle, Axis_Y)); }
  friend Rotation rotate_z(double angle ) { return Rotation(rotation_to_quaternion(angle, Axis_Z)); }

};

#endif /* ROTATION_HH */
