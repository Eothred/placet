#ifndef QUATERNION_HH
#define QUATERNION_HH

/*

  Author: Andrea Latina

*/

#include <cmath>

#include "Matrix3d.hh"
#include "Axis.hh"

struct Quaternion {

  double s;
  
  Vector3d v;
  
  Quaternion(double _s, double _vi, double _vj, double _vk ) : s(_s), v(_vi, _vj, _vk) {}
  Quaternion(double _s, const Vector3d &_v ) : s(_s), v(_v) {}
  Quaternion() : s(0.) {}
  
  double real() const { return s; }
  const Vector3d &imag() const { return v; }
  
  friend double abs(const Quaternion &q ) { return sqrt(norm(q)); }
  
  friend Quaternion conj(const Quaternion &q ) { return Quaternion(q.s, -q.v); }
  friend double norm(const Quaternion &q )
  {
    double n = q.s * q.s + q.v * q.v; 
    if (1. + n > 1.)
      return n;
    return 0.;
  }
  
  friend Quaternion inverse(const Quaternion &q ) { return conj(q) / norm(q); }
  
  friend Quaternion exp(const Quaternion &q ) { double qv = norm(q); return exp(q.s) * Quaternion(cos(qv), sin(qv) * q.v / qv); }
  
  const Quaternion &operator += (const Quaternion &q ) { s += q.s; v += q.v; return *this; }
  const Quaternion &operator -= (const Quaternion &q ) { s -= q.s; v -= q.v; return *this; }
  const Quaternion &operator *= (const Quaternion &q ) { return *this = *this * q; }
  const Quaternion &operator *= (const Vector3d &v ) { return *this = *this * Quaternion(0, v); }
  const Quaternion &operator *= (double t ) { s *= t; v *= t; return *this; }
  const Quaternion &operator /= (double t ) { s /= t; v /= t; return *this; }
  
  friend Quaternion operator * (const Quaternion &a, const Quaternion &b ) { return Quaternion(a.s * b.s - a.v * b.v, a.s * b.v + b.s * a.v + (a.v ^ b.v)); }
  friend Quaternion operator * (const Vector3d &a, const Quaternion &b ) { return Quaternion(-(a * b.v), b.s * a + (a ^ b.v)); }
  friend Quaternion operator * (const Quaternion &a, const Vector3d &b ) { return a * Quaternion(0, b); }
  
  friend Quaternion operator * (const Quaternion &a, double b )	{ return Quaternion(a.s * b, a.v * b); }
  friend Quaternion operator * (double a, const Quaternion &b )	{ return Quaternion(b.s * a, b.v * a); }
  
  friend Quaternion operator / (const Quaternion &a, const Quaternion &b ) { return a * inverse(b); }
  friend Quaternion operator / (const Quaternion &a, double b )	{ return Quaternion(a.s / b, a.v / b); }
  
  friend Quaternion operator + (const Quaternion &a, const Quaternion &b ) { return Quaternion(a.s + b.s, a.v + b.v); }
  friend Quaternion operator - (const Quaternion &a, const Quaternion &b ) { return Quaternion(a.s - b.s, a.v - b.v); }
  friend Quaternion operator - (const Quaternion &a ) { return Quaternion(-a.s, -a.v); }
  
  friend std::ostream &operator << (std::ostream &stream, const Quaternion &q )	{ return stream << '(' << q.s << ", " << q.v.x << ", " << q.v.y << ", " << q.v.z << ')'; }
  
};

namespace {

  Quaternion rotation_to_quaternion(double angle, const Axis &axis )
  {
    return Quaternion(cos(angle/2), sin(angle/2) * axis);
  }

  Quaternion normalize(const Quaternion &q )
  {
    double s = (q.s > 0.) ? q.s : -q.s;
    double x = (q.v.x > 0.) ? q.v.x : -q.v.x;
    double y = (q.v.y > 0.) ? q.v.y : -q.v.y;
    double z = (q.v.z > 0.) ? q.v.z : -q.v.z;
    double denom = 1.;
    if ( s > x ) {
      if ( s > y ) {
	if ( s > z ) {
	  if ( 1. + s > 1. ) {
	    x /= s;
	    y /= s;
	    z /= s;
	    denom = s*sqrt(1.+x*x+y*y+z*z);
	  }
	} else {
	  if ( 1.+z > 1. ) {
	    x /= z;
	    y /= z;
	    s /= z;
	    denom = z*sqrt(1.+x*x+y*y+s*s);
	  }
	}
      } else {
	if ( y > z ) {
	  if ( 1.+y > 1. ) {
	    x /= y;
	    s /= y;
	    z /= y;
	    denom = y*sqrt(1.+x*x+s*s+z*z);
	  }
	} else {
	  if ( 1.+z > 1. ) {
	    x /= z;
	    y /= z;
	    s /= z;
	    denom = z*sqrt(1.+x*x+y*y+s*s);
	  }
	}
      }
    } else {	
      if ( x > y ) {
	if ( x > z ) {
	  if ( 1.+x > 1. ) {
	    s /= x;
	    y /= x;
	    z /= x;
	    denom = x*sqrt(1.+s*s+y*y+z*z);
	  }
	} else {
	  if ( 1.+z > 1. ) {
	    x /= z;
	    y /= z;
	    s /= z;
	    denom = z*sqrt(1.+x*x+y*y+s*s);
	  }
	}
      } else {
	if ( y > z ) {
	  if ( 1.+y > 1. ) {
	    x /= y;
	    s /= y;
	    z /= y;
	    denom = y*sqrt(1.+x*x+s*s+z*z);
	  }
	} else {
	  if ( 1.+z > 1. ) {
	    x /= z;
	    y /= z;
	    s /= z;
	    denom = z*sqrt(1.+x*x+y*y+s*s);
	  }
	}
      }		
    }

    if (1. + s + x + y + z > 1. ) {
      return q / denom;
    } else {
      return q;
    } 
  }

  Matrix3d quaternion_to_matrix(const Quaternion &q )
  {
    return Matrix3d(Vector3d(1 - 2 * (q.v.y * q.v.y + q.v.z * q.v.z), 	    2 * (q.v.x * q.v.y - q.s * q.v.z),		2 * (q.v.x * q.v.z + q.s * q.v.y)),
		    Vector3d(    2 * (q.v.x * q.v.y + q.s * q.v.z), 	1 - 2 * (q.v.x * q.v.x + q.v.z * q.v.z),        2 * (q.v.y * q.v.z - q.s * q.v.x)),
		    Vector3d(    2 * (q.v.x * q.v.z - q.s * q.v.y),	    2 * (q.v.y * q.v.z + q.s * q.v.x),      1 - 2 * (q.v.x * q.v.x + q.v.y * q.v.y)));
  }

  Quaternion matrix_to_quaternion(const Matrix3d &m )
  {
    double tr = m[0][0] + m[1][1] + m[2][2];
    Quaternion q;
    if (tr >= 0) {
      double s = sqrt(tr + 1);
      q.s = 0.5 * s;
      q.v = Vector3d(m[2][1] - m[1][2],
		     m[0][2] - m[2][0],
		     m[1][0] - m[0][1]) * 0.5 / s;
    } else {
      int i = 0;
      if (m[1][1] > m[0][0])	i = 1;
      if (m[2][2] > m[i][i])	i = 2;
      double s;
      switch(i) {
      case 0:
	s = sqrt((m[0][0] - (m[1][1] + m[2][2])) + 1);
	q.v.x = 0.5 * s;
	s = 0.5 / s;
	q.v.y =	(m[0][1] + m[1][0]) * s;
	q.v.z =	(m[2][0] + m[0][2]) * s;
	q.s =	(m[2][1] - m[1][2]) * s;
	break;

      case 1:
	s = sqrt((m[1][1] - (m[2][2] + m[2][2])) + 1);
	q.v.y = 0.5 * s;
	s = 0.5 / s;
	q.v.z =	(m[1][2] + m[2][1]) * s;
	q.v.x =	(m[0][1] + m[1][0]) * s;
	q.s =	(m[0][2] - m[2][0]) * s;
	break;

      case 2:
	s = sqrt((m[2][2] - (m[0][0] + m[1][1])) + 1);
	q.v.z = 0.5 * s;
	s = 0.5 / s;
	q.v.x =	(m[2][0] + m[0][2]) * s;
	q.v.y =	(m[1][2] + m[2][1]) * s;
	q.s =	(m[1][0] - m[0][1]) * s;
	break;
      }
    }
    return q;
  }

}

#endif /* QUATERNION_HH */
