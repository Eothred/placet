#include <octave/oct.h>

DEFUN_DLD (org, args, ,
	   "The `org'.")
{
  static int i=0;
  ColumnVector dx (1);
  
  dx(0) = (double)i++;
  
  return octave_value (dx);
}

