#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define N 10000000
#define TWOPI 6.283185307179586
#define TWOPI_INV 0.159154943091895

double *quick_s,*quick_c,quick_i,quick_f;
int quick_n,quick_n2,quick_n4;

void
quick_prepare()
{
  int i;
  quick_n=1000;
  quick_n2=quick_n/2;
  quick_n4=quick_n/4;
  quick_s=(double*)malloc(sizeof(double)*quick_n);
  quick_c=(double*)malloc(sizeof(double)*quick_n);
  quick_i=quick_n/TWOPI;
  quick_f=TWOPI/quick_n;
  for(i=0;i<quick_n;++i){
    quick_s[i]=sin((i)*TWOPI/quick_n);
    quick_c[i]=cos((i)*TWOPI/quick_n);
  }
}

inline void
quick(double x,double *s,double *c)
{
  int i;
  i=lrint(x*quick_i);
  //  i=(int)floor(x*quick_i+0.5);
  x-=i*quick_f;
  i=i%quick_n;
  //  printf("%g %d\n",x,i);
  //  *s=quick_s[i]+x*(quick_c[i]-0.5*x*quick_s[i]);
  //  *c=quick_c[i]-x*(quick_s[i]+0.5*x*quick_c[i]);
  *s=quick_s[i]+x*(quick_c[i]-x*(0.5*quick_s[i]+0.166666666666666667*x*quick_c[i]));
  *c=quick_c[i]-x*(quick_s[i]+x*(0.5*quick_c[i]-0.166666666666666667*x*quick_s[i]));
}

inline void
quick2(double x,double *s,double *c)
{
  int i,i1;
  i=lrint(x*quick_i);
  //  i=(int)floor(x*quick_i+0.5);
  x-=i*quick_f;
  i=i%quick_n;
  i1=(i+quick_n4)%quick_n;
  *s=quick_s[i]+x*(quick_s[i1]-x*(0.5*quick_s[i]+0.166666666666666667*x*quick_s[i1]));
  *c=quick_s[i1]-x*(quick_s[i]+x*(0.5*quick_s[i1]-0.166666666666666667*x*quick_s[i]));
}

main()
{
  int i;
  double s=0,c=0,x,s0,c0;

  quick_prepare();
  x=0.5*quick_f;
  for(i=0;i<N;++i){
    s+=sin(x);
    c+=cos(x);
    //    quick(x,&s0,&c0);
    //                      printf("%g %g %g\n",x,s0-sin(x),c0-cos(x));
    s+=s0;
    c+=c0;
    x+=quick_f;
  }
  printf("%g %g\n",s,c);
  exit(0);
}
