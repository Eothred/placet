#include <stdlib.h>
#include <stdio.h>
#include <math.h>

main(int argc,char *argv[])
{
  FILE *f1,*f2;
  char line[1024],*point;
  int n1,n2,i;
  double x1,x2;

  f1=fopen(argv[1],"r");
  f2=fopen(argv[3],"r");
  n1=strtol(argv[2],&point,10);
  n2=strtol(argv[4],&point,10);
  while (point=fgets(line,1024,f1)){
    for (i=0;i<=n1;i++){
      x1=strtod(point,&point);
    }
    point=fgets(line,1024,f2);
    for (i=0;i<=n2;i++){
      x2=strtod(point,&point);
    }
    printf("%g %g\n",x1,x2);
  }
  fclose(f1);
  fclose(f2);
}
