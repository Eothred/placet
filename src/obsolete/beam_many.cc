void
write_particle_from_slice(BEAM* beam,int n)
{
  double z[6];
  z[3]=beam->z_position[n/beam->macroparticles];
  z[0]=beam->particle[n].energy;
  z[1]=gasdev()*sqrt(beam->sigma_xx[n].r11);
  z[4]=gasdev()*sqrt(beam->sigma_xx[n].r22-beam->sigma_xx[n].r12*
		     beam->sigma_xx[n].r21/beam->sigma_xx[n].r11)
    +z[1]*beam->sigma_xx[n].r12/beam->sigma_xx[n].r11;
  z[2]=gasdev()*sqrt(beam->sigma[n].r11);
  z[3]=gasdev()*sqrt(beam->sigma[n].r22-beam->sigma[n].r12*
		     beam->sigma[n].r21/beam->sigma[n].r11)
    +z[1]*beam->sigma[n].r12/beam->sigma[n].r11;
  z[1]+=beam->particle[n].x;
  z[2]+=beam->particle[n].y;
  z[4]+=beam->particle[n].xp;
  z[5]+=beam->particle[n].yp;
}
