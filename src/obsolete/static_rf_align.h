void beamline_bin_divide_static_rf(BEAMLINE *beamline,int ,int ,BIN **,
                                                 int *,BIN **,int ,int );
void bin_define_static_rf(BEAMLINE *,int ,int ,int *,int *,
                         int *,int *,int *,int *,int *);
void static_rf_correct_emitt(BEAMLINE *,BIN **,int ,BEAM *,BEAM *);
void bin_response_static_rf(BEAMLINE *,BEAM *,BEAM *,int ,BIN *,int ,int );
void bin_correct_static_rf(BEAMLINE *,BIN *,int );
double test_static_rf_correction(BEAMLINE *,BIN **,BIN **,int ,
	    BEAM *,BEAM *,int ,void (*survey)(BEAMLINE*),char *,char *,int );
void inter_girder_move(BEAMLINE*,double,double,double,double,int);
void scatter_girder(BEAMLINE*,double,double,double,double,int);
