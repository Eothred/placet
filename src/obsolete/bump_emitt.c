struct{
  double W11,W12,W22,W10,W20,W00;
  double Wpp11,Wpp12,Wpp22,Wpp10,Wpp20,Wpp00;
  double Wp11,Wp12,Wp22,Wp10,Wp20,Wp00;
  BEAM *beam;
} bump_emitt_data;

bump_emitt_init(BEAM *beam,BUMP *bump)
{
  int i,n;

  n=beam->slices;
  bump_emitt_data.beam=beam;

  bump_emitt_data.W00=0.0;
  bump_emitt_data.W10=0.0;
  bump_emitt_data.W20=0.0;
  bump_emitt_data.W11=0.0;
  bump_emitt_data.W12=0.0;
  bump_emitt_data.W22=0.0;

  bump_emitt_data.Wpp00=0.0;
  bump_emitt_data.Wpp10=0.0;
  bump_emitt_data.Wpp20=0.0;
  bump_emitt_data.Wpp11=0.0;
  bump_emitt_data.Wpp12=0.0;
  bump_emitt_data.Wpp22=0.0;

  bump_emitt_data.Wp00=0.0;
  bump_emitt_data.Wp10=0.0;
  bump_emitt_data.Wp20=0.0;
  bump_emitt_data.Wp11=0.0;
  bump_emitt_data.Wp12=0.0;
  bump_emitt_data.Wp22=0.0;

  for (i=0;i<n;i++){

    bump_emitt_data.W11+=beam->particle[i].wgt*bump->a11[i]*bump->a11[i];
    bump_emitt_data.W12+=beam->particle[i].wgt*bump->a11[i]*bump->a21[i];
    bump_emitt_data.W22+=beam->particle[i].wgt*bump->a21[i]*bump->a21[i];
    bump_emitt_data.W10+=beam->particle[i].wgt*bump->a11[i]
      *beam->particle[i].y;
    bump_emitt_data.W20+=beam->particle[i].wgt*bump->a21[i]
      *beam->particle[i].y;
    bump_emitt_data.W00+=beam->particle[i].wgt*(beam->particle[i].y
      *beam->particle[i].y+beam->sigma[i].r11);

    bump_emitt_data.Wpp11+=beam->particle[i].wgt*bump->a12[i]*bump->a12[i];
    bump_emitt_data.Wpp12+=beam->particle[i].wgt*bump->a12[i]*bump->a22[i];
    bump_emitt_data.Wpp22+=beam->particle[i].wgt*bump->a22[i]*bump->a22[i];
    bump_emitt_data.Wpp10+=beam->particle[i].wgt*bump->a12[i]
      *beam->particle[i].yp;
    bump_emitt_data.Wpp20+=beam->particle[i].wgt*bump->a22[i]
      *beam->particle[i].yp;
    bump_emitt_data.Wpp00+=beam->particle[i].wgt*(beam->particle[i].yp
      *beam->particle[i].yp+beam->sigma[i].r12);

    bump_emitt_data.Wp11+=beam->particle[i].wgt*bump->a11[i]*bump->a12[i];
    bump_emitt_data.Wp12+=beam->particle[i].wgt*bump->a11[i]*bump->a22[i];
    bump_emitt_data.Wp22+=beam->particle[i].wgt*bump->a21[i]*bump->a22[i];
    bump_emitt_data.Wp10+=beam->particle[i].wgt*bump->a11[i]
      *beam->particle[i].yp;
    bump_emitt_data.Wp20+=beam->particle[i].wgt*bump->a21[i]
      *beam->particle[i].yp;
    bump_emitt_data.Wp00+=beam->particle[i].wgt*(beam->particle[i].y
      *beam->particle[i].yp+beam->sigma[i].r22);

  }
}

double bump_emitt(double a1, double a2)
{
  int i,n;
  double S11,S12,S22;

  S11=bump_emitt_data.W00
    +a1*a1*bump_emitt_data.W11
    +a2*a2*bump_emitt_data.W22
    +2.0*a1*a2*bump_emitt_data.W12
    +2.0*a1*bump_emitt_data.W10
    +2.0*a2*bump_emitt_data.W20;

  S12=bump_emitt_data.Wp00
    +a1*a1*bump_emitt_data.Wp11
    +a2*a2*bump_emitt_data.Wp22
    +2.0*a1*a2*bump_emitt_data.Wp12
    +2.0*a1*bump_emitt_data.Wp10
    +2.0*a2*bump_emitt_data.Wp20;

  S22=bump_emitt_data.Wpp00
    +a1*a1*bump_emitt_data.Wpp11
    +a2*a2*bump_emitt_data.Wpp22
    +2.0*a1*a2*bump_emitt_data.Wpp12
    +2.0*a1*bump_emitt_data.Wpp10
    +2.0*a2*bump_emitt_data.Wpp20;

  return S11*S22-S12*S12;
}
