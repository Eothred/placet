#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int create_pipe(char *child, int opipe[2], int ipipe[2])
{
    pid_t pid;
    
    /* Create output pipe and input pipe  */
    if (pipe (opipe)) {
        fprintf (stderr, "Pipe failed.\n");
        return EXIT_FAILURE;
    }
    if (pipe (ipipe)) {
        fprintf (stderr, "Pipe failed.\n");
        return EXIT_FAILURE;
    }

     /* Create the child process.  */
    pid = fork ();
    if (pid == (pid_t) 0) {
        /* This is the child process.  */
        /* Child stdin is opipe[0] */
        close(0);
        dup(opipe[0]);
        close(opipe[0]);
        /* Child stdout is ipipe[1] */
        close(1);
        dup(ipipe[1]);
        close(ipipe[1]);
        /* Closed unused FD */
        close(opipe[1]);
        close(ipipe[0]);
        execlp(child, child, NULL);
    }
    else if (pid < (pid_t) 0) {
        /* The fork failed.  */
        fprintf (stderr, "Fork failed.\n");
        return EXIT_FAILURE;
    }
    return pid;
}

#define NBUF 2048

main(int argc, char *argv[])
{
    FILE *po, *pi;
    char s[128];
    pid_t pid;
    int opipe[2], ipipe[2];
    char buff[NBUF];
    int ch;
    
    if (argc<2) {
        fprintf(stderr, "Tk display subprogram required.\n");
        fprintf(stderr, "Usage: %s display.tk\n", argv[0]);
        exit(1);
    }

    /* Change low level pipe FD to streams */
    pid=create_pipe(argv[1], opipe, ipipe);
    po=fdopen(opipe[1], "w");
    pi=fdopen(ipipe[0], "r");

    while(ch=fgetc(pi)) {
      fputc(ch,stdout);
    }
    fflush(stdout);
    fprintf(po,"s=rand[4,2];\n");
    fflush(po);
    fprintf(po,"x=rand[2,4];\n");
    fflush(po);
    fprintf(po,"a=x*s;\n");
    fflush(po);
    fprintf(po,"x\n");
    fflush(po);
    while(ch=fgetc(pi)) {
      fputc(ch,stdout);
    }
    fflush(stdout);
    /* Close output pipe and wait input pipe flush */
    fclose(po);
    exit(0);
    fgets(buff,NBUF, pi);
    fprintf(stderr, "%s: %s", argv[0], buff);

    return 0;
}
