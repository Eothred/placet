#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../fftw/fftw-2.1.3/fftw/fftw.h"

#define TWOPI 6.283185307179586

#define Noff 0
#define Npoint 3000
#define M 1

main()
{
  char buffer[1000],*point;
  FILE *f;
  int i,m=0,j;
  double dummy,wgt[M],y2,yp2,yyp,y,yp,wgtsum,y_norm,yp_norm,y2_norm,yp2_norm,
      yyp_norm;
  fftw_complex *in,*in2,*norm,*norm2;
  fftw_plan p,p2;

  in=(fftw_complex*)malloc(sizeof(fftw_complex)*2*M*Npoint);
  norm=(fftw_complex*)malloc(sizeof(fftw_complex)*2*M*Npoint);
  norm2=(fftw_complex*)malloc(sizeof(fftw_complex)*2*M*Npoint);
  p = fftw_create_plan_specific(2*Npoint,FFTW_FORWARD,
				FFTW_ESTIMATE | FFTW_IN_PLACE,
				in,M,NULL,M);
  in2=(fftw_complex*)malloc(sizeof(fftw_complex)*2*M*Npoint);
  p2 = fftw_create_plan_specific(2*Npoint,FFTW_FORWARD,
				FFTW_ESTIMATE | FFTW_IN_PLACE,
				in2,M,NULL,M);
  for (i=Noff;i<Noff+Npoint;i++){
    snprintf(buffer,1000,"b_%d\0",i);
    f=fopen(buffer,"r");
    for (j=0;j<M;j++){
      point=fgets(buffer,1000,f);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      wgt[j]=dummy;
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      in[m].re=dummy;
      in[m].im=0.0;
      dummy=strtod(point,&point);
      in2[m].re=dummy;
      in2[m].im=0.0;
      norm[m].re=0.0;
      norm[m].im=0.0;
      norm2[m].re=0.0;
      norm2[m].im=0.0;
      m++;
    }
    fclose(f);
  }
  for (i=0;i<Npoint;i++){
    for (j=0;j<M;j++){
	in[m].re=0.0;
	in[m].im=0.0;
	in2[m].re=0.0;
	in2[m].im=0.0;
	norm[m].re=0.0;
	norm[m].im=0.0;
	norm2[m].re=0.0;
	norm2[m].im=0.0;
	m++;
    }
  }
  wgtsum=0.0;
  for (i=0;i<M;i++){
      wgtsum+=wgt[i];
      norm[i].re=in[i].re;
      norm[i].im=in[i].im;
      norm2[i].re=in2[i].re;
      norm2[i].im=in2[i].im;
  }
  fftw(p,M,in,M,1,NULL,1,1);
  fftw(p2,M,in2,M,1,NULL,1,1);
  fftw(p2,M,norm,M,1,NULL,1,1);
  fftw(p2,M,norm2,M,1,NULL,1,1);
  for (i=0;i<Npoint*2;i++){
      y=0.0;
      yp=0.0;
      y2=0.0;
      yp2=0.0;
      yyp=0.0;
      y_norm=0.0;
      yp_norm=0.0;
      y2_norm=0.0;
      yp2_norm=0.0;
      yyp_norm=0.0;
      for (j=0;j<M;j++){
	  y+=in[i*M+j].re*wgt[j];
	  yp+=in2[i*M+j].re*wgt[j];
	  y_norm+=norm[i*M+j].re*wgt[j];
	  yp_norm+=norm2[i*M+j].re*wgt[j];
      }
      y/=wgtsum;
      yp/=wgtsum;
      y_norm/=wgtsum;
      yp_norm/=wgtsum;
      for (j=0;j<M;j++){
	  y2+=(in[i*M+j].re-y)*(in[i*M+j].re-y)*wgt[j]*Npoint*Npoint*4;
	  yp2+=(in2[i*M+j].re-yp)*(in2[i*M+j].re-yp)*wgt[j]*Npoint*Npoint*4;
	  y2+=(in[i*M+j].re-y)*(in2[i*M+j].re-yp)*wgt[j]*Npoint*Npoint*4;
	  y2_norm+=(norm[i*M+j].re-y_norm)*(norm[i*M+j].re-y_norm)
	      *wgt[j]*Npoint*Npoint*4;
	  yp2_norm+=(norm2[i*M+j].re-yp_norm)*(norm2[i*M+j].re-yp_norm)
	      *wgt[j]*Npoint*Npoint*4;
	  y2_norm+=(norm[i*M+j].re-y_norm)*(norm2[i*M+j].re-yp_norm)
	      *wgt[j]*Npoint*Npoint*4;
      }
      y2/=wgtsum;
      yp2/=wgtsum;
      yyp/=wgtsum;
      y2_norm/=wgtsum;
      yp2_norm/=wgtsum;
      yyp_norm/=wgtsum;
//      printf("%d %g %g %g %g\n",i,sqrt(y2*yp2-yyp*yyp)/sqrt(y2_norm*yp2_norm-yyp_norm*yyp_norm),y/y_norm,yp/yp_norm,2*Npoint*sqrt(y*y+65.0*65.0*yp*yp)/(2*Npoint));
//      printf("%d %g %g %g %g\n",i,sqrt(y2*yp2-yyp*yyp),y*2*Npoint,yp*2*Npoint,2*Npoint*sqrt(y*y+65.0*65.0*yp*yp));
      printf("%d %g %g %g %g\n",i,sqrt(y2*yp2-yyp*yyp)/(4*Npoint*Npoint),y,yp,sqrt(y*y+65.0*65.0*yp*yp));
  }
}
