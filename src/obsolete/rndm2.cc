#include <cstdlib>
#include <cstdio>
#include <cmath>

#include "rndm2.h"

void
RANDOM8::init(int idummy)
{
  static int m1=259200,ia1=7141,ic1=54773;
  static float rm1=1.0/259200.0;
  static int m2=134456,ia2=8121,ic2=28411;
  static float rm2=1.0/134456.0;
  //  static int m3=243000,ia3=4561,ic3=51349;
  static int m3=243000;
  static int jx1,jx2,jx3,j;
  //  static float rm3=1.0/243000.0;

  if (idummy>0) idummy=-idummy;
  jx1=(ic1-idummy) % m1;
  if (jx1<0) jx1=-jx1; // potential overflow cure
  jx1=(ia1*jx1+ic1) % m1;
  jx2=jx1 % m2;
  jx1=(ia1*jx1+ic1) % m1;
  jx3=jx1 % m3;
  for (j=0;j<97;j++)
    {
      jx1=(ia1*jx1+ic1) % m1;
      jx2=(ia2*jx2+ic2) % m2;
      this->r[j]=(jx1+jx2*rm2)*rm1;
    }
  this->ix1=jx1;
  this->ix2=jx2;
  this->ix3=jx3;
}

float RANDOM8::value()
{
  static int m1=259200,ia1=7141,ic1=54773;
  static float rm1=1.0/259200.0;
  static int m2=134456,ia2=8121,ic2=28411;
  static float rm2=1.0/134456.0;
  static int m3=243000,ia3=4561,ic3=51349;
  static int j;
  static float rm3=1.0/243000.0;
  float help;

  this->ix1=(ia1*this->ix1+ic1) % m1;
  this->ix2=(ia2*this->ix2+ic2) % m2;
  this->ix3=(ia3*this->ix3+ic3) % m3;
  j=(int)((float)(97*this->ix3)*rm3);
  help=this->r[j];
  this->r[j]=(this->ix1+this->ix2*rm2)*rm1;
  return help;
}

void
RANDOM_GAUSS::init (int seed)
{
  this->rndm.init(seed);
  this->iset=0;
  this->low=-1e10;
  this->high=1e10;
  this->mean=0.0;
  this->sigma=1.0;
}

void
RANDOM_GAUSS::init (int seed,double mean0,double sigma0)
{
  this->rndm.init(seed);
  this->iset=0;
  this->low=-1e10;
  this->high=1e10;
  this->mean=mean0;
  this->sigma=sigma0;
}

float
RANDOM_GAUSS::value_int ()
{
  float r;
  if (this->iset==0)
    {
      for (;;)
	{
          this->v1=2.0*rndm.value()-1.0;
          this->v2=2.0*rndm.value()-1.0;
          r=this->v1*this->v1+this->v2*this->v2;
          if ((r<=1.0) && (r!=0))
	    {
	      break;
	    }
        }
      this->iset=1;
      r=sqrt(-2.0*log((double)r)/r);
      this->v1*=r;
      this->v2*=r;
      return this->v1;
    }
  else
    {
      this->iset=0;
      return this->v2;
    }
}

float
RANDOM_GAUSS::value ()
{
  float tmp;
  tmp=this->value_int();
  /*
  while ((tmp<this->low)||(tmp>this->high)) {
    tmp=this->value_int();
  }
  */
  return this->mean+this->sigma*tmp;
}

/*
main ()
{
  int i;
  RANDOM_GAUSS r;
  r.init(1,1.0,0.001);
  for(i=0;i<100;++i) {
    placet_printf(INFO,"%g\n",r.value());
  }
}
*/
