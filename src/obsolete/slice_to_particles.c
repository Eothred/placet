#include <stdlib.h>

class rndm_chanel {
  double *xsum;
  int nch;
 public:
  rndm_chanel(double wgt[],int n);
};

rndm_chanel::rndm_chanel(double wgt[],int n)
{
  int i;
  nch=n;
  xsum=new double[n];
  if (n<1) {
    //    fprintf(stderr,"too small number of channels in rndm_chanel\n");
    //    fflush(stderr);
    exit(1);
  }
  xsum[0]=wgt[0];
  for(i=1;i<nch;++i){
    xsum[i]=xsum[i-1]+wgt[i];
  }
  for(i=0;i<nch-1;++i){
    xsum[i]/=xsum[nch-1];
  }
  xsum[nch-1]=1.0;
}

int
select_chanel(double wgt[],int n,double r)
{
  return 1;
}

int
select_slice(double wgt[],int n,double r)
{
  
}
