#ifndef socket_hh
#define socket_hh

#include <algorithm>
#include <cstring>
#include <cstdio>

extern "C" {

#include <netinet/in.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>

}

#include "stream.hh"
#include "error.hh"

struct SocketAddress : public sockaddr_in {

  explicit SocketAddress(unsigned short port = 0 );
  SocketAddress(const in_addr &address, unsigned short port );
  SocketAddress(unsigned long address, unsigned short port );
  SocketAddress(const char *hostname, unsigned short port );

  operator const sockaddr & () const { return reinterpret_cast<const sockaddr &>(*this); }
  operator sockaddr & () { return reinterpret_cast<sockaddr &>(*this); }

  void set_address(const in_addr &address ) { sin_addr = address; }
  void set_address(unsigned long address ) { sin_addr.s_addr = htonl(address); }
  void set_address(const char *hostname ) { hostent *h = gethostbyname(hostname); sin_addr.s_addr = h != NULL ? ((in_addr*)h->h_addr)->s_addr : inet_addr(hostname); }

  void set_port(unsigned short port ) { sin_port = port; }

  const char *get_ip_address() const { return inet_ntoa(sin_addr); }
  const char *get_hostname() const { hostent *h = gethostbyaddr((char*)&sin_addr, sizeof(sin_addr), AF_INET); return h != NULL ? h->h_name : NULL; }

  unsigned short get_port() const { return sin_port; }

};

class TCP_Socket : public IOStream {
protected:

  int sd;

  inline bool writable() const { return sd != -1; }
  inline bool readable() const { return sd != -1; }

public:

  TYPEDEF_ERROR(error);

  TCP_Socket(const TCP_Socket &s ) : sd(s.sd) {}
  TCP_Socket() : sd(-1) {}
	
  virtual ~TCP_Socket() {}

  operator const int &() const { return sd; }
  operator bool() const { return sd != -1; }
	
  bool operator == (const TCP_Socket &s ) { return sd == s.sd; }
  bool operator  < (const TCP_Socket &s ) { return sd  < s.sd; }


  SocketAddress get_peername() const;

  template <class T> bool set_sockopt(int optname, const T &optval ) { return setsockopt(sd, SOL_SOCKET, optname, &optval, sizeof(T)) == 0; }
  bool set_sockopt(int optname, const void *optval, socklen_t optlen )	{ return setsockopt(sd, SOL_SOCKET, optname,  optval, optlen) == 0; }

  template <class T> 	bool get_sockopt(int optname, T &optval )				                      { socklen_t len; return getsockopt(sd, SOL_SOCKET, optname, &optval, &len) == 0; }
  bool get_sockopt(int optname, void *optval, socklen_t *optlen )		    { return getsockopt(sd, SOL_SOCKET, optname,  optval, optlen) == 0; }

  void close();

  bool connect(const SocketAddress &remote, int type = SOCK_STREAM );
  bool connect(char *hostname, unsigned short port ) { return connect(SocketAddress(hostname, port)); }

  virtual bool bind(unsigned int _port );
  virtual bool bind(const SocketAddress &local );
  virtual bool bind(int _type, unsigned int _port );
  virtual bool bind(int _type, const SocketAddress &local );

  bool listen(int _backlog = 10 );

  TCP_Socket accept() const;
  TCP_Socket accept(SocketAddress &remote ) const;

  int send(const void *buff, size_t len )	{ return ::send(sd, buff, len, 0); }
  int recv(void *buff, size_t len)			  { return ::recv(sd, buff, len, 0); }
	
#define WRITE(TYPE)							\
  size_t write(const TYPE *ptr, size_t n )	{ return ::send(sd, ptr, n * sizeof(TYPE), 0) / sizeof(TYPE); }	\
  size_t write(const TYPE &ref )			{ return ::send(sd, &ref, sizeof(TYPE), 0) ? 1 : 0; }

  WRITE(char)
  WRITE(float)
    WRITE(double)
    WRITE(long double)
    WRITE(signed short)
    WRITE(signed int)
    WRITE(signed long int)
    WRITE(unsigned short)
    WRITE(unsigned int)
    WRITE(unsigned long int)
#undef WRITE

#define READ(TYPE)							\
    inline size_t read(TYPE *ptr, size_t n )  { return ::recv(sd, ptr, n * sizeof(TYPE), 0) / sizeof(TYPE); } \
    inline size_t read(TYPE &ref )            { return ::recv(sd, &ref, sizeof(TYPE), 0) ? 1 : 0; }

  READ(char)
  READ(float)
    READ(double)
    READ(long double)
    READ(signed short)
    READ(signed int)
    READ(signed long int)
    READ(unsigned short)
    READ(unsigned int)
    READ(unsigned long int)
#undef READ

    };

class UDP_Socket : public TCP_Socket {
public:

  UDP_Socket()
  {
    sd = socket(AF_INET, SOCK_DGRAM, 0);
  }
	
  virtual ~UDP_Socket() {}
	
  int sendto(const void *buff, size_t len, const SocketAddress &to  )	{ return ::sendto(sd, buff, len, 0, reinterpret_cast<const sockaddr*>(&to), sizeof(sockaddr)); }
  int recvfrom(void *buff, size_t len, SocketAddress &from )			    { socklen_t fromlen = sizeof(sockaddr);	return ::recvfrom(sd, buff, len, 0, reinterpret_cast<sockaddr*>(&from), &fromlen); }
  int recvfrom(void *buff, size_t len )						                    { socklen_t fromlen = 0; return ::recvfrom(sd, buff, len, 0, NULL, &fromlen); }

  bool bind(unsigned int _port )
  {
    SocketAddress local(_port);
    return ::bind(sd, (sockaddr*)&local, sizeof(sockaddr)) == 0;
  }

};

class SocketSet {

  std::list<TCP_Socket> sockets;
	
  fd_set set;
	
public:

  typedef std::list<TCP_Socket>::const_iterator 	const_iterator;
  typedef std::list<TCP_Socket>::iterator 	iterator;
	
  SocketSet(const std::list<TCP_Socket> &s ) : sockets(s) { FD_ZERO(&set); for (const_iterator i = sockets.begin(); i != sockets.end(); i++) FD_SET(*i, &set); }
  SocketSet(const SocketSet &s ) : sockets(s.sockets), set(s.set) {}
  SocketSet(const TCP_Socket &sd ) { FD_ZERO(&set); append(sd); }
  SocketSet() { FD_ZERO(&set); }
	
  ~SocketSet() {}
	
  const_iterator	begin() const	  	{ return sockets.begin(); }
  iterator 	begin() 			  	{ return sockets.begin(); }
	
  const_iterator	end() const 		  { return sockets.end(); }
  iterator 	end()						  { return sockets.end(); }

  size_t		size() const					  { return sockets.size(); }

  void append(const TCP_Socket &s ) { FD_SET(int(s), &set); sockets.push_back(s); }
  void append(const SocketSet &s )	{ for (const_iterator i = s.sockets.begin(); i != s.sockets.end(); i++) append(*i); }
	
  void clear()									    { FD_ZERO(&set); sockets.clear(); }

  void remove(const TCP_Socket &s )	{ FD_CLR(s, &set); sockets.remove(s); }
  void remove(const SocketSet &s )	{ for (const_iterator i = s.sockets.begin(); i != s.sockets.end(); i++) remove(*i); }


  bool      is_set(int sd ) const		{ return FD_ISSET(sd, &set); }
  iterator 	is_set(iterator begin, const iterator &end ) const;

  int select(timeval *timeout = NULL )		{ return ::select(int(*std::max_element(sockets.begin(), sockets.end())) + 1, &set, NULL, NULL, timeout); }
  int select(time_t _sec, time_t _usec = 0 )	{ timeval timeout = { _sec, _usec }; return ::select(int(*std::max_element(sockets.begin(), sockets.end())) + 1, &set, NULL, NULL, &timeout); }

  //	int pselect(sigset_t *sigmask, const timespec *timeout = NULL )	{ return ::pselect((*max_element(sockets.begin(), sockets.end())) + 1, &set, NULL, NULL, timeout, sigmask); }
  //	int pselect(sigset_t *sigmask, time_t _sec, time_t _usec = 0 )	{ timespec timeout = { _sec, _usec * 1000 }; return ::pselect((*max_element(sockets.begin(), sockets.end())) + 1, &set, NULL, NULL, &timeout, sigmask); }

  int poll(short events, int timeout = 0 ) const
  {
    pollfd _pollfds[sockets.size()];
    size_t index = 0;
    for (const_iterator i = sockets.begin(); i != sockets.end(); i++) {
      _pollfds[index].fd = *i;
      _pollfds[index].events = events;
      index++;
    }
    return ::poll(_pollfds, sockets.size(), timeout);
  }
	
  const SocketSet &operator = (const TCP_Socket &s )		{ clear(); append(s); return *this; }
  const SocketSet &operator = (const SocketSet &s )			{ clear(); append(s); return *this; }

  const SocketSet &operator |= (const TCP_Socket &s ) 	{ append(s); return *this; }
  const SocketSet &operator |= (const SocketSet &s )		{ append(s); return *this; }

  friend SocketSet operator | (const TCP_Socket &a, const TCP_Socket &b )	{ SocketSet result(a); result.append(b); return result; }
  friend SocketSet operator | (SocketSet a, const TCP_Socket &b )		{ a.append(b); return a; }
  friend SocketSet operator | (const TCP_Socket &a, SocketSet b )		{ b.append(a); return b; }

  friend SocketSet operator | (SocketSet a, const SocketSet &b )		{ a.append(b.sockets); return a; }

};

#define Socket TCP_Socket

#endif /* socket_hh */
