void MULTIPOLE::step_4d_tl_daniel(BEAM *beam)
{
  if (abs(strength)<std::numeric_limits<double>::epsilon()) {
    drift_step_4d(beam);
    return;
  }
  std::complex<double> Kn=strength*1e6/double(flags.thin_lens);
  for (int i=2;i<field;i++)
    Kn/=i;
  Kn*=std::polar(1.,tilt);
  /*
  std::complex<double> k=strength/double(flags.thin_lens);
  for (int i=2;i<field;i++){
    k/=double(i);
  }
  k*=std::pow(1e-6,field-2);
  double c0, s0;
#ifdef TWODIM
  sincos(tilt,&s0,&c0);
#else
  c0=1.0;
  s0=0.0;
#endif
  */
  double l=0.5*geometry.length/flags.thin_lens;
  for (int i=0;i<beam->slices;i++){
    PARTICLE &particle=beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      for (int j=0;j<flags.thin_lens;++j) {
	double kx, ky;
#ifdef TWODIM
	particle.x+=l*particle.xp;
	particle.y+=l*particle.yp;
    	// multipole_kick(field,k,s0,c0,particle.x,particle.y,kx,ky);
	std::complex<double> Kick = Kn * std::pow(std::complex<double>(particle.x, particle.y) * 1e-6, field-1);
#else
	particle.y+=l*particle.yp;
	// multipole_kick(field,k,s0,c0,0.0,particle.y,kx,ky);
	std::complex<double> Kick = Kn * std::pow(std::complex<double>(0.0, particle.y) * 1e-6, field-1);
#endif
	kx=-real(Kick)/particle.energy;
	ky=+imag(Kick)/particle.energy;
#ifdef TWODIM
	particle.xp+=kx;
	particle.x+=l*particle.xp;
#endif
	particle.yp+=ky;
	particle.y+=l*particle.yp;
      }
      drift_sigma_matrix(beam->sigma[i],geometry.length);
#ifdef TWODIM
      drift_sigma_matrix(beam->sigma_xx[i],geometry.length);	
      drift_sigma_matrix(beam->sigma_xy[i],geometry.length);
#endif
    }
  }
}
