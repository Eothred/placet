double bump_emitt(double a1, double a2)
{
  double S11,S12,S22;

  S11=bump_emitt_data.W00
    +a1*a1*bump_emitt_data.W11
    +a2*a2*bump_emitt_data.W22
    +2.0*a1*a2*bump_emitt_data.W12
    +2.0*a1*bump_emitt_data.W10
    +2.0*a2*bump_emitt_data.W20;

  S12=bump_emitt_data.Wp00
    +a1*a1*bump_emitt_data.Wp11
    +a2*a2*bump_emitt_data.Wp22
    +a1*a2*bump_emitt_data.Wp12
    +a1*a2*bump_emitt_data.Wp21
    +a1*bump_emitt_data.Wp10
    +a1*bump_emitt_data.Wp01
    +a2*bump_emitt_data.Wp20
    +a2*bump_emitt_data.Wp02;

  S22=bump_emitt_data.Wpp00
    +a1*a1*bump_emitt_data.Wpp11
    +a2*a2*bump_emitt_data.Wpp22
    +2.0*a1*a2*bump_emitt_data.Wpp12
    +2.0*a1*bump_emitt_data.Wpp10
    +2.0*a2*bump_emitt_data.Wpp20;

  //  placet_printf(INFO,"bump_emitt %g %g %g %g %g\n",a1,a2,S11,S12,S22);
  return S11*S22-S12*S12;
}

void bump_emitt_init(BEAM *beam,BUMP *bump)
{
  int i,ns,nb;
  double y=0.0,yp=0.0,a11=0.0,a12=0.0,a21=0.0,a22=0.0,wgtsum=0.0,wgt;

  nb=beam->bunches;
  ns=beam->slices_per_bunch*beam->macroparticles;
  bump_emitt_data.beam=beam;

  bump_emitt_data.wgt=0.0;

  bump_emitt_data.W00=0.0;
  bump_emitt_data.W10=0.0;
  bump_emitt_data.W20=0.0;
  bump_emitt_data.W11=0.0;
  bump_emitt_data.W12=0.0;
  bump_emitt_data.W22=0.0;

  bump_emitt_data.Wpp00=0.0;
  bump_emitt_data.Wpp10=0.0;
  bump_emitt_data.Wpp20=0.0;
  bump_emitt_data.Wpp11=0.0;
  bump_emitt_data.Wpp12=0.0;
  bump_emitt_data.Wpp22=0.0;

  bump_emitt_data.Wp00=0.0;
  bump_emitt_data.Wp10=0.0;
  bump_emitt_data.Wp01=0.0;
  bump_emitt_data.Wp20=0.0;
  bump_emitt_data.Wp02=0.0;
  bump_emitt_data.Wp11=0.0;
  bump_emitt_data.Wp12=0.0;
  bump_emitt_data.Wp21=0.0;
  bump_emitt_data.Wp22=0.0;

  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(beam->particle[i].wgt);
    bump_emitt_data.wgt+=wgt;
    y+=wgt*beam->particle[i].y;
    yp+=wgt*beam->particle[i].yp;
    wgtsum+=wgt;
    a11+=wgt*bump->a11[i];
    a12+=wgt*bump->a12[i];
    a21+=wgt*bump->a21[i];
    a22+=wgt*bump->a22[i];
  }

  for (;i<ns*nb;i++){
    wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
    bump_emitt_data.wgt+=wgt;
    y+=wgt*beam->particle[i].y;
    yp+=wgt*beam->particle[i].yp;
    wgtsum+=wgt;
    a11+=wgt*bump->a11[i];
    a12+=wgt*bump->a12[i];
    a21+=wgt*bump->a21[i];
    a22+=wgt*bump->a22[i];
  }
  y/=wgtsum;
  yp/=wgtsum;
  a11/=wgtsum;
  a12/=wgtsum;
  a21/=wgtsum;
  a22/=wgtsum;

  for (i=0;i<ns*(nb-1);i++){
    
    wgt=fabs(beam->particle[i].wgt);
    bump_emitt_data.W11+=wgt*(bump->a11[i]-a11)
      *(bump->a11[i]-a11);
    bump_emitt_data.W12+=wgt*(bump->a11[i]-a11)
      *(bump->a12[i]-a12);
    bump_emitt_data.W22+=wgt*(bump->a12[i]-a12)
      *(bump->a12[i]-a12);
    bump_emitt_data.W10+=wgt*(bump->a11[i]-a11)
      *(beam->particle[i].y-y);
    bump_emitt_data.W20+=wgt*(bump->a12[i]-a12)
      *(beam->particle[i].y-y);
    bump_emitt_data.W00+=wgt*((beam->particle[i].y-y)
			      *(beam->particle[i].y-y)+beam->sigma[i].r11);
    //    bump_emitt_data.W00+=wgt*(beam->particle[i].y
    //  *beam->particle[i].y);
    
    bump_emitt_data.Wpp11+=wgt*(bump->a21[i]-a21)
      *(bump->a21[i]-a21);
    bump_emitt_data.Wpp12+=wgt*(bump->a21[i]-a21)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wpp22+=wgt*(bump->a22[i]-a22)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wpp10+=wgt*(bump->a21[i]-a21)
      *(beam->particle[i].yp-yp);
    bump_emitt_data.Wpp20+=wgt*(bump->a22[i]-a22)
      *(beam->particle[i].yp-yp);
    bump_emitt_data.Wpp00+=wgt*((beam->particle[i].yp-yp)
				*(beam->particle[i].yp-yp)
				+beam->sigma[i].r22);
    //    bump_emitt_data.Wpp00+=wgt*(beam->particle[i].yp
    //  *beam->particle[i].yp);
    
    bump_emitt_data.Wp11+=wgt*(bump->a11[i]-a11)
      *(bump->a21[i]-a21);
    bump_emitt_data.Wp12+=wgt*(bump->a11[i]-a11)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wp21+=wgt*(bump->a12[i]-a12)
      *(bump->a21[i]-a21);
    bump_emitt_data.Wp22+=wgt*(bump->a12[i]-a12)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wp10+=wgt*(bump->a11[i]-a11)
      *(beam->particle[i].yp-yp);
    bump_emitt_data.Wp01+=wgt*(beam->particle[i].y-y)
      *(bump->a21[i]-a21);
    bump_emitt_data.Wp20+=wgt*(bump->a12[i]-a12)
      *(beam->particle[i].yp-yp);
    bump_emitt_data.Wp02+=wgt*(beam->particle[i].y-y)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wp00+=wgt*((beam->particle[i].y-y)
			       *(beam->particle[i].yp-yp)
			       +beam->sigma[i].r12);
    //    bump_emitt_data.Wp00+=wgt*(beam->particle[i].y
    //  *beam->particle[i].yp);
    
  }
  for (;i<ns*nb;i++){

    wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
    bump_emitt_data.W11+=wgt*(bump->a11[i]-a11)
      *(bump->a11[i]-a11);
    bump_emitt_data.W12+=wgt*(bump->a11[i]-a11)
      *(bump->a12[i]-a12);
    bump_emitt_data.W22+=wgt*(bump->a12[i]-a12)
      *(bump->a12[i]-a12);
    bump_emitt_data.W10+=wgt*(bump->a11[i]-a11)
      *(beam->particle[i].y-y);
    bump_emitt_data.W20+=wgt*(bump->a12[i]-a12)
      *(beam->particle[i].y-y);
    bump_emitt_data.W00+=wgt*((beam->particle[i].y-y)
			      *(beam->particle[i].y-y)+beam->sigma[i].r11);
    //    bump_emitt_data.W00+=wgt*(beam->particle[i].y
    //  *beam->particle[i].y);
    
    bump_emitt_data.Wpp11+=wgt*(bump->a21[i]-a21)
      *(bump->a21[i]-a21);
    bump_emitt_data.Wpp12+=wgt*(bump->a21[i]-a21)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wpp22+=wgt*(bump->a22[i]-a22)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wpp10+=wgt*(bump->a21[i]-a21)
      *(beam->particle[i].yp-yp);
    bump_emitt_data.Wpp20+=wgt*(bump->a22[i]-a22)
      *(beam->particle[i].yp-yp);
    bump_emitt_data.Wpp00+=wgt*((beam->particle[i].yp-yp)
				*(beam->particle[i].yp-yp)
				+beam->sigma[i].r22);
    //    bump_emitt_data.Wpp00+=wgt*(beam->particle[i].yp
    //  *beam->particle[i].yp);
    
    bump_emitt_data.Wp11+=wgt*(bump->a11[i]-a11)
      *(bump->a21[i]-a21);
    bump_emitt_data.Wp12+=wgt*(bump->a11[i]-a11)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wp21+=wgt*(bump->a12[i]-a12)
      *(bump->a21[i]-a21);
    bump_emitt_data.Wp22+=wgt*(bump->a12[i]-a12)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wp10+=wgt*(bump->a11[i]-a11)
      *(beam->particle[i].yp-yp);
    bump_emitt_data.Wp01+=wgt*(beam->particle[i].y-y)
      *(bump->a21[i]-a21);
    bump_emitt_data.Wp20+=wgt*(bump->a12[i]-a12)
      *(beam->particle[i].yp-yp);
    bump_emitt_data.Wp02+=wgt*(beam->particle[i].y-y)
      *(bump->a22[i]-a22);
    bump_emitt_data.Wp00+=wgt*((beam->particle[i].y-y)
			       *(beam->particle[i].yp-yp)
			       +beam->sigma[i].r12);
    //    bump_emitt_data.Wp00+=wgt*(beam->particle[i].y
    //  *beam->particle[i].yp);
    
  }
  //  bump_emitt_print();
}

int frprmn(double p[],int *n,double *ftol,int *iter,
			    double *fret)
{
    /* System generated locals */
    int i__1;
    double d__1;

    /* Builtin functions */

    /* Local variables */
    extern double func(double*);
    static double g[50], h__[50];
    static int j;
    extern /* Subroutine */ int dfunc(double*,double*);
    static double gg, fp, xi[50];
    extern /* Subroutine */ int linmin(double*,double*,int*,double*);
    static double dgg, gam;
    static int its;

    /* Parameter adjustments */
    --p;

    /* Function Body */
    fp = func(&p[1]);
    dfunc(&p[1], xi);
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	g[j - 1] = -xi[j - 1];
	h__[j - 1] = g[j - 1];
	xi[j - 1] = h__[j - 1];
    }
    for (its = 1; its <= 200; ++its) {
	*iter = its;
	linmin(&p[1], xi, n, fret);
	if ((d__1 = *fret - fp, fabs(d__1)) * (float)2. <= *ftol * (fabs(*fret) 
		+ fabs(fp) + 1e-10)) {
	    return 0;
	}
	fp = func(&p[1]);
	dfunc(&p[1], xi);
	gg = (float)0.;
	dgg = (float)0.;
	i__1 = *n;
	for (j = 1; j <= i__1; ++j) {
	    d__1 = g[j - 1];
	    gg += d__1 * d__1;
/*         DGG=DGG+XI(J)**2 */
	    dgg += (xi[j - 1] + g[j - 1]) * xi[j - 1];
	}
	if (gg == (float)0.) {
	    return 0;
	}
	gam = dgg / gg;
	i__1 = *n;
	for (j = 1; j <= i__1; ++j) {
	    g[j - 1] = -xi[j - 1];
	    h__[j - 1] = g[j - 1] + gam * h__[j - 1];
	    xi[j - 1] = h__[j - 1];
	}
    }
    placet_printf(INFO,"FRPR maximum iterations exceeded\n");
    exit(1);
    return 0;
} /* frprmn_ */
