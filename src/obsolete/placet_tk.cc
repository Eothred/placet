/**********************************************************************/
/*                      beamline_survey_hook (not a command)          */
/**********************************************************************/

void beamline_survey_hook(BEAMLINE * /*beamline*/)
{
  Tcl_Eval(beamline_survey_hook_interp,"survey_hook");
}

void interp_append_error(char *s)
{
  Tcl_AppendResult(store_interp.interp,s,NULL);
  store_interp.error=TCL_ERROR;
}

/**********************************************************************/
/*                      BeamlineListTransport                         */
/**********************************************************************/

int tk_BeamlineListTransport(ClientData clientdata,Tcl_Interp *interp,int argc,
			     char *argv[])
{
  int error=TCL_OK;
  char *name=NULL;
  double gradient=1.0;
  FILE *file;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-gradient",TK_ARGV_FLOAT,(char*)NULL,(char*)&gradient,
     (char*)"Gradient to multiply cavity gradient with"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  if (name){
    file=fopen(name,"w");
    beamline_list_transport(file,inter_data.beamline,gradient);
    fclose(file);
  } else {
    beamline_list_transport(stdout,inter_data.beamline,gradient);
  }
  return TCL_OK;
}

int tk_BeamlineReadTransport(ClientData clientdata,Tcl_Interp *interp,int argc,
                             char *argv[])
{
  int error=TCL_OK;
  char *name=NULL;
  FILE *file;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }
  if (name){
    file=fopen(name,"r");
    //ALNEW    transport_read_file(file,inter_data.beamline,&inter_data.girder);
    fclose(file);
  } else {
    //ALNEW    transport_read_file(stdin,inter_data.beamline,&inter_data.girder);
  }
  return TCL_OK;
}

int tk_BeamParameterPrint(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			  char *argv[])
{
  int error;
  char *file_name=(char*)"autophase.dat";
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to CheckAutophase",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  check_autophase(inter_data.beamline,inter_data.bunch,file_name);
  return TCL_OK;
}

int tk_BeamScatterBunches(ClientData clientdata,Tcl_Interp * / * interp * /,int / * argc * /,
			  char * / * argv[] * /)
{
  int i,j,k,n_bunch,n_slice,n_macro;
  BEAM *beam;
  double off_y,off_y0;
  beam=(BEAM*)clientdata;
  n_bunch=beam->bunches;
  n_slice=beam->slices_per_bunch;
  n_macro=beam->macroparticles;
  k=0;
  for (i=0;i<n_bunch;i++){
    off_y=gasdev()*off_y0; // off_y0 is uninitialised! JS
    for (j=0;j<n_slice*n_macro;j++){
      beam->particle[k].y+=off_y;
      k++;
    }
  }
  return TCL_OK;
}

int tk_BeamSetRange(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  char file_name[]="beam.dat";
  MAIN_BEAM_PARAM param;
  Tk_ArgvInfo table[]={
    {(char*)"-macroparticles",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_macro,
     (char*)"N"},
    {(char*)"-energyspread",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.espread,
     (char*)"Energy spread of intial beam"},
    {(char*)"-ecut",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.ecut,
     (char*)"Cut of the energy spread of intial beam"},
    {(char*)"-bunches",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_bunch,
     (char*)"Number of bunches"},
    {(char*)"-slices",TK_ARGV_INT,(char*)NULL,
     (char*)&param.n_slice,
     (char*)"Number of slices"},
    {(char*)"-e0",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.e0,
     (char*)"Beam energy at entrance"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&param.charge,
     (char*)"Bunch charge"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* default values */

  param.charge=0.95e11;
  param.e0=1.5;
  param.n_slice=11;
  param.n_bunch=1;
  param.n_macro=1;
  param.espread=0.0;
  param.ecut=3.0;

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  /* perform checks */

  if (argc>2){
    Tcl_SetResult(interp,"Too many arguments to MainBeam",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<2){
    Tcl_SetResult(interp,"Too few arguments to MainBeam",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (param.n_macro<1) {
    interp->result="-macroparticles must be at least 1";
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  /* run command */
  // param not fully initialised!! -- should be fixed or not used -- JS
  inter_data.bunch=make_multi_bunch(inter_data.beamline,file_name,
				    &param);

  /* create new command to manipulate beam */

  Placet_CreateCommand(interp,argv[1],&tk_Beam,inter_data.bunch,
		       NULL);

  return TCL_OK;
}

/**********************************************************************/
/*                      BeamSetSamples (not a command)                */
/**********************************************************************/

int tk_BeamSetSamples(ClientData / * clientdata * /,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error=TCL_OK;
  int i,n,bump_type,pos;
  char **v;
  BUMP *bump;
  Tk_ArgvInfo table[]={
    {(char*)"-list",TK_ARGV_STRING,(char*)NULL,(char*)&bump_type,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"list\n",NULL);
    return TCL_ERROR;
  }
  if(error=Tcl_SplitList(interp,argv[1],&n,&v)) return error;
  for (i=0;i<n;i++){
    if (error=Tcl_GetInt(interp,v[i],&pos)){
      return error;
    }
  }
  for (i=0;i<n;i++){
    Tcl_GetInt(interp,v[i],&pos);
    bump_define(inter_data.beamline,bump,pos,bump_type); // bump is uninitialised! JS
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      BpmPrepareSamples (not a command)             */
/**********************************************************************/

int tk_BpmPrepareSamples(ClientData clientdata,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error=TCL_OK;
  int i,n,bump_type,pos;
  char **v;
  BUMP *bump;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"list\n",NULL);
    return TCL_ERROR;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  if(error=Tcl_GetInt(interp,argv[1],&n)) return error;
  bpm_prepare_samples(inter_data.beamline,n);
  return TCL_OK;
}

/*
 * Prepares a number of bumps from a list
 */

int tk_BumpListPrepare(ClientData / * clientdata * /,Tcl_Interp *interp,int argc,
		       char *argv[])
{
  int error=TCL_OK;
  int i,n,bump_type,pos;
  char **v;
  BUMP *bump;
  Tk_ArgvInfo table[]={
    {(char*)"-bumptype",TK_ARGV_STRING,(char*)NULL,(char*)&bump_type,
     (char*)"Name of the file where to store the results"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"list\n",NULL);
    return TCL_ERROR;
  }
  if(error=Tcl_SplitList(interp,argv[1],&n,&v)) return error;
  for (i=0;i<n;i++){
    if (error=Tcl_GetInt(interp,v[i],&pos)){
      return error;
    }
  }
  for (i=0;i<n;i++){
    Tcl_GetInt(interp,v[i],&pos);
    bump_define(inter_data.beamline,bump,pos,bump_type); // bump is uninitialised! JS
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      DipoleWake                                    */
/**********************************************************************/

int tk_DipoleWake(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error,ok;
  Tcl_HashEntry *entry;
  char *name=NULL;
  double q=-1.0,lambda=-1.0,loss=0.0;
  static int number=0;
  WAKE_MODE *wake;
  Tk_ArgvInfo table[]={
    {(char*)"-Q",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&q,
     (char*)"Q-factor of mode"},
    {(char*)"-lambda",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&lambda,
     (char*)"wavelength of mode"},
    {(char*)"-loss",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&loss,
     (char*)"loss factor of mode"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_SetResult(interp,"Must give exactly one name in DipoleWake",TCL_VOLATILE);
    return TCL_ERROR;
  }
  name=argv[1];
  wake=(WAKE_MODE*)xmalloc(sizeof(WAKE_MODE));
  wake->Q=q;
  wake->a0=loss;
  wake->lambda=lambda;
  wake->k=2.0*PI/lambda;
  wake->which=1;
  wake->number=number++;
  if (name){
    entry=Tcl_CreateHashEntry(&ModeTable,name,&ok);
    if (ok){
      Tcl_SetHashValue(entry,wake);
      quad_wake.on=1;
      return TCL_OK;
    }
  }
  xfree(wake);
  return TCL_ERROR;
}

/**********************************************************************/
/*                      ElementSetOffset                              */
/**********************************************************************/

int tk_ElementSetOffset(ClientData /*clientdata*/,Tcl_Interp *interp,
			int argc,char *argv[])
{
  int error=0,j=-1;
  double offset_x=0, offset_y=0;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command sets the offset of an ELEMENT."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  //  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
  if (Tk_ParseArgv(interp,NULL,&argc,argv,table,0)!=TCL_OK){
    placet_printf(INFO,"%s\n",interp->result);
    return error;
  }

  if (argc!=4){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," element_number value_x value_y\n",NULL);
    return TCL_ERROR;
  }
  error=Tcl_GetInt(interp,argv[1],&j);
  if (error) {
    return error;
  }

  if ((j<0)||(j>=inter_data.beamline->n_elements)) {
    sprintf(interp->result,"Element #%d does not exist\n",j);
    return TCL_ERROR;
  }

  ELEMENT* element = inter_data.beamline->element[j];

  error=Tcl_GetDouble(interp,argv[2],&offset_x);
  if (error) {
    return error;
  }

  element->offset.x = offset_x;
  error=Tcl_GetDouble(interp,argv[3],&offset_y);
  if (error) {
    return error;
  }
  element->offset.y = offset_y;
  
  return TCL_OK;
}

int tk_EmittanceRun(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," number value\n",NULL);
    return TCL_ERROR;
  }
  char buf[20];
  snprintf(buf,20,"%d",inter_data.do_emitt);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}

/**********************************************************************/
/*                      MainPhases                                    */
/**********************************************************************/

int tk_MainPhases(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error,i,n;
  char *name=NULL;
  char **v;
  Tk_ArgvInfo table[]={
    {(char*)"-phases",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"List of phases in degrees"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"MainPhases is called only with option -phases",TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (!name) {
    Tcl_SetResult(interp,"You must specify the option -phases",TCL_VOLATILE);
    return TCL_ERROR;
  }
   
  if(error=Tcl_SplitList(interp,name,&n,&v)) return error;
  if (n>10) {
    Tcl_SetResult(interp,
		  "You can have only up to 10 phases in MainPhases",TCL_VOLATILE);
    return TCL_ERROR;
  }
  for (i=0;i<n;i++){
    if (error=Tcl_GetDouble(interp,v[i],inter_data.beamline->phase+i))
      return error;
  }
  inter_data.beamline->n_phases=n;
  Tcl_Free((char*)v);
  return TCL_OK;
}

int tk_PlotGirder()
{
  return TCL_OK;
}

/**********************************************************************/
/*                      SetBeamEnergy                                 */
/**********************************************************************/

int tk_SetBeamEnergy(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error=0;
  char *point,**v;
  BEAM *beam;
  int i,j,nb,np,m,n,nm,k;
  double derf,debeam,lambda;
  double alpha_x,beta_x,emitt_x,eps_x,alpha_y,beta_y,emitt_y,eps_y;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc>3){
    Tcl_SetResult(interp,"Too many arguments to <SetBeamEnergy>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<3){
    Tcl_SetResult(interp,"Not enough arguments to <SetBeamEnergy>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  beam=get_beam(argv[1]);
  if(error=Tcl_SplitList(interp,argv[2],&n,&v)) return error;

  // check to prevent unitialised values
  if(n<=0) {
    Tcl_SetResult(interp,"List for <SetBeamEnergy> is empty",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  np=beam->slices_per_bunch;
  nm=beam->macroparticles;
  nb=beam->bunches;
  lambda=injector_data.lambda[0];
  for (j=0;j<nb;j++){
    m=j*np;
    if (j<n) {
      derf=strtod(v[j],&point);
      debeam=strtod(point,&point);
      alpha_x=strtod(point,&point);
      beta_x=strtod(point,&point);
      emitt_x=strtod(point,&point);
      alpha_y=strtod(point,&point);
      beta_y=strtod(point,&point);
      emitt_y=strtod(point,&point);
    }
    for (i=0;i<np;i++) {
      for (k=0;k<nm;k++){
	beam->particle[m+i*nm+k].energy=derf
	  *cos(beam->z_position[m+i]*1e-6/lambda*TWOPI+
	       inter_data.beamline->phase[0]*PI/180.0)
	  +debeam*cos(beam->z_position[m+i]*1e-6/lambda*TWOPI);
	double dummy = EMASS/beam->particle[m+i*nm+k].energy*1e12*EMITT_UNIT;
	eps_x=emitt_x*dummy;
	eps_y=emitt_y*dummy;
#ifdef TWODIM
	bunch_set_sigma_xx(beam,0,i,k,beta_x,alpha_x,eps_x);
	bunch_set_sigma_xy(beam,0,i,k);
#endif
	bunch_set_sigma_yy(beam,0,i,k,beta_y,alpha_y,eps_y);
      }
    }
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      SetCentreGradient                             */
/**********************************************************************/

int tk_SetCentreGradient(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error=0;
  char *point,**v;
  BEAM *beam;
  int jt=0,i,j,nb,np,m,n;
  double *de,de_centre,de_scale;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc>3){
    Tcl_SetResult(interp,"Too many arguments to <SetCentreGradient>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<3){
    Tcl_SetResult(interp,"Not enough arguments to <SetCentreGradient>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beam=get_beam(argv[1]);
  if(error=Tcl_SplitList(interp,argv[2],&n,&v)) return error;
  np=beam->slices_per_bunch;
  nb=beam->bunches;
  de=beam->acc_field[jt];
  for (j=0;j<nb;j++){
    m=j*np;
    if (j<n) {
      de_centre=strtod(v[j],&point);
    }
    de_scale=(de_centre-beam->field[0].de[m+np/2])/de[m+np/2];
    for (i=0;i<np;i++) {
      de[m+i]*=de_scale;
    }
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestTeslaCorrection                           */
/**********************************************************************/

int tk_TestTeslaCorrection(ClientData /*clientdata*/,
			   Tcl_Interp *interp,int argc,
			   char *argv[])
{
  int error;
  int nquad=1,noverlap=0,nbin,i,nbin_rf,do_rf=0;
  void (*survey)(BEAMLINE*);
  char *file_name=NULL;
  char *beamname=NULL,*survey_name,
    survey0[]="Clic";
  BIN **bin,**bin_rf;
  BEAM *beam=NULL;
  int iter=1;
  double jitter_y=0.0,jitter_x=0.0,wgt0=1.0,wgt1=-1.0,pwgt=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,
     (char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-rf_align",TK_ARGV_INT,(char*)NULL,(char*)&do_rf,
     (char*)"Align the RF after the correction?"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to CLIC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-wgt0",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt0,
     (char*)"Weight for the BPM position"},
    {(char*)"-wgt1",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt1,
     (char*)"Weight for the BPM resolution"},
    {(char*)"-pwgt",TK_ARGV_FLOAT,(char*)NULL,(char*)&pwgt,
     (char*)"Weight for the old position"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=survey0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestFreeCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  corr.pwgt=pwgt;
  corr.w=wgt1;
  corr.w0=wgt0;

  bin=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide(inter_data.beamline,nquad,noverlap,bin,
		      &nbin);
  if (do_rf) {
    bin_rf=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
    beamline_bin_divide(inter_data.beamline,12,0,bin_rf,
			&nbin_rf);
    test_free_correction(inter_data.beamline,
			 quad_set0,quad_set1,quad_set2,
			 bin,nbin,beam,bin_rf,nbin_rf,
			 jitter_y,iter,survey,
			 file_name);
    for (i=0;i<nbin_rf;i++){
      bin_delete(bin_rf[i]);
    }
    free(bin_rf);
  }
  else{
    test_free_correction(inter_data.beamline,
			 quad_set0,quad_set1,quad_set2,
			 bin,nbin,beam,NULL,0,jitter_y,iter,
			 survey,file_name);
  }
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);
  return TCL_OK;
}

/**********************************************************************/
/*                      TwissPlotFull                                 */
/**********************************************************************/

int tk_TwissPlotFull(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error;
  char *file_default=NULL,*file_name;
  char *beam_name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beam_name,
     (char*)"Name of the beam to use for the calculation"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  file_name=file_default;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  BEAM* beam=get_beam(beam_name);
  if (file_name){
    twiss_plot_full(inter_data.beamline,beam,file_name);
  }
  else{
    store_interp.interp=interp;
      twiss_plot_full(inter_data.beamline,beam,file_name);
  }
  return TCL_OK;
}
