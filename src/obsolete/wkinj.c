/* wkinj.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    doublereal scal;
} scalc_;

#define scalc_1 scalc_

/* Table of constant values */

static integer c__5 = 5;
static integer c__1 = 1;
static integer c__3 = 3;
static doublereal c_b18 = 2.2;
static doublereal c_b19 = 2.;
static doublereal c_b22 = 0.;

/* Main program */ MAIN__()
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2;
    olist o__1;
    cllist cl__1;

    /* Builtin functions */
    integer s_rsle(), do_lio(), e_rsle(), f_open(), s_wsle(), e_wsle();
    double pow_dd();
    integer f_clos();

    /* Local variables */
    static doublereal suml, a, b;
    static integer i, j, n;
    static doublereal w[1000];
    extern /* Subroutine */ int qromb_();
    extern /* Subroutine */ int gauss_();
    static doublereal z0;
    extern doublereal fl_(), ft_();
    static doublereal dz, sl, wl[1000], wt[1000], anpart, sigmaz, fl0, ft0, 
	    rad, wgt;

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 5, 0, 0, 0 };
    static cilist io___10 = { 0, 10, 0, 0, 0 };
    static cilist io___21 = { 0, 6, 0, 0, 0 };
    static cilist io___22 = { 0, 10, 0, 0, 0 };
    static cilist io___23 = { 0, 10, 0, 0, 0 };
    static cilist io___24 = { 0, 10, 0, 0, 0 };


    s_rsle(&io___1);
    do_lio(&c__5, &c__1, (char *)&scalc_1.scal, (ftnlen)sizeof(doublereal));
    do_lio(&c__5, &c__1, (char *)&anpart, (ftnlen)sizeof(doublereal));
    do_lio(&c__5, &c__1, (char *)&sigmaz, (ftnlen)sizeof(doublereal));
    do_lio(&c__5, &c__1, (char *)&a, (ftnlen)sizeof(doublereal));
    do_lio(&c__5, &c__1, (char *)&b, (ftnlen)sizeof(doublereal));
    do_lio(&c__3, &c__1, (char *)&n, (ftnlen)sizeof(integer));
    do_lio(&c__5, &c__1, (char *)&rad, (ftnlen)sizeof(doublereal));
    e_rsle();
    sl = (float)1.;
    o__1.oerr = 0;
    o__1.ounit = 10;
    o__1.ofnmlen = 7;
    o__1.ofnm = "cav.dat";
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = 0;
    o__1.oblnk = 0;
    f_open(&o__1);
    dz = (b - a) / n;
    s_wsle(&io___10);
    do_lio(&c__3, &c__1, (char *)&n, (ftnlen)sizeof(integer));
    e_wsle();
    z0 = b;
    ft0 = sl * (float).0090603540005840845 * (anpart * (float)1.6e-19 * (
	    float)1e12) * (float)1e-6 * (float)1e3;
    fl0 = sl * (float).043460706070001742 * (anpart * (float)1.6e-19 * (float)
	    1e12) * (float)1e-6;
/*      ft0=sl*(anpart*1.6e-19*1e12)*1e-6*1e3 */
/*      fl0=sl*(anpart*1.6e-19*1e12)*1e-6 */
/* Computing 3rd power */
    d__1 = scalc_1.scal, d__2 = d__1;
    ft0 *= d__2 * (d__1 * d__1);
/* Computing 2nd power */
    d__1 = scalc_1.scal;
    fl0 *= d__1 * d__1;
    d__1 = (float)12. / rad;
    ft0 *= pow_dd(&d__1, &c_b18);
    d__1 = (float)12. / rad;
    fl0 *= pow_dd(&d__1, &c_b19);
    i__1 = n;
    for (i = 1; i <= i__1; ++i) {
	d__1 = z0 - dz;
	qromb_(gauss_, &d__1, &z0, &wgt);
	w[i - 1] = wgt;
	suml = (float)0.;
	i__2 = i - 1;
	for (j = 1; j <= i__2; ++j) {
	    d__1 = (i - j) * dz * sigmaz;
	    suml += fl_(&d__1) * w[j - 1];
/* L5: */
	}
	wl[i - 1] = (suml + fl_(&c_b22) * (float).5 * w[i - 1]) * fl0;
	d__1 = (i - 1) * dz * sigmaz;
	wt[i - 1] = ft_(&d__1) * ft0;
	s_wsle(&io___21);
	d__1 = (i - 1) * dz * sigmaz;
	do_lio(&c__5, &c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
	do_lio(&c__5, &c__1, (char *)&wl[i - 1], (ftnlen)sizeof(doublereal));
	e_wsle();
	z0 -= dz;
/* L10: */
    }
/*      wt(1)=0.5*ft(0.0)*ft0 */
    z0 = b;
    s_wsle(&io___22);
    d__1 = wl[(n + 1) / 2 - 1] / sl;
    do_lio(&c__5, &c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
    e_wsle();
    i__1 = n;
    for (i = 1; i <= i__1; ++i) {
	s_wsle(&io___23);
	d__1 = (z0 - dz * (float).5) * sigmaz;
	do_lio(&c__5, &c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
	do_lio(&c__5, &c__1, (char *)&wl[i - 1], (ftnlen)sizeof(doublereal));
	do_lio(&c__5, &c__1, (char *)&w[i - 1], (ftnlen)sizeof(doublereal));
	e_wsle();
	z0 -= dz;
/* L20: */
    }
    i__1 = n;
    for (i = 1; i <= i__1; ++i) {
	i__2 = i;
	for (j = 1; j <= i__2; ++j) {
/*            write (10,*) w(j)*wt(i-j+1) */
	    s_wsle(&io___24);
	    do_lio(&c__5, &c__1, (char *)&wt[i - j], (ftnlen)sizeof(
		    doublereal));
	    e_wsle();
/* L30: */
	}
/* L40: */
    }
    cl__1.cerr = 0;
    cl__1.cunit = 10;
    cl__1.csta = 0;
    f_clos(&cl__1);
} /* MAIN__ */

doublereal fl_(z)
doublereal *z;
{
    /* System generated locals */
    doublereal ret_val;

    /* Builtin functions */
    double sqrt(), exp();

    /* Local variables */
    static doublereal s;

    s = *z * (float).20847231487658438 * .001 * scalc_1.scal;
    ret_val = exp(sqrt(s) * (float)-.85) * (float)-250.;
    return ret_val;
} /* fl_ */

doublereal ft_(z)
doublereal *z;
{
    /* System generated locals */
    doublereal ret_val;

    /* Builtin functions */
    double sqrt(), exp();

    /* Local variables */
    static doublereal s;

    s = *z * (float).20847231487658438 * .001 * scalc_1.scal;
    ret_val = (1. - exp(sqrt(s) * -1.13) * (sqrt(s) * (float)1.13 + (float)1.)
	    ) * (float)5.45;
    return ret_val;
} /* ft_ */

doublereal gauss_(x)
doublereal *x;
{
    /* System generated locals */
    doublereal ret_val, d__1;

    /* Builtin functions */
    double exp(), acos(), sqrt();

/* Computing 2nd power */
    d__1 = *x;
    ret_val = exp(d__1 * d__1 * -.5) / sqrt(acos(-1.) * 2.);
    return ret_val;
} /* gauss_ */

/* Subroutine */ int qromb_(func, a, b, ss)
doublereal *func, *a, *b, *ss;
{
    static doublereal h[21];
    static integer j, l;
    static doublereal s[21];
    extern /* Subroutine */ int trapzd_(), polint_();
    static doublereal dss;

    h[0] = (float)1.;
    for (j = 1; j <= 20; ++j) {
	trapzd_(func, a, b, &s[j - 1], &j);
	if (j >= 5) {
	    l = j - 4;
	    polint_(&h[l - 1], &s[l - 1], &c__5, &c_b22, ss, &dss);
/* scd changed from .lt. to .le. */
	    if (abs(dss) <= abs(*ss) * 1e-6) {
		return 0;
	    }
	}
	s[j] = s[j - 1];
	h[j] = h[j - 1] * (float).25;
/* L11: */
    }
/*      PAUSE 'Too many steps.' */
} /* qromb_ */

/* Subroutine */ int trapzd_(func, a, b, s, n)
doublereal (*func) ();
doublereal *a, *b, *s;
integer *n;
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer j;
    static doublereal x;
    static integer it;
    static doublereal del, tnm, sum;

    if (*n == 1) {
	*s = (*b - *a) * (float).5 * ((*func)(a) + (*func)(b));
	it = 1;
    } else {
	tnm = (doublereal) it;
	del = (*b - *a) / tnm;
	x = *a + del * (float).5;
	sum = (float)0.;
	i__1 = it;
	for (j = 1; j <= i__1; ++j) {
	    sum += (*func)(&x);
	    x += del;
/* L11: */
	}
	*s = (*s + (*b - *a) * sum / tnm) * (float).5;
	it <<= 1;
    }
    return 0;
} /* trapzd_ */

/* Subroutine */ int polint_(xa, ya, n, x, y, dy)
doublereal *xa, *ya;
integer *n;
doublereal *x, *y, *dy;
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_paus();

    /* Local variables */
    static doublereal dift, c[10], d[10];
    static integer i, m;
    static doublereal w, ho, hp;
    static integer ns;
    static doublereal dif, den;

    /* Parameter adjustments */
    --ya;
    --xa;

    /* Function Body */
    ns = 1;
    dif = (d__1 = *x - xa[1], abs(d__1));
    i__1 = *n;
    for (i = 1; i <= i__1; ++i) {
	dift = (d__1 = *x - xa[i], abs(d__1));
	if (dift < dif) {
	    ns = i;
	    dif = dift;
	}
	c[i - 1] = ya[i];
	d[i - 1] = ya[i];
/* L11: */
    }
    *y = ya[ns];
    --ns;
    i__1 = *n - 1;
    for (m = 1; m <= i__1; ++m) {
	i__2 = *n - m;
	for (i = 1; i <= i__2; ++i) {
	    ho = xa[i] - *x;
	    hp = xa[i + m] - *x;
	    w = c[i] - d[i - 1];
	    den = ho - hp;
	    if (den == (float)0.) {
		s_paus("", 0L);
	    }
	    den = w / den;
	    d[i - 1] = hp * den;
	    c[i - 1] = ho * den;
/* L12: */
	}
	if (ns << 1 < *n - m) {
	    *dy = c[ns];
	} else {
	    *dy = d[ns - 1];
	    --ns;
	}
	*y += *dy;
/* L13: */
    }
    return 0;
} /* polint_ */

/* Subroutine */ int spline_(x, y, n, yp1, ypn, y2)
doublereal *x, *y;
integer *n;
doublereal *yp1, *ypn, *y2;
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i, k;
    static doublereal p, u[1000], qn, un, sig;

    /* Parameter adjustments */
    --y2;
    --y;
    --x;

    /* Function Body */
    if (*yp1 > 9.9e299) {
	y2[1] = (float)0.;
	u[0] = (float)0.;
    } else {
	y2[1] = (float)-.5;
	u[0] = (float)3. / (x[2] - x[1]) * ((y[2] - y[1]) / (x[2] - x[1]) - *
		yp1);
    }
    i__1 = *n - 1;
    for (i = 2; i <= i__1; ++i) {
	sig = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
	p = sig * y2[i - 1] + (float)2.;
	y2[i] = (sig - (float)1.) / p;
	u[i - 1] = (((y[i + 1] - y[i]) / (x[i + 1] - x[i]) - (y[i] - y[i - 1])
		 / (x[i] - x[i - 1])) * (float)6. / (x[i + 1] - x[i - 1]) - 
		sig * u[i - 2]) / p;
/* L11: */
    }
    if (*ypn > 9.9e299) {
	qn = (float)0.;
	un = (float)0.;
    } else {
	qn = (float).5;
	un = (float)3. / (x[*n] - x[*n - 1]) * (*ypn - (y[*n] - y[*n - 1]) / (
		x[*n] - x[*n - 1]));
    }
    y2[*n] = (un - qn * u[*n - 2]) / (qn * y2[*n - 1] + (float)1.);
    for (k = *n - 1; k >= 1; --k) {
	y2[k] = y2[k] * y2[k + 1] + u[k - 1];
/* L12: */
    }
    return 0;
} /* spline_ */

/* Subroutine */ int splint_(xa, ya, y2a, n, x, y)
doublereal *xa, *ya, *y2a;
integer *n;
doublereal *x, *y;
{
    /* System generated locals */
    doublereal d__1, d__2, d__3, d__4, d__5;

    /* Builtin functions */
    /* Subroutine */ int s_paus();

    /* Local variables */
    static doublereal a, b, h;
    static integer k, khi, klo;

    /* Parameter adjustments */
    --y2a;
    --ya;
    --xa;

    /* Function Body */
    klo = 1;
    khi = *n;
L1:
    if (khi - klo > 1) {
	k = (khi + klo) / 2;
	if (xa[k] > *x) {
	    khi = k;
	} else {
	    klo = k;
	}
	goto L1;
    }
    h = xa[khi] - xa[klo];
    if (h == (float)0.) {
	s_paus("Bad XA input.", 13L);
    }
    a = (xa[khi] - *x) / h;
    b = (*x - xa[klo]) / h;
/* Computing 3rd power */
    d__1 = a, d__2 = d__1;
/* Computing 3rd power */
    d__3 = b, d__4 = d__3;
/* Computing 2nd power */
    d__5 = h;
    *y = a * ya[klo] + b * ya[khi] + ((d__2 * (d__1 * d__1) - a) * y2a[klo] + 
	    (d__4 * (d__3 * d__3) - b) * y2a[khi]) * (d__5 * d__5) / (float)
	    6.;
    return 0;
} /* splint_ */

/* Main program alias */ int fast_ () { MAIN__ (); }
