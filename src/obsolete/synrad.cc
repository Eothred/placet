#include <cstdlib>
#include <cstdio>
#include <cmath>

#include "placet.h"
#include "placet_print.h"
#include "synrad.h"
/*
#ifdef _OPENMP
#include <omp.h>
#endif

double Ran1(void)
// Minimal Park-Miller generator with Bays-Durham shuffle
// result between 0 and 1 exclusive.
// should be of good quality but sequence limited to about 10**8 numbers
// similar to numerical recipes (in fortran page 271)

// WARNING: This is function is at the moment only reasonably thread-safe.
// Randomness has been checked in threaded mode for 100k draws and no
// correlation was found. However, the generator should still be assumed
// of bad quality in threaded mode due to potential correlation with the
// chosen seeds.
{ long const ia=16807;
  long const im=2147483647;
  long const iq=127773;
  long const ir=2836;
  long const ntab=32;
  long const ndiv=1+(im-1)/ntab;
  double const am=1./im;
  double const eps=1.2e-7;
  double const rnmx=1.-eps;

  static long IdumRan1=-123; // role of seed

  static long iv[ntab];
  static long iy=0;

#pragma omp threadprivate(IdumRan1,iv,iy)
#ifdef _OPENMP
  // Give each thread different seed on first run through..
  // This has been checked for 100k draws on 4 threads.
  if (iy==0 || IdumRan1==-123) IdumRan1-=omp_get_thread_num()*12;
#endif

  long k;
  if(IdumRan1<0 || iy==0) {
      if(-IdumRan1>1) IdumRan1=-IdumRan1; else IdumRan1=1;
      for(long j=ntab+7; j>-1 ; j--){
          k=IdumRan1/iq;
          IdumRan1=ia*(IdumRan1-k*iq)-ir*k;
          if(IdumRan1<0) IdumRan1+=im;
          if(j<ntab) iv[j]=IdumRan1;
      }
      iy=iv[0];
  }
  k=IdumRan1/iq;     // first action if not initializing
  IdumRan1=ia*(IdumRan1-k*iq)-ir*k; // compute mod(ia*IdumRan1,im) avoiding overflows
  if(IdumRan1<0) IdumRan1+=im;
  long j=iy/ndiv;
  iy=iv[j];
  iv[j]=IdumRan1;
  double result;
  if(am*iy<rnmx) result=am*iy; else result=rnmx;
  return result;
}
*/
double SynRadC(double x)
/*   x :    energy normalized to the critical energy
     returns function value SynRadC   photon spectrum dn/dx
     (integral of modified 1/3 order Bessel function)
     principal: Chebyshev series see H.H.Umstaetter CERN/PS/SM/81-13 10-3-1981
     see also my LEP Note 632 of 12/1990
     converted to C++, H.Burkhardt 21-4-1996    */
{ double synrad=0.;
  if(x>0. && x<800.) // otherwise result synrad remains 0
  { if(x<6.)
     { double a,b,z;
       z=x*x/16.-2.;
       b=          .00000000000000000012;
       a=z*b  +    .00000000000000000460;
       b=z*a-b+    .00000000000000031738;
       a=z*b-a+    .00000000000002004426;
       b=z*a-b+    .00000000000111455474;
       a=z*b-a+    .00000000005407460944;
       b=z*a-b+    .00000000226722011790;
       a=z*b-a+    .00000008125130371644;
       b=z*a-b+    .00000245751373955212;
       a=z*b-a+    .00006181256113829740;
       b=z*a-b+    .00127066381953661690;
       a=z*b-a+    .02091216799114667278;
       b=z*a-b+    .26880346058164526514;
       a=z*b-a+   2.61902183794862213818;
       b=z*a-b+  18.65250896865416256398;
       a=z*b-a+  92.95232665922707542088;
       b=z*a-b+ 308.15919413131586030542;
       a=z*b-a+ 644.86979658236221700714;
       double p;
       p=.5*z*a-b+  414.56543648832546975110;
       a=          .00000000000000000004;
       b=z*a+      .00000000000000000289;
       a=z*b-a+    .00000000000000019786;
       b=z*a-b+    .00000000000001196168;
       a=z*b-a+    .00000000000063427729;
       b=z*a-b+    .00000000002923635681;
       a=z*b-a+    .00000000115951672806;
       b=z*a-b+    .00000003910314748244;
       a=z*b-a+    .00000110599584794379;
       b=z*a-b+    .00002581451439721298;
       a=z*b-a+    .00048768692916240683;
       b=z*a-b+    .00728456195503504923;
       a=z*b-a+    .08357935463720537773;
       b=z*a-b+    .71031361199218887514;
       a=z*b-a+   4.26780261265492264837;
       b=z*a-b+  17.05540785795221885751;
       a=z*b-a+  41.83903486779678800040;
       double q;
       q=.5*z*a-b+28.41787374362784178164;
       double y;
       double const twothird=2./3.;
       y=pow(x,twothird);
       synrad=(p/y-q*y-1.)*1.81379936423421784215530788143;
     }
     else // 6 < x < 174
     { double a,b,z;
       z=20./x-2.;
       a=      .00000000000000000001;
       b=z*a  -.00000000000000000002;
       a=z*b-a+.00000000000000000006;
       b=z*a-b-.00000000000000000020;
       a=z*b-a+.00000000000000000066;
       b=z*a-b-.00000000000000000216;
       a=z*b-a+.00000000000000000721;
       b=z*a-b-.00000000000000002443;
       a=z*b-a+.00000000000000008441;
       b=z*a-b-.00000000000000029752;
       a=z*b-a+.00000000000000107116;
       b=z*a-b-.00000000000000394564;
       a=z*b-a+.00000000000001489474;
       b=z*a-b-.00000000000005773537;
       a=z*b-a+.00000000000023030657;
       b=z*a-b-.00000000000094784973;
       a=z*b-a+.00000000000403683207;
       b=z*a-b-.00000000001785432348;
       a=z*b-a+.00000000008235329314;
       b=z*a-b-.00000000039817923621;
       a=z*b-a+.00000000203088939238;
       b=z*a-b-.00000001101482369622;
       a=z*b-a+.00000006418902302372;
       b=z*a-b-.00000040756144386809;
       a=z*b-a+.00000287536465397527;
       b=z*a-b-.00002321251614543524;
       a=z*b-a+.00022505317277986004;
       b=z*a-b-.00287636803664026799;
       a=z*b-a+.06239591359332750793;
       double p;
       p=.5*z*a-b    +1.06552390798340693166;
       double const pihalf=PI/2.;
       synrad=p*sqrt(pihalf/x)/exp(x);
     }
  }
  return synrad;
}

double SynGenC(double xmin)
// C++ version
// synchrotron radiation photon spectrum generator
// returns photon energy in units of the critical energy
// xmin is the lower limit for the photon energy to be generated
//          see H.Burkhardt, LEP Note 632
{ static bool DoInit=true;
  static double a1,a2,c1,xlow,ratio,LastXmin=-1.;
  
  if(DoInit || xmin!=LastXmin)
  { DoInit=false;
     LastXmin=xmin;
     if(xmin>1.) xlow=xmin; else xlow=1.;
     // initialize constants used in the approximate expressions
     // for SYNRAD   (integral over the modified Bessel function K5/3)
    a1=SynRadC(1.e-38)/pow(1.e-38,-2./3.); // = 2**2/3 GAMMA(2/3)
    a2=SynRadC(xlow)/exp(-xlow);
    c1=pow(xmin,1./3.);
     // calculate the integrals of the approximate expressions
    if(xmin<1.) // low and high approx needed
    { double sum1ap=3.*a1*(1.-pow(xmin,1./3.)); //     integral xmin --> 1
      double sum2ap=a2*exp(-1.);                    //     integral 1 --> infin
      ratio=sum1ap/(sum1ap+sum2ap);
    }
    else // only high approx needed
    {  ratio=0.;     //     generate only high energies using approx. 2
       a2=SynRadC(xlow)/exp(-xlow);
    }
  }
  // Init done, now generate
  double appr,exact,result;
  do
  { if(Radiation.Uniform()<ratio)           // use low energy approximation
     { result=c1+(1.-c1)*Radiation.Uniform();
       result*=result*result;  // take to 3rd power;
       exact=SynRadC(result);
       appr=a1*pow(result,-2./3.);
     }
     else                      // use high energy approximation
     { result=xlow-log(Radiation.Uniform());
       exact=SynRadC(result);
       appr=a2*exp(-result);
     }
  }
  while(exact<appr*Radiation.Uniform());    // reject in proportion of approx
  return result;               // result now exact spectrum with unity weight
}

double synrad_free_path()
{
  return -log(1.0-Radiation.Uniform());
}

double synrad_eloss(double e,double angle,double length)
{
  double const c2=1.5*HBAR*C_LIGHT/(EMASS*EMASS*EMASS);
  double ecrit=c2*angle/length*e*e*e;
  double tmp=SynGenC(0.0)*ecrit;
  if (fabs(tmp)>fabs(e))
    tmp=e;
  /*
  if (angle>0.001) {
    exit(1);
  }
  */
  //   sum+=tmp;
  //   placet_printf(INFO,"%g %d\n",sum,++n);
  return tmp;
}

double
SynRadEloss(double e,double angle,double length)
{
    double ecrit,prob,free_path,eloss=0.0;
    double const c1=9.06899682117109/(TWOPI)*ALPHA_EM/EMASS;
    double const c2=1.5*HBAR*C_LIGHT/(EMASS*EMASS*EMASS);
    static double w=0.0;

    prob=c1*fabs(angle)*e;
    if (prob<0.1) {
        if (Radiation.Uniform()<prob) {
            ecrit=c2*angle/length*e*e*e;
            return SynGenC(0.0)*ecrit;
        }
        return 0.0;
    }
    else {
      if (prob>w) {
        placet_printf(WARNING,
                "WARNING: probability of synchrotron radiation high (%g)\n",
                prob);
        w=prob;
      }
      ecrit=c2*angle/length*e*e*e;
      while ((free_path=-log(1.0-Radiation.Uniform()))<prob){
        prob-=free_path;
        eloss+=SynGenC(0.0)*ecrit;
      }
    }
    return eloss;
}
