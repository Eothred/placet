#include <cstdlib>
#include <cstdio>
#include <cmath>

// #define N 10000000
#define TWOPI 6.283185307179586
// #define TWOPI_INV 0.159154943091895

double *quick_s,*quick_c,quick_i,quick_f;
int quick_n,quick_n2,quick_n4;

void
sin_cos_prepare()
{
  int i;
  quick_n=1000;
  quick_n2=quick_n/2;
  quick_n4=quick_n/4;
  quick_s=(double*)malloc(sizeof(double)*quick_n);
  quick_c=(double*)malloc(sizeof(double)*quick_n);
  quick_i=quick_n/TWOPI;
  quick_f=TWOPI/quick_n;
  for(i=0;i<quick_n;++i){
    quick_s[i]=sin((i)*TWOPI/quick_n);
    quick_c[i]=cos((i)*TWOPI/quick_n);
  }
}

void
quick_start()
{
  sin_cos_prepare();
}
