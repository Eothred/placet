double average_beta_drift(R_MATRIX *sigma,double l)
{
  return sigma->r11+l*(sigma->r12+l*sigma->r22/3.0);
}

double average_drift(R_MATRIX *sigma,double l)
{
  return sigma->r11+l*(2.0*sigma->r12+l*sigma->r22);
}

void ballistic_set_print(FILE *file)
{
  fprintf(file,"Ballistic correction method is using the following parameters\n");
  fprintf(file,"Number of total iterations: %d\n",ballistic_data.nta);
  fprintf(file,"Number of iterations for ballistic step: %d\n",
	  ballistic_data.ntn);
  fprintf(file,"Number of iterations for few-to-few: %d\n",
	  ballistic_data.ntc);
  fprintf(file,"Method to determine response coefficients: ");
  switch(ballistic_data.measure_online){
  case -1:
    fprintf(file,"Without wakefields\n");
    break;
  case 0:
    fprintf(file,"Perfect knowledge\n");
    break;
  case 1:
    fprintf(file,"Measure for first case\n");
    break;
  case 2:
    fprintf(file,"Measure for each case\n");
    break;
  }
}

void beam_position_store(BEAM *beam,char *name)
{
  FILE *file;
  int i,n;
  if (name){
    file=fopen(name,"w");
  }
  else{
    file=stdout;
  }
  n=beam->slices;
  for (i=0;i<n;i++){
    fprintf(file,"%g %g %g %g\n",beam->particle[i].energy,
	    beam->particle[i].y/sqrt(beam->sigma[i].r11),
	    beam->particle[i].yp/sqrt(beam->sigma[i].r22),
	    beam->sigma[i].r12/sqrt(beam->sigma[i].r11*beam->sigma[i].r22-
				    beam->sigma[i].r12*beam->sigma[i].r21));
  }
  if (name){
    fclose(file);
  }
}

void drive_linac_new(BEAMLINE *beamline)
{
  double tmp;
  int i;
  BEAM *bunch0;

//  SCAL=1.0;
//  MODEL=0;
  SHIFT=1.07;
  FOCUS=0.3;


  ATTENUATION=pow(0.91,1.0/(1.0-0.409));
  wakefield_init(1,1);
  / *
  drive_data.l_cav=1.023;
  drive_data.beta_group_t=0.409;
  drive_data.beta_group_l=0.409;
  drive_data.r_over_q=50.0*1.1;
  drive_data.a0=1000.0*1.1;
  * /
#ifdef MULTI
  emitt_store_switch_2(0);
#else
  emitt_store_switch(0);
#endif

  make_bunch_init(0.0,0.0,1);
  corr.pwgt=0.0;
  bump_init(1,0,0);

  zero_point=0.0;

//  bunch0=make_multi_bunch_drive(beamline,"beam.dat",1301);

  tmp=0.1*sqrt(bunch0->sigma[0].r11); // bunch0 is uninitialised! JS
  for (i=0;i<bunch0->slices;i++){
    bunch0->particle[i].y=tmp;
  }
  test_no_correction(beamline,bunch0,NULL,1,&beamline_set_zero,
		     "DD.tmp");
  return;
}

static double my_f(const gsl_vector *v, void * /*params*/ )
{
	double x[5]; 

	for (int i=0; i<4; i++)
		x[i+1] = gsl_vector_get(v, i);

	return optimize_phase(x); 
}

void test_autophase(BEAMLINE *beamline,BEAM *bunch0,void (*survey)(BEAMLINE*))
{
	int nbin;

	BIN **bin = (BIN**)xmalloc(sizeof(BIN*) * MAX_BIN);
	beamline_bin_divide_1(beamline,1,0,bin,&nbin);

	optimize_init(beamline,bin,nbin,bunch0,survey);

	const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex;
	gsl_multimin_fminimizer *s = NULL;
	gsl_multimin_function minex_func;
	gsl_vector *ss, *x;

	int iter = 0;
	int status;
	double size;

	/* Starting point */
	x = gsl_vector_alloc(4);

	gsl_vector_set(x, 0, -10.0);
	gsl_vector_set(x, 1,   8.0);
	gsl_vector_set(x, 2,  11.0);
	gsl_vector_set(x, 3, 290.0);

	/* Initial vertex size vector */
	ss = gsl_vector_alloc(4);

	/* Set all step sizes */
	gsl_vector_set(ss, 0, -10.0 * 0.2);
	gsl_vector_set(ss, 1,   8.0 * 0.2);
	gsl_vector_set(ss, 2,  11.0 * 0.2);
	gsl_vector_set(ss, 3, 290.0 * 0.2);

	/* Initialize method and iterate */
	minex_func.f = &my_f;
	minex_func.n = 4;
	minex_func.params = (void *)NULL;

	s = gsl_multimin_fminimizer_alloc(T, 4);
	gsl_multimin_fminimizer_set(s, &minex_func, x, ss);

	do {
		iter++;
		status = gsl_multimin_fminimizer_iterate(s);

		if (status) break;

		size = gsl_multimin_fminimizer_size (s);
		status = gsl_multimin_test_size (size, 1e-60);

		if (status == GSL_SUCCESS)
		{
			printf ("converged to minimum at\n");

			printf ("%5d ", iter);
			for (int i = 0; i < 4; i++)
			{
				printf ("%10.3e ", gsl_vector_get (s->x, i));
			}
		}
		
		printf ("f() = %7.3f size = %.3f\n", s->fval, size);

	} while (status == GSL_CONTINUE && iter < 100);

	for (int i=0;i<4;i++)
	{
		printf("%g\n", gsl_vector_get (s->x, i));
  	}
	
	printf("%g\n", s->fval);
  
  	gsl_vector_free(x);
	gsl_vector_free(ss);
	gsl_multimin_fminimizer_free (s);
}

void test_bump(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
	  void (*survey)(BEAMLINE*),char *name)
{
  const int step_correct=1;
  BEAM *tb,*workbunch,*testbunch;
  BUMP **bump;
  int i,j=0,n,ib,nloop=5;
  FILE *file;
  double s[2];
  bump=(BUMP**)xmalloc(sizeof(BUMP*)*nloop);
  tb=bunch_remake(bunch);
  workbunch=bunch_remake(bunch);
  testbunch=bunch_remake(bunch);
  for (i=0;i<nloop;i++){
    bump[i]=bump_make(bunch->slices);
    bump_define(beamline,bump[i],2700*(i+1)*2,1);
//    bump_define(beamline,bump[i],bin[7*i]->quad[bin[7*i]->nquad-10]-1);
    beam_copy(bunch,tb);
    bunch_track(beamline,tb,0,bump[i]->start);
    bump_fill(beamline,bump[i],tb,workbunch,testbunch);
    printf("bump #%d\n",i);
    for (j=0;j<nbin;j++){
      if (bin[j]->start>bump[i]->stop) break;
    }
    bump[i]->start_bin=j;
  }
  emitt_store_init(beamline->n_quad);
  for (n=0;n<100;n++){
//    file=fopen("bump.out","w");
    beamline_set_zero(beamline);
    survey(beamline);
    beam_copy(bunch,tb);
    simple_correct(beamline,bin,nbin,tb,workbunch);
printf("first: %g\n",emitt_y(tb));
    j=0;
    i=0;
    beam_copy(bunch,tb);
    for (ib=0;ib<nloop;ib++){
      for (;i<bump[ib]->start;i++){
	beamline->element[i]->track(tb);
//	if (beamline->element[i]->is_quad()){
//	  fprintf(file,"%d %g\n",j++,emitt_y(tb));
//	  emitt_store(j,tb);
//	}
      }
      beam_copy(tb,workbunch);
      bump_step(beamline,bump[ib],workbunch,testbunch);
//bunch_track(beamline,workbunch,bump[ib]->start,bump[ib]->stop);
printf("w: %g %g %g\n",emitt_y(workbunch),bunch_get_offset_y(workbunch),
bunch_get_offset_yp(workbunch));
      bump_calculate(bump[ib],workbunch,s);
      printf("c: %g %g\n",s[0],s[1]);
      bump_set(beamline,bump[ib],0,s[0]);
      bump_set(beamline,bump[ib],1,s[1]);
      bump_step(beamline,bump[ib],tb,workbunch);
printf("t: %g %g %g\n",emitt_y(tb),bunch_get_offset_y(tb),
bunch_get_offset_yp(tb));
      i=bump[ib]->stop;
/*test*/
      switch (step_correct){
      case 0:
	break;
      case 1:
	beam_copy(tb,workbunch);
	if (ib<nloop-1){
	  simple_correct_range(beamline,bin+bump[ib]->start_bin,
			       bump[ib+1]->start_bin-bump[ib]->start_bin,
			       workbunch,testbunch,
			       bump[ib]->stop,bump[ib+1]->stop);
	}
	else{
	  simple_correct_range(beamline,bin+bump[ib]->start_bin,
			       nbin-bump[ib]->start_bin,workbunch,testbunch,
			       bump[ib]->stop,beamline->n_elements);
	}
	break;
      }
/**/
//printf("s: %d %g\n",ib,emitt_y(workbunch));
    }
    for (;i<beamline->n_elements;i++){
      beamline->element[i]->track(tb);
//      if (beamline->element[i]->is_quad()){
//	fprintf(file,"%d %g\n",j++,emitt_y(tb));
//	emitt_store(j,tb);
//      }
    }
beam_copy(bunch,tb);
j=0;
file=fopen("bump.out","w");
//simple_correct_emitt(beamline,bin,nbin,tb,workbunch);
for (i=0;i<beamline->n_elements;i++){
  beamline->element[i]->track(tb);
  if (beamline->element[i]->is_quad()){
    fprintf(file,"%d %g %g %g\n",j++,emitt_y(tb),bunch_get_offset_y(tb),
	    bunch_get_offset_yp(tb));
    emitt_store(j,tb);
  }
}
    fclose(file);
    emitt_store(beamline->n_quad,tb);
    printf("%d %g\n",n,emitt_y(tb));
  }
//  }
  emitt_print(name);
}

void test_bump_2(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		 void (*survey)(BEAMLINE*),char * /*name*/)
{
  const int btype=1; // step_correct=1
  BEAM *tb,*workbunch,*testbunch;
  BUMP **bump;
  int i,loop[]={-1};
  int nloop=0;

  while(loop[nloop]>=0) nloop++;
  bump=(BUMP**)xmalloc(sizeof(BUMP*)*nloop);
  tb=bunch_remake(bunch);
  workbunch=bunch_remake(bunch);
  testbunch=bunch_remake(bunch);
  bump_prepare(beamline,bin,nbin,bump,loop,btype,bunch,tb,workbunch,
	       testbunch);
  for (i=0;i<100;i++){
    beam_copy(bunch,tb);
    survey(beamline);
    bump_correct(beamline,bin,nbin,bump,loop,tb,workbunch,testbunch,0);
    printf("emitt: %d %g\n",i,emitt_y(tb));
  }
}

void test_free_correction_drive(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
			   int niter,void (*survey)(BEAMLINE*),char *name)
{
  BEAM *tb0,*tb0_1,*workbunch;
  int i,do_emitt,do_jitter=0;
  double esum=0.0,emitt0,esum2=0.0; //offset_y=1.3;

  corr.pwgt=0.0;
  corr.w=1.0/(2.0*0.1*0.1);
  corr.w0=1.0/(10.0*10.0+0.1*0.1);
//  corr.w=0.0;
  corr.w0=1.0;
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_store_init(beamline->n_quad);
  }
  tb0=bunch_remake(bunch0);
  if (do_jitter){
    tb0_1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  bunch_copy_0(bunch0,tb0);
  free_bin_fill_drive(beamline,tb0,workbunch,bin,nbin);
  if (do_emitt==0) {
    beam_copy(bunch0,tb0);
    bunch_track(beamline,tb0,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    beam_copy(bunch0,tb0);
    free_correct_drive(beamline,bin,nbin,tb0,workbunch,1);
    emitt0=emitt_y(tb0);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    printf("%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  printf("emitt= %g +/- %g\n",esum,
	 sqrt(std::max(0.0,esum2-esum*esum)/(double)niter));
  beam_delete(workbunch);
  beam_delete(tb0);
  if (do_emitt){
    emitt_print(name);
    emitt_store_delete();
  }
  if (do_jitter){
    beam_delete(tb0_1);
  }
}

void test_multibunch(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
		void (*survey)(BEAMLINE*))
{
  BEAM *bunch,*tb,*tb_m,*workbunch;
  FILE *file;
  int i;
  double e1,e2,tmp;

  rndm_load("track.rndm");
  tb_m=bunch_remake(bunch0);
  bunch=bunch_remake(bunch0);
  beam_copy(bunch0,bunch);
  bunch->slices=11;
  bunch->slices_per_bunch=11;
  for (i=0;i<11;i++){
    bunch->particle[i].wgt*=45.0;
  }
  tb=bunch_remake(bunch);
  workbunch=bunch_remake(bunch);
  beam_copy(bunch,tb);
  simple_bin_fill(beamline,bin,nbin,tb,workbunch);
  for (i=0;i<1;i++){
    survey(beamline);
    beam_copy(bunch,tb);
    simple_correct(beamline,bin,nbin,tb,workbunch);
    e1=emitt_y_range(tb,0,11,&tmp);
    beam_copy(bunch0,tb_m);
    bunch_track(beamline,tb_m,0,beamline->n_elements);
    e2=emitt_y(tb_m);
  }
  file=fopen("multi.res","a");
  fprintf(file,"%g %g %g %g\n",e1,e2,e2-e1,e2/e1);
  fclose(file);
  rndm_save("track.rndm");
}

void write_tor(BEAMLINE *beamline,BEAM *bunch,char *name)
{
  FILE *file;
  int i,ipos=0,nc;
  ELEMENT *element;

  nc=bunch->slices/2;
  file=fopen(name,"w");
  for (i=0;i<beamline->n_elements;i++){
    element=beamline->element[i];
    if (QUADRUPOLE *quad=element->quad_ptr()) {
      bunch_track(beamline,bunch,ipos,i);
      fprintf(file," \"QUAD\" %g %g 0.0\n",element->get_length(),
	      -quad->get_strength()/(element->get_length()*bunch->particle[nc].energy));
      ipos=i;
    } else if (element->is_drift()) {
      fprintf(file," \"DRIFT\" %g 0.0 0.0\n",element->get_length());
    } else if (element->is_drift()) {
      fprintf(file," \"DRIFT\" %g 0.0 0.0\n",0.5*element->get_length());
      fprintf(file," \"BPM\" 0.0 0.0 0.0\n");
      fprintf(file," \"DRIFT\" %g 0.0 0.0\n",0.5*element->get_length());
    } else if (element->is_cavity() || element->is_cavity_pets()) {
      fprintf(file," \"CAVITY\" %g 100.0 0.0\n",element->get_length());
    }
  }
  fclose(file);
}
