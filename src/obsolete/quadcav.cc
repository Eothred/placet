#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "bessel.h"
#include "matrix.h"
#include "placeti3.h"
#include "quadcav.h"
#include "solcav.h"
#include "wakekick.h"

#ifndef PI
#define PI 3.141592653589793
#endif

extern WAKEFIELD_DATA_STRUCT wakefield_data;

void quadcav_coefficients(double gradient,double k,double length,double energy0,
		     R_MATRIX *r1,R_MATRIX *r2)
{
  double j0_0,j1_0,y0_0,y1_0,k0_0,k1_0,i0_0,i1_0;
  double j0_1,j1_1,y0_1,y1_1,k0_1,k1_1,i0_1,i1_1;
  double a_0,a_1,energy1;
  R_MATRIX *rswap;
  if (k<0.0) {
    rswap=r1;
    r1=r2;
    r2=rswap;
    k=-k;
  }
  k/=length;
  energy1=energy0+length*gradient;
  a_0=2.0*sqrt(k*energy0)/gradient;
  j0_0=bessel_j0(a_0);
  j1_0=bessel_j1(a_0);
  i0_0=bessel_i0(a_0);
  i1_0=bessel_i1(a_0);
  y0_0=bessel_y0(a_0);
  y1_0=bessel_y1(a_0);
  k0_0=bessel_k0(a_0);
  k1_0=bessel_k1(a_0);
  a_1=2.0*sqrt(k*energy1)/gradient;
  j0_1=bessel_j0(a_1);
  j1_1=bessel_j1(a_1);
  i0_1=bessel_i0(a_1);
  i1_1=bessel_i1(a_1);
  y0_1=bessel_y0(a_1);
  y1_1=bessel_y1(a_1);
  k0_1=bessel_k0(a_1);
  k1_1=bessel_k1(a_1);

  r2->r11=a_0*(k1_0*i0_1+k0_1*i1_0);
  r2->r12=2.0*energy0/gradient*(k0_0*i0_1-k0_1*i0_0);
  //  r2->r21=2.0/gradient*k*sqrt(energy0/energy1)*(k1_0*i1_1-k1_1*i1_0);
  r2->r21=2.0/gradient*k*sqrt(energy1/energy0)*(k1_0*i1_1-k1_1*i1_0);
  r2->r22=2.0*energy0/gradient*sqrt(k/energy1)*(k1_1*i0_0+k0_0*i1_1);

  a_0*=0.5;
  r1->r11=PI*a_0*(j1_0*y0_1-j0_1*y1_0);
  r1->r12=PI*energy0/gradient*(j0_0*y0_1-j0_1*y0_0);
  //  r1->r21=PI/gradient*k*sqrt(energy0/energy1)*(j1_1*y1_0-j1_0*y1_1);
  r1->r21=PI/gradient*k*sqrt(energy1/energy0)*(j1_1*y1_0-j1_0*y1_1);
  r1->r22=PI*energy0/gradient*sqrt(k/energy1)*(j1_1*y0_0-j0_0*y1_1);
}

void quadcav_coefficients2(double gradient,double k,double length,double energy0,
		      R_MATRIX *r1,R_MATRIX *r2,int n)
{
  double dl,energy1;
  int i;
  R_MATRIX *rswap,rc,rq;
  if (k<0.0) {
    rswap=r1;
    r1=r2;
    r2=rswap;
    k=-k;
  }
  r1->r11=1.0;
  r1->r12=0.0;
  r1->r21=0.0;
  r1->r22=1.0;
  r2->r11=1.0;
  r2->r12=0.0;
  r2->r21=0.0;
  r2->r22=1.0;

  dl=length/(double)n;
  k/=(double)n;
  rc.r11=1.0;
  rc.r12=dl;
  rc.r21=0.0;

  rq.r11=1.0;
  rq.r12=0.0;
  rq.r22=1.0;

  for(i=0;i<n;i++){

    energy1=energy0+dl*gradient;
    rc.r22=energy0/energy1;
    energy0=energy1;
    mult_M_M(&rc,r1,r1);
    mult_M_M(&rc,r2,r2);

    rq.r21=-k/energy0;
    mult_M_M(&rq,r1,r1);
    rq.r21=-rq.r21;
    mult_M_M(&rq,r2,r2);

  }
}

void quadcav_coefficients3(double gradient,double k,double length,double energy0,
		      R_MATRIX *r1,R_MATRIX *r2,int n)
{
  double dl,energy1;
  int i;
  R_MATRIX *rswap,rc,rq;
  if (k<0.0) {
    rswap=r1;
    r1=r2;
    r2=rswap;
    k=-k;
  }
  r1->r11=1.0;
  r1->r12=0.0;
  r1->r21=0.0;
  r1->r22=1.0;
  r2->r11=1.0;
  r2->r12=0.0;
  r2->r21=0.0;
  r2->r22=1.0;

  dl=length/(double)n;
  k/=(double)n;
  rc.r11=1.0;
  rc.r12=dl;
  rc.r21=0.0;

  rq.r11=1.0;
  rq.r12=0.0;
  rq.r22=1.0;

  rq.r21=-0.5*k/energy0;
  mult_M_M(&rq,r1,r1);
  rq.r21=-rq.r21;
  mult_M_M(&rq,r2,r2);

  for(i=0;i<n;i++){
    energy1=energy0+dl*gradient;
    rc.r22=energy0/energy1;
    rc.r12=dl*(1.0-0.5*dl*gradient/energy0);
    energy0=energy1;
    mult_M_M(&rc,r1,r1);
    mult_M_M(&rc,r2,r2);
    rq.r21=-k/energy0;
    mult_M_M(&rq,r1,r1);
    rq.r21=-rq.r21;
    mult_M_M(&rq,r2,r2);
  }

  rq.r21=0.5*k/energy0;
  mult_M_M(&rq,r1,r1);
  rq.r21=-rq.r21;
  mult_M_M(&rq,r2,r2);
}

void quadcav_sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		       double energy,double d_g)
{
  cavity_sigma_step_end(sigma,sigma_xx,sigma_xy,energy,d_g);
}

void quadcav_sigma_step(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double length,double gradient,double k)
{
  R_MATRIX r1,r2;
  quadcav_coefficients2(gradient,k,length,energy,&r1,&r2,30);
  mult_M_T(&r1,sigma,sigma);
  mult_M_M(sigma_xy,&r2,sigma_xy);
  mult_M_M(&r1,sigma_xy,sigma_xy);
  mult_M_T(&r2,sigma_xx,sigma_xx);
  return;
}

void quadcav_step(ELEMENT *element,BEAM *beam)
{
  double factor,de0,length,half_length;
  double *de,tmp,s_long,c_long,*s_beam,*c_beam;
  int i;
  R_MATRIX r1,r2;

  factor=beam->factor;
  length=element->length;
  half_length=0.5*length;
  de=beam->acc_field[element->field];
  s_beam=beam->s_long[0];
  c_beam=beam->c_long[0];
  s_long=sin(element->v5);
  c_long=cos(element->v5);

/*
  Move beam to the centre of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field->de[i/beam->macroparticles];
    quadcav_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			   beam->particle[i].energy,de0);
    quadcav_coefficients2(de0,0.5*element->v2,half_length,
			  beam->particle[i].energy,
			  &r1,&r2,30);
    tmp=beam->particle[i].y;
    beam->particle[i].y*=r1.r11;
    beam->particle[i].y+=r1.r12*beam->particle[i].yp;
    beam->particle[i].yp*=r1.r22;
    beam->particle[i].yp+=r1.r21*tmp;
#ifdef TWODIM
    beam->particle[i].x*=r2.r11;
    beam->particle[i].x+=r2.r12*beam->particle[i].xp;
    beam->particle[i].xp*=r2.r22;
    beam->particle[i].xp+=r2.r21*tmp;
#endif
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
    transpose_M(beam->sigma+i);
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
#ifdef TWODIM
    mult_M_M(&r2,beam->sigma_xx+i,beam->sigma_xx+i);
    transpose_M(beam->sigma_xx+i);
    mult_M_M(&r2,beam->sigma_xx+i,beam->sigma_xx+i);
    transpose_M(&r1);
    mult_M_M(beam->sigma_xy+i,&r2,beam->sigma_xy+i);
    mult_M_M(&r1,beam->sigma_xy+i,beam->sigma_xy+i);
#endif
    beam->particle[i].energy+=half_length*de0;
  }

/*
  Apply the wakefield kick
*/

  if(wakefield_data.transv){
    wake_kick(element,beam,length);
  }

/*
  Move beam to the end of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field->de[i/beam->macroparticles];
    quadcav_coefficients2(de0,0.5*element->v2,half_length,
			  beam->particle[i].energy,
			  &r1,&r2,30);
    tmp=beam->particle[i].y;
    beam->particle[i].y*=r1.r11;
    beam->particle[i].y+=r1.r12*beam->particle[i].yp;
    beam->particle[i].yp*=r1.r22;
    beam->particle[i].yp+=r1.r21*tmp;
#ifdef TWODIM
    beam->particle[i].x*=r2.r11;
    beam->particle[i].x+=r2.r12*beam->particle[i].xp;
    beam->particle[i].xp*=r2.r22;
    beam->particle[i].xp+=r2.r21*tmp;
#endif
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
    transpose_M(beam->sigma+i);
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
#ifdef TWODIM
    mult_M_M(&r2,beam->sigma_xx+i,beam->sigma_xx+i);
    transpose_M(beam->sigma_xx+i);
    mult_M_M(&r2,beam->sigma_xx+i,beam->sigma_xx+i);
    transpose_M(&r1);
    mult_M_M(beam->sigma_xy+i,&r2,beam->sigma_xy+i);
    mult_M_M(&r1,beam->sigma_xy+i,beam->sigma_xy+i);
#endif
    beam->particle[i].energy+=half_length*de0;
    quadcav_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			   beam->particle[i].energy,-de0);
  }
}

void quadcav_step_4d_0(ELEMENT *element,BEAM *beam)
{
  double factor,de0,length,half_length;
  double *de,tmp,s_long,c_long,*s_beam,*c_beam;
  int i;
  R_MATRIX r1,r2;

  factor=beam->factor;
  length=element->length;
  half_length=0.5*length;
  de=beam->acc_field[element->field];
  s_beam=beam->s_long[0];
  c_beam=beam->c_long[0];
  s_long=sin(element->v5);
  c_long=cos(element->v5);

/*
  Move beam to the centre of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field->de[i/beam->macroparticles];
    quadcav_coefficients2(de0,0.5*element->v2,half_length,
			  beam->particle[i].energy,
			  &r1,&r2,30);
    tmp=beam->particle[i].y;
    beam->particle[i].y*=r1.r11;
    beam->particle[i].y+=r1.r12*beam->particle[i].yp;
    beam->particle[i].yp*=r1.r22;
    beam->particle[i].yp+=r1.r21*tmp;
#ifdef TWODIM
    beam->particle[i].x*=r2.r11;
    beam->particle[i].x+=r2.r12*beam->particle[i].xp;
    beam->particle[i].xp*=r2.r22;
    beam->particle[i].xp+=r2.r21*tmp;
#endif
    beam->particle[i].energy+=half_length*de0;
  }

/*
  Apply the wakefield kick
*/

  if(wakefield_data.transv){
    wake_kick(element,beam,length);
  }

/*
  Move beam to the end of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field->de[i/beam->macroparticles];
    quadcav_coefficients2(de0,0.5*element->v2,half_length,
			  beam->particle[i].energy,
			  &r1,&r2,30);
    tmp=beam->particle[i].y;
    beam->particle[i].y*=r1.r11;
    beam->particle[i].y+=r1.r12*beam->particle[i].yp;
    beam->particle[i].yp*=r1.r22;
    beam->particle[i].yp+=r1.r21*tmp;
#ifdef TWODIM
    beam->particle[i].x*=r2.r11;
    beam->particle[i].x+=r2.r12*beam->particle[i].xp;
    beam->particle[i].xp*=r2.r22;
    beam->particle[i].xp+=r2.r21*tmp;
#endif
    beam->particle[i].energy+=half_length*de0;
  }
}

void quadcav_step_twiss(ELEMENT *element,BEAM *beam,FILE *file,double step0,int j,
		   double s,void (*callback)(FILE*,BEAM*,int,double))
{
  double length;
  int i,nstep,istep;
  double *de,de0,factor,half_length,s_long,c_long,*s_beam,*c_beam;

  factor=beam->factor;
  nstep=(int)(element->length/step0)+1;
  length=element->length/(nstep);
  half_length=0.5*length;
  de=beam->acc_field[element->field];
  s_beam=beam->s_long[0];
  c_beam=beam->c_long[0];
  s_long=sin(element->v5);
  c_long=cos(element->v5);

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field->de[i/beam->macroparticles];
    quadcav_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			   beam->particle[i].energy,de0);
  }
  callback(file,beam,j,s);
  for (istep=0;istep<nstep;istep++){
    s+=length;
    for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field->de[i/beam->macroparticles];
      quadcav_sigma_step(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			beam->particle[i].energy,length,de0,
			element->v2);
      beam->particle[i].energy+=de0*length;
    }
    callback(file,beam,j,s);
  }
  for (i=0;i<beam->slices;i++){
    quadcav_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			  beam->particle[i].energy,-de0);
  }
}

ELEMENT* quadcav_make(double length,double gradient,double k,double phase)
{
  ELEMENT *element;
  element=element_make(length,0);
  element->type=QUADCAV;
  element->v1=gradient;
  element->v2=-k;
  element->v5=phase*PI/180.0;
  return element;
}
