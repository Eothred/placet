double*
make_matrix(int nknob,int nbpm)
{
  double *a;
  int i;
  a=(double*)malloc(sizeof(double)*nknob*nbpm);
  for(i=0;i<nbpm*nknob;++i){
    a[i]=0.0;
  }
  for(i=0;i<nbpm;++i){
    a[2*i*nbpm+i]=1.0;
    a[(2*i+1)*nbpm+i]=1.0;
  }
  return a;
}

double*
make_matrix2(int nknob,int nbpm)
{
  double *a;
  int i;
  a=(double*)malloc(sizeof(double)*nknob*nbpm);
  for(i=0;i<nbpm*nknob;++i){
    a[i]=0.0;
  }
  for(i=0;i<nbpm;++i){
    a[2*i*nbpm+i]=1.0+0.0001*i*i;
    a[(2*i+1)*nbpm+i]=1.0+0.0001*i*i;
  }
  return a;
}

double*
make_matrix_old(int nknob,int nbpm)
{
  double *a;
  int i;
  a=(double*)malloc(sizeof(double)*nknob*nbpm);
  for(i=0;i<nbpm*nknob;++i){
    a[i]=0.0;
  }
  for(i=0;i<nknob;++i){
    a[i*nbpm+i]=1.0;
  }
  return a;
}
