#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <gsl/gsl_linalg.h>

#define PI 3.141592653589793

#define max(a,b) (((a)<(b))?(b):(a))

#define rndm() rndm5()

#include "rndm.d"
#include "amoeba2.c"


mkquad(double rk,double rl,double r[])
{
    double sign1, sign2;
    int i;
    double rkabs, c1, c2, s1, s2, rksqrt;

    for (i=0;i<25;i++) {
      r[i]=0.0;
    }
    rkabs=fabs(rk);
    rksqrt=sqrt(rkabs);
    if (rk>0.0) {
	c1=cos(rksqrt*rl);
	s1=sin(rksqrt*rl);
	c2=cosh(rksqrt*rl);
	s2=sinh(rksqrt*rl);
	sign1=-1.0;
	sign2=1.0;
    } else {
	c1=cosh(rksqrt*rl);
	s1=sinh(rksqrt*rl);
	c2=cos(rksqrt*rl);
	s2=sin(rksqrt*rl);
	sign1=1.0;
	sign2=-1.0;
    }
    r[0]=c1;
    r[1]=s1/rksqrt;
    r[5]=s1*rksqrt*sign1;
    r[6]=c1;
    r[12]=c2;
    r[13]=s2/rksqrt;
    r[17]=s2*rksqrt*sign2;
    r[18]=c2;
    r[24]=1.0;
}

mksbend(double alpha, double r[])
{
    int i;
    
    for (i=0;i<25;i++) {
      r[i]=0.0;
    }
    
    r[0]=r[6]=r[12]=r[18]=r[24]=1.0;
    r[19]=alpha;
}

mkdrift(double rl,double r[])
{
  int i;
  
  for (i=0;i<25;i++) {
    r[i]=0.0;
  }
  r[0]=1.0;
  r[1]=rl;
  r[6]=1.0;
  r[12]=1.0;
  r[13]=rl;
  r[18]=1.0;
  r[24]=1.0;
}

mattransp(double r[])
{
  int i;
  for (i=1;i<5;i++){
    int j;
    for (j=0;j<i;j++){
      double tmp=r[i*5+j];
      r[i*5+j]=r[i+5*j];
      r[i+5*j]=tmp;
    }
  }
}

matcopy(double r1[],double r2[])
{
  int i;
  for (i=0;i<25;i++){
    r2[i]=r1[i];
  }
}

matmul(double r1[],double r2[],double r3[])
{
  int i,j,k;
  double rhelp[25],sum;
  
  for (k=0;k<5;k++) {
    for (j=0;j<5;j++) {
      sum=0.0;
      for (i=0;i<5;i++) {
	sum += r1[i+k*5]*r2[j+5*i];
      }
      rhelp[j+5*k]=sum;
    }
  }
  for (j=0;j<25;j++) {
    r3[j]=rhelp[j];
  }
}

print_r(double r[])
{
  int j;
  for (j=0;j<5;j++){
    int i;
    for (i=0;i<5;i++){
      printf("%g ",r[i+j*5]);
    }  
    printf("\n");
  }
  printf("\n");
}

infodo(double a1, double rk1,double rl1,double rk2,double rl2,double d,
       double *alpha1,double *alpha2,double *bdisp1,double *bdisp2,
       double *rmu1,double *rmu2, double *disp1, double *disp2)
{
    double rtot[25],drift,rd[25],rq[25],rs[25];
    double cos1, cos2;

    drift=d-0.5*(rl1+rl2);
    mkquad(rk2,rl2,rq);
    mkdrift(drift,rd);
    matmul(rq,rd,rtot);
    matmul(rd,rtot,rtot);
    mksbend(a1,rs);
    matmul(rs,rtot,rtot);
    matmul(rtot,rs,rtot);
    mkquad(rk1,0.5*rl1,rq);
    matmul(rtot,rq,rtot);
    matmul(rq,rtot,rtot);
    cos1=0.5*(rtot[0]+rtot[6]);
    cos2=0.5*(rtot[12]+rtot[18]);
    
    if (cos1>1.0) {
      printf("error in infodo: abs(cos1)>1.0\n");
      exit(-1);
    }
    
    if (cos2>1.0) {
      printf("error in infodo: abs(cos2)>1.0\n");
      exit(-1);
    }
    *rmu1 = acos(cos1);
    *rmu2 = acos(cos2);
    
    if (rtot[1]<0.0) {
	*rmu1 = 2.0*PI-(*rmu1);
    }
    
    if (rtot[13]<0.0) {
	*rmu2 = 2.0*PI-(*rmu2);
    }
    
    *alpha1 = (rtot[0]-rtot[6])*0.5/sin(*rmu1);
    *alpha2 = (rtot[12]-rtot[18])*0.5/sin(*rmu2);
    *bdisp1 = rtot[1]/sin(*rmu1);
    *bdisp2 = rtot[13]/sin(*rmu2);
    
    /*
    ** calculate the eigenvector for the eigenvalue 1
    */
    
    rtot[0]-=1.0;
    rtot[6]-=1.0;
    rtot[12]-=1.0;
    rtot[18]-=1.0;
    rtot[24]-=1.0;

    double b_data[] = { 0.0, 0.0, 0.0, 0.0, 0.0 };

    gsl_matrix_view m = gsl_matrix_view_array (rtot, 5, 5);
    gsl_vector_view b = gsl_vector_view_array (b_data, 5);

    gsl_vector *x = gsl_vector_alloc (5);

    gsl_permutation * p = gsl_permutation_alloc (5);

    int s;

    gsl_linalg_LU_decomp (&m.matrix, p, &s);
    gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x);

    gsl_permutation_free (p);

    disp1[0]= gsl_vector_get(x, 0);
    disp1[1]= gsl_vector_get(x, 1);
    disp2[0]= gsl_vector_get(x, 2);
    disp2[1]= gsl_vector_get(x, 3);

    gsl_vector_free(x);
}

struct {
  double alpha1_0,alpha2_0,beta1_0,beta2_0,mu1_0,mu2_0;
  double disp1_0[2], disp2_0[2];
  double l[5],d1,d2,k1,l1,k2,l2,k11,k12,k21,k22;
  double rbeam[25];
  double disp1[2];
  double disp2[2];
} match_data;

mkbeam(double alpha1,double beta1,double alpha2,double beta2,double r[])
{
  int i;
  for (i=0;i<25;i++){
    r[i]=0.0;
  }
  r[0]=beta1;
  r[1]=-alpha1;
  r[5]=-alpha1;
  r[6]=(1.0+alpha1*alpha1)/beta1;
  r[12]=beta2;
  r[13]=-alpha2;
  r[17]=-alpha2;
  r[18]=(1.0+alpha2*alpha2)/beta2;
  r[24]=1.0;
}

double match_section(double x[])
{
  double alpha1,alpha2,beta1,beta2,mu1,mu2,delta=0.0,tmp;
  double rtot[25],rq[25],rd[25],rbeam[25],rtrans[25], rs[25];
  double s1=1.01,s2=0.99,sl=1e4,w1=0.1,w2=0.1,beta_wgt=0.1, alpha_wgt=1.0, disp_wgt=1.0;
  int do_diff=0;
  int i;

  x++;
  /* nominal beam */

  mkquad(match_data.k1,match_data.l1,rq);
  mkdrift(match_data.d1,rd);
  matmul(rd,rq,rtot);
  mkquad(x[0],match_data.l[0],rq);
  matmul(rq,rtot,rtot);


  mksbend(x[5], rs);
  matmul(rs,rtot,rtot);


  matmul(rd,rtot,rtot);
  mkquad(x[1],match_data.l[1],rq);
  matmul(rq,rtot,rtot);
  matmul(rd,rtot,rtot);
  mkquad(x[2],match_data.l[2],rq);
  matmul(rq,rtot,rtot);
  matmul(rd,rtot,rtot);
  mkquad(x[3],0.5*match_data.l[3],rq);
  matmul(rq,rtot,rtot);
  /* match beta_x */
  matmul(rtot,match_data.rbeam,rbeam);
  matcopy(rtot,rtrans);
  mattransp(rtrans);
  matmul(rbeam,rtrans,rbeam);
  tmp=rbeam[0]-match_data.beta1_0;
  delta+=tmp*tmp*beta_wgt;
  
  double disp1[2], disp2[2];
  disp1[0]=rtot[0]*match_data.disp1[0]+rtot[1]*match_data.disp1[1]+rtot[2];
  disp1[1]=rtot[6]*match_data.disp1[0]+rtot[7]*match_data.disp1[1]+rtot[8];
  disp2[0]=rtot[21]*match_data.disp2[0]+rtot[22]*match_data.disp2[1]+rtot[23];
  disp2[1]=rtot[27]*match_data.disp2[0]+rtot[28]*match_data.disp2[1]+rtot[29];
  tmp=disp1[0]-match_data.disp1[0];
  delta+=tmp*tmp*disp_wgt;
    
//printf("1: %g\n",delta);
  matmul(rq,rtot,rtot);

  mkdrift(match_data.d2,rd);
  matmul(rd,rtot,rtot);
  mkquad(x[4],match_data.l[4],rq);
  matmul(rq,rtot,rtot);
  matmul(rd,rtot,rtot);
  mkquad(match_data.k2,match_data.l2,rq);
  matmul(rq,rtot,rtot);
  matmul(rtot,match_data.rbeam,rq);
  mattransp(rtot);
  matmul(rq,rtot,rtot);
  beta1=rtot[0];
  alpha1=-rtot[1];
  beta2=rtot[21];
  alpha2=-rtot[22];
  tmp=beta1-match_data.beta1_0;
  delta+=tmp*tmp;
//printf("2: %g\n",delta);
  tmp=beta2-match_data.beta2_0;
  delta+=tmp*tmp;
//printf("3: %g\n",delta);
  tmp=alpha1-match_data.alpha1_0;
  delta+=tmp*tmp*alpha_wgt;
//printf("4: %g\n",delta);
  tmp=alpha2-match_data.alpha2_0;
  delta+=tmp*tmp*alpha_wgt;
//printf("5: %g\n",delta);

  tmp=fabs(x[0])-match_data.k11;
  tmp=max(tmp,0.0);
  /*printf("%g ",tmp);*/
  delta+=sl*tmp*tmp;

  tmp=fabs(x[1])-match_data.k12;
  tmp=max(tmp,0.0);
  /*printf("%g ",tmp);*/
  delta+=sl*tmp*tmp;

  tmp=fabs(x[2])-match_data.k11;
  tmp=max(tmp,0.0);
  /*printf("%g ",tmp);*/
  delta+=sl*tmp*tmp;

  tmp=fabs(x[3])-match_data.k21;
  tmp=max(tmp,0.0);
  /*printf("%g\n",tmp);*/
  delta+=sl*tmp*tmp;

  tmp=fabs(x[4])-match_data.k22;
  tmp=max(tmp,0.0);
  delta+=sl*tmp*tmp;

  /* non nominal beams */
  if (do_diff){
    mkquad(match_data.k1*s1,match_data.l1,rq);
    mkdrift(match_data.d1,rd);
    matmul(rd,rq,rtot);
    mkquad(x[0]*s1,match_data.l[0],rq);
    matmul(rq,rtot,rtot);
    matmul(rd,rtot,rtot);
    mkquad(x[1]*s1,match_data.l[1],rq);
    matmul(rq,rtot,rtot);
    matmul(rd,rtot,rtot);
    mkquad(x[2]*s1,match_data.l[2],rq);
    matmul(rq,rtot,rtot);
    matmul(rd,rtot,rtot);
    mkquad(x[3]*s1,match_data.l[3],rq);
    matmul(rq,rtot,rtot);
    mkdrift(match_data.d2,rd);
    matmul(rd,rtot,rtot);
    mkquad(x[4]*s1,match_data.l[4],rq);
    matmul(rq,rtot,rtot);
    matmul(rd,rtot,rtot);
    mkquad(match_data.k2*s1,match_data.l2,rq);
    matmul(rq,rtot,rtot);
    matmul(rtot,match_data.rbeam,rq);
    mattransp(rtot);
    matmul(rq,rtot,rtot);
    beta1=rtot[0];
    alpha1=-rtot[1];
    beta2=rtot[21];
    alpha2=-rtot[22];
    tmp=beta1-match_data.beta1_0;
    delta+=tmp*tmp*w1;
    tmp=beta2-match_data.beta2_0;
    delta+=tmp*tmp*w1;
    tmp=alpha1-match_data.alpha1_0;
    delta+=tmp*tmp*w1*alpha_wgt;
    tmp=alpha2-match_data.alpha2_0;
    delta+=tmp*tmp*w1*alpha_wgt;
    
    mkquad(match_data.k1*s2,match_data.l1,rq);
    mkdrift(match_data.d1,rd);
    matmul(rd,rq,rtot);
    mkquad(x[0]*s2,match_data.l[0],rq);
    matmul(rq,rtot,rtot);
    matmul(rd,rtot,rtot);
    mkquad(x[1]*s2,match_data.l[1],rq);
    matmul(rq,rtot,rtot);
    matmul(rd,rtot,rtot);
    mkquad(x[2]*s2,match_data.l[2],rq);
    matmul(rq,rtot,rtot);
    matmul(rd,rtot,rtot);
    mkquad(x[3]*s2,match_data.l[3],rq);
    matmul(rq,rtot,rtot);
    mkdrift(match_data.d2,rd);
    matmul(rd,rtot,rtot);
    mkquad(x[4]*s2,match_data.l[4],rq);
    matmul(rq,rtot,rtot);
    matmul(rd,rtot,rtot);
    mkquad(match_data.k2*s2,match_data.l2,rq);
    matmul(rq,rtot,rtot);
    matmul(rtot,match_data.rbeam,rq);
    mattransp(rtot);
    matmul(rq,rtot,rtot);
    beta1=rtot[0];
    alpha1=-rtot[1];
    beta2=rtot[21];
    alpha2=-rtot[22];
    tmp=beta1-match_data.beta1_0;
    delta+=tmp*tmp*w2;
    tmp=beta2-match_data.beta2_0;
    delta+=tmp*tmp*w2;
    tmp=alpha1-match_data.alpha1_0;
    delta+=tmp*tmp*w2*alpha_wgt;
    tmp=alpha2-match_data.alpha2_0;
    delta+=tmp*tmp*w2*alpha_wgt;
    /*  */
  }
  if (delta<1e100) return delta;

  return 1e100;
}

/* routine to match two FODO lattice sectors
 * input are the last five quadrupoles of the first sector and the first four
 * of the second
 * l[] contains the lengths
 * k[] the strengths
 * d1 is the distance between quadrupole centres in the first
 * d2 the same in the second sector
 * k11 is the strength limit for the last quadrupole of sector one
 * k12 is the limit for the other type of quadrupole
 * k21 is the limit for the first qwuadrupole of sector two
 * k22 is the limit for the other type of quadrupole
 * all limits are given as positive numbers
 * if a limit is zero or smaller it is replaced by the maximal strength of
 * the other quadrupoles
 * kf[] returns the fitted strengths
 */

match(double l[],double k[],double d1,double d2,double k11,double k12,
      double k21,double k22,double kf[])
{
  double rd[25],rq[25],rtot[25];
  int i,j,niter,jmin=-1,iter=0;
  double **x,y[8];// <<<<< it was 6, it should be 7... (now 8 because of the sbend)
  double alpha1,alpha2,beta1,beta2,mu1,mu2,ymin,yminold,kmax;

  x=malloc(sizeof(double*)*8);
  for (i=0;i<8;i++){
    x[i]=malloc(sizeof(double)*7);
  }

  kmax=max(fabs(k[0]),fabs(k[1]));
  kmax=max(kmax,fabs(k[7]));
  kmax=max(kmax,fabs(k[8]));
  if (k11>0.0){
    match_data.k11=k11;
  }
  else{
    match_data.k11=kmax;
  }
  if (k12>0.0){
    match_data.k12=k12;
  }
  else{
    match_data.k12=kmax;
  }
  if (k21>0.0){
    match_data.k21=k21;
  }
  else{
    match_data.k21=kmax;
  }
  if (k22>0.0){
    match_data.k22=k22;
  }
  else{
    match_data.k22=kmax;
  }
  /*
match_data.k11=1e10;
match_data.k12=1e10;
match_data.k21=1e10;
match_data.k22=1e10;
  */

  double disp1[2];
  double disp2[2];

//infodo(double a1, double rk1,double rl1,double rk2,double rl2,double d,
//       double *alpha1,double *alpha2,double *beta1,double *beta2,
//       double *rmu1,double *rmu2, double *eta1, double *eta2)

  infodo(k[9], k[1],l[1],k[0],l[0],d1,&alpha1,&alpha2,&beta1,&beta2,&mu1,&mu2, disp1, disp2);
 
  infodo(k[10], k[7],l[7],k[8],l[8],d2,
  	&match_data.alpha1_0,&match_data.alpha2_0,
	&match_data.beta1_0,&match_data.beta2_0,
	&match_data.mu1_0,&match_data.mu2_0,
	match_data.disp1_0, match_data.disp2_0);

  for (i=0;i<5;i++){
    match_data.l[i]=l[i+2];
  }

  mkbeam(alpha1,beta1,alpha2,beta2,match_data.rbeam);
  
  /*print_r(match_data.rbeam);*/
  match_data.d1=d1-0.5*(l[0]+l[1]);
  match_data.d2=d2-0.5*(l[7]+l[8]);
  match_data.k1=k[1];
  match_data.k2=k[7];
  match_data.l1=0.5*l[1];
  match_data.l2=0.5*l[7];

  for (j=1;j<=7;j++){
    for (i=1;i<=5;i++){
      x[j][i]=k[i+1];
      if (i==j) {
	if(i<4){
	  x[j][i]*=0.8;
	}
	else{
	  x[j][i]*=1.05;
	}
      }
    }
    x[j][6]=k[9]; // controlla
    if (j==6) x[j][6] *= 1.05; // controlla
        
    y[j]=match_section(x[j]);
  }
  printf("%g\n",y[7]);
  amoeba2(x,y,5,1e-20,&match_section,&niter);
  ymin=1e300;
  for (i=1;i<8;i++){
    for (j=0;j<6;j++){
      printf("%g ",x[i][j+1]);
    }
    printf("%g\n",y[i]);
    if (y[i]<ymin){
      ymin=y[i];
      for (j=0;j<6;j++){
	kf[j]=x[i][j+1];
      }
    }
  }
  yminold=ymin;
  while ((yminold>1e-5)&&(iter<20)){
    for (j=1;j<=7;j++){
      for (i=1;i<=6;i++){
	x[j][i]=k[i+1];
	x[j][i]*=1.0+0.3*gasdev();
      }
      y[j]=match_section(x[j]);
    }
    amoeba2(x,y,6,1e-20,&match_section,&niter);
    
    ymin=1e300;
    for (i=1;i<7;i++){
      for (j=0;j<5;j++){
	printf("%g ",x[i][j+1]);
      }
      printf("%g\n",y[i]);
      if (y[i]<ymin){
	ymin=y[i];
      }
    }
    if (ymin<yminold){
      yminold=ymin;
      ymin=1e300;
      for (i=1;i<=7;i++){
	if (y[i]<ymin){
	  ymin=y[i];
	  for (j=0;j<6;j++){
	    kf[j]=x[i][j+1];
	  }
	}
      }
    }
    iter++;
  }
}

main()
{
  double alpha1,alpha2,beta1,beta2,mu1,mu2,eps1=18.8,eps2=0.5;
  double disp1[2], disp2[2];
  double l[9],k[11];
  double kf[5],kfm[2],scal1,scal2,delta=0.03,diff1=0.0,diff2=0.0;
  FILE *infile,*outfile,*calfile,*lattice;
  int i,n,nq[100];
  double k1[100],k2[100],d[100],l1[100],l2[100], sb[100];
  double kcal0[100][2],kcal1[100][5];
  char buffer[100],*point;

  rndmst5(12,34,56,78);
  infile=fopen("sectors.ini","r");
  point=fgets(buffer,100,infile);
  n=strtol(point,&point,10);
  printf ("reading %d sectors\n",n);
  for (i=0;i<n;i++){
    point=fgets(buffer,100,infile);
    nq[i]=strtol(point,&point,10);
    l1[i]=strtod(point,&point);
    k1[i]=strtod(point,&point);
    l2[i]=strtod(point,&point);
    k2[i]=strtod(point,&point);
    d[i]=strtod(point,&point);
    sb[i]=strtod(point,&point);
    /*
k1[i]/=l1[i];
k2[i]/=l2[i];
*/
    /*    if(i!=n-1){*/
    k1[i]*=1.0-diff1;
    k2[i]*=1.0+diff2;
    /*    }*/
    infodo(sb[i],k1[i],l1[i],k2[i],l2[i],d[i],&alpha1,&alpha2,&beta1,&beta2,
	   &mu1,&mu2,disp1, disp2);
    printf("sector %d:\n",i);
    printf("lengths %g %g\n",l1[i],l2[i]);
    printf("strengths %g %g\n",k1[i],k2[i]);
    printf("alpha_x %g alpha_y %g\n",alpha1,alpha2);
    printf("beta_x %g beta_y %g\nmu_x %g mu_y %g\n",
	   beta1,beta2,mu1*180.0/PI,mu2*180.0/PI,
	   disp1, disp2);
  }
  fclose(infile);
  outfile=fopen("match.ini","w");
  infodo(sb[0], k1[0],l1[0],k2[0],l2[0],d[0],&alpha1,&alpha2,&beta1,&beta2,
	 &mu1,&mu2, disp1, disp2);
  fprintf(outfile,"%g %g %g\n",eps1,beta1,alpha1);
  fprintf(outfile,"%g %g %g\n",eps2,beta2,alpha2);

  fprintf(outfile,"# %g %g %g %g\n",disp1[0], disp1[1], disp2[0], disp2[1]);
  fclose(outfile);

  outfile=fopen("sectors.out", "w");
  lattice=fopen("lattice.ini", "a");

  fprintf(lattice,"\n\n");
  scal1=1.0; scal2=1.0;
  for (i=0;i<n-1;i++){
    k[0]=k2[i]*scal2; l[0]=l2[i]; k[1]=k1[i]*scal1; l[1]=l1[i];
    k[8]=k2[i+1]*scal2; l[8]=l2[i+1]; k[7]=k1[i+1]*scal1; l[7]=l1[i+1];
    k[2]=k[0]; k[3]=k[1]; k[4]=k[0]; k[5]=k[7]; k[6]=k[8];
    l[2]=l[0]; l[3]=l[1]; l[4]=l[0]; l[5]=l[7]; l[6]=l[8];
    k[9]=sb[i];
    k[10]=sb[i+1];
    match(l,k,d[i],d[i+1],fabs(k[0]),fabs(k[1]),0.0,0.0,kf);
    /*    match(l,k,d[i],d[i+1],max(fabs(k[0]),fabs(k[1])),
	  max(fabs(k[0]),fabs(k[1])),0.0,0.0,kf);*/
    printf("%g %g %g %g %g %g %g\n",k[0],kf[0],kf[1],kf[2],kf[3],kf[4],k[8]);
    if (i==0) {
      fprintf(outfile,"%g %g %g %g %g %g %g\n",
	      fabs(k[1]),fabs(k[0]),fabs(k[1]),fabs(k[0]),
	      fabs(kf[0]),fabs(kf[1]),fabs(kf[2]));
    }
    else {
      fprintf(outfile,"%g %g %g %g %g %g %g\n",
	      fabs(k[1]),fabs(k[0]),fabs(kfm[0]),fabs(kfm[1]),
	      fabs(kf[0]),fabs(kf[1]),fabs(kf[2]));
    }
    kfm[0]=kf[3];
    kfm[1]=kf[4];
    fprintf(lattice,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
    kcal0[i][0]=fabs(k[1]);
    kcal0[i][1]=fabs(k[0]);
    kcal1[i][0]=fabs(kf[0]);
    kcal1[i][1]=fabs(kf[1]);
    kcal1[i][2]=fabs(kf[2]);
    kcal1[i][3]=fabs(kf[3]);
    kcal1[i][4]=fabs(kf[4]);
  }
  fprintf(outfile,"%g %g %g %g %g %g %g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),fabs(kfm[0]),fabs(kfm[1]),fabs(k2[n-1]*scal2),fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2));
  fprintf(lattice,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);
  kcal0[n-1][0]=fabs(k1[n-1]);
  kcal0[n-1][1]=fabs(k2[n-1]);
  kcal1[n-1][0]=fabs(k2[n-1]);
  kcal1[n-1][1]=fabs(k1[n-1]);
  kcal1[n-1][2]=fabs(k2[n-1]);
  kcal1[n-1][3]=0.0;
  kcal1[n-1][4]=0.0;
  for (i=n;i<13;i++){
    kcal0[i][0]=0.0;
    kcal0[i][1]=0.0;
    kcal1[i][0]=0.0;
    kcal1[i][1]=0.0;
    kcal1[i][2]=0.0;
    kcal1[i][3]=0.0;
    kcal1[i][4]=0.0;
  }
  calfile=fopen("00.dat","w");
  fprintf(calfile," &STREN  QUADS0=");
  for (i=0;i<13;i++){
    fprintf(calfile," %g,",kcal0[i][0]);
  }
  fprintf(calfile,"\n");
  for (i=0;i<13;i++){
    fprintf(calfile," %g,",kcal0[i][1]);
  }
  fprintf(calfile,"\n");
  fprintf(calfile," QUADS1=");
  for (i=0;i<11;i++){
    fprintf(calfile," %g,%g,%g,%g,%g,\n",kcal1[i][0],kcal1[i][1],kcal1[i][2],
	    kcal1[i][3],kcal1[i][4]);
  }
  fprintf(calfile," %g,%g,%g,%g,%g /\n",kcal1[11][0],kcal1[11][1],kcal1[11][2],
	  kcal1[11][3],kcal1[11][4]);
  fclose(calfile);

  scal1=1.0+delta; scal2=1.0+delta;
  for (i=0;i<n-1;i++){
    k[0]=k2[i]*scal2; l[0]=l2[i]; k[1]=k1[i]*scal1; l[1]=l1[i];
    k[8]=k2[i+1]*scal2; l[8]=l2[i+1]; k[7]=k1[i+1]*scal1; l[7]=l1[i+1];
    k[2]=k[0]; k[3]=k[1]; k[4]=k[0]; k[5]=k[7]; k[6]=k[8];
    l[2]=l[0]; l[3]=l[1]; l[4]=l[0]; l[5]=l[7]; l[6]=l[8];
    match(l,k,d[i],d[i+1],fabs(k[0]),fabs(k[1]),0.0,0.0,kf);
    printf("%g %g %g %g %g %g %g\n",k[0],kf[0],kf[1],kf[2],kf[3],kf[4],k[8]);
    fprintf(outfile,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
    fprintf(lattice,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
  }
  fprintf(outfile,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);
  fprintf(lattice,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);

  scal1=1.0-delta; scal2=1.0-delta;
  for (i=0;i<n-1;i++){
    k[0]=k2[i]*scal2; l[0]=l2[i]; k[1]=k1[i]*scal1; l[1]=l1[i];
    k[8]=k2[i+1]*scal2; l[8]=l2[i+1]; k[7]=k1[i+1]*scal1; l[7]=l1[i+1];
    k[2]=k[0]; k[3]=k[1]; k[4]=k[0]; k[5]=k[7]; k[6]=k[8];
    l[2]=l[0]; l[3]=l[1]; l[4]=l[0]; l[5]=l[7]; l[6]=l[8];
    match(l,k,d[i],d[i+1],fabs(k[0]),fabs(k[1]),0.0,0.0,kf);
    printf("%g %g %g %g %g %g %g\n",k[0],kf[0],kf[1],kf[2],kf[3],kf[4],k[8]);
    fprintf(outfile,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
    fprintf(lattice,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
  }
  fprintf(outfile,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);
  fprintf(lattice,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);

  scal1=1.0-delta; scal2=1.0+delta;
  for (i=0;i<n-1;i++){
    k[0]=k2[i]*scal2; l[0]=l2[i]; k[1]=k1[i]*scal1; l[1]=l1[i];
    k[8]=k2[i+1]*scal2; l[8]=l2[i+1]; k[7]=k1[i+1]*scal1; l[7]=l1[i+1];
    k[2]=k[0]; k[3]=k[1]; k[4]=k[0]; k[5]=k[7]; k[6]=k[8];
    l[2]=l[0]; l[3]=l[1]; l[4]=l[0]; l[5]=l[7]; l[6]=l[8];
    match(l,k,d[i],d[i+1],fabs(k[0]),fabs(k[1]),0.0,0.0,kf);
    printf("%g %g %g %g %g %g %g\n",k[0],kf[0],kf[1],kf[2],kf[3],kf[4],k[8]);
    fprintf(outfile,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
    fprintf(lattice,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
  }
  fprintf(outfile,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);
  fprintf(lattice,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);

  scal1=1.0+delta; scal2=1.0-delta;
  for (i=0;i<n-1;i++){
    k[0]=k2[i]*scal2; l[0]=l2[i]; k[1]=k1[i]*scal1; l[1]=l1[i];
    k[8]=k2[i+1]*scal2; l[8]=l2[i+1]; k[7]=k1[i+1]*scal1; l[7]=l1[i+1];
    k[2]=k[0]; k[3]=k[1]; k[4]=k[0]; k[5]=k[7]; k[6]=k[8];
    l[2]=l[0]; l[3]=l[1]; l[4]=l[0]; l[5]=l[7]; l[6]=l[8];
    match(l,k,d[i],d[i+1],fabs(k[0]),fabs(k[1]),0.0,0.0,kf);
    printf("%g %g %g %g %g %g %g\n",k[0],kf[0],kf[1],kf[2],kf[3],kf[4],k[8]);
    fprintf(outfile,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
    fprintf(lattice,"%g %g %g %g %g %g\n%g\n",
	    fabs(k[1]),fabs(k[0]),fabs(kf[0]),fabs(kf[1]),
	    fabs(kf[2]),fabs(kf[3]),fabs(kf[4]));
  }
  fprintf(outfile,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);
  fprintf(lattice,"%g %g %g %g %g %g\n%g\n\n\n\n",
	  fabs(k1[n-1]*scal1),fabs(k2[n-1]*scal2),0.0,0.0,0.0,0.0,0.0);

  fclose(outfile);
  fclose(lattice);
}
