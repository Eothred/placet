//struct RndmSelect {
  //  int i;
// float r[97];
//  int ix1,ix2,ix3;
//};
//
//float gasdev8_select();
//void rndmst8_select(int idummy);
//float rndm8_select();

static RndmSelect rndm8_store_select;

void rndmst8_select(int idummy) // contains data to produce random numbers
{
  static int m1=259200,ia1=7141,ic1=54773;
  static float rm1=1.0/259200.0;
  static int m2=134456,ia2=8121,ic2=28411;
  static float rm2=1.0/134456.0;
  //  static int m3=243000,ia3=4561,ic3=51349;
  static int m3=243000;
  static int ix1,ix2,ix3,i;
  //  static float rm3=1.0/243000.0;

  if (idummy>0) idummy=-idummy;
  ix1=(ic1-idummy) % m1;
  if (ix1<0) ix1=-ix1; // potential overflow cure
  ix1=(ia1*ix1+ic1) % m1;
  ix2=ix1 % m2;
  ix1=(ia1*ix1+ic1) % m1;
  ix3=ix1 % m3;
  for (i=0;i<97;i++)
    {
      ix1=(ia1*ix1+ic1) % m1;
      ix2=(ia2*ix2+ic2) % m2;
      rndm8_store_select.r[i]=(ix1+ix2*rm2)*rm1;
    }
  rndm8_store_select.ix1=ix1;
  rndm8_store_select.ix2=ix2;
  rndm8_store_select.ix3=ix3;
}

float rndm8_select() // returns a random number
{
  static int m1=259200,ia1=7141,ic1=54773;
  static float rm1=1.0/259200.0;
  static int m2=134456,ia2=8121,ic2=28411;
  static float rm2=1.0/134456.0;
  static int m3=243000,ia3=4561,ic3=51349;
  static int i;
  static float rm3=1.0/243000.0;
  float help;

  rndm8_store_select.ix1=(ia1*rndm8_store_select.ix1+ic1) % m1;
  rndm8_store_select.ix2=(ia2*rndm8_store_select.ix2+ic2) % m2;
  rndm8_store_select.ix3=(ia3*rndm8_store_select.ix3+ic3) % m3;
  i=(int)((float)(97*rndm8_store_select.ix3)*rm3);
  help=rndm8_store_select.r[i];
  rndm8_store_select.r[i]=
    (rndm8_store_select.ix1+rndm8_store_select.ix2*rm2)*rm1;
  return help;
}

static struct{
  int iset;
  float v1,v2;
} gasdev_data8_select;

float gasdev8_select() // used for functions below
{
  // returns a standard normal (Gaussian) random variable
  // Marsaglia polar method, based on Box-Muller transform:
  // http://en.wikipedia.org/wiki/Marsaglia_polar_method
  // it produces two independent numbers, of which one is stored for next use
  float r;
  if (gasdev_data8_select.iset==0){
    for (;;){
      gasdev_data8_select.v1=2.0*rndm8_select()-1.0;
      gasdev_data8_select.v2=2.0*rndm8_select()-1.0;
      r=gasdev_data8_select.v1*gasdev_data8_select.v1
	+gasdev_data8_select.v2*gasdev_data8_select.v2;
      if ((r<=1.0) && (r!=0)){
	break;
      }
    }
    gasdev_data8_select.iset=1;
    r=sqrt(-2.0*log((double)r)/r);
    gasdev_data8_select.v1*=r;
    gasdev_data8_select.v2*=r;
    return gasdev_data8_select.v1;
  }
  else{
    gasdev_data8_select.iset=0;
    return gasdev_data8_select.v2;
  }
}

void
one_particle_ellipse2(R_MATRIX *sxx,R_MATRIX * /*sxy*/,R_MATRIX *syy,
		     double rx,double ry,double phasex,double phasey,
		     PARTICLE *p)
{
  double s,c;
  p->wgt=1.0;
  sincos(phasex,&s,&c);
  p->x=s*sqrt(sxx->r11);
  p->xp=c*sqrt(sxx->r22-sxx->r12*sxx->r12/sxx->r11)
    +p->x*sxx->r12/sxx->r11;
  p->x*=rx; p->xp*=rx;
  sincos(phasey,&s,&c);
  p->y=s*sqrt(syy->r11);
  p->yp=c*sqrt(syy->r22-syy->r12*syy->r12/syy->r11)
    +p->y*syy->r12/syy->r11;
  p->y*=ry; p->yp*=ry;
}
