#ifndef brent_h
#define brent_h

double r_sign(double ,double);
double brent(double ,double ,double ,double (*)(double),
	     double ,double *);

#endif // brent_h
