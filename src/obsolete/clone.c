#include <stdlib.h>
#include <stdio.h>
#include <math.h>

main(int argc,char *argv[])
{
  FILE *fin1,*fin2,*fout;
  char name[100],buffer[1024],*point;
  int n,i;

  for (n=0;n<13;n++){
    //  n=strtol(argv[1],&point,10);
    sprintf(name,"lateq2.3TeV.%d",n);
    fin1=fopen(name,"r");
    sprintf(name,"lattice.ini",n);
    fin2=fopen(name,"r");
    sprintf(name,"latnew.%d",n);
    fout=fopen(name,"w");
    
    point=fgets(buffer,1024,fin1);
    fputs(point,fout);
    for (i=0;i<15;i++){
      point=fgets(buffer,1024,fin1);
    }
    point=fgets(buffer,1024,fin2);
    for (i=0;i<13;i++){
      point=fgets(buffer,1024,fin2);
      fputs(point,fout);
    }
    for (i=0;i<4;i++){
      point=fgets(buffer,1024,fin2);
    }
    for (i=0;i<4;i++){
      point=fgets(buffer,1024,fin1);
      fputs(point,fout);
    }
    fclose(fin1);
    while(point=fgets(buffer,1024,fin2)) fputs(point,fout);
    fclose(fin2);
    fclose(fout);
  }
  exit(0);
}
