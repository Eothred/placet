#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <readline/readline.h>
#include <readline/history.h>
#include <tcl.h>

#include <fnmatch.h>

#include <octave/config.h>
#include <octave/octave.h>
#include <octave/symtab.h>
#include <octave/parse.h>
#include <octave/unwind-prot.h>
#include <octave/toplev.h>
#include <octave/error.h> 
#include <octave/quit.h>
#include <octave/variables.h>
#include <octave/sighandlers.h>
#include <octave/sysdep.h>
#include <octave/defun.h>

//#include "octave_embed.h"

#include "octave.hh"

#include "beamline.h"
#include "multipole.h"
#include "dipole.h"
#include "bpm.h"

#include "placet_tk.h"
#include "andrea.h"

#include "octave_placet.h"

DEFUN(Tcl_GetVar, args, ,
"    Get the value of a variable contained in the placet tcl/tk interpreter.\n\n\
Usage:\n\n\
    value = Tcl_GetVar(varname )\n\
    value = Tcl_GetVar(varname1, varname2 )\n\n\
Input:\n\
    - varname : name of the tcl/tk variable [STRING]\n\
    - varname2 : name of the tcl/tk variable within the array 'varname1' [STRING]\n\n\
Output:\n\
    - value : may be a character string or a strings matrix. Contains\n\
              the value of the tcl/tk variable varname.\n")
{
  octave_value retval;
  if (args.length()!=1 && args.length()!=2) {
    print_usage();
    return retval;
  }

  if (!args(0).is_string()) {
    error("Tcl_GetVar: expecting 'varname' (arg 1) to be a string");
    return retval;
  }

  if (args.length()==2 && !args(0).is_string()) {
    error("Tcl_GetVar: expecting 'varname2' (arg 2) to be a string");
    return retval;
  }

  if (args.length()==1) {
    std::string var = args(0).string_value();
    if (const char *value_ptr = Tcl_GetVar(Andrea::Placet_interp, const_cast<char*>(var.c_str()), 0)) {
      retval = octave_value(std::string(value_ptr));
    } else {
      error("Tcl_GetVar: variable '%s' (arg 1) does not exist", var.c_str());
    }
  } else {
    std::string var1 = args(0).string_value();
    std::string var2 = args(1).string_value();
    if (const char *value_ptr = Tcl_GetVar2(Andrea::Placet_interp, const_cast<char*>(var1.c_str()), const_cast<char*>(var2.c_str()), 0)) {
      retval = octave_value(std::string(value_ptr));  
    } else {
      error("Tcl_GetVar: variable '%s' in array '%s' does not exist", var1.c_str(), var2.c_str());
    }
  }
  return retval;
}

DEFUN(Tcl_SetVar, args, ,
"    Set the value of a variable contained in the placet tcl/tk interpreter.\n\n\
Usage:\n\n\
    Tcl_SetVar(varname, value )\n\
    Tcl_SetVar(varname, varname2, value )\n\n\
Input:\n\
    - varname : name of the tcl/tk variable [STRING]\n\
    - varname2 : name of the tcl/tk variable within the array 'varname' [STRING]\n\
    - value : may be a character string, a scalar, a real or string matrix (m x n).\n")
{
  octave_value retval;
  if (args.length()!=2 && args.length()!=3) {
    print_usage();
    return retval;
  }

  if (!args(0).is_string()) {
    error("Tcl_SetVar: expecting 'varname' (arg 1) to be a string");
    return retval;
  }

  std::string value;
  {
    int index = args.length()==2 ? 1 : 2;
    if (args(index).is_string()) {
      value=args(index).string_value();
    } else if (args(index).is_real_scalar() || args(index).is_real_matrix()) {
      Matrix mat=args(index).matrix_value();
      std::ostringstream value_stream;
      if (mat.rows()>1) value_stream << "{ ";        
      for(int i=0; i<mat.rows(); i++) {
        if (mat.columns()>1) {
          value_stream << "{ ";        
          for(int j=0; j<mat.columns(); j++)
            value_stream << mat(i,j) << ' ';
          value_stream << "} ";
        } else {
          value_stream << mat(i,0);
        }
      }    
      if (mat.rows()>1) value_stream << '}';
      value=value_stream.str().c_str();
    } else {
      error("Tcl_SetVar: expecting 'value' (arg %d) to be a string, a scalar, a real or string matrix", index+1);
      return retval;
    }
  }

  std::string var1=args(0).string_value();
  if (args.length()==2) {
    Tcl_SetVar(Andrea::Placet_interp, const_cast<char*>(var1.c_str()), const_cast<char*>(value.c_str()), 0);
  } else {
    std::string var2=args(1).string_value();
    Tcl_SetVar2(Andrea::Placet_interp, const_cast<char*>(var1.c_str()), const_cast<char*>(var2.c_str()), const_cast<char*>(value.c_str()), 0);
  }
  return retval;
}

DEFUN(Tcl_EvalFile, args, ,
"    Read and evaluate a tcl/tk file within the placet tcl/tk interpreter.\n\n\
Usage:\n\n\
    Tcl_EvalFile(filename )\n\n\
Input:\n\
    - filename : name of the file to read and evaluate [STRING]\n")
{
  octave_value retval;
  if (args.length()!=1) {
    print_usage();
    return retval;
  }

  if (!args(0).is_string()) {
    error("Tcl_EvalFile: expecting 'filename' (arg 1) to be a string");
    return retval;
  }

  std::string file = args(0).string_value();
  switch (Tcl_EvalFile(Andrea::Placet_interp, const_cast<char*>(file.c_str()))) {
    case TCL_ERROR:     error("Tcl_EvalFile returned 'TCL_ERROR'"); break;
    case TCL_BREAK:     warning("Tcl_EvalFile returned 'TCL_BREAK'"); break;
    case TCL_CONTINUE:  warning("Tcl_EvalFile returned 'TCL_CONTINUE'"); break;
  }

  return retval;
}

DEFUN(Tcl_Eval, args, ,
"    Read and evaluate a tcl/tk string within the placet tcl/tk interpreter.\n\n\
Usage:\n\n\
    Tcl_Eval(script )\n\n\
Input:\n\
    - script : string containing the tcl/tk instructions to evaluate [STRING]\n")
{
  octave_value retval;
  if (args.length()!=1) {
    print_usage();
    return retval;
  }

  if (!args(0).is_string()) {
    error("Tcl_Eval: expecting 'script' (arg 1) to be a string");
    return retval;
  }

  std::string tcl_string = args(0).string_value();
  switch (Tcl_Eval(Andrea::Placet_interp, const_cast<char*>(tcl_string.c_str()))) {
    case TCL_ERROR:     error("Tcl_Eval returned 'TCL_ERROR'"); break;
    case TCL_BREAK:     warning("Tcl_Eval returned 'TCL_BREAK'"); break;
    case TCL_CONTINUE:  warning("Tcl_Eval returned 'TCL_CONTINUE'"); break;
  }
  
  return retval;
}
