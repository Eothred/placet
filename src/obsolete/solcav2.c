  double factor,de0,delta,length,half_length,length_i;
  double *de;
  int i;

  factor=bunch->factor;
  length=element->length;
  half_length=0.5*length;
  length_i=1.0/length;
  de=bunch->acc_field[element->field];

/*
  Move beam to the centre of the structure
*/

  for (i=0;i<bunch->slices;i++){
    de0=de[i/bunch->macroparticles]*element->v1
      +factor*bunch->field->de[i/bunch->macroparticles];
    delta=half_length*de0/bunch->particle[i].energy;
#ifdef CAVITY_2
    solcav_sigma_step_end(bunch->sigma+i,bunch->sigma_xx+i,bunch->sigma_xy+i,
			  bunch->particle[i].energy,de0,element->v2);
    solcav_sigma_step(bunch->sigma+i,bunch->sigma_xx+i,bunch->sigma_xy+i,
		      bunch->particle[i].energy,length,2.0*delta,
		      element->v2);
#endif
    solcav_particle_step_end(bunch->particle+i,de0,element->v2);
    solcav_particle_step(bunch->particle+i,half_length,delta,element->v2);
  }

/*
  Apply the wakefield kick
*/

  if(wakefield_data.transv){
    wake_kick(element,bunch,length);
  }

/*
  Move beam to the end of the structure
*/

  for (i=0;i<bunch->slices;i++){
    de0=de[i/bunch->macroparticles]*element->v1
      +factor*bunch->field->de[i/bunch->macroparticles];
    delta=half_length*de0/bunch->particle[i].energy;
    solcav_particle_step(bunch->particle+i,half_length,delta,element->v2);
    solcav_particle_step_end(bunch->particle+i,-de0,-element->v2);
#ifdef CAVITY_2
    solcav_sigma_step_end(bunch->sigma+i,bunch->sigma_xx+i,bunch->sigma_xy+i,
			  bunch->particle[i].energy,-de0,-element->v2);
#endif
  }
