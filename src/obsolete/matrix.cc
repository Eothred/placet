/** method that calculates 4x4 matrices a and b (as [16] arrays) into c */
void matrix_mult_4(double a[],double b[],double c[])
{
  int i,j,k;
  double sum,*p1,*p2;
  for(i=0;i<4;i++){
    for (j=0;j<4;j++){
      sum=0.0;
      p1=a+4*i;
      p2=b+j;
      for (k=0;k<4;k++){
	sum+= (*p1++)*(*p2);
	p2+=4;
      }
      *c++=sum;
    }
  }
}

// arguments n2 and m2 not used! JS - 30-06-2009 
void matrix_sub(double r1[],int n1,int m1,double r2[],int /*n2*/,int /*m2*/,double r3[])
{
  int i,n;
  n=n1*m1;
  for (i=0;i<n;i++){
    *r3++=*r1++ - *r2++;
  }
}

/** method that puts 1 in the diagonal places and zero's the others for the first n places of array a */
void matrix_unit(double r[],int n)
{
  int i;
  for (i=0;i<n*n;i++){
    r[i]=0.0;
  }
  for (i=0;i<n;i++){
    r[i*(n+1)]=1.0;
  }
}

/** method that puts zero's in first n places of array a */
void matrix_zero(double a[],int n)
{
  int i;
  for (i=0;i<n;i++){
    *a++=0.0;
  }
}

/** method that calculates s1*r1 + s2*r2 and stores in r3 */
void sum_M_M(R_MATRIX *r1,double s1,R_MATRIX *r2,double s2,R_MATRIX *r3)
{
  r3->r11=r1->r11*s1+r2->r11*s2;
  r3->r12=r1->r12*s1+r2->r12*s2;
  r3->r21=r1->r21*s1+r2->r21*s2;
  r3->r22=r1->r22*s1+r2->r22*s2;
}
