#include "rndm.c"

#ifndef TWOPI
#define TWOPI 6.283185307179586
#endif

class GROUND_DATA {
public:
  double *f,*k,*a;
  double *s0,*c0,*sf,*cf,*sk,*ck,*sk0,*ck0;
  double *as,*ph_s,*ks;
  double t,s;
  int n_f,n_k,n_ks;
  GROUND_DATA();
  GROUND_DATA(char*);
  GROUND_DATA(char*,int);
  double xpos(double t0,double s0);
  void xpos(double t0,double s0,double ds,double *x,int n);
  void start();
};

void
GROUND_DATA::start()
{
    int i,j,m=0;
    double phi;

    for (i=0;i<n_k*n_f;i++) {
	phi=TWOPI*rndm8();
	c0[i]=cos(phi);
	s0[i]=sin(phi);
    }
    for (j=0;j<n_f;j++){
	cf[j]=1.0;
	sf[j]=0.0;
    }
    for (i=0;i<n_k;i++){
	ck0[i]=0.0;
	sk0[i]=0.0;
	for (j=0;j<n_f;j++){
	    ck0[i]+=a[m]*c0[m];
	    sk0[i]+=a[m]*s0[m];
	    m++;
	}
	ck[i]=ck0[i];
	sk[i]=sk0[i];
	ck[i]=0.0;
	sk[i]=0.0;
    }
    for (i=0;i<n_ks;i++){
	ph_s[i]=TWOPI*rndm8();
    }
}

GROUND_DATA::GROUND_DATA()
{
    int i,j,m=0;
    n_f=1000;
    n_k=1000;
    f=new double[n_f];
    sf=new double[n_f];
    cf=new double[n_f];
    k=new double[n_k];
    sk=new double[n_k];
    ck=new double[n_k];
    sk0=new double[n_k];
    ck0=new double[n_k];
    a=new double[n_f*n_k];
    s0=new double[n_f*n_k];
    c0=new double[n_f*n_k];
    for (i=0;i<n_f;i++) {
	f[i]=TWOPI/(i+1);
	sf[i]=0.0;
	cf[i]=1.0;
    }
    for (i=0;i<n_k;i++) {
	k[i]=TWOPI/(i+1);
    }
    for (i=0;i<n_k;i++){
	for (j=0;j<n_f;j++){
	    a[m]=0.0;
	    m++;
	}
    }
    t=0.0;
    s=0.0;
    a[0]=1.0;
}

GROUND_DATA::GROUND_DATA(char *name)
{
  FILE *file;
  int i,j,m=0;
  char buffer[1000],*point;

  file=fopen(name,"r");
  point=fgets(buffer,1000,file);
  n_f=strtol(point,&point,10);
  n_k=strtol(point,&point,10);
  f=new double[n_f];
  sf=new double[n_f];
  cf=new double[n_f];
  k=new double[n_k];
  sk=new double[n_k];
  ck=new double[n_k];
  sk0=new double[n_k];
  ck0=new double[n_k];
  a=new double[n_f*n_k];
  s0=new double[n_f*n_k];
  c0=new double[n_f*n_k];
  for (i=0;i<n_f;i++) {
    point=fgets(buffer,1000,file);
    f[i]=strtod(point,&point);
  }
  for (i=0;i<n_k;i++) {
    point=fgets(buffer,1000,file);
    k[i]=strtod(point,&point);
  }
  for (i=0;i<n_f;i++) {
    sf[i]=0.0;
    cf[i]=1.0;
  }

  /*
  for (i=0;i<n_k;i++){
    for (j=0;j<n_f;j++){
      point=fgets(buffer,1000,file);
      a[m]=fabs(strtod(point,&point));
      m++;
    }
  }
  */

  for (i=0;i<n_f;i++){
    m=i;
    for (j=0;j<n_k;j++){
      point=fgets(buffer,1000,file);
      a[m]=fabs(strtod(point,&point));
      m+=n_f;
    }
  }

  t=0.0;
  s=0.0;
  fclose(file);
  n_ks=0;
}

GROUND_DATA::GROUND_DATA(char *name,int do_syst)
{
  FILE *file;
  int i,j,m=0;
  char buffer[1000],*point;
  double fmin,fmax,df,kmin,kmax,dk;

  file=fopen(name,"r");

  point=fgets(buffer,1000,file);
  n_f=strtol(point,&point,10);
  n_k=strtol(point,&point,10);
  fmin=strtod(point,&point);
  fmax=strtod(point,&point);
  df=pow(fmax/fmin,1.0/(n_f-1));
  kmin=strtod(point,&point);
  kmax=strtod(point,&point);
  dk=pow(kmax/kmin,1.0/(n_k-1));

  f=new double[n_f];
  sf=new double[n_f];
  cf=new double[n_f];
  k=new double[n_k];
  sk=new double[n_k];
  ck=new double[n_k];
  sk0=new double[n_k];
  ck0=new double[n_k];
  a=new double[n_f*n_k];
  s0=new double[n_f*n_k];
  c0=new double[n_f*n_k];
  f[0]=fmin;
  for (i=1;i<n_f;i++) {
    f[i]=f[i-1]*df;
  }
  k[0]=kmin;
  for (i=1;i<n_k;i++) {
    k[i]=k[i-1]*dk;
  }
  for (i=0;i<n_f;i++) {
    sf[i]=0.0;
    cf[i]=1.0;
  }

  m=0;
  for (i=0;i<n_f;i++){
    for (j=0;j<n_k;j++){
      point=fgets(buffer,1000,file);
      strtol(point,&point,10);
      strtol(point,&point,10);
      m=j*n_f+i;
      a[m]=fabs(strtod(point,&point));
    }
  }

  t=0.0;
  s=0.0;
  if (do_syst) {
      point=fgets(buffer,1000,file);
      n_ks=strtol(point,&point,10);
      kmin=strtod(point,&point);
      kmax=strtod(point,&point);
      dk=pow(kmax/kmin,1.0/(n_ks-1));
      ks= new double[n_ks];
      ph_s= new double[n_ks];
      as= new double[n_ks];
      ks[0]=kmin;
      for (i=1;i<n_k;i++) {
	  ks[i]=ks[i-1]*dk;
      }
      for (i=0;i<n_k;i++) {
	  point=fgets(buffer,1000,file);
	  strtol(point,&point,10);
	  as[i]=strtod(point,&point);
      }
  }
  else {
      n_ks=0;
  }
  fclose(file);
}

double
GROUND_DATA::xpos(double t_e,double s_e)
{
    int i,j,m;
    double pos=0.0,s,c;

    if (t_e!=t) {
	m=0;
	for (j=0;j<n_f;j++){
	    cf[j]=cos(t_e*f[j]);
	    sf[j]=sin(t_e*f[j]);
	}
	for (i=0;i<n_k;i++){
	    c=0.0;
	    s=0.0;
	    for (j=0;j<n_f;j++){
		c+=a[m]*(c0[m]*cf[j]-s0[m]*sf[j]);
		s+=a[m]*(c0[m]*sf[j]+s0[m]*cf[j]);
		m++;
	    }
      	    ck[i]=c-ck0[i];
       	    sk[i]=s-sk0[i];
	    //	    ck[i]=c;
	    //	    sk[i]=s;
	}
	t=t_e;
    }
    for (j=0;j<n_k;j++){
	pos+=ck[j]*cos(k[j]*s_e)-sk[j]*sin(k[j]*s_e);
    }
    for (j=0;j<n_ks;j++){
	pos+=as[j]*cos(ks[j]*s_e+ph_s[j])*t_e;
    }
    return pos;
}

void
GROUND_DATA::xpos(double t_e,double s_e,double ds,double *x,int n)
{
    int i,j,m;
    double pos,tmp,s[n_k],c[n_k];
    double sd[n_k],cd[n_k];

    if (t_e!=t) {
	m=0;
	for (j=0;j<n_f;j++){
	    cf[j]=cos(t_e*f[j]);
	    sf[j]=sin(t_e*f[j]);
	}
	for (i=0;i<n_k;i++){
	    sk[i]=0.0;
	    ck[i]=0.0;
	    for (j=0;j<n_f;j++){
		ck[i]+=a[m]*(c0[m]*cf[j]-s0[m]*sf[j]);
		sk[i]+=a[m]*(c0[m]*sf[j]+s0[m]*cf[j]);
		m++;
	    }
	    ck[i]-=ck0[i];
	    sk[i]-=sk0[i];
	}
	t=t_e;
    }
    for (j=0;j<n_k;j++){
	cd[j]=cos(k[j]*ds);
	sd[j]=sin(k[j]*ds);
	c[j]=cos(k[j]*s_e);
	s[j]=sin(k[j]*s_e);
    }
    for (i=0;i<n-1;i++) {
	pos=0.0;
	for (j=0;j<n_k;j++){
	    pos+=ck[j]*c[j]-sk[j]*s[j];
	    tmp=c[j]*cd[j]-s[j]*sd[j];
	    s[j]=s[j]*cd[j]+c[j]*sd[j];
	    c[j]=tmp;
	}
	x[i]=pos;
    }
    for (j=0;j<n_k;j++){
	pos+=ck[j]*c[j]-sk[j]*s[j];
    }
    x[n-1]=pos;
    return;
}

#define N 100000

main(int argc,char *argv[])
{
    int i,j,n=0,iter=3,first_zero=1;
    int end_zero=0,s_abs=0,last_start=1,seed=-1;
    double t,s0[N],x,x0[N],y,y0[N],d[2],dt=0.01*100.0,t0=0.0,
      x1=0.0,y1=0.0,x2=0.0,y2=0.0,dsi;
    //    GROUND_DATA gx("harm.dat"),gy("harm.dat");
    double s_start=0.0,s_sign=1.0;
    GROUND_DATA gx(argv[1],0),gy(argv[1],0);
    FILE *f;
    char *point,buffer[1024],name[128];

//    rndmst5(12,34,56,78);
    iter=strtol(argv[3],NULL,10);
    dt=strtod(argv[4],NULL);
    if (argc>5) {
      s_sign=strtod(argv[5],NULL);
    }
    if (argc>6) {
      last_start=strtol(argv[6],NULL,10);
    }
    if (argc>7) {
      seed=strtol(argv[7],NULL,10);
    }
    rndmst8(seed);
    gx.start();
    gy.start();
    f=fopen(argv[2],"r");
    if (s_abs) {
      while(point=fgets(buffer,1024,f)) {
	s0[n++]=strtod(point,NULL);
      }
    }
    else {
      s0[0]=s_start;
      n=1;
      while(point=fgets(buffer,1024,f)) {
	s0[n]=strtod(point,NULL)*s_sign+s0[n-1];
	n++;
      }
    }
    fclose(f);
    if (last_start) {
      for (i=0;i<n;i++){
	s0[i]-=s0[n-1];
      }
    }
    if (first_zero) {
      if (last_start==0) {
	x1=gx.xpos(0.0,s0[0]);
	y1=gy.xpos(0.0,s0[0]);
      }
      else {
	x1=gx.xpos(0.0,s0[n-1]);
	y1=gy.xpos(0.0,s0[n-1]);
      }
    }
    if (end_zero) {
      x2=gx.xpos(0.0,s0[n-1]);
      y2=gy.xpos(0.0,s0[n-1]);
    }
    else {
      x2=x1;
      y2=y1;
    }
    dsi=1.0/(s0[n-1]-s0[0]);
    for (j=0;j<n;j++) {
	x0[j]=gx.xpos(0.0,s0[j])-x1*(s0[n-1]-s0[j])*dsi-x2*(s0[j]-s0[0])*dsi;
	y0[j]=gy.xpos(0.0,s0[j])-y1*(s0[n-1]-s0[j])*dsi-y2*(s0[j]-s0[0])*dsi;
    }
    for (i=0;i<iter;i++){
	sprintf(name,"position.%d",i);
	f=fopen(name,"w");
	if (first_zero) {
	  if (first_zero>0) {
	    x1=gx.xpos(dt*i,s0[0]);
	    y1=gy.xpos(dt*i,s0[0]);
	  }
	  else {
	    x1=gx.xpos(dt*i,s0[n-1]);
	    y1=gy.xpos(dt*i,s0[n-1]);
	  }
	}
	if (end_zero) {
	  x2=gx.xpos(dt*i,s0[n-1]);
	  y2=gy.xpos(dt*i,s0[n-1]);
	}
	else {
	  x2=x1;
	  y2=y1;
	}	
	for (j=0;j<n;j++) {
	  x=gx.xpos(dt*i,s0[j])-x1*(s0[n-1]-s0[j])*dsi-x2*(s0[j]-s0[0])*dsi;
	  d[0]=(x-x0[j])*1e6;
	  x0[j]=x;
	  y=gy.xpos(dt*i,s0[j])-y1*(s0[n-1]-s0[j])*dsi-y2*(s0[j]-s0[0])*dsi;
	  d[1]=(y-y0[j])*1e6;
	  y0[j]=y;
	  //	    fwrite(d,sizeof(double),2,f);
	  	  fwrite(d,sizeof(double),1,f);
//	  fprintf(f,"%g %g\n",s0[j],d[0]);
	}
	fclose(f);
    }
}
