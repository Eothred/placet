#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <tcl.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

#include "static_rf_align.h"
#include "beamline.h"
#include "bin.h"
#include "bpm.h"
#include "cavity.h"
#include "girder.h"
#include "placeti3.h"
#include "placeti4.h"
#include "placeti5.h"
#include "rndm.h"


extern EMITT_DATA emitt_data;
extern CORR corr;
extern ERRORS errors;
extern RF_DATA rf_data;
extern BUMP_DATA bump_data;
extern SURVEY_ERRORS_STRUCT survey_errors;

static GIRDERS_IN_BIN girbin[MAX_BIN];
static double qcorr[5*MAX_BIN],hcorr[5*MAX_BIN],a_zero=0.0;
#ifdef TWODIM
static double qcorrx[5*MAX_BIN],hcorrx[5*MAX_BIN],ax_zero=0.0;
#endif
static int method,u_method;

// For Static Rf alignment

void beamline_bin_divide_static_rf(BEAMLINE *beamline,int nq,int interleave,
		    BIN **bin,int *bin_number,BIN **binq,int first,int last)
{
  int start,nquad,qlast,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  int nbpmq,bpmq[BIN_MAX_BPM]; 
  GIRDER *first_girder_in_bin; 
  *bin_number=0;

  start=0; 
  if (first>0) start = first;

  while (start>=0){    
    bin_define_static_rf(beamline,start,nq,bpm,&nbpm,quad,&nquad,bpmq,
                         &nbpmq,bin_number);
    if (nquad>nbpm){
      nquad=nbpm;
    }        
    if ((nquad>=nq-interleave+1)||((nquad>=interleave+1)&&(interleave==0))){
      qlast=nquad-interleave;
      start=quad[qlast-1]+1;
    }
    else{
      start=-1;
      qlast=nquad;
    }

    if ( ((last>0) && (start>=last)) || nquad<nq ) {
      start = -1;
    }
      /// For CAV (RF static)
    if(nquad>0) { 
      bin[*bin_number]=bin_make(nquad,nbpm);
      bin_set_elements(bin[*bin_number],quad,nquad,bpm,nbpm,qlast);

      // For quadrupole (few-to-few)
      binq[*bin_number]=bin_make(nquad,nbpmq);
      bin_set_elements(binq[*bin_number],quad,nquad,bpmq,nbpmq,qlast);
      (*bin_number)++;
    }
  }
}


//
/*
When we define the 2 bins (For static CAV alignment and for quadrupoles), 
we save information about BPM element number (in bpmq), 
cavity element number (in bpm) and first girder adress (girbin). 
*/

void bin_define_static_rf(BEAMLINE *beamline,int start,
                int nquad,int bpm[],int *nbpm,int quad[],
                int *nq,int bpmq[],int *nbpmq,int *bin_number)
{
  int j,no_more_CAV;
  int i=0;
  GIRDER *girder=(GIRDER *) (beamline->first);
  ELEMENT **element,*g_element;

  *nq=0;
  *nbpm=0;
  *nbpmq=0;
  element=beamline->element;
  while (element[start]!=NULL){
    if ((element[start]->is_quad())||(element[start]->is_quadbpm())) break;
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  quad[0]=start++;
  (*nq)++;
  while((element[start]!=NULL)&&(nquad)){
    if ((element[start]->is_quad())||(element[start]->is_quadbpm())){
      nquad--;
      if (nquad) quad[(*nq)++]=start;
    }
    if ((element[start]->is_bpm())||(element[start]->is_quadbpm())){
      bpmq[(*nbpmq)++]=start;
     }
    start++;
  }

  /// Save the pointer to girder into vquad and memorize the CAV 
  /// element number in bpm array

  j = 0;
  no_more_CAV = 1;
  while (j<(*nq)) {
    g_element=girder->element; 
    while (g_element!=NULL && quad[j] != i ){ 
      if((j>0) && g_element->is_cavity() && (i<quad[j])) {
         bpm[(*nbpm)++] = i;
      }
      g_element=g_element->next;
      i++;  
    }
  
    if (i==quad[j]) {
      if(j==0) girbin[*bin_number].first_girder = girder;   
      j++;
      while (g_element!=NULL) {
        if(g_element->is_cavity()) {
          bpm[(*nbpm)++] = i;
        }
        g_element=g_element->next;
        i++; 
      } 
    }
    girder=girder->next;

    /* 
        Finish the bin by including all the CAV positionned after the 
        last quadrupole of this bin and before 
        the first quadrupole of the next bin.
    */

    if(j==(*nq)) {
      while(girder!=NULL && no_more_CAV) {
        g_element=girder->element;
        if(g_element->is_quad() || g_element->is_quadbpm())no_more_CAV =0;
        while (g_element!=NULL && no_more_CAV){
          if(g_element->is_cavity()) {
             bpm[(*nbpm)++] = i;
          }
          if(g_element->is_quad() || g_element->is_quadbpm())no_more_CAV =0;
          g_element=g_element->next;
          i++;  
        }
        girder=girder->next;
      }
    }
  }
}
/////////////////////////////
void bin_correct_static_rf(BEAMLINE *beamline,BIN *bin,int j)
{
  double *a,bpm0[BIN_MAX_BPM];
  double z_pos,start_z_girder,end_z_girder,last_dist;
#ifdef TWODIM
  double *a_x,bpm0_x[BIN_MAX_BPM];
#endif

  double z0=0.0,z1;

  int *bpm,nq,nb;
#ifdef TWODIM
  int *bpm_x,nq_x,nb_x;
#endif
  int i,iq,ib,il=0,gfound,previous_girder;
  ELEMENT *gelement,*gscan_elem;
  GIRDER *girder;
  GIRDER *scan_girder;
  int elemn;

  // Initialization
  //if (j==0) { 
    z_pos=0.0;
    start_z_girder=0.0;
    end_z_girder=0.0;
    last_dist=0.0;
    scan_girder=beamline->first;
    elemn = -1;
    //}
    //pu_method=method;
    if(u_method==4 || u_method==3) il=1;
  //  

  nb=bin->nbpm;
  nq=bin->nq;
  bpm=bin->bpm;
 
    /// Scan girders until we find the first girder of the bin
    /// Compute z position...and count the elem. number

  girder = girbin[j].first_girder;
  //pif(u_method==2)previous_girder=0;

  while(scan_girder!=girder){
    start_z_girder = end_z_girder + last_dist; 
    gscan_elem = scan_girder->element;

    if(u_method==2)previous_girder = 0;    
    while (gscan_elem!=NULL){
      elemn++;
      z_pos += gscan_elem->get_length(); 
      //pif(u_method==2 && gscan_elem->is_cavity()){
      //pprevious_girder = 1;
      //p}  
      gscan_elem = gscan_elem->next;  
    }

    end_z_girder =  start_z_girder + scan_girder->get_length(); 
      
    z0 = end_z_girder + 0.5 * scan_girder->dist;
    z1 = z0 + 0.5 * scan_girder->dist; 
    last_dist = scan_girder->dist;
    scan_girder = scan_girder->next;
  } 

  /* In method 2, if there is CAV in the previous girder of the first 
     girder of the bin, don't move the first articulation ! */ 

  //pif(previous_girder==0){
  //pil=1;
  //pu_method=4;
  //p}
  //vprintf("In correct: u_method %d %d %d\n",u_method,il,previous_girder);
  //vprintf("Qcorr %g %g \n",qcorr[il-1],qcorr[il]);
  /*
    Now scan all the girders of the bin and fill the matrix
  */ 
    
  iq=0;
  ib=0;


  if(scan_girder==girder){

    while (ib<nb && scan_girder!=NULL) {

      start_z_girder = end_z_girder + last_dist;   
      z1 = start_z_girder + scan_girder->get_length() + 0.5 * scan_girder->dist;
      gscan_elem = scan_girder->element;

      gfound = 0 ;
      z_pos = start_z_girder;
    /// Scan the elements of the bins

      while (gscan_elem!=NULL){

        elemn++;
        z_pos += 0.5*gscan_elem->get_length(); 
        if(elemn==bpm[ib] && ib<nb) {
          gfound = 1;

          if(il>0) { 
            if(u_method==0) {
              gscan_elem->offset.y += qcorr[il]+
                         (hcorr[il]-qcorr[il])*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.yp += (hcorr[il]-qcorr[il])/(z1-z0);
            } else if(u_method==1) {
              gscan_elem->offset.y += qcorr[il]*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.yp += qcorr[il]/(z1-z0);
            } else if(u_method>1) {

              //vprintf("Ok for correction %g %g \n",qcorr[il-1],qcorr[il]);

              gscan_elem->offset.y += qcorr[il-1]+
                             (qcorr[il]-qcorr[il-1])*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.yp += (qcorr[il]-qcorr[il-1])/(z1-z0);
            }
	  } else {
            if(u_method==0) {
              gscan_elem->offset.y += qcorr[il]+
                      (hcorr[il]-qcorr[il])*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.yp += (hcorr[il]-qcorr[il])/(z1-z0);
            } else if(u_method==1) {
              gscan_elem->offset.y += (qcorr[il]-a_zero)*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.yp += qcorr[il]/(z1-z0);
            } else if(u_method>1) {
              gscan_elem->offset.y += a_zero+(qcorr[il]-a_zero)*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.yp += (qcorr[il]-a_zero)/(z1-z0);
            }
          }
#ifdef TWODIM
          if(il>0) { 
            if(u_method==0) {
              gscan_elem->offset.x += qcorrx[il]+
                         (hcorrx[il]-qcorrx[il])*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.xp += (hcorrx[il]-qcorrx[il])/(z1-z0);
            } else if(u_method==1) {
              gscan_elem->offset.x += qcorrx[il]*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.xp += qcorrx[il]/(z1-z0);
            } else if(u_method>1) {
              gscan_elem->offset.x += qcorrx[il-1]+
                             (qcorrx[il]-qcorrx[il-1])*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.xp += (qcorrx[il]-qcorrx[il-1])/(z1-z0);
            }
	  } else {
            if(u_method==0) {
              gscan_elem->offset.x += qcorrx[il]+
                      (hcorrx[il]-qcorrx[il])*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.xp += (hcorrx[il]-qcorrx[il])/(z1-z0);
            } else if(u_method==1) {
              gscan_elem->offset.x += (qcorrx[il]-ax_zero)*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.xp += qcorrx[il]/(z1-z0);
            } else if(u_method>1) {
              gscan_elem->offset.x += ax_zero+(qcorrx[il]-ax_zero)*
                                 (z_pos-z0)/(z1-z0);
              gscan_elem->offset.xp += (qcorrx[il]-ax_zero)/(z1-z0);
            }
          }
#endif
          ib++;    
        }
        z_pos += 0.5*gscan_elem->get_length(); 
        gscan_elem = gscan_elem->next;
      }

      end_z_girder =  start_z_girder + scan_girder->get_length(); 
      z0 = end_z_girder + 0.5 * scan_girder->dist;  
      z1 = z0 + 0.5 * scan_girder->dist;
      last_dist = scan_girder->dist;  
      scan_girder = scan_girder->next; 
 
      if(u_method==4 || u_method==3 ){
        if(gfound!=0 || il!=1) il++;
      } else {
        if(gfound!=0 || il!=0) il++;
      }     
    }

    /*
    BINS ARE CONNECTED HERE ! Next elements of last girder will then move !!!
     */
    a_zero = 0.0;
#ifdef TWODIM
    ax_zero = 0.0;
#endif

    //vprintf("Do the connection to the next girder\n");

    if(method==2) {

      if(ib>=nb && scan_girder!=NULL) {
        start_z_girder = end_z_girder + last_dist;   
        z1 = start_z_girder + scan_girder->get_length() + 0.5 * scan_girder->dist;
        gscan_elem = scan_girder->element;

        z_pos = start_z_girder;
    /// Scan the elements of the girder wich immediately follows the 
    /// last girder of the bin. 
        while (gscan_elem!=NULL){
          elemn++;
          z_pos += 0.5*gscan_elem->get_length(); 

          if(gscan_elem->is_cavity()) {

	    //vprintf("Next CAV correction %g\n",qcorr[il-1]);

              gscan_elem->offset.y += qcorr[il-1]
                      -qcorr[il-1]*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.yp -= qcorr[il-1]/(z1-z0);      
#ifdef TWODIM
              gscan_elem->offset.x += qcorrx[il-1]
                      -qcorrx[il-1]*(z_pos-z0)/(z1-z0);
              gscan_elem->offset.xp -= qcorrx[il-1]/(z1-z0);  
#endif
          }
          z_pos += 0.5*gscan_elem->get_length(); 
          gscan_elem = gscan_elem->next;
        }
      }
    }

  }
}


///////////////////////
/*
Fill the matrix M to do static RF minimization : Offsets_in_RF = M * A
where A is the vector containing the position of the middle connection (*) 
between 2 girders : girder --- * --- girder --- * --- girder

Method =0 :  Move both extremities of each girder independently in order 
            to minimize the sum of the square CAV offsets. Bins 
            are not connected.

Method =1 : Move (the end extremity of) each girder independently in order 
            to minimize the sum of the square CAV offsets. Bins 
            are not connected.

Method =2 : Girder are connected each other. Minimize the RMS of the 
            offsets : S = Sum_over_girders [(beam - CAV_position)**2]. 
            Bin are connected. First girder of the bin is fixed : 
            need overlap option in order to aligne the full linac.

Method =3 : Minimize the average. Bins are disconnected.

Method =4 : Minimize the RMS. First girder may move. Bins are disconnected.

 */
/// For Static Rf structures

void bin_response_static_rf(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,
                            int flag,BIN *bin,int j)
{
  double *a,bpm0[BIN_MAX_BPM];
  double z_pos,start_z_girder,end_z_girder,last_dist;
#ifdef TWODIM
  double *a_x,bpm0_x[BIN_MAX_BPM];
#endif

  int indx[BIN_MAX_QUAD];
  double bmat[BIN_MAX_QUAD],amat[BIN_MAX_QUAD2],z0=0.0,z1;
  double bb[BIN_MAX_QUAD],aa[BIN_MAX_QUAD][BIN_MAX_QUAD],m,d;
  double invaa[2][2],det;
  double yi[4],zi[4],mi[4];

#ifdef TWODIM
  double bmatx[BIN_MAX_QUAD];
  double bbx[BIN_MAX_QUAD];
  double xi[4];
#endif
  int num_cav[BIN_MAX_QUAD];


  int *quad,*bpm,nq,nb;
#ifdef TWODIM
  int *quad_x,*bpm_x,nq_x,nb_x;
#endif
  int i,iq,ib,il=0,gfound,test,ip,previous_girder;
  ELEMENT **element,*gelement,*gscan_elem;
  GIRDER *girder;
  GIRDER *scan_girder;
  int elemn;
  
  // initialization 
  //if (j==0) {
    z_pos=0.0;
    start_z_girder=0.0;
    end_z_girder=0.0;
    last_dist=0.0;
    scan_girder=beamline->first;
    elemn = -1;
    //}
    u_method=method;

    if(u_method==4 || u_method==3) il=1;
  //
  switch (flag){
  case 0:
    a=bin->a0;
#ifdef TWODIM
    a_x=bin->a0_x;
#endif
    break;
  case 1:
    a=bin->a1;
#ifdef TWODIM
    a_x=bin->a1_x;
#endif
    break;
  case 2:
    a=bin->a2;
#ifdef TWODIM
    a_x=bin->a2_x;
#endif
    break;
  }

  nb=bin->nbpm;
  nq=bin->nq;
  bpm=bin->bpm;
  quad=bin->quad;
#ifdef TWODIM
  nb_x=bin->nbpm_x;
  nq_x=bin->nq_x;
  bpm_x=bin->bpm_x;
  quad_x=bin->quad_x;
#endif
  element=beamline->element;
  bunch_copy_0(bunch0,bunch);
  bunch_track_0(beamline,bunch,bin->start,bin->stop);

  if(bin->a){
    for (ib=0;ib<nb;ib++){
      bpm0[ib]=element[bpm[ib]]->get_bpm_y_reading();
    }
  }
#ifdef TWODIM
  if (bin->a_x){
    for (ib=0;ib<nb_x;ib++){
      bpm0_x[ib]=element[bpm_x[ib]]->get_bpm_x_reading();
    }
  }
#endif

  if(nb==0)return;

    //INITIALISATIONS
    for(iq=0;iq<BIN_MAX_QUAD;iq++){
      bb[iq]=0.0;
#ifdef TWODUM
      bbx[iq]=0.0;
#endif       
      num_cav[iq] = 0;
      for(i=0;i<BIN_MAX_QUAD;i++){
        aa[iq][i]=0.0;
      }
    }
    
    /// Check if there is any CAV on the first girder of the bin
    if(u_method==2)previous_girder=0;

    girder = girbin[j].first_girder;
    gscan_elem = girder->element;
    while (gscan_elem!=NULL){
      if(gscan_elem!=NULL && u_method==2 && gscan_elem->is_cavity()){
        previous_girder=1; 
      } 
      gscan_elem = gscan_elem->next;
    }
    /* In method 2, if there is CAV in the first girder of  
       the bin, don't move the first articulation (Method 2).
       Else move it (Back to Method 4 !!!). */ 
    if(previous_girder==0 && u_method==2){
      il=1;
      u_method=4;
    }

    if(u_method==2)previous_girder=0;

    /* Scan girders from the beginning of the beamline 
       until we find the first girder of the bin
       Compute z position...and count the elem. number
    */

    while(scan_girder!=girder){
      start_z_girder = end_z_girder + last_dist; 
      gscan_elem = scan_girder->element;

      if(u_method==2)previous_girder = 0;    
      while (gscan_elem!=NULL){
        elemn++;
        z_pos += gscan_elem->get_length(); 
        if(gscan_elem!=NULL && u_method==2 && gscan_elem->is_cavity()){
          previous_girder = 1;
        } 
        gscan_elem = gscan_elem->next;
      }
      end_z_girder =  start_z_girder + scan_girder->get_length(); 
      
      z0 = end_z_girder + 0.5 * scan_girder->dist;
      last_dist = scan_girder->dist;
      scan_girder = scan_girder->next;
    } 

    /* In method 2, if there is CAV in the previous girder of the first 
       girder of the bin, don't move the first articulation (Method 2).
       Else move it (Back to Method 4 !!!). */ 
    if(previous_girder==0 && u_method==2){
      il=1;
      u_method=4;
    }

    //
    //Now scan all the girders of the bin and fill the matrix
    //printf("TEST El %d Quad %d BPM %d POSITION %g\n",(elemn),bin->quad[0],
    //                       bin->bpm[0],z_pos);
    // 
    
    iq=0;
    ib=0;

    test=0;

    //vprintf("A u_method %d %d %d \n",u_method,il,previous_girder);

    if(scan_girder==girder){

      while (ib<nb && scan_girder!=NULL) {    
   
        start_z_girder = end_z_girder + last_dist;   
        z1 = start_z_girder + scan_girder->get_length() + 0.5 * scan_girder->dist; 
        gscan_elem = scan_girder->element;

	/// Some initialisations
        bmat[il] = 0.0;
        amat[2*il]=0.0;
#ifdef TWODIM
        bmatx[il] = 0.0;
#endif
	if((il-1)>=0) amat[(2*il)-1]=0.0;
        if((u_method==4 && il==1)) {
          bmat[il-1] = 0.0;
          amat[2*(il-1)]=0.0;
#ifdef TWODIM
          bmatx[il-1] = 0.0;
#endif
        }
       
        if(u_method==3){   
          for(iq=0;iq<4;iq++){
            yi[iq] = 0.0;
            zi[iq] = 0.0;
            mi[iq] = 0.0;
#ifdef TWODIM
            xi[iq] = 0.0;
#endif
          }
        }

      /// Scan the elements of the bins
        z_pos = start_z_girder;
        gfound = 0 ;
        while (gscan_elem!=NULL){

          elemn++;
          z_pos += 0.5*gscan_elem->get_length(); 

          /// Have found a CAV
          if(gscan_elem->is_cavity() && (elemn!=bpm[ib])) {
            printf("YOU MISS A CAV ! %d %d \n",ib,bpm[ib]);
            exit(0);
          }

          if(elemn==bpm[ib] && ib<nb) {
            if(gfound==0) ip=0;
            gfound = 1;

            if(u_method==1) {
	      /*
              End extremity of each girder moves (First extremity is fixed)
	      */
              amat[2*il]+=(z_pos-z0)/(z1-z0);
              bmat[il]+=gscan_elem->get_bpm_y_reading(); 
              if(ib==0) bmat[il]+= a_zero * (z_pos-z1)/(z1-z0);
#ifdef TWODIM
              bmatx[il]+=gscan_elem->get_bpm_x_reading(); 
              if(ib==0) bmatx[il]+= ax_zero * (z_pos-z1)/(z1-z0);
#endif            
            } else if(u_method==0) {
              /*
              RMS of each girde is minimised by moving each extremity
              Girders are disconnected !!!
	      */
              num_cav[il] +=1;
              m = (z_pos-z0)/(z1-z0);
              bb[2*il]+=(1.0-m)*gscan_elem->get_bpm_y_reading();
              bb[2*il+1]+=m*gscan_elem->get_bpm_y_reading();
              aa[2*il][2*il]+=(1.0-m)*(1.0-m);
              aa[2*il][2*il+1]+=m*(1.0-m);
              aa[2*il+1][2*il]+=m*(1.0-m);
              aa[2*il+1][2*il+1]+=m*m;
#ifdef TWODIM
              bbx[2*il]+=(1.0-m)*gscan_elem->get_bpm_x_reading();
              bbx[2*il+1]+=m*gscan_elem->get_bpm_x_reading();
#endif  
            } else if(u_method==2 || u_method==4) {

              /* 
              RMS is minimised in a bin by moving all intersections (method 4)
              except the first one of the bin (for method 2).
	      */
              m = (z_pos-z0)/(z1-z0);
              if(il==0) {
                bb[il]+=(gscan_elem->get_bpm_y_reading()-a_zero)*m;
                bb[il]-=a_zero*m*m;
                aa[il][il]+=m*m;
#ifdef TWODIM
                bbx[il]+=(gscan_elem->get_bpm_x_reading()-ax_zero)*m;
                bbx[il]-=ax_zero*m*m;
#endif
	      } else if(il>0) {
                bb[il]+=gscan_elem->get_bpm_y_reading()*m;
                aa[il][il]+=m*m;
                aa[il][il-1]+=m*(1.0-m);
                bb[il-1]+=gscan_elem->get_bpm_y_reading()*(1.0-m); 
                aa[il-1][il-1]+=(1.0-m)*(1.0-m);
                aa[il-1][il]+=m*(1.0-m);
#ifdef TWODIM
                bbx[il]+=gscan_elem->get_bpm_x_reading()*m;
                bbx[il-1]+=gscan_elem->get_bpm_x_reading()*(1.0-m); 
#endif
              }
             } else if(u_method==3) {
              /* 
              average is minimised in a bin by moving all intersections
              except the first one of the bin
	      */                
                yi[ip] = gscan_elem->get_bpm_y_reading();
                zi[ip] = z_pos;
                mi[ip] = (z_pos-z0)/(z1-z0); 
#ifdef TWODIM
                xi[ip] = gscan_elem->get_bpm_x_reading();
#endif 
                ip++;
            }
            ib++; 
	  }
          z_pos += 0.5*gscan_elem->get_length(); 
          gscan_elem = gscan_elem->next;
	}

	if(u_method==3 ) {
          for(iq=0;iq<ip;iq++){ 
            for(i=0;i<ip;i++){ 
              if(il==0) {
                bb[il]+=yi[iq]*mi[i];
                bb[il]+=yi[i]*mi[iq];
                bb[il]-=a_zero*(zi[iq]+zi[i]-2.0*z0)/(z1-z0);
                bb[il]+=2.0*a_zero*(zi[iq]-z0)/(z1-z0)*(zi[i]-z0)/(z1-z0);
                aa[il][il]+=2.0*mi[iq]*mi[i];
#ifdef TWODIM
                bbx[il]+=xi[iq]*mi[i];
                bbx[il]+=xi[i]*mi[iq];
                bbx[il]-=ax_zero*(zi[iq]+zi[i]-2.0*z0)/(z1-z0);
                bbx[il]+=2.0*ax_zero*(zi[iq]-z0)/(z1-z0)*(zi[i]-z0)/(z1-z0);
#endif
	      } else if(il>0) {
                bb[il]+=yi[iq]*mi[i];
                bb[il]+=yi[i]*mi[iq];
                aa[il][il]+=2.0*mi[iq]*mi[i]; 
                aa[il][il-1]+=mi[iq]+mi[i];
                aa[il][il-1]-=2.0*mi[iq]*mi[i];
                bb[il-1]+=yi[iq]+yi[i];
                bb[il-1]-=yi[iq]*mi[i];
                bb[il-1]-=yi[i]*mi[iq];
                aa[il-1][il-1]+=2.0*(1-mi[iq])*(1-mi[i]);
                aa[il-1][il]+=mi[iq]+mi[i];
                aa[il-1][il]-=2.0*(mi[iq]*mi[i]);
#ifdef TWODIM
                bbx[il]+=xi[iq]*mi[i];
                bbx[il]+=xi[i]*mi[iq];
                bbx[il-1]+=xi[iq]+xi[i];
                bbx[il-1]-=xi[iq]*mi[i];
                bbx[il-1]-=xi[i]*mi[iq];
#endif
              }              
            }
          } 
        }  

        end_z_girder =  start_z_girder + scan_girder->get_length(); 
        z0 = z1;
        last_dist = scan_girder->dist;  
        scan_girder = scan_girder->next; 

        if(u_method==4){
          if(gfound!=0 || il!=1) il++;
        } else {
          if(gfound!=0 || il!=0) il++;
        }

      }     

      /*  
         Define the corrections in qcorr which contain the extremities 
         of the girders to move. Notice : first extremity of first girder 
         in the bin is fixed and its value is a_zero=0.0 by default.
      */

      for(iq=0;iq<il;iq++){
        qcorr[iq] = 0.0;
#ifdef TWODIM
        qcorrx[iq] = 0.0;
#endif
      }
      if(u_method==0){
        for(iq=0;iq<il;iq++){
          hcorr[iq] = 0.0;
#ifdef TWODIM
          hcorrx[iq] = 0.0;
#endif
	}
      }

      if(u_method==1) {      
        for(iq=0;iq<il;iq++){
          if(iq==0) { 
            if(amat[iq]!=0.0){ 
              qcorr[iq] = bmat[iq]/amat[iq];
#ifdef TWODIM
              qcorrx[iq] = bmatx[iq]/amat[iq];
#endif
	    }
          } else {
            if(amat[2*iq]!=0.0){
              qcorr[iq] = (bmat[iq] - amat[(2*iq)-1]*
                                   qcorr[iq-1])/amat[2*iq];
#ifdef TWODIM
              qcorrx[iq] = (bmatx[iq] - amat[(2*iq)-1]*
                                   qcorrx[iq-1])/amat[2*iq];
#endif
	    }
          }
        } 
      } else if(u_method>=2) {

        for(iq=0;iq<il;iq++){
          bmat[iq]=bb[iq];
#ifdef TWODIM
          bmatx[iq]=bbx[iq];
#endif
          //vif(iq<3)printf("bb[il] is %g %d\n",bb[iq],iq);
          for(i=0;i<il;i++){
            amat[iq+il*i]=aa[iq][i];
          }
        }
 
        ludcmp(amat,il,indx,&d);
        lubksb(amat,il,indx,bmat);          

#ifdef TWODIM  
        lubksb(amat,il,indx,bmatx);   
#endif

        for(iq=0;iq<il;iq++){
          qcorr[iq] = bmat[iq];
          //vif(iq<10)printf("Coor of Y %d %d %g\n",iq,il,qcorr[iq]);
#ifdef TWODIM
          qcorrx[iq] = bmatx[iq];
#endif
        }
      } else if(u_method==0) {

        for(iq=0;iq<il;iq++){
          det = (aa[2*iq][2*iq]*aa[2*iq+1][2*iq+1]) - 
	    (aa[2*iq+1][2*iq]*aa[2*iq][2*iq+1]);
          invaa[2*iq][2*iq] = (1.0/det)*aa[2*iq+1][2*iq+1];
          invaa[2*iq+1][2*iq+1] = (1.0/det)*aa[2*iq][2*iq];
          invaa[2*iq+1][2*iq] = (-1.0/det)*aa[2*iq][2*iq+1];
          invaa[2*iq][2*iq+1] = (-1.0/det)*aa[2*iq+1][2*iq];        
          qcorr[iq]=invaa[2*iq][2*iq]*bb[2*iq]+invaa[2*iq][2*iq+1]*bb[2*iq+1];
          hcorr[iq]=invaa[2*iq+1][2*iq]*bb[2*iq]+
                    invaa[2*iq+1][2*iq+1]*bb[2*iq+1];
#ifdef TWODIM
          qcorrx[iq]=invaa[2*iq][2*iq]*bbx[2*iq]+
                     invaa[2*iq][2*iq+1]*bbx[2*iq+1];
          hcorrx[iq]=invaa[2*iq+1][2*iq]*bbx[2*iq]+
                    invaa[2*iq+1][2*iq+1]*bbx[2*iq+1];
#endif
          if(num_cav[iq]==1) {
            qcorr[iq] = bb[2*iq+1]/sqrt(aa[2*iq+1][2*iq+1]);
            hcorr[iq] = qcorr[iq]; 
#ifdef TWODIM
            qcorrx[iq] = bbx[2*iq+1]/sqrt(aa[2*iq+1][2*iq+1]);
            hcorrx[iq] = qcorrx[iq]; 
#endif
          } 
        }
      }
    }

}

/*
 Move the extremities of the girders : CAV elements will move...
 */

void inter_girder_move_x(BEAMLINE *beamline,double amp_y,double amp_x, 
			 double flo_y,double flo_x,int /*all*/)
{

  int il=1,i,elemn=0;
  double z1,z0=0.0,z_pos;
  double start_z_girder,end_z_girder=0.0,last_dist=0.0;
  double gint[5*MAX_BIN];
  double h1,h2;
#ifdef TWODIM
  double gintx[5*MAX_BIN],h1x,h2x;
#endif
  GIRDER *scan_girder=(GIRDER *) (beamline->first);
  ELEMENT *gscan_elem;
  
  //Define the Intersection girder offsets
  
  gint[0] = 0.0;
  for(i=0;i<(5*MAX_BIN);i++) {
    //gint[i] = 2.0*amp_y*rndm()-amp_y;
    gint[i] = amp_y*gasdev(); 
    
    //vif(i<10)printf("Y art int %d %g\n",i,gint[i]);
#ifdef TWODIM
    //gintx[i] = 2.0*amp_x*rndm()-amp_x;
    gintx[i] = amp_x*gasdev();
#endif
  }
  
  // scan the girders
  while(scan_girder!=NULL){
    start_z_girder = end_z_girder + last_dist; 
    
    z1 = start_z_girder + scan_girder->get_length() + 0.5 * scan_girder->distance_to_prev_girder();
    gscan_elem = scan_girder->element();
    z_pos = start_z_girder;
    /// Scan the elements 
    
    /*
      Allow a mismatch in the intersection of the girders : h1, h2, ...
    */
    h1=flo_y*gasdev();
    h2=flo_y*gasdev();  
#ifdef TWODIM
    h1x=flo_x*gasdev();
    h2x=flo_x*gasdev();  
#endif

    while (gscan_elem!=NULL){

      z_pos += 0.5*gscan_elem->get_length(); 
      if(gscan_elem->is_cavity()) {
        gscan_elem->offset.y += (gint[il-1]+h1)+
                             (gint[il]-gint[il-1]+h1-h2)*(z_pos-z0)/(z1-z0);
        gscan_elem->offset.yp += (gint[il]-gint[il-1]+h1-h2)/(z1-z0);
#ifdef TWODIM      
        gscan_elem->offset.x += (gintx[il-1]+h1x)+
                       (gintx[il]-gintx[il-1]+h1x-h2x)*(z_pos-z0)/(z1-z0);
        gscan_elem->offset.xp += (gintx[il]-gintx[il-1]+h1x-h2x)/(z1-z0);
#endif
      }
      z_pos += 0.5*gscan_elem->get_length();
      elemn++; 
      gscan_elem = gscan_elem->next;     
    }

    il++;

    end_z_girder =  start_z_girder + scan_girder->get_length();
    z0 = end_z_girder + 0.5 * scan_girder->distance_to_prev_girder();
    z1 = z0 + 0.5 * scan_girder->distance_to_prev_girder();
    last_dist = scan_girder->distance_to_prev_girder();
    scan_girder = scan_girder->next(); 
  }
  //
  for(i=il;i<(5*MAX_BIN);i++) {
    gint[i] = 0.0;
#ifdef TWODIM
    gintx[i] = 0.0;
#endif
  }
}

//////////

void
static_rf_correct_jitter(BEAMLINE *beamline,BIN **bin,BIN **binq,
     int nbin,BEAM *bunch0,BEAM *bunch1,BEAM *workbunch,int do_emitt)
{
  int ipos,i,j;
  ipos=0;


  if (do_emitt){
    bunch_track_emitt_start();  
  }

  for (i=0;i<nbin;i++){

    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track(beamline,bunch0,ipos,bin[i]->start);
    }

    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);

    // Girder position correction
    bunch_join_0(bunch0,bunch1,gasdev(),workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_response_static_rf(beamline,bunch0,workbunch,0,bin[i],i);
    bin_correct_static_rf(beamline,bin[i],i);

    // Quad correction 
    bunch_join_0(bunch0,bunch1,gasdev(),workbunch);
    bin_measure(beamline,workbunch,0,binq[i]);
    bin_correct(beamline,0,binq[i]);

    ipos=bin[i]->start;
  }

  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0);
  }
  else{
    bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  }
  

}

//

void
static_rf_correct_jitter_0(BEAMLINE *beamline,BIN **bin,BIN **binq,int nbin,
                BEAM *bunch0,BEAM *bunch1,BEAM *workbunch)
{
  int ipos,i,j;
  ipos=0;


  for (i=0;i<nbin;i++){
    bunch_track_0(beamline,bunch0,ipos,bin[i]->start);
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);

    // Girder position correction
    bunch_join_0(bunch0,bunch1,gasdev(),workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_response_static_rf(beamline,bunch0,workbunch,0,bin[i],i);
    bin_correct_static_rf(beamline,bin[i],i);

    // Quad correction 
    bunch_join_0(bunch0,bunch1,gasdev(),workbunch);
    bin_measure(beamline,workbunch,0,binq[i]);
    bin_correct(beamline,0,binq[i]);
   
    ipos=bin[i]->start;
  } 

  bunch_track_0(beamline,bunch0,ipos,beamline->n_elements);
}

//

void
static_rf_correct_emitt(BEAMLINE *beamline,BIN **bin,BIN **binq,int nbin,
                             BEAM *bunch,BEAM *workbunch)
{
  int ipos,i,j;
  ipos=0;

  bunch_track_emitt_start();

  for (i=0;i<nbin;i++){
    bunch_track_emitt(beamline,bunch,ipos,bin[i]->start);

    // Girder position correction
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_response_static_rf(beamline,bunch,workbunch,0,bin[i],i);
    bin_correct_static_rf(beamline,bin[i],i);
    // Quad correction   
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,binq[i]);
    bin_correct(beamline,0,binq[i]);
 
    ipos=bin[i]->start;
  }

  bunch_track_emitt(beamline,bunch,ipos,beamline->n_elements);
  bunch_track_emitt_end(bunch);

}
/////////////
void static_rf_correct_0(BEAMLINE *beamline,BIN **bin,BIN **binq,int nbin,
                   BEAM *bunch,BEAM *workbunch)
{
  int ipos,i,j;
  ipos=0;

  for (i=0;i<nbin;i++){

    //i=nbin;    

    bunch_track_0(beamline,bunch,ipos,bin[i]->start);

    // Girder position correction
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_response_static_rf(beamline,bunch,workbunch,0,bin[i],i);
    bin_correct_static_rf(beamline,bin[i],i);

    // Quad correction   
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,binq[i]);
    bin_correct(beamline,0,binq[i]);

    ipos=bin[i]->start;
  }

  bunch_track_0(beamline,bunch,ipos,beamline->n_elements);
}

///

double test_static_rf_correction(BEAMLINE *beamline,BIN **bin,BIN **binq,
            int nbin,BEAM *bunch0,BEAM *probe,int niter,
	    void (*survey)(BEAMLINE*),char *name,char *name2,int st_method)
{
  BEAM *tb,*tb1,*workbunch,*p_b=NULL;
  int i,j,do_emitt,do_jitter=0,k;
  int iem=0; 
  double esum=0.0,emitt0,esum2=0.0,qp,qps,qpsum=0.0;
  double offset_y,offset_y2,offset_x,offset_x2;
  FILE *file,*file2;

  do_jitter=errors.do_jitter;  
  printf("do_jitter=%d\n",do_jitter);

  method=st_method;

  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
#ifdef MULTI
    emitt_store_init_2(beamline->n_quad,MULTI);
#else
    emitt_store_init(beamline->n_quad);
#endif
  }
  tb=bunch_remake(bunch0);
  if (do_jitter){
    tb1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  if (probe!=NULL){
    p_b=bunch_remake(probe);
  }
  bunch_copy_0(bunch0,tb);

  simple_bin_fill(beamline,binq,nbin,tb,workbunch);

  if (do_emitt==0) {
    beam_copy(bunch0,tb);
    bunch_track(beamline,tb,0,beamline->n_elements);
  }

  if(name2!=NULL){
    file2=fopen(name2,"w");
  }

  for (i=0;i<niter;i++){
    survey(beamline);

    if ((do_emitt)&&(probe==NULL)){
      beam_copy(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb->slices;j++){
	  tb1->particle[j].y+=errors.jitter_y;
#ifdef TWODIM
	  tb1->particle[j].x+=errors.jitter_x;
#endif
	}
	static_rf_correct_jitter(beamline,bin,binq,nbin,tb,tb1,workbunch,1);
      }
      else{
	static_rf_correct_emitt(beamline,bin,binq,nbin,tb,workbunch);
      }
    }
    else{
      bunch_copy_0(bunch0,tb);
      if (do_jitter){ 
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb->slices;j++){
	  tb1->particle[j].y+=errors.jitter_y;
#ifdef TWODIM
	  tb1->particle[j].y+=errors.jitter_x;
#endif
	}
	static_rf_correct_jitter_0(beamline,bin,binq,nbin,tb,tb1,workbunch);
      }
      else{
	static_rf_correct_0(beamline,bin,binq,nbin,tb,workbunch);
      }
    }

    emitt0=emitt_y(tb);

    if (probe!=NULL){
      beam_copy(probe,p_b);
      if (do_emitt){
  	bunch_track_emitt_start();
        bunch_track_emitt(beamline,p_b,0,beamline->n_elements);
        bunch_track_emitt_end(p_b);
      }
      else{
        bunch_track_0(beamline,p_b,0,beamline->n_elements);
      }
      emitt0=emitt_y(p_b);
    }

    iem++;
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    printf("%d %g %g %g\n",i,emitt0,esum/(double)(i+1),
	   sqrt(max(0.0,esum2/(double)(i+1)-esum*esum/(double)((i+1)*(i+1)))));

    if(name2!=NULL){
      fprintf(file2,"%d %g %g %g\n",i,emitt0,esum/(double)(i+1),
       sqrt(max(0.0,esum2/(double)(i+1)-esum*esum/(double)((i+1)*(i+1)))));
    }

  }
  esum/=(double)iem;
  esum2/=(double)iem;
  printf("emitt= %g +/- %g\n",esum,
	 sqrt(max(0.0,esum2-esum*esum)/(double)iem));

  if(name2!=NULL){
    fclose(file2);
  }
  beam_delete(tb);
  beam_delete(workbunch);
  if (do_jitter) {
      beam_delete(tb1);
  }
  if (probe) {
      beam_delete(p_b);
  }
  return 0.0;
}
///


/*
 Move the extremities of the girders : CAV elements will move...
 */

void inter_girder_move_x(BEAMLINE *beamline,double amp_y,double amp_x, 
			 double flo_y,double flo_x,int all)
{

  int gfound,il=1,i,elemn=0;
  double z1,z0=0.0,z_pos;
  double start_z_girder,end_z_girder=0.0,last_dist=0.0;
  double gint[5*MAX_BIN];
  double h1,h2;
#ifdef TWODIM
  double gintx[5*MAX_BIN],h1x,h2x;
#endif
  GIRDER *scan_girder=(GIRDER *) (beamline->first);
  ELEMENT *gscan_elem;

  //Define the Intersection girder offsets

  gint[0] = 0.0;
  for(i=0;i<(5*MAX_BIN);i++) {
    //gint[i] = 2.0*amp_y*rndm()-amp_y;
    gint[i] = amp_y*gasdev(); 

    //vif(i<10)printf("Y art int %d %g\n",i,gint[i]);
#ifdef TWODIM
    //gintx[i] = 2.0*amp_x*rndm()-amp_x;
    gintx[i] = amp_x*gasdev();
#endif
  }

  // scan the girders
  while(scan_girder!=NULL){
    start_z_girder = end_z_girder + last_dist; 
 
    z1 = start_z_girder + scan_girder->get_length() + 0.5 * scan_girder->dist;
    gscan_elem = scan_girder->element;
    z_pos = start_z_girder;
    /// Scan the elements 
    gfound=0;

    /*
    Allow a mismatch in the intersection of the girders : h1, h2, ...
     */
    h1=flo_y*gasdev();
    h2=flo_y*gasdev();  
#ifdef TWODIM
    h1x=flo_x*gasdev();
    h2x=flo_x*gasdev();  
#endif

    while (gscan_elem!=NULL){

      z_pos += 0.5*gscan_elem->get_length(); 
      if(gscan_elem->is_cavity()) {
        gfound=1;      
        gscan_elem->offset.y += (gint[il-1]+h1)+
                             (gint[il]-gint[il-1]+h1-h2)*(z_pos-z0)/(z1-z0);
        gscan_elem->offset.yp += (gint[il]-gint[il-1]+h1-h2)/(z1-z0);
#ifdef TWODIM      
        gscan_elem->offset.x += (gintx[il-1]+h1x)+
                       (gintx[il]-gintx[il-1]+h1x-h2x)*(z_pos-z0)/(z1-z0);
        gscan_elem->offset.xp += (gintx[il]-gintx[il-1]+h1x-h2x)/(z1-z0);
#endif
      }
      z_pos += 0.5*gscan_elem->get_length();
      elemn++; 
      gscan_elem = gscan_elem->next;     
    }

    il++;

    end_z_girder =  start_z_girder + scan_girder->get_length(); 
    z0 = end_z_girder + 0.5 * scan_girder->dist;  
    z1 = z0 + 0.5 * scan_girder->dist;
    last_dist = scan_girder->dist;  
    scan_girder = scan_girder->next; 
  }
  //
  for(i=il;i<(5*MAX_BIN);i++) {
    gint[i] = 0.0;
#ifdef TWODIM
    gintx[i] = 0.0;
#endif
  }
}

void inter_girder_move(BEAMLINE *beamline,double ampl_y,double ampl_x, 
			 double flo_y,double flo_x,int cav_only)
{
    GIRDER* girder=inter_data.beamline->first;
    double x2=ampl_x*gasdev();
    double y2=ampl_y*gasdev();
    double x1,y1;

    if (cav_only) {
	while (girder) {
	    x1=x2;
	    y1=y2;
	    x2=ampl_x*gasdev();
	    y2=ampl_y*gasdev();
	    girder_move_ends_cav(girder,x1+flo_x*gasdev(),x2+flo_x*gasdev(),
				 y1+flo_y*gasdev(),y2+flo_y*gasdev());
	    girder=girder->next;
	}
    }
    else {
	while (girder) {
	    x1=x2;
	    y1=y2;
	    x2=ampl_x*gasdev();
	    y2=ampl_y*gasdev();
	    girder_move_ends(girder,x1+flo_x*gasdev(),x2+flo_x*gasdev(),
			     y1+flo_y*gasdev(),y2+flo_y*gasdev());
	    girder=girder->next;
	}
    }
}

void
scatter_girder(BEAMLINE *beamline,double ampl_y,double ampl_yp,
	       double ampl_x,double ampl_xp,int cav_only)
{
  GIRDER* girder=inter_data.beamline->first;
  double x,y,xp,yp;
  
  if (cav_only) {
    while (girder) {
      x=ampl_x*gasdev();
      y=ampl_y*gasdev();
      xp=ampl_xp*gasdev();
      yp=ampl_yp*gasdev();
      girder_move_cav(girder,y,0.0);
      girder_move_cav_x(girder,x,0.0);
      girder=girder->next;
    }
  }
  else {
    while (girder) {
      x=ampl_x*gasdev();
      y=ampl_y*gasdev();
      xp=ampl_xp*gasdev();
      yp=ampl_yp*gasdev();
      girder_move(girder,y,yp);
      girder_move_x(girder,x,xp);
      girder=girder->next;
    }
  }
}
