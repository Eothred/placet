#include <cstring>

#include <getopt.h>
#include <libgen.h>

#include <mpi.h>

#include "mpi_buffered_stream_bcast.hh"
#include "mpi_buffered_stream.hh"
#include "file_buffered_stream.hh"
#include "socket.hh"

#include "quadrupole.hh"
#include "multipole.hh"
#include "sbend.hh"
#include "drift.hh"

#include "beamline.hh"
#include "beam.hh"

#include "stopwatch.hh"

struct args_struct {
  char 		*file_name;
  char 		*fifo_name;
  unsigned int 	 port_number; 
  bool help;
};

static void read_args(int argc, char *const argv[], args_struct &args );
static void MPI_Reduce_bpm_readings(Beamline &beamline, const ParticleSet &beam, int root, MPI_Comm comm=MPI_COMM_WORLD )
{
  std::list<Bpm*> bpms_list = beamline.get_list_of<Bpm>();
  if (bpms_list.size() > 0) {
    double bpms[3*bpms_list.size()];
    size_t i = 0;
    for (std::list<Bpm*>::const_iterator itr = bpms_list.begin(); itr != bpms_list.end(); ++itr) {
      bpms[3*i+0] = (*itr)->get_x_sum();
      bpms[3*i+1] = (*itr)->get_y_sum();
      bpms[3*i+2] = (*itr)->get_sum();
      i++;
    }
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == root) {
      double bpms_sum[3*bpms_list.size()];
      MPI_Reduce(bpms, bpms_sum, 3*bpms_list.size(), MPI_DOUBLE, MPI_SUM, root, comm);
      size_t i=0;
      for (std::list<Bpm*>::iterator itr = bpms_list.begin(); itr != bpms_list.end(); ++itr) {
	(*itr)->set_x_sum(bpms_sum[3*i+0]);
	(*itr)->set_y_sum(bpms_sum[3*i+1]);
	(*itr)->set_sum  (bpms_sum[3*i+2]);
	i++;
      }
    } else {
      MPI_Reduce(bpms, NULL, 3*bpms_list.size(), MPI_DOUBLE, MPI_SUM, root, comm);
    }
  }
}

int main(int argc, char **argv )
{
  if (MPI_Init(&argc, &argv) == MPI_SUCCESS) {
    // "global" variables
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if (size==1) {
      fprintf(stderr, "error:\t`%s\' requires at least 2 processes\n", basename(argv[0]));
      MPI_Abort(MPI_COMM_WORLD, -1);
      exit(1);
    }
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    Beamline	beamline;
    ParticleSet	beam;
    Stopwatch	timer;
    int		loop;
    // "root" variables
    args_struct	args;
    std::string 	beam_name;
    size_t		beam_size;
    double		tracking_time;
    Socket		listener;
    // parse the arguments from shell
    if (rank == 0) {
      args.file_name = NULL;
      args.fifo_name = NULL;
      args.port_number = 0;
      args.help = false;
      read_args(argc, argv, args);
      if (args.file_name)	loop = 0;
      else			loop = 1;
      MPI_Bcast(&loop, 1, MPI_INT, 0, MPI_COMM_WORLD);
      if (args.port_number) {
	if (listener.bind(args.port_number)) {
	  listener.listen();
	} else {
	  fprintf(stderr, "error:\tcannot listen on port %d\n", args.port_number);
	  MPI_Abort(MPI_COMM_WORLD, -1);
	  exit(1);
	}
      }	
    } else {
      MPI_Bcast(&loop, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }		
    // initialize the random number generator with a different seed for each process
    for (int i=0; i < rank; i++) rand();
    gsl_rng_set(Element::rng, rand());
    do {
      if (rank == 0) {
	File_Buffered_IStream 	file;
	Socket 			client;
	IStream *istream_ptr;
	if 	(args.file_name)	{ istream_ptr = &file; file.open(args.file_name); }
	else if (args.fifo_name)	{ istream_ptr = &file; file.open(args.fifo_name); }
	else if (args.port_number)	{ istream_ptr = &client; client = listener.accept(); }
	else if (!args.help) {
	  fprintf(stderr, "error:\tyou should specify one of --file, --fifo, --port options\n");
	  fprintf(stderr, "try:\t%s --help\n", basename(argv[0]));
	  MPI_Abort(MPI_COMM_WORLD, -1);
	  exit(1);
	}
	IStream &istream = *istream_ptr;
	if (istream) {
	  if (loop) system("clear");
	  std::cerr << "\nloading the beamline...\t";
	  try {
	    istream >> beamline;
	  } catch (Error &msg ) {
	    std::cerr << "error:\t" << msg << std::endl;
	    MPI_Abort(MPI_COMM_WORLD, -1);
	    exit(-1);
	  }
	  std::cerr << "this beamline is composed by:\n\n  "
		    << beamline.get_list_of<Quadrupole>().size() << "\tquadrupoles\n  "
		    << beamline.get_list_of<Multipole>().size() << "\tmultipoles\n  "
		    << beamline.get_list_of<SBend>().size() << "\tsbends\n  "
		    << beamline.get_list_of<Drift>().size() << "\tdrifts\n\n";
	  MPI_Buffered_OStream_Bcast broadcast;	
	  timer.start();
	  broadcast << beamline;
	  broadcast.flush();
	  timer.stop();
	  std::cerr << "broadcasting the beamline:\t\t" << timer.cpu_seconds()+timer.system_seconds() << " cpu+system seconds" << std::endl;
	  istream	>> beam_name
			>> beam_size;
	  beam.resize(beam_size / size);
	  for (size_t i=0; i < beam.size(); i++)
	    istream >> beam[i];
	  MPI_Buffered_OStream ostream;
	  timer.start();
	  for (size_t i=1; i<size; i++) {
	    ostream.set_dest(i,0);
	    size_t beam_base = i * beam_size / size;
	    size_t beam_end = (i+1) * beam_size / size;
	    size_t beam_isize = beam_end - beam_base;
	    ostream << beam_isize;
	    for (size_t j=0; j < beam_isize; j++) {
	      Particle particle;
	      istream >> particle;
	      ostream << particle;
	    }
	    ostream.flush();
	  }
	  timer.stop();
	  std::cerr << "reading and scattering the beam:\t" << timer.cpu_seconds()+timer.system_seconds() << " cpu+system seconds" << std::endl;
#ifdef DEBUG
	  std::cerr << "process 0 is processing " << beam.size() << " particles\n";	
#endif
	} else {
	  fprintf(stderr, "error:\tcannot open input data stream\n");
	  MPI_Abort(MPI_COMM_WORLD, -1);
	  exit(1);
	}
      } else  {
	MPI_Buffered_IStream_Bcast bcast(0);
	bcast >> beamline;
	MPI_Buffered_IStream stream(0,0);
	stream >> beam_size;
	beam.resize(beam_size);
	for (size_t i=0; i<beam.size(); i++)
	  stream >> beam[i];
#ifdef DEBUG
	std::cerr << "process " << rank << ": received beamline and " << beam.size() << " particles from process 0\n";
#endif
      }
      // Actually, this is the main loop
#ifdef DEBUG
      std::cerr << "process " << rank << " starts tracking\n";
#endif
      timer.start();
      for (Beamline::element_itr itr=beamline.begin(); itr!=beamline.end(); ++itr) {
	const Element *element_ptr = *itr;
	element_ptr->transport_synrad(beam);
	//				element_ptr->transport(beam);
      }
      timer.stop();
      double local_tracking_time = timer.real_seconds();
      MPI_Reduce(&local_tracking_time, &tracking_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
#ifdef DEBUG
      std::cerr << "process " << rank << " ended the tracking (" << local_tracking_time << " real seconds)\n";
#endif
      MPI_Reduce_bpm_readings(beamline, beam, 0);
      if (rank == 0) {
	std::cerr << "\ntracking time:\t\t\t\t" 
		  << beam_size << " particles in " 
		  << tracking_time << " real seconds (" << beam_size/tracking_time << " particles/sec)\n"
		  << "\ngathering and outputting the results..." << std::flush;
	File_Buffered_OStream 	file;
	Socket 			client;
	OStream *ostream_ptr;
	if 	(args.fifo_name)	{ ostream_ptr = &file; file.open(args.fifo_name); }
	else if (args.port_number)	{ ostream_ptr = &client; client = listener.accept(); }
	if (args.fifo_name || args.port_number) {
	  OStream &ostream = *ostream_ptr;
	  ostream	<< beam_name
			<< beam_size;
	  for (size_t i=0; i<beam.size(); i++) { 
	    ostream << beam[i];
	  }
	  for(size_t i=1; i<size; i++) {
	    MPI_Buffered_IStream istream(i,0);
	    size_t beam_size;
	    istream >> beam_size;
	    for (size_t j=0; j<beam_size; j++) {
	      Particle particle;
	      istream >> particle;
	      ostream << particle;
	    }
	  }
	  std::list<Bpm*> bpms_list = beamline.get_list_of<Bpm>();
	  double bpms[2*bpms_list.size()];
	  size_t i = 0;
	  for (std::list<Bpm*>::const_iterator itr = bpms_list.begin(); itr != bpms_list.end(); ++itr) {
	    bpms[2*i+0] = (*itr)->get_x_sum() / (*itr)->get_sum();
	    bpms[2*i+1] = (*itr)->get_y_sum() / (*itr)->get_sum();
	    i++;
	  }
	  ostream << Stream::array(bpms, 2*bpms_list.size());
	  ostream.close();
	} else {
	  for (size_t i=0; i<beam.size(); i++)  { 
	    fprintf(stdout,"%.15g %g %g %g %g %g\n", beam[i].energy,
		    beam[i].x,
		    beam[i].y,
		    beam[i].z,
		    beam[i].xp,
		    beam[i].yp);
	  }
	  for(size_t i=1; i<size; i++) {
	    MPI_Buffered_IStream stream(i,0);
	    size_t beam_size;
	    stream >> beam_size;
	    for (size_t j=0; j<beam_size; j++) {
	      Particle particle;
	      stream >> particle;
	      fprintf(stdout,"%.15g %g %g %g %g %g\n", particle.energy,
		      particle.x,
		      particle.y,
		      particle.z,
		      particle.xp,
		      particle.yp);
	    }
	  }
	}
	std::cerr << " done!" << std::endl << std::endl;
      } else {
	MPI_Buffered_OStream ostream(0,0);
	ostream << beam.size();
	for(size_t i=0; i<beam.size(); i++) {
	  ostream << beam[i];
	}
      }
    } while(loop);
    MPI_Finalize();
  } else fputs("error:\tcould not initialize the MPI execution environment\n", stderr);
  return 0;
}

static void read_args(int argc, char *const argv[], args_struct &args )
{
  struct option options[] = {
    {"help", 0, 0, 0},
    {"version", 0, 0, 0},
    {"file", 1, 0, 0},
    {"fifo", 1, 0, 0},
    {"port", 1, 0, 0},
    {0, 0, 0, 0}
  };
  while (true)
    {
      int option_idx = 0, c = getopt_long_only(argc, argv, "", options, &option_idx);
      if (c==-1)	break;
      if (c==0) {
	switch (option_idx) {
	case 0:	fprintf(stdout, "\nSix Dimensional Tracking Routine (" __DATE__ ")\n\n"
			"USAGE:\t%s [OPTIONS]\n\n"
			"OPTIONS:\n"
			"  --help                    Display this help\n"
			"  --version                 Display version information\n"
			"  --fifo=filename           Use fifo `filename\' for input/output\n"
			"  --file=filename           Use file `filename\' for input/stdout for output\n"
			"  --port=number             Listen on port `number\' for input/output\n\n", argv[0]);
	  args.help = true;
	  break;
	case 1:	fprintf(stdout, "\n%s\n(" __DATE__ ")\n\n", argv[0]); args.help = true; break;
	case 2:	args.file_name = optarg; break;
	case 3: args.fifo_name = optarg; break;
	case 4: args.port_number = atoi(optarg); break;
	default: fprintf(stderr, "unrecognized option `%s\'\n", options[option_idx].name); break;
	}
      }
    }
}
