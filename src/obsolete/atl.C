#include "rndm.c"

#ifndef TWOPI
#define TWOPI 6.283185307179586
#endif

#define N 100000

main(int argc,char *argv[])
{
    int i,j,n=0,iter=3,first_zero=1;
    int end_zero=0,s_abs=0,last_start=0,seed=1;
    double t,s0[N],x,x0[N],y,y0[N],d[2],dt=0.01*100.0,t0=0.0,
      x1=0.0,y1=0.0,x2=0.0,y2=0.0,dsi;
    double s_start=0.0,s_sign=1.0;
    FILE *f;
    char *point,buffer[1024],name[128];

    iter=strtol(argv[3],NULL,10);
    dt=strtod(argv[4],NULL);
    if (argc>5) {
      s_start=strtod(argv[5],NULL);
    }
    if (argc>6) {
      s_sign=strtod(argv[6],NULL);
    }
    if (argc>7) {
      last_start=strtol(argv[7],NULL,10);
    }
    if (argc>8) {
      seed=strtol(argv[8],NULL,10);
    }
    rndmst8(seed);
    gx.start();
    gy.start();
    f=fopen(argv[2],"r");
    if (s_abs) {
      while(point=fgets(buffer,1024,f)) {
	s0[n++]=strtod(point,NULL);
      }
    }
    else {
	if (last_start) {
	    s0[0]=0.0;
	    n=1;
	    while(point=fgets(buffer,1024,f)) {
		s0[n]=strtod(point,NULL)*s_sign+s0[n-1];
		n++;
	    }
	    for (i=0;i<n;i++){
		s0[i]-=s0[n-1]-s_start;
	    }
	}
	else {
	    s0[0]=s_start;
	    n=1;
	    while(point=fgets(buffer,1024,f)) {
		s0[n]=strtod(point,NULL)*s_sign+s0[n-1];
		n++;
	    }
	}
    }
    fclose(f);
    x1=gx.xpos(0.0,0.0);
    y1=gy.xpos(0.0,0.0);
    for (j=0;j<n;j++) {
	x0[j]=gx.xpos(0.0,s0[j])-x1;
	y0[j]=gy.xpos(0.0,s0[j])-y1;
    }
    for (i=0;i<iter;i++){
	sprintf(name,"position.%d",i);
	f=fopen(name,"w");
	x1=gx.xpos(dt*i,0.0);
	y1=gy.xpos(dt*i,0.0);
	for (j=0;j<n;j++) {
	  x=gx.xpos(dt*i,s0[j])-x1;
	  d[0]=(x-x0[j])*1e6;
	  x0[j]=x;
	  y=gy.xpos(dt*i,s0[j])-y1;
	  d[1]=(y-y0[j])*1e6;
	  y0[j]=y;
	  //	    fwrite(d,sizeof(double),2,f);
	  	  fwrite(d,sizeof(double),1,f);
//	  fprintf(f,"%g %g %g\n",s0[j],d[0],d[1]);
	}
	fclose(f);
    }
}
