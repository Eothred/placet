/*
  The routine steps through a cavity using first order transport
*/

#define CAVITY_2

void CAVITY::step_many(BEAM *beam )
{
  double half_length=0.5*geometry.length;
  double length_i=1.0/geometry.length;
/*changed to allow different structures */
  double *s_beam=beam->s_long[field];
  double *c_beam=beam->c_long[field];
  double s_long, c_long;
  sincos(phase, &s_long, &c_long);

/*
  Move beam to the centre of the structure
*/
#ifdef HTGEN
  int imacro3=0; // haloparticles
#endif
  for (int i=0,i_s=0;i_s<beam->slices_per_bunch;i_s++) {
    double de0=(c_beam[i_s]*c_long+s_beam[i_s]*s_long)*gradient+beam->factor*beam->field[0].de[i_s];
    // beam particles
    for (int i_m=0;i_m<beam->particle_number[i_s];i_m++) {
      PARTICLE &particle=beam->particle[i];
#ifdef RFCAV
      wgt+=particle.wgt;
      y1+=particle.y*particle.wgt;
      x1+=particle.x*particle.wgt;
#endif
      double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
      double gammap=delta*length_i;
      particle.yp-=gammap*particle.y;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
//    gammap=0.5*de0/(particle.energy*geometry.length);
      double gammap=delta*length_i;
#ifdef END_FIELDS
      particle.yp-=gammap*particle.y;
#endif
      /* scd 1.0-delta ? */
      tmp=(1.0-gammap*length);
#endif
    
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
      particle.yp*=tmp;

#ifdef END_FIELDS
      particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
      particle.xp*=tmp;
      particle.energy+=half_length*de0;
	  
#ifdef CAVITY_2
      sigma_step_x(beam->sigma+i,beam->sigma_xx+i, beam->sigma_xy+i,2.0*delta);
#endif
      i++;
    }
#ifdef HTGEN
    // haloparticles
    for (int i_h=0;i_h<beam->particle_number_sec[i_s];i_h++) {
      PARTICLE &particle=beam->particle_sec[imacro3];
      double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
      double gammap=delta*length_i;
      particle.yp-=gammap*particle.y;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      double gammap=delta*length_i;
#ifdef END_FIELDS
      particle.yp-=gammap*particle.y;
#endif
      tmp=(1.0-gammap*length);
#endif
    
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
      particle.yp*=tmp;

#ifdef END_FIELDS
      particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
      particle.xp*=tmp;
      particle.energy+=half_length*de0;
	  
      imacro3++;
    }
#endif
  }
 /*
  Apply the wakefield kick
 */
  if(wakefield_data.transv) {
    wake_kick_many(beam);
    //      dipole_kick(element,bunch,length);
    if (modes.size()>0) {
      for (int i=0;i<modes[0];i++) {
	quadrupole_kick(beam,geometry.length,modes[i+1]);
	//	quadrupole_kick_const(bunch,geometry.length,element->modes[i+1]);
      }
    }
  }
/*
  Move beam to the end of the structure
*/
#ifdef HTGEN
  imacro3=0;
#endif 
  for (int i=0, i_s=0;i_s<beam->slices_per_bunch;i_s++) {
    // beam particles
    for (int i_m=0;i_m<beam->particle_number[i_s];i_m++) {
      PARTICLE &particle=beam->particle[i];
      double de0=(c_beam[i_s]*c_long
		  +s_beam[i_s]*s_long)*gradient
	+beam->factor*beam->field[0].de[i_s];
#ifdef CAV_PRECISE
      double delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
      double gammap=delta/geometry.length;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
//    gammap=0.5*de0/(particle.energy*geometry.length);
      gammap=half_length*de0/(particle.energy*length);
      tmp=(1.0-gammap*length);
#endif
	  
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
      particle.yp+=gammap*particle.y;
#endif
      particle.yp*=tmp;
	  
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
      particle.xp+=gammap*particle.x;
#endif
      particle.xp*=tmp;
	  
      particle.energy+=half_length*de0;
	  
#ifdef EARTH_FIELD
      particle.xp+=earth_field.x*geometry.length/particle.energy;
      particle.yp+=earth_field.y*geometry.length/particle.energy;
#endif
	  
#ifdef RFCAV
      y2+=particle.y*particle.wgt;
      x2+=particle.x*particle.wgt;
#endif
      i++;
    }
#ifdef HTGEN
    // haloparticles
    for (int i_h=0;i_h<beam->particle_number_sec[i_s];i_h++) {
      PARTICLE &particle=beam->particle_sec[imacro3];
      double de0=(c_beam[i_s]*c_long+s_beam[i_s]*s_long)*gradient+beam->factor*beam->field[0].de[i_s];
#ifdef CAV_PRECISE
      double delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
      double gammap=delta/geometry.length;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      gammap=half_length*de0/(particle.energy*length);
      tmp=(1.0-gammap*length);
#endif
	  
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
      particle.yp+=gammap*particle.y;
#endif
      particle.yp*=tmp;
	  
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
      particle.xp+=gammap*particle.x;
#endif
      particle.xp*=tmp;
      
      particle.energy+=half_length*de0;
	  
#ifdef EARTH_FIELD
      particle.xp+=earth_field.x*geometry.length/particle.energy;
      particle.yp+=earth_field.y*geometry.length/particle.energy;
#endif
      imacro3++; 
    }
#endif
  }
#ifdef RFCAV
  y1/=wgt;
  y2/=wgt;
  x1/=wgt;
  x2/=wgt;
#endif
}

void CAVITY::step_many_0(BEAM *beam )
{

/*
  Move beam to the centre of the structure
*/
  double *s_beam=beam->s_long[field];
  double *c_beam=beam->c_long[field];
  double s_long, c_long;
  sincos(phase, &s_long, &c_long);
  double length_i=1.0/geometry.length;
  double half_length=0.5*geometry.length;
#ifdef HTGEN
  int imacro3=0; //haloparticles
#endif
  for (int i=0,i_s=0;i_s<beam->slices_per_bunch;i_s++) {
    // beam particles
    for (int i_m=0;i_m<beam->particle_number[i_s];i_m++) {
      PARTICLE &particle=beam->particle[i];
#ifdef RFCAV
      wgt+=particle.wgt;
      y1+=particle.y*particle.wgt;
      x1+=particle.x*particle.wgt;
#endif
      double de0=(c_beam[i_s]*c_long+s_beam[i_s]*s_long)*gradient+beam->factor*beam->field[0].de[i_s];
      double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
      double gammap=delta*length_i;
      particle.yp-=gammap*particle.y;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      //    gammap=0.5*de0/(particle.energy*geometry.length);
      gammap=delta*length_i;
#ifdef END_FIELDS
      particle.yp-=gammap*particle.y;
#endif
      /* scd 1.0-delta ? */
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
      particle.yp*=tmp;
      
#ifdef END_FIELDS
      particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
      particle.xp*=tmp;
      particle.energy+=half_length*de0;
      
      i++;
    }
#ifdef HTGEN    
    // Move haloparticles to the centre of the structure
    for (int i_h=0;i_h<beam->particle_number_sec[i_s];i_h++) {
      PARTICLE &particle=beam->particle_sec[imacro3];
      double de0=(c_beam[i_s]*c_long+s_beam[i_s]*s_long)*gradient+beam->factor*beam->field[0].de[i_s];
      double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
      double gammap=delta*length_i;
      particle.yp-=gammap*particle.y;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      gammap=delta*length_i;
#ifdef END_FIELDS
      particle.yp-=gammap*particle.y;
#endif
      /* scd 1.0-delta ? */
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
      particle.yp*=tmp;
      
#ifdef END_FIELDS
      particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
      particle.xp*=tmp;
      particle.energy+=half_length*de0;
      
      imacro3++;
    }
#endif
  }
  
  /*
    Apply the wakefield kick
  */
  
  if (wakefield_data.transv) {
    wake_kick_many(beam);
    //      dipole_kick(element,beam,length);
    if (modes.size()>0) {
      for (int i=0;i<modes[0];i++) {
	quadrupole_kick(beam,geometry.length,modes[i+1]);// no halo tracking included
	//	quadrupole_kick_const(beam,geometry.length,element->modes[i+1]);
      }
    }
  }
  
  /*
    Move beam to the end of the structure
  */
#ifdef HTGEN
  imacro3=0;
#endif 
  for (int i=0, i_s=0;i_s<beam->slices_per_bunch;i_s++) {
    //beam particles
    for (int i_m=0;i_m<beam->particle_number[i_s];i_m++) {
      PARTICLE &particle=beam->particle[i];
      double de0=(c_beam[i_s]*c_long+s_beam[i_s]*s_long)*gradient+beam->factor*beam->field[0].de[i_s];
#ifdef CAV_PRECISE
      double delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
      double gammap=delta/geometry.length;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      //    gammap=0.5*de0/(particle.energy*geometry.length);
      double gammap=half_length*de0/(particle.energy*length);
      double tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
      particle.yp+=gammap*particle.y;
#endif
      particle.yp*=tmp;
      
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
      particle.xp+=gammap*particle.x;
#endif
      particle.xp*=tmp;
      
      particle.energy+=half_length*de0;
      
#ifdef EARTH_FIELD
      particle.yp+=earth_field.y*geometry.length/particle.energy;
      particle.xp+=earth_field.x*geometry.length/particle.energy;
#endif
      
#ifdef RFCAV
      y2+=particle.y*particle.wgt;
      x2+=particle.x*particle.wgt;
#endif
      i++;
    }
#ifdef HTGEN
    //Move the haloparticles to the end of the structure
    for (int i_h=0;i_h<beam->particle_number_sec[i_s];i_h++) {
      PARTICLE &particle=beam->particle_sec[imacro3];
      double de0=(c_beam[i_s]*c_long+s_beam[i_s]*s_long)*gradient+beam->factor*beam->field[0].de[i_s];
#ifdef CAV_PRECISE
      double delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
      double gammap=delta/geometry.length;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      double gammap=half_length*de0/(particle.energy*length);
      double tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
      particle.yp+=gammap*particle.y;
#endif
      particle.yp*=tmp;
      
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
      particle.xp+=gammap*particle.x;
#endif
      particle.xp*=tmp;
      
      particle.energy+=half_length*de0;
      
#ifdef EARTH_FIELD
      particle.yp+=earth_field.y*geometry.length/particle.energy;
      particle.xp+=earth_field.x*geometry.length/particle.energy;
#endif
      imacro3++;
    }
#endif
  }
#ifdef RFCAV
  y1/=wgt; ///// a che servono queste istruzioni?!?!??!
  y2/=wgt;
  x1/=wgt;
  x2/=wgt;
#endif
}

void CAVITY::wake_kick_many(BEAM *beam )
{
  double factor=beam->transv_factor*geometry.length*beam->factor;
  switch(beam->which_field) {
  case 1://one bunch; one macroparticle per slice
    {
      double *p=beam->field[0].kick;
      //loop over slices = loop over macroparticles
      if(beam->nhalo){
#ifdef HTGEN 
	int imacro1=0;
	int imacro3=0;
	for (int k=0;k<beam->bunches;k++) {
	  for(int i=0;i<beam->slices_per_bunch; i++){
	    double sumy=0.0;
	    double sumx=0.0;
	    for (int j=0;j<beam->particle_number[k*beam->slices_per_bunch+i];j++) {
	      sumy=0.0;
	      sumx=0.0;
	      for (int i_m=0;i_m<=imacro1;i_m++) {
		sumy+=beam->particle[i_m].y * p[0];
		sumx+=beam->particle[i_m].x * p[0];
		p++;
	      }
	      beam->particle[imacro1].yp+=sumy*factor/beam->particle[imacro1].energy;
	      beam->particle[imacro1].xp+=sumx*factor/beam->particle[imacro1].energy;
	    }
	    // now apply the kick of this slice to the haloparticles
	    for(int i_h=0; i_h<beam->particle_number_sec[k*beam->slices_per_bunch+i];i_h++){
	      beam->particle_sec[imacro3].yp+=sumy*factor/beam->particle_sec[imacro3].energy;
	      beam->particle_sec[imacro3].xp+=sumx*factor/beam->particle_sec[imacro3].energy;
	      imacro3++; 
	    }
	  }
	}
#endif
      } else { 
	for (int j=0;j<beam->slices;j++) {
	  double sumy=0.0;
	  double sumx=0.0;
	  for (int i=0;i<=j;i++) {
	    sumy+=beam->particle[i].y * p[0];
	    sumx+=beam->particle[i].x * p[0];
	    p++;
	  }
	  beam->particle[j].yp+=sumy*factor/beam->particle[j].energy;
	  beam->particle[j].xp+=sumx*factor/beam->particle[j].energy;
	}
      }
    break;
    }
    /* old version 6 */
  case 61:
    {
      int n=beam->slices_per_bunch;
      int m=0; 
      double *rho_y=beam->rho_y[0];
      double *rho_x=beam->rho_x[0];
      for (int i=0;i<n;i++) {
	double pos_y=0.0;
	double pos_x=0.0;
	for (int j=0;j<beam->particle_number[i];j++) {
	  pos_y+=beam->particle[m].y*beam->particle[m].wgt;
	  pos_x+=beam->particle[m].x*beam->particle[m].wgt;
	  m++;
	}
	rho_y[i]=pos_y;
	rho_x[i]=pos_x;
      }
      double *p=beam->field[0].kick;
      m=0;
#ifdef HTGEN
      int imacro3=0;
#endif 
      for (int i=0;i<n;i++) {
	double sumy=0.0;
	double sumx=0.0;
	for (int j=0;j<=i;j++) {
	  sumy+=rho_y[j]* p[0];
	  sumx+=rho_x[j]* p[0];
	  p++;
	}
	for (int j=0;j<beam->particle_number[i];j++) {
	  beam->particle[m].yp+=sumy*factor/beam->particle[m].energy;
	  beam->particle[m].xp+=sumx*factor/beam->particle[m].energy;
	  m++;
	}
#ifdef HTGEN
	//haloparticles
	for(int i_h=0; i_h<beam->particle_number_sec[i];i_h++){
	  beam->particle_sec[imacro3].yp+=sumy*factor/beam->particle_sec[imacro3].energy;
	  beam->particle_sec[imacro3].xp+=sumx*factor/beam->particle_sec[imacro3].energy;
	  imacro3++; 
	}
#endif
      }
      break;
    }
  case 6:
    {
      double *rho_y=beam->rho_y[0];
      double *rho_x=beam->rho_x[0];
      double *p=beam->field[0].kick;
      int n=beam->slices_per_bunch;
      int m=0;
#ifdef HTGEN
      int imacro3=0; //haloparticles
#endif
      for (int i=0;i<n;i++) {
	double sumx=0.0;
	double sumy=0.0;
	double pos_x=0.0;
	double pos_y=0.0;  
	for (int j=0;j<=i;j++) {
	  sumy+=rho_y[j]* p[0];
	  sumx+=rho_x[j]* p[0];
	  p++;
	}
	for (int j=0;j<beam->particle_number[i];j++) {
	  pos_x+=beam->particle[m].x*beam->particle[m].wgt;
	  pos_y+=beam->particle[m].y*beam->particle[m].wgt;
	  beam->particle[m].xp+=sumx*factor/beam->particle[m].energy;
	  beam->particle[m].yp+=sumy*factor/beam->particle[m].energy;
	  m++;
	}
	rho_y[i]=pos_y;
	rho_x[i]=pos_x;
#ifdef HTGEN
	//haloparticles
	for (int i_h=0;i_h<beam->particle_number_sec[i];i_h++) {
	  beam->particle_sec[imacro3].xp+=sumx*factor/beam->particle_sec[imacro3].energy;
	  beam->particle_sec[imacro3].yp+=sumy*factor/beam->particle_sec[imacro3].energy;
	  imacro3++;
	}
#endif
      }
    }
    break;
  }
}

