#define CAV_MULTI_ANGLE
#define EQUAL_SLICE

extern struct{
  double a0,l_cav,lambda_transverse,
    lambda[1],gradient,q_value,shift,band;
  int steps;
  WAKE_DATA *wake;
} injector_data;

void
dipole_kick_n(CAVITY *element,BEAM *beam,double length)
{
  double tmp,factor,x,y,half_length;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,multi_y[injector_data.steps],multi_x[injector_data.steps],
    *rho_a,*rho_b,multi_y_b[injector_data.steps],pos_yb,pos_xb,longfact[injector_data.steps],s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,
    multi_x_b[injector_data.steps],tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2,*rho_xp,*rho_yp,z_mode,d_z;
  DRIVE_DATA *cavity_data;
#ifdef EQUAL_SLICE
  double *ds0,*dc0;
#endif  
  int ap=0;

  n_macro=beam->macroparticles;
  cavity_data=beam->drive_data;
  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  rho_yp=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  rho_xp=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  half_length=0.5*length;
  
  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  rndm_scale=element->v_tesla;
  rho_y=beam->rho_y[0];
#ifdef TWODIM
  rho_x=beam->rho_x[0];
#endif
  if (beam->bunches==1) {
      p=beam->field->kick;
      for (j=0;j<beam->slices_per_bunch;j++){
	  rho_y[j]=0.0;
#ifdef TWODIM
	  rho_x[j]=0.0;
#endif
	  for (i_m=0;i_m<n_macro;i_m++){
	      rho_y[j]+=beam->particle[j*n_macro+i_m].y
		  *beam->particle[j*n_macro+i_m].wgt;
#ifdef TWODIM
	      rho_x[j]+=beam->particle[j*n_macro+i_m].x
		  *beam->particle[j*n_macro+i_m].wgt;
#endif
	  }
	  sumy=0.0;
#ifdef TWODIM
	  sumx=0.0;
#endif
	  for (i=0;i<=j;i++){
	      sumy+=rho_y[i] * *p;
#ifdef TWODIM
	      sumx+=rho_x[i] * *p;
#endif
	      p++;
	  }
	  for (i_m=0;i_m<n_macro;i_m++){
	      beam->particle[j*n_macro+i_m].yp+=sumy
		  *factor/beam->particle[j*n_macro+i_m].energy;
#ifdef TWODIM
	      beam->particle[j*n_macro+i_m].xp+=sumx
		  *factor/beam->particle[j*n_macro+i_m].energy;
#endif
	  }
      }
      return;
  }

#ifdef EQUAL_SLICE
  ds0=(double*)alloca(sizeof(double)*injector_data.steps);
  dc0=(double*)alloca(sizeof(double)*injector_data.steps);
#endif

  for (step=0;step<injector_data.steps;step++){
    multi_y[step]=0.0;
    multi_y_b[step]=0.0;
#ifdef TWODIM
    multi_x[step]=0.0;
    multi_x_b[step]=0.0;
#endif
    longfact[step]=cavity_data->along[step*beam->bunches];
#ifdef EQUAL_SLICE
    ds0[step]=sin(rndm_scale*(beam->z_position[1]-beam->z_position[0])
		  *TWOPI*1e-6/injector_data.wake->lambda[step]);
    dc0[step]=cos(rndm_scale*(beam->z_position[1]-beam->z_position[0])
		  *TWOPI*1e-6/injector_data.wake->lambda[step]);
#endif
  }
  
  d_z=length/injector_data.steps;

  /* loop over all bunches */
  
  for (k=0;k<beam->bunches;k++){
    
    /* apply attenuation */
    
    if (k>0){
      for (step=0;step<injector_data.steps;step++){
	multi_y[step]*=cavity_data->along[step*beam->bunches+k];
	multi_y_b[step]*=cavity_data->along[step*beam->bunches+k];
#ifdef TWODIM
	multi_x[step]*=cavity_data->along[step*beam->bunches+k];
	multi_x_b[step]*=cavity_data->along[step*beam->bunches+k];
#endif
      }
    }
    
    m=k*beam->slices_per_bunch;
    p=beam->field->kick;
    for (j=0;j<beam->slices_per_bunch;j++){
      rho_y[j]=0.0;
      rho_yp[j]=0.0;
#ifdef TWODIM
      rho_x[j]=0.0;
      rho_xp[j]=0.0;
#endif
      for (i_m=0;i_m<n_macro;i_m++){
	rho_y[j]+=beam->particle[(m+j)*n_macro+i_m].y
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef CAV_MULTI_ANGLE
	rho_yp[j]+=beam->particle[(m+j)*n_macro+i_m].yp
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
#ifdef TWODIM
	rho_x[j]+=beam->particle[(m+j)*n_macro+i_m].x
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef CAV_MULTI_ANGLE
	rho_xp[j]+=beam->particle[(m+j)*n_macro+i_m].xp
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
#endif
      }
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      kick_y[j]=sumy;
#ifdef TWODIM
      kick_x[j]=sumx;
#endif
    }
    z_mode=-0.5*(length-d_z);
    for (step=0;step<injector_data.steps;step++) {
      pos_y=0.0;
      pos_yb=0.0;
#ifdef TWODIM
      pos_x=0.0;
      pos_xb=0.0;
#endif
      s=sin(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
      c=cos(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/injector_data.wake->lambda[step]);
#ifdef EQUAL_SLICE
      ds=ds0[step];
      dc=dc0[step];
#else
      ds=sin(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	     *TWOPI*1e-6/injector_data.wake->lambda[step]);
      dc=cos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	     *TWOPI*1e-6/injector_data.wake->lambda[step]);
#endif
      for (j=0;j<beam->slices_per_bunch;j++){
#ifdef CAV_MULTI_ANGLE
	pos_y+=(rho_y[j]+z_mode*rho_yp[j])*c;
	pos_yb-=(rho_y[j]+z_mode*rho_yp[j])*s;
#ifdef TWODIM
	pos_x+=(rho_x[j]+z_mode*rho_xp[j])*c;
	pos_xb-=(rho_x[j]+z_mode*rho_xp[j])*s;
#endif
#else
	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
#endif
#endif
	kick_y[j]+=(multi_y[step]*s+multi_y_b[step]*c)*longfact[step];
#ifdef TWODIM
	kick_x[j]+=(multi_x[step]*s+multi_x_b[step]*c)*longfact[step];
#endif
	tmp=dc*c-ds*s;
	s=ds*c+dc*s;
	c=tmp;
      }
      multi_y[step]+=pos_y;
      multi_y_b[step]+=pos_yb;
#ifdef TWODIM
      multi_x[step]+=pos_x;
      multi_x_b[step]+=pos_xb;
#endif
      z_mode+=d_z;
    }
    for (j=0;j<beam->slices_per_bunch;j++) {
      for (i_m=0;i_m<n_macro;i_m++){
	beam->particle[(m+j)*n_macro+i_m].yp+=
	  kick_y[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#ifdef TWODIM
	beam->particle[(m+j)*n_macro+i_m].xp+=
	  kick_x[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#endif
      }
    }
  }
}

void
dipole_kick(CAVITY *element,BEAM *beam,double length)
{
  double tmp,factor,x,y,half_length;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,multi_y[injector_data.steps],multi_x[injector_data.steps],
    *rho_a,*rho_b,multi_y_b[injector_data.steps],pos_yb,pos_xb,longfact[injector_data.steps],s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,
    multi_x_b[injector_data.steps],tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2,*rho_xp,*rho_yp,z_mode,d_z;
  double *w,w_s,w_sp;
  DRIVE_DATA *cavity_data;
  
  n_macro=beam->macroparticles;
  if (n_macro>1) {
      dipole_kick_n(element,beam,length);
      return;
  }

  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  w=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  rho_yp=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  rho_xp=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  half_length=0.5*length;
  cavity_data=beam->drive_data;  
  factor=beam->transv_factor*length*beam->factor;

  rho_y=beam->rho_y[0];
#ifdef TWODIM
  rho_x=beam->rho_x[0];
#endif

  if (beam->bunches==1) {
      p=beam->field->kick;
      for (j=0;j<beam->slices_per_bunch;j++){
	  rho_y[j]=0.0;
#ifdef TWODIM
	  rho_x[j]=0.0;
#endif
	  rho_y[j]+=beam->particle[j].y
	      *beam->particle[j].wgt;
#ifdef TWODIM
	  rho_x[j]+=beam->particle[j].x
	      *beam->particle[j].wgt;
#endif
	  sumy=0.0;
#ifdef TWODIM
	  sumx=0.0;
#endif
	  for (i=0;i<=j;i++){
	      sumy+=rho_y[i] * *p;
#ifdef TWODIM
	      sumx+=rho_x[i] * *p;
#endif
	      p++;
	  }
	  beam->particle[j].yp+=sumy
	      *factor/beam->particle[j].energy;
#ifdef TWODIM
	  beam->particle[j].xp+=sumx
	      *factor/beam->particle[j].energy;
#endif
      }
      return;
  }

  /* Initialise wakefields */

  rndm_scale=element->v_tesla;
  for (step=0;step<injector_data.steps;step++){
    multi_y[step]=0.0;
    multi_y_b[step]=0.0;
#ifdef TWODIM
    multi_x[step]=0.0;
    multi_x_b[step]=0.0;
#endif
    longfact[step]=cavity_data->along[step*beam->bunches];
  }

  d_z=length/injector_data.steps;

  /* loop over all bunches */
  
  for (k=0;k<beam->bunches;k++){
    
    /* apply attenuation */
    
      if (k>0){
	  for (step=0;step<injector_data.steps;step++){
	      multi_y[step]*=cavity_data->along[step*beam->bunches+k];
	      multi_y_b[step]*=cavity_data->along[step*beam->bunches+k];
#ifdef TWODIM
	      multi_x[step]*=cavity_data->along[step*beam->bunches+k];
	      multi_x_b[step]*=cavity_data->along[step*beam->bunches+k];
#endif
	  }
      }
    
      m=k*beam->slices_per_bunch;
      p=beam->field->kick;
      for (j=0;j<beam->slices_per_bunch;j++){
	//nanu
	w[j]=beam->particle[m+j].wgt;
	  rho_y[j]=beam->particle[m+j].y
	      *beam->particle[m+j].wgt;
	  rho_yp[j]=beam->particle[m+j].yp
	      *beam->particle[m+j].wgt;
#ifdef TWODIM
	  rho_x[j]=beam->particle[m+j].x
	      *beam->particle[m+j].wgt;
	  rho_xp[j]=beam->particle[m+j].xp
	      *beam->particle[m+j].wgt;
#endif
	  sumy=0.0;
#ifdef TWODIM
	  sumx=0.0;
#endif
	  for (i=0;i<=j;i++){
	      sumy+=rho_y[i] * *p;
#ifdef TWODIM
	      sumx+=rho_x[i] * *p;
#endif
	      p++;
	  }
	  kick_y[j]=sumy;
#ifdef TWODIM
	  kick_x[j]=sumx;
#endif
      }
      z_mode=-0.5*(length-d_z);
      for (step=0;step<injector_data.steps;step++) {
	  pos_y=0.0;
	  pos_yb=0.0;
#ifdef TWODIM
	  pos_x=0.0;
	  pos_xb=0.0;
#endif
	  s=sin(rndm_scale*beam->z_position[m]
		*TWOPI*1e-6/injector_data.wake->lambda[step]);
	  c=cos(rndm_scale*beam->z_position[m]
		*TWOPI*1e-6/injector_data.wake->lambda[step]);
	  ds=sin(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
		 *TWOPI*1e-6/injector_data.wake->lambda[step]);
	  dc=cos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
		     *TWOPI*1e-6/injector_data.wake->lambda[step]);
	  for (j=0;j<beam->slices_per_bunch;j++){
	      pos_y+=(rho_y[j]+z_mode*rho_yp[j])*c;
	      pos_yb-=(rho_y[j]+z_mode*rho_yp[j])*s;
#ifdef TWODIM
	      pos_x+=(rho_x[j]+z_mode*rho_xp[j])*c;
	      pos_xb-=(rho_x[j]+z_mode*rho_xp[j])*s;
#endif
	      kick_y[j]+=(multi_y[step]*s+multi_y_b[step]*c)*longfact[step];
#ifdef TWODIM
	      kick_x[j]+=(multi_x[step]*s+multi_x_b[step]*c)*longfact[step];
#endif
	      tmp=dc*c-ds*s;
	      s=ds*c+dc*s;
	      c=tmp;
	  }
	  multi_y[step]+=pos_y;
	  multi_y_b[step]+=pos_yb;
#ifdef TWODIM
	  multi_x[step]+=pos_x;
	  multi_x_b[step]+=pos_xb;
#endif
	  z_mode+=d_z;
      }
      for (j=0;j<beam->slices_per_bunch;j++) {
	  beam->particle[m+j].yp+=
	      kick_y[j]*factor/beam->particle[m+j].energy;
#ifdef TWODIM
	  beam->particle[m+j].xp+=
	      kick_x[j]*factor/beam->particle[m+j].energy;
#endif
      }
  }
}

