#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../fftw/fftw-2.1.3/fftw/fftw.h"

#define M 128
#define N 1024

main()
{
  int i,j,m,mdo,mdone;
  fftw_complex *in,*out;
  fftwnd_plan p,p2;
  fftw_plan p_a,p_b,p2_a,p2_b;
  double r,s;
  FILE *file;
  char buffer[1000];
  double d[M];
  float f[M];

  in=(fftw_complex*)malloc(sizeof(fftw_complex)*N*M);
  out=(fftw_complex*)malloc(sizeof(fftw_complex)*N*M);

  p = fftw2d_create_plan_specific(N,M,FFTW_FORWARD,
				  FFTW_ESTIMATE | FFTW_IN_PLACE,
				  in,1,out,1);
  p2 = fftw2d_create_plan_specific(N,M,FFTW_BACKWARD,
				  FFTW_ESTIMATE | FFTW_IN_PLACE,
				  in,1,out,1);
  p_a = fftw_create_plan_specific(M,FFTW_FORWARD,
				  FFTW_ESTIMATE | FFTW_IN_PLACE,
				  in,1,out,1);
  p_b = fftw_create_plan_specific(N,FFTW_FORWARD,
				  FFTW_ESTIMATE | FFTW_IN_PLACE,
				  in,M,out,1);
  p2_a = fftw_create_plan_specific(M,FFTW_BACKWARD,
				   FFTW_ESTIMATE | FFTW_IN_PLACE,
				   in,1,out,1);
  p2_b = fftw_create_plan_specific(N,FFTW_BACKWARD,
				   FFTW_ESTIMATE | FFTW_IN_PLACE,
				   in,M,out,1);
  s=1.0/(N*M);
  m=0;
  for (j=0;j<N/2;j++) {
      for (i=0;i<M/2;i++){
	  in[m].re=1.0*m;
	  in[m].im=0.0;
	  m++;
      }
      for (i=M/2;i<M;i++){
	  in[m].re=0.0;
	  in[m].im=0.0;
	  m++;
      }
  }
  for (j=N/2;j<N;j++) {
      for (i=0;i<M;i++){
	  in[m].re=0.0;
	  in[m].im=0.0;
	  m++;
      }
  }
  for (i=0;i<100;i++){

//      fftwnd_one(p,in,NULL);
      fftw(p_b,M/2,in,M,1,NULL,1,1);
      fftw(p_a,N,in,1,M,NULL,1,1);
      for (j=0;j<N*M;j++){
	  in[j].re*=s;
	  in[j].im*=s;
      }
      fftw(p2_a,N,in,1,M,NULL,1,1);
      fftw(p2_b,M/2,in,M,1,NULL,1,1);
//      fftwnd_one(p2,in,NULL);

  }
//  for (i=0;i<N*M;i++){
//      printf("%g %g\n",in[i].re,in[i].im);
//  }
  fftwnd_destroy_plan(p);
  fftwnd_destroy_plan(p2);
  exit(0);
}

