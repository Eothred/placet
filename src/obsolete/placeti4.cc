void align_quad(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,BEAM *tb,
	   BEAM *workbunch)
{
  int i,j,iter,ipos;
  ELEMENT **element;
  element=beamline->element;

  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch0,ipos,bin[i]->start);
    bunch_copy_0(bunch0,tb);
    for (j=ipos;j<bin[i]->bpm[bin[i]->nbpm-1];j++){
      if (CAVITY *cavity=element[j]->cavity_ptr()){
	for (iter=0;iter<5;iter++){
	  bunch_copy_0(tb,workbunch);
	  cavity->track_rf_0(workbunch);
	}
      }
      element[j]->track_0(tb);
    }
    bunch_copy_0(bunch0,tb);
    bin_measure(beamline,tb,0,bin[i]);
    bin_correct(beamline,0,bin[i]);
    ipos=bin[i]->start;
  }
  bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  /*
  for (i=0;i<beamline->n_elements;i++){
    if (is_cavity(element[i])){
      for (j=0;j<5;j++){
	bunch_copy_0(bunch0,workbunch);
	element[i]->track_rf_0(workbunch);
      }
      printf("%g\n",element[i]->offset.y);
    }
    element[i]->track(bunch0);
  }
  */
}

void align_rf_jitter(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
		BEAM *bunch1,BEAM *tb,BEAM *tb1,BEAM *workbunch)
{
  int i,j,iter,ipos;

  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_copy_0(bunch0,tb);
    bunch_copy_0(bunch1,tb1);
    for (j=ipos;j<bin[i]->bpm[bin[i]->nbpm-1];j++){
      if (CAVITY *cavity=beamline->element[j]->cavity_ptr()){
	for (iter=0;iter<3;iter++){
	  bunch_join_0(tb,tb1,gasdev(),workbunch);
	  cavity->track_rf_0(workbunch);
	  cavity->offset.y+=cavity->get_bpm_y_reading();
#ifdef TWODIM
	  cavity->offset.x+=cavity->get_bpm_x_reading();
#endif
	}
      }
      beamline->element[j]->track_0(tb);
      beamline->element[j]->track_0(tb1);
    }
    bunch_join_0(bunch0,bunch1,gasdev(),tb);
    bunch_track_0(beamline,tb,ipos,bin[i]->start);
    bin_measure(beamline,tb,0,bin[i]);
    bin_correct(beamline,0,bin[i]);
    bunch_track(beamline,bunch0,ipos,bin[i]->quad[bin[i]->qlast-1]);
    bunch_track_0(beamline,bunch1,ipos,bin[i]->quad[bin[i]->qlast-1]);
    ipos=bin[i]->quad[bin[i]->qlast-1];
  }
  bunch_track(beamline,bunch0,ipos,beamline->n_elements);
}

void ballistic_correct_emitt(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			     BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    emitt_store(i,bunch);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_correct(beamline,0,bin[i]);
    ipos=bin[i]->start;
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
  emitt_store(nbin,bunch);
}

void beamline_cut(BEAMLINE *beamline,BIN **bin,int nbin,int pos[])
{
  int i;
  pos[0]=0;
  for (i=1;i<nbin+1;i++){
    pos[i]=bin[i-1]->start;
  }
  pos[nbin+1]=bin[nbin-1]->bpm[bin[nbin-1]->nbpm-1]+1;
  pos[nbin+2]=beamline->n_elements;
}

void beamline_survey_clic_old(BEAMLINE *beamline)
{
  int i;

  for (i=0;i<beamline->n_elements;i++){
    switch (beamline->element[i]->type){
    case CAV_PETS:
    case CAV:
      beamline->element[i]->offset.y=survey_errors.cav_error*gasdev_survey();
      beamline->element[i]->offset.yp=survey_errors.cav_error_angle*gasdev_survey();
      ((CAVITY*)beamline->element[i])->bpm_y=
	survey_errors.cav_error_realign*gasdev_survey();
#ifdef TWODIM
      beamline->element[i]->offset.x=survey_errors.cav_error_x*gasdev_survey();
      beamline->element[i]->offset.xp=survey_errors.cav_error_angle_x
	*gasdev_survey();
      ((CAVITY*)(beamline->element[i]))->bpm_x=
	survey_errors.cav_error_realign_x*gasdev_survey();
#endif
      break;
    case QUAD:
      beamline->element[i]->offset.y=survey_errors.quad_error*gasdev_survey();
      beamline->element[i]->offset.yp=survey_errors.quad_error_angle
	*gasdev_survey();
#ifdef TWODIM
      beamline->element[i]->offset.x=survey_errors.quad_error_x*gasdev_survey();
      beamline->element[i]->offset.xp=survey_errors.quad_error_angle_x
	*gasdev_survey();
      beamline->element[i]->offset.roll=survey_errors.quad_roll*gasdev_survey();
#endif
      break;
    case QUADBPM:
      beamline->element[i]->offset.y=survey_errors.quad_error*gasdev_survey();
      beamline->element[i]->offset.yp=survey_errors.quad_error_angle
	*gasdev_survey();
      beamline->element[i]->help[0]=survey_errors.bpm_error*gasdev_survey();
#ifdef TWODIM
      beamline->element[i]->offset.x=survey_errors.quad_error_x*gasdev_survey();
      beamline->element[i]->offset.xp=survey_errors.quad_error_angle_x
	*gasdev_survey();
      beamline->element[i]->offset.roll=survey_errors.quad_roll*gasdev_survey();
      beamline->element[i]->help[1]=survey_errors.bpm_error*gasdev_survey();
#endif
      break;
    case BPM:
    case BPMDRIFT:
      beamline->element[i]->offset.y=survey_errors.bpm_error*gasdev_survey();
      beamline->element[i]->offset.yp=survey_errors.bpm_error_angle*gasdev_survey();
#ifdef TWODIM
      beamline->element[i]->offset.x=survey_errors.bpm_error_x*gasdev_survey();
      beamline->element[i]->offset.xp=survey_errors.bpm_error_angle_x
	*gasdev_survey();
      beamline->element[i]->set_tilt(survey_errors.bpm_roll*gasdev_survey());
#endif
      break;
    case _DIPOLE:
      beamline->element[i]->v1=0.0;
#ifdef TWODIM
      beamline->element[i]->v2=0.0;
#endif
    case DRIFT:
      beamline->element[i]->offset.y=survey_errors.drift_error*gasdev_survey();
      beamline->element[i]->offset.yp=survey_errors.drift_error_angle
	*gasdev_survey();
#ifdef TWODIM
      beamline->element[i]->offset.x=survey_errors.drift_error_x*gasdev_survey();
      beamline->element[i]->offset.xp=survey_errors.drift_error_angle_x
	*gasdev_survey();
#endif
      break;
    }
    beamline->element[i]->offset.y+=zero_point;
  }
}

void beamline_survey_tesla(BEAMLINE *beamline)
{
  int i;

  for (i=0;i<beamline->n_elements;i++){
    if (beamline->element[i]->is_cavity()){
      ((CAVITY*)(beamline->element[i]))->v_tesla=(1.0+0.001*gasdev());
    }
  }
}

void beamline_transfer(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		  BEAM *workbunch,int pos[])
{
  int i;
  double transf[5000],tot[5000],tmp[5000];
  bunch_backtrack_0(beamline,bunch,pos[nbin+2],pos[nbin+1]);
  bin_transfer(beamline,NULL,pos[nbin+1],pos[nbin+2],bunch,workbunch,tmp);
  for (i=nbin;i>0;i--){
    beamline_set_zero(beamline);
    bunch_backtrack_0(beamline,bunch,pos[i+1],pos[i]);
    bin_transfer(beamline,bin[i-1],pos[i],pos[i+1],bunch,workbunch,transf);
    matrix_mult(tmp,62,62,transf,62,62,tot);
    matrix_copy(tot,tmp,62*62);
    printf("%g\n",tot[0]);
    /*
printf("tot %d=\n",i);
print_transf(tot);
*/
  }
  beamline_set_zero(beamline);
  bunch_backtrack_0(beamline,bunch,pos[1],pos[0]);
  bin_transfer(beamline,NULL,pos[0],pos[1],bunch,workbunch,transf);
  /*
printf("transf=\n");
print_transf(transf);
*/
  matrix_mult(tmp,62,62,transf,62,62,tot);
printf("tot=\n");
  print_transf(tot);
}

void bin_quadrupoles_set_zero(BEAMLINE *beamline,BIN *bin)
{
  int i;
  for (i=0;i<bin->nq;i++){
    element_set_offset_to(beamline->element[bin->quad[i]],0.0,0.0);
  }
}

void bin_transfer(BEAMLINE *beamline,BIN *bin,int start,int end,BEAM *bunch,
	     BEAM *tb,double r[])
{
  int i,j,n;
  n=bunch->slices;
  for (i=0;i<n;i++){
    if (bin!=NULL){
      bunch_copy_0(bunch,tb);
      tb->particle[i].y=1.0;
      bin_measure(beamline,tb,0,bin);
      bin_correct(beamline,0,bin);
    }
    bunch_copy_0(bunch,tb);
    tb->particle[i].y=1.0;
    bunch_track_0(beamline,tb,start,end);
    for (j=0;j<n;j++){
      r[2*n*(j*2)+2*i]=tb->particle[j].y;
      r[2*n*(j*2+1)+2*i]=tb->particle[j].yp;
    }
    if (bin!=NULL){
      bunch_copy_0(bunch,tb);
      tb->particle[i].yp=1.0;
      bin_measure(beamline,tb,0,bin);
      bin_correct(beamline,0,bin);
    }
    bunch_copy_0(bunch,tb);
    tb->particle[i].yp=1.0;
    bunch_track_0(beamline,tb,start,end);
    for (j=0;j<n;j++){
      r[2*n*(2*j)+2*i+1]=tb->particle[j].y;
      r[2*n*(2*j+1)+2*i+1]=tb->particle[j].yp;
    }
  }
}

void bpm_bin_reset(BEAMLINE *beamline,BIN *bin)
{
  int i;
  for (i=0;i<bin->nbpm;i++){
    beamline->element[bin->bpm[i]]->offset.y-=
      (beamline->element[bin->bpm[i]])->get_bpm_y_reading_exact();
  }
}

void bump_calculate_2(BUMP *bump,BEAM *bunch,double s[])
{
  double k1=0.0,k2=0.0,h11=0.0,h12=0.0,h22=0.0;
  double k1_2=0.0,k2_2=0.0,h11_2=0.0,h12_2=0.0,h22_2=0.0;
  double tmp_a11=0.0,tmp_a12=0.0,tmp_a21=0.0,tmp_a22=0.0,wgt_sum=0.0,
    tmp2_a11=0.0,tmp2_a12=0.0,tmp2_a21=0.0,tmp2_a22=0.0,wgt2_sum=0.0;
  double *a11,*a12,*a21,*a22;
  double alpha,beta,gamma,wgt;
  int i,ns,nb;
  PARTICLE *particle;
  double y=0.0,y2=0.0,yp=0.0,yp2=0.0;

  nb=bunch->bunches;
  ns=bunch->slices_per_bunch*bunch->macroparticles;
  alpha=bump->alpha;
  beta=bump->beta;
  gamma=bump->gamma;
  a11=bump->a11;
  a12=bump->a12;
  a21=bump->a21;
  a22=bump->a22;
  particle=bunch->particle;
//  n=bunch->slices;
  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(particle[i].wgt);
    if (particle[i].wgt<0.0) {
      wgt2_sum+=wgt;
      tmp2_a11+=wgt*a11[i];
      tmp2_a12+=wgt*a12[i];
      tmp2_a21+=wgt*a21[i];
      tmp2_a22+=wgt*a22[i];
#ifndef BUMP_AXIS
      y2+=wgt*particle[i].y;
      yp2+=wgt*particle[i].yp;
#endif
    }
    else {
      wgt_sum+=wgt;
      tmp_a11+=wgt*a11[i];
      tmp_a12+=wgt*a12[i];
      tmp_a21+=wgt*a21[i];
      tmp_a22+=wgt*a22[i];
#ifndef BUMP_AXIS
      y+=wgt*particle[i].y;
      yp+=wgt*particle[i].yp;
#endif
    }
  }
  for (;i<ns*nb;i++){
    wgt=fabs(particle[i].wgt)*bunch->last_wgt;
    if (particle[i].wgt<0.0) {
      wgt2_sum+=wgt;
      tmp2_a11+=wgt*a11[i];
      tmp2_a12+=wgt*a12[i];
      tmp2_a21+=wgt*a21[i];
      tmp2_a22+=wgt*a22[i];
#ifndef BUMP_AXIS
      y2+=wgt*particle[i].y;
      yp2+=wgt*particle[i].yp;
#endif
    }
    else {
      wgt_sum+=wgt;
      tmp_a11+=wgt*a11[i];
      tmp_a12+=wgt*a12[i];
      tmp_a21+=wgt*a21[i];
      tmp_a22+=wgt*a22[i];
#ifndef BUMP_AXIS
      y+=wgt*particle[i].y;
      yp+=wgt*particle[i].yp;
#endif
    }
  }
  y/=wgt_sum;
  yp/=wgt_sum;
  tmp_a11/=wgt_sum;
  tmp_a12/=wgt_sum;
  tmp_a21/=wgt_sum;
  tmp_a22/=wgt_sum;
  y2/=wgt2_sum;
  yp2/=wgt2_sum;
  tmp2_a11/=wgt2_sum;
  tmp2_a12/=wgt2_sum;
  tmp2_a21/=wgt2_sum;
  tmp2_a22/=wgt2_sum;
  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(particle[i].wgt);
    if (particle[i].wgt<0.0) {
      k1_2+=gamma*(particle[i].y-y2)*wgt*(a11[i]-tmp2_a11)
	+beta*(particle[i].yp-yp2)*wgt*(a21[i]-tmp2_a21)
	+alpha*wgt*((particle[i].y-y2)*(a21[i]-tmp2_a21)
		    +(particle[i].yp-yp2)*(a11[i]-tmp2_a11));
      k2_2+=gamma*(particle[i].y-y2)*wgt*(a12[i]-tmp2_a12)
	+beta*(particle[i].yp-yp2)*wgt*(a22[i]-tmp2_a22)
	+alpha*wgt*((particle[i].y-y2)*(a22[i]-tmp2_a22)
		    +(particle[i].yp-yp2)*(a12[i]-tmp2_a12));
      h11_2+=wgt*(gamma*a11[i]*a11[i]+beta*a21[i]*a21[i]
		+2.0*alpha*a11[i]*a21[i]);
      h22_2+=wgt*(gamma*a12[i]*a12[i]+beta*a22[i]*a22[i]
		+2.0*alpha*a12[i]*a22[i]);
      h12_2+=wgt*(gamma*a11[i]*a12[i]+beta*a21[i]*a22[i]
		+alpha*(a12[i]*a21[i]+a11[i]*a22[i]));
    }
    else {
      k1+=gamma*(particle[i].y-y)*wgt*(a11[i]-tmp_a11)
	+beta*(particle[i].yp-yp)*wgt*(a21[i]-tmp_a21)
	+alpha*wgt*((particle[i].y-y)*(a21[i]-tmp_a21)
		    +(particle[i].yp-yp)*(a11[i]-tmp_a11));
      k2+=gamma*(particle[i].y-y)*wgt*(a12[i]-tmp_a12)
	+beta*(particle[i].yp-yp)*wgt*(a22[i]-tmp_a22)
	+alpha*wgt*((particle[i].y-y)*(a22[i]-tmp_a22)
		    +(particle[i].yp-yp)*(a12[i]-tmp_a12));
      h11+=wgt*(gamma*a11[i]*a11[i]+beta*a21[i]*a21[i]
		+2.0*alpha*a11[i]*a21[i]);
      h22+=wgt*(gamma*a12[i]*a12[i]+beta*a22[i]*a22[i]
		+2.0*alpha*a12[i]*a22[i]);
      h12+=wgt*(gamma*a11[i]*a12[i]+beta*a21[i]*a22[i]
		+alpha*(a12[i]*a21[i]+a11[i]*a22[i]));
    }
  }
  for (;i<ns*nb;i++){
    wgt=fabs(particle[i].wgt)*bunch->last_wgt;
    k1+=gamma*(particle[i].y-y)*wgt*(a11[i]-tmp_a11)
      +beta*(particle[i].yp-yp)*wgt*(a21[i]-tmp_a21)
      +alpha*wgt*((particle[i].y-y)*(a21[i]-tmp_a21)
	  +(particle[i].yp-yp)*(a11[i]-tmp_a11));
    k2+=gamma*(particle[i].y-y)*wgt
	*(a12[i]-tmp_a12)
      +beta*(particle[i].yp-yp)*wgt
	*(a22[i]-tmp_a22)
      +alpha*wgt
	*((particle[i].y-y)*(a22[i]-tmp_a22)
	  +(particle[i].yp-yp)*(a12[i]-tmp_a12));
    h11+=wgt
	*(gamma*a11[i]*a11[i]+beta*a21[i]*a21[i]
	  +2.0*alpha*a11[i]*a21[i]);
    h22+=wgt
	*(gamma*a12[i]*a12[i]+beta*a22[i]*a22[i]
	  +2.0*alpha*a12[i]*a22[i]);
    h12+=wgt
	*(gamma*a11[i]*a12[i]+beta*a21[i]*a22[i]
	  +alpha*(a12[i]*a21[i]+a11[i]*a22[i]));
  }
  h11-=gamma*tmp_a11*tmp_a11+beta*tmp_a21*tmp_a21+2.0*alpha*tmp_a11*tmp_a21;
  h22-=gamma*tmp_a12*tmp_a12+beta*tmp_a22*tmp_a22+2.0*alpha*tmp_a12*tmp_a22;
  h12-=gamma*tmp_a11*tmp_a12+beta*tmp_a21*tmp_a22
    +alpha*(tmp_a12*tmp_a21+tmp_a11*tmp_a22);
  s[0]=-(h22*k1-h12*k2)/(h11*h22-h12*h12);
  s[1]=-(h11*k2-h12*k1)/(h11*h22-h12*h12);
  //  printf("found %g %g\n",s[0],s[1]);
  bump_minimise(bunch,bump,s);
  //  printf("found> %g %g\n",s[0],s[1]);
}

void bump_calculate_dispersion(BUMP * /*bump*/,BEAM *bunch,double s[])
{
  double k1=0.0,k2=0.0,h11=0.0,h12=0.0,h22=0.0,e0=0.0;
  double tmp_a11=0.0,tmp_a12=0.0,tmp_a21=0.0,tmp_a22=0.0,wgt_sum=0.0;
  double alpha=0.0,beta=0.0,gamma=0.0,wgt;
  int i,ns,nb;
  PARTICLE *particle;
  double y=0.0,yp=0.0;

  nb=bunch->bunches;
  ns=bunch->slices_per_bunch*bunch->macroparticles;
  particle=bunch->particle;
//  n=bunch->slices;
  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(particle[i].wgt);
    wgt_sum+=wgt;
    e0+=wgt*particle[i].energy;
    alpha+=wgt*bunch->sigma[i].r12;
    beta+=wgt*bunch->sigma[i].r11;
    gamma+=wgt*bunch->sigma[i].r22;
#ifndef BUMP_AXIS
    y+=wgt*particle[i].y;
    yp+=wgt*particle[i].yp;
#endif
  }
  for (;i<ns*nb;i++){
    wgt=fabs(particle[i].wgt)*bunch->last_wgt;
    wgt_sum+=wgt;
    e0+=wgt*particle[i].energy;
    alpha+=wgt*bunch->sigma[i].r12;
    beta+=wgt*bunch->sigma[i].r11;
    gamma+=wgt*bunch->sigma[i].r22;
#ifndef BUMP_AXIS
    y+=wgt*particle[i].y;
    yp+=wgt*particle[i].yp;
#endif
  }
  y/=wgt_sum;
  yp/=wgt_sum;
  e0/=wgt_sum;
  alpha/=wgt_sum;
  beta/=wgt_sum;
  gamma/=wgt_sum;
  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(particle[i].wgt);
    k1+=gamma*(particle[i].y-y)*wgt*(particle[i].energy-e0)
      +beta*(particle[i].yp-yp)*wgt*0.0
      +alpha*wgt*((particle[i].y-y)*0.0
		  +(particle[i].yp-yp)*(particle[i].energy-e0));
    k2+=gamma*(particle[i].y-y)*wgt*0.0
      +beta*(particle[i].yp-yp)*wgt*(particle[i].energy-e0)
      +alpha*wgt*((particle[i].y-y)*(particle[i].energy-e0)
		  +(particle[i].yp-yp)*0.0);
    h11+=wgt*(gamma*(particle[i].energy*particle[i].energy)+beta*0.0
	      +2.0*alpha*0.0);
    h22+=wgt*(gamma*0.0+beta*particle[i].energy*particle[i].energy
	      +2.0*alpha*0.0);
    h12+=wgt*(gamma*0.0+beta*0.0
	      +alpha*(particle[i].energy*particle[i].energy));
		     }
  for (;i<ns*nb;i++){
    wgt=fabs(particle[i].wgt)*bunch->last_wgt;
    k1+=gamma*(particle[i].y-y)*wgt*(particle[i].energy-e0)
      +beta*(particle[i].yp-yp)*wgt*0.0
      +alpha*wgt*((particle[i].y-y)*0.0
		  +(particle[i].yp-yp)*(particle[i].energy-e0));
    k2+=gamma*(particle[i].y-y)*wgt*0.0
      +beta*(particle[i].yp-yp)*wgt*(particle[i].energy-e0)
      +alpha*wgt*((particle[i].y-y)*(particle[i].energy-e0)
		  +(particle[i].yp-yp)*0.0);
    h11+=wgt*(gamma*particle[i].energy*particle[i].energy+beta*0.0
	      +2.0*alpha*0.0);
    h22+=wgt*(gamma*0.0+beta*particle[i].energy*particle[i].energy
	      +2.0*alpha*0.0);
    h12+=wgt*(gamma*0.0+beta*0.0
	      +alpha*(particle[i].energy*particle[i].energy));
  }
  h11-=gamma*tmp_a11*tmp_a11+beta*tmp_a21*tmp_a21+2.0*alpha*tmp_a11*tmp_a21;
  h22-=gamma*tmp_a12*tmp_a12+beta*tmp_a22*tmp_a22+2.0*alpha*tmp_a12*tmp_a22;
  h12-=gamma*tmp_a11*tmp_a12+beta*tmp_a21*tmp_a22
    +alpha*(tmp_a12*tmp_a21+tmp_a11*tmp_a22);
  s[0]=-(h22*k1-h12*k2)/(h11*h22-h12*h12);
  s[1]=-(h11*k2-h12*k1)/(h11*h22-h12*h12);
	      printf("found %g %g\n",s[0],s[1]);
}

void bump_calculate_positive(BUMP *bump,BEAM *bunch,double s[])
{
  double k1=0.0,k2=0.0,h11=0.0,h12=0.0,h22=0.0;
  double tmp_a11=0.0,tmp_a12=0.0,tmp_a21=0.0,tmp_a22=0.0,wgt_sum=0.0;
  double *a11,*a12,*a21,*a22;
  double alpha,beta,gamma,wgt;
  int i,ns,nb;
  PARTICLE *particle;
  double y=0.0,yp=0.0;

  nb=bunch->bunches;
  ns=bunch->slices_per_bunch*bunch->macroparticles;
  alpha=bump->alpha;
  beta=bump->beta;
  gamma=bump->gamma;
  a11=bump->a11;
  a12=bump->a12;
  a21=bump->a21;
  a22=bump->a22;
  particle=bunch->particle;
//  n=bunch->slices;
  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(particle[i].wgt);
    if (wgt<0.0) wgt=0.0;
    wgt_sum+=wgt;
    tmp_a11+=wgt*a11[i];
    tmp_a12+=wgt*a12[i];
    tmp_a21+=wgt*a21[i];
    tmp_a22+=wgt*a22[i];
#ifndef BUMP_AXIS
    y+=wgt*particle[i].y;
    yp+=wgt*particle[i].yp;
#endif
  }
  for (;i<ns*nb;i++){
    wgt=fabs(particle[i].wgt)*bunch->last_wgt;
    if (wgt<0.0) wgt=0.0;
    wgt_sum+=wgt;
    tmp_a11+=wgt*a11[i];
    tmp_a12+=wgt*a12[i];
    tmp_a21+=wgt*a21[i];
    tmp_a22+=wgt*a22[i];
#ifndef BUMP_AXIS
    y+=wgt*particle[i].y;
    yp+=wgt*particle[i].yp;
#endif
  }
  y/=wgt_sum;
  yp/=wgt_sum;
  tmp_a11/=wgt_sum;
  tmp_a12/=wgt_sum;
  tmp_a21/=wgt_sum;
  tmp_a22/=wgt_sum;
  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(particle[i].wgt);
    if (wgt<0.0) wgt=0.0;
    k1+=gamma*(particle[i].y-y)*wgt*(a11[i]-tmp_a11)
      +beta*(particle[i].yp-yp)*wgt*(a21[i]-tmp_a21)
      +alpha*wgt*((particle[i].y-y)*(a21[i]-tmp_a21)
		  +(particle[i].yp-yp)*(a11[i]-tmp_a11));
    k2+=gamma*(particle[i].y-y)*wgt*(a12[i]-tmp_a12)
      +beta*(particle[i].yp-yp)*wgt*(a22[i]-tmp_a22)
      +alpha*wgt*((particle[i].y-y)*(a22[i]-tmp_a22)
		  +(particle[i].yp-yp)*(a12[i]-tmp_a12));
    h11+=wgt*(gamma*a11[i]*a11[i]+beta*a21[i]*a21[i]
	      +2.0*alpha*a11[i]*a21[i]);
    h22+=wgt*(gamma*a12[i]*a12[i]+beta*a22[i]*a22[i]
	      +2.0*alpha*a12[i]*a22[i]);
    h12+=wgt*(gamma*a11[i]*a12[i]+beta*a21[i]*a22[i]
	      +alpha*(a12[i]*a21[i]+a11[i]*a22[i]));
  }
  for (;i<ns*nb;i++){
    wgt=fabs(particle[i].wgt)*bunch->last_wgt;
    if (wgt<0.0) wgt=0.0;
    k1+=gamma*(particle[i].y-y)*wgt*(a11[i]-tmp_a11)
      +beta*(particle[i].yp-yp)*wgt*(a21[i]-tmp_a21)
      +alpha*wgt*((particle[i].y-y)*(a21[i]-tmp_a21)
	  +(particle[i].yp-yp)*(a11[i]-tmp_a11));
    k2+=gamma*(particle[i].y-y)*wgt
	*(a12[i]-tmp_a12)
      +beta*(particle[i].yp-yp)*wgt
	*(a22[i]-tmp_a22)
      +alpha*wgt
	*((particle[i].y-y)*(a22[i]-tmp_a22)
	  +(particle[i].yp-yp)*(a12[i]-tmp_a12));
    h11+=wgt
	*(gamma*a11[i]*a11[i]+beta*a21[i]*a21[i]
	  +2.0*alpha*a11[i]*a21[i]);
    h22+=wgt
	*(gamma*a12[i]*a12[i]+beta*a22[i]*a22[i]
	  +2.0*alpha*a12[i]*a22[i]);
    h12+=wgt
	*(gamma*a11[i]*a12[i]+beta*a21[i]*a22[i]
	  +alpha*(a12[i]*a21[i]+a11[i]*a22[i]));
  }
  h11-=gamma*tmp_a11*tmp_a11+beta*tmp_a21*tmp_a21+2.0*alpha*tmp_a11*tmp_a21;
  h22-=gamma*tmp_a12*tmp_a12+beta*tmp_a22*tmp_a22+2.0*alpha*tmp_a12*tmp_a22;
  h12-=gamma*tmp_a11*tmp_a12+beta*tmp_a21*tmp_a22
    +alpha*(tmp_a12*tmp_a21+tmp_a11*tmp_a22);
  s[0]=-(h22*k1-h12*k2)/(h11*h22-h12*h12);
  s[1]=-(h11*k2-h12*k1)/(h11*h22-h12*h12);
  //  printf("found %g %g\n",s[0],s[1]);
  bump_minimise(bunch,bump,s);
  //  printf("found> %g %g\n",s[0],s[1]);
}

void bunch_emitt_plot(BEAM *bunch,char *name)
{
  FILE *file;
  int i;

  file=fopen(name,"w");
  for (i=0;i<bunch->slices;i++){
    fprintf(file,"%d %g %g %g\n",i,sqrt(bunch->sigma[i].r11),
	    sqrt(bunch->sigma[i].r22),
	    sqrt(bunch->sigma[i].r11*bunch->sigma[i].r22
		 -bunch->sigma[i].r12*bunch->sigma[i].r12)/EMASS*
	    bunch->particle[i].energy);
  }
  fclose(file);
}

void calico_compare(BEAMLINE *beamline,BEAM *bunch0)
{
  BEAM *b;
  int i;
  FILE *file;
  calico_store(beamline);
  b=bunch_remake(bunch0);
  beam_copy(bunch0,b);
  file=fopen("emitt.line","w");
  for (i=0;i<beamline->n_elements;i++){
    beamline->element[i]->track(b);
    if (i>0){
      if ((beamline->element[i-1]->is_quad())
       ||(beamline->element[i-1]->is_quadbpm())){
	fprintf(file,"%g %g\n",emitt_y(b),bunch_get_offset_y(b));
      }
    }
  }
  fclose(file);
  beam_delete(b);
}

void calico_store(BEAMLINE *beamline)
{
  int i;
  FILE *cav_file,*bpm_file,*quad_file;

  cav_file=fopen("../calico/dxysa.data","w");
  bpm_file=fopen("../calico/dxyspu.data","w");
  quad_file=fopen("tmp.quad","w");

  for (i=0;i<beamline->n_elements;i++){
    ELEMENT *element=beamline->element[i];
    if (element->is_quad())
      fprintf(quad_file,"%g %g %g\n",0.0,beamline->element[i]->offset.y,0.0);
    else if (element->is_drift() || element->is_bpm())
      fprintf(bpm_file,"%g %g\n",0.0,beamline->element[i]->offset.y*1e-6);
    else if (element->is_cavity() || element->is_cavity_pets())
      fprintf(cav_file,"%g %g\n",0.0,beamline->element[i]->offset.y*1e-6);
  }

  fclose(cav_file);
  fclose(bpm_file);
  fclose(quad_file);
}

void free_bin_fill_drive(BEAMLINE *beamline,BEAM *bunch0,BEAM *workbunch,
		    BIN **bin,int bin_number)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<bin_number;i++){

    bunch_track(beamline,bunch0,ipos,bin[i]->start);
    bin_response_drive(beamline,bunch0,workbunch,0,bin[i]);
    
    bin_finish(bin[i],1);
    ipos=bin[i]->start;
  }
}

void free_correct_drive(BEAMLINE *beamline,BIN **bin,int bin_number,
			BEAM *bunch0,BEAM *workbunch,int do_emitt)
{
  int i,ipos;
  ipos=0;
  if (do_emitt){
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt){
      bunch_track_emitt(beamline,bunch0,ipos,bin[i]->start);
    }
    else{
      bunch_track(beamline,bunch0,ipos,bin[i]->start);
    }
    bunch_copy_0(bunch0,workbunch);
    bin_measure_drive(beamline,workbunch,0,bin[i]);
    
    bin_correct(beamline,1,bin[i]);
    
    ipos=bin[i]->start;
  }
  if (do_emitt){
    bunch_track_emitt(beamline,bunch0,ipos,beamline->n_elements);
    bunch_track_emitt_end(bunch0);
  }
  else{
    bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  }
}

void
measured_bin_fill(BEAMLINE *beamline,double *quad0,double *quad1,double *quad2,
		  BEAM *b0,BEAM *b1,BEAM *b2,BEAM *workbunch,
		  BIN **bin,int bin_number)
{
  int i,ipos;
  ipos=0;

  //  wakefield_init(1,0);
  for (i=0;i<bin_number;i++){

    quadrupoles_set(beamline,quad0);
    bunch_track_0(beamline,b0,ipos,bin[i]->start);
    bin_response(beamline,b0,workbunch,0,bin[i]);
    
    quadrupoles_set(beamline,quad1);
    bunch_track_0(beamline,b1,ipos,bin[i]->start);
    bin_response(beamline,b1,workbunch,1,bin[i]);

    quadrupoles_set(beamline,quad2);
    bunch_track_0(beamline,b2,ipos,bin[i]->start);
    bin_response(beamline,b2,workbunch,2,bin[i]);
    
    bin_finish(bin[i],1);
    ipos=bin[i]->start;
  }
  //  wakefield_init(1,1);
}

void
measured_bin_fill(BEAMLINE *beamline,double *quad0,double *quad1,double *quad2,
		  BEAM *b0,BEAM *b1,BEAM *b2,BEAM *workbunch,
		  BIN **bin,int bin_number)
{
  measured_bin_fill_new(beamline,quad0,quad1,quad2,b0,b1,b2,workbunch,
  			NULL,NULL,NULL,bin,bin_number,0.8,0.8);
}

void measured_bin_fill_2(BEAMLINE *beamline,double *quad0,double *quad1,
		    double *quad2,BEAM *b0,BEAM *b1,BEAM *b2,
		    BEAM *workbunch,BIN **bin,int bin_number)
{
  int i,ipos;
  ipos=0;
  for (i=0;i<bin_number;i++){

    quadrupoles_set(beamline,quad0);
    bunch_track(beamline,b0,ipos,bin[i]->start);
    bin_response(beamline,b0,workbunch,0,bin[i]);
    
    quadrupoles_set(beamline,quad1);
    bunch_track(beamline,b1,ipos,bin[i]->start);
    bin_response(beamline,b1,workbunch,1,bin[i]);

    quadrupoles_set(beamline,quad2);
    bunch_track(beamline,b2,ipos,bin[i]->start);
    bin_response(beamline,b2,workbunch,2,bin[i]);
    
    bin_finish_2(bin[i],0);
    ipos=bin[i]->start;
  }
}

/*
  This is the old version
 */

void
old_measured_bin_fill_new(BEAMLINE *beamline,double *quad0,double *quad1,
			  double *quad2,
			  BEAM *b0,BEAM *b1,BEAM *b2,BEAM *workbunch,
			  BIN **bin,int bin_number,double g1,double g2)
{
  int i,ipos;
  ipos=0;
  double tmp;

  tmp=errors.bpm_resolution;
  errors.bpm_resolution=0.0;
  for (i=0;i<bin_number;i++){

    quadrupoles_set(beamline,quad0);
    bunch_track_0(beamline,b0,ipos,bin[i]->start);
    bin_response(beamline,b0,workbunch,0,bin[i]);
    
    quadrupoles_set(beamline,quad1);
    bunch_track_gradient_0(beamline,b1,ipos,bin[i]->start,g1);
    bin_response(beamline,b1,workbunch,1,bin[i]);

    if (b2) {
      quadrupoles_set(beamline,quad2);
      bunch_track_gradient_0(beamline,b2,ipos,bin[i]->start,g2);
      bin_response(beamline,b2,workbunch,2,bin[i]);
    }
    else {
      bin_response(beamline,b1,workbunch,2,bin[i]);
    }
    
    bin_finish(bin[i],1);
    ipos=bin[i]->start;
  }
  errors.bpm_resolution=tmp;
}

void
measured_correct(BEAMLINE *beamline,
		 double *quad0,double *quad1,double *quad2,
		 BIN **bin,int bin_number,BEAM *b0,BEAM *b1,BEAM *b2,
		 BEAM *workbunch,int do_emitt,double jitter)
{
  int i,ipos,is,np;
  double tmp1,tmp2,rat;

  ipos=0;
  np=b0->slices/2;
  quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt) {
      bunch_track_emitt(beamline,b0,ipos,bin[i]->start);
    }
    else {
      bunch_track_0(beamline,b0,ipos,bin[i]->start);
    }

    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad1);
    }

    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad1);
    }
    bunch_track_0(beamline,b1,ipos,bin[i]->start);
    quadrupoles_bin_set(beamline,bin[i],quad1);
    bunch_copy_0(b1,workbunch);
    rat=sqrt(b0->sigma[np].r22/b0->sigma[np].r11);
    tmp1=jitter*gasdev();
    tmp2=jitter*gasdev()*rat;
    for (is=0;is<b1->slices;is++){
      workbunch->particle[is].y+=tmp1;
      workbunch->particle[is].yp+=tmp2;
    }
    bin_measure(beamline,workbunch,1,bin[i]);
    
    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad2);
    }
    bunch_track_0(beamline,b2,ipos,bin[i]->start);
    quadrupoles_bin_set(beamline,bin[i],quad2);
    bunch_copy_0(b2,workbunch);
    tmp1=jitter*gasdev();
    tmp2=jitter*gasdev()*rat;
    for (is=0;is<b2->slices;is++){
      workbunch->particle[is].y+=tmp1;
      workbunch->particle[is].yp+=tmp2;
    }
    bin_measure(beamline,workbunch,2,bin[i]);
    
    bunch_copy_0(b0,workbunch);
    tmp1=jitter*gasdev();
    tmp2=jitter*gasdev()*rat;
    for (is=0;is<b0->slices;is++){
      workbunch->particle[is].y+=tmp1;
      workbunch->particle[is].yp+=tmp2;
    }
    quadrupoles_bin_set(beamline,bin[i],quad0);
    bin_measure(beamline,workbunch,0,bin[i]);

    bin_correct(beamline,1,bin[i]);
//    quadrupoles_bin_set(beamline,bin[i],quad0);
    
    ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt(beamline,b0,ipos,beamline->n_elements);
    bunch_track_emitt_end(b0);
  }
  else {
    bunch_track_0(beamline,b0,ipos,beamline->n_elements);
  }
}

void measured_correct_2(BEAMLINE *beamline,double *quad0,double *quad1,
		   double *quad2,BIN **bin,int bin_number,BEAM *b0,BEAM *b1,
		   BEAM *b2,BEAM *workbunch,int do_emitt)
{
  int i,ipos;
    ipos=0;
    quadrupoles_set(beamline,quad0);
    for (i=0;i<bin_number;i++){

      bunch_track(beamline,b0,ipos,bin[i]->start);
      if (do_emitt){
	emitt_store(i,b0);
      }
      bunch_copy_0(b0,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);

      if (i>0){
	quadrupoles_bin_set(beamline,bin[i-1],quad1);
      }
      bunch_track_0(beamline,b1,ipos,bin[i]->start);
      quadrupoles_bin_set(beamline,bin[i],quad1);
      bunch_copy_0(b1,workbunch);
      bin_measure(beamline,workbunch,1,bin[i]);

      if (i>0){
	quadrupoles_bin_set(beamline,bin[i-1],quad2);
      }
      bunch_track_0(beamline,b2,ipos,bin[i]->start);
      quadrupoles_bin_set(beamline,bin[i],quad2);
      bunch_copy_0(b2,workbunch);
      bin_measure(beamline,workbunch,2,bin[i]);

      bin_correct_2(beamline,1,bin[i]);
      quadrupoles_bin_set(beamline,bin[i],quad0);

      ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  bunch_track(beamline,b0,ipos,beamline->n_elements);
  if (do_emitt){
    emitt_store(bin_number,b0);
  }
}

void
measured_correct_old(BEAMLINE *beamline,
		     double *quad0,double *quad1,double *quad2,
		     BIN **bin,int bin_number,BEAM *b0,BEAM *b1,BEAM *b2,
		     BEAM *workbunch,int do_emitt)
{
  int i,ipos;

  ipos=0;
  quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt) {
      bunch_track_emitt(beamline,b0,ipos,bin[i]->start);
    }
    else {
      bunch_track_0(beamline,b0,ipos,bin[i]->start);
    }

    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad1);
    }

    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad1);
    }
    bunch_track_0(beamline,b1,ipos,bin[i]->start);
    quadrupoles_bin_set(beamline,bin[i],quad1);
    bunch_copy_0(b1,workbunch);
    bin_measure(beamline,workbunch,1,bin[i]);
    
    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad2);
    }
    bunch_track_0(beamline,b2,ipos,bin[i]->start);
    quadrupoles_bin_set(beamline,bin[i],quad2);
    bunch_copy_0(b2,workbunch);
    bin_measure(beamline,workbunch,2,bin[i]);
    
    bunch_copy_0(b0,workbunch);
    quadrupoles_bin_set(beamline,bin[i],quad0);
    bin_measure(beamline,workbunch,0,bin[i]);

    bin_correct(beamline,1,bin[i]);
//    quadrupoles_bin_set(beamline,bin[i],quad0);
    
    ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt(beamline,b0,ipos,beamline->n_elements);
    bunch_track_emitt_end(b0);
  }
  else {
    bunch_track_0(beamline,b0,ipos,beamline->n_elements);
  }
}

static struct{
  BEAMLINE *beamline;
  BEAM *bunch0;
  void (*survey)(BEAMLINE*);
  BIN **bin;
  int nbin;
} optimize_data;

void optimize_init(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
	      void (*survey)(BEAMLINE*))
{
  optimize_data.beamline=beamline;
  optimize_data.bunch0=bunch0;
  optimize_data.survey=survey;
  optimize_data.bin=bin;
  optimize_data.nbin=nbin;
}

double optimize_phase(double x[])
{
  double phase[4],estep[4],s;
  phase[0]=x[1];
  phase[1]=x[2];
  phase[2]=x[3];
  phase[3]=30.0;
  estep[0]=30.0;
  estep[1]=x[4];
  s=((500.0-9.0)*sin(12.0*PI/180.0)-(estep[0]-9.0)*sin(phase[0]*PI/180.0)-
     (estep[1]-estep[0])*sin(phase[1]*PI/180.0))/(500.0-estep[1]);
  s=(s-sin(phase[3]*PI/180.0))
    /(sin(phase[2]*PI/180.0)-sin(phase[3]*PI/180.0));
  estep[2]=estep[1]+s*(500.0-estep[1]);
  estep[3]=10000.0;
  s=0.0;
  if (estep[1]<estep[0]) s=(estep[0]-estep[1])*1e1;
  if (estep[2]<estep[1]) s=(estep[1]-estep[2])*1e1;
  if (estep[3]<estep[2]) s=(estep[2]-estep[3])*1e1;
  if (fabs(phase[0])>31.0) s=(fabs(phase[0])-31.0)*1e1;
  if (fabs(phase[1])>31.0) s=(fabs(phase[1])-31.0)*1e1;
  if (fabs(phase[2])>31.0) s=(fabs(phase[2])-31.0)*1e1;
  if (fabs(phase[3])>31.0) s=(fabs(phase[3])-31.0)*1e1;
  if (s<=0.01){
    lattice_rescale(optimize_data.beamline,phase,estep,9.0);
    int n1 = optimize_data.bunch0->slices/2;
    int n2 = n1;
    twiss_plot(optimize_data.beamline,optimize_data.bunch0,"twiss.dat",n1,n2,&fprint_twiss);
    /*
    s=check_autophase(optimize_data.beamline,optimize_data.bunch0,NULL);
    */
    rndmst5(12,34,56,78);
    s=test_simple_correction(optimize_data.beamline,optimize_data.bin,
			     optimize_data.nbin,optimize_data.bunch0,NULL,30,
			     optimize_data.survey,"emitt.dat");
  }
  printf("%g %g %g %g\n",phase[0],phase[1],phase[2],phase[3]);
  printf("%g %g %g %g\n",estep[0],estep[1],estep[2],estep[3]);
  printf("s= %g\n",s);
  return s;
}

void print_transf(double transf[])
{
  int i,j,n=31;
  for (j=0;j<2*n;j++){
    for (i=0;i<2*n;i++){
      printf("%g ",*transf++);
    }
    printf("\n");
  }
}

void quadrupoles_write_file(BEAMLINE *beamline,char name[])
{
  FILE *file;
  int i;
  file=fopen(name,"w");
  for (i=0;i<beamline->n_quad;i++){
    fprintf(file,"%d %g\n",i,beamline->quad[i]->offset.y);
  }
  fclose(file);
}

/* runs several beams through the linac with a RMS position jitter of sigma_y
and a relative charge jitter of sigma_c */

double run_beam_jitter(BEAMLINE *beamline,BEAM *bunch0,BEAM *tb,int iter,
		       double sigma_y,double sigma_c,double sigma_phase)
{
  int i,j;
  double off,tmp,sum=0.0,factor;
  beam_copy(bunch0,tb);
  tb=make_bunch(beamline,"beam.dat");
  for (i=0;i<iter;i++){
    beam_copy(bunch0,tb);
    factor=1.0+sigma_c*gasdev();
    tb->factor=factor;
    bunch_shift_phase(beamline,tb,sigma_phase*gasdev());
    off=sigma_y*gasdev();
    for (j=0;j<tb->slices;j++){
      tb->particle[j].y+=off;
    }
    bunch_track(beamline,tb,0,beamline->n_elements);
    tmp=emitt_y(tb);
    printf("%g\n",tmp);
    sum+=tmp;
  }
  return sum/(double)iter;
}

void simple_correct_dipole(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		      BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
    bin_correct_dipole(beamline,0,bin[i]);
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
}

void simple_correct_emitt_2(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		       BEAM *workbunch)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    emitt_store(i,bunch);

    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    bin_correct(beamline,0,bin[i]);

//    bunch_copy_0(bunch,workbunch);
//    bin_measure(beamline,workbunch,0,bin[i]);
//    bin_correct(beamline,0,bin[i]);

    ipos=bin[i]->start;
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
  emitt_store(nbin,bunch);
}

void
simple_correct_emitt_indep_old(BEAMLINE *beamline,BIN **bin,int nbin,
			       BEAM *bunch,BEAM *workbunch)
{
  int ipos,i,j;
  ipos=0;


  bunch_track_emitt_start();

  //  bunch_track_emitt(beamline,bunch,0,beamline->n_elements);
  for (i=0;i<nbin;i++){
    bunch_track_emitt(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]); 
    ipos=bin[i]->start;
  }

  for (j=0;j<nbin;j++){
    bin_correct(beamline,0,bin[j]);
  }

  bunch_track_emitt(beamline,bunch,ipos,beamline->n_elements);
  bunch_track_emitt_end(bunch);

}

void simple_correct_interleave(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			  BEAM *workbunch)
{
  int ipos,i;
  double step;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
    /*
    bin_measure_zero(beamline,workbunch,1,bin[i]);
    bin_measure_zero(beamline,workbunch,2,bin[i]);
    */
    ipos=bin[i]->start;
    step=-bin[i]->b0[1]/bin[i]->a0[3];
    element_add_offset(beamline->element[bin[i]->start],step,0.0);
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
}

/* Does an NLC type correction of the sectors but moves only the BPMs,
 this is followed by a one-to-one correction. */

void simple_correct_nlc_2(BEAMLINE *beamline,BIN **bin,int nbin,BIN **bin2,
			  int /*nbin2*/,BEAM *bunch,BEAM *workbunch)
{
  int ipos,i,j;
  double tmp_pwgt;
  ipos=0;
  j=0;
  tmp_pwgt=corr.pwgt;
  for (i=0;i<nbin;i++){
    while(bin2[j]->stop<bin[i]->start){
      corr.pwgt=0.0;
      bunch_track(beamline,bunch,ipos,bin2[j]->start);
      bunch_copy_0(bunch,workbunch);
      bin_measure(beamline,workbunch,0,bin2[j]);
      bin_correct(beamline,0,bin2[j]);
      ipos=bin2[j]->start;
      j++;
    }
    bunch_copy_0(bunch,workbunch);
    bunch_track(beamline,workbunch,ipos,bin[i]->start);
    corr.pwgt=tmp_pwgt;
    bin_measure(beamline,workbunch,0,bin[i]);
    ipos=bin[i]->start;
    bin_correct_nlc_2(beamline,0,bin[i]);
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
}

void simple_correct_nlc_n(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		     BEAM *workbunch)
{
  int ipos,i,j;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    for (j=0;j<10;j++){
      bunch_copy_0(bunch,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
//errors.quad_move_res=0.5;
      bin_correct_nlc(beamline,0,bin[i]); 
    }
    ipos=bin[i]->start;

    /*
    bunch_copy_0(bunch,workbunch);
    bin_measure(beamline,workbunch,0,bin[i]);
//errors.quad_move_res=0.0;
    bin_correct_nlc(beamline,0,bin[i]); 
    */
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
}

void survey_init_0(double quad,double bpm,double cav,double drift,double roll)
{
  survey_errors.dipole_error=0.0;
  survey_init_x(quad,bpm,cav,drift,roll);
  survey_errors.quad_error=quad;
  survey_errors.quad_roll=roll*1e-6;
  survey_errors.bpm_error=bpm;
  survey_errors.cav_error=cav;
  survey_errors.drift_error=drift;
  survey_errors.piece_error=0.0;
  survey_errors.piece_error_angle=0.0;
  survey_errors.quad_error_angle=0.0;
  survey_errors.cav_error_angle=0.0;
  survey_errors.bpm_error_angle=0.0;
  survey_errors.drift_error_angle=0.0;
  survey_errors.sbend.y=0.0;
  survey_errors.sbend.yp=0.0;
}

void survey_init_x(double quad,double bpm,double cav,double drift,double roll)
{
  survey_errors.quad_error_x=quad;
  survey_errors.bpm_error_x=bpm;
  survey_errors.cav_error_x=cav;
  survey_errors.drift_error_x=drift;
  survey_errors.quad_roll=roll*(double)1e-6;
  survey_errors.cav_error_realign=0.0;
  survey_errors.cav_error_realign_x=0.0;
  survey_errors.cav_error_dipole_x=0.0;
  survey_errors.cav_error_dipole_y=0.0;
  survey_errors.piece_error_x=0.0;
  survey_errors.piece_error_angle_x=0.0;
  survey_errors.quad_error_angle_x=0.0;
  survey_errors.cav_error_angle_x=0.0;
  survey_errors.bpm_error_angle_x=0.0;
  survey_errors.drift_error_angle_x=0.0;
  survey_errors.sbend.x=0.0;
  survey_errors.sbend.xp=0.0;
  survey_errors.sbend.roll=0.0;
}

void test_atl(BEAMLINE *beamline,BEAM *bunch,BEAM *workbunch)
{
  int i,j,k,nk=100;
  double e[1000],e_a[1000],emitt0_a,esum_a,esum2_a,
    esum,esum2,emitt0,lumi,lumi_a,t;
  FILE *file;

  k=0;
  beam_copy(bunch,workbunch);
  bunch_track_0(beamline,workbunch,0,beamline->n_elements);
  emitt0=emitt_y(workbunch);
  emitt0_a=emitt_y_axis(workbunch);
  t=1.0;
  file=fopen("atl.dat","w");
  fclose(file);
  for (i=0;i<1;i++){
    esum=0.0;
    esum2=0.0;
    esum_a=0.0;
    esum2_a=0.0;
    for (k=0;k<nk;k++){
      beamline_set_zero(beamline);
      beamline_move_atl(beamline,0.5e-6,512.0);
      beam_copy(bunch,workbunch);
      bunch_track(beamline,workbunch,0,beamline->n_elements);
      e[k]=emitt_y(workbunch);
      e_a[k]=emitt_y_axis(workbunch);
      esum+=e[k];
      esum2+=e[k]*e[k];
      esum_a+=e_a[k];
      esum2_a+=e_a[k]*e_a[k];
      printf("%g %g\n",e[k],e_a[k]);
    }
    lumi=0.0;
    lumi_a=0.0;
    for (j=0;j<nk;j++){
      for (k=0;k<nk;k++){
	lumi+=sqrt(2.0*emitt0/(e[j]+e[k]));
	lumi_a+=sqrt(2.0*emitt0_a/(e_a[j]+e_a[k]));
      }
    }
    file=fopen("atl.dat","a");
    fprintf(file,"%g %g %g %g %g %g %g\n",t,esum,esum2-esum*esum/100.0,lumi,
	    esum_a,esum2_a-esum_a*esum_a/100.0,lumi_a);
    fclose(file);
    t*=sqrt(2.0);
  }
}

double test_atl_2(BEAMLINE *beamline,BIN ** /*bin*/,int nbin,BEAM *bunch,
		  int niter,double time,char *name)
{
  int i,do_emitt;
  BEAM *workbunch;
  double esum=0.0,e,esum_tail=0.0;

  if (name!=NULL){
    do_emitt=1;
    printf("emittance is plotted with %d points\n",nbin);
   }
  else{
    do_emitt=0;
  }

  workbunch=bunch_remake(bunch);
  emitt_store_init(beamline->n_quad);
  for (i=0;i<niter;i++){
    beamline_set_zero(beamline);
    beamline_move_atl(beamline,0.5e-6,time);
    beam_copy(bunch,workbunch);
    if (do_emitt){
      bunch_track_line_emitt_new(beamline,workbunch);
    }
    else{
      bunch_track(beamline,workbunch,0,beamline->n_elements);
    }
    e=emitt_y(workbunch);
    esum+=e;
    esum_tail+=emitt_y_tail(bunch);
    printf("%d %g %g\n",i,esum/(double)(i+1),esum_tail/(double)(i+1));
  }
  if (do_emitt){
    emitt_print(name);
    emitt_store_delete();
  }
  return esum/(double)(niter);
}

void test_feedback(BEAMLINE *beamline,double /*quad0*/[],double /*quad1*/[],double /*quad2*/[],
		   BEAM *bunch0,BEAM * /*bunch1*/,BEAM * /*bunch2*/,
		   int istat,int niter,double t,void (* /*survey*/)(BEAMLINE*),
		   char *name)
{
  int dist;
  BIN **bin,**binc;
  BEAM *tb,*workbunch;
  int nbin,nbinc,i,do_emitt;
  double emitt0,esum;
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
  }
  bin=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  binc=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);

  tb=bunch_remake(bunch0);
  //  tb1=bunch_remake(bunch1);
  //  tb2=bunch_remake(bunch2);
  workbunch=bunch_remake(bunch0);

  beamline_bin_divide(beamline,2,0,bin,&nbin);

  emitt_store_init(nbin);

  beamline_bin_divide(beamline,12,6,binc,&nbinc);

  beam_copy(bunch0,tb);

  corr.pwgt=0.0;
corr.w=0.0;
corr.w0=1.0;
  simple_bin_fill(beamline,bin,nbin,tb,workbunch);
printf("hier\n");

/*
corr.w=1.0;
corr.w0=0.0;
  beam_copy(bunch0,tb);
  beam_copy(bunch1,tb1);
  beam_copy(bunch2,tb2);
printf("%g %g\n",tb1->factor,tb2->factor);
  measured_bin_fill(beamline,quad0,quad1,quad2,tb,tb1,tb2,workbunch,binc,
		    nbinc);
		    */
  esum=0.0;
  if (istat>0){
    dist=nbin/(istat+1);
  }
  else{
    dist=nbin+1;
  }
  for (i=0;i<niter;i++){
    beamline_set_zero(beamline);
    /*
    errors.bpm_resolution=0.1;
    errors.quad_move_res=0.5;

    beam_copy(bunch0,tb);
    beam_copy(bunch1,tb1);
    beam_copy(bunch2,tb2);
    survey(beamline);
corr.w=1.0;
corr.w0=0.0;
    measured_correct(beamline,quad0,quad1,quad2,binc,nbinc,tb,tb1,tb2,
		     workbunch,0,0.0);

    errors.bpm_resolution=0.0;
    errors.quad_move_res=0.0;
    beam_copy(bunch0,tb);
    bpm_reset(beamline,tb);
    */

    beam_copy(bunch0,tb);
    beamline_move_atl(beamline,0.5e-6,t);
corr.w=0.0;
corr.w0=1.0;
    feedback_correct(beamline,bin,nbin,tb,workbunch,dist);

    emitt0=emitt_y(tb);
    esum+=emitt0;
    printf("%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  if (do_emitt){
    emitt_print(name);
  }
  free(bin);
}

void test_feedback_2(BEAMLINE *beamline,BEAM *bunch0,char *stat,int niter,
		char *name)
{
  int do_jitter=0,loop[100],nloop=0;
  double jitter=1.3;
  BIN **bin;
  BEAM *tb,*tb1,*workbunch;
  FILE *file;
  int nbin,i,j,do_emitt;
  double emitt0,esum;
  char buffer[100],*point;
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
  }
  file=fopen(stat,"r");
  while(point=fgets(buffer,100,file)){
    loop[nloop++]=strtol(point,&point,10);
  }
  fclose(file);
  bin=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  tb=bunch_remake(bunch0);
  if (do_jitter){
    tb1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  beamline_bin_divide(beamline,2,0,bin,&nbin);
  emitt_store_init(nbin);
  beam_copy(bunch0,tb);
  simple_bin_fill(beamline,bin,nbin,tb,workbunch);
  esum=0.0;
  corr.pwgt=0.0;
  errors.bpm_resolution=0.0;
  errors.quad_move_res=0.0;
  errors.quad_step_size=0.0;
  for (i=0;i<nloop;i++){
    printf("set %d\n",bin[loop[i]]->bpm[0]);
    beamline->element[bin[loop[i]]->bpm[0]]->type=FDBK;
  }
  for (i=0;i<niter;i++){
    beam_copy(bunch0,tb);
    if (do_jitter){
      beam_copy(bunch0,tb1);
      for (j=0;j<bunch0->slices;j++){
	tb1->particle[j].y=jitter;
      }
    }
    beamline_set_zero(beamline);
    beamline_move_atl(beamline,0.5e-6,1800.0);
    if (do_jitter){
      / *      feedback_correct_jitter_2(beamline,bin,nbin,tb,tb1,workbunch,dist);**** /
    }
    else{
      / *      feedback_correct_2(beamline,bin,nbin,tb,workbunch,loop,nloop);**   /
      for(j=0;j<beamline->n_elements;j++){
	element_step_fdbk(beamline->element[j],tb);
      }
    }
    emitt0=emitt_y(tb);
    esum+=emitt0;
    printf("%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  if (do_emitt){
    emitt_print(name);
  }
  free(bin);
}

void test_feedback_3(BEAMLINE *beamline,BEAM *bunch0,int istat,int distribute,
		int niter,double t,char *name)
{
  int ndist;
  BIN **bin;
  BEAM *tb,*workbunch;
  int nbin,i,do_emitt;
  double emitt0,esum,dist;
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
  }
  bin=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);

  tb=bunch_remake(bunch0);
  workbunch=bunch_remake(bunch0);

  beamline_bin_divide(beamline,2,0,bin,&nbin);
  divide_length(beamline,bin,nbin);

  emitt_store_init(nbin);

  beam_copy(bunch0,tb);

  simple_bin_fill(beamline,bin,nbin,tb,workbunch);

  esum=0.0;
  if (istat>0){
    dist=bin[nbin-1]->s_pos/(istat+1);
    ndist=nbin/(istat+1);
  }
  else{
    dist=bin[nbin-1]->s_pos+1e6;
    ndist=nbin+1;
  }
  corr.w=0.0;
  corr.w0=1.0;
  corr.pwgt=0.0;
  for (i=0;i<niter;i++){
    beamline_set_zero(beamline);

    beam_copy(bunch0,tb);
    beamline_move_atl(beamline,0.5e-6,t);
    if (distribute){
      feedback_correct_3(beamline,bin,nbin,tb,workbunch,dist);
    }
    else{
      feedback_correct(beamline,bin,nbin,tb,workbunch,ndist);
    }

    emitt0=emitt_y(tb);
    esum+=emitt0;
    printf("%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  if (do_emitt){
    emitt_print(name);
  }
  free(bin);
}

void test_free_correction_2(BEAMLINE *beamline,double quad0[],
		       double quad1[],double quad2[],BIN **bin,
		       int nbin,BEAM *bunch0,int niter,
		       void (*survey)(BEAMLINE*),char *name,
		       BIN **bin_emitt,int nbin_emitt)
{
  BEAM *tb0,*tb0_1,*workbunch;
  int i,j,do_jitter=0;
  double esum=0.0,emitt0,esum2=0.0,offset_y=1.3;

  emitt_store_init(nbin_emitt);
  tb0=bunch_remake(bunch0);
  if (do_jitter){
    tb0_1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  bunch_copy_0(bunch0,tb0);
  free_bin_fill(beamline,quad0,quad1,quad2,tb0,workbunch,bin,
		nbin);
  beam_copy(bunch0,tb0);
  bunch_track(beamline,tb0,0,beamline->n_elements);
  for (i=0;i<niter;i++){
    survey(beamline);
    bunch_copy_0(bunch0,tb0);
    if (do_jitter){
      bunch_copy_0(bunch0,tb0_1);
      for (j=0;j<tb0->slices;j++){
	tb0_1->particle[j].y+=offset_y;
      }
      free_correct_jitter(beamline,quad0,quad1,quad2,bin,nbin,tb0,tb0_1,
			  workbunch,0);
    }
    else{
      free_correct(beamline,quad0,quad1,quad2,bin,nbin,tb0,workbunch,0);
    }
    beam_copy(bunch0,tb0);
    for (j=0;j<tb0->slices;j++){
      tb0->field->de[j]=-0.9*9.0/320.0*(double)j/(double)(tb0->slices-1);
    }
    quadrupoles_set(beamline,quad1);
    bunch_track_line_emitt(beamline,bin_emitt,nbin_emitt,tb0);
    emitt0=emitt_y(tb0);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    printf("%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  printf("emitt= %g +/- %g\n",esum,
	 sqrt(max(0.0,esum2-esum*esum)/(double)niter));
  beam_delete(workbunch);
  beam_delete(tb0);
  if (do_jitter){
    beam_delete(tb0_1);
  }
  emitt_print(name);
  emitt_store_delete();
}

void test_measured_correction_2(BEAMLINE *beamline,double quad0[],
				double quad1[],
				double quad2[],double /*quad3*/[],BIN **bin,
				int nbin,
				BEAM *bunch0,BEAM *bunch1,BEAM *bunch2,
				int niter,
				void (*survey)(BEAMLINE*),char *name,
				BIN **bin_emitt,int nbin_emitt)
{
  BEAM *tb0,*tb1,*tb2,*workbunch;
  int i,j;
  double esum=0.0,emitt0,esum2=0.0;

  emitt_store_init(nbin_emitt);
  tb0=bunch_remake(bunch0);
  tb1=bunch_remake(bunch1);
  tb2=bunch_remake(bunch2);

  workbunch=bunch_remake(bunch0);
  bunch_copy_0(bunch0,tb0);
  bunch_copy_0(bunch1,tb1);
  bunch_copy_0(bunch2,tb2);
  measured_bin_fill_2(beamline,quad0,quad1,quad2,tb0,tb1,tb2,workbunch,bin,
		      nbin);
  for (i=0;i<niter;i++){
    survey(beamline);
    bunch_copy_0(bunch0,tb0);
    bunch_copy_0(bunch1,tb1);
    bunch_copy_0(bunch2,tb2);
    measured_correct_2(beamline,quad0,quad1,quad2,bin,nbin,tb0,tb1,tb2,
		       workbunch,0);
    for (j=0;j<beamline->n_quad;j++){
      beamline->quad[j]->offset.y+=0.0*gasdev();
    }
    /*    quadrupoles_print(beamline,"quad.dat");*/
    beam_copy(bunch0,tb0);
    for (j=0;j<tb0->slices;j++){
      tb0->field->de[j]=-0.9*9.0/320.0*(double)j/(double)(tb0->slices-1);
    }
    quadrupoles_set(beamline,quad1);
    bunch_track_line_emitt(beamline,bin_emitt,nbin_emitt,tb0);
    emitt0=emitt_y(tb0);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    printf("%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  printf("emitt= %g +/- %g\n",esum,
	 sqrt(max(0.0,esum2-esum*esum)/(double)niter));
  beam_delete(workbunch);
  beam_delete(tb0);
  beam_delete(tb1);
  beam_delete(tb2);
  emitt_print(name);
  emitt_store_delete();
}

void test_measured_spectrum(BEAMLINE *beamline,double quad0[],double quad1[],
		       double quad2[],BIN **bin,int nbin,BEAM *bunch0,
		       BEAM *b0,BEAM *b1,BEAM *b2,BEAM *workbunch)
{
  int i,j;
  FILE *file;
  double lambda,fact,a,emitt1,emitt2,emitt0,scale1,scale2;

  file=fopen("spectrum.measured","w");
  a=1.0;
  lambda=1.0;
  fact=pow(2.0,0.02);
  beam_copy(bunch0,b0);
  bunch_track(beamline,b0,0,beamline->n_elements);
  emitt0=emitt_y(b0);
  corr.pwgt=0.0;
  corr.w0=0.0;
  corr.w=1.0;
  errors.bpm_resolution=0.0;
  errors.quad_move_res=0.0;
  errors.quad_step_size=0.0;
  for (i=0;i<500;i++){
    beam_copy(bunch0,b0);
    bunch_copy_0(bunch0,b1);
    b1->factor=0.1;
    bunch_copy_0(bunch0,b2);
    b2->factor=0.1;
    beamline_set_zero(beamline);
    beamline_move_sine(beamline,a,TWOPI/lambda,0.0);
    measured_correct(beamline,quad0,quad1,quad2,bin,nbin,b0,b1,b2,workbunch,0,
		     0.0);
    scale1=emitt_y_scale(b0,1.06);
    emitt1=emitt_y(b0);
    beam_copy(bunch0,b0);
    bunch_copy_0(bunch0,b1);
    b1->factor=0.1;
    bunch_copy_0(bunch0,b2);
    b2->factor=0.1;
    beamline_set_zero(beamline);
    beamline_move_sine(beamline,a,TWOPI/lambda,0.5*PI);
    measured_correct(beamline,quad0,quad1,quad2,bin,nbin,b0,b1,b2,workbunch,0,
		     0.0);
    scale2=emitt_y_scale(b0,1.06);
    for (j=0;j<b0->slices;j++){
      b0->particle[j].y*=scale2;
      b0->particle[j].yp*=scale2;
    }
    emitt2=emitt_y(b0);
    fprintf(file,"%g %g %g %g %g %g %g\n",lambda,emitt1,emitt2,
	    sqrt(0.06*emitt0/(emitt1-emitt0)),
	    sqrt(0.06*emitt0/(emitt2-emitt0)),scale1,scale2);
    placet_printf(INFO,"%g %g %g %g %g\n",lambda,emitt1,emitt2,scale1,scale2);
    lambda*=fact;
  }
  fclose(file);
}

void test_simple_alignment_2(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
			int niter,void (*survey)(BEAMLINE*),char *name,
			double w0,double w1)
{
  BEAM *tb,*tb1,*tbx,*workbunch;
  int i,j,do_emitt,do_jitter=0,nbin_s;
  double esum=0.0,emitt0,esum2=0.0,offset_y=1.3;
  BIN **bin_s;

  bin_s=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide(beamline,1,0,bin_s,&nbin_s);
  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_store_init(nbin_s);
  }
  tb=bunch_remake(bunch0);
  tbx=bunch_remake(bunch0);
  if (do_jitter){
    tb1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
corr.pwgt=w0;
  bunch_copy_0(bunch0,tb);
  simple_bin_fill_nlc(beamline,bin,nbin,tb,workbunch);
corr.pwgt=w1;
  bunch_copy_0(bunch0,tb);
  simple_bin_fill(beamline,bin_s,nbin_s,tb,workbunch);
  if (do_emitt==0) {
    beam_copy(bunch0,tb);
    bunch_track(beamline,tb,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    /*
    beam_copy(bunch0,tb);
    simple_bin_fill(beamline,bin,nbin,tb,workbunch);
    */
    beam_copy(bunch0,tb);

    if (do_emitt){
      beam_copy(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter(beamline,bin,nbin,tb,tb1,workbunch,1);
      }
      else{
	/*	simple_correct_emitt(beamline,bin,nbin,tb,workbunch);*/
	for (j=0;j<1;j++){
	  corr.pwgt=w0;
	  beam_copy(bunch0,tb);
	  simple_correct_nlc_2(beamline,bin,nbin,bin_s,nbin_s,tb,workbunch);
	}
	beam_copy(bunch0,tb);
	bunch_track_line_emitt_new(beamline,tb);
	quadrupoles_print(beamline,"quad.dat");
      }
    }
    else{
      bunch_copy_0(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter_0(beamline,bin,nbin,tb,tb1,workbunch);
      }
      else{
	simple_correct_0(beamline,bin,nbin,tb,workbunch);
      }
    }
    emitt0=emitt_y(tb);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    printf("%d %g %g\n",i,emitt0,esum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  printf("emitt= %g +/- %g\n",esum,
	 sqrt(max(0.0,esum2-esum*esum)/(double)niter));
  if (do_emitt){
    emitt_print(name);
    emitt_store_delete();
  }
  if (do_jitter){
    beam_delete(tb1);
  }
  beam_delete(workbunch);
  beam_delete(tbx);
  beam_delete(tb);
  free(bin_s);
}

double test_simple_correction_2(BEAMLINE *beamline,double quad[],BIN **bin,
				int nbin,BEAM *bunch0,int niter,
				void (*survey)(BEAMLINE*),char *name)
{
  BEAM *tb,*tb1,*workbunch;
  int i,j,do_emitt,do_jitter=0;
  double esum=0.0,emitt0,esum2=0.0,offset_y=1.3,qp,qps,qpsum=0.0;

  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_store_init(nbin);
  }
  tb=bunch_remake(bunch0);
  if (do_jitter){
    tb1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  bunch_copy_0(bunch0,tb);
  simple_bin_fill(beamline,bin,nbin,tb,workbunch);
  if (do_emitt==0) {
    beam_copy(bunch0,tb);
    bunch_track(beamline,tb,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    quadrupoles_set_error(beamline,quad,0.001,2.0);
    bunch_copy_0(bunch0,tb);
    int n1 = tb->slices/2;
    int n2 = n1;
    twiss_plot(beamline,tb,"twiss.tmp",n1,n2,&fprint_twiss);
//    beam_copy(bunch0,tb);
//    simple_bin_fill(beamline,bin,nbin,tb,workbunch);

    beam_copy(bunch0,tb);

    if (do_emitt){
      beam_copy(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter(beamline,bin,nbin,tb,tb1,workbunch,1);
      }
      else{
//	simple_correct_emitt_2(beamline,bin,nbin,tb,workbunch);
	simple_correct_emitt(beamline,bin,nbin,tb,workbunch);
	measure_quadrupoles(beamline,&qp,&qps);
	qpsum+=qp;
      }
    }
    else{
      bunch_copy_0(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter_0(beamline,bin,nbin,tb,tb1,workbunch);
      }
      else{
	simple_correct_0(beamline,bin,nbin,tb,workbunch);
      }
    }
    emitt0=emitt_y(tb);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    printf("%d %g %g %g\n",i,emitt0,esum/(double)(i+1),qpsum/(double)(i+1));
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  printf("emitt= %g +/- %g\n",esum,
	 sqrt(max(0.0,esum2-esum*esum)/(double)niter));
  if (do_emitt){
    emitt_print(name);
    emitt_store_delete();
  }
  if (do_jitter){
    beam_delete(tb1);
  }
  beam_delete(workbunch);
  beam_delete(tb);
  return esum;
}

double test_simple_correction_both(BEAMLINE *beamline,BEAM *bunch0,int niter,
				   void (*survey)(BEAMLINE*),char *name)
{
  BEAM *tb,*tb1,*workbunch;
  int i,j,do_emitt,do_jitter=0;
  double esum=0.0,emitt0,esum2=0.0,offset_y=1.3,qpsum=0.0;
  BIN **bin_nlc,**bin_clic;
  int nbin_nlc,nbin_clic;

  bin_nlc=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  bin_clic=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);

  beamline_bin_divide_1(beamline,1,0,bin_nlc,&nbin_nlc);
  beamline_bin_divide(beamline,1,0,bin_clic,&nbin_clic);

  if (name==NULL){
    do_emitt=0;
  }
  else{
    do_emitt=1;
    emitt_store_init(nbin_nlc);
  }
  tb=bunch_remake(bunch0);
  if (do_jitter){
    tb1=bunch_remake(bunch0);
  }
  workbunch=bunch_remake(bunch0);
  bunch_copy_0(bunch0,tb);
  simple_bin_fill(beamline,bin_nlc,nbin_nlc,tb,workbunch);
  bunch_copy_0(bunch0,tb);
  simple_bin_fill(beamline,bin_clic,nbin_clic,tb,workbunch);

  if (do_emitt==0) {
    beam_copy(bunch0,tb);
    bunch_track(beamline,tb,0,beamline->n_elements);
  }
  for (i=0;i<niter;i++){
    survey(beamline);
    beam_copy(bunch0,tb);
    if (do_emitt){
      beam_copy(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter_0(beamline,bin_clic,nbin_clic,tb,tb1,workbunch);
      }
      else{
	simple_correct_0(beamline,bin_clic,nbin_clic,tb,workbunch);
      }
      for (j=0;j<beamline->n_elements;j++){
	if (beamline->element[j]->is_bpm()){
	  element_add_offset(beamline->element[j],gasdev()*10.0,0.0);
	}
      }

      beam_copy(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter(beamline,bin_nlc,nbin_nlc,tb,tb1,workbunch,1);
      }
      else{
	simple_correct_emitt(beamline,bin_nlc,nbin_nlc,tb,workbunch);
      }
    }
    else{
      bunch_copy_0(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter_0(beamline,bin_clic,nbin_clic,tb,tb1,workbunch);
      }
      else{
	simple_correct_0(beamline,bin_clic,nbin_clic,tb,workbunch);
      }

      bunch_copy_0(bunch0,tb);
      if (do_jitter){
	bunch_copy_0(bunch0,tb1);
	for (j=0;j<tb1->slices;j++){
	  tb1->particle[j].y+=offset_y;
	}
	simple_correct_jitter_0(beamline,bin_nlc,nbin_nlc,tb,tb1,workbunch);
      }
      else{
	simple_correct_0(beamline,bin_nlc,nbin_nlc,tb,workbunch);
      }
    }
    emitt0=emitt_y(tb);
    esum+=emitt0;
    esum2+=emitt0*emitt0;
    printf("%d %g %g %g\n",i,emitt0,esum/(double)(i+1),qpsum/(double)(i+1)); // qpsum is always 0! JS
  }
  esum/=(double)niter;
  esum2/=(double)niter;
  printf("emitt= %g +/- %g\n",esum,
	 sqrt(max(0.0,esum2-esum*esum)/(double)niter));
  if (do_emitt){
    emitt_print(name);
    emitt_store_delete();
  }
  if (do_jitter){
    beam_delete(tb1);
  }
  beam_delete(workbunch);
  beam_delete(tb);
  return esum;
}

void try2(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,BEAM *tb,
    BEAM *workbunch)
{
  int i,j,k;
  double rtot1[5000],rtot2[5000],r1[5000],r2[5000];
  int c_pos[1000];

  beamline_cut(beamline,bin,nbin,c_pos);

  for (i=0;i<31;i++){
    beam_copy(bunch,tb);
    tb->particle[i].y=1.0;
    /*    beamline_set_zero(beamline);*/
    simple_correct(beamline,bin,nbin,tb,workbunch);
    for (j=0;j<31;j++){
      rtot1[2*31*(j*2)+2*i]=tb->particle[j].y;
      rtot1[2*31*(j*2+1)+2*i]=tb->particle[j].yp;
    }
    beam_copy(bunch,tb);
    tb->particle[i].yp=1.0;
    /*    beamline_set_zero(beamline);*/
    simple_correct(beamline,bin,nbin,tb,workbunch);
    for (j=0;j<31;j++){
      rtot1[2*31*(j*2)+2*i+1]=tb->particle[j].y;
      rtot1[2*31*(j*2+1)+2*i+1]=tb->particle[j].yp;
    }
  }

  beamline_set_zero(beamline);
  matrix_unit(rtot2,62);
  for (k=0;k<nbin+2;k++){
    for (i=0;i<31;i++){
      bunch_copy_0(bunch,tb);
      tb->particle[i].y=1.0;
      /*      beamline_set_zero(beamline);*/
      if ((k>0)&&(k<=nbin)){
	bunch_copy_0(tb,workbunch);
	bin_measure(beamline,workbunch,0,bin[k-1]);
	bin_correct(beamline,0,bin[k-1]);
      }
      bunch_track_0(beamline,tb,c_pos[k],c_pos[k+1]);
      for (j=0;j<31;j++){
	r2[2*31*(j*2)+2*i]=tb->particle[j].y;
	r2[2*31*(j*2+1)+2*i]=tb->particle[j].yp;
      }
      bunch_copy_0(bunch,tb);
      tb->particle[i].yp=1.0;
      /*      beamline_set_zero(beamline);*/
      if ((k>0)&&(k<=nbin)){
	bunch_copy_0(tb,workbunch);
	bin_measure(beamline,workbunch,0,bin[k-1]);
	bin_correct(beamline,0,bin[k-1]);
      }
      bunch_track_0(beamline,tb,c_pos[k],c_pos[k+1]);
      for (j=0;j<31;j++){
	r2[2*31*(j*2)+2*i+1]=tb->particle[j].y;
	r2[2*31*(j*2+1)+2*i+1]=tb->particle[j].yp;
      }
    }
    if ((k>0)&&(k<=nbin)){
      /*
      printf("bin= %d %d\n",bin[k-1]->quad[0],bin[k-1]->bpm[bin[k-1]->nbpm-1]);
      */
      bin_quadrupoles_set_zero(beamline,bin[k-1]);
    }
    /*
    printf("c_pos= %d %d\n",c_pos[k],c_pos[k+1]);
    */
    beamline_set_zero(beamline);
    bunch_track(beamline,bunch,c_pos[k],c_pos[k+1]);
    matrix_copy(rtot2,r1,62*62);
    matrix_mult(r2,62,62,r1,62,62,rtot2);
  }
  printf("rtot1=\n");
  print_transf(rtot1);
  printf("rtot2=\n");
  print_transf(rtot2);
  matrix_sub(rtot2,62,62,rtot1,62,62,r1);
  printf("diff=\n");
  print_transf(r1);
}

void tryx(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,BEAM *workbunch)
{
  int i,j,k;
  double rtot1[5000],rtot2[5000],r1[5000],r2[5000];
  int c_pos[1000];

  beamline_cut(beamline,bin,nbin,c_pos);

  for (i=0;i<31;i++){
    beam_copy(bunch,workbunch);
    workbunch->particle[i].y=1.0;
    bunch_track_0(beamline,workbunch,0,beamline->n_elements);
    for (j=0;j<31;j++){
      rtot1[2*31*(j*2)+2*i]=workbunch->particle[j].y;
      rtot1[2*31*(j*2+1)+2*i]=workbunch->particle[j].yp;
    }
    beam_copy(bunch,workbunch);
    workbunch->particle[i].yp=1.0;
    bunch_track_0(beamline,workbunch,0,beamline->n_elements);
    for (j=0;j<31;j++){
      rtot1[2*31*(j*2)+2*i+1]=workbunch->particle[j].y;
      rtot1[2*31*(j*2+1)+2*i+1]=workbunch->particle[j].yp;
    }
  }

  matrix_unit(rtot2,62);
  for (k=0;k<nbin+1;k++){
    for (i=0;i<31;i++){
      beam_copy(bunch,workbunch);
      workbunch->particle[i].y=1.0;
      bunch_track_0(beamline,workbunch,c_pos[k],c_pos[k+1]);
      for (j=0;j<31;j++){
	r2[2*31*(j*2)+2*i]=workbunch->particle[j].y;
	r2[2*31*(j*2+1)+2*i]=workbunch->particle[j].yp;
      }
      beam_copy(bunch,workbunch);
      workbunch->particle[i].yp=1.0;
      bunch_track_0(beamline,workbunch,c_pos[k],c_pos[k+1]);
      for (j=0;j<31;j++){
	r2[2*31*(j*2)+2*i+1]=workbunch->particle[j].y;
	r2[2*31*(j*2+1)+2*i+1]=workbunch->particle[j].yp;
      }
    }
    bunch_track(beamline,bunch,c_pos[k],c_pos[k+1]);
    matrix_copy(rtot2,r1,62*62);
    matrix_mult(r2,62,62,r1,62,62,rtot2);
  }
  printf("rtot1=\n");
  print_transf(rtot1);
  printf("rtot2=\n");
  print_transf(rtot2);
  matrix_sub(rtot2,62,62,rtot1,62,62,r1);
  printf("diff=\n");
  print_transf(r1);
}

void
xmeasured_bin_fill(BEAMLINE *beamline,double *quad0,double *quad1,
		   double *quad2,
		   BEAM *b0,BEAM * /*b1*/,BEAM * /*b2*/,BEAM *workbunch,
		   BIN **bin,int bin_number,double g1,double g2)
{
  int i,j,ipos;
  ipos=0;

  //  wakefield_init(1,0);
  for (i=0;i<bin_number;i++){

    quadrupoles_set(beamline,quad0);
    bunch_track(beamline,b0,ipos,bin[i]->start);
    bin_response(beamline,b0,workbunch,0,bin[i]);

    for (j=0;j<b0->slices;j++){
      b0->particle[j].energy*=g1;
    }

    quadrupoles_set(beamline,quad1);
    bin_response(beamline,b0,workbunch,1,bin[i]);

    for (j=0;j<b0->slices;j++){
      b0->particle[j].energy*=g2/g1;
    }

    quadrupoles_set(beamline,quad2);
    bin_response(beamline,b0,workbunch,2,bin[i]);

    for (j=0;j<b0->slices;j++){
      b0->particle[j].energy/=g2;
    }

    bin_finish(bin[i],1);
    ipos=bin[i]->start;
  }
  //  wakefield_init(1,1);
}

void
xmeasured_correct(BEAMLINE *beamline,
		  double *quad0,double *quad1,double *quad2,
		  BIN **bin,int bin_number,BEAM *b0,BEAM * /*b1*/,BEAM * /*b2*/,
		  BEAM *workbunch,int do_emitt,double jitter,
		  double g1,double g2)
{
  int i,ipos,is,np;
  double tmp1,tmp2,rat;

  ipos=0;
  np=b0->slices/2;
  quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt_start();
  }
  for (i=0;i<bin_number;i++){
    
    if (do_emitt) {
      bunch_track_emitt(beamline,b0,ipos,bin[i]->start);
    }
    else {
      bunch_track_0(beamline,b0,ipos,bin[i]->start);
    }

    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad1);
    }

    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad1);
    }
    quadrupoles_bin_set(beamline,bin[i],quad1);
    bunch_copy_0(b0,workbunch);
    rat=sqrt(b0->sigma[np].r22/b0->sigma[np].r11);
    tmp1=jitter*gasdev();
    tmp2=jitter*gasdev()*rat;
    for (is=0;is<b0->slices;is++){
      workbunch->particle[is].y+=tmp1;
      workbunch->particle[is].yp+=tmp2;
      workbunch->particle[is].energy*=g1;
    }
    bin_measure(beamline,workbunch,1,bin[i]);
    
    if (i>0){
      quadrupoles_bin_set(beamline,bin[i-1],quad2);
    }
    quadrupoles_bin_set(beamline,bin[i],quad2);
    bunch_copy_0(b0,workbunch);
    tmp1=jitter*gasdev();
    tmp2=jitter*gasdev()*rat;
    for (is=0;is<b0->slices;is++){
      workbunch->particle[is].y+=tmp1;
      workbunch->particle[is].yp+=tmp2;
      workbunch->particle[is].energy*=g2;
    }
    bin_measure(beamline,workbunch,2,bin[i]);
    for (is=0;is<bin[i]->nbpm;is++){
      bin[i]->b2[is]=bin[i]->b1[is];
      bin[i]->b2_x[is]=bin[i]->b1_x[is];
    }

    bunch_copy_0(b0,workbunch);
    tmp1=jitter*gasdev();
    tmp2=jitter*gasdev()*rat;
    for (is=0;is<b0->slices;is++){
      workbunch->particle[is].y+=tmp1;
      workbunch->particle[is].yp+=tmp2;
    }
    quadrupoles_bin_set(beamline,bin[i],quad0);
    bin_measure(beamline,workbunch,0,bin[i]);

    bin_correct(beamline,1,bin[i]);
//    quadrupoles_bin_set(beamline,bin[i],quad0);
    
    ipos=bin[i]->start;
  }
  quadrupoles_set(beamline,quad0);
  if (do_emitt) {
    bunch_track_emitt(beamline,b0,ipos,beamline->n_elements);
    bunch_track_emitt_end(b0);
  }
  else {
    bunch_track_0(beamline,b0,ipos,beamline->n_elements);
  }
}
