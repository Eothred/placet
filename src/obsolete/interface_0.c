#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "malloc.h"
#include "rndm.h"

void data_MAD_track(BEAM *beam,char *name,int n_particles,int axis,int bunch_number,double energy0)
{
  FILE *file;
  int i,j,j_slice,n,nb,m=0, np = 0;
  double y=0.0,yp=0.0,wgt=0.0,x=0.0,xp=0.0;
  double x_slice, xp_slice, y_slice, yp_slice;
  double z_rndm, x_rndm, xp_rndm, y_rndm, yp_rndm;
  double energy_average = 0.0, z_pos, energy, energy_permille;
  double *wgt_comul;
  double energy_prev_slice, energy_slice, weight_prev_slice, weight_slice;
  double radius_x, radius_y, theta_x, theta_y;
  double det_sigma_xx, det_sigma, emittance_x, emittance_y;

  wgt_comul = (double *)alloca(sizeof(double *)*beam->slices);
  
  n=beam->slices/beam->bunches;
  nb=beam->bunches;

  /* open file */

  if (name){
    file=fopen(name,"w");
  }
  else{
    file=stdout;
  }

  /* get average slice position, slope and energy. Compute comulative distribution */

  m=0;
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
#ifdef TWODIM
      x += beam->particle[m].x*beam->particle[m].wgt;
      xp += beam->particle[m].xp*beam->particle[m].wgt;
#endif
      y += beam->particle[m].y*beam->particle[m].wgt;
      yp += beam->particle[m].yp*beam->particle[m].wgt;
      wgt += beam->particle[m].wgt;
      wgt_comul[m] = wgt;
      energy_average += beam->particle[m].energy*beam->particle[m].wgt;
      m++;
    }
  }
  x /= wgt;
  xp /= wgt;
  y /= wgt;
  yp /= wgt;
  energy_average /= (double)wgt;

  /* normalise wgt_comul */

  for (j=0; j<m; j++){
    wgt_comul[j] /= wgt;
  }

  /* loop on the required n_particles */

  while (np < n_particles){

  /* get a random number between 0 and 1 (uniform distribution) */

  z_rndm = rndm();


  /* find the slice with the comulative weight just above z_rnd */

  j = 0;
  while (z_rndm > wgt_comul[j]) j++;
  j_slice = j;

  /* compute z position */

  z_pos = beam->z_position[j_slice];

  /* interpolate energy */

  energy_prev_slice = beam->particle[j_slice - 1].energy;  
  energy_slice = beam->particle[j_slice].energy;
  weight_prev_slice = wgt_comul[j_slice - 1];
  weight_slice = wgt_comul[j_slice];
  energy = (energy_slice*(z_rndm - weight_prev_slice) -
            energy_prev_slice*(z_rndm - weight_slice))/
           (weight_slice - weight_prev_slice);
  energy_permille = energy*1000.0/energy_average;

  /* fetch the centre coordinates of the x & y phases ellipses for the selected slice */

  x_slice = beam->particle[j_slice].x;
  xp_slice = beam->particle[j_slice].xp;
  y_slice = beam->particle[j_slice].y;
  yp_slice = beam->particle[j_slice].yp; 

  /* get the random point inside the normalised circle in phase space.
     Coordinates are polar ( gaussian distribution between 0 and sigma for the radius 
     and uniform distribution between 0 and 2 pi for the azimuth). */

  radius_x = gasdev();
  radius_y = gasdev();
  theta_x = rndm()*TWOPI;
  theta_y = rndm()*TWOPI;

  /* get the emittances */

  det_sigma_xx = beam->sigma_xx[j_slice].r11*beam->sigma_xx[j_slice].r22 -
                 beam->sigma_xx[j_slice].r12*beam->sigma_xx[j_slice].r12;
  det_sigma = beam->sigma[j_slice].r11*beam->sigma[j_slice].r22 -
              beam->sigma[j_slice].r12*beam->sigma[j_slice].r12;
  emittance_x = sqrt(det_sigma_xx);
  emittance_y = sqrt(det_sigma);

  /* transform to the true phase space */

  x_rndm = radius_x*cos(theta_x)*beam->sigma_xx[j_slice].r11/sqrt(emittance_x);
  xp_rndm = radius_x*(-cos(theta_x)*beam->sigma_xx[j_slice].r12 + 
                      sin(theta_x)*emittance_x)/sqrt(emittance_x);
  y_rndm = radius_y*cos(theta_y)*beam->sigma[j_slice].r11/sqrt(emittance_y);
  yp_rndm = radius_y*(-cos(theta_y)*beam->sigma[j_slice].r12 + 
                      sin(theta_y)*emittance_y)/sqrt(emittance_y);

  /* add the offsets */

  x_rndm += x_slice;
  xp_rndm += xp_slice;
  y_rndm += y_slice;
  yp_rndm += yp_slice;

  /* save on file */

  fprintf(file,"%g %g %g %g %g %g \n",
	  x_rndm, xp_rndm, y_rndm, yp_rndm, energy_permille, z_pos);
  np++;
  }
  if (name){
    fclose(file);
  }
}
