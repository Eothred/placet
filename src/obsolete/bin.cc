void bin_define_1_test(BEAMLINE *beamline,int start,int nquad,int nnbpm,
             int bpm[],int *nbpm,int quad[],int *nq)
{
  ELEMENT **element;
  int mb,first_bpm=0,old_conf=0;
  int nq1,nq2;
  *nq=0;
  *nbpm=0;
  element=beamline->element;

  nq1=nq2=nquad/2;
  nquad = nq1 + nq2;
  mb=nnbpm;


  /*** First F quadrupole ***/
  while (element[start]!=NULL)
  {
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr()){
       if (quad_ptr->get_strength()>0.0) break;
    }
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  // placet_printf(INFO,"************* BIN BEGIN *************\n");
  // placet_printf(INFO,"F on %d\n",start);
  
  quad[0]=start++;
  (*nq)++;

  /*** nb BPM and a F quadrupole ***/

  first_bpm = -1;
  if(old_conf) first_bpm = -1;

  while((element[start]!=NULL)&&(nq1)){
    if (element[start]->is_bpm()&&(mb))
    {
      if(first_bpm == 0) {
        mb--;
        bpm[(*nbpm)++]=start;
        // placet_printf(INFO,"BPM a on %d\n",start);
      }
      first_bpm = 0;
    }
          
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr())
    {
      if (quad_ptr->get_strength()>0.0)
      {
        nq1--;
        nquad--;
        if (nquad) quad[(*nq)++]=start; 
        // placet_printf(INFO,"F on %d\n",start);
      }
    }
    start++;
  }
  
   first_bpm = -1;
  if(old_conf) first_bpm = 1;

  /*** nb BPM and a F quadrupole ***/ 
 
  // first_bpm = 0;

  while((element[start]!=NULL)&&(nq2)){
    if (element[start]->is_bpm()&&(mb)){
      if(first_bpm == 1) {
        mb--;
        bpm[(*nbpm)++]=start;
	// placet_printf(INFO,"BPM b on %d\n",start);
      }
      first_bpm *= -1;
      if(old_conf) first_bpm = 1;
    }
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr()) {
      if (quad_ptr->get_strength()>0.0) {
        nq2--;
        nquad--;
        if (nquad) quad[(*nq)++]=start; 
        // if (nquad) placet_printf(INFO,"F on %d\n",start);
      }
    }
    start++;
  }


  first_bpm = -1;
  if(old_conf) first_bpm = 1;

  while((element[start]!=NULL)&&(mb)){
    if (element[start]->is_bpm()){
      if(first_bpm == 1) {
        mb--;
        bpm[(*nbpm)++]=start;
        // placet_printf(INFO,"BPM c on %d\n",start);
      }
      first_bpm = (-1)*first_bpm;
      if(old_conf) first_bpm = 1;
    }
    start++;
  }

  // placet_printf(INFO,"nq = %d and nbpm = %d\n",(*nq),(*nbpm));  
}

void bin_define_ballistic(BEAMLINE *beamline,int start,int nquad,int bpm[],
		     int *nbpm,int quad[],int *nq,int *nq2)
{
  ELEMENT **element;

  *nq=0;
  *nq2=0;
  *nbpm=0;
  element=beamline->element;
  while (element[start]!=NULL){
    if (element[start]->is_quad()) break;
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  /** /
  if (element[start]->is_quadbpm()){
    bpm[(*nbpm)++]=start;
  }
  / **/
  quad[0]=start++;
  (*nq)++;
  (*nq2)++;
  while((element[start]!=NULL)&&(nquad)){
    if (element[start]->is_quad()){
      nquad--;
      if(nquad){
	(*nq2)++;
      }
    }
    if (element[start]->is_bpm()){
      if (nquad) quad[(*nq)++]=start;
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
}

void bin_response_print(FILE *file,BIN *bin)
{
  int i,j,nq,nb;
  double *a;
  nq=bin->nq;
  nb=bin->nbpm;
  a=bin->a0;
  for (i=0;i<nq;i++){
    for (j=0;j<nb;j++){
      fprintf(file,"%g ",*a++);
    }
    fprintf(file,"\n");
  }
}

int tk_BinList(ClientData /*clientdata*/,Tcl_Interp * /*interp*/,int /*argc*/,
	       char * /*argv*/[])
{
  /*
  bin_list();
  */
  return TCL_OK;
}
