#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "distribute.h"

typedef struct {
  double x,xp,y,yp,e,z,c,s,charge;
} PARTICLE;

print_field(double kick_x_S[],double kick_x_C[],int nstep)
{
  int i;
  for (i=0;i<nstep;i++){
    printf("%d %g %g\n",i,kick_x_S[i],kick_x_C[i]);
  }
}

pass_particle(PARTICLE *p,
	      double kick_x_S[],double kick_x_C[],
	      double kick_y_S[],double kick_y_C[],
	      double kick_x_short_S[],double kick_x_short_C[],
	      double kick_y_short_S[],double kick_y_short_C[],
	      int nstep,
	      double delta,double a,double e0,double e0_fact)
{
  int i;
  double step;

  step=0.5*delta/nstep;
  e0*=2.0*step;
  kick_x_short_C[nstep]-=2.0*a*p->charge*step*p->x*p->s;
  kick_x_short_S[nstep]+=2.0*a*p->charge*step*p->x*p->c;
  kick_y_short_C[nstep]-=2.0*a*p->charge*step*p->y*p->s;
  kick_y_short_S[nstep]+=2.0*a*p->charge*step*p->y*p->c;
  for (i=nstep-1;i>=0;i--){
    p->x+=step*p->xp;
    p->y+=step*p->yp;
    p->xp+=(p->c*(e0+kick_x_C[i]+0.5*(kick_x_short_C[i]+kick_x_short_C[i+1]))
	    +p->s*(kick_x_S[i]+0.5*(kick_x_short_S[i]+kick_x_short_S[i+1])))
      /p->e;
    p->yp+=(p->c*(kick_y_C[i]+0.5*(kick_y_short_C[i]+kick_y_short_C[i+1]))
	    +p->s*(kick_y_S[i]+0.5*(kick_y_short_S[i]+kick_y_short_S[i+1])))
      /p->e;
    p->x+=step*p->xp;
    p->y+=step*p->yp;
    kick_x_short_C[i]-=2.0*a*p->charge*step*p->x*p->s;
    kick_x_short_S[i]+=2.0*a*p->charge*step*p->x*p->c;
    kick_y_short_C[i]-=2.0*a*p->charge*step*p->y*p->s;
    kick_y_short_S[i]+=2.0*a*p->charge*step*p->y*p->c;
    e0*=e0_fact;
  }
}

pass_bunch(double x_S[],double x_C[],double y_S[],double y_C[],double dec[],
	   int nstep,double z[],int ibunch,double a,double delta,double f,
	   double e0,double e0_fact,
	   PARTICLE p[],int nparticles)
{
  int i,n_off;
  double *kick_x_S,*kick_x_C,*kick_y_S,*kick_y_C,
    *kick_x_short_S,*kick_x_short_C,*kick_y_short_S,*kick_y_short_C;

  /*
    allocate temporary memory
  */

  kick_x_S=(double*)alloca(sizeof(double)*(nstep+1));
  kick_x_C=(double*)alloca(sizeof(double)*(nstep+1));
  kick_y_S=(double*)alloca(sizeof(double)*(nstep+1));
  kick_y_C=(double*)alloca(sizeof(double)*(nstep+1));
  kick_x_short_S=(double*)alloca(sizeof(double)*(nstep+1));
  kick_x_short_C=(double*)alloca(sizeof(double)*(nstep+1));
  kick_y_short_S=(double*)alloca(sizeof(double)*(nstep+1));
  kick_y_short_C=(double*)alloca(sizeof(double)*(nstep+1));

  /*
    initialise short range kick to zero
   */

  for (i=0;i<nstep+1;i++){
    kick_x_short_S[i]=0.0;
    kick_x_short_C[i]=0.0;
    kick_y_short_S[i]=0.0;
    kick_y_short_C[i]=0.0;
  }

  /*
    calculate long range wakefields
   */

  distribute_cav_Q(x_S,x_C,kick_x_S,kick_x_C,nstep,f,z,dec,ibunch);
  distribute_cav_Q(y_S,y_C,kick_y_S,kick_y_C,nstep,f,z,dec,ibunch);
  
  /*
    step particles through the structure
   */

  for (i=0;i<nparticles;i++){
    pass_particle(p+i,kick_x_S,kick_x_C,kick_y_S,kick_y_C,
		  kick_x_short_S,kick_x_short_C,kick_y_short_S,kick_y_short_C,
		  nstep,delta,a,e0,e0_fact);
  }

  /*
    update longrange wakefields with new bunch
   */

  n_off=(nstep+1)*ibunch;
  for (i=0;i<nstep+1;i++){
    x_S[n_off]=kick_x_short_S[i];
    x_C[n_off]=kick_x_short_C[i];
    y_S[n_off]=kick_y_short_S[i];
    y_C[n_off]=kick_y_short_C[i];
    n_off++;
  }
}

pass_train(double z[],int nbunches,PARTICLE p[],int nparticles,double delta,
	   int nstep)
{
  int i,n;
  double *x_S,*x_C,*y_S,*y_C,*dec;
  double a,f,beta,e0,e0_fact;

  x_S=(double*)alloca(sizeof(double)*(nstep+1)*nbunches);
  x_C=(double*)alloca(sizeof(double)*(nstep+1)*nbunches);
  y_S=(double*)alloca(sizeof(double)*(nstep+1)*nbunches);
  y_C=(double*)alloca(sizeof(double)*(nstep+1)*nbunches);
  dec=(double*)alloca(sizeof(double)*nbunches);

  n=nparticles/nbunches;
  a=811.0*1.6e-7*1e-9;
  beta=0.025;
  f=beta*nstep/(1.0-beta)/delta;
  e0=0.00272727272727273*10.0/9.0;
  e0_fact=1.0;
  for (i=0;i<nbunches;i++){
    dec[i]=1.0;
    pass_bunch(x_S,x_C,y_S,y_C,dec,nstep,z,i,a,delta,f,e0,e0_fact,p+i*n,n);
  }
}

drift_train(PARTICLE p[],int nparticles,double dist)
{
  int i;

  for (i=0;i<nparticles;i++){
    p[i].x+=dist*p[i].xp;
    p[i].y+=dist*p[i].yp;
  }
}

quadrupole_train(PARTICLE p[],int nparticles,double k)
{
  int i;
  //  return;
  //  k*=0.001;
  for (i=0;i<nparticles;i++){
    p[i].xp-=k*p[i].x/p[i].e;
    p[i].yp+=k*p[i].y/p[i].e;
  }
}

turn_train(PARTICLE p[],int nparticles,double phi,double b1,double b2)
{
  int i;
  double f1,f2,tmp,c,s;

  f1=sqrt(b1);
  f2=sqrt(b2);
  c=cos(phi/180.0*acos(-1.0));
  s=sin(phi/180.0*acos(-1.0));
  for (i=0;i<nparticles;i++){
    p[i].x/=f1;
    p[i].xp*=f1;
    tmp=p[i].x;
    p[i].x=c*p[i].x-s*p[i].xp;
    p[i].xp=c*p[i].xp+s*tmp;
    p[i].x*=f2;
    p[i].xp/=f2;
  }
}

shift_train(PARTICLE p[],int n,double k,double s)
{
  double c0,s0,tmp;
  int i;

  c0=cos(k*s);
  s0=sin(k*s);
  for (i=0;i<n;i++){
    tmp=p[i].c*c0-p[i].s*s0;
    p[i].s=c0*p[i].s+s0*p[i].c;
    p[i].c=tmp;
    p[i].z+=s;
  }
}

merge_train(PARTICLE p0[],PARTICLE p1[],int n,int i0,int n0)
{
  int i,j,jn,k;
  j=n*n0-1;
  jn=j+n;
  for (i=0;i<n0-i0;i++){
    p0[jn]=p0[j];
    j--;
    jn--;
  }
  for (i=0;i<n;i++){
    p0[jn]=p1[n-i-1];
    jn--;
    for (k=0;k<n0;k++){
      p0[jn]=p0[j];
      j--;
      jn--;
    }
  }
}

z_extract(PARTICLE p[],int n,double z[])
{
  int i;
  for (i=0;i<n;i++){
    z[i]=p[i].z;
  }
}

print_train(PARTICLE p[],int n)
{
  int i;

  for (i=0;i<n;i++){
    printf("%d %g %g\n",i,p[i].x,p[i].xp);
  }
}

#define NBUNCH 200
#define N 5

main(int argc,char *argv[])
{
  PARTICLE p[NBUNCH*N],pnew[NBUNCH];
  double z[NBUNCH*N],
    zpos[]={0.0,0.025},
    xpos[]={0.000825,0.0},
    xp_pos[]={0.00,0.0},
    angle[]={-0.005,0.0};
  int i,j,np;
  double k,phase=0.0,sig=1.0,b1=5.0,b2=5.0,advance,delta_x,delta_xp;
  char *point;

  k=2.0*acos(-1.0)/0.1;
  np=0;
  for (i=0;i<NBUNCH;i++){
    for (j=0;j<2;j++){
      z[np]=(double)i*0.1+zpos[j];
      pnew[np].s=sin(z[np]*k);
      pnew[np].c=cos(z[np]*k);
      pnew[np].e=0.2;
      pnew[np].x=xpos[j]+xp_pos[j];
      pnew[np].xp=angle[j];
      pnew[np].y=0.0;
      pnew[np].yp=0.0;
      pnew[np].charge=1.5e10;
      pnew[np].z=z[np];
      np++;
    }
  }
  pass_train(z,np,pnew,np,0.33,10);
  print_train(pnew,np);
  exit(0);
}
