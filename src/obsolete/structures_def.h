struct BEAMINFO{
  double emitt_y,emitt_y_axis;
  double y_mean,yp_mean,y_max,yp_max;
  double sigma_y;
};

struct BNS_DATA{
  double charge,kick;
};

struct help_pointer {
  char *name;
  struct help_pointer *next;
};

typedef struct help_pointer HELP_POINTER;
