#ifndef octave_corrector_h
#define octave_corrector_h

#include "option_info.h"

inline bool octave_set_value(const option_info *option_table, const char *option_name, octave_value value )
{
	while(option_table->type!=OPT_END) {
		if (strcmp(option_table->name, option_name) == 0) {
			switch(option_table->type) {
        case OPT_DOUBLE:  (*reinterpret_cast<double*>(option_table->dest)) = value.scalar_value(); break;
        case OPT_INT:     (*reinterpret_cast<int*>(option_table->dest)) = value.int_value();  break;
  		}
		  return true;
    }
    option_table++;
	}
	return false;
}

inline bool add_value(const option_info *option_table, const char *option_name, octave_value value )
{
   while(option_table->type!=OPT_END) {
		if (strcmp(option_table->name, option_name) == 0) {
			switch(option_table->type) {
        case OPT_DOUBLE:  (*reinterpret_cast<double*>(option_table->dest)) += value.scalar_value(); break;
        case OPT_INT:     (*reinterpret_cast<int*>(option_table->dest)) += value.int_value();  break;
  		}
		  return true;
    }
    option_table++;
	}
	return false;
}

#endif /* octave_corrector_h */
