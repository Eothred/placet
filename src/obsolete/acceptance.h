#ifndef acceptance_h
#define acceptance_h

class ACCEPTANCE : public ELEMENT {
  double ax0,ay0;
  double mx0,my0;
  int rms;
 public:
  explicit ACCEPTANCE(double ax=0.0, double ay=0.0){rms=0;};
  double mx() {return mx0;};
  double my() {return my0;};
  double ax(){return ax0;};
  double ay(){return ay0;};
  void set_ax(double x){ax0=x;};
  void set_ay(double x){ay0=x;};
  void step_4d_0(BEAM*);
  void step_4d(BEAM*);
};
int tk_Acceptance(ClientData clientdata,Tcl_Interp *interp,int argc,
		  char *argv[]);

#endif // acceptance_h
