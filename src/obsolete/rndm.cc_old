float expdev()
{
    return -log(1.0-rndm());
}

static struct
{
  int i;
} rndm0_store;

void rndmst0(int i)
{
  rndm0_store.i=i;
}

float rndm0()
{
  const int m=2147483647,a=16807,q=127773,r=2836;
  const float m_inv=(1.0-RNDM_EPS)/(float)m;
  int k;

  k=rndm0_store.i/q;
  if((rndm0_store.i=a*(rndm0_store.i-k*q)-r*k)<0) rndm0_store.i+=m;
  return m_inv*rndm0_store.i;
}

static struct
{
  long i,p,is[32];
} rndm1_store;

void rndmst1(int i)
{
  const long m=2147483647,a=16807,q=127773,r=2836,n=32; // m1=m-1, nd=1+m1/n
  // const float m_inv=(1.0-RNDM_EPS)/(float)m;
  int k,j;

  for (j=n+7;j>=0;j--)
    {
      k=i/q;
      if((i=a*(i-k*q)-r*k)<0) i+=m;
      if (j<n) rndm1_store.is[j]=i;
    }
  rndm1_store.i=i;
  rndm1_store.p=rndm1_store.is[0];
}

float rndm1()
{
  const long m=2147483647,m1=m-1,a=16807,q=127773,r=2836,n=32,nd=1+m1/n;
  const float m_inv=(1.0-RNDM_EPS)/(float)m;
  int k,j;

  k=rndm1_store.i/q;
  if((rndm1_store.i=a*(rndm1_store.i-k*q)-r*k)<0) rndm1_store.i+=m;
  j=rndm1_store.p/nd;
  rndm1_store.p=rndm1_store.is[j];
  rndm1_store.is[j]=rndm1_store.i;
  return rndm1_store.p<m1?m_inv*rndm1_store.p:m_inv*m1;
//  return (m_inv*min(rndm1_store.p,m1));
}

static struct
{
  int i1,i2,p,is[32];
} rndm2_store;

void rndmst2(int i)
{
  const int m1=2147483563; // m1_1=m1-1, m2=2147483399
  const int a1=40014,q1=53668,r1=12211,n=32;
  // a2=40692,q2=52774,r2=3791,nd=1+m1_1/n
//  const float m1_inv=(1.0-RNDM_EPS)/(float)m1,m2_inv=(1.0-RNDM_EPS)/(float)m2;
  int k,j;

  for (j=n+7;j>=0;j--)
    {
      k=i/q1;
      if((i=a1*(i-k*q1)-r1*k)<0) i+=m1;
      if (j<n) rndm2_store.is[j]=i;
    }
  rndm2_store.i1=i;
  rndm2_store.i2=12345678;
  rndm2_store.p=rndm2_store.is[0];
}

float rndm2()
{
  const int m1=2147483563,m2=2147483399,m1_1=m1-1;
  const int a1=40014,a2=40692,q1=53668,q2=52774,r1=12211,r2=3791,n=32,
            nd=1+m1_1/n;
  const float m_inv=(1.0-RNDM_EPS)/(float)m1;
  int k,j;

  k=rndm2_store.i1/q1;
  if((rndm2_store.i1=a1*(rndm2_store.i1-k*q1)-r1*k)<0) rndm2_store.i1+=m1;
  k=rndm2_store.i2;
  if((rndm2_store.i2=a2*(rndm2_store.i2-k*q2)-r2*k)<0) rndm2_store.i2+=m2;
  j=rndm2_store.p/nd;
  if((rndm2_store.p=rndm2_store.is[j]-rndm2_store.i2)<1) rndm2_store.p+=m1_1;
  rndm2_store.is[j]=rndm2_store.i2;
  return rndm2_store.p*m_inv;
  //  return rndm2_store.p<m1_1?m_inv*rndm2_store.p:m_inv*m1_1;
  //  return m_inv*min(rndm2_store.p,m1_1);
}

static struct
{
  int in1,in2,is[55];
} rndm3_store;

void rndmst3(int i)
{
  const int big=1000000000,seed=161803398,z=0;
  //  const float fact=1.0/(float)big*(1.0-RNDM_EPS);
  int j,ii,k;

  j=(seed-abs(i)) % big;
  rndm3_store.is[54]=j;
  k=1;
  for (i=1;i<=54;i++)
    {
      ii=(21*(i+1)) % 54;
      rndm3_store.is[ii-1]=k;
      if((k=j-k)<z) k+=big;
      j=rndm3_store.is[ii];
    }
  for (k=0;k<4;k++)
    {
      for(i=1;i<=55;i++)
	{
	  if((rndm3_store.is[i-1]-=rndm3_store.is[(i+30)%55])<z)
	                                         rndm3_store.is[i-1]+=big;
	}
      }
  rndm3_store.in1=-1;
  rndm3_store.in2=30;
}

float rndm3()
{
  const int big=1000000000,z=0; // seed=161803398
  const float fact=1.0/(float)big*(1.0-RNDM_EPS);
  int j;

  if (++rndm3_store.in1==55) rndm3_store.in1=0;
  if (++rndm3_store.in2==55) rndm3_store.in2=0;
  if((j=rndm3_store.is[rndm3_store.in1]-rndm3_store.is[rndm3_store.in2])<z)
      j+=big;
  rndm3_store.is[rndm3_store.in1]=j;
  return j*fact;
}

static struct
{
  int i;
} rndm6_store;

void rndmst6(int i)
{
  rndm6_store.i=i;
}

float rndm6()
{

  if((rndm6_store.i*=48828125)<0)
    {
      rndm6_store.i-=2147483647;
      rndm6_store.i--;
    }
  return (float)rndm6_store.i*0.46566129e-9*(1.0-RNDM_EPS);
}

static struct
{
  unsigned int i;
  float scal;
  unsigned int n;
} rndm7_store;

void rndmst7(int i)
{
  rndm7_store.i=i;
}

float rndm7()
{
  rndm7_store.i=1664525L*rndm7_store.i+1013904223L;
  // additional factor to ensure rndm7!=1.0 if float
  return (float)rndm7_store.i*0.232830643654e-9*(1.0-RNDM_EPS);
}
