#include "rndm.c"

#ifndef TWOPI
#define TWOPI 6.283185307179586
#endif

class GROUND_DATA {
public:
    double *f,*k,*a;
    double *s0,*c0,*sf,*cf,*sk,*ck,*sk0,*ck0;
    double t,s;
    int n_f,n_k;
    GROUND_DATA();
    double xpos(double t0,double s0);
    void xpos(double t0,double s0,double ds,double *x,int n);
    void start();
};

void
GROUND_DATA::start()
{
    int i,j,m=0;
    double phi;

    for (i=0;i<n_k*n_f;i++) {
	phi=TWOPI*rndm();
	c0[i]=cos(phi);
	s0[i]=sin(phi);
    }
    for (j=0;j<n_f;j++){
	cf[j]=1.0;
	sf[j]=0.0;
    }
    for (i=0;i<n_k;i++){
	ck0[i]=0.0;
	sk0[i]=0.0;
	for (j=0;j<n_f;j++){
	    ck0[i]+=a[m]*c0[m];
	    sk0[i]+=a[m]*s0[m];
	    m++;
	}
	ck[i]=ck0[i];
	sk[i]=sk0[i];
    }
}

GROUND_DATA::GROUND_DATA()
{
    int i,j,m=0;
    n_f=1000;
    n_k=1000;
    f=new double[n_f];
    sf=new double[n_f];
    cf=new double[n_f];
    k=new double[n_k];
    sk=new double[n_k];
    ck=new double[n_k];
    sk0=new double[n_k];
    ck0=new double[n_k];
    a=new double[n_f*n_k];
    s0=new double[n_f*n_k];
    c0=new double[n_f*n_k];
    for (i=0;i<n_f;i++) {
	f[i]=TWOPI/(i+1);
	sf[i]=0.0;
	cf[i]=1.0;
    }
    for (i=0;i<n_k;i++) {
	k[i]=TWOPI/(i+1);
    }
    for (i=0;i<n_k;i++){
	for (j=0;j<n_f;j++){
	    a[m]=0.0;
	    m++;
	}
    }
    t=0.0;
    s=0.0;
    a[0]=1.0;
}

double
GROUND_DATA::xpos(double t_e,double s_e)
{
    int i,j,m;
    double pos=0.0,s,c;

    if (t_e!=t) {
	m=0;
	for (j=0;j<n_f;j++){
	    cf[j]=cos(t_e*f[j]);
	    sf[j]=sin(t_e*f[j]);
	}
	for (i=0;i<n_k;i++){
	    s=0.0;
	    c=0.0;
	    for (j=0;j<n_f;j++){
		c+=a[m]*(c0[m]*cf[j]-s0[m]*sf[j]);
		s+=a[m]*(c0[m]*sf[j]+s0[m]*cf[j]);
		m++;
	    }
	    ck[i]=c-ck0[i];
	    sk[i]=s-sk0[i];
	}
	t=t_e;
    }
    for (j=0;j<n_k;j++){
	pos+=ck[j]*cos(k[j]*s_e)-sk[j]*sin(k[j]*s_e);
    }
    return pos;
}

void
GROUND_DATA::xpos(double t_e,double s_e,double ds,double *x,int n)
{
    int i,j,m;
    double pos,tmp,s[n_k],c[n_k];
    double sd[n_k],cd[n_k];

    if (t_e!=t) {
	m=0;
	for (j=0;j<n_f;j++){
	    cf[j]=cos(t_e*f[j]);
	    sf[j]=sin(t_e*f[j]);
	}
	for (i=0;i<n_k;i++){
	    sk[i]=0.0;
	    ck[i]=0.0;
	    for (j=0;j<n_f;j++){
		ck[i]+=a[m]*(c0[m]*cf[j]-s0[m]*sf[j]);
		sk[i]+=a[m]*(c0[m]*sf[j]+s0[m]*cf[j]);
		m++;
	    }
	    ck[i]-=ck0[i];
	    sk[i]-=sk0[i];
	}
	t=t_e;
    }
    for (j=0;j<n_k;j++){
	cd[j]=cos(k[j]*ds);
	sd[j]=sin(k[j]*ds);
	c[j]=cos(k[j]*s_e);
	s[j]=sin(k[j]*s_e);
    }
    for (i=0;i<n-1;i++) {
	pos=0.0;
	for (j=0;j<n_k;j++){
	    pos+=ck[j]*c[j]-sk[j]*s[j];
	    tmp=c[j]*cd[j]-s[j]*sd[j];
	    s[j]=s[j]*cd[j]+c[j]*sd[j];
	    c[j]=tmp;
	}
	x[i]=pos;
    }
    for (j=0;j<n_k;j++){
	pos+=ck[j]*c[j]-sk[j]*s[j];
    }
    x[n-1]=pos;
    return;
}

#define N 100000

main()
{
    int i,j,n=0;
    double t,s0[N],x,x0[N],y,y0[N],d[2],dt=0.01;
    GROUND_DATA gx,gy;
    FILE *f;
    char *point,buffer[1024],name[128];

    rndmst5(12,34,56,78);
    gx.start();
    gy.start();
    for (i=0;i<12000;i++){
	s0[i]=i;
    }
    for (i=0;i<100;i++){
	gx.xpos(s0[i],0.0,dt,x0,12000);
	gy.xpos(s0[i],0.0,dt,y0,12000);
    }
}
