#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double xpos_retard(double x[],double ds_inv,double ds,double s)
{
  int i;
  double wgt;
}

distribute_old(double S_trans[],double C_trans[],
	       double S_long[],double C_long[],
	       double sum_S_trans[],double sum_C_trans[],
	       double sum_S_long[],double sum_C_long[],
	       int n,double factor,double z[],int k0)
{
  double ds_inv=1.0,w,s,*Sk_trans,*Ck_trans,*Sk_long,*Ck_long;
  int i,j,k;

  for(i=0;i<n;i++){
    sum_S_trans[i]=0.0;
    sum_C_trans[i]=0.0;
    sum_S_long[i]=0.0;
    sum_C_long[i]=0.0;
  }
  for (k=k0-1;k>=0;k--){
    w=factor*(z[k0]-z[k]);
    j=(int)w;
    w-=j;
    if (j>=n) break;
    i=0;
    Sk_trans=S_trans+k*(n+1);
    Ck_trans=C_trans+k*(n+1);
    Sk_long=S_long+k*(n+1);
    Ck_long=C_long+k*(n+1);
    sum_S_trans[j]+=(1.0-w)
      *(Sk_trans[i]+0.5*(1.0-w)*(Sk_trans[i+1]-Sk_trans[i]));
    sum_C_trans[j]+=(1.0-w)
      *(Ck_trans[i]+0.5*(1.0-w)*(Ck_trans[i+1]-Ck_trans[i]));
    sum_S_long[j]+=(1.0-w)
      *(Sk_long[i]+0.5*(1.0-w)*(Sk_long[i+1]-Sk_long[i]));
    sum_C_long[j]+=(1.0-w)
      *(Ck_long[i]+0.5*(1.0-w)*(Ck_long[i+1]-Ck_long[i]));
    j++;
    while (j<n){
      i++;
      sum_S_trans[j]+=w*(Sk_trans[i]+(1.0-0.5*w)*(Sk_trans[i+1]-Sk_trans[i]));
      sum_C_trans[j]+=w*(Ck_trans[i]+(1.0-0.5*w)*(Ck_trans[i+1]-Ck_trans[i]));
      sum_S_trans[j]+=(1.0-w)
	*(Sk_trans[i]+0.5*(1.0-w)*(Sk_trans[i+1]-Sk_trans[i]));
      sum_C_trans[j]+=(1.0-w)
	*(Ck_trans[i]+0.5*(1.0-w)*(Ck_trans[i+1]-Ck_trans[i]));
      sum_S_long[j]+=w*(Sk_long[i]+(1.0-0.5*w)*(Sk_long[i+1]-Sk_long[i]));
      sum_C_long[j]+=w*(Ck_long[i]+(1.0-0.5*w)*(Ck_long[i+1]-Ck_long[i]));
      sum_S_long[j]+=(1.0-w)
	*(Sk_long[i]+0.5*(1.0-w)*(Sk_long[i+1]-Sk_long[i]));
      sum_C_long[j]+=(1.0-w)
	*(Ck_long[i]+0.5*(1.0-w)*(Ck_long[i+1]-Ck_long[i]));
      j++;
    }
  }
}

distribute(double S[],double C[],double sum_S[],double sum_C[],
	   int n,double factor,double z[],int k0)
{
  double ds_inv=1.0,w,s,*Sk,*Ck;
  int i,j,k;

  for(i=0;i<n;i++){
    sum_S[i]=0.0;
    sum_C[i]=0.0;
  }
  for (k=k0-1;k>=0;k--){
    w=factor*(z[k0]-z[k]);
    j=(int)w;
    w-=j;
    if (j>=n) break;
    i=0;
    Sk=S+k*(n+1);
    Ck=C+k*(n+1);
    sum_S[j]+=(1.0-w)*(Sk[i]+0.5*(1.0-w)*(Sk[i+1]-Sk[i]));
    sum_C[j]+=(1.0-w)*(Ck[i]+0.5*(1.0-w)*(Ck[i+1]-Ck[i]));
    j++;
    while (j<n){
      i++;
      sum_S[j]+=w*(Sk[i]+(1.0-0.5*w)*(Sk[i+1]-Sk[i]));
      sum_C[j]+=w*(Ck[i]+(1.0-0.5*w)*(Ck[i+1]-Ck[i]));
      sum_S[j]+=(1.0-w)*(Sk[i]+0.5*(1.0-w)*(Sk[i+1]-Sk[i]));
      sum_C[j]+=(1.0-w)*(Ck[i]+0.5*(1.0-w)*(Ck[i+1]-Ck[i]));
      j++;
    }
  }
}

main()
{
  double z[1000],sum1[10],sum2[10],x1[11000],x2[11000];
  double beta=0.5,sum=0.0,s,s0=5.001,ds_inv,ds=10.0,wgt,factor;
  int i,j,k,m,npoint=10,nstep=2;

  ds/=(double)(npoint);
  ds_inv=1.0/ds;
    
  for (i=0;i<1000;i++){
    //    printf("%d\n",i);
    z[i]=(double)i*1.0;
    for(j=0;j<11;j++){
      x1[i*11+j]=0.0;
      x2[i*11+j]=0.0;
    }
  }
  factor=beta/(1.0-beta);
  for (i=0;i<11;i++){
    x2[11*47+i]=i+1;
  }
  for(j=0;j<1;j++){
    distribute(x1,x2,sum1,sum2,10,factor*ds_inv,z,0);
  }
  for(i=0;i<10;i++){
    printf("%g %g\n",sum1[i],sum2[i]);
  }
  exit(0);
}
