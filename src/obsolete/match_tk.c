#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <tcl.h>
#include <tk.h>

/* function returns 1 if no solution exists 0 otherwise */

int infodo(double rk1,double rl1,double rk2,double rl2,double d,
	   double *alpha1,double *alpha2,double *beta1,double *beta2,
	   double *rmu1,double *rmu2)
{
    double rtot[16],drift,rd[16],rq[16];
    double cos1, cos2;

    drift=d-0.5*(rl1+rl2);
    mkquad(rk2,rl2,rq);
    mkdrift(drift,rd);
    matmul(rq,rd,rtot);
    matmul(rd,rtot,rtot);
    mkquad(rk1,0.5*rl1,rq);
    matmul(rtot,rq,rtot);
    matmul(rq,rtot,rtot);
    cos1=0.5*(rtot[0]+rtot[5]);
    cos2=0.5*(rtot[10]+rtot[15]);
    if (cos1>1.0) {
      return 1;
    }
    if (cos2>1.0) {
      return 1;
    }
    *rmu1 = acos(cos1);
    *rmu2 = acos(cos2);
    if (rtot[1]<0.0) {
	*rmu1 = 2.0*PI-(*rmu1);
    }
    if (rtot[11]<0.0) {
	*rmu2 = 2.0*PI-(*rmu2);
    }
    *alpha1 = (rtot[0]-rtot[5])*0.5/sin(*rmu1);
    *alpha2 = (rtot[10]-rtot[15])*0.5/sin(*rmu2);
    *beta1 = rtot[1]/sin(*rmu1);
    *beta2 = rtot[11]/sin(*rmu2);
    return 0;
}

int tk_MatchList(ClientData clientdata,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error;
  char *l=NULL,**v,**v1;
  double alphax,alphay,betax,betay,mux,muy;
  int n,m,i;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: MatchList options"},
    {(char*)"-list",TK_ARGV_STRING,(char*)NULL,
     (char*)&l,
     (char*)"List with the elements"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }
  if (!l) {
    Tcl_SetResult(interp,"You need to provide a list for the fit",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if(error=Tcl_SplitList(interp,bin_list,&n,&v)) {
    return error;
  }
  for (i=0;i<n;i++){
  }
  return TCL_OK;
}
