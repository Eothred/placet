#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <limits>
#include <readline/readline.h>
#include <readline/history.h>
#include <tcl.h>

#include <gsl/gsl_multifit.h>

#include <fnmatch.h>

#include <octave/config.h>
#include <octave/octave.h>
#include <octave/symtab.h>
#include <octave/parse.h>
#include <octave/unwind-prot.h>
#include <octave/toplev.h>
#include <octave/error.h> 
#include <octave/quit.h>
#include <octave/variables.h>
#include <octave/sighandlers.h>
#include <octave/sysdep.h>
#include <octave/defun.h>

#include "octave_placet.h"

#include "beam.h"
#include "beamline.h"
#include "rmatrix.h"

#include "placeti3.h"
#include "track.h"

#include "quadrupole.h"
#include "structures_def.h"


using std::max;

static Matrix octave_emitt_print()
{
  int j=emitt_data.nswitch;
  Matrix retval(emitt_data.nmax[j], 10);
  for (int i=0;i<emitt_data.nmax[j];i++){
    int n=max(1,emitt_data.n[j][i]);
    retval(i, 0) = i<inter_data.beamline->n_quad ? inter_data.beamline->quad[i]->get_s() : beamline_length(inter_data.beamline);
    retval(i, 1) = emitt_data.ex[j][i]/n;
    retval(i, 2) = sqrt(max(0.0,((double)n*emitt_data.ex2[j][i]-emitt_data.ex[j][i]*emitt_data.ex[j][i])))/(double)n;
    retval(i, 3) = emitt_data.xm[j][i]/(double)n;
    retval(i, 4) = sqrt(emitt_data.xsig[j][i]);
    retval(i, 5) = emitt_data.e[j][i]/n;
    retval(i, 6) = sqrt(max(0.0,((double)n*emitt_data.e2[j][i]-emitt_data.e[j][i]*emitt_data.e[j][i])))/(double)n;
    retval(i, 7) = emitt_data.ym[j][i];
    retval(i, 8) = sqrt(emitt_data.ysig[j][i]);
    retval(i, 9) = emitt_data.n[j][i];
  }
  return retval;
}

octave_value_list octave_test_no_correction(BEAMLINE *beamline, BEAM *bunch0, int niter, void (*survey)(BEAMLINE*), int start, int end, int nargout )
{
  BEAM *tb=bunch_remake(bunch0);

  bool emitt=true;
  if (emitt){
#ifdef MULTI
    emitt_store_init_2(beamline->n_quad,MULTI);
#else
    emitt_store_init(beamline->n_quad);
#endif
  }
  double esumy=0.0,esumx=0.0,esumy2=0.0,esumx2=0.0;
  
  for (int i=0;i<niter;i++) {
    survey(beamline);
    if (i==0 || emitt){
      beam_copy(bunch0,tb);
    } else{
      bunch_copy_0(bunch0,tb);
    }
    if (emitt){
      bunch_track_line_emitt_new(beamline,tb,start,end);
    } else {
      if (i==0){
        bunch_track(beamline,tb,start,end);
      } else{
        bunch_track_0(beamline,tb, start,end);
      }
    }
    double emitty=emitt_y(tb);
    esumy=(esumy*i+emitty)/(double)(i+1);
    esumy2=(esumy2*i+emitty*emitty)/(double)(i+1);
#ifdef TWODIM
    double emittx=emitt_x(tb);
    esumx=(esumx*i+emittx)/(double)(i+1);
    esumx2=(esumx2*i+emittx*emittx)/(double)(i+1);
#endif
    //    retval(i,0) = emittx;
    //    retval(i,1) = emitty;
    //    placet_printf(INFO,"emitt_x %d %g %g %g\n",i,emittx,esumx,
    //	   sqrt(max(0.0,(esumx2-esumx*esumx)/(double)(i+1))));
    //    placet_printf(INFO,"emitt_y %d %g %g %g\n",i,emitty,esumy,
    //	   sqrt(max(0.0,(esumy2-esumy*esumy)/(double)(i+1))));
  }

  octave_value_list retval;
  if(emitt){
    retval(0)=octave_emitt_print();
    emitt_store_delete();
  }
  if (nargout>1) {
    size_t count=0;
    for (int i=0;i<tb->slices;i++) {
      PARTICLE &particle=tb->particle[i];
      if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon())
        count++;
    }
    Matrix B(count, 6);
    if (tb->particle_beam) {
      B.resize(count,6);
      for (int i=0,j=0;i<tb->slices;i++) {
        PARTICLE &particle=tb->particle[i];
        if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
          B(j,0)=particle.energy;
          B(j,1)=particle.x;
          B(j,2)=particle.y;
          B(j,3)=particle.z;
          B(j,4)=particle.xp;
          B(j,5)=particle.yp;
          j++;
        }
      }
    } else {
      B.resize(count,17);
      int n=tb->slices/tb->bunches/tb->macroparticles;
      int nm=tb->macroparticles;
      int nb=tb->bunches;
      for (int j=0,l=0,m=0;j<nb;j++){
        for (int i=0;i<n;i++){
          for (int k=0;k<nm;k++){
            PARTICLE &particle=tb->particle[m];
            if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
              B(l,0)=tb->z_position[j*n+i];
	      B(l,1)=particle.wgt;
	      B(l,2)=particle.energy;
	      B(l,3)=particle.x;
	      B(l,4)=particle.xp;
	      B(l,5)=particle.y;
	      B(l,6)=particle.yp;
	      B(l,7)=tb->sigma_xx[m].r11;
	      B(l,8)=tb->sigma_xx[m].r12;
	      B(l,9)=tb->sigma_xx[m].r22;
	      B(l,10)=tb->sigma[m].r11;
	      B(l,11)=tb->sigma[m].r12;
	      B(l,12)=tb->sigma[m].r22;
	      B(l,13)=tb->sigma_xy[m].r11;
	      B(l,14)=tb->sigma_xy[m].r12;
	      B(l,15)=tb->sigma_xy[m].r21;
	      B(l,16)=tb->sigma_xy[m].r22;
              l++;
            }
            m++;
          }
        }
      }
    }
    retval(1)=B;
  }
  beam_delete(tb);
  return retval;
}

octave_value_list octave_get_response_matrix_fit(BEAMLINE *beamline, BEAM *bunch, void (*survey)(BEAMLINE*), int *bpms, int *corrs, int nbpms, int ncorrs, int nargout  )
{
  octave_value_list ret_value;
  if (BEAM *tb = bunch_remake(bunch)) {
    Matrix Rxx(nbpms, ncorrs);
    Matrix Rxy(nbpms, ncorrs);
    Matrix Ryx(nbpms, ncorrs);
    Matrix Ryy(nbpms, ncorrs);
    Matrix Rxxx(nbpms, ncorrs);
    Matrix Rxxy(nbpms, ncorrs);
    Matrix Rxyy(nbpms, ncorrs);
    Matrix Ryxx(nbpms, ncorrs);
    Matrix Ryyx(nbpms, ncorrs);
    Matrix Ryyy(nbpms, ncorrs);
    int nparticles=tb->slices;
    // polynomial fit using GSL's gsl_multifit_linear
    // 1 fit for the horizontal bpm_readings observables
    // 1 fit for the vertical bpm_readings observables
    // gsl_multifit_linear requires X_ij matrix of predictor variables
    // in our case:
    // X_ij:
    //   i: index 0-5 in the phase space (x x' y y' z d)
    //   j: index 0-26 
    //      0-5 : Rij, n == _j
    //      6-26 : Rijk, n = 6 + ( j*(j-1)/2 + k-1 )         with k<j 
    gsl_vector *x[nbpms];
    gsl_vector *y[nbpms];
    for(int i=0; i<nbpms; i++) {
      x[i] = gsl_vector_alloc(nparticles);
      y[i] = gsl_vector_alloc(nparticles);
    }
    gsl_matrix *P[ncorrs];
    for(int i=0; i<ncorrs; i++) {
      P[i] = gsl_matrix_alloc(nparticles, 2);
    }
    survey(beamline);
    beam_copy(bunch, tb);
    inter_data.bunch=tb;
    ELEMENT **element=beamline->element;
    enum INDEX {
      XP,
      YP,
      XPXP,
      XPYP,
      YPYP,
      INDEX_COUNT
    };
    for (int start=0, ibpm=0, icorr=0; start<=bpms[nbpms-1] && start<beamline->n_elements; start++) {
      // pre-tracking reading
      if (start==bpms[ibpm]) {
	for (int i=0; i<nparticles; i++) {
	  const PARTICLE &particle = tb->particle[i];
	  gsl_vector_set(x[ibpm], i, particle.x);
	  gsl_vector_set(y[ibpm], i, particle.y);
	}
      }
      if (icorr<ncorrs && start==corrs[icorr]) {
	for (int i=0; i<nparticles; i++) {
	  const PARTICLE &particle = tb->particle[i];
	  gsl_matrix_set(P[icorr], i, XP, particle.xp * particle.energy); // 1 kV kick at each corrector, expressed in urad
	  gsl_matrix_set(P[icorr], i, YP, particle.yp * particle.energy); // 1 kV kick at each corrector, expressed in urad
	}
      }
      // tracking
      element[start]->track_0(tb);
      // post-tracking reading
      if (start==bpms[ibpm]) {
	for (int i=0; i<nparticles; i++) {
	  const PARTICLE &particle = tb->particle[i];
	  gsl_vector_set(x[ibpm], i, gsl_vector_get(x[ibpm], i) + particle.x);
	  gsl_vector_set(y[ibpm], i, gsl_vector_get(y[ibpm], i) + particle.y);
	}
	ibpm++;
      }
      if (icorr<ncorrs && start==corrs[icorr]) {
	for (int i=0; i<nparticles; i++) {
	  const PARTICLE &particle = tb->particle[i];
	  gsl_matrix_set(P[icorr], i, XP, gsl_matrix_get(P[icorr], i, XP) + particle.xp * particle.energy); // 1 kV kick at each corrector, expressed in urad
	  gsl_matrix_set(P[icorr], i, YP, gsl_matrix_get(P[icorr], i, YP) + particle.yp * particle.energy); // 1 kV kick at each corrector, expressed in urad
	}
	icorr++;
      }
    }
    beam_delete(tb);
    // it takes the average of the two "readings", before and after bpms and correctors
    // commented out, as it does not change the result of the fit
    // for(int i=0; i<nbpms; i++) {
    //   gsl_vector_scale(x[i], 0.5);
    //   gsl_vector_scale(y[i], 0.5);
    // }
    // for(int i=0; i<ncorrs; i++) {
    //   gsl_matrix_scale(P[i], 0.5);
    // }
    // it performs the fits
    if (gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc(nparticles, INDEX_COUNT)) {
      if (gsl_matrix *X = gsl_matrix_alloc(nparticles, INDEX_COUNT)) {
	if (gsl_matrix *cov = gsl_matrix_alloc(INDEX_COUNT, INDEX_COUNT)) {
	  if (gsl_vector *c = gsl_vector_alloc(INDEX_COUNT)) {
	    for (int j=0; j<ncorrs; j++) {
	      // it prepares the model 'X'
	      for (int n=0; n<nparticles; n++) {
		gsl_vector_const_view particle = gsl_matrix_const_row(P[j], n);
		const gsl_vector *p = &particle.vector;
		gsl_matrix_set(X, n, XP, gsl_vector_get(p, XP));
		gsl_matrix_set(X, n, YP, gsl_vector_get(p, YP));
		gsl_matrix_set(X, n, XPXP, gsl_vector_get(p, XP) * gsl_vector_get(p, XP));
		gsl_matrix_set(X, n, XPYP, gsl_vector_get(p, XP) * gsl_vector_get(p, YP));
		gsl_matrix_set(X, n, YPYP, gsl_vector_get(p, YP) * gsl_vector_get(p, YP));
	      }
	      // double Ej = element[corrs[j]]->get_ref_energy();
	      for (int i=0; i<nbpms; i++) {
		if (corrs[j]<=bpms[i]) {
		  double chisq;
		  gsl_multifit_linear(X, x[i], c, cov, &chisq, work);
		  double R12 = gsl_vector_get(c, XP);
		  double R14 = gsl_vector_get(c, YP);
		  double R122 = gsl_vector_get(c, XPXP);
		  double R124 = gsl_vector_get(c, XPYP);
		  double R144 = gsl_vector_get(c, YPYP);
		  gsl_multifit_linear(X, y[i], c, cov, &chisq, work);
		  double R32 = gsl_vector_get(c, XP);
		  double R34 = gsl_vector_get(c, YP);
		  double R322 = gsl_vector_get(c, XPXP);
		  double R342 = gsl_vector_get(c, XPYP);
		  double R344 = gsl_vector_get(c, YPYP);
		  Rxx(i,j) = R12; // / Ej;
		  Rxy(i,j) = R14; // / Ej;
		  Ryx(i,j) = R32; // / Ej;
		  Ryy(i,j) = R34; // / Ej;
		  Rxxx(i,j) = R122; // / Ej / Ej;
		  Rxxy(i,j) = R124; // / Ej / Ej;
		  Rxyy(i,j) = R144; // / Ej / Ej;
		  Ryxx(i,j) = R322; // / Ej / Ej;
		  Ryyx(i,j) = R342; // / Ej / Ej;
		  Ryyy(i,j) = R344; // / Ej / Ej;
		} else {
		  Rxx(i,j) = 0.;
		  Rxy(i,j) = 0.;
		  Ryx(i,j) = 0.;
		  Ryy(i,j) = 0.;
		  Rxxx(i,j) = 0.;
		  Rxxy(i,j) = 0.;
		  Rxyy(i,j) = 0.;
		  Ryxx(i,j) = 0.;
		  Ryyx(i,j) = 0.;
		  Ryyy(i,j) = 0.;
		}
	      }
	      gsl_matrix_free(P[j]);
	      P[j]=NULL;
	    }
	    gsl_vector_free(c);
	  }
	  gsl_matrix_free(cov);
	}
	gsl_matrix_free(X);
      }
      gsl_multifit_linear_free(work);
    }
    for(int i=0; i<nbpms; i++) {
      if (x[i]) gsl_vector_free(x[i]);
      if (y[i]) gsl_vector_free(y[i]);
    }
    for(int i=0; i<ncorrs; i++) {
      if (P[i]) gsl_matrix_free(P[i]);
    }
    ret_value(0) = Rxx;
    if (nargout>1) ret_value(1) = Rxy;
    if (nargout>2) ret_value(2) = Ryx;
    if (nargout>3) ret_value(3) = Ryy;
    if (nargout>4) ret_value(4) = Rxxx;
    if (nargout>5) ret_value(5) = Rxxy;
    if (nargout>6) ret_value(6) = Rxyy;
    if (nargout>7) ret_value(7) = Ryxx;
    if (nargout>8) ret_value(8) = Ryyx;
    if (nargout>9) ret_value(9) = Ryyy;
  }
  return ret_value; 
}

octave_value_list octave_get_transfer_matrix_fit(BEAMLINE *beamline, BEAM *bunch, void (*survey)(BEAMLINE*), int _start, int _end, int nargout  )
{
  octave_value_list ret_value;
  if (BEAM *tb = bunch_remake(bunch)) {
    int nparticles=tb->slices;
    beam_copy(bunch, tb);
    inter_data.bunch=tb;
    survey(beamline);
    if (gsl_matrix *P = gsl_matrix_alloc(nparticles, 6)) {
      gsl_vector *x[6];
      for(int i=0; i<6; i++) {
	x[i] = gsl_vector_alloc(nparticles);
      }
      {
	double e0 = beamline->element[_start]->get_ref_energy();
	for (int i=0; i<nparticles; i++) {
	  const PARTICLE &particle = tb->particle[i];
	  gsl_matrix_set(P, i, 0, particle.x * 1e-6);
	  gsl_matrix_set(P, i, 1, particle.xp * 1e-6);
	  gsl_matrix_set(P, i, 2, particle.y * 1e-6);
	  gsl_matrix_set(P, i, 3, particle.yp * 1e-6);
	  gsl_matrix_set(P, i, 4, particle.z * 1e-6);
	  gsl_matrix_set(P, i, 5, (particle.energy - e0) / e0);
	}
	for (int start=_start; start<=_end; start++) {
	  beamline->element[start]->track_0(tb);
	}
	e0 = beamline->element[_end]->get_ref_energy();
	for (int i=0; i<nparticles; i++) {
	  const PARTICLE &particle = tb->particle[i];
	  gsl_vector_set(x[0], i, particle.x * 1e-6);
	  gsl_vector_set(x[1], i, particle.xp * 1e-6);
	  gsl_vector_set(x[2], i, particle.y * 1e-6);
	  gsl_vector_set(x[3], i, particle.yp * 1e-6);
	  gsl_vector_set(x[4], i, particle.z * 1e-6);
	  gsl_vector_set(x[5], i, (particle.energy - e0) / e0);
	}
      }
      // polynomial fit using GSL's gsl_multifit_linear
      // 1 fit for the horizontal bpm_readings observables
      // 1 fit for the vertical bpm_readings observables
      // gsl_multifit_linear requires X_ij matrix of predictor variables
      // in our case:
      // X_ij:
      //   i: index 0-5 in the phase space (x x' y y' z d)
      //   j: index 0-26 
      //      0-5 : Rij, n == _j
      //      6-26 : Rijk, n = 6 + ( j*(j-1)/2 + k-1 ) with k<j 
      if (gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc(nparticles, 27)) {
	if (gsl_matrix *X = gsl_matrix_alloc(nparticles, 27)) {
	  if (gsl_matrix *cov = gsl_matrix_alloc(27, 27)) {
	    if (gsl_vector *c = gsl_vector_alloc(27)) {
#ifdef INDEX
#undef INDEX
#endif
#define INDEX(J,K)  (((K)<=(J))?((J)*((J)+1)/2+(K)):((K)*((K)+1)/2+(J)))
	      for (int n=0; n<nparticles; n++) {
		gsl_vector_const_view particle = gsl_matrix_const_row(P, n);
		const gsl_vector *p = &particle.vector;
		for (int j=0; j<6; j++) {
		  gsl_matrix_set(X, n, j, gsl_vector_get(p, j));
		  for (int k=0; k<=j; k++) {
		    gsl_matrix_set(X, n, 6+INDEX(j,k), gsl_vector_get(p, j) * gsl_vector_get(p, k));
		  }
		}
	      }
	      Matrix R(6,6);
	      dim_vector dv3;
	      dv3.resize(3);
	      dv3.elem(0)=6;	      
	      dv3.elem(1)=6;
	      dv3.elem(2)=6;
	      NDArray T(dv3);
	      for (int i=0; i<6; i++) {
		double chisq;
		gsl_multifit_linear(X, x[i], c, cov, &chisq, work);
		for (int j=0; j<6; j++) {
		  R(i,j) = gsl_vector_get(c, j);
		  T(i,j,j) = gsl_vector_get(c, 6+INDEX(j,j)) / 2;
		  for (int k=0; k<j; k++) {
		    T(i,j,k) = T(i,k,j) = gsl_vector_get(c, 6+INDEX(j,k)) / 2;
		  }
		}
	      }
	      ret_value(0) = R;
	      if (nargout>1) {
		ret_value(1) = T;
	      }
 	      gsl_vector_free(c);
	    }
	    gsl_matrix_free(cov);
	  }
	  gsl_matrix_free(X);
	}
	gsl_multifit_linear_free(work);
      }
      for(int i=0; i<6; i++) {
	if (x[i]) gsl_vector_free(x[i]);
      }
      gsl_matrix_free(P);
    }
    beam_delete(tb);
  }
  return ret_value; 
}
