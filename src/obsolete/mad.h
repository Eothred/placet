#ifndef mad_h
#define mad_h

class MAD : public ELEMENT {
  char *file,*lattice;
 public:
  virtual void step_4d_0(BEAM*);
  virtual void step_4d(BEAM*); 
  virtual void list(FILE*);
  void set_file(char *n);
  void set_lattice(char *n);
  char* get_file() {return file;};
  char* get_lattice() {return lattice;};
};

int
tk_Mad(ClientData clientdata,Tcl_Interp *interp,int argc,
       char *argv[]);

#endif // mad_h
