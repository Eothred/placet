#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "old/fftw/fftw-2.1.3/fftw/fftw.h"

#define TWOPI 6.283185307179586

//#define Noff 0
//#define Npoint 100
//#define M 11

main(int argc,char *argv[])
{
  char buffer[1000],*point;
  FILE *f;
  int i,m=0,j,single=1,ns,nm,M,Npoint,Noff=0;
  double dummy,*wgt,y2,yp2,yyp,y,yp,wgtsum,y_norm,yp_norm,y2_norm,yp2_norm,
      yyp_norm,y_full_norm,yp_full_norm,*e,emean;
  double y_im,yp_im,y2_im,yp2_im,yyp_im;
  double y_zero,yim_zero,yp_zero,ypim_zero,yyp_zero,yypim_zero;
  fftw_complex *in,*in2,*norm,*norm2;
  fftw_plan p,p2;

  snprintf(buffer,1000,"%s%d\0",argv[1],Noff);
  f=fopen(buffer,"r");
  point=fgets(buffer,1000,f);
  fclose(f);
  dummy=strtol(point,&point,10);
  ns=strtol(point,&point,10);
  nm=strtol(point,&point,10);
  M=ns*nm;

  Noff=strtol(argv[2],NULL,10);
  Npoint=strtol(argv[3],NULL,10);

  if (single) {
      in=(fftw_complex*)malloc(sizeof(fftw_complex)*2*M*Npoint);
      norm=(fftw_complex*)malloc(sizeof(fftw_complex)*2*M*Npoint);
      norm2=(fftw_complex*)malloc(sizeof(fftw_complex)*2*M*Npoint);
      p = fftw_create_plan_specific(2*Npoint,FFTW_FORWARD,
				    FFTW_ESTIMATE | FFTW_IN_PLACE,
				    in,M,NULL,M);
      in2=(fftw_complex*)malloc(sizeof(fftw_complex)*2*M*Npoint);
      p2 = fftw_create_plan_specific(2*Npoint,FFTW_FORWARD,
				     FFTW_ESTIMATE | FFTW_IN_PLACE,
				     in2,M,NULL,M);
  }
  else {
      in=(fftw_complex*)malloc(sizeof(fftw_complex)*M*Npoint);
      norm=(fftw_complex*)malloc(sizeof(fftw_complex)*M*Npoint);
      norm2=(fftw_complex*)malloc(sizeof(fftw_complex)*M*Npoint);
      p = fftw_create_plan_specific(Npoint,FFTW_FORWARD,
				    FFTW_ESTIMATE | FFTW_IN_PLACE,
				    in,M,NULL,M);
      in2=(fftw_complex*)malloc(sizeof(fftw_complex)*M*Npoint);
      p2 = fftw_create_plan_specific(Npoint,FFTW_FORWARD,
				     FFTW_ESTIMATE | FFTW_IN_PLACE,
				     in2,M,NULL,M);
  }
  e=(double*)malloc(sizeof(double)*M);
  wgt=(double*)malloc(sizeof(double)*M);

  for (i=Noff;i<Noff+Npoint;i++){
    snprintf(buffer,1000,"%s%d\0",argv[1],i);
    f=fopen(buffer,"r");
    point=fgets(buffer,1000,f);
    for (j=0;j<M;j++){
      point=fgets(buffer,1000,f);
      dummy=strtod(point,&point);
      wgt[j]=strtod(point,&point);
      e[j]=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      dummy=strtod(point,&point);
      in[m].re=dummy;
      in[m].im=0.0;
      dummy=strtod(point,&point);
      in2[m].re=dummy;
      in2[m].im=0.0;
      norm[m].re=0.0;
      norm[m].im=0.0;
      norm2[m].re=0.0;
      norm2[m].im=0.0;
      m++;
    }
    fclose(f);
  }
  if (single) {
      for (i=0;i<Npoint;i++){
	  for (j=0;j<M;j++){
	      in[m].re=0.0;
	      in[m].im=0.0;
	      in2[m].re=0.0;
	      in2[m].im=0.0;
	      norm[m].re=0.0;
	      norm[m].im=0.0;
	      norm2[m].re=0.0;
	      norm2[m].im=0.0;
	      m++;
	  }
      }
  }
  emean=0.0;
  wgtsum=0.0;
  y_full_norm=0.0;
  yp_full_norm=0.0;
  for (i=0;i<M;i++){
      wgtsum+=wgt[i];
      norm[i].re=in[i].re;
      norm[i].im=in[i].im;
      norm2[i].re=in2[i].re;
      norm2[i].im=in2[i].im;
      y_full_norm+=wgt[i]*in[i].re;
      yp_full_norm+=wgt[i]*in2[i].re;
      emean+=e[i]*wgt[i];
  }
  emean/=wgtsum;
  y_full_norm=fabs(y_full_norm/wgtsum);
  yp_full_norm=fabs(yp_full_norm/wgtsum);
  fftw(p,M,in,M,1,NULL,1,1);
  fftw(p2,M,in2,M,1,NULL,1,1);
  fftw(p2,M,norm,M,1,NULL,1,1);
  fftw(p2,M,norm2,M,1,NULL,1,1);
  for (i=0;i<Npoint;i++){
      y=0.0;
      yp=0.0;
      y2=0.0;
      yp2=0.0;
      yyp=0.0;
      y_norm=0.0;
      yp_norm=0.0;
      y2_norm=0.0;
      yp2_norm=0.0;
      yyp_norm=0.0;
      y_im=0.0;
      y2_im=0.0;
      yp_im=0.0;
      yp2_im=0.0;
      yyp_im=0.0;
      y_zero=0.0;
      yim_zero=0.0;
      yp_zero=0.0;
      ypim_zero=0.0;
      yyp_zero=0.0;
      yypim_zero=0.0;
      for (j=0;j<M;j++){
	  y+=in[i*M+j].re*wgt[j];
	  y_im+=in[i*M+j].im*wgt[j];
	  yp+=in2[i*M+j].re*wgt[j];
	  yp_im+=in2[i*M+j].im*wgt[j];
	  y_norm+=norm[i*M+j].im*wgt[j];
	  yp_norm+=norm2[i*M+j].im*wgt[j];
      }
      y/=wgtsum;
      y_im/=wgtsum;
      yp/=wgtsum;
      yp_im/=wgtsum;
      y_norm/=wgtsum;
      yp_norm/=wgtsum;
      for (j=0;j<M;j++){
	  y2+=(in[i*M+j].re-y)*(in[i*M+j].re-y)*wgt[j]*Npoint*Npoint*4;
	  yp2+=(in2[i*M+j].re-yp)*(in2[i*M+j].re-yp)*wgt[j]*Npoint*Npoint*4;
	  //	  y2+=(in[i*M+j].re-y)*(in2[i*M+j].re-yp)*wgt[j]*Npoint*Npoint*4;
	  yyp+=(in[i*M+j].re-y)*(in2[i*M+j].re-yp)*wgt[j]*Npoint*Npoint*4;
	  y2_im+=(in[i*M+j].im-y_im)*(in[i*M+j].im-y_im)
	      *wgt[j]*Npoint*Npoint*4;
	  yp2_im+=(in2[i*M+j].im-yp_im)*(in2[i*M+j].im-yp_im)
	      *wgt[j]*Npoint*Npoint*4;
	  //	  y2_im+=(in[i*M+j].im-y_im)*(in2[i*M+j].im-yp_im)
	  //	      *wgt[j]*Npoint*Npoint*4;
	  yyp_im+=(in[i*M+j].im-y_im)*(in2[i*M+j].im-yp_im)
	      *wgt[j]*Npoint*Npoint*4;
	  y2_norm+=(norm[i*M+j].re-y_norm)*(norm[i*M+j].re-y_norm)
	      *wgt[j]*Npoint*Npoint*4;
	  yp2_norm+=(norm2[i*M+j].re-yp_norm)*(norm2[i*M+j].re-yp_norm)
	      *wgt[j]*Npoint*Npoint*4;
	  y2_norm+=(norm[i*M+j].re-y_norm)*(norm2[i*M+j].re-yp_norm)
	      *wgt[j]*Npoint*Npoint*4;
	  y_zero+=in[i*M+j].re*in[i*M+j].re*wgt[j];
	  yim_zero+=in[i*M+j].im*in[i*M+j].im*wgt[j];
	  yp_zero+=in2[i*M+j].re*in2[i*M+j].re*wgt[j];
	  ypim_zero+=in2[i*M+j].im*in2[i*M+j].im*wgt[j];
	  yyp_zero+=in[i*M+j].re*in2[i*M+j].re*wgt[j];
	  yypim_zero+=in[i*M+j].im*in2[i*M+j].im*wgt[j];
      }
      y2/=wgtsum;
      yp2/=wgtsum;
      yyp/=wgtsum;
      y2_im/=wgtsum;
      yp2_im/=wgtsum;
      yyp_im/=wgtsum;
      y2_norm/=wgtsum;
      yp2_norm/=wgtsum;
      yyp_norm/=wgtsum;
      y_zero/=wgtsum;
      yim_zero/=wgtsum;
      yp_zero/=wgtsum;
      ypim_zero/=wgtsum;
      yyp_zero/=wgtsum;
      yypim_zero/=wgtsum;
//      printf("%d %g %g %g %g\n",i,sqrt(y2*yp2-yyp*yyp)/sqrt(y2_norm*yp2_norm-yyp_norm*yyp_norm),y/y_norm,yp/yp_norm,2*Npoint*sqrt(y*y+65.0*65.0*yp*yp)/(2*Npoint));
//      printf("%d %g %g %g %g\n",i,sqrt(y2*yp2-yyp*yyp),y*2*Npoint,yp*2*Npoint,2*Npoint*sqrt(y*y+65.0*65.0*yp*yp));
      printf("%d %g %g %g %g %g %g %g %g %g %g\n",i,
	     emean/0.511e-3*1e-6*sqrt(y2*yp2-yyp*yyp)/(4*Npoint*Npoint),
	     emean/0.511e-3*1e-6*sqrt(y2_im*yp2_im-yyp_im*yyp_im)
	     /(4*Npoint*Npoint),
	     emean/0.511e-3*1e-6*sqrt(y_zero*yp_zero-yyp_zero*yyp_zero),
	     emean/0.511e-3*1e-6*sqrt(yim_zero*ypim_zero-yypim_zero*yypim_zero),
	     y,y_im,yp,yp_im,sqrt(y*y+y_im*y_im)/y_full_norm,
	     sqrt(yp*yp+yp_im*yp_im)/yp_full_norm);
  }
  exit(0);
}

