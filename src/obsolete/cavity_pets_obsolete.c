void cavity_step_pets(ELEMENT *element,BEAM *bunch)
{
  R_MATRIX r1,r2;
int i_b,i_s,i_m,i_step,nhelp,i_z,i_part,n_macro;
static double *S_long,*C_long,*S_transv,*C_transv;
static double *S_b_long=NULL,*C_b_long,*S_b_transv,*C_b_transv;
double S_s_long,C_s_long,S_s_transv,C_s_transv;
static double *rho_S_long=NULL,*rho_C_long=NULL,
  *rho_S_transv=NULL,*rho_C_transv=NULL;
double f_long,f_transv,de,de0,kicky,kickx,rf_ampl,rf_ampl2;
double length,half_length,length_i,gammap,tmpy,tmpx,tmp,tmp2,delta,lndelta;
static int nsize=0;
double rf_x,rf_y,c_cav,s_cav,enhance;
int i_rf,n_rf;
#ifdef TWODIM
static double *S_b_transv_x,*C_b_transv_x,*S_transv_x,*C_transv_x,
  *rho_S_transv_x,*rho_C_transv_x;
#endif

n_rf=drive_data.n_rf;

/*
 */

 if (bunch->macroparticles==1) {
   sort_beam(bunch);
   longrange_fill_new(bunch,drive_data.lambda_transverse);
   longrange_fill_long_new(bunch,drive_data.lambda_longitudinal);
 }

/*
 */

if((drive_data.rf_kick)||(drive_data.rf_long)){
  c_cav=cos(element->v1)*drive_data.rf_a0_i;
  s_cav=sin(element->v1)*drive_data.rf_a0_i;
}
f_long=drive_data.beta_group_l/(1.0-drive_data.beta_group_l)*1e-6
*CAVSTEP/element->length;
f_transv=drive_data.beta_group_t/(1.0-drive_data.beta_group_t)*1e-6
*CAVSTEP/element->length;

if (S_b_long==NULL){
  S_b_long=(double*)malloc(sizeof(double)*(CAVSTEP+1));
  C_b_long=(double*)malloc(sizeof(double)*(CAVSTEP+1));
  S_b_transv=(double*)malloc(sizeof(double)*(CAVSTEP+1));
  C_b_transv=(double*)malloc(sizeof(double)*(CAVSTEP+1));
  S_long=(double*)malloc(sizeof(double)*CAVSTEP);
  C_long=(double*)malloc(sizeof(double)*CAVSTEP);
  S_transv=(double*)malloc(sizeof(double)*CAVSTEP);
  C_transv=(double*)malloc(sizeof(double)*CAVSTEP);
#ifdef TWODIM
  S_b_transv_x=(double*)malloc(sizeof(double)*(CAVSTEP+1));
  C_b_transv_x=(double*)malloc(sizeof(double)*(CAVSTEP+1));
  S_transv_x=(double*)malloc(sizeof(double)*CAVSTEP);
  C_transv_x=(double*)malloc(sizeof(double)*CAVSTEP);
#endif
}
if (nsize<bunch->bunches){
  nsize=bunch->bunches;
  rho_S_long=(double*)realloc(rho_S_long,sizeof(double)*(CAVSTEP+1)*nsize);
  rho_C_long=(double*)realloc(rho_C_long,sizeof(double)*(CAVSTEP+1)*nsize);
  rho_S_transv=(double*)realloc(rho_S_transv,sizeof(double)*(CAVSTEP+1)*nsize);
  rho_C_transv=(double*)realloc(rho_C_transv,sizeof(double)*(CAVSTEP+1)*nsize);
#ifdef TWODIM
  rho_S_transv_x=(double*)realloc(rho_S_transv_x,sizeof(double)*(CAVSTEP+1)*nsize);
  rho_C_transv_x=(double*)realloc(rho_C_transv_x,sizeof(double)*(CAVSTEP+1)*nsize);
#endif
}


n_macro=bunch->macroparticles;
length=element->length/(double)CAVSTEP;
half_length=0.5*length;
length_i=1.0/length;

for (i_b=0;i_b<bunch->bunches;i_b++){
  distribute_cav(rho_S_long,rho_C_long,S_long,C_long,CAVSTEP,f_long,
	     bunch->drive_data->bunch_z,i_b);
  distribute_cav_Q(rho_S_transv,rho_C_transv,S_transv,C_transv,CAVSTEP,
		   f_transv,bunch->drive_data->bunch_z,
		   bunch->drive_data->along,i_b);
#ifdef TWODIM
  distribute_cav_Q(rho_S_transv_x,rho_C_transv_x,S_transv_x,C_transv_x,CAVSTEP,
		   f_transv,bunch->drive_data->bunch_z,
		   bunch->drive_data->along,i_b);
#endif
  for (nhelp=0;nhelp<CAVSTEP+1;nhelp++){
    S_b_long[nhelp]=0.0;
    C_b_long[nhelp]=0.0;
    S_b_transv[nhelp]=0.0;
    C_b_transv[nhelp]=0.0;
#ifdef TWODIM
    S_b_transv_x[nhelp]=0.0;
    C_b_transv_x[nhelp]=0.0;
#endif
  }
  for (i_s=0;i_s<bunch->slices_per_bunch;i_s++){
    i_z=i_b*bunch->slices_per_bunch+i_s;
    for (i_step=0;i_step<CAVSTEP;i_step++){
      tmp=0.0;
      tmpy=0.0;
#ifdef TWODIM
      tmpx=0.0;
#endif
      for (i_m=0;i_m<bunch->macroparticles;i_m++){
	tmp+=bunch->particle[i_z*n_macro+i_m].wgt;
	tmpy+=tmp*bunch->particle[i_z*n_macro+i_m].y;
#ifdef TWODIM
	tmpx+=tmp*bunch->particle[i_z*n_macro+i_m].x;
#endif
      }
      /*
	change for variable z-position
      */
      S_s_long=tmp*bunch->drive_data->al[i_z];
      C_s_long=tmp*bunch->drive_data->bl[i_z];
      S_b_transv[i_step]+=tmpy*bunch->drive_data->bf[i_z];
      C_b_transv[i_step]-=tmpy*bunch->drive_data->af[i_z];
#ifdef TWODIM
      S_b_transv_x[i_step]+=tmpx*bunch->drive_data->bf[i_z];
      C_b_transv_x[i_step]-=tmpx*bunch->drive_data->af[i_z];
#endif

      /* energy loss */

      de=(S_long[i_step]+S_b_long[i_step]+0.5*S_s_long)
	*bunch->drive_data->al[i_z]+
	(C_long[i_step]+C_b_long[i_step]+0.5*C_s_long)
	*bunch->drive_data->bl[i_z];
      de*=bunch->drive_data->factor_long;

      /* transverse kick */

      kicky=(S_transv[i_step]+S_b_transv[i_step])*bunch->drive_data->af[i_z]
	+(C_transv[i_step]+C_b_transv[i_step])*bunch->drive_data->bf[i_z];
      kicky*=length*bunch->drive_data->along[0];

#ifdef TWODIM
      kickx=(S_transv_x[i_step]+S_b_transv_x[i_step])
	*bunch->drive_data->af[i_z]
	+(C_transv_x[i_step]+C_b_transv_x[i_step])
	*bunch->drive_data->bf[i_z];
      kickx*=length*bunch->drive_data->along[0];
#endif

      /* factor for nonlinear RF-kick */

      if(drive_data.rf_kick){
	rf_ampl=-(S_long[i_step]+S_b_long[i_step])*bunch->drive_data->bl[i_z]+
	  (C_long[i_step]+C_b_long[i_step])*bunch->drive_data->al[i_z];
	rf_ampl2=rf_ampl*fabs(element->v1)*bunch->drive_data->factor_kick;

	rf_ampl*=bunch->drive_data->factor_kick;
      }

      for (i_m=0;i_m<bunch->macroparticles;i_m++){
	i_part=i_z*n_macro+i_m;

	de0=de;

	/* rf long */
	if(drive_data.rf_long){
	  enhance=1.0;
	  for(i_rf=0;i_rf<n_rf;i_rf++){
#ifdef TWODIM
	    rf_long2p(drive_data.rf_order*(i_rf+1),
		      drive_data.rf_size[i_rf],
		      half_length,element->v1,bunch->particle[i_part].x,
		      bunch->particle[i_part].xp,
		      bunch->particle[i_part].y,
		      bunch->particle[i_part].yp,
		      c_cav,s_cav,
		      &tmpx);
#else
	    rf_long2p(drive_data.rf_order*(i_rf+1),
		      drive_data.rf_size[i_rf],
		      half_length,element->v1,0.0,0.0,
		      bunch->particle[i_part].y,
		      bunch->particle[i_part].yp,
		      c_cav,s_cav,
		      &tmpx);
#endif
	    enhance+=tmpx;
	  }
	  //	  printf("%g %g %g %g\n",rf_x,tmpx,c_cav,bunch->particle[i_part].x);
	  de0*=enhance;
	}
	
        delta=half_length*de0/bunch->particle[i_part].energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
	gammap=delta*length_i;
	bunch->particle[i_part].yp-=gammap*bunch->particle[i_part].y;
#endif
	if (delta>0.01) {
	    tmp2=log1p(delta)/delta;
	}
	else {
	    tmp2=1.0-delta*(0.5+delta*0.3308345316809);
	}
	tmp=1.0/(1.0+delta);
#else
	gammap=delta*length_i;
#ifdef END_FIELDS
	bunch->particle[i_part].yp-=gammap*bunch->particle[i_part].y;
#endif
	tmp=(1.0-gammap*length);
#endif
    
#ifdef CAV_PRECISE
	bunch->particle[i_part].y+=half_length*bunch->particle[i_part].yp*tmp2;
#else
	bunch->particle[i_part].y+=half_length*bunch->particle[i_part].yp;
#endif
	bunch->particle[i_part].yp*=tmp;

#ifdef TWODIM
#ifdef END_FIELDS
	bunch->particle[i_part].xp-=gammap*bunch->particle[i_part].x;
#endif
#ifdef CAV_PRECISE
	bunch->particle[i_part].x+=half_length*bunch->particle[i_part].xp*tmp2;
#else
	bunch->particle[i_part].x+=half_length*bunch->particle[i_part].xp;
#endif
	bunch->particle[i_part].xp*=tmp;
#endif
	bunch->particle[i_part].energy+=half_length*de0;

	/* include the rotation due to the endfields */
#ifdef END_FIELDS
#ifdef CAV_PRECISE
	if (delta>0.01) {
	    lndelta=0.5*log1p(2.0*delta);
	}
	else {
	    lndelta=delta*(1.0-delta*(1.0+delta*1.3233381267236));
	}
//	lndelta=0.5*log(1.0+2.0*delta);
#else
	lndelta=delta;
#endif

	r1.r11=1.0-lndelta;
	r1.r12=length*lndelta/delta;
	r1.r21=-delta/(length*(1.0+2.0*delta))*lndelta;
	r1.r22=(1.0+lndelta)/(1.0+2.0*delta);
	r2.r11=r1.r11;
	r2.r12=r1.r21;
	r2.r21=r1.r12;
	r2.r22=r1.r22;
#else
#ifdef CAV_PRECISE
	if (delta>0.01) {
	    lndelta=0.5*log1p(2.0*delta);
	}
	else {
	    lndelta=delta*(1.0-delta*(1.0+delta*1.3233381267236));
	}
	r1.r12=length*lndelta/delta;
	r2.r21=r1.r12;
#endif
	r1.r22=1.0/(1.0+2.0*delta);
	r2.r22=r1.r22;
#endif
	mult_M_M(&r1,bunch->sigma+i_part,bunch->sigma+i_part);
	mult_M_M(bunch->sigma+i_part,&r2,bunch->sigma+i_part);
#ifdef TWODIM
	mult_M_M(&r1,bunch->sigma_xx+i_part,bunch->sigma_xx+i_part);
	mult_M_M(bunch->sigma_xx+i_part,&r2,bunch->sigma_xx+i_part);
	mult_M_M(&r1,bunch->sigma_xy+i_part,bunch->sigma_xy+i_part);
	mult_M_M(bunch->sigma_xy+i_part,&r2,bunch->sigma_xy+i_part);
#endif

	//kicks and suchlike


	/* kick from nonlinearity */

	if(drive_data.rf_kick){
#ifdef TWODIM
	  rf_x=0.0;
	  rf_y=0.0;
	  for(i_rf=0;i_rf<n_rf;i_rf++){	
	    rf_kick2p(drive_data.rf_order*(i_rf+1),
		     drive_data.rf_size[i_rf]*rf_ampl
		     /bunch->particle[i_part].energy,
		     half_length,element->v1,bunch->particle[i_part].x,
		     bunch->particle[i_part].xp,
		     bunch->particle[i_part].y,
		     bunch->particle[i_part].yp,
		     c_cav,s_cav,
		     &tmpx,&tmpy);
		    rf_x+=tmpx;
	    rf_y+=tmpy;
	  }
	  bunch->particle[i_part].x+=0.5*half_length*rf_x;
	  bunch->particle[i_part].xp+=rf_x;
	  bunch->particle[i_part].y+=0.5*half_length*rf_y;
	  bunch->particle[i_part].yp+=rf_y;
#else
	  tmpy=rf_kick48(drive_data.rf_a0_i,rf_ampl2,rf_ampl,length,
			 bunch->particle[i_part].y,
			 bunch->particle[i_part].yp,0.0,half_length)
	    /bunch->particle[i_part].energy;
 printf("%g %g\n",bunch->particle[i_part].y,tmpy);
	  bunch->particle[i_part].y+=0.5*half_length*tmpy;
	  bunch->particle[i_part].yp+=tmpy;
#endif
	}

	/* wakefield kick */

	bunch->particle[i_part].yp+=kicky/bunch->particle[i_part].energy;
#ifdef TWODIM
	bunch->particle[i_part].xp+=kickx/bunch->particle[i_part].energy;
#endif

	/* kick from nonlinearity */

	if(drive_data.rf_kick){
#ifdef TWODIM
	  rf_x=0.0;
	  rf_y=0.0;
	  for(i_rf=0;i_rf<n_rf;i_rf++){
	    rf_kick2p(drive_data.rf_order*(i_rf+1),
		     drive_data.rf_size[i_rf]*rf_ampl
		     /bunch->particle[i_part].energy,
		     half_length,element->v1,bunch->particle[i_part].x,
		     bunch->particle[i_part].xp,
		     bunch->particle[i_part].y,
		     bunch->particle[i_part].yp,
		     c_cav,s_cav,
		     &tmpx,&tmpy);
	    rf_x+=tmpx;
	    rf_y+=tmpy;
	  }
	  bunch->particle[i_part].x-=0.5*half_length*rf_x;
	  bunch->particle[i_part].xp+=rf_x;
	  bunch->particle[i_part].y-=0.5*half_length*rf_y;
	  bunch->particle[i_part].yp+=rf_y;
#else
	  tmpy=rf_kick48(drive_data.rf_a0_i,rf_ampl2,rf_ampl,length,
			 bunch->particle[i_part].y
			 +half_length*bunch->particle[i_part].yp,
			 bunch->particle[i_part].yp,half_length,length)
	    /bunch->particle[i_part].energy;
	  bunch->particle[i_part].y-=0.5*half_length*tmpy;
	  bunch->particle[i_part].yp+=tmpy;
#endif
	}

#ifdef CAV_PRECISE
	delta=half_length*de0/bunch->particle[i_part].energy;
#ifdef END_FIELDS
	gammap=delta/length;
#endif
	if (delta>0.01) {
	    tmp2=log1p(delta)/delta;
	}
	else {
	    tmp2=1.0-delta*(0.5+delta*0.3308345316809);
	}
	tmp=1.0/(1.0+delta);
#else
	gammap=half_length*de0/(bunch->particle[i_part].energy*length);
	tmp=(1.0-gammap*length);
#endif

#ifdef CAV_PRECISE
	bunch->particle[i_part].y+=half_length*bunch->particle[i_part].yp*tmp2;
#else
	bunch->particle[i_part].y+=half_length*bunch->particle[i_part].yp;
#endif
#ifdef END_FIELDS
	bunch->particle[i_part].yp+=gammap*bunch->particle[i_part].y;
#endif
	bunch->particle[i_part].yp*=tmp;

#ifdef TWODIM
#ifdef CAV_PRECISE
	bunch->particle[i_part].x+=half_length*bunch->particle[i_part].xp*tmp2;
#else
	bunch->particle[i_part].x+=half_length*bunch->particle[i_part].xp;
#endif
#ifdef END_FIELDS
	bunch->particle[i_part].xp+=gammap*bunch->particle[i_part].x;
#endif
	bunch->particle[i_part].xp*=tmp;
#endif

	bunch->particle[i_part].energy+=half_length*de0;
      }          
      S_b_long[i_step]+=S_s_long;
      C_b_long[i_step]+=C_s_long;
    }
    tmpy=0.0;
#ifdef TWODIM
    tmpx=0.0;
#endif
    for (i_m=0;i_m<bunch->macroparticles;i_m++){
      tmpy+=bunch->particle[i_z*n_macro+i_m].wgt
	*bunch->particle[i_z*n_macro+i_m].y;
#ifdef TWODIM
      tmpx+=bunch->particle[i_z*n_macro+i_m].wgt
	*bunch->particle[i_z*n_macro+i_m].x;
#endif
    }
    S_b_transv[CAVSTEP]+=tmpy*bunch->drive_data->bf[i_z];
    C_b_transv[CAVSTEP]-=tmpy*bunch->drive_data->af[i_z];
#ifdef TWODIM
    S_b_transv_x[CAVSTEP]+=tmpx*bunch->drive_data->bf[i_z];
    C_b_transv_x[CAVSTEP]-=tmpx*bunch->drive_data->af[i_z];
#endif
  }
  for (nhelp=0;nhelp<CAVSTEP+1;nhelp++){
    /* R/Q is constant along the structure */
    rho_S_long[i_b*(CAVSTEP+1)+nhelp]=S_b_long[0];
    rho_C_long[i_b*(CAVSTEP+1)+nhelp]=C_b_long[0];
    rho_S_transv[i_b*(CAVSTEP+1)+nhelp]=S_b_transv[nhelp];
    rho_C_transv[i_b*(CAVSTEP+1)+nhelp]=C_b_transv[nhelp];
#ifdef TWODIM
    rho_S_transv_x[i_b*(CAVSTEP+1)+nhelp]=S_b_transv_x[nhelp];
    rho_C_transv_x[i_b*(CAVSTEP+1)+nhelp]=C_b_transv_x[nhelp];
#endif
  }
}
}

