void BEAMLINE::check()const
{
  GIRDER *girder=first;
  while(girder!=NULL) {
    girder->check();
    girder=girder->next();
  }
}

void solenoid_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"Solenoid -length %g -bz %g\n",element->length,
	  element->v1);
}

void solcav_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"SolCav -length %g -bz %g -gradient %g\n",element->length,
	  element->v1,element->v2);
}

void quadcav_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"QuadCav -length %g -gradient %g -k %g\n",element->length,
	  -element->v1,element->v2);
}

void quadrupole_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"Quadrupole -length %g -strength %g\n",element->length,
	  -element->v1);
}

void quadbpm_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"QuadBpm -length %g -strength %g\n",element->length,
	  element->v1);
}

void bpm_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"Bpm -length %g\n",element->length);
}

void drift_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"Drift -length %g\n",element->length);
}

void cavity_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"Cavity -length %g -gradient %g -phase %g -type %d\n",
	  element->length,element->v1,element->v5*180.0/PI,element->field);
}

void cav_pets_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"DecCav -length %g -phi %g\n",element->length,
	  element->v1);
}

void dipole_list(FILE *file,ELEMENT *element)
{
  fprintf(file,"Dipole -length %g\n",element->length);
}

void ELEMENT::list(FILE *file)
{
  switch (this->type) {
  case QUAD:
    quadrupole_list(file,this);
    break;
  case _DIPOLE:
    dipole_list(file,this);
    break;
  case QUADBPM:
    quadbpm_list(file,this);
    break;
  case BPM:
    bpm_list(file,this);
    break;
  case CAV_PETS:
    cav_pets_list(file,this);
    break;
  case CAV:
    cavity_list(file,this);
    break;
  case DRIFT:
    drift_list(file,this);
    break;
  case SOLENOID:
    solenoid_list(file,this);
    break;
  case SOLCAV:
    solcav_list(file,this);
    break;
  case QUADCAV:
    quadcav_list(file,this);
    break;
  }
}

void solenoid_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"Solenoid -length %g -bz %g",element->length,
	  element->v1);
}

void solcav_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"SolCav -length %g -bz %g -gradient %g",element->length,
	  element->v1,element->v2);
}

void quadcav_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"QuadCav -length %g -gradient %g -k %g",element->length,
	  -element->v1,element->v2);
}

void quadrupole_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"Quadrupole -length %g -strength %g",element->length,
	  -element->v1);
}

void quadbpm_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"QuadBpm -length %g -strength %g",element->length,
	  element->v1);
}

void bpm_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"Bpm -length %g",element->length);
}

void drift_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"Drift -length %g",element->length);
}

void cavity_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"Cavity -length %g -gradient %g -phase %g -type %d",
	  element->length,element->v1,element->v5*180.0/PI,element->field);
}

void cav_pets_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"DecCav -length %g -phi %g",element->length,
	  element->v1);
}

void dipole_list(Tcl_Interp *interp,ELEMENT *element)
{
  sprintf(interp->result,"Dipole -length %g",element->length);
}

int ELEMENT::list(Tcl_Interp *interp)
{
  switch (this->type) {
  case QUAD:
    quadrupole_list(interp,this);
    break;
  case _DIPOLE:
    dipole_list(interp,this);
    break;
  case QUADBPM:
    quadbpm_list(interp,this);
    break;
  case BPM:
    bpm_list(interp,this);
    break;
  case CAV_PETS:
    cav_pets_list(interp,this);
    break;
  case CAV:
    cavity_list(interp,this);
    break;
  case DRIFT:
    drift_list(interp,this);
    break;
  case SOLENOID:
    solenoid_list(interp,this);
    break;
  case SOLCAV:
    solcav_list(interp,this);
    break;
  case QUADCAV:
    quadcav_list(interp,this);
    break;
  }
  return TCL_OK;
}
