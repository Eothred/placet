#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "cavity.h"
#include "matrix.h"
#include "quadwake.h"
#include "wakekick.h"
#include "rfkick.h"
#include "sort.h"
#include "distribute.h"

#include "dipolekick.cpp"
#include "dipolekick2.c"

extern SWITCH_DATA_STRUCT switch_data;
extern EARTH_FIELD_STRUCT earth_field;

int data_nstep;
double data_attenuation;
DRIVE_DATA_STRUCT drive_data;
WAKEFIELD_DATA_STRUCT wakefield_data;

/* wakefield_data contains the switches for the wakefields
 * transv==1: use transverse wakefields
 * transv==2: use transverse wakefields with several particles per slice
 */


/*
  The routine steps through a PETS cavity using first second transport
*/

#include "cavity_strange_pets.cpp"

CAVITY_STRANGE_PETS *cavity_make_strange_pets(double length,int field,
					      double rotation)
{
  CAVITY_STRANGE_PETS* element;
  element = new CAVITY_STRANGE_PETS;
  element->length=length;
  element->field=field;
  element->set_y(0.0);
  element->set_yp(0.0);
  element->v3=0.0;
  element->v4=0.0;
#ifdef TWODIM
  element->set_x(0.0);
  element->set_xp(0.0);
  element->set_tilt(0.0);
#endif
  element->t=field;
  element->callback=NULL;
  element->help=NULL;
  element->modes=NULL;
  element->type=CAV;
  element->v1=1.0;
  element->v5=rotation*PI/180.0;
  return element;
}

void
CAVITY_STRANGE_PETS::step_4d_0(BEAM *beam)
{
  cavity_strange_step_pets_0(this,beam);
}

void
CAVITY_STRANGE_PETS::step(BEAM *beam)
{
  cavity_strange_step_pets(this,beam);
}
