struct{
  double length,sigma;
} survey_wire_data;

survey_wire_init(double length,double sigma)
{
  survey_wire_data.length=length;
  survey_wire_data.sigma=sigma;
}

beamline_survey_wire_1(BEAMLINE *beamline)
{
  ELEMENT *element;
  int j=0;
  double s=0.0,y10=0.0,y00=0.0,y11,y01,stot=0.0,y;
  double smax;

  smax=0.5*survey_wire_data.length;

  element=beamline->element[j++];

  y10=survey_wire_data.sigma*gasdev();
  y01=survey_wire_data.sigma*gasdev();
  y11=y01+survey_wire_data.sigma*gasdev();

  while(element){
    if (s>smax){
      y00=y11;
      y01=y11+(y11-y10);
      y10=y00+survey_wire_data.sigma*gasdev();
      y11=y01+survey_wire_data.sigma*gasdev();
      s-=smax;
      stot+=smax;
    }
    wgt=(smax-s)/smax;
    y=wgt*(y00+s/smax*(y01-y00))+(1.0-wgt)*(y10+s/smax*(y11-y10));
    s+=element->length;
    element=beamline->element[j++];
    printf("%g %g\n",stot+s,y);
  }
}
