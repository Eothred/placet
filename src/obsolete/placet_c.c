#include "placeti3.c"

main(int argc,char *argv[])
{
  double emitt0,offset,esum,esum2,sa,st,sg,s,tmp;
  BEAM *bunch,*workbunch,*bunch0,*bunch1,*bunch2,*tb0,*tb1,*tb2,
    *tb0_1,*tb1_1,*tb2_1;
  BEAMLINE *beamline;
  int i,j,nbin;
  FILE *file1,*file2;
  BIN **bin;
  clock_t t_old;
  char buffer[100],*point,name[1000];

//placet_init();
//drive_linac();
main_linac();
injector_linac(argc,argv);
BAND=strtod(argv[2],&point);
SCAL=1.0;
ATTENUATION=strtod(argv[3],&point);
MODEL=1;
SHIFT=1.07;
FOCUS=0.3;

//ATTENUATION=pow(0.812252,1.0/(1.0-0.409));
//ATTENUATION=pow(0.91,1.0/(1.0-0.785));
ATTENUATION=0.812252;
#ifdef TIMING
  timer_init();
#endif
//xxx
#ifdef MULTI
  emitt_store_switch_2(0);
#else
  emitt_store_switch(0);
#endif
  rf_init();
  wakefield_init(1,1);
  corr_init();
  errors_init();
  bpm_init(0,0);
  make_bunch_init(0.09*1.4*1.0,2.0,1);
corr.pwgt=0.0;
bump_init(1,0);

zero_point=0.0;
  bin=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);

  rndmst();
  element_init();
  girder_init();

  beamline=beamline_make();
  setup_drivelinac(beamline);
//  bunch0=make_multi_bunch_drive(beamline,"beam.dat",1301);
  /*
  tb0=bunch_remake(bunch0);
  beam_copy(bunch0,tb0);
  write_tor(beamline,tb0,"tor.dat");
  beam_copy(bunch0,tb0);
  bns_plot(beamline,tb0,"bns.dat");
  beam_copy(bunch0,tb0);
  twiss_plot(beamline,tb0,"twiss.dat");
  beam_copy(bunch0,tb0);
  energy_spread_plot(beamline,tb0,"spread.dat");
  beam_copy(bunch0,tb0);
*/
  tmp=50.0;
  for (i=0;i<bunch0->slices;i++){
    bunch0->particle[i].y=tmp;
  }
  survey_init_0(0.0,0.0,0.0,0.0,0.0);
  beamline_bin_divide(beamline,1,0,bin,&nbin);
//test_no_correction(beamline,bunch0,NULL,1,&beamline_survey_clic,
//		       "BB");
//test_simple_correction(beamline,bin,nbin,bunch0,NULL,1,&beamline_survey_clic,
//		       "BB");
exit(0);

  beamline=beamline_make();
//  setup_TeV("lattice.ini",beamline);
setup_injector(beamline,"beam.dat");
//setup_lil(beamline);
check_beamline(beamline);

//  bunch0=make_bunch(beamline,"beam.dat");
  bunch0=make_multi_bunch_inject(beamline,"beam.dat",1);
  bunch1=make_multi_bunch_inject(beamline,"beam.dat",
				 82*strtol(argv[1],&point,10));
  beamline_set_zero(beamline);

  tb0=bunch_remake(bunch0);
  tb1=bunch_remake(bunch1);
//  tb2=bunch_remake(bunch0);
  beam_copy(bunch0,tb0);
  beam_copy(bunch1,tb1);
//  beam_copy(bunch1,tb2);
  beamline_bin_divide(beamline,1,0,bin,&nbin);
  survey_init_0(0.0,2.0,10.0,0.0,0.0);

  tmp=000.0;
  for (i=0;i<tb1->slices;i++){
    tb1->particle[i].y=tmp;
  }
t_old=clock();
rndmst();
printf("emitt: %g\n",emitt_y(tb1));
test_no_correction(beamline,tb1,NULL,1,&beamline_set_zero,"BB");
printf("emitt: %g\n",emitt_y(tb1));
printf("time1= %g\n",(clock()-t_old)*1e-6);
exit(0);

test_simple_alignment(beamline,bin,nbin,tb0,100,&beamline_survey_nlc,"dummy",0.02*0.02*4,0.0,0,0);
exit(0);
//tb1->factor=0.1; tb2->factor=0.1;
  /*
test_simple_correction(beamline,bin,nbin,tb0,NULL,100,
		       &beamline_survey_nlc,"interleaved.emitt");
exit(0);
*/
/*
  beamline_bin_divide(beamline,12,6,bin,&nbin);
//  test_measured_correction(beamline,quad_set0,quad_set1,quad_set3,bin,nbin,
//			   tb0,tb0,tb0,100,&beamline_survey_clic,0.0,0.17,
//			   "DF.1");
  test_measured_correction(beamline,quad_set0,quad_set1,quad_set2,bin,nbin,
			   tb0,tb1,tb2,100,&beamline_survey_clic,0.001,0.0017,
			   "dd.2");
  test_measured_correction(beamline,quad_set0,quad_set1,quad_set3,bin,nbin,
			   tb0,tb0,tb0,100,&beamline_survey_clic,0.0,0.0,
			   "DF.0");
exit(0);		       
  /*
    check_autophase(beamline,tb0,"autophase.dat");
    exit(0);
    */

  survey_init_0(0.0,2.0,10.0,0.0,0.0);
test_simple_correction(beamline,bin,nbin,tb0,NULL,100,&beamline_survey_clic,"BB");
exit(0);

  beam_copy(bunch0,tb1);
//  beam_copy(bunch0,tb2);
//  tb1->factor=0.1;
//  tb2->factor=0.1;

  errors.quad_move_res=0.0;
  errors.bpm_resolution=0.1;
  errors.do_jitter=1;
  errors.jitter_y=0.173;
  errors.do_field=2;
  errors.field_res=0.001;
  errors.field_zero=1.0*0.375;
  errors.field_zero_rms=0.01;
  errors.do_position=1;
  errors.position_error=10.0;

  survey_init_0(50.0,10.0,10.0,0.0,0.0);
  ballistic_init();
  ballistic_set(1,1,4,0,12,0);
  ballistic_set_atl(0,1e7);
rndmst();
  test_ballistic_2(beamline,quad_set0,quad_set1,tb0,NULL,NULL,100,
		   &beamline_survey_clic,"emitt.3TeV",
		   "log.3TeV");
//tmp=run_beam_jitter(beamline,tb0,tb1,100,1.7*0.0,0.0,3.0);
//printf("mean: %g\n",tmp);
//bunch_track_line(beamline,tb0,0);
//bunch_save("tmp.bunch",tb0);
exit(0);

  /*
  sprintf(name,"feedback.%d",strtol(argv[1],&point,10));
  test_feedback_3(beamline,tb0,5,1,1000,3000.0,name);
  exit(0);
  */

  j=strtol(argv[3],&point,10);
  i=strtol(argv[2],&point,10);
  /*
  beamline_bin_divide(beamline,1,0,bin,&nbin);
  for (i=0;i<20;i++){
    errors.do_jitter=0;
    survey_init_0(10.0,5.0,10.0,0.0,0.0);
    sprintf(name,"longtest cav.dat 60 0.49 %g %g\n",1.0*i,exp(-0.01*j));
    call_fortran(name," ");
    sprintf(name,"longball%d.%d",i,j);
    rndmst();
    tb1=make_bunch(beamline,"beam2.dat");
    test_simple_correction(beamline,bin,nbin,tb0,tb1,1,&beamline_survey_clic,
			   name,);
  }
  */
//  for (i=0;i<20;i++){
    survey_init_0(50.0,10.0,10.0,0.0,0.0);
    sprintf(name,"longtest cav.dat 60 0.49 %g %g\n",1.0*i,exp(-0.01*j));
    call_fortran(name," ");
    sprintf(name,"longgrid.%d",argv[4]);
    rndmst();
    tb1=make_bunch(beamline,"beam2.dat");
    test_ballistic_2(beamline,quad_set0,quad_set1,tb0,tb0,tb1,30,
		     &beamline_survey_clic,name,"dummy");
//  }
  exit(0);
  /*
    bunch_bpm_filter_init(tb1);
    for (i=0;i<tb1->slices;i++){
      if (i>660-2-3*11){
	bunch_bpm_filter_add(tb1,1.0,i);
      }
      else{
	bunch_bpm_filter_add(tb1,0.0,i);
      }
    }
    bunch_bpm_filter_finish(tb1);
    */

  errors.quad_move_res=0.0;
  errors.bpm_resolution=0.1;
  errors.do_jitter=1;
  errors.jitter_y=0.173;
  errors.do_field=2;
  errors.field_res=0.001;
  errors.field_zero=1.0*0.375;
  errors.field_zero_rms=0.01;
  errors.do_position=1;
  errors.position_error=10.0;

  /*
  file1=fopen("jitter2.dat","w");
  survey_init_0(50.0,10.0,10.0,0.0,0.0);
  beamline_bin_divide(beamline,12,6,bin,&nbin);
  esum=0.0;
  for (j=0;j<100;j++){
    for (i=0;i<beamline->n_quad;i++){
      quad_set[i]=quad_set0[i]*(1.0+0.001*gasdev());
      quad_set3[i]=quad_set1[i]*(1.0+0.001*gasdev());
      quad_set4[i]=quad_set2[i]*(1.0+0.001*gasdev());
    }
    tmp=test_measured_correction(beamline,quad_set,quad_set3,quad_set4,
				 bin,nbin,
				 tb0,tb1,tb2,1,&beamline_survey_clic,
				 0.0,0.0,"meas.dat");
    esum+=tmp;
    printf("%g\n",esum/(double)(j+1));
    fprintf(file1,"%d %g %g\n",j,esum/(double)(j+1),tmp);
    fflush(file1);
  }
  fclose(file1);
  exit(0);
  */
  for (i=0;i<10;i++){
    errors.jitter_y=0.173*0.5*i;
    rndmst();
    tmp=test_ballistic_2(beamline,quad_set0,quad_set1,tb0,NULL,NULL,1,
			 &beamline_survey_clic,"dummy1","dummy2");
    fprintf(file1,"%g %g\n",errors.jitter_y,tmp);
    fflush(file1);
  }
  fclose(file1);
  exit(0);
}
