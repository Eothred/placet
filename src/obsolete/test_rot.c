#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "matrix.c"

void
bunch_rotate(R_MATRIX *sigma_xx,R_MATRIX *sigma,R_MATRIX *sigma_xy,
	     double theta)
{
  double c,s,tmp;
  R_MATRIX tmp1,tmp2;
  int i;
  if (theta==0.0) return;
  s=sin(theta);
  c=cos(theta);

  tmp1.r11=c*c*sigma_xx->r11-2.0*c*s*sigma_xy->r11
    +s*s*sigma->r11;
  tmp1.r12=c*c*sigma_xx->r12-c*s*(sigma_xy->r12+sigma_xy->r21)
    +s*s*sigma->r12;
  tmp1.r22=c*c*sigma_xx->r22-2.0*c*s*sigma_xy->r22
    +s*s*sigma->r22;
  
  tmp2.r11=c*c*sigma->r11+2.0*c*s*sigma_xy->r11
    +s*s*sigma_xx->r11;
  tmp2.r12=c*c*sigma->r12+c*s*(sigma_xy->r12+sigma_xy->r21)
    +s*s*sigma_xx->r12;
  tmp2.r22=c*c*sigma->r22+2.0*c*s*sigma_xy->r22
    +s*s*sigma_xx->r22;
  
  sigma_xy->r11=(c*c-s*s)*sigma_xy->r11
    +c*s*(sigma_xx->r11-sigma->r11);
  /* changed sign from + to - 1.10.1999 */
  sigma_xy->r12=c*c*sigma_xy->r12
    -s*s*sigma_xy->r21
    +c*s*(sigma_xx->r12-sigma->r12);
  sigma_xy->r21=c*c*sigma_xy->r21-s*s*sigma_xy->r12
    +c*s*(sigma_xx->r21-sigma->r21);
  sigma_xy->r22=(c*c-s*s)*sigma_xy->r22
    +c*s*(sigma_xx->r22-sigma->r22);
  
  sigma->r11=tmp2.r11;
  sigma->r12=tmp2.r12;
  sigma->r21=tmp2.r12;
  sigma->r22=tmp2.r22;
  sigma_xx->r11=tmp1.r11;
  sigma_xx->r12=tmp1.r12;
  sigma_xx->r21=tmp1.r12;
  sigma_xx->r22=tmp1.r22;
}

main()
{
  R_MATRIX ry,rx,rxy;
  int i;
  rx.r11=1.0;
  rx.r12=0.0;
  rx.r21=0.0;
  rx.r22=1.0;
  ry.r11=10.0;
  ry.r12=0.0;
  ry.r21=0.0;
  ry.r22=10.0;
  rxy.r11=0.0;
  rxy.r12=0.0;
  rxy.r21=0.0;
  rxy.r22=0.0;

  for (i=0;i<10000;i++){
    bunch_rotate(&rx,&ry,&rxy,acos(-1.0)*0.5);
    bunch_rotate(&rx,&ry,&rxy,-acos(-1.0)*0.5);
  }
  print_M(&rx);
  print_M(&rxy);
  print_M(&ry);
}
