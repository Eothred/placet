
/* applies the transverse wakefield kick */

void
dipole_kick_long(CAVITY *element,BEAM *beam,double length)
{
  double tmp,factor,x,y;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0,nstep;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m,l;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,*multi_y,*multi_x,
    *rho_a,*rho_b,*multi_y_b,pos_yb,pos_xb,*longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,
    *multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  nstep=element->id->n;

  multi_y=(double*)alloca(sizeof(double)*nstep);
  multi_x=(double*)alloca(sizeof(double)*nstep);
  multi_y_b=(double*)alloca(sizeof(double)*nstep);
  multi_x_b=(double*)alloca(sizeof(double)*nstep);
  longfact=(double*)alloca(sizeof(double)*nstep);
  n_macro=beam->macroparticles;

  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  /* loop over all bunches */
  
  for (k=0;k<beam->bunches;k++){
    
    m=k*beam->slices_per_bunch;
    p=beam->field[0].kick;
    rho_y=beam->rho_y[k];
#ifdef TWODIM
    rho_x=beam->rho_x[k];
#endif
    for (j=0;j<beam->slices_per_bunch;j++){
      rho_y[j]=0.0;
#ifdef TWODIM
      rho_x[j]=0.0;
#endif
      for (i_m=0;i_m<n_macro;i_m++){
	rho_y[j]+=beam->particle[(m+j)*n_macro+i_m].y
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef TWODIM
	rho_x[j]+=beam->particle[(m+j)*n_macro+i_m].x
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
      }
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      kick_y[j]=sumy;
#ifdef TWODIM
      kick_x[j]=sumx;
#endif
    }
    for (j=k-1;j>=0;j--){
      force=beam->force[k-j]+beam->slices_per_bunch;
      rho_y=beam->rho_y[j];
#ifdef TWODIM
      rho_x=beam->rho_x[j];
#endif
      for (i=0;i<beam->slices_per_bunch;i++){
	  sumy=kick_y[i];
#ifdef TWODIM
	  sumx=kick_x[i];
#endif
	  for (l=0;l<beam->slices_per_bunch;l++){
	      sumy+=force[l]*rho_y[l];
#ifdef TWODIM
	      sumx+=force[l]*rho_x[l];
#endif
	  }
	  kick_y[i]=sumy;
#ifdef TWODIM
	  kick_x[i]=sumx;
#endif
	  force--;
      }
    }

    for (j=0;j<beam->slices_per_bunch;j++) {
      for (i_m=0;i_m<n_macro;i_m++){
	beam->particle[(m+j)*n_macro+i_m].yp+=
	  kick_y[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#ifdef TWODIM
	beam->particle[(m+j)*n_macro+i_m].xp+=
	  kick_x[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#endif
      }
    }
  }
}

void
dipole_kick_long_fft(CAVITY *element,BEAM *beam,double length)
{
  double tmp,factor,x,y;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0,nstep;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m,l;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,*multi_y,*multi_x,
    *rho_a,*rho_b,*multi_y_b,pos_yb,pos_xb,*longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,
    *multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  nstep=element->id->n;

  multi_y=(double*)alloca(sizeof(double)*nstep);
  multi_x=(double*)alloca(sizeof(double)*nstep);
  multi_y_b=(double*)alloca(sizeof(double)*nstep);
  multi_x_b=(double*)alloca(sizeof(double)*nstep);
  longfact=(double*)alloca(sizeof(double)*nstep);
  n_macro=beam->macroparticles;

  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  /* loop over all bunches */
  
  for (k=0;k<beam->bunches;k++){
    
    m=k*beam->slices_per_bunch;
    p=beam->field[0].kick;
    rho_y=beam->rho_y[k];
#ifdef TWODIM
    rho_x=beam->rho_x[k];
#endif
    for (j=0;j<beam->slices_per_bunch;j++){
      rho_y[j]=0.0;
#ifdef TWODIM
      rho_x[j]=0.0;
#endif
      for (i_m=0;i_m<n_macro;i_m++){
	rho_y[j]+=beam->particle[(m+j)*n_macro+i_m].y
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef TWODIM
	rho_x[j]+=beam->particle[(m+j)*n_macro+i_m].x
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
      }
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      kick_y[j]=sumy;
#ifdef TWODIM
      kick_x[j]=sumx;
#endif
    }
    for (j=k-1;j>=0;j--){
      force=beam->force[k-j]+beam->slices_per_bunch;
      rho_y=beam->rho_y[j];
#ifdef TWODIM
      rho_x=beam->rho_x[j];
#endif
      for (i=0;i<beam->slices_per_bunch;i++){
	  sumy=kick_y[i];
#ifdef TWODIM
	  sumx=kick_x[i];
#endif
	  for (l=0;l<beam->slices_per_bunch;l++){
	      sumy+=force[l]*rho_y[l];
#ifdef TWODIM
	      sumx+=force[l]*rho_x[l];
#endif
	  }
	  kick_y[i]=sumy;
#ifdef TWODIM
	  kick_x[i]=sumx;
#endif
	  force--;
      }
    }

    for (j=0;j<beam->slices_per_bunch;j++) {
      for (i_m=0;i_m<n_macro;i_m++){
	beam->particle[(m+j)*n_macro+i_m].yp+=
	  kick_y[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#ifdef TWODIM
	beam->particle[(m+j)*n_macro+i_m].xp+=
	  kick_x[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#endif
      }
    }
  }
}
