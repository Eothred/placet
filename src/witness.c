#ifdef _OPENMP
#include <omp.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"
#include "select.h"
#include "placet_tk.h"

extern INTER_DATA_STRUCT inter_data;

/*class witness {
  double xsum,ysum,xpsum,ypsum,zsum,esum,sxxsum,sxxpsum,sxpxpsum,syysum;
  double syypsum,sypypsum,sxysum,sxypsum,sxpysum,sxpypsum,szzsum;
  double sxesum,syesum,sxpesum,sypesum,szesum,seesum;
  int count;
 public:
  void reset() {xsum=0.0; ysum=0.0; xpsum=0.0; ypsum=0.0; zsum=0.0; esum=0.0; 
    count=0; sxxsum=0.0;sxxpsum=0.0;sxpxpsum=0.0;syysum=0.0;syypsum=0.0;
    sypypsum=0.0; sxysum=0.0; sxypsum=0.0; sxpysum=0.0; sxpypsum=0.0; szzsum=0.0;
    sxesum=0.0; syesum=0.0; sxpesum=0.0; sypesum=0.0; szesum=0.0; seesum=0.0;};
    };
*/

int measure_beam(Tcl_Interp *interp,BEAM *beam,int n1,int n2)
{
  double wgtsum=0.0,xsum=0.0,ysum=0.0,xpsum=0.0,ypsum=0.0,zsum=0.0, esum=0.0;
  double sxxsum=0.0,sxxpsum=0.0,sxpxpsum=0.0, syysum=0.0,syypsum=0.0,sypypsum=0.0, 
    sxysum=0.0, sxypsum=0.0, sxpysum=0.0, sxpypsum=0.0, szzsum=0.0;
  double sxesum=0.0,syesum=0.0,sxpesum=0.0,sypesum=0.0,szesum=0.0, 
    seesum=0.0, e_spread=0.0;
  
  if (beam->particle_beam) {
#pragma omp parallel for reduction(+:wgtsum,xsum,ysum,xpsum,ypsum,zsum,esum,sxxsum,sxxpsum,sxpxpsum,syysum,syypsum,sypypsum,sxysum,sxypsum,sxpysum,sxpypsum,szzsum,sxesum,syesum,sxpesum,sypesum,szesum,seesum,e_spread)  
    for (int i=n1;i<n2;++i){
      const PARTICLE &particle=beam->particle[i];
      double energy=fabs(particle.energy);
      double wgt=fabs(particle.wgt);
      wgtsum+=wgt;
      xsum+=wgt*particle.x;
      xpsum+=wgt*particle.xp;
      ysum+=wgt*particle.y;
      ypsum+=wgt*particle.yp;
      zsum+=wgt*particle.z;
      esum+=wgt*energy;
      sxxsum+=wgt*(particle.x*particle.x);
      sxxpsum+=wgt*(particle.x*particle.xp);
      sxpxpsum+=wgt*(particle.xp*particle.xp);
      syysum+=wgt*(particle.y*particle.y);
      syypsum+=wgt*(particle.y*particle.yp);
      sypypsum+=wgt*(particle.yp*particle.yp);
      sxysum+=wgt*(particle.x*particle.y);
      sxypsum+=wgt*(particle.x*particle.yp);
      sxpypsum+=wgt*(particle.xp*particle.yp);
      sxpysum+=wgt*(particle.xp*particle.y);
      szzsum+=wgt*(particle.z*particle.z);
      sxesum+=wgt*(particle.x*energy);
      syesum+=wgt*(particle.y*energy);
      sxpesum+=wgt*(particle.xp*energy);
      sypesum+=wgt*(particle.yp*energy);
      szesum+=wgt*(particle.z*energy);
      seesum+=wgt*(energy*energy);
    }
  } else {
#pragma omp parallel for reduction(+:wgtsum,xsum,ysum,xpsum,ypsum,zsum,esum,sxxsum,sxxpsum,sxpxpsum,syysum,syypsum,sypypsum,sxysum,sxypsum,sxpysum,sxpypsum,szzsum,sxesum,syesum,sxpesum,sypesum,szesum,seesum,e_spread)
    for (int i=n1;i<n2;++i){
      const PARTICLE &particle=beam->particle[i];
      double energy=fabs(particle.energy);
      double wgt=fabs(particle.wgt);
      wgtsum+=wgt;
      xsum+=wgt*particle.x;
      xpsum+=wgt*particle.xp;
      ysum+=wgt*particle.y;
      ypsum+=wgt*particle.yp;
      zsum+=wgt*particle.z;
      esum+=wgt*energy;
      sxxsum+=wgt*(beam->sigma_xx[i].r11+particle.x*particle.x);
      sxxpsum+=wgt*(beam->sigma_xx[i].r12+particle.x*particle.xp);
      sxpxpsum+=wgt*(beam->sigma_xx[i].r22+particle.xp*particle.xp);
      syysum+=wgt*(beam->sigma[i].r11+particle.y*particle.y);
      syypsum+=wgt*(beam->sigma[i].r12+particle.y*particle.yp);
      sypypsum+=wgt*(beam->sigma[i].r22+particle.yp*particle.yp);
      sxysum+=wgt*(beam->sigma_xy[i].r11+particle.x*particle.y);
      sxypsum+=wgt*(beam->sigma_xy[i].r12+particle.x*particle.yp);
      sxpypsum+=wgt*(beam->sigma_xy[i].r22+particle.xp*particle.yp);
      sxpysum+=wgt*(beam->sigma_xy[i].r21+particle.xp*particle.y);
      szzsum+=wgt*(particle.z*particle.z);
      sxesum+=wgt*(particle.x*energy);
      syesum+=wgt*(particle.y*energy);
      sxpesum+=wgt*(particle.xp*energy);
      sypesum+=wgt*(particle.yp*energy);
      szesum+=wgt*(particle.z*energy);
      seesum+=wgt*(energy*energy);
    }
  }
  if (wgtsum>std::numeric_limits<double>::epsilon()) {
    xsum/=wgtsum;
    xpsum/=wgtsum;
    ysum/=wgtsum;
    ypsum/=wgtsum;
    zsum/=wgtsum;
    esum/=wgtsum;
    sxxsum/=wgtsum;
    sxxpsum/=wgtsum;
    sxpxpsum/=wgtsum;
    syysum/=wgtsum;
    syypsum/=wgtsum;
    sypypsum/=wgtsum;
    sxysum/=wgtsum;
    sxypsum/=wgtsum;
    sxpypsum/=wgtsum;
    sxpysum/=wgtsum;
    szzsum/=wgtsum;
    sxesum/=wgtsum;
    syesum/=wgtsum;
    sxpesum/=wgtsum;
    sypesum/=wgtsum;
    szesum/=wgtsum;
    seesum/=wgtsum;
  }
  char buffer[150];

  Tcl_AppendElement(interp,"x");
  snprintf(buffer,150,"%21.16e\0",xsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"e");
  snprintf(buffer,150,"%21.16e\0",esum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"xp");
  snprintf(buffer,150,"%21.16e\0",xpsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxx");
  snprintf(buffer,150,"%21.16e\0",sxxsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxpxp");
  snprintf(buffer,150,"%21.16e\0",(sxpxpsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxxp");
  snprintf(buffer,150,"%21.16e\0",(sxxpsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"y");
  snprintf(buffer,150,"%21.16e\0",ysum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"yp");
  snprintf(buffer,150,"%21.16e\0",ypsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"syy");
  snprintf(buffer,150,"%21.16e\0",(syysum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sypyp");
  snprintf(buffer,150,"%21.16e\0",(sypypsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"syyp");
  snprintf(buffer,150,"%21.16e\0",syypsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxy");
  snprintf(buffer,150,"%21.16e\0",sxysum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxpyp");
  snprintf(buffer,150,"%21.16e\0",(sxpypsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxyp");
  snprintf(buffer,150,"%21.16e\0",(sxypsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxpy");
  snprintf(buffer,150,"%21.16e\0",(sxpysum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"z");
  snprintf(buffer,150,"%21.16e\0",zsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"szz");
  snprintf(buffer,150,"%21.16e\0",szzsum);
  Tcl_AppendElement(interp,buffer);

  sxxsum-=xsum*xsum;
  sxxpsum-=xsum*xpsum;
  sxpxpsum-=xpsum*xpsum;
  syysum-=ysum*ysum;
  syypsum-=ysum*ypsum;
  sypypsum-=ypsum*ypsum;
  sxysum-=xsum*ysum;
  sxypsum-=xsum*ypsum;
  sxpypsum-=xpsum*ypsum;
  sxpysum-=xpsum*ysum;
  szzsum-=zsum*zsum;
  double emitt_x=sqrt(sxxsum*sxpxpsum-sxxpsum*sxxpsum);
  double emitt_y=sqrt(syysum*sypypsum-syypsum*syypsum);

  Tcl_AppendElement(interp,"sxx_axis");
  snprintf(buffer,150,"%21.16e\0",sxxsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxpxp_axis");
  snprintf(buffer,150,"%21.16e\0",(sxpxpsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxxp_axis");
  snprintf(buffer,150,"%21.16e\0",(sxxpsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"syy_axis");
  snprintf(buffer,150,"%21.16e\0",(syysum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sypyp_axis");
  snprintf(buffer,150,"%21.16e\0",(sypypsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"syyp_axis");
  snprintf(buffer,150,"%21.16e\0",syypsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxy_axis");
  snprintf(buffer,150,"%21.16e\0",sxysum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxpyp_axis");
  snprintf(buffer,150,"%21.16e\0",(sxpypsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxyp_axis");
  snprintf(buffer,150,"%21.16e\0",(sxypsum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxpy_axis");
  snprintf(buffer,150,"%21.16e\0",(sxpysum));
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"szz_axis");
  snprintf(buffer,150,"%21.16e\0",szzsum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"beta_x");
  if (emitt_x>std::numeric_limits<double>::epsilon())
    snprintf(buffer,150,"%21.16e\0",sxxsum/emitt_x);
  else
    snprintf(buffer,150,"%21.16e\0",0.0);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"beta_y");
  if (emitt_y>std::numeric_limits<double>::epsilon())
    snprintf(buffer,150,"%21.16e\0",syysum/emitt_y);
  else
    snprintf(buffer,150,"%21.16e\0",0.0);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"alpha_x");
  if (emitt_x>std::numeric_limits<double>::epsilon())
    snprintf(buffer,150,"%21.16e\0",-sxxpsum/emitt_x);
  else
    snprintf(buffer,150,"%21.16e\0",0.0);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"alpha_y");
  if (emitt_y>std::numeric_limits<double>::epsilon())
    snprintf(buffer,150,"%21.16e\0",-syypsum/emitt_y);
  else
    snprintf(buffer,150,"%21.16e\0",0.0);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxe");
  snprintf(buffer,150,"%21.16e\0",sxesum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxpe");
  snprintf(buffer,150,"%21.16e\0",sxpesum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sye");
  snprintf(buffer,150,"%21.16e\0",syesum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sype");
  snprintf(buffer,150,"%21.16e\0",sypesum);
  Tcl_AppendElement(interp,buffer);
  
  Tcl_AppendElement(interp,"sze");
  snprintf(buffer,150,"%21.16e\0",szesum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxe_axis");
  snprintf(buffer,150,"%21.16e\0",sxesum-xsum*esum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sxpe_axis");
  snprintf(buffer,150,"%21.16e\0",sxpesum-xpsum*esum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sye_axis");
  snprintf(buffer,150,"%21.16e\0",syesum-ysum*esum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"sype_axis");
  snprintf(buffer,150,"%21.16e\0",sypesum-ypsum*esum);
  Tcl_AppendElement(interp,buffer);
  
  Tcl_AppendElement(interp,"sze_axis");
  snprintf(buffer,150,"%21.16e\0",szesum-zsum*esum);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"see");
  snprintf(buffer,150,"%21.16e\0",seesum);
  Tcl_AppendElement(interp,buffer);

  double tmp=(seesum-esum*esum);

  if (fabs(tmp)>std::numeric_limits<double>::epsilon()) {
    Tcl_AppendElement(interp,"disp_x");
    snprintf(buffer,150,"%21.16e\0",(sxesum-xsum*esum)/tmp);
    Tcl_AppendElement(interp,buffer);

    Tcl_AppendElement(interp,"disp_xp");
    snprintf(buffer,150,"%21.16e\0",(sxpesum-xpsum*esum)/tmp);
    Tcl_AppendElement(interp,buffer);

    Tcl_AppendElement(interp,"disp_y");
    snprintf(buffer,150,"%21.16e\0",(syesum-ysum*esum)/tmp);
    Tcl_AppendElement(interp,buffer);

    Tcl_AppendElement(interp,"disp_yp");
    snprintf(buffer,150,"%21.16e\0",(sypesum-ypsum*esum)/tmp);
    Tcl_AppendElement(interp,buffer);

    Tcl_AppendElement(interp,"disp_z");
    snprintf(buffer,150,"%21.16e\0",(szesum-zsum*esum)/tmp);
    Tcl_AppendElement(interp,buffer);
  } else {
    snprintf(buffer,150,"%21.16e\0",0.0);
    Tcl_AppendElement(interp,"disp_x");
    Tcl_AppendElement(interp,buffer);
    Tcl_AppendElement(interp,"disp_xp");
    Tcl_AppendElement(interp,buffer);
    Tcl_AppendElement(interp,"disp_y");
    Tcl_AppendElement(interp,buffer);
    Tcl_AppendElement(interp,"disp_yp");
    Tcl_AppendElement(interp,buffer);
    Tcl_AppendElement(interp,"disp_z");
    Tcl_AppendElement(interp,buffer);
  }

  Tcl_AppendElement(interp,"s");
  snprintf(buffer,150,"%21.16e\0", beam->s);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"emitt_x");
  snprintf(buffer,150,"%21.16e\0",emitt_x * esum / EMASS * 1e-12 / EMITT_UNIT);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"emitt_y");
  snprintf(buffer,150,"%21.16e\0",emitt_y * esum / EMASS * 1e-12 / EMITT_UNIT);
  Tcl_AppendElement(interp,buffer);

  e_spread = sqrt(tmp);
  Tcl_AppendElement(interp,"e_spread");
  snprintf(buffer,150,"%21.16e\0", e_spread);
  Tcl_AppendElement(interp,buffer);

  return TCL_OK;
}

int
Tcl_BeamMeasure(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  BEAM *beam;
  int i,bn=-1,n1,n2;
  Tk_ArgvInfo table[]={
    {(char*)"-bunch",TK_ARGV_INT,(char*)NULL,(char*)&bn,
     (char*)"bunch to be measured\n(-1=all bunches together, -2 all bunches individually)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamMeasure>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beam=inter_data.bunch;
  if (bn>=-1) {
    if (bn==-1) {
      n1=0;
      n2=beam->slices;
    }
    else {
      n1=bn*beam->slices_per_bunch*beam->macroparticles;
      n2=(bn+1)*beam->slices_per_bunch*beam->macroparticles;
    }
    return measure_beam(interp,beam,n1,n2);
  }
  else {
    for (i=0;i<beam->bunches;++i) {
      n1=i*beam->slices_per_bunch*beam->macroparticles;
      n2=(i+1)*beam->slices_per_bunch*beam->macroparticles;
      Tcl_AppendResult(interp," {",(char*)NULL);
      Tcl_AppendResult(interp," }",(char*)NULL);
    }
  }
  return TCL_OK;
}

int
Tcl_BeamMeasureFrequency(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  BEAM *beam;
  double wgtsum=0.0;
  double xsum_s=0.0,xsum_c=0.0,ysum_s=0.0,ysum_c=0.0,wsum_s=0.0,wsum_c=0.0,
    xpsum_s=0.0,xpsum_c=0.0,ypsum_s=0.0,ypsum_c=0.0;
  double xsum=0.0,ysum=0.0,xpsum=0.0,ypsum=0.0;
  double s,c,ds,dc,tmp,wl,wl_i;
  char buffer[100];
  int j,k,l,i=0;
  Tk_ArgvInfo table[]={
    {(char*)"-lambda",TK_ARGV_FLOAT,(char*)NULL,(char*)&wl,
     (char*)"wavelength to be tested"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamMeasureFrequency>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beam=inter_data.bunch;
  wl_i=TWOPI*1e-6/wl;
  for (j=0;j<beam->bunches;++j) {
    sincos(beam->z_position[j*beam->slices_per_bunch]*wl_i,&s,&c);
    /*
    c=cos(beam->z_position[j*beam->slices_per_bunch]*wl_i);
    s=sin(beam->z_position[j*beam->slices_per_bunch]*wl_i);
    */
    sincos((beam->z_position[j*beam->slices_per_bunch+1]
	    -beam->z_position[j*beam->slices_per_bunch])*wl_i,&ds,&dc);
    /*
    dc=cos((beam->z_position[j*beam->slices_per_bunch+1]
	    -beam->z_position[j*beam->slices_per_bunch])*wl_i);
    ds=sin((beam->z_position[j*beam->slices_per_bunch+1]
	    -beam->z_position[j*beam->slices_per_bunch])*wl_i);
    */
    for (k=0;k<beam->slices_per_bunch;++k) {
      //      sincos(beam->z_position[j*beam->slices_per_bunch]*wl_i,&s,&c);
      for (l=0;l<beam->macroparticles;++l) {
	wgtsum+=beam->particle[i].wgt;
	wsum_s+=beam->particle[i].wgt*s;
	wsum_c+=beam->particle[i].wgt*c;
	xsum_s+=beam->particle[i].wgt*beam->particle[i].x*s;
	xsum_c+=beam->particle[i].wgt*beam->particle[i].x*c;
	xsum+=beam->particle[i].wgt*beam->particle[i].x;
	xpsum+=beam->particle[i].wgt*beam->particle[i].xp;
	xpsum_s+=beam->particle[i].wgt*beam->particle[i].xp*s;
	xpsum_c+=beam->particle[i].wgt*beam->particle[i].xp*c;
	ysum_s+=beam->particle[i].wgt*beam->particle[i].y*s;
	ysum_c+=beam->particle[i].wgt*beam->particle[i].y*c;
	ysum+=beam->particle[i].wgt*beam->particle[i].y;
	ypsum+=beam->particle[i].wgt*beam->particle[i].yp;
	ypsum_s+=beam->particle[i].wgt*beam->particle[i].yp*s;
	ypsum_c+=beam->particle[i].wgt*beam->particle[i].yp*c;
	++i;
      }
      tmp=dc*c-ds*s;
      s=ds*c+dc*s;
      c=tmp;
    }
  }
  xsum/=wgtsum;
  ysum/=wgtsum;
  xpsum/=wgtsum;
  ypsum/=wgtsum;

  xsum_s-=xsum*wsum_s;
  xsum_c-=xsum*wsum_c;
  ysum_s-=ysum*wsum_s;
  ysum_c-=ysum*wsum_c;
  xpsum_s-=xpsum*wsum_s;
  xpsum_c-=xpsum*wsum_c;
  ypsum_s-=ypsum*wsum_s;
  ypsum_c-=ypsum*wsum_c;

  xsum_s/=wsum_s;
  xsum_c/=wsum_c;
  xpsum_s/=wsum_s;
  xpsum_c/=wsum_c;
  ysum_s/=wsum_s;
  ysum_c/=wsum_c;
  ypsum_s/=wsum_s;
  ypsum_c/=wsum_c;

  Tcl_AppendElement(interp,"norm_s");
  snprintf(buffer,100,"%g\0",wsum_s);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"norm_c");
  snprintf(buffer,100,"%g\0",wsum_c);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"x_s");
  snprintf(buffer,100,"%g\0",xsum_s);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"x_c");
  snprintf(buffer,100,"%g\0",xsum_c);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"xp_c");
  snprintf(buffer,100,"%g\0",xpsum_c);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"xp_s");
  snprintf(buffer,100,"%g\0",xpsum_s);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"y_s");
  snprintf(buffer,100,"%g\0",ysum_s);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"y_c");
  snprintf(buffer,100,"%g\0",ysum_c);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"yp_s");
  snprintf(buffer,100,"%g\0",ypsum_s);
  Tcl_AppendElement(interp,buffer);

  Tcl_AppendElement(interp,"yp_c");
  snprintf(buffer,100,"%g\0",ypsum_c);
  Tcl_AppendElement(interp,buffer);

  return TCL_OK;
}

int
Tcl_BeamAddDispersion(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error;
  BEAM *beam;
  double eref=0.0,dx=0.0,dy=0.0,dxp=0.0,dyp=0.0;
  int i,n1,n2;
  char *sn1="start",*sn2="end",*name=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&name,
     (char*)"Name of the beam to apply dispersion to"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,(char*)&sn1,
     (char*)"First particle to apply dispersion to"},
    {(char*)"-end",TK_ARGV_STRING,(char*)NULL,(char*)&sn2,
     (char*)"Last particle to apply dispersion to"},
    {(char*)"-eref",TK_ARGV_FLOAT,(char*)NULL,(char*)&eref,
     (char*)"Reference energy"},
    {(char*)"-dx",TK_ARGV_FLOAT,(char*)NULL,(char*)&dx,
     (char*)"Dispersion in x [um]"},
    {(char*)"-dy",TK_ARGV_FLOAT,(char*)NULL,(char*)&dy,
     (char*)"Dispersion in y [um]"},
    {(char*)"-dxprime",TK_ARGV_FLOAT,(char*)NULL,(char*)&dxp,
     (char*)"Dispersion in x prime [urad]"},
    {(char*)"-dyprime",TK_ARGV_FLOAT,(char*)NULL,(char*)&dyp,
     (char*)"Dispersion in y prime [urad]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <BeamAddDispersion>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (name) {
    beam=get_beam(name);
  } else {
    beam=inter_data.bunch;
  }
  if (error=eval_beam_position(interp,beam,sn1,&n1)) return error;
  if (error=eval_beam_position(interp,beam,sn2,&n2)) return error;
  for (i=n1;i<n2;++i){
    beam->particle[i].y+=dy*(beam->particle[i].energy-eref)/eref;
#ifdef TWODIM
    beam->particle[i].x+=dx*(beam->particle[i].energy-eref)/eref;
#endif
    beam->particle[i].yp+=dyp*(beam->particle[i].energy-eref)/eref;
#ifdef TWODIM
    beam->particle[i].xp+=dxp*(beam->particle[i].energy-eref)/eref;
#endif
  }
  return TCL_OK;
}

void
Witness_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"BeamMeasure",Tcl_BeamMeasure,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(interp,"BeamMeasureFrequency",Tcl_BeamMeasureFrequency,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(interp,"BeamAddDispersion",Tcl_BeamAddDispersion,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
}
