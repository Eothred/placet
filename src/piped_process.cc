#include <algorithm>
#include <unistd.h>
#include <cstdio>
#include <vector>
#include <cstdlib>

#include <sys/socket.h>
#include <signal.h>
#include <stdarg.h>

#include "piped_process.hh"

Piped_process::Piped_process(const char *command, char *const argv[] )
{
  int i_pipe[2];
  int o_pipe[2];
  int e_pipe[2];
  pipe(i_pipe);
  pipe(o_pipe);
  pipe(e_pipe);
# define STDIN_CHILD   i_pipe[0]
# define STDIN_PARENT  i_pipe[1]
# define STDOUT_PARENT o_pipe[0]
# define STDOUT_CHILD  o_pipe[1]
# define STDERR_PARENT e_pipe[0]
# define STDERR_CHILD  e_pipe[1]
  int i_filedes[2];
  int o_filedes[2];
  pipe(i_filedes);
  pipe(o_filedes);
# define IFILEDES_CHILD  i_filedes[0]
# define IFILEDES_PARENT i_filedes[1]
# define OFILEDES_PARENT o_filedes[0]
# define OFILEDES_CHILD  o_filedes[1]
  if ((PID=vfork())<0) {
    fputs("can't vfork\n", ::stderr);
    exit(-1);
  } else if (PID==0) {
    // this is the child
    ::close(STDIN_PARENT);
    ::close(STDOUT_PARENT);
    ::close(STDERR_PARENT);
    dup2(STDIN_CHILD,  STDIN_FILENO);
    dup2(STDOUT_CHILD, STDOUT_FILENO);
    dup2(STDERR_CHILD, STDERR_FILENO);
    ::close(IFILEDES_PARENT);
    ::close(OFILEDES_PARENT);
    dup2(IFILEDES_CHILD, IFILEDES_PARENT);
    dup2(OFILEDES_CHILD, OFILEDES_PARENT);
    execvp(command, argv);
    _exit(0);
  }
  // this is the parent
  ::close(STDIN_CHILD);
  ::close(STDOUT_CHILD);
  ::close(STDERR_CHILD);
  _stdin  = STDIN_PARENT;
  _stdout = STDOUT_PARENT;
  _stderr = STDERR_PARENT;
  ::close(IFILEDES_CHILD);
  ::close(OFILEDES_CHILD);
  _ifiledes = IFILEDES_PARENT; // parent -> child
  _ofiledes = OFILEDES_PARENT; // child -> parent
}

Piped_process::~Piped_process()
{
  if (PID!=0)
    ::kill(PID, SIGKILL); 
}
