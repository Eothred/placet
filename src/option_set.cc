#include <functional>
#include <algorithm>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <vector>

#include "option_set.hh"

#include "placet_cout.hh"

option_set::option_set(const option_set &o ) : set(o.set)
{}

option_set::~option_set()
{}

option_set &option_set::operator = (const option_set &o )
{
  if (&o!=this) {
    set=o.set;
  }
  return *this;
}

option_info *option_set::find(const char *attribute )
{
  for (std::vector<option_info>::iterator itr=set.begin(); itr!=set.end(); ++itr) {
    option_info &option = *itr;
    if (strcmp(option.get_name(),attribute)==0) {
      return &option;
    }
  }
  return NULL;
}

const option_info *option_set::find(const char *attribute ) const
{
  for (std::vector<option_info>::const_iterator itr=set.begin(); itr!=set.end(); ++itr) {
    const option_info &option = *itr;
    if (strcmp(option.get_name(),attribute)==0) {
      return &option;
    }
  }
  return NULL;
}

void option_set::add(option_info option )
{
  set.push_back(option);
}

void option_set::add(const char *name, const char *description, option_type type, void *dest, double_unary_function func, double_unary_function ifunc )
{
  add(option_info(name, description, type, dest, func, ifunc));
}

void option_set::add(const char *name, const char *description, option_type type, void *dest )
{
  add(option_info(name, description, type, dest));
}

void option_set::add(const char *name, option_type type, void *dest, double_unary_function func, double_unary_function ifunc )
{
  add(option_info(name, type, dest, func, ifunc));
}

void option_set::add(const char *name, option_type type, void *dest )
{
  add(option_info(name, type, dest));
}

void option_set::remove(const char *name )
{
  std::vector<option_info>::iterator itr;
  for (itr=set.end()-1; itr!=set.begin()-1; --itr) {
    if (strcmp((*itr).get_name(),name)==0) {
      set.erase(itr);
      break;
    }
  }
}

bool option_set::parse_args(int &argc, char **argv, bool purge_argv )
{
  if (!argv)
    return true;
  bool success=true;
  // we have to create the option_list for getopt()
  struct option options[set.size()+1];
  for (size_t i=0; i<set.size(); i++) options[i] = set[i];
  //  options[set.size()] = option_info(); // terminating NULL option
  option null_option = {0,0,0,0};
  options[set.size()] = null_option; // terminating NULL option

  // now that we have `options', we can start the parsing loop
  // reset getopt()'s state
  bool mask[argc];
  for (int i=0; i<argc; i++) mask[i]=false;
#ifdef __GLIBC__
  optind=0;
#else
  optind=1;
#endif
#ifdef HAVE_OPTRESET
  optreset=1;
#endif
  opterr=1; // we do want error messages from getopt()
  while(1) {	
    int option_index = 0;
    int this_optind=optind;
    int c = getopt_long_only(argc, argv, "-", options, &option_index); // see e.g. http://linux.die.net/man/3/getopt_long_only
    if (c==-1) {
      break;
    } else if (c==0) {
      option_info &item = set[option_index];
      switch(item.type()) {
      case OPT_DOUBLE:
	item.set(atof(optarg));
	break;
      case OPT_INT:
	item.set((int)atoi(optarg));
	break;
      case OPT_BOOL:
	if (strcmp(optarg, "0")==0) {
	  item.set(false);
	} else {
	  item.set(true);
	}
	/*if (optarg) {
	  if (strcmp(optarg, "0")==0) { 
	  item.set(false);
	  } else { 
	  item.set(true);
	  }
	  } else {
	  item.set(true);
	  }*/
       	break;
      case OPT_STRING:
      case OPT_CHAR_PTR:
	item.set(optarg);
	break;
      case OPT_COMPLEX:
	{
	  std::istringstream opt(optarg);
	  std::complex<double> value;
	  opt >> value;
	  item.set(value);
	}
	break;
      default:
	placet_cout << ERROR << "[internal error] unrecognized argument type for option " << options[option_index].name << endmsg;
	success=false;
	goto end_loop;
        break;
      }
      while(this_optind<optind)
	mask[this_optind++]=true;
    } else {
      while(this_optind<optind)
	mask[this_optind++]=false;
      if (c == '?') {
	placet_cout << ERROR << "invalid option for command " << argv[0] << endmsg;
      }
    }
  }
  if (purge_argv) {
    int first_true=1;
    int first_false=1;
    do {
      if (first_true!=first_false) {
        std::swap(argv[first_true],argv[first_false]);
        std::swap(mask[first_true],mask[first_false]);
      }
      while(first_true<argc&&!mask[first_true]) first_true++;
      first_false=first_true;
      while(first_false<argc&&mask[first_false]) first_false++;
    } while(first_false<argc);
    argc-=first_false-first_true;
    // print error messages if any leftover options:
    if (argc > 1) {
      placet_cout << ERROR << "invalid option(s) for command " << argv[0] << ":";
      for (int i=1; i<argc; i++) {
	placet_cout << " " << argv[i];
      }
      placet_cout << endmsg;
    }
  }
 end_loop:
  return success;
}

std::ostream &operator<<(std::ostream &stream, const option_set &set )
{
  size_t maxlength=0;
  for (std::vector<option_info>::const_iterator itr=set.set.begin(); itr!=set.set.end(); ++itr) {
    const option_info &option = *itr;
    if (strlen(option.get_name())>maxlength)
      maxlength=strlen(option.get_name());
  }
  for (std::vector<option_info>::const_iterator itr=set.set.begin(); itr!=set.set.end(); ++itr) {
    const option_info &option = *itr;
    if (option.description())
      stream << " -" << option.get_name() << std::setw(maxlength-strlen(option.get_name())+1) << ' ' << option.description() << std::endl;
    else
      stream << " -" << option.get_name() << std::endl;
  }
  return stream;
}

OStream &operator << (OStream &stream, const option_set &o )
{
  stream << o.size();
  for (std::vector<option_info>::const_iterator itr=o.set.begin(); itr!=o.set.end(); ++itr) {
    const option_info &option = *itr;
    int type = int(option.type());
    std::string name = option.get_name() ? option.get_name() : "";
    std::string value = option.get_value_as_string();
    stream << type << name << value;
  }
  return stream;
}

IStream &operator >> (IStream &stream, option_set &o )
{
  size_t count;
  stream >> count;
  for (size_t i=0; i<count; i++) {
    int type;
    std::string name;
    std::string value;
    stream >> type >> name >> value;
    o.set_value_from_string(name.c_str(), value);
  }
  return stream;
}
