#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>

#define PLACET
#include "placet.h"
#include "beam.h"
#include "structures_def.h"

/* Function prototypes */

#include "minimise.h"
#include "rmatrix.h"
#include "powell.h"

static struct{
  double wgt;
  double W11,W12,W22,W10,W20,W00;
  double Wpp11,Wpp12,Wpp22,Wpp10,Wpp20,Wpp00;
  double Wp11,Wp12,Wp21,Wp22,Wp10,Wp01,Wp20,Wp02,Wp00;
  BEAM *beam;
} bump_emitt_data;

static struct{
  double wgt;
  double W11,W12,W22,W10,W20,W00;
  double Wpp11,Wpp12,Wpp22,Wpp10,Wpp20,Wpp00;
  double Wp11,Wp12,Wp21,Wp22,Wp10,Wp01,Wp20,Wp02,Wp00;
  BEAM *beam;
} bump_emitt_data2;

extern BUMP_DATA bump_data;

void bump_emitt_print()
{
  placet_printf(INFO,"bump_emitt_data:\n");
  placet_printf(INFO,"%g %g %g %g %g %g\n",bump_emitt_data.W00,bump_emitt_data.W11,
	 bump_emitt_data.W22,bump_emitt_data.W10,bump_emitt_data.W20,
	 bump_emitt_data.W12);
  placet_printf(INFO,"%g %g %g %g %g %g\n",bump_emitt_data.Wp00,bump_emitt_data.Wp11,
	 bump_emitt_data.Wp22,bump_emitt_data.Wp10,bump_emitt_data.Wp20,
	 bump_emitt_data.Wp12);
  placet_printf(INFO,"%g %g %g %g %g %g\n",bump_emitt_data.Wpp00,bump_emitt_data.Wpp11,
	 bump_emitt_data.Wpp22,bump_emitt_data.Wpp10,bump_emitt_data.Wpp20,
	 bump_emitt_data.Wpp12);
}

void bump_beam_print(BUMP *bump,BEAM *beam,double a[])
{
  int i;
  for (i=0;i<beam->slices;i++){
    placet_printf(INFO,"%d %g\n",i,beam->particle[i].y-a[0]*bump->a11[i]-a[1]*bump->a12[i]);
  }
}

void bump_emitt_init_2(BEAM *beam,BUMP *bump)
{
  int i,ns,nb;
  double y=0.0,yp=0.0,a11=0.0,a12=0.0,a21=0.0,a22=0.0,wgtsum=0.0,wgt;
  double y2=0.0,yp2=0.0,a11_2=0.0,a12_2=0.0,a21_2=0.0,a22_2=0.0,wgtsum2=0.0;

  nb=beam->bunches;
  ns=beam->slices_per_bunch*beam->macroparticles;
  bump_emitt_data.beam=beam;

  bump_emitt_data.wgt=0.0;

  bump_emitt_data.W00=0.0;
  bump_emitt_data.W10=0.0;
  bump_emitt_data.W20=0.0;
  bump_emitt_data.W11=0.0;
  bump_emitt_data.W12=0.0;
  bump_emitt_data.W22=0.0;

  bump_emitt_data.Wpp00=0.0;
  bump_emitt_data.Wpp10=0.0;
  bump_emitt_data.Wpp20=0.0;
  bump_emitt_data.Wpp11=0.0;
  bump_emitt_data.Wpp12=0.0;
  bump_emitt_data.Wpp22=0.0;

  bump_emitt_data.Wp00=0.0;
  bump_emitt_data.Wp10=0.0;
  bump_emitt_data.Wp01=0.0;
  bump_emitt_data.Wp20=0.0;
  bump_emitt_data.Wp02=0.0;
  bump_emitt_data.Wp11=0.0;
  bump_emitt_data.Wp12=0.0;
  bump_emitt_data.Wp21=0.0;
  bump_emitt_data.Wp22=0.0;

  bump_emitt_data2.wgt=0.0;

  bump_emitt_data2.W00=0.0;
  bump_emitt_data2.W10=0.0;
  bump_emitt_data2.W20=0.0;
  bump_emitt_data2.W11=0.0;
  bump_emitt_data2.W12=0.0;
  bump_emitt_data2.W22=0.0;

  bump_emitt_data2.Wpp00=0.0;
  bump_emitt_data2.Wpp10=0.0;
  bump_emitt_data2.Wpp20=0.0;
  bump_emitt_data2.Wpp11=0.0;
  bump_emitt_data2.Wpp12=0.0;
  bump_emitt_data2.Wpp22=0.0;

  bump_emitt_data2.Wp00=0.0;
  bump_emitt_data2.Wp10=0.0;
  bump_emitt_data2.Wp01=0.0;
  bump_emitt_data2.Wp20=0.0;
  bump_emitt_data2.Wp02=0.0;
  bump_emitt_data2.Wp11=0.0;
  bump_emitt_data2.Wp12=0.0;
  bump_emitt_data2.Wp21=0.0;
  bump_emitt_data2.Wp22=0.0;

  for (i=0;i<ns*(nb-1);i++){
    wgt=fabs(beam->particle[i].wgt);
    if (beam->particle[i].wgt>0){
      bump_emitt_data.wgt+=wgt;
      y+=wgt*beam->particle[i].y;
      yp+=wgt*beam->particle[i].yp;
      wgtsum+=wgt;
      a11+=wgt*bump->a11[i];
      a12+=wgt*bump->a12[i];
      a21+=wgt*bump->a21[i];
      a22+=wgt*bump->a22[i];
    }
    else {
      bump_emitt_data2.wgt+=wgt;
      y2+=wgt*beam->particle[i].y;
      yp2+=wgt*beam->particle[i].yp;
      wgtsum2+=wgt;
      a11_2+=wgt*bump->a11[i];
      a12_2+=wgt*bump->a12[i];
      a21_2+=wgt*bump->a21[i];
      a22_2+=wgt*bump->a22[i];
    }
  }

  for (;i<ns*nb;i++){
    wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
    if (beam->particle[i].wgt>0.0) {
      bump_emitt_data.wgt+=wgt;
      y+=wgt*beam->particle[i].y;
      yp+=wgt*beam->particle[i].yp;
      wgtsum+=wgt;
      a11+=wgt*bump->a11[i];
      a12+=wgt*bump->a12[i];
      a21+=wgt*bump->a21[i];
      a22+=wgt*bump->a22[i];
    }
    else {
      bump_emitt_data2.wgt+=wgt;
      y2+=wgt*beam->particle[i].y;
      yp2+=wgt*beam->particle[i].yp;
      wgtsum2+=wgt;
      a11_2+=wgt*bump->a11[i];
      a12_2+=wgt*bump->a12[i];
      a21_2+=wgt*bump->a21[i];
      a22_2+=wgt*bump->a22[i];
    }
  }

  y/=wgtsum;
  yp/=wgtsum;
  a11/=wgtsum;
  a12/=wgtsum;
  a21/=wgtsum;
  a22/=wgtsum;

  y2/=wgtsum2;
  yp2/=wgtsum2;
  a11_2/=wgtsum2;
  a12_2/=wgtsum2;
  a21_2/=wgtsum2;
  a22_2/=wgtsum2;

  for (i=0;i<ns*(nb-1);i++){
    
    wgt=fabs(beam->particle[i].wgt);

    if (beam->particle[i].wgt>0.0) {

      bump_emitt_data.W11+=wgt*(bump->a11[i]-a11)
	*(bump->a11[i]-a11);
      bump_emitt_data.W12+=wgt*(bump->a11[i]-a11)
	*(bump->a12[i]-a12);
      bump_emitt_data.W22+=wgt*(bump->a12[i]-a12)
	*(bump->a12[i]-a12);
      bump_emitt_data.W10+=wgt*(bump->a11[i]-a11)
	*(beam->particle[i].y-y);
      bump_emitt_data.W20+=wgt*(bump->a12[i]-a12)
	*(beam->particle[i].y-y);
      bump_emitt_data.W00+=wgt*((beam->particle[i].y-y)
				*(beam->particle[i].y-y)+beam->sigma[i].r11);
    
      bump_emitt_data.Wpp11+=wgt*(bump->a21[i]-a21)
	*(bump->a21[i]-a21);
      bump_emitt_data.Wpp12+=wgt*(bump->a21[i]-a21)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wpp22+=wgt*(bump->a22[i]-a22)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wpp10+=wgt*(bump->a21[i]-a21)
	*(beam->particle[i].yp-yp);
      bump_emitt_data.Wpp20+=wgt*(bump->a22[i]-a22)
	*(beam->particle[i].yp-yp);
      bump_emitt_data.Wpp00+=wgt*((beam->particle[i].yp-yp)
				  *(beam->particle[i].yp-yp)
				  +beam->sigma[i].r22);
    
      bump_emitt_data.Wp11+=wgt*(bump->a11[i]-a11)
	*(bump->a21[i]-a21);
      bump_emitt_data.Wp12+=wgt*(bump->a11[i]-a11)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wp21+=wgt*(bump->a12[i]-a12)
	*(bump->a21[i]-a21);
      bump_emitt_data.Wp22+=wgt*(bump->a12[i]-a12)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wp10+=wgt*(bump->a11[i]-a11)
	*(beam->particle[i].yp-yp);
      bump_emitt_data.Wp01+=wgt*(beam->particle[i].y-y)
	*(bump->a21[i]-a21);
      bump_emitt_data.Wp20+=wgt*(bump->a12[i]-a12)
	*(beam->particle[i].yp-yp);
      bump_emitt_data.Wp02+=wgt*(beam->particle[i].y-y)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wp00+=wgt*((beam->particle[i].y-y)
				 *(beam->particle[i].yp-yp)
				 +beam->sigma[i].r12);
    }
    else {
      bump_emitt_data2.W11+=wgt*(bump->a11[i]-a11_2)
	*(bump->a11[i]-a11_2);
      bump_emitt_data2.W12+=wgt*(bump->a11[i]-a11_2)
	*(bump->a12[i]-a12_2);
      bump_emitt_data2.W22+=wgt*(bump->a12[i]-a12_2)
	*(bump->a12[i]-a12_2);
      bump_emitt_data2.W10+=wgt*(bump->a11[i]-a11_2)
	*(beam->particle[i].y-y2);
      bump_emitt_data2.W20+=wgt*(bump->a12[i]-a12_2)
	*(beam->particle[i].y-y2);
      bump_emitt_data2.W00+=wgt*((beam->particle[i].y-y2)
				*(beam->particle[i].y-y2)+beam->sigma[i].r11);
    
      bump_emitt_data2.Wpp11+=wgt*(bump->a21[i]-a21_2)
	*(bump->a21[i]-a21_2);
      bump_emitt_data2.Wpp12+=wgt*(bump->a21[i]-a21_2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wpp22+=wgt*(bump->a22[i]-a22_2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wpp10+=wgt*(bump->a21[i]-a21_2)
	*(beam->particle[i].yp-yp2);
      bump_emitt_data2.Wpp20+=wgt*(bump->a22[i]-a22_2)
	*(beam->particle[i].yp-yp2);
      bump_emitt_data2.Wpp00+=wgt*((beam->particle[i].yp-yp2)
				  *(beam->particle[i].yp-yp2)
				  +beam->sigma[i].r22);
    
      bump_emitt_data2.Wp11+=wgt*(bump->a11[i]-a11_2)
	*(bump->a21[i]-a21_2);
      bump_emitt_data2.Wp12+=wgt*(bump->a11[i]-a11_2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wp21+=wgt*(bump->a12[i]-a12_2)
	*(bump->a21[i]-a21_2);
      bump_emitt_data2.Wp22+=wgt*(bump->a12[i]-a12_2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wp10+=wgt*(bump->a11[i]-a11_2)
	*(beam->particle[i].yp-yp2);
      bump_emitt_data2.Wp01+=wgt*(beam->particle[i].y-y2)
	*(bump->a21[i]-a21_2);
      bump_emitt_data.Wp20+=wgt*(bump->a12[i]-a12_2)
	*(beam->particle[i].yp-yp2);
      bump_emitt_data2.Wp02+=wgt*(beam->particle[i].y-y2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data.Wp00+=wgt*((beam->particle[i].y-y2)
				 *(beam->particle[i].yp-yp2)
				 +beam->sigma[i].r12);
    }
  }
  for (;i<ns*nb;i++){

    wgt=fabs(beam->particle[i].wgt)*beam->last_wgt;
    if (beam->particle[i].wgt>0.0) {
      bump_emitt_data.W11+=wgt*(bump->a11[i]-a11)
	*(bump->a11[i]-a11);
      bump_emitt_data.W12+=wgt*(bump->a11[i]-a11)
	*(bump->a12[i]-a12);
      bump_emitt_data.W22+=wgt*(bump->a12[i]-a12)
	*(bump->a12[i]-a12);
      bump_emitt_data.W10+=wgt*(bump->a11[i]-a11)
	*(beam->particle[i].y-y);
      bump_emitt_data.W20+=wgt*(bump->a12[i]-a12)
	*(beam->particle[i].y-y);
      bump_emitt_data.W00+=wgt*((beam->particle[i].y-y)
				*(beam->particle[i].y-y)+beam->sigma[i].r11);

      bump_emitt_data.Wpp11+=wgt*(bump->a21[i]-a21)
	*(bump->a21[i]-a21);
      bump_emitt_data.Wpp12+=wgt*(bump->a21[i]-a21)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wpp22+=wgt*(bump->a22[i]-a22)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wpp10+=wgt*(bump->a21[i]-a21)
	*(beam->particle[i].yp-yp);
      bump_emitt_data.Wpp20+=wgt*(bump->a22[i]-a22)
	*(beam->particle[i].yp-yp);
      bump_emitt_data.Wpp00+=wgt*((beam->particle[i].yp-yp)
				  *(beam->particle[i].yp-yp)
				  +beam->sigma[i].r22);

      bump_emitt_data.Wp11+=wgt*(bump->a11[i]-a11)
	*(bump->a21[i]-a21);
      bump_emitt_data.Wp12+=wgt*(bump->a11[i]-a11)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wp21+=wgt*(bump->a12[i]-a12)
	*(bump->a21[i]-a21);
      bump_emitt_data.Wp22+=wgt*(bump->a12[i]-a12)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wp10+=wgt*(bump->a11[i]-a11)
	*(beam->particle[i].yp-yp);
      bump_emitt_data.Wp01+=wgt*(beam->particle[i].y-y)
	*(bump->a21[i]-a21);
      bump_emitt_data.Wp20+=wgt*(bump->a12[i]-a12)
	*(beam->particle[i].yp-yp);
      bump_emitt_data.Wp02+=wgt*(beam->particle[i].y-y)
	*(bump->a22[i]-a22);
      bump_emitt_data.Wp00+=wgt*((beam->particle[i].y-y)
				 *(beam->particle[i].yp-yp)
				 +beam->sigma[i].r12);
    }
    else {
      bump_emitt_data2.W11+=wgt*(bump->a11[i]-a11_2)
	*(bump->a11[i]-a11_2);
      bump_emitt_data2.W12+=wgt*(bump->a11[i]-a11_2)
	*(bump->a12[i]-a12_2);
      bump_emitt_data2.W22+=wgt*(bump->a12[i]-a12_2)
	*(bump->a12[i]-a12_2);
      bump_emitt_data2.W10+=wgt*(bump->a11[i]-a11_2)
	*(beam->particle[i].y-y2);
      bump_emitt_data2.W20+=wgt*(bump->a12[i]-a12_2)
	*(beam->particle[i].y-y2);
      bump_emitt_data2.W00+=wgt*((beam->particle[i].y-y2)
				*(beam->particle[i].y-y2)+beam->sigma[i].r11);
    
      bump_emitt_data2.Wpp11+=wgt*(bump->a21[i]-a21_2)
	*(bump->a21[i]-a21_2);
      bump_emitt_data2.Wpp12+=wgt*(bump->a21[i]-a21_2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wpp22+=wgt*(bump->a22[i]-a22_2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wpp10+=wgt*(bump->a21[i]-a21_2)
	*(beam->particle[i].yp-yp2);
      bump_emitt_data2.Wpp20+=wgt*(bump->a22[i]-a22_2)
	*(beam->particle[i].yp-yp2);
      bump_emitt_data2.Wpp00+=wgt*((beam->particle[i].yp-yp2)
				  *(beam->particle[i].yp-yp2)
				  +beam->sigma[i].r22);
    
      bump_emitt_data2.Wp11+=wgt*(bump->a11[i]-a11_2)
	*(bump->a21[i]-a21_2);
      bump_emitt_data2.Wp12+=wgt*(bump->a11[i]-a11_2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wp21+=wgt*(bump->a12[i]-a12_2)
	*(bump->a21[i]-a21_2);
      bump_emitt_data2.Wp22+=wgt*(bump->a12[i]-a12_2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wp10+=wgt*(bump->a11[i]-a11_2)
	*(beam->particle[i].yp-yp2);
      bump_emitt_data2.Wp01+=wgt*(beam->particle[i].y-y2)
	*(bump->a21[i]-a21_2);
      bump_emitt_data2.Wp20+=wgt*(bump->a12[i]-a12_2)
	*(beam->particle[i].yp-yp2);
      bump_emitt_data2.Wp02+=wgt*(beam->particle[i].y-y2)
	*(bump->a22[i]-a22_2);
      bump_emitt_data2.Wp00+=wgt*((beam->particle[i].y-y2)
				 *(beam->particle[i].yp-yp2)
				 +beam->sigma[i].r12);
    }
  }
  //  bump_emitt_print();
}

double bump_emitt_2(double a1, double a2)
{
  double S11,S12,S22;
  double S11_2,S12_2,S22_2;

  S11=bump_emitt_data.W00
    +a1*a1*bump_emitt_data.W11
    +a2*a2*bump_emitt_data.W22
    +2.0*a1*a2*bump_emitt_data.W12
    +2.0*a1*bump_emitt_data.W10
    +2.0*a2*bump_emitt_data.W20;

  S12=bump_emitt_data.Wp00
    +a1*a1*bump_emitt_data.Wp11
    +a2*a2*bump_emitt_data.Wp22
    +a1*a2*bump_emitt_data.Wp12
    +a1*a2*bump_emitt_data.Wp21
    +a1*bump_emitt_data.Wp10
    +a1*bump_emitt_data.Wp01
    +a2*bump_emitt_data.Wp20
    +a2*bump_emitt_data.Wp02;

  S22=bump_emitt_data.Wpp00
    +a1*a1*bump_emitt_data.Wpp11
    +a2*a2*bump_emitt_data.Wpp22
    +2.0*a1*a2*bump_emitt_data.Wpp12
    +2.0*a1*bump_emitt_data.Wpp10
    +2.0*a2*bump_emitt_data.Wpp20;

  S11_2=bump_emitt_data2.W00
    +a1*a1*bump_emitt_data2.W11
    +a2*a2*bump_emitt_data2.W22
    +2.0*a1*a2*bump_emitt_data2.W12
    +2.0*a1*bump_emitt_data2.W10
    +2.0*a2*bump_emitt_data2.W20;

  S12_2=bump_emitt_data2.Wp00
    +a1*a1*bump_emitt_data2.Wp11
    +a2*a2*bump_emitt_data2.Wp22
    +a1*a2*bump_emitt_data2.Wp12
    +a1*a2*bump_emitt_data2.Wp21
    +a1*bump_emitt_data2.Wp10
    +a1*bump_emitt_data2.Wp01
    +a2*bump_emitt_data2.Wp20
    +a2*bump_emitt_data2.Wp02;

  S22_2=bump_emitt_data2.Wpp00
    +a1*a1*bump_emitt_data2.Wpp11
    +a2*a2*bump_emitt_data2.Wpp22
    +2.0*a1*a2*bump_emitt_data2.Wpp12
    +2.0*a1*bump_emitt_data2.Wpp10
    +2.0*a2*bump_emitt_data2.Wpp20;

  //  placet_printf(INFO,"bump_emitt %g %g %g %g %g\n",a1,a2,S11,S12,S22);
  return S11*S22-S12*S12+S11_2*S22_2-S12_2*S12_2;
}

namespace {
  double func(double x[])
  {
    double tmp;
    //  tmp=bump_emitt(x[0],x[1]);
    tmp=bump_emitt_2(x[0],x[1]);
    if (fabs(x[0])>bump_data.maxshift){
      tmp+=bump_emitt_data.wgt
	*(x[0]-bump_data.maxshift)*(x[0]-bump_data.maxshift);
    }
  if (fabs(x[1])>bump_data.maxshift){
    tmp+=bump_emitt_data.wgt
      *(x[1]-bump_data.maxshift)*(x[1]-bump_data.maxshift);
  }
  return tmp;
  }
}

void bump_minimise(BEAM *beam,BUMP *bump,double p[])
{
  double xi[4]={1.0,0.0,0.0,1.0},frep,p0[2]={0.0,0.0};
  int iter=0;
  return; // early return statement!!
  // this function and subsequently bump_calculate do nothing! - JS

  //  bump_emitt_init(beam,bump);
  bump_emitt_init_2(beam,bump);
  placet_printf(INFO,"init %g %g %g\n",p[0],p[1],sqrt(func(p)));
  POWELL powe(&func);
  powe.powell(p0,xi,2,2,1e-12,&iter,&frep);
  p[0]=p0[0]; p[1]=p0[1];
  placet_printf(INFO,"exit %g %g %g %d\n",p[0],p[1],sqrt(func(p)),iter); 
}
