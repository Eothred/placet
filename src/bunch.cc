#ifdef _OPENMP
#include <omp.h>
#endif
#include <iostream>
#include <utility>


#include "placet.h"
#include "beam.h"
#include "rmatrix.h"

double bunch_get_offset(const BEAM* bunch, double PARTICLE::* value)
{
  int i=0;
  double ym=0.0,wsum=0.0;
  #pragma omp parallel reduction(+:ym,wsum)
{ int nb=bunch->bunches;
  int ns=bunch->slices_per_bunch*bunch->macroparticles;
#pragma omp for
  for (i=0;i<ns*(nb-1);i++){
    ym+=(bunch->particle[i].*value)*bunch->particle[i].wgt;
    wsum+=bunch->particle[i].wgt;
  } 
#pragma omp for  
  for (i=ns*(nb-1);i<ns*nb;i++){
    ym+=(bunch->particle[i].*value)*bunch->particle[i].wgt*bunch->last_wgt;
    wsum+=bunch->particle[i].wgt*bunch->last_wgt;
  }
}
  return ym/wsum;
}

std::vector<double> bunch_get_moments(const BEAM *bunch )
{
  double wgtsum=0.0,xsum=0.0,ysum=0.0,xpsum=0.0,ypsum=0.0,xxsum=0.0,xxpsum=0.0,xpxpsum=0.0,yysum=0.0,yypsum=0.0,ypypsum=0.0;
  if (bunch->particle_beam) {
#pragma omp parallel for reduction(+:wgtsum,xsum,ysum,xpsum,ypsum,xxsum,xxpsum,xpxpsum,yysum,yypsum,ypypsum) 
    for (int i=0;i<bunch->slices;i++) {
      const PARTICLE &particle=bunch->particle[i];
      double wgt=fabs(particle.wgt);
      wgtsum+=wgt;
      xsum+=wgt*particle.x;
      xpsum+=wgt*particle.xp;
      ysum+=wgt*particle.y;
      ypsum+=wgt*particle.yp;
      xxsum+=wgt*(particle.x*particle.x);
      xxpsum+=wgt*(particle.x*particle.xp);
      xpxpsum+=wgt*(particle.xp*particle.xp);
      yysum+=wgt*(particle.y*particle.y);
      yypsum+=wgt*(particle.y*particle.yp);
      ypypsum+=wgt*(particle.yp*particle.yp);
    }
  } else {
#pragma omp parallel for reduction(+:wgtsum,xsum,ysum,xpsum,ypsum,xxsum,xxpsum,xpxpsum,yysum,yypsum,ypypsum) 
    for (int i=0;i<bunch->slices;i++) {
      const PARTICLE &particle=bunch->particle[i];
      double wgt=fabs(particle.wgt);
      wgtsum+=wgt;
      xsum+=wgt*particle.x;
      xpsum+=wgt*particle.xp;
      ysum+=wgt*particle.y;
      ypsum+=wgt*particle.yp;
      xxsum+=wgt*(bunch->sigma_xx[i].r11+particle.x*particle.x);
      xxpsum+=wgt*(bunch->sigma_xx[i].r12+particle.x*particle.xp);
      xpxpsum+=wgt*(bunch->sigma_xx[i].r22+particle.xp*particle.xp);
      yysum+=wgt*(bunch->sigma[i].r11+particle.y*particle.y);
      yypsum+=wgt*(bunch->sigma[i].r12+particle.y*particle.yp);
      ypypsum+=wgt*(bunch->sigma[i].r22+particle.yp*particle.yp);
    }
  }
  if (wgtsum>std::numeric_limits<double>::epsilon()) {
    xsum/=wgtsum;
    xpsum/=wgtsum;
    ysum/=wgtsum;
    ypsum/=wgtsum;
    xxsum/=wgtsum;
    xxpsum/=wgtsum;
    xpxpsum/=wgtsum;
    yysum/=wgtsum;
    yypsum/=wgtsum;
    ypypsum/=wgtsum;
  }
  std::vector<double> retval(10);
  retval[0] = xsum;
  retval[1] = ysum;
  retval[2] = xpsum;
  retval[3] = ypsum;
  retval[4] = xxsum;
  retval[5] = yysum;
  retval[6] = xpxpsum;
  retval[7] = ypypsum;
  retval[8] = xxpsum;
  retval[9] = yypsum;
  return retval;
}

std::pair<double,double> bunch_get_energy(const BEAM *bunch ) ///< Returns average energy, and energy spread
{
  double wgtsum=0.0,esum=0.0,e2sum=0.0;
#pragma omp parallel for reduction (+: wgtsum,esum,e2sum)  
  for (int i=0;i<bunch->slices;i++) {
    const PARTICLE &particle=bunch->particle[i];
    double e=fabs(particle.energy);
    double wgt=fabs(particle.wgt);
    wgtsum+=wgt;
    esum+=wgt*e;
    e2sum+=wgt*e*e;
  }
  if (wgtsum>std::numeric_limits<double>::epsilon()) {
    esum /= wgtsum;
    e2sum /= wgtsum;
    return std::pair<double,double>(esum, sqrt(e2sum - esum*esum));
  }
  return std::pair<double,double>(0.0, 0.0);
}

double bunch_get_offset_y(const BEAM *bunch)
{
  return bunch_get_offset(bunch,&PARTICLE::y);
}

double bunch_get_offset_yp(const BEAM *bunch)
{
  return bunch_get_offset(bunch,&PARTICLE::yp);
}

#ifdef TWODIM
double bunch_get_offset_x(const BEAM *bunch)
{
  return bunch_get_offset(bunch,&PARTICLE::x);
}
double bunch_get_offset_xp(const BEAM *bunch)
{
  return bunch_get_offset(bunch,&PARTICLE::xp);
}
#endif

double bunch_get_length(const BEAM *beam, int bunch )
{
  double wgtsum=0.0, zsum=0.0, z2sum=0.0; 
  if (beam->particle_beam) {
#pragma omp parallel for reduction(+:wgtsum,zsum,z2sum)  
    for (int i=0;i<beam->slices; i++) {
      const PARTICLE &particle=beam->particle[i];
      double wgt=particle.wgt;
      double zp=particle.z;
      zsum+=wgt*zp;
      z2sum+=wgt*zp*zp;
      wgtsum+=wgt;
    }
  } else {
#pragma omp parallel for reduction(+:wgtsum,zsum,z2sum)  
    for (int i=0;i<beam->slices_per_bunch; i++) {
      const double zp = beam->z_position[bunch*beam->slices_per_bunch+i]; 
      for (int j=0;j<beam->macroparticles; j++) {
	const PARTICLE &particle=beam->particle[bunch*beam->slices_per_bunch*beam->macroparticles+i*beam->macroparticles+j];
	double wgt=particle.wgt;
	zsum+=wgt*zp;
	z2sum+=wgt*zp*zp;
	wgtsum+=wgt;
      }
    }
  }
  if (fabs(wgtsum)>std::numeric_limits<double>::epsilon()) {
    zsum /= wgtsum;
    z2sum /= wgtsum;
    return sqrt(z2sum - zsum*zsum);
  }
  return 0.0;
}
