#include <cstdlib>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "beamline.h"
#include "cavity.h"
#include "girder.h"
#include "quadrupole.h"
//#include "matrix.h"
#include "placet_tk.h"
#include "placeti3.h"
#include "placeti4.h"
#include "random.hh"
#include "bpm.h"
#include "track.h"
#include "bin.h"

#include "static_rf_align.h"

extern ERRORS errors;

class _Fit {
  double sumyy;
  double sumxy;
  double sumy;
  double sumxx;
  double sumx;
  int n;
public:
  _Fit() : sumyy(0.0), sumxy(0.0), sumy(0.0), sumxx(0.0), sumx(0.0), n(0), a(0.0), b(0.0) {}
  double a, b;
  void add_point(double x, double y )
  {
    sumxx+=x*x;
    sumxy+=x*y;
    sumyy+=y*y;
    sumx+=x;
    sumy+=y;
    n++;
  }
  void fit()
  {
    double Sxx = sumxx-sumx*sumx/n;
    double Sxy = sumxy-sumx*sumy/n;
    //    double Syy = sumyy-sumy*sumy/n;
    a = Sxy/Sxx; // regression coefficient
    b = (sumy-a*sumx)/n;
  }
  double operator()(double x ) const 
  {
    return a * x + b;
  }
  int points() const
  {
    return n;
  }
};

void align_rf_girder(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0, BEAM *tb, BEAM *workbunch,BEAM *probe, int girder_alignment )
{
  placet_printf(INFO,"alignRF Girder\n");
  int ipos=0;
  bunch_copy_0(bunch0,tb);
  for (int i=0;i<nbin;i++) {
    GIRDER *girder_first=NULL;
    GIRDER *girder_last=NULL;
    // double z0=0.0;
    int first_quad=bin[i]->quad[0];
    int last_bpm=bin[i]->bpm[bin[i]->nbpm-1];
    for (int j=ipos;j<first_quad;j++) {
      if (CAVITY *cavity_ptr=beamline->element[j]->cavity_ptr())
        cavity_ptr->track_rf_0(tb);
      else
        beamline->element[j]->track_0(tb);
    }
    for (int iter=0; iter<3; iter++) {
      bunch_copy_0(tb, workbunch);
      for (int j=first_quad;j<=last_bpm;j++) {
	if (CAVITY *cavity_ptr=beamline->element[j]->cavity_ptr())
	  cavity_ptr->track_rf_0(workbunch);
	else
	  beamline->element[j]->track_0(workbunch);
      }
      ipos=first_quad;
      for (GIRDER *girder_ptr=beamline->first;girder_ptr!=NULL;girder_ptr=girder_ptr->next()) {
	for (ELEMENT *element_ptr=girder_ptr->element();element_ptr!=NULL;element_ptr=element_ptr->next) {
	  if (element_ptr==beamline->element[first_quad]) {
	    girder_first=girder_ptr; 
	    break;
	  } else if (element_ptr==beamline->element[last_bpm]) { 
	    girder_last=girder_ptr; 
	    break;
	  }
	}
	if (girder_first&&girder_last) {
	  break;
	}
	//	z0+=girder_ptr->length+girder_ptr->dist;
      }
      // here we align the girders
      if (girder_first&&girder_last) {
	if (girder_alignment==PER_BIN) {
	  _Fit fit_x;
	  _Fit fit_y;
	  double z0=0.0;
	  for (GIRDER *girder_ptr=girder_first;girder_ptr!=girder_last;girder_ptr=girder_ptr->next()) {
	    // let's collect the bpm's readings of all cavities on the girder
	    double z_element=z0;
	    for (ELEMENT *element_ptr=girder_ptr->element();element_ptr!=NULL;element_ptr=element_ptr->next) {
	      ELEMENT &element=*element_ptr;
	      z_element+=0.5*element.get_length();
	      if (element.is_cavity()) {
		double x=element.get_bpm_x_reading_exact();
		double y=element.get_bpm_y_reading_exact();
		fit_x.add_point(z_element,x);
		fit_y.add_point(z_element,y);
	      }
	      z_element+=0.5*element.get_length();
	    }
	    z0+=girder_ptr->get_length()+girder_ptr->distance_to_prev_girder();
	  }
	  if (fit_x.points()>=2) {
	    fit_x.fit();
	    fit_y.fit();
	    double atan_x=atan(fit_x.a);
	    double atan_y=atan(fit_y.a);
	    z0=0.0;
	    for (GIRDER *girder_ptr=girder_first;girder_ptr!=girder_last;girder_ptr=girder_ptr->next()) {
	      double z_element=z0;
	      for (ELEMENT *element_ptr=girder_ptr->element();element_ptr!=NULL;element_ptr=element_ptr->next) {
		ELEMENT &element=*element_ptr;
		z_element+=0.5*element.get_length();
		if (element.is_cavity()) {
		  element.offset.x+=fit_x(z_element);
		  element.offset.y+=fit_y(z_element);
		  element.offset.xp+=atan_x;
		  element.offset.yp+=atan_y;
		}
		z_element+=0.5*element.get_length();
	      }
	      z0+=girder_ptr->get_length()+girder_ptr->distance_to_prev_girder();
	    }
	  }
	} else if (girder_alignment==PER_GIRDER) {
	  double z0=0.0;
	  for (GIRDER *girder_ptr=girder_first;girder_ptr!=girder_last;girder_ptr=girder_ptr->next()) {
	    // let's collect the bpm's readings of all cavities on the girder
	    _Fit fit_x;
	    _Fit fit_y;
	    double z_element=z0;
	    for (ELEMENT *element_ptr=girder_ptr->element();element_ptr!=NULL;element_ptr=element_ptr->next) {
	      ELEMENT &element=*element_ptr;
	      z_element+=0.5*element.get_length();
	      if (element.is_cavity()) {
		double x=element.get_bpm_x_reading_exact();
		double y=element.get_bpm_y_reading_exact();
		fit_x.add_point(z_element,x);
		fit_y.add_point(z_element,y);
	      }
	      z_element+=0.5*element.get_length();
	    }
	    if (fit_x.points()>=2) {
	      fit_x.fit();
	      fit_y.fit();
	      double atan_x=atan(fit_x.a);
	      double atan_y=atan(fit_y.a);
	      z_element=z0;
	      for (ELEMENT *element_ptr=girder_ptr->element();element_ptr!=NULL;element_ptr=element_ptr->next) {
		ELEMENT &element=*element_ptr;
		z_element+=0.5*element.get_length();
		if (element.is_cavity()) {
		  element.offset.x+=fit_x(z_element);
		  element.offset.y+=fit_y(z_element);
		  element.offset.xp+=atan_x;
		  element.offset.yp+=atan_y;
		}
		z_element+=0.5*element.get_length();
	      }
	      z0+=girder_ptr->get_length()+girder_ptr->distance_to_prev_girder();
	    }
	  }
	}
      }
      bunch_copy_0(tb,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      bin_correct(beamline,0,bin[i]);
    }
  }
  if (probe){
    bunch_track_line_emitt_new(beamline,probe);
  }
}

/*
 Move the extremities of the girders : CAV elements will move...
 */

void inter_girder_move(BEAMLINE * /*beamline*/,double ampl_y,double ampl_x, 
			 double flo_y,double flo_x,int cav_only)
{
  GIRDER* girder=inter_data.beamline->first;
  double x2=ampl_x*Misalignments.Gauss();
  double y2=ampl_y*Misalignments.Gauss();
  double x1,y1;
  while (girder) {
    x1=x2;
    y1=y2;
    x2=ampl_x*Misalignments.Gauss();
    y2=ampl_y*Misalignments.Gauss();
    if (cav_only) {
      girder->move_ends_cav(x1+flo_x*Misalignments.Gauss(),x2+flo_x*Misalignments.Gauss(),
			    y1+flo_y*Misalignments.Gauss(),y2+flo_y*Misalignments.Gauss());
    } else {
      girder->move_ends(x1+flo_x*Misalignments.Gauss(),x2+flo_x*Misalignments.Gauss(),
			y1+flo_y*Misalignments.Gauss(),y2+flo_y*Misalignments.Gauss());
    }
    girder=girder->next();
  }
}

void
scatter_girder(BEAMLINE * /*beamline*/,double ampl_y,double ampl_yp,
	       double ampl_x,double ampl_xp,int cav_only)
{
  GIRDER* girder=inter_data.beamline->first;
  double x,y,xp,yp;
  while (girder) {
    x=ampl_x*Misalignments.Gauss();
    y=ampl_y*Misalignments.Gauss();
    xp=ampl_xp*Misalignments.Gauss();
    yp=ampl_yp*Misalignments.Gauss();
    if (cav_only) {
      girder->move_cav(y,0.0);
      girder->move_cav_x(x,0.0);
    } else {
      girder->move(y,yp);
      girder->move_x(x,xp);
    }
    girder=girder->next();
  }
}

int tk_TestRfAlignment(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		       char *argv[])
{
  int error,i;
  int nq=1;
  int nbin,iter=1;
  void (*survey)(BEAMLINE*);
  char *beamname=NULL,*testbeamname=NULL,*survey_name,sn[]="Clic";
  char *format = "%s %ex %sex %x 0 %ey %sey %Env 0 %n";
  char *file_name=NULL;
  double wgt0=1.0,wgt1=0.0,pwgt=0.0;
  GIRDER_ALIGNMENT girder_alignment=NONE;
  BIN **bin;
  BEAM *beam=NULL,*b,*testbeam=NULL,*tb,*workbeam,*pb=NULL;

  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for correction"},
    {(char*)"-testbeam",TK_ARGV_STRING,(char*)NULL,
     (char*)&testbeamname,
     (char*)"Name of the beam to be used for evaluating the corrected beamline"},
   {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines"},
   {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nq,
     (char*)"Length of the correction bins"},
    {(char*)"-wgt0",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt0,
     (char*)"Weight for the BPM position"},
    {(char*)"-wgt1",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt1,
     (char*)"Weight for the BPM resolution"},
    {(char*)"-pwgt",TK_ARGV_FLOAT,(char*)NULL,(char*)&pwgt,
     (char*)"Weight for the old position"},
    {(char*)"-girder",TK_ARGV_INT,(char*)NULL,(char*)&girder_alignment,
     (char*)"Girder alignment model: 0=none; 1=per girder; 2=per bin"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to <Clic>"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,
     (char*)&format,
     (char*)"emittance file format"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  survey_name=sn;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestRfAlign",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (nq<1){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be at least 1\n",NULL);
    error=TCL_ERROR;
  }

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (testbeamname!=NULL){
    testbeam=get_beam(testbeamname);
  }

  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (error) return error;

  bin=(BIN**)malloc(sizeof(BIN*)*MAX_BIN); 

  b=bunch_remake(beam);
  tb=bunch_remake(beam);
  workbeam=bunch_remake(beam);
  if (testbeam) {
    pb=bunch_remake(testbeam);
  }
  if (file_name) {
    emitt_store_init(inter_data.beamline->n_quad);
  }
  beam_copy(beam,b);
  beam_copy(beam,workbeam);
  // scd temp 2004
  beamline_bin_divide(inter_data.beamline,nq,0,bin,&nbin);
  simple_bin_fill(inter_data.beamline,bin,nbin,b,workbeam);
  //  corr.pwgt=pwgt;
  //  corr.w=wgt1;
  //  corr.w0=wgt0;
  //  placet_printf(INFO,"weights: %g %g %g\n",corr.pwgt,corr.w,corr.w0);
  //  beamline_bin_divide(inter_data.beamline,12,0,bin,&nbin);
  //  train_bin_fill(inter_data.beamline,b,workbeam,bin,nbin);
  // scd ent temp
  for (i=0;i<iter;i++) {
    if (testbeam) {
      beam_copy(testbeam,pb);
    }
    beam_copy(beam,b);
    beam_copy(beam,tb);
    beam_copy(beam,workbeam);
    survey(inter_data.beamline);
    if (girder_alignment==NONE)    
      align_rf(inter_data.beamline,bin,nbin,b,tb,workbeam,pb);
    else 
      align_rf_girder(inter_data.beamline,bin,nbin,b,tb,workbeam,pb,girder_alignment);
    if(testbeamname==NULL) {
      beam_copy(beam,tb);
      bunch_track_line_emitt_new(inter_data.beamline,tb);
    }
  }
  if (file_name) {
    emitt_print(file_name,format);
    emitt_store_delete();
  }
  beam_delete(workbeam);
  beam_delete(tb);
  if (pb) {
    beam_delete(pb);
  }
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);
  return TCL_OK;
}
