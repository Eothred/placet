#include <cstdio>

#include "girder.h"

#include "element.h"

GIRDER::GIRDER(double len){
  length=len;
  next_girder=NULL;
  first_element=NULL;
  dist=0.0;
}

void GIRDER::add_element(ELEMENT* element){
  double z = length+0.5*element->get_length();
  add_element(element,z);
  length+=element->get_length();
}

void GIRDER::add_element(ELEMENT* element, double z){
  ELEMENT *point=first_element;
  if (point==NULL){
    first_element=element;
  } else {
    while (point->next!=NULL){
      point=point->next;
    }
    point->next=element;
  }
  element->next=NULL;
  element->set_z(z);
}

void GIRDER::insert_element(ELEMENT* element, double z){
  if (z<=first_element->get_z()) { // first element
    element->next = first_element;
    first_element = element;
  }
  else {
    ELEMENT* el = first_element;
    ELEMENT* prev_el = first_element;
    while(el!=NULL){
      if (z<=el->get_z()) break;
      prev_el = el;
      el=el->next;
    }
    prev_el->next=element; // put new element between prev_el and el
    element->next=el;
    while (el!=NULL){ // adjust z positions of remaining elements
      el->set_z(el->get_z()+element->get_length());
      el=el->next;
    }
  }
  length+=element->get_length();
  element->set_z(z);
}

void GIRDER::move_ends(double x1,double x2,double y1 ,double y2, bool cav_only)const{
  ELEMENT *element;
  double dyp,dxp,dx,dy;
  double eps=1e-7,eps2=1e-7;
  element=first_element;

  if ((length+dist)<eps) {
    if (fabs(x1-x2)>eps2||fabs(y2-y1)>eps2) {
      placet_printf(WARNING,"%g %g %g %g %g\n",
	      length+dist,x2,x1,y1,y2);
      placet_printf(WARNING,
	   "WARNING: girder too short to allow for different misalignments\n");
      //exit(1);
      return;
    }
    dx=x1;
    dy=y1;
    while (element!=NULL){
      if (cav_only==false || element->is_cavity()) {
	element->offset.y+=dy;
	element->offset.x+=dx;
      }
      element=element->next;
    }
  }
  else {
    dyp=(y2-y1)/(length+dist);
    dxp=(x2-x1)/(length+dist);
    dy=0.5*((y2+y1)+dist/length*y1);
    dx=0.5*((x2+x1)+dist/length*x1);
    while (element!=NULL){
      if (cav_only==false || element->is_cavity()) {
	element->offset.y+=dy+element->get_z()*dyp;
	element->offset.yp+=dyp;
	element->offset.x+=dx+element->get_z()*dxp;
	element->offset.xp+=dxp;
      }
      element=element->next;
    }
  }
}

void GIRDER::move_ends(double x1,double x2,double y1 ,double y2)const{
  move_ends(x1,x2,y1,y2,false);
}

void GIRDER::move_ends_cav(double x1,double x2,double y1 ,double y2)const{
  move_ends(x1,x2,y1,y2,true);
}

void GIRDER::move(double dy, double dyp,bool cav_only)const{
  ELEMENT *element=first_element;
  while (element!=NULL){
    if (cav_only==false || element->is_cavity()) {
      element->offset.y+=dy+element->get_z()*dyp;
      element->offset.yp+=dyp;
    }
    element=element->next;
  }
}

void GIRDER::move(double dy, double dyp)const{
  move(dy,dyp,false);
}

void GIRDER::move_cav(double dy, double dyp)const{
  move(dy,dyp,true);
}

#ifdef TWODIM

void GIRDER::move_x(double dx, double dxp, bool cav_only)const{
  ELEMENT *element=first_element;
  while (element!=NULL){
    if (cav_only==false || element->is_cavity()){
      element->offset.x+=dx+element->get_z()*dxp;
      element->offset.xp+=dxp;
    }
    element=element->next;
  }
}

void GIRDER::move_x(double dx, double dxp)const{
  move_x(dx,dxp,false);
}

void GIRDER::move_cav_x(double dx, double dxp)const{
  move_x(dx,dxp,true);
}

#endif

double GIRDER::calc_length()const{
  double l=0.0;
  ELEMENT* element = first_element;
  while(element!=NULL){
    l+=element->get_length();
    element=element->next;
  }
  return l;
}

void GIRDER::print(FILE* file)const{
  int i=0;
  fprintf(file,"GIRDER: dist %g length %g\n",distance_to_prev_girder(),get_length());
  ELEMENT* el=element();
  while(el!=NULL){
    fprintf(file,"ELEMENT N0 %d: %g %g\n",i++,
	    el->get_z()-0.5*el->get_length(),
	    el->get_z()+0.5*el->get_length());
    el->print(file);
    el=el->next;
  }
}
