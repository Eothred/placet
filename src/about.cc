
#include "placet_tk.h"
#include "config.h"
#include <tcl.h>
#include <tk.h>
#ifdef _OPENMP
#include <omp.h>
#endif
#include <stdlib.h>
#include <sys/utsname.h>

int tk_About(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[])
{ 

// First some basic info about this version..
placet_cout << INFO << "PLACET Version No " PACKAGE_VERSION;
#ifdef PLACET_SVN_REVISION
  placet_cout << " (SVN revision " PLACET_SVN_REVISION ")";
#endif
  placet_cout << endmsg;
  placet_cout << INFO << "Enabled interfaces: ";
#if OCTAVE_INTERFACE == 1
  placet_cout << "Octave";
#endif
#if OCTAVE_INTERFACE == 1 && PYTHON_INTERFACE == 1
  placet_cout << ", ";
#endif
#if PYTHON_INTERFACE == 1
  placet_cout << "Python";
#endif
placet_cout << endmsg;
  placet_cout << INFO << "Enabled modules: ";
#if MPI_MODULE == 1
  placet_cout << "MPI";
#endif
placet_cout << "\n" << endmsg;

// Author information...
placet_cout << INFO <<  "Main author: D. Schulte" << endmsg;
placet_cout << INFO << "Contributions from:" << endmsg;
placet_cout << INFO << "  A. Latina, N. Leros," << endmsg;
placet_cout << INFO << "  P. Eliasson, E. D'Amico, E. Adli," << endmsg;
placet_cout << INFO << "  B. Dalena, J. Snuverink, Y. Levinsen\n" << endmsg;

// System information..
placet_cout << INFO << "System information:" << endmsg;
struct utsname unameData;
uname(&unameData);
placet_cout << INFO << "  " << unameData.sysname << " " << unameData.release << endmsg;
placet_cout << INFO << "  Architecture: " << unameData.machine << endmsg;
// This is quite useful, as it will give the number of threads for *THIS RUN*
#ifdef _OPENMP
  placet_cout << INFO <<  "  Number of threads available: " << omp_get_max_threads() << endmsg;
#endif
  Tcl_SetResult(interp, "", TCL_VOLATILE);
  return TCL_OK;
}

