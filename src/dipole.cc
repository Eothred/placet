#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "dipole.h"

DIPOLE::DIPOLE(double l,double _a_x,double _a_y ) : ELEMENT(l)
{
    a_x = _a_x;
    a_y = _a_y;
    attributes.add("strength_x", "Horizontal integrated strength [urad*GeV=keV]", OPT_DOUBLE, &a_x);
    attributes.add("strength_y", "Vertical integrated strength [urad*GeV=keV]", OPT_DOUBLE, &a_y);
}

void DIPOLE::step_partial(BEAM *beam, double l, void (ELEMENT::*step_function)(BEAM*) )
{
  double _a_x=a_x;
  double _a_y=a_y;
  a_x*=l;
  a_y*=l;
  ELEMENT::step_partial(beam,l,step_function);
  a_x=_a_x;
  a_y=_a_y;
}


void DIPOLE::step_4d_0(BEAM *beam )
{
  drift_step_4d_0(beam,0.5);
  for (int i=0;i<beam->slices;i++){
    PARTICLE &particle=beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      particle.xp+=a_x/particle.energy;
      particle.yp+=a_y/particle.energy;
    }
  }
  drift_step_4d_0(beam,0.5);
}

void DIPOLE::step_4d(BEAM *beam )
{
  drift_step_4d(beam, 0.5);
  for (int i=0;i<beam->slices;i++){
    PARTICLE &particle=beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      particle.xp+=a_x/particle.energy;
      particle.yp+=a_y/particle.energy;
    }
  }
  drift_step_4d(beam, 0.5);
}

void DIPOLE::step_6d_0(BEAM *beam )
{
  drift_step_6d_0(beam,0.5);
  for (int i=0;i<beam->slices;i++){
    PARTICLE &particle=beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      particle.xp+=a_x/particle.energy;
      particle.yp+=a_y/particle.energy;
    }
  }
  drift_step_6d_0(beam,0.5);
}

void DIPOLE::step_6d(BEAM *beam )
{
  drift_step_6d(beam, 0.5);
  for (int i=0;i<beam->slices;i++){
    PARTICLE &particle=beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      particle.xp+=a_x/particle.energy;
      particle.yp+=a_y/particle.energy;
    }
  }
  drift_step_6d(beam, 0.5);
}
void DIPOLE::step_twiss(BEAM *beam,FILE * /*file*/,double /*step*/,
			int /*j*/,double /*s0*/, int /*n1*/,int /*n2*/, 
			void (* /*callback*/)(FILE*,BEAM*,int,double,int,int))
{
  static bool warned = false;
  if (!warned) {
    placet_cout << WARNING << "DIPOLE::step_twiss : no twiss computed, tracking only" << endmsg;
    warned = true;
  }
  track(beam);
}

void DIPOLE::step_twiss_0(BEAM *beam,FILE * /*file*/,double /*step*/,
			  int /*j*/,double /*s0*/, int /*n1*/, int /*n2*/,
			  void (* /*callback*/)(FILE*,BEAM*,int,double,int,int))
{
  static bool warned = false;
  if (!warned) {
    placet_cout << WARNING << " DIPOLE::step_twiss_0: no twiss computed, tracking only" << endmsg;
    warned = true;
  }
  track_0(beam);
}

void DIPOLE::GetMagField(PARTICLE* particle, double* bfield)
{
    ELEMENT::GetMagField(particle, bfield);
    placet_cout << WARNING << "IRTracking code does not consider dipole fields correctly yet.." << endmsg;
}

