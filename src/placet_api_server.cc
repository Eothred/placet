#include "file_buffered_stream.hh"
#include "placet_api_server.hh"

int placet_api_dispatcher(int ifiledes, int ofiledes )
{
  File_Buffered_OStream OSTREAM(ofiledes);
  File_IStream ISTREAM(ifiledes);
  if (ISTREAM && OSTREAM) {
    int command;
    ISTREAM >> command;
    switch(command) {
      case PLACET_TEST_NO_CORRECTION: {
        MatrixNd out_emitt;
        MatrixNd out_beam;
        std::string in_beamline;
        std::string in_beam;
        std::string in_survey;
        int in_nmachines;
        int in_start;
        int in_end;
        std::string in_format;
        ISTREAM >> in_beamline;
        ISTREAM >> in_beam;
        ISTREAM >> in_survey;
        ISTREAM >> in_nmachines;
        ISTREAM >> in_start;
        ISTREAM >> in_end;
        ISTREAM >> in_format;
        placet_test_no_correction(out_emitt,out_beam,in_beamline,in_beam,in_survey,in_nmachines,in_start,in_end,in_format);
        OSTREAM << out_emitt;
        OSTREAM << out_beam;
      }
      break;
      case PLACET_TEST_NO_CORRECTION2: {
        MatrixNd out_emitt;
        MatrixNd out_beam;
        std::string in_beamline;
        std::string in_beam;
        std::string in_survey;
        std::string in_format;
        ISTREAM >> in_beamline;
        ISTREAM >> in_beam;
        ISTREAM >> in_survey;
        ISTREAM >> in_format;
        placet_test_no_correction(out_emitt,out_beam,in_beamline,in_beam,in_survey,in_format);
        OSTREAM << out_emitt;
        OSTREAM << out_beam;
      }
      break;
      case PLACET_GET_BPM_READINGS: {
        MatrixNd out_readings;
        MatrixNd out_positions;
        std::vector<size_t> out_indexes;
        MatrixNd out_transmission;
        std::string in_beamline;
        std::vector<size_t> in_bpms;
        bool in_exact;
        ISTREAM >> in_beamline;
        ISTREAM >> in_bpms;
        ISTREAM >> in_exact;
        placet_get_bpm_readings(out_readings,out_positions,out_indexes,out_transmission,in_beamline,in_bpms,in_exact);
        OSTREAM << out_readings;
        OSTREAM << out_positions;
        OSTREAM << out_indexes;
        OSTREAM << out_transmission;
      }
      break;
      case PLACET_GET_NUMBER_LIST: {
        std::vector<size_t> out_indexes;
        std::string in_beamline;
        std::vector<std::string> in_types;
        ISTREAM >> in_beamline;
        ISTREAM >> in_types;
        placet_get_number_list(out_indexes,in_beamline,in_types);
        OSTREAM << out_indexes;
      }
      break;
      case PLACET_GET_NAME_NUMBER_LIST: {
        std::vector<size_t> out_indexes;
        std::string in_beamline;
        std::string in_pattern;
        ISTREAM >> in_beamline;
        ISTREAM >> in_pattern;
        placet_get_name_number_list(out_indexes,in_beamline,in_pattern);
        OSTREAM << out_indexes;
      }
      break;
      case PLACET_ELEMENT_SET_ATTRIBUTE: {
        std::string in_beamline;
        std::vector<size_t> in_indexes;
        std::vector<std::string> in_attributes;
        std::vector<Attribute> in_value;
        ISTREAM >> in_beamline;
        ISTREAM >> in_indexes;
        ISTREAM >> in_attributes;
        ISTREAM >> in_value;
        placet_element_set_attribute(in_beamline,in_indexes,in_attributes,in_value);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_ELEMENT_GET_ATTRIBUTE: {
        std::vector<Attribute> out_value;
        std::string in_beamline;
        std::vector<size_t> in_indexes;
        std::vector<std::string> in_attributes;
        ISTREAM >> in_beamline;
        ISTREAM >> in_indexes;
        ISTREAM >> in_attributes;
        placet_element_get_attribute(out_value,in_beamline,in_indexes,in_attributes);
        OSTREAM << out_value;
      }
      break;
      case PLACET_ELEMENT_GET_ATTRIBUTES: {
        std::vector<std::map<std::string,Attribute> > out_attributes;
        std::string in_beamline;
        std::vector<size_t> in_indexes;
        ISTREAM >> in_beamline;
        ISTREAM >> in_indexes;
        placet_element_get_attributes(out_attributes,in_beamline,in_indexes);
        OSTREAM << out_attributes;
      }
      break;
      case PLACET_VARY_CORRECTOR: {
        std::string in_beamline;
        std::vector<size_t> in_indexes;
        MatrixNd in_delta;
        ISTREAM >> in_beamline;
        ISTREAM >> in_indexes;
        ISTREAM >> in_delta;
        placet_vary_corrector(in_beamline,in_indexes,in_delta);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_GET_BEAM: {
        MatrixNd out_beam;
        double out_position;
        std::string in_name;
        ISTREAM >> in_name;
        placet_get_beam(out_beam,out_position,in_name);
        OSTREAM << out_beam;
        OSTREAM << out_position;
      }
      break;
      case PLACET_SET_BEAM: {
        std::string in_name;
        MatrixNd in_beam;
        ISTREAM >> in_name;
        ISTREAM >> in_beam;
        placet_set_beam(in_name,in_beam);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_SET_BEAM2: {
        MatrixNd in_beam;
        ISTREAM >> in_beam;
        placet_set_beam(in_beam);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_BEAMLINE_REFRESH: {
        std::string in_beamline;
        ISTREAM >> in_beamline;
        placet_beamline_refresh(in_beamline);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_GET_RESPONSE_MATRIX_FIT: {
        MatrixNd out_Rxx;
        MatrixNd out_Rxy;
        MatrixNd out_Ryx;
        MatrixNd out_Ryy;
        MatrixNd out_Rxxx;
        MatrixNd out_Rxxy;
        MatrixNd out_Rxyy;
        MatrixNd out_Ryxx;
        MatrixNd out_Ryyx;
        MatrixNd out_Ryyy;
        std::string in_beamline;
        std::string in_beam;
        std::string in_survey;
        std::vector<size_t> in_bpms;
        std::vector<size_t> in_correctors;
        ISTREAM >> in_beamline;
        ISTREAM >> in_beam;
        ISTREAM >> in_survey;
        ISTREAM >> in_bpms;
        ISTREAM >> in_correctors;
        placet_get_response_matrix_fit(out_Rxx,out_Rxy,out_Ryx,out_Ryy,out_Rxxx,out_Rxxy,out_Rxyy,out_Ryxx,out_Ryyx,out_Ryyy,in_beamline,in_beam,in_survey,in_bpms,in_correctors);
        OSTREAM << out_Rxx;
        OSTREAM << out_Rxy;
        OSTREAM << out_Ryx;
        OSTREAM << out_Ryy;
        OSTREAM << out_Rxxx;
        OSTREAM << out_Rxxy;
        OSTREAM << out_Rxyy;
        OSTREAM << out_Ryxx;
        OSTREAM << out_Ryyx;
        OSTREAM << out_Ryyy;
      }
      break;
      case PLACET_GET_TRANSFER_MATRIX_FIT: {
        MatrixNd out_R;
        std::vector<MatrixNd> out_T;
        std::string in_beamline;
        std::string in_beam;
        std::string in_survey;
        int in_start;
        int in_end;
        ISTREAM >> in_beamline;
        ISTREAM >> in_beam;
        ISTREAM >> in_survey;
        ISTREAM >> in_start;
        ISTREAM >> in_end;
        placet_get_transfer_matrix_fit(out_R,out_T,in_beamline,in_beam,in_survey,in_start,in_end);
        OSTREAM << out_R;
        OSTREAM << out_T;
      }
      break;
      case PLACET_GET_TRANSFER_MATRIX: {
        MatrixNd out_R;
        std::string in_beamline;
        int in_start;
        int in_end;
        ISTREAM >> in_beamline;
        ISTREAM >> in_start;
        ISTREAM >> in_end;
        placet_get_transfer_matrix(out_R,in_beamline,in_start,in_end);
        OSTREAM << out_R;
      }
      break;
      case PLACET_GET_TRANSFER_MATRIX_ELEMENTS_FIT: {
        MatrixNd out_elements;
        std::string in_beamline;
        std::string in_beam;
        std::string in_survey;
        std::vector<std::string> in_elements;
        int in_start;
        int in_end;
        ISTREAM >> in_beamline;
        ISTREAM >> in_beam;
        ISTREAM >> in_survey;
        ISTREAM >> in_elements;
        ISTREAM >> in_start;
        ISTREAM >> in_end;
        placet_get_transfer_matrix_elements_fit(out_elements,in_beamline,in_beam,in_survey,in_elements,in_start,in_end);
        OSTREAM << out_elements;
      }
      break;
      case PLACET_GIRDER_GET_LENGTH: {
        MatrixNd out_lengths;
        std::string in_beamline;
        ISTREAM >> in_beamline;
        placet_girder_get_length(out_lengths,in_beamline);
        OSTREAM << out_lengths;
      }
      break;
      case PLACET_GIRDER_MOVE: {
        std::string in_beamline;
        MatrixNd in_delta;
        ISTREAM >> in_beamline;
        ISTREAM >> in_delta;
        placet_girder_move(in_beamline,in_delta);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_TCL_GETVAR: {
        std::string out_value;
        std::string in_varname1;
        std::string in_varname2;
        ISTREAM >> in_varname1;
        ISTREAM >> in_varname2;
        placet_Tcl_GetVar(out_value,in_varname1,in_varname2);
        OSTREAM << out_value;
      }
      break;
      case PLACET_TCL_SETVAR: {
        std::string in_varname1;
        std::string in_varname2;
        Attribute in_value;
        ISTREAM >> in_varname1;
        ISTREAM >> in_varname2;
        ISTREAM >> in_value;
        placet_Tcl_SetVar(in_varname1,in_varname2,in_value);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_TCL_SETVAR2: {
        std::string in_varname1;
        Attribute in_value;
        ISTREAM >> in_varname1;
        ISTREAM >> in_value;
        placet_Tcl_SetVar(in_varname1,in_value);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_TCL_EVAL: {
        std::string in_script;
        ISTREAM >> in_script;
        placet_Tcl_Eval(in_script);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
      case PLACET_TCL_EVALFILE: {
        std::string in_filename;
        ISTREAM >> in_filename;
        placet_Tcl_EvalFile(in_filename);
        int dummy=0;
        OSTREAM << dummy;
      }
      break;
    }
    OSTREAM.flush();
  }
  return 0;
}
