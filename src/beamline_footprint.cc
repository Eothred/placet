#include <limits>
#include <cfloat>

#include "beamline.h"
#include "sbend.h"

#include "Frame.hh"

void BEAMLINE::save_footprint(std::ostream &stream, int start, int end )
{
  //int old_precision = stream.precision(DBL_DIG);
  stream <<
    "# Beamline three-dimensional footprint, in 10 columns per element\n"
    "# 1. 'x' coordinate (center) [m]\n"
    "# 2. 'y' coordinate (center) [m]\n"
    "# 3. 'z' coordinate (center) [m]\n"
    "# 4. 's' position along the beam line (taken at element exit) [m]\n"
    "# 5. element name\n"
    "# 6. element type\n"
    "# 7. element length [m]\n"
    "# 8. euler angle alpha (aka phi) [rad]\n"
    "# 9. euler angle beta (aka theta) [rad]\n"
    "# 10. euler angle gamma (aka psi) [rad]\n";
  stream << "0 0 0 0 \"[BEGIN]\" MARKER 0 0 0 0" << std::endl;
  if (end==-1)
    end=n_elements-1;
  Frame frame;
  std::valarray<double> euler(3);
  double s = 0.0;
  for (int i=start;i<=end;i++) {
    const ELEMENT &element_ref = *element[i];
    if (fabs(element_ref.get_length()) > std::numeric_limits<double>::epsilon()) {
      s += element_ref.get_length();
      Vector3d element_center = Vector3d(element_ref.get_length()/2, 0., 0.);
      euler = frame.get_axes().get_euler_angles();
      stream << frame * element_center << ' ' << s << " \"" << element_ref.get_name() << "\" " << typeid(element_ref).name() << ' ' << element_ref.get_length() << ' ' << euler[0] << ' ' << euler[1] << ' ' << euler[2] << '\n';
      const SBEND *sbend = element_ref.sbend_ptr();
      if (sbend && fabs(sbend->get_angle()) > std::numeric_limits<double>::epsilon()) {
	double half_tangent_length = element_ref.get_length() / sbend->get_angle() * tan(sbend->get_angle() / 2); 
        if (fabs(sbend->get_tilt()) > std::numeric_limits<double>::epsilon()) {
          frame *= Frame(Vector3d(half_tangent_length, 0., 0.), Rotation(Axis_X,  sbend->get_tilt()));
          frame *= Frame(Vector3d(half_tangent_length, 0., 0.), Rotation(Axis_Z, -sbend->get_angle()));
          frame *= Frame(Rotation(Axis_X, -sbend->get_tilt()));
        } else {
          frame *= Frame(Vector3d(half_tangent_length, 0., 0.));
          frame *= Frame(Vector3d(half_tangent_length, 0., 0.), Rotation(Axis_Z, -sbend->get_angle()));
        }
      } else {
        frame *= Frame(Vector3d(element_ref.get_length(), 0., 0.));
      }
    } else {
      euler = frame.get_axes().get_euler_angles();
      stream << frame.get_origin() << ' ' << s << " \"" << element_ref.get_name() << "\" " << typeid(element_ref).name() << ' ' << element_ref.get_length() << ' ' << euler[0] << ' ' << euler[1] << ' ' << euler[2] << '\n';
    }
  }
  euler = frame.get_axes().get_euler_angles();
  stream << frame.get_origin() << ' ' << s << ' ' << "\"[END]\" MARKER 0 " << euler[0] << ' ' << euler[1] << ' ' << euler[2] << '\n';
  //stream.precision(old_precision);
}
