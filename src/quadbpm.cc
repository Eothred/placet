#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beamline.h"
#include "structures_def.h"

/* Function prototypes */

#include "matrix.h"
#include "placeti3.h"
#include "quadbpm.h"

void QUADBPM::step_4d(BEAM *beam)
{
  QUADRUPOLE::step_half(beam);
  const double _length=geometry.length;
  geometry.length=0.0;
  BPM::step_4d(beam);
  geometry.length=_length;
  QUADRUPOLE::step_half(beam);
}

void QUADBPM::step_4d_0(BEAM *beam)
{
  QUADRUPOLE::step_half(beam);
  const double _length=geometry.length;
  geometry.length=0.0;
  BPM::step_4d_0(beam);
  geometry.length=_length;
  QUADRUPOLE::step_half(beam);
}

void QUADBPM::step_4d_sr_0(BEAM *beam)
{
  QUADRUPOLE::step_half(beam);
  const double _length=geometry.length;
  geometry.length=0.0;
  BPM::step_4d_0(beam);
  geometry.length=_length;
  QUADRUPOLE::step_half(beam);
}

void QUADBPM::step_6d_0(BEAM *beam)
{
  QUADRUPOLE::step_half(beam);
  const double _length=geometry.length;
  geometry.length=0.0;
  BPM::step_6d_0(beam);
  geometry.length=_length;
  QUADRUPOLE::step_half(beam);
}

int tk_BpmSetToOffset(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error,n;
  double x,y;
  Tk_ArgvInfo table[]={
    {(char*)"-x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&x,
     (char*)"horizontal shift in micro-metres"},
    {(char*)"-y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&y,
     (char*)"horizontal shift in micro-metres"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if (argc<2){
    Tcl_SetResult(interp,"Not enough arguments to <BpmSetToOffset>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if ((error=Tcl_GetInt(interp,argv[1],&n))) return error;
  if (!inter_data.beamline->element[n]->is_quadbpm()) {
    char buf[50];
    snprintf(buf,50,"Element number %d is not a QuadBpm",n);
    Tcl_SetResult(interp,buf,TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (QUADBPM *qbpm=inter_data.beamline->element[n]->quadbpm_ptr()) {
#ifdef TWODIM
    x=qbpm->bpm_offset.x;
#endif
    y=qbpm->bpm_offset.y;
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
      return error;
    }
    if (argc!=2){
      Tcl_SetResult(interp,"Too many arguments to <BpmSetToOffset>",
	  	  TCL_VOLATILE);
      return TCL_ERROR;
    }
#ifdef TWODIM
    qbpm->bpm_offset.x=x;
#endif
    qbpm->bpm_offset.y=y;
  }
  return TCL_OK;
}
