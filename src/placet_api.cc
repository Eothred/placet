#include <algorithm>
#include <fnmatch.h>
#include <cfloat>

#include "placet_api.hh"
#include "placet_tk.h"
#include "quadrupole.h"
#include "multipole.h"
#include "rmatrix.h"
#include "girder.h"
#include "bpm.h"

#include "placeti3.h"
#include "function.h"
#include "placet_tk.h"
#include "track.h"

#include <gsl/gsl_multifit.h>

void placet_test_no_correction(MatrixNd          &out_emitt,
			       MatrixNd          &out_beam,
			       const std::string &in_beamline,
			       const std::string &in_beam,
			       const std::string &in_survey,
			       int                in_nmachines,
			       int                in_start,
			       int                in_end,
			       const std::string &in_format )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  BEAM *bunch0 = get_beam(in_beam.c_str());
  if (!bunch0) bunch0 = inter_data.bunch;
  void (*survey)(BEAMLINE*) = (void (*)(BEAMLINE *))survey_find(in_survey.c_str());
  if (!survey) {
    placet_cout << ERROR << "placet_test_no_correction: survey '" << in_survey.c_str() << "' not found" << endmsg;
    exit(1);
  }
  if (in_end == -1)
    in_end = beamline->n_elements;
  else
    in_end++;
  BEAM *tb = bunch_remake(bunch0);
  emitt_store_init(beamline->n_quad);
  for (int i=0;i<in_nmachines;i++) {
    survey(beamline);
    beam_copy(bunch0,tb);
    bunch_track_line_emitt_new(beamline,tb,in_start,in_end);
  }
  for (int i=0;i<emitt_data.data.size();i++) {
    const std::vector<double> &data = emitt_data.data[i].get_values_as_vector(in_format.c_str());
    if (i==0)
      out_emitt.resize(emitt_data.data.size(), data.size());
    for (int j=0; j<data.size(); j++) {
      out_emitt[i][j] = data[j];
    }
  }
  emitt_store_delete();
  size_t count=0;
  for (int i=0;i<tb->slices;i++) {
    const PARTICLE &particle=tb->particle[i];
    if (particle.wgt != 0.0 && fabs(particle.energy) > std::numeric_limits<double>::epsilon())
      count++;
  }
  if (tb->particle_beam) {
    out_beam.resize(count,6);
    for (int i=0,j=0;i<tb->slices;i++) {
      const PARTICLE &particle=tb->particle[i];
      if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	out_beam[j][0]=particle.energy;
	out_beam[j][1]=particle.x;
	out_beam[j][2]=particle.y;
	out_beam[j][3]=particle.z;
	out_beam[j][4]=particle.xp;
	out_beam[j][5]=particle.yp;
	j++;
      }
    }
  } else {
    out_beam.resize(count,17);
    int n=tb->slices/tb->bunches/tb->macroparticles;
    int nm=tb->macroparticles;
    int nb=tb->bunches;
    for (int j=0,l=0,m=0;j<nb;j++){
      for (int i=0;i<n;i++){
	for (int k=0;k<nm;k++){
	  const PARTICLE &particle=tb->particle[m];
	  if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	    out_beam[l][0]=tb->z_position[j*n+i];
	    out_beam[l][1]=particle.wgt;
	    out_beam[l][2]=particle.energy;
	    out_beam[l][3]=particle.x;
	    out_beam[l][4]=particle.xp;
	    out_beam[l][5]=particle.y;
	    out_beam[l][6]=particle.yp;
	    out_beam[l][7]=tb->sigma_xx[m].r11;
	    out_beam[l][8]=tb->sigma_xx[m].r12;
	    out_beam[l][9]=tb->sigma_xx[m].r22;
	    out_beam[l][10]=tb->sigma[m].r11;
	    out_beam[l][11]=tb->sigma[m].r12;
	    out_beam[l][12]=tb->sigma[m].r22;
	    out_beam[l][13]=tb->sigma_xy[m].r11;
	    out_beam[l][14]=tb->sigma_xy[m].r12;
	    out_beam[l][15]=tb->sigma_xy[m].r21;
	    out_beam[l][16]=tb->sigma_xy[m].r22;
	    l++;
	  }
	  m++;
	}
      }
    }
  }
  beam_delete(tb);
}

void placet_test_no_correction(MatrixNd          &out_emitt,
			       MatrixNd          &out_beam,
			       const std::string &in_beamline,
			       const std::string &in_beam,
			       const std::string &in_survey,
			       const std::string &in_format )
{
  placet_test_no_correction(out_emitt, out_beam, in_beamline, in_beam, in_survey, 1, 0, -1, in_format);
}

void placet_get_bpm_readings(MatrixNd                  &out_readings,
			     MatrixNd                  &out_positions,
			     std::vector<size_t>       &out_indexes,
			     MatrixNd                  &out_transmission,
			     const std::string         &in_beamline,
			     const std::vector<size_t> &in_bpms,
			     bool                       in_exact )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  ELEMENT **element=beamline->element;
  std::vector<size_t> bpms = in_bpms;
  if (bpms.size()!=0) {
    for (size_t i=0; i<bpms.size(); i++) {
      if (!(element[bpms[i]]->is_bpm() || element[bpms[i]]->is_quadbpm())) {
        placet_printf(ERROR,"placet_get_bpm_readings: index is not a bpm\n");
        return;
      }
    }
  } else {
    for (int i=0; i<beamline->n_elements; i++) {
      if (element[i]->is_bpm() || element[i]->is_quadbpm()) {
	bpms.push_back(i);
      }
    }
  }
  out_readings.resize(bpms.size(), 2);
  out_positions.resize(1, bpms.size());
  out_transmission.resize(1, bpms.size());
  out_indexes = bpms;
  double s = 0.0;
  for (size_t i=0, j=0; j<bpms.size(); i++, j++) {
    while(i<bpms[j])
      s+=element[i++]->get_length();
    s+=0.5*element[i]->get_length();
    out_positions[0][j] = s;
    if (in_exact) {
      out_readings[j][0] = element[i]->bpm_ptr()->get_x_reading_exact();
      out_readings[j][1] = element[i]->bpm_ptr()->get_y_reading_exact();
    } else {
      out_readings[j][0] = element[i]->bpm_ptr()->get_x_reading();
      out_readings[j][1] = element[i]->bpm_ptr()->get_y_reading();
    }
    out_transmission[0][j] = element[i]->bpm_ptr()->get_transmitted_charge();
    s+=0.5*element[i]->get_length();
  }
}

void placet_get_number_list(std::vector<size_t>            &out_indexes,
			    const std::string              &in_beamline,
			    const std::vector<std::string> &in_types )
{
  out_indexes.clear();
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  for (int i=0; i<beamline->n_elements; i++) {
    ELEMENT *element = beamline->element[i];
    for (size_t j=0; j<in_types.size(); j++) {
      const std::string &type = in_types[j];
      if ((type == "quadrupole" && element->is_quad()) ||
	  (type == "cavity_pets" && element->is_cavity_pets()) ||
	  (type == "multipole" && element->is_multipole()) ||
	  (type == "decapole" && element->is_multipole() && element->multipole_ptr()->get_field() == 5) ||
	  (type == "octupole" && element->is_multipole() && element->multipole_ptr()->get_field() == 4) ||
	  (type == "sextupole" && element->is_multipole() && element->multipole_ptr()->get_field() == 3) ||
	  (type == "dipole" && element->is_dipole()) ||
	  (type == "cavity" && element->is_cavity()) ||
	  (type == "drift" && element->is_drift()) ||
	  (type == "sbend" && element->is_sbend()) ||
	  (type == "quadbpm" && element->is_quadbpm()) ||
	  (type == "bpm" && element->is_bpm()) ||
	  (type == "cavitybpm" && element->is_cavbpm()) ||
	  (type == "magnet" && (element->is_quad() || element->is_quadbpm() || element->is_multipole() || element->is_sbend()))) {
	out_indexes.push_back(i);
	break;
      }
    }
  }
}

void placet_get_name_number_list(std::vector<size_t>  &out_indexes,
				 const std::string    &in_beamline,
				 const std::string    &in_pattern )
{
  out_indexes.clear();
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  ELEMENT **element=beamline->element;
  for (int i=0; i<beamline->n_elements; i++) {
    if (fnmatch(in_pattern.c_str(),element[i]->get_name(),0)==0) {
      out_indexes.push_back(i);
    }
  }
}

void placet_element_set_attribute(const std::string              &in_beamline,
				  const std::vector<size_t>      &in_indexes,
				  const std::vector<std::string> &in_attributes,
				  const std::vector<Attribute>   &in_value )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  if (in_value.size() == 1 || in_value.size() == in_indexes.size()) {
    if (in_attributes.size() == 1 || in_attributes.size() == in_indexes.size()) {
      for (size_t i=0, j=0, k=0; i<in_indexes.size(); i++) {
	ELEMENT *element=beamline->element[in_indexes[i]];
	option_set &attributes_table = element->get_attributes_table();
	if (option_info *attribute = attributes_table.find(in_attributes[k].c_str())) {
	  if (attribute->type() != in_value[j].type()) {
	    if (attribute->type()==OPT_COMPLEX && in_value[j].type() == OPT_DOUBLE) {
	      attribute->set(std::complex<double>(in_value[j].get_double(),0.0));
	    } else if (attribute->type()==OPT_INT && in_value[j].type()==OPT_DOUBLE && trunc(in_value[j].get_double()) == in_value[j].get_double()) {
	      attribute->set(int(in_value[j].get_double()));
	    } else {
	      placet_cout << ERROR 
			  << "placet_element_set_attribute: type mismatch for attribute '" << in_attributes[k] << "' (type '" 
			  << attribute->get_type_name() << "' wanted, type '" 
			  << in_value[j].get_type_name() << "' received)" << endmsg;
	      break;    
	    }
	  } else {
	    switch(in_value[j].type()) {
	    case OPT_INT:      attribute->set(in_value[j].get_int()); break;
	    case OPT_BOOL:     attribute->set(in_value[j].get_bool()); break;
	    case OPT_UINT:     attribute->set(in_value[j].get_uint()); break;
	    case OPT_DOUBLE:   attribute->set(in_value[j].get_double()); break;
	    case OPT_MATRIX:   attribute->set(in_value[j].get_matrix()); break;
	    case OPT_STRING:
	    case OPT_CHAR_PTR: attribute->set(in_value[j].get_string()); break;
	    case OPT_COMPLEX:  attribute->set(in_value[j].get_complex()); break;
	    case OPT_NONE:     break;
	    }
	  }
	} else {
	  placet_cout << ERROR << "placet_element_set_attribute: attribute '" << in_attributes[k] << "' does not exist" << endmsg;
	  break;
	}
	if (in_value.size()>1)
	  j++;
	if (in_attributes.size()>1)
	  k++;
      }
    } else {
      placet_cout << ERROR << "placet_element_set_attribute: the list of attributes must have one element, or as many elements as indexes" << endmsg;
    }
  } else {
    placet_cout << ERROR << "placet_element_set_attribute: vector of input values must have one element, or as many elements as indexes" << endmsg;
  }
}

void placet_element_get_attribute(std::vector<Attribute>         &out_value,
				  const std::string              &in_beamline,
				  const std::vector<size_t>      &in_indexes,
				  const std::vector<std::string> &in_attributes )
{
  out_value.clear();
  if (in_attributes.size() == 1 || in_attributes.size() == in_indexes.size()) {
    BEAMLINE *beamline = get_beamline(in_beamline.c_str());
    if (!beamline) beamline = inter_data.beamline;
    for (size_t i=0, j=0; i<in_indexes.size(); i++) {
      ELEMENT *element=beamline->element[in_indexes[i]];
      option_set &attributes_table = element->get_attributes_table();
      Attribute value;
      if (in_attributes[j] == "type_name") {
	if      (element->is_quad()) value="quadrupole";
	else if (element->is_multipole()) value="multipole";
	else if (element->is_quadbpm()) value="quad_bpm";
	else if (element->is_dipole()) value="dipole";
	else if (element->is_cavity()) value="cavity";
	else if (element->is_cavity_pets()) value="cavity_pets";
	else if (element->is_sbend()) value="sbend";
	else if (element->is_drift()) value="drift";
	else if (element->is_cavbpm()) value="cavitybpm";
	else if (element->is_bpm()) value="bpm";
	else value=typeid(*element).name();
      } else if (option_info *attribute = attributes_table.find(in_attributes[j].c_str())) {
	switch(attribute->type()) {
	case OPT_INT:      value = attribute->get_int(); break;
	case OPT_BOOL:     value = attribute->get_bool(); break;
	case OPT_UINT:     value = attribute->get_uint(); break;
	case OPT_DOUBLE:   value = attribute->get_double(); break;
	case OPT_MATRIX:   value = attribute->get_matrix(); break;
	case OPT_STRING:   value = attribute->get_string(); break;
	case OPT_CHAR_PTR: value = attribute->get_char_ptr(); break;
	case OPT_COMPLEX:  value = attribute->get_complex(); break;
	case OPT_NONE:     break;
	}
      } else {
	placet_cout << ERROR << "placet_element_get_attribute: attribute '" << in_attributes[j] << "' does not exist" << endmsg;
	break;
      }
      if (in_attributes.size()>1)
	j++;
      out_value.push_back(value);
    }
  } else {
    placet_cout << ERROR << "placet_element_get_attribute: the list of attributes must have one element, or as many elements as indexes" << endmsg;
  }
}

void placet_element_get_attributes(std::vector<std::map<std::string,Attribute> > &out_attributes,
				   const std::string         &in_beamline,
				   const std::vector<size_t> &in_indexes )
{
  out_attributes.clear();
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  std::vector<size_t> indexes = in_indexes;
  if (indexes.size()==0) {
    indexes.resize(beamline->n_elements);
    for (size_t i=0; i<beamline->n_elements; i++)
      indexes[i] = i;
  }
  for (size_t i=0; i<indexes.size(); i++) {
    ELEMENT *element=beamline->element[indexes[i]];
    std::map<std::string,Attribute> table;
    const char *type;
    if      (element->is_quad()) type="quadrupole";
    else if (element->is_multipole()) type="multipole";
    else if (element->is_quadbpm()) type="quad_bpm";
    else if (element->is_dipole()) type="dipole";
    else if (element->is_cavity()) type="cavity";
    else if (element->is_cavity_pets()) type="cavity_pets";
    else if (element->is_sbend()) type="sbend";
    else if (element->is_drift()) type="drift";
    else if (element->is_cavbpm()) type="cavitybpm";
    else if (element->is_bpm()) type="bpm";
    else type=typeid(*element).name();
    table[std::string("type_name")] = Attribute(std::string(type));
    const option_set &attributes = element->get_attributes_table();
    for (size_t j=0; j<attributes.size(); j++) {
      const option_info &attribute = attributes[j];
      option_type attribute_type = attribute.type();
      Attribute value;
      switch(attribute_type) {
      case OPT_INT:      value = attribute.get_int(); break;
      case OPT_BOOL:     value = attribute.get_bool(); break;
      case OPT_UINT:     value = attribute.get_uint(); break;
      case OPT_DOUBLE:   value = attribute.get_double(); break;
      case OPT_MATRIX:   value = attribute.get_matrix(); break;
      case OPT_STRING:   value = attribute.get_string(); break;
      case OPT_CHAR_PTR: value = attribute.get_char_ptr(); break;
      case OPT_COMPLEX:  value = attribute.get_complex(); break;
      case OPT_NONE:     break;
      }
      table[std::string(attribute.get_name())] = value;
    }
    out_attributes.push_back(table);
  }
}

void placet_vary_corrector(const std::string         &in_beamline,
			   const std::vector<size_t> &in_indexes,
			   const MatrixNd            &in_delta )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  if (in_indexes.size() == in_delta.rows() && in_delta.columns() == 2) {
    for (size_t i=0; i<in_indexes.size(); i++) {
      ELEMENT *element = beamline->element[in_indexes[i]];
      double dx = in_delta[i][0];
      double dy = in_delta[i][1];
      element->correct_x(dx);
      element->correct_y(dy);
    }
  } else {
    placet_cout << ERROR << "placet_vary_corrector: the matrix of input deltas must have as many rows as the number of indexes, and two columns" << endmsg;
  }
}

void placet_get_beam(MatrixNd          &out_beam,
		     double            &out_position,
		     const std::string &in_name )
{
  BEAM *beam = NULL;
  if (in_name.size()>0) {
    beam = get_beam(in_name.c_str());
  }
  if (!beam) beam = inter_data.bunch;
  size_t count = 0;
  for (int i=0;i<beam->slices;i++) {
    const PARTICLE &particle = beam->particle[i];
    if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon())
      count++;
  }
  out_position = beam->s;
  if (beam->particle_beam) {
    out_beam.resize(count,6);
    for (int i=0,j=0;i<beam->slices;i++) {
      const PARTICLE &particle=beam->particle[i];
      if (particle.wgt != 0.0 && fabs(particle.energy) > std::numeric_limits<double>::epsilon()) {
        out_beam[j][0]=particle.energy;
        out_beam[j][1]=particle.x;
        out_beam[j][2]=particle.y;
        out_beam[j][3]=particle.z;
        out_beam[j][4]=particle.xp;
        out_beam[j][5]=particle.yp;
        j++;
      }
    }
  } else {
    out_beam.resize(count,17);
    int n=beam->slices/beam->bunches/beam->macroparticles;
    int nm=beam->macroparticles;
    int nb=beam->bunches;
    for (int j=0,l=0,m=0;j<nb;j++){
      for (int i=0;i<n;i++){
        for (int k=0;k<nm;k++){
          const PARTICLE &particle=beam->particle[m];
          if (particle.wgt!=0.0&&fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
            out_beam[l][0]=beam->z_position[j*n+i];
	    out_beam[l][1]=particle.wgt;
	    out_beam[l][2]=particle.energy;
	    out_beam[l][3]=particle.x;
	    out_beam[l][4]=particle.xp;
	    out_beam[l][5]=particle.y;
	    out_beam[l][6]=particle.yp;
	    out_beam[l][7]=beam->sigma_xx[m].r11;
	    out_beam[l][8]=beam->sigma_xx[m].r12;
	    out_beam[l][9]=beam->sigma_xx[m].r22;
	    out_beam[l][10]=beam->sigma[m].r11;
	    out_beam[l][11]=beam->sigma[m].r12;
	    out_beam[l][12]=beam->sigma[m].r22;
	    out_beam[l][13]=beam->sigma_xy[m].r11;
	    out_beam[l][14]=beam->sigma_xy[m].r12;
	    out_beam[l][15]=beam->sigma_xy[m].r21;
	    out_beam[l][16]=beam->sigma_xy[m].r22;
            l++;
          }
          m++;
        }
      }
    }
  }
}

void placet_set_beam(const std::string &in_name,
		     const MatrixNd    &in_beam )
{
  BEAM *beam = NULL;
  if (in_name.size()>0)
    beam = get_beam(in_name.c_str());
  if (!beam) beam = inter_data.bunch;
  if (in_beam.columns()==6) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.energy=in_beam[i][0];
      particle.x=in_beam[i][1];
      particle.y=in_beam[i][2];
      particle.z=in_beam[i][3];
      particle.xp=in_beam[i][4];
      particle.yp=in_beam[i][5];
    }
  } else {
    int n=beam->slices/beam->bunches/beam->macroparticles;
    int nm=beam->macroparticles;
    int nb=beam->bunches;
    for (int j=0,m=0;j<nb;j++){
      for (int i=0;i<n;i++){
        for (int k=0;k<nm;k++){
          PARTICLE &particle=beam->particle[m];
          beam->z_position[j*n+i]=in_beam[m][0];
	  particle.wgt=in_beam[m][1];
	  particle.energy=in_beam[m][2];
	  particle.x=in_beam[m][3];
	  particle.xp=in_beam[m][4];
	  particle.y=in_beam[m][5];
	  particle.yp=in_beam[m][6];
	  beam->sigma_xx[m].r11=in_beam[m][7];
	  beam->sigma_xx[m].r12=in_beam[m][8];
	  beam->sigma_xx[m].r22=in_beam[m][9];
	  beam->sigma[m].r11=in_beam[m][10];
	  beam->sigma[m].r12=in_beam[m][11];
	  beam->sigma[m].r22=in_beam[m][12];
	  beam->sigma_xy[m].r11=in_beam[m][13];
	  beam->sigma_xy[m].r12=in_beam[m][14];
	  beam->sigma_xy[m].r21=in_beam[m][15];
	  beam->sigma_xy[m].r22=in_beam[m][16];
          m++;
        }
      }
    }
  }
}

void placet_set_beam(const MatrixNd &in_beam )
{
  placet_set_beam(std::string(), in_beam);
}

void placet_beamline_refresh(const std::string &in_beamline )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  beamline->unset();
  // set beamline, output suppressed
  beamline->set(NULL);
}

void placet_get_response_matrix_fit(MatrixNd                  &out_Rxx,
				    MatrixNd                  &out_Rxy,
				    MatrixNd                  &out_Ryx,
				    MatrixNd                  &out_Ryy,
				    MatrixNd                  &out_Rxxx,
				    MatrixNd                  &out_Rxxy,
				    MatrixNd                  &out_Rxyy,
				    MatrixNd                  &out_Ryxx,
				    MatrixNd                  &out_Ryyx, 
				    MatrixNd                  &out_Ryyy,
				    const std::string         &in_beamline,
				    const std::string         &in_beam,
				    const std::string         &in_survey,
				    const std::vector<size_t> &in_bpms,
				    const std::vector<size_t> &in_correctors )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  BEAM *bunch = get_beam(in_beam.c_str());
  if (!bunch) bunch = inter_data.bunch;
  void (*survey)(BEAMLINE*) = (void (*)(BEAMLINE *))survey_find(in_survey.c_str());
  if (!survey) {
    placet_cout << ERROR << "placet_get_response_matrix_fit: survey '" << in_survey.c_str() << "' not found" << endmsg;
    exit(1);
  }
  BEAM *tb = bunch_remake(bunch);
  const size_t nbpms = in_bpms.size();
  const size_t ncorrs = in_correctors.size();
  out_Rxx.resize(nbpms, ncorrs);
  out_Rxy.resize(nbpms, ncorrs);
  out_Ryx.resize(nbpms, ncorrs);
  out_Ryy.resize(nbpms, ncorrs);
  out_Rxxx.resize(nbpms, ncorrs);
  out_Rxxy.resize(nbpms, ncorrs);
  out_Rxyy.resize(nbpms, ncorrs);
  out_Ryxx.resize(nbpms, ncorrs);
  out_Ryyx.resize(nbpms, ncorrs);
  out_Ryyy.resize(nbpms, ncorrs);
  size_t nparticles = tb->slices;
  // polynomial fit using GSL's gsl_multifit_linear
  // 1 fit for the horizontal bpm_readings observables
  // 1 fit for the vertical bpm_readings observables
  // gsl_multifit_linear requires X_ij matrix of predictor variables
  // in our case:
  // X_ij:
  //   i: index 0-5 in the phase space (x x' y y' z d)
  //   j: index 0-26 
  //      0-5 : Rij, n == _j
  //      6-26 : Rijk, n = 6 + ( j*(j-1)/2 + k-1 )         with k<j 
  gsl_vector *x[nbpms];
  gsl_vector *y[nbpms];
  for(size_t i=0; i<nbpms; i++) {
    x[i] = gsl_vector_alloc(nparticles);
    y[i] = gsl_vector_alloc(nparticles);
  }
  gsl_matrix *P[ncorrs];
  for(size_t i=0; i<ncorrs; i++) {
    P[i] = gsl_matrix_alloc(nparticles, 2);
  }
  survey(beamline);
  beam_copy(bunch, tb);
  inter_data.bunch=tb;
  ELEMENT **element = beamline->element;
  enum INDEX {
    XP,
    YP,
    XPXP,
    XPYP,
    YPYP,
    INDEX_COUNT
  };
  for (size_t start=0, ibpm=0, icorr=0; start<=in_bpms[nbpms-1] && start<(size_t)beamline->n_elements; start++) {
    // pre-tracking reading
    if (start==in_bpms[ibpm]) {
      for (size_t i=0; i<nparticles; i++) {
	const PARTICLE &particle = tb->particle[i];
	gsl_vector_set(x[ibpm], i, particle.x);
	gsl_vector_set(y[ibpm], i, particle.y);
      }
    }
    if (icorr<ncorrs && start==in_correctors[icorr]) {
      for (size_t i=0; i<nparticles; i++) {
	const PARTICLE &particle = tb->particle[i];
	gsl_matrix_set(P[icorr], i, XP, particle.xp * particle.energy); // 1 kV kick at each corrector, expressed in urad
	gsl_matrix_set(P[icorr], i, YP, particle.yp * particle.energy); // 1 kV kick at each corrector, expressed in urad
      }
    }
    // tracking
    element[start]->track_0(tb);
    // post-tracking reading
    if (start==in_bpms[ibpm]) {
      for (size_t i=0; i<nparticles; i++) {
	const PARTICLE &particle = tb->particle[i];
	gsl_vector_set(x[ibpm], i, gsl_vector_get(x[ibpm], i) + particle.x);
	gsl_vector_set(y[ibpm], i, gsl_vector_get(y[ibpm], i) + particle.y);
      }
      ibpm++;
    }
    if (icorr<ncorrs && start==in_correctors[icorr]) {
      for (size_t i=0; i<nparticles; i++) {
	const PARTICLE &particle = tb->particle[i];
	gsl_matrix_set(P[icorr], i, XP, gsl_matrix_get(P[icorr], i, XP) + particle.xp * particle.energy); // 1 kV kick at each corrector, expressed in urad
	gsl_matrix_set(P[icorr], i, YP, gsl_matrix_get(P[icorr], i, YP) + particle.yp * particle.energy); // 1 kV kick at each corrector, expressed in urad
      }
      icorr++;
    }
  }
  beam_delete(tb);
  // it takes the average of the two "readings", before and after bpms and correctors
  // commented out, as it does not change the result of the fit
  // for(int i=0; i<nbpms; i++) {
  //   gsl_vector_scale(x[i], 0.5);
  //   gsl_vector_scale(y[i], 0.5);
  // }
  // for(int i=0; i<ncorrs; i++) {
  //   gsl_matrix_scale(P[i], 0.5);
  // }
  // it performs the fits
  if (gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc(nparticles, INDEX_COUNT)) {
    if (gsl_matrix *X = gsl_matrix_alloc(nparticles, INDEX_COUNT)) {
      if (gsl_matrix *cov = gsl_matrix_alloc(INDEX_COUNT, INDEX_COUNT)) {
	if (gsl_vector *c = gsl_vector_alloc(INDEX_COUNT)) {
	  for (size_t j=0; j<ncorrs; j++) {
	    // it prepares the model 'X'
	    for (size_t n=0; n<nparticles; n++) {
	      gsl_vector_const_view particle = gsl_matrix_const_row(P[j], n);
	      const gsl_vector *p = &particle.vector;
	      gsl_matrix_set(X, n, XP, gsl_vector_get(p, XP));
	      gsl_matrix_set(X, n, YP, gsl_vector_get(p, YP));
	      gsl_matrix_set(X, n, XPXP, gsl_vector_get(p, XP) * gsl_vector_get(p, XP));
	      gsl_matrix_set(X, n, XPYP, gsl_vector_get(p, XP) * gsl_vector_get(p, YP));
	      gsl_matrix_set(X, n, YPYP, gsl_vector_get(p, YP) * gsl_vector_get(p, YP));
	    }
	    // double Ej = element[in_correctors[j]]->get_ref_energy();
	    for (size_t i=0; i<nbpms; i++) {
	      if (in_correctors[j]<=in_bpms[i]) {
		double chisq;
		gsl_multifit_linear(X, x[i], c, cov, &chisq, work);
		double R12 = gsl_vector_get(c, XP);
		double R14 = gsl_vector_get(c, YP);
		double R122 = 0.5 * gsl_vector_get(c, XPXP);
		double R124 = 0.5 * gsl_vector_get(c, XPYP);
		double R144 = 0.5 * gsl_vector_get(c, YPYP);
		gsl_multifit_linear(X, y[i], c, cov, &chisq, work);
		double R32 = gsl_vector_get(c, XP);
		double R34 = gsl_vector_get(c, YP);
		double R322 = 0.5 * gsl_vector_get(c, XPXP);
		double R342 = 0.5 * gsl_vector_get(c, XPYP);
		double R344 = 0.5 * gsl_vector_get(c, YPYP);
		out_Rxx[i][j] = R12; // / Ej;
		out_Rxy[i][j] = R14; // / Ej;
		out_Ryx[i][j] = R32; // / Ej;
		out_Ryy[i][j] = R34; // / Ej;
		out_Rxxx[i][j] = R122; // / Ej / Ej;
		out_Rxxy[i][j] = R124; // / Ej / Ej;
		out_Rxyy[i][j] = R144; // / Ej / Ej;
		out_Ryxx[i][j] = R322; // / Ej / Ej;
		out_Ryyx[i][j] = R342; // / Ej / Ej;
		out_Ryyy[i][j] = R344; // / Ej / Ej;
	      } else {
		out_Rxx[i][j] = 0.;
		out_Rxy[i][j] = 0.;
		out_Ryx[i][j] = 0.;
		out_Ryy[i][j] = 0.;
		out_Rxxx[i][j] = 0.;
		out_Rxxy[i][j] = 0.;
		out_Rxyy[i][j] = 0.;
		out_Ryxx[i][j] = 0.;
		out_Ryyx[i][j] = 0.;
		out_Ryyy[i][j] = 0.;
	      }
	    }
	    gsl_matrix_free(P[j]);
	    P[j] = NULL;
	  }
	  gsl_vector_free(c);
	}
	gsl_matrix_free(cov);
      }
      gsl_matrix_free(X);
    }
    gsl_multifit_linear_free(work);
  }
  for(size_t i=0; i<nbpms; i++) {
    if (x[i]) gsl_vector_free(x[i]);
    if (y[i]) gsl_vector_free(y[i]);
  }
  for(size_t i=0; i<ncorrs; i++) {
    if (P[i]) gsl_matrix_free(P[i]);
  }
}

void placet_get_transfer_matrix_fit(MatrixNd               &out_R,
				    std::vector<MatrixNd>  &out_T,
				    const std::string      &in_beamline,
				    const std::string      &in_beam,
				    const std::string      &in_survey,
				    int                     in_start,
				    int                     in_end )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  BEAM *bunch = get_beam(in_beam.c_str());
  if (!bunch) bunch = inter_data.bunch;
  void (*survey)(BEAMLINE*) = (void (*)(BEAMLINE *))survey_find(in_survey.c_str());
  if (!survey) {
    placet_cout << ERROR << "placet_get_transfer_matrix_fit: survey '" << in_survey.c_str() << "' not found" << endmsg;
    exit(1);
  }
  if (in_end == -1 || in_end >= beamline->n_elements)
    in_end = beamline->n_elements-1;
  if (in_start > in_end) {
    placet_cout << ERROR << "placet_get_transfer_matrix_fit: parameter 'start' (arg 4) must be smaller or equal to paramter 'end' (arg 5)" << endmsg;
    exit(1);
  }
  BEAM *tb = bunch_remake(bunch);
  int nparticles = tb->slices;
  beam_copy(bunch, tb);
  inter_data.bunch = tb;
  survey(beamline);
  if (gsl_matrix *P = gsl_matrix_alloc(nparticles, 6)) {
    gsl_vector *x[6];
    for(int i=0; i<6; i++) {
      x[i] = gsl_vector_alloc(nparticles);
    }
    {
      double e0 = beamline->element[in_start]->get_ref_energy();
      for (int i=0; i<nparticles; i++) {
	const PARTICLE &particle = tb->particle[i];
	gsl_matrix_set(P, i, 0, particle.x * 1e-6);
	gsl_matrix_set(P, i, 1, particle.xp * 1e-6);
	gsl_matrix_set(P, i, 2, particle.y * 1e-6);
	gsl_matrix_set(P, i, 3, particle.yp * 1e-6);
	gsl_matrix_set(P, i, 4, particle.z * 1e-6);
	gsl_matrix_set(P, i, 5, (particle.energy - e0) / e0);
      }
      for (int start=in_start; start<=in_end; start++) {
	beamline->element[start]->track_0(tb);
      }
      e0 = beamline->element[in_end]->get_ref_energy();
      for (int i=0; i<nparticles; i++) {
	const PARTICLE &particle = tb->particle[i];
	gsl_vector_set(x[0], i, particle.x * 1e-6);
	gsl_vector_set(x[1], i, particle.xp * 1e-6);
	gsl_vector_set(x[2], i, particle.y * 1e-6);
	gsl_vector_set(x[3], i, particle.yp * 1e-6);
	gsl_vector_set(x[4], i, particle.z * 1e-6);
	gsl_vector_set(x[5], i, (particle.energy - e0) / e0);
      }
    }
    // polynomial fit using GSL's gsl_multifit_linear
    // 1 fit for the horizontal bpm_readings observables
    // 1 fit for the vertical bpm_readings observables
    // gsl_multifit_linear requires X_ij matrix of predictor variables
    // in our case:
    // X_ij:
    //   i: index 0-5 in the phase space (x x' y y' z d)
    //   j: index 0-26 
    //      0-5 : Rij, n == _j
    //      6-26 : Rijk, n = 6 + ( j*(j-1)/2 + k-1 ) with k<j 
    if (gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc(nparticles, 27)) {
      if (gsl_matrix *X = gsl_matrix_alloc(nparticles, 27)) {
	if (gsl_matrix *cov = gsl_matrix_alloc(27, 27)) {
	  if (gsl_vector *c = gsl_vector_alloc(27)) {
#ifdef INDEX
#undef INDEX
#endif
#define INDEX(J,K)  (((K)<=(J))?((J)*((J)+1)/2+(K)):((K)*((K)+1)/2+(J)))
	    for (int n=0; n<nparticles; n++) {
	      gsl_vector_const_view particle = gsl_matrix_const_row(P, n);
	      const gsl_vector *p = &particle.vector;
	      for (int j=0; j<6; j++) {
		gsl_matrix_set(X, n, j, gsl_vector_get(p, j));
		for (int k=0; k<=j; k++) {
		  gsl_matrix_set(X, n, 6+INDEX(j,k), gsl_vector_get(p, j) * gsl_vector_get(p, k));
		}
	      }
	    }
	    out_R.resize(6,6);
	    out_T.resize(6);
	    for (int i=0; i<6; i++) {
	      out_T[i].resize(6,6);
	      double chisq;
	      gsl_multifit_linear(X, x[i], c, cov, &chisq, work);
	      for (int j=0; j<6; j++) {
		out_R[i][j] = gsl_vector_get(c, j);
		out_T[i][j][j] = gsl_vector_get(c, 6+INDEX(j,j));
		for (int k=0; k<j; k++) {
		  out_T[i][j][k] = out_T[i][k][j] = gsl_vector_get(c, 6+INDEX(j,k));
		}
	      }
	    }
	    gsl_vector_free(c);
	  }
	  gsl_matrix_free(cov);
	}
	gsl_matrix_free(X);
      }
      gsl_multifit_linear_free(work);
    }
    for(int i=0; i<6; i++) {
      if (x[i]) gsl_vector_free(x[i]);
    }
    gsl_matrix_free(P);
  }
  beam_delete(tb);
}

void placet_get_transfer_matrix(MatrixNd               &out_R,
				const std::string      &in_beamline,
				int                     in_start,
				int                     in_end )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  if (in_end == -1 || in_end >= beamline->n_elements)
    in_end = beamline->n_elements-1;
  if (in_start > in_end) {
    placet_cout << ERROR << "placet_get_transfer_matrix: parameter 'start' (arg 2) must be smaller or equal to paramter 'end' (arg 3)" << endmsg;
    exit(1);
  }
  Matrix<6,6> R = Identity<6,6>();
  for (int i=in_start;i<=in_end;i++) {
    ELEMENT *element=beamline->element[i];
    R = element->get_transfer_matrix_6d()*R;
  }
  out_R.resize(6,6);
  for (int i=0;i<6;i++) {
    for (int j=0;j<6;j++) {
      out_R[i][j] = R[i][j];
    }
  }
}

static std::vector<std::pair<std::string, size_t> > get_indexes_and_multiplicities(size_t nindexes )
{
  std::vector<std::pair<std::string, size_t> > ret;
  if (nindexes == 0) {
    ret.push_back(std::pair<std::string, size_t>("", 1));
  } else {
    const size_t max_nterms = pow_i(6, nindexes);
    size_t coeffs[max_nterms];
    for (size_t i=0; i<max_nterms; i++) {
      coeffs[i] = 0;
    }
    for (size_t i=0; i<max_nterms; i++) {
      char terms[nindexes];
      for (size_t j=0, term=i; j<nindexes; j++) {
	terms[j] = char(term%6);
	term /= 6;
      }
      std::sort(terms, terms+nindexes);
      size_t term = terms[0];
      for (size_t j=1; j<nindexes; j++) {
	term *= 6;
	term += terms[j];
      }
      coeffs[term]++;
    }
    for (size_t i=0; i<max_nterms; i++) {
      if (coeffs[i]>0) {
	char terms[nindexes];
	for (size_t j=0, term=i; j<nindexes; j++) {
	  terms[j] = '1' + char(term%6);
	  term /= 6;
	}
	std::sort(terms, terms+nindexes); 
	const std::string indexes = std::string(terms, nindexes);
	const size_t multiplicity = coeffs[i];
	ret.push_back(std::pair<std::string, size_t>(indexes, multiplicity));
      }
    }
  }
  return ret;
}

void placet_get_transfer_matrix_elements_fit(MatrixNd                       &out_elements,
					     const std::string              &in_beamline,
					     const std::string              &in_beam,
					     const std::string              &in_survey,
					     const std::vector<std::string> &in_matrix_elements,
					     int                             in_start,
					     int                             in_end )
{
  out_elements.clear();
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  BEAM *bunch = get_beam(in_beam.c_str());
  if (!bunch) bunch = inter_data.bunch;
  void (*survey)(BEAMLINE*) = (void (*)(BEAMLINE *))survey_find(in_survey.c_str());
  if (!survey) {
    placet_cout << ERROR << "placet_get_transfer_matrix_fit: survey '" << in_survey.c_str() << "' not found" << endmsg;
    exit(1);
  }
  if (in_end == -1 || in_end >= beamline->n_elements)
    in_end = beamline->n_elements-1;
  if (in_start > in_end) {
    placet_cout << ERROR << "placet_get_transfer_matrix_element_fit: parameter 'start' (arg 5) must be smaller or equal to paramter 'end' (arg 6)" << endmsg;
    exit(1);
  }
  BEAM *tb = bunch_remake(bunch);
  int nparticles = tb->slices;
  beam_copy(bunch, tb);
  inter_data.bunch = tb;
  survey(beamline);
  if (gsl_matrix *P = gsl_matrix_alloc(nparticles, 6)) {
    gsl_vector *x[6];
    for(int i=0; i<6; i++) {
      x[i] = gsl_vector_alloc(nparticles);
    }
    {
      double e0 = beamline->element[in_start]->get_ref_energy();
      for (int i=0; i<nparticles; i++) {
	const PARTICLE &particle = tb->particle[i];
	gsl_matrix_set(P, i, 0, particle.x * 1e-6);
	gsl_matrix_set(P, i, 1, particle.xp * 1e-6);
	gsl_matrix_set(P, i, 2, particle.y * 1e-6);
	gsl_matrix_set(P, i, 3, particle.yp * 1e-6);
	gsl_matrix_set(P, i, 4, particle.z * 1e-6);
	gsl_matrix_set(P, i, 5, (particle.energy - e0) / e0);
      }
      for (int start=in_start; start<=in_end; start++) {
	beamline->element[start]->track_0(tb);
      }
      e0 = beamline->element[in_end]->get_ref_energy();
      for (int i=0; i<nparticles; i++) {
	const PARTICLE &particle = tb->particle[i];
	gsl_vector_set(x[0], i, particle.x * 1e-6);
	gsl_vector_set(x[1], i, particle.xp * 1e-6);
	gsl_vector_set(x[2], i, particle.y * 1e-6);
	gsl_vector_set(x[3], i, particle.yp * 1e-6);
	gsl_vector_set(x[4], i, particle.z * 1e-6);
	gsl_vector_set(x[5], i, (particle.energy - e0) / e0);
      }
    }
    bool output_indexes[6] = { false }; // which indexes are required in the output
    size_t max_nindexes = 0; // max order required (e.g. Tijk => 2 output indexes, i.e. 2nd order required)
    for (size_t i=0; i<in_matrix_elements.size(); i++) {
      int oindex = in_matrix_elements[i][0] - '1';
      output_indexes[oindex] = true;
      if (in_matrix_elements[i].size()>1) {
	int nindexes_i = in_matrix_elements[i].size() - 1;
	if (nindexes_i > max_nindexes) {
	  max_nindexes = nindexes_i;
	}
      }
    }
    std::vector<std::pair<std::string, size_t> > all_indexes;
    for (size_t i=0; i<=max_nindexes; i++) {
      std::vector<std::pair<std::string, size_t> > indexes = get_indexes_and_multiplicities(i);
      all_indexes.insert(all_indexes.end(), indexes.begin(), indexes.end() );
    }
    // polynomial fit using GSL's gsl_multifit_linear
    // 1 fit for the horizontal bpm_readings observables
    // 1 fit for the vertical bpm_readings observables
    // gsl_multifit_linear requires X_ij matrix of predictor variables
    // in our case:
    // X_ij:
    //   i: index 0-5 in the phase space (x x' y y' z d)
    //   j: index 0-26 
    //      0-5 : Rij, n == _j
    //      6-26 : Rijk, n = 6 + ( j*(j-1)/2 + k-1 ) with k<j 
    const size_t nelements = all_indexes.size();
    if (gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc(nparticles, nelements)) {
      if (gsl_matrix *X = gsl_matrix_alloc(nparticles, nelements)) {
	if (gsl_matrix *cov = gsl_matrix_alloc(nelements, nelements)) {
	  if (gsl_vector *c = gsl_vector_alloc(nelements)) {
	    for (size_t i=0; i<nelements; i++) {
	      const std::pair<std::string, size_t> &pair=all_indexes[i];
	      const std::string &indexes_str = pair.first;
	      const size_t nindexes = indexes_str.size();
	      for (int n=0; n<nparticles; n++) {
		gsl_vector_const_view particle = gsl_matrix_const_row(P, n);
		const gsl_vector *p = &particle.vector;
		double value = 1.0;
		for (size_t j=0; j<nindexes; j++) {
		  int index = indexes_str[j] - '1';
		  value *= gsl_vector_get(p, index);
		}
		gsl_matrix_set(X, n, i, value);
	      }
	    }
	    out_elements.resize(in_matrix_elements.size(), 1);
	    for (size_t index=0; index<6; index++) {
	      if (output_indexes[index]) {
		double chisq;
		gsl_multifit_linear(X, x[index], c, cov, &chisq, work);
		// extracts the results for the index 'index'
		for (size_t i=0; i<in_matrix_elements.size(); i++) {
		  int _index = in_matrix_elements[i][0] - '1';
		  // if it is the index just calculated
		  if (index == _index) {
		    std::string _indexes = in_matrix_elements[i].substr(1);
		    if (_indexes.size()>0) {
		      std::sort(_indexes.begin(), _indexes.end());
		      for (size_t j=0; j<nelements; j++) {
			const std::pair<std::string, size_t> &pair = all_indexes[j];
			const std::string &indexes = pair.first;
			if (indexes == _indexes) {
			  // const size_t multiplicity = pair.second;
			  out_elements[i][0] = gsl_vector_get(c, j); // / multiplicity;
			  break;
			}
		      }
		    } else {
		      out_elements[i][0] = gsl_vector_get(c, 0);
		    }
		  }
		}
	      }
	    }
	    gsl_vector_free(c);
	  }
	  gsl_matrix_free(cov);
	}
	gsl_matrix_free(X);
      }
      gsl_multifit_linear_free(work);
    }
    for(int i=0; i<6; i++) {
      if (x[i]) gsl_vector_free(x[i]);
    }
    gsl_matrix_free(P);
  }
  beam_delete(tb);
}

void placet_girder_get_length(MatrixNd          &out_lengths,
			      const std::string &in_beamline )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  size_t count = 0;
  for (GIRDER *girder=beamline->first; girder; count++, girder=girder->next());
  out_lengths.resize(count,1);
  count = 0;
  for (GIRDER *girder=beamline->first; girder; girder=girder->next())
    out_lengths[count++][0] = girder->get_length()+girder->distance_to_prev_girder();
}

void placet_girder_move(const std::string &in_beamline,
			const MatrixNd    &in_delta )
{
  BEAMLINE *beamline = get_beamline(in_beamline.c_str());
  if (!beamline) beamline = inter_data.beamline;
  size_t index = 0;
  for (GIRDER *girder=beamline->first; index<in_delta.rows() && girder; index++, girder=girder->next()) {
    girder->move_ends(in_delta[index][0],
		      in_delta[index][2],
		      in_delta[index][1], 
		      in_delta[index][3]);
  }
}

void placet_Tcl_GetVar(std::string &out_value,
		       const std::string &in_varname1,
		       const std::string &in_varname2 )
{
  if (in_varname2.size()==0) {
    if (const char *value_ptr = Tcl_GetVar(beamline_survey_hook_interp, const_cast<char*>(in_varname1.c_str()), TCL_LEAVE_ERR_MSG)) {
      out_value = std::string(value_ptr);
    } else {
      placet_cout << ERROR << "Tcl_GetVar: variable '" << in_varname1 << "' (arg 1) does not exist" << endmsg;
      out_value = "";
    }
  } else {
    if (const char *value_ptr = Tcl_GetVar2(beamline_survey_hook_interp, const_cast<char*>(in_varname1.c_str()), const_cast<char*>(in_varname2.c_str()), TCL_LEAVE_ERR_MSG)) {
      out_value = std::string(value_ptr);
    } else {
      placet_cout << ERROR << "Tcl_GetVar: variable '" << in_varname1 << "' in array '" << in_varname2 << "' does not exist" << endmsg;
      out_value = "";
    }
  }
}

void placet_Tcl_SetVar(const std::string &in_varname1,
		       const std::string &in_varname2,
		       const Attribute   &in_value )
{
  std::ostringstream str;
  str.precision(DBL_DIG);
  if (in_value.type()==OPT_MATRIX) {
    const MatrixNd &matrix=in_value.get_matrix();
    str << '{';
    for (size_t i=0; i<matrix.size1(); i++) {
      str << " {";
      for (size_t j=0; j<matrix.size2(); j++)
	str << ' ' << matrix[i][j];
      str << " }";
      if (i<matrix.size1()-1)
	str << std::endl;
    }
    str << " }";
  } else {
    str << in_value;
  }
  Tcl_SetVar2(beamline_survey_hook_interp, const_cast<char*>(in_varname1.c_str()), const_cast<char*>(in_varname2.c_str()), const_cast<char*>(str.str().c_str()), TCL_LEAVE_ERR_MSG);
}

void placet_Tcl_SetVar(const std::string &in_varname1,
		       const Attribute   &in_value )
{
  std::ostringstream str;
  str.precision(DBL_DIG);
  if (in_value.type()==OPT_MATRIX) {
    const MatrixNd &matrix=in_value.get_matrix();
    str << '{';
    for (size_t i=0; i<matrix.size1(); i++) {
      str << " {";
      for (size_t j=0; j<matrix.size2(); j++)
	str << ' ' << matrix[i][j];
      str << " }";
      if (i<matrix.size1()-1)
	str << std::endl;
    }
    str << " }";
  } else {
    str << in_value;
  }
  Tcl_SetVar(beamline_survey_hook_interp, const_cast<char*>(in_varname1.c_str()), const_cast<char*>(str.str().c_str()), TCL_LEAVE_ERR_MSG);
}

void placet_Tcl_Eval(const std::string &in_script )
{
  int retval = Tcl_Eval(beamline_survey_hook_interp, const_cast<char*>(in_script.c_str()));
  if (const char *result = Tcl_GetStringResult(beamline_survey_hook_interp))
    switch (retval) {
    case TCL_ERROR:     placet_cout << ERROR << result << endmsg; break;
    case TCL_BREAK:     placet_cout << WARNING << result << endmsg; break;
    case TCL_CONTINUE:  placet_cout << WARNING << result << endmsg; break;
    }
  switch (retval) {
  case TCL_ERROR:     placet_cout << ERROR << "Tcl_Eval returned 'TCL_ERROR'" << endmsg; Tcl_Exit(1);
  case TCL_BREAK:     placet_cout << WARNING << "Tcl_Eval returned 'TCL_BREAK'" << endmsg; break;
  case TCL_CONTINUE:  placet_cout << WARNING << "Tcl_Eval returned 'TCL_CONTINUE'" << endmsg; break;
  }
}

void placet_Tcl_EvalFile(const std::string &in_filename )
{
  int retval = Tcl_EvalFile(beamline_survey_hook_interp, const_cast<char*>(in_filename.c_str()));
  if (const char *result = Tcl_GetStringResult(beamline_survey_hook_interp))
    switch (retval) {
    case TCL_ERROR:     placet_cout << ERROR << result << endmsg; break;
    case TCL_BREAK:     placet_cout << WARNING << result << endmsg; break;
    case TCL_CONTINUE:  placet_cout << WARNING << result << endmsg; break;
    }
  switch (retval) {
  case TCL_ERROR:     placet_cout << ERROR << "Tcl_EvalFile returned 'TCL_ERROR'" << endmsg; Tcl_Exit(1);
  case TCL_BREAK:     placet_cout << WARNING << "Tcl_EvalFile returned 'TCL_BREAK'" << endmsg; break;
  case TCL_CONTINUE:  placet_cout << WARNING << "Tcl_EvalFile returned 'TCL_CONTINUE'" << endmsg; break;
  }
}
