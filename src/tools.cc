#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include "errf.h"

/*
  Returns a list with Gaussian distribution
  */

int
Tcl_GaussList(ClientData /*clientData*/,Tcl_Interp *interp,int argc,char *argv[])
{
  Tcl_DString res;
  int n=1,norm=0;
  double x_min0=+1e300,x_max0=-1e300;
  double x_min,x_max;
  double d_x,sigma_z=1.0,charge=1.0,shift=0.0;
  int i,error;
  char buffer[127];
  double *wgt,wgt_sum;
  Tk_ArgvInfo table[]={
    {(char*)"-n_slices",TK_ARGV_INT,(char*)NULL,
     (char*)&n,
     (char*)"number of longitudinal slices for the bunch"},
    {(char*)"-norm",TK_ARGV_INT,(char*)NULL,
     (char*)&norm,
     (char*)"Normalise distribution to 1"},
    {(char*)"-min",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&x_min0,
     (char*)"start position of the charge (in units of sigma)"},
    {(char*)"-max",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&x_max0,
     (char*)"end position of the charge (in units of sigma)"},
    {(char*)"-sigma",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&sigma_z,
     (char*)"width of the distribution (in micro m)"},
    {(char*)"-shift",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&shift,
     (char*)"shift of the distribution centre (in micro m)"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&charge,
     (char*)"scaling factor for the charge (defaults to 1)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <",TCL_VOLATILE);
    Tcl_AppendResult(interp,argv[0],">:\n",NULL);
    for (i=1;i<argc;i++){
      Tcl_AppendResult(interp,argv[i],NULL);
    }
    return TCL_ERROR;
  }
  x_min=x_min0;
  x_max=x_max0;
  if (x_max<x_min) {
    Tcl_SetResult(interp,"In command <",TCL_VOLATILE);
    Tcl_AppendResult(interp,argv[0],">:\n",NULL);
    Tcl_AppendResult(interp,"max must be larger than min",NULL);
    return TCL_ERROR;
  }
  //  if (first) {
  //    first=0;
    Tcl_DStringInit(&res);
    //  }
  wgt=(double*)alloca(sizeof(double)*n);
  d_x=(x_max-x_min)/(double)n;
  x_max=x_min;
  wgt_sum=0.0;
  for (i=0;i<n;i++){
    x_min=x_max;
    x_max=x_min+d_x;
    wgt[i]=gauss_bin(x_min,x_max);
    wgt_sum+=wgt[i];
  }
  if (norm) {
    if (wgt_sum<=0.0) {
      for (i=0;i<n;i++){
	wgt[i]=1.0/(double)n;
      }
    }
    else {
      for (i=0;i<n;i++){
	wgt[i]/=wgt_sum;
      }
    }
  }
  for (i=0;i<n;i++) {
    snprintf(buffer,127,"%g %g",sigma_z*(x_min0+(i+0.5)*d_x)+shift,
	    wgt[i]*charge);
    //    placet_printf(INFO,"%s\n",buffer);
    Tcl_DStringAppendElement(&res,buffer);
  }
  Tcl_DStringResult(interp,&res);
  return TCL_OK;
}

/*
  Returns a list with a constant distribution
  */

int
Tcl_FlatList(ClientData /*clientData*/,Tcl_Interp *interp,int argc,char *argv[])
{
  Tcl_DString res;
  int n=1,norm=0;
  double x_min0=+1e300,x_max0=-1e300;
  double x_min,x_max;
  double d_x,sigma_z=1.0,charge=1.0,shift=0.0;
  int i,error;
  char buffer[127];
  double *wgt,wgt_sum;
  Tk_ArgvInfo table[]={
    {(char*)"-n_slices",TK_ARGV_INT,(char*)NULL,
     (char*)&n,
     (char*)"number of longitudinal slices for the bunch"},
    {(char*)"-norm",TK_ARGV_INT,(char*)NULL,
     (char*)&norm,
     (char*)"Normalise distribution to 1"},
    {(char*)"-min",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&x_min0,
     (char*)"start position of the charge (in units of sigma)"},
    {(char*)"-max",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&x_max0,
     (char*)"end position of the charge (in units of sigma)"},
    {(char*)"-sigma",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&sigma_z,
     (char*)"width of the distribution (in micro m)"},
    {(char*)"-shift",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&shift,
     (char*)"shift of the distribution centre (in micro m)"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&charge,
     (char*)"scaling factor for the charge (defaults to 1)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <",TCL_VOLATILE);
    Tcl_AppendResult(interp,argv[0],">:\n",NULL);
    for (i=1;i<argc;i++){
      Tcl_AppendResult(interp,argv[i],NULL);
    }
    return TCL_ERROR;
  }
  x_min=x_min0;
  x_max=x_max0;
  if (x_max<x_min) {
    Tcl_SetResult(interp,"In command <",TCL_VOLATILE);
    Tcl_AppendResult(interp,argv[0],">:\n",NULL);
    Tcl_AppendResult(interp,"max must be larger than min",NULL);
    return TCL_ERROR;
  }
  //  if (first) {
  //    first=0;
    Tcl_DStringInit(&res);
    //  }
  wgt=(double*)alloca(sizeof(double)*n);
  d_x=(x_max-x_min)/(double)n;
  x_max=x_min;
  wgt_sum=0.0;
  for (i=0;i<n;i++){
    x_min=x_max;
    x_max=x_min+d_x;
    wgt[i]=gauss_bin(x_min,x_max);
    wgt_sum+=wgt[i];
  }
  if (norm) {
    if (wgt_sum<=0.0) {
      for (i=0;i<n;i++){
	wgt[i]=1.0/(double)n;
      }
    }
    else {
      for (i=0;i<n;i++){
	wgt[i]/=wgt_sum;
      }
    }
  }
  for (i=0;i<n;i++) {
    snprintf(buffer,127,"%g %g",sigma_z*(x_min0+(i+0.5)*d_x)+shift,
	    wgt[i]*charge);
    //    placet_printf(INFO,"%s\n",buffer);
    Tcl_DStringAppendElement(&res,buffer);
  }
  Tcl_DStringResult(interp,&res);
  return TCL_OK;
}

int Tools_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"GaussList",Tcl_GaussList,(ClientData)NULL,
		    (Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(interp,"FlatList",Tcl_FlatList,(ClientData)NULL,
		    (Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Stupid","1.0");
  return TCL_OK;
}
