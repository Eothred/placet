/********************************************************************************
 *  Implementation file for the implementation of the function phase_advance	*
 *  										*
 *  Juergen Pfingster								*
 *  6. Jan 2009									*
 *										*
 *  Phase advance calculates the phase advance of a given beam line and 		*
 *  a given beam.								*
 *********************************************************************************/

#include "phase_advance.h" 
#include "placeti3.h"
#include "rmatrix.h"

/****************************************************
 * function: calc_beta			       	    *
 * Juergen Pfingstner, 18. Dec. 2008		    *
 *						    *
 * Content: Function calculates the beta_x and 	    *
 *	   beta_y out of the actual beam. It is a   *
 *	   simplified version of twiss_emitt	    *
 *						    *
 ****************************************************/

void calc_beta(BEAM *bunch, double *_beta_new_x, double *_beta_new_y)
{
  //double beta=0.0,alpha=0.0,beta0=0.0,alpha0=0.0,e;
  //double beta_x=0.0,alpha_x=0.0,beta0_x=0.0,alpha0_x=0.0;
  double beta_x = 0.0, beta_y = 0.0;

  // Calculation for a particle beam
  if (bunch->particle_beam) {
    double wgtsum=0.0; //xsum=0.0,ysum=0.0,xpsum=0.0,ypsum=0.0,esum=0.0;
    double sxxsum=0.0,sxxpsum=0.0,sxpxpsum=0.0,syysum=0.0,syypsum=0.0,sypypsum=0.0;
    for (int particle_index=0; particle_index < bunch->slices; ++particle_index){
      const PARTICLE &particle=bunch->particle[particle_index];
      //double energy=fabs(particle.energy);
      double wgt=fabs(particle.wgt);
      wgtsum+=wgt;
      //xsum+=wgt*particle.x;
      //xpsum+=wgt*particle.xp;
      //ysum+=wgt*particle.y;
      //ypsum+=wgt*particle.yp;
      //esum+=wgt*energy;
      sxxsum+=wgt*(particle.x*particle.x);
      sxxpsum+=wgt*(particle.x*particle.xp);
      sxpxpsum+=wgt*(particle.xp*particle.xp);
      syysum+=wgt*(particle.y*particle.y);
      syypsum+=wgt*(particle.y*particle.yp);
      sypypsum+=wgt*(particle.yp*particle.yp);
    }
    //xsum/=wgtsum;
    //xpsum/=wgtsum;
    //ysum/=wgtsum;
    //ypsum/=wgtsum;
    //esum/=wgtsum;
    sxxsum/=wgtsum;
    sxxpsum/=wgtsum;
    sxpxpsum/=wgtsum;
    syysum/=wgtsum;
    syypsum/=wgtsum;
    sypypsum/=wgtsum;
    double emitt_x=sqrt(sxxsum*sxpxpsum-sxxpsum*sxxpsum);
    double emitt_y=sqrt(syysum*sypypsum-syypsum*syypsum);
    beta_x=sxxsum/emitt_x;
    beta_y=syysum/emitt_y;
    *_beta_new_x = beta_x;
    *_beta_new_y = beta_y;
    //alpha_x=-sxxpsum/emitt_x;
    //alpha=-syypsum/emitt_y;
    //e=wgtsum;
  }

  // Calculation for a sliced beam in two different ways 
  else 
    {
      double eps_y=0.0,eps_x=0.0, wgtsum=0.0;
      //int nc=bunch->slices/2;
      for (int slice_index=0; slice_index < bunch->slices; slice_index++){
	double wgt = fabs(bunch->particle[slice_index].wgt);
	wgtsum+=wgt;
	eps_y+=(bunch->sigma[slice_index].r11*bunch->sigma[slice_index].r22
		-bunch->sigma[slice_index].r12*bunch->sigma[slice_index].r21)*bunch->particle[slice_index].wgt;
	beta_y+=bunch->sigma[slice_index].r11*bunch->particle[slice_index].wgt;
	//alpha_y-=bunch->sigma[i].r12*bunch->particle[i].wgt;
#ifdef TWODIM
	eps_x+=(bunch->sigma_xx[slice_index].r11*bunch->sigma_xx[slice_index].r22
		-bunch->sigma_xx[slice_index].r12*bunch->sigma_xx[slice_index].r21)
	  *bunch->particle[slice_index].wgt;
	beta_x+=bunch->sigma_xx[slice_index].r11*bunch->particle[slice_index].wgt;
	//alpha_x-=bunch->sigma_xx[i].r12*bunch->particle[i].wgt;
#endif
      }
      eps_y/=wgtsum;
      beta_y/=wgtsum;
      eps_y=sqrt(eps_y);
      beta_y/=eps_y;
      //alpha/=eps;
      //double eps0=(bunch->sigma[nc].r11*bunch->sigma[nc].r22
      //  -bunch->sigma[nc].r12*bunch->sigma[nc].r21);
      //beta0=bunch->sigma[nc].r11;
      //alpha0=-bunch->sigma[nc].r12;
      //eps0=sqrt(eps0);
      //beta0/=eps0;
      //alpha0/=eps0;
      *_beta_new_y = beta_y; // beta is the average beta function of all beam slices, beta0 would just be the center one.
#ifdef TWODIM
      eps_x=sqrt(eps_x);
      beta_x/=eps_x;
      //alpha_x/=eps_x;
      //double eps0_x=(bunch->sigma_xx[nc].r11*bunch->sigma_xx[nc].r22
      //    -bunch->sigma_xx[nc].r12*bunch->sigma_xx[nc].r21);
      //beta0_x=bunch->sigma_xx[nc].r11;
      //alpha0_x=-bunch->sigma_xx[nc].r12;
      //eps0_x=sqrt(eps0_x);
      //beta0_x/=eps0_x;
      //alpha0_x/=eps0_x;
      *_beta_new_x = beta_x; // beta_x is the average beta function of all beam slices, beta0_x would just be the center one.
#endif
      //e=bunch->particle[nc].energy;
    }
}

/****************************************************
 * function: phase_advance_calc			    *
 * Juergen Pfingstner, 17. Dec. 2008		    *
 *						    *
 * Content: Function calculates the phase advance    *
 *	   for a given element, with the help of    *
 *	   the response matrix and the beta function*
 *	   at the start and the end of the element. *
 *	   See CAS material for details		    *
 *						    *
 ****************************************************/

void phase_advance_calc(ELEMENT **element,int element_index, double *_phase_advance_x, double *_phase_advance_y, 
			double beta_x_old, double beta_y_old, double beta_x_new, double beta_y_new)
{
  double phase_advance_x_cell = 0.0, phase_advance_y_cell = 0.0;
  double trans_x = 0.0, trans_y = 0.0;
  double argument_x = 0.0, argument_y = 0.0;
  Matrix<6,6> trans(0.0);

  // Debug file
  //FILE *debug_f = fopen("debug.info","a");

  // Get the response matrix and extract the necessary values
  trans = element[element_index]->get_transfer_matrix_6d();
  trans_x = trans[0][1];
  trans_y = trans[2][3];
	
  // Calculate the cell phase advance
  argument_x = trans_x/sqrt(beta_x_old*beta_x_new);
  if(fabs(argument_x) > 1)
    {
      // Should never happen but happens !?

      // Debug code
      //fprintf(debug_f, "==============================\n");
      //fprintf(debug_f, "Error in the argument x: %g \n", argument_x);

      argument_x = 0;
    }
  phase_advance_x_cell = asin(argument_x);
  if(phase_advance_x_cell < 0.0)
    phase_advance_x_cell += M_PI; 

  argument_y = trans_y/sqrt(beta_y_old*beta_y_new);
  if(fabs(argument_y) > 1)
    {
      // Should never happen but happens !?

      // Debug code
      //fprintf(debug_f, "==============================\n");
      //fprintf(debug_f, "Error in the argument y: %g \n", argument_y);

      argument_y = 0;
    }
  phase_advance_y_cell = asin(argument_y);
  if(phase_advance_y_cell < 0.0)
    phase_advance_y_cell += M_PI; 
	
  // Update the overall phase advance
  *_phase_advance_x += phase_advance_x_cell;
  *_phase_advance_y += phase_advance_y_cell;

  // Debug code
  /*fprintf(debug_f, "argument x: %g \n", argument_x);
    fprintf(debug_f, "argument y: %g \n", argument_y);
    fprintf(debug_f, "phase advance x per cell [deg]: %g \n", phase_advance_x_cell*180/M_PI);
    fprintf(debug_f, "phase advance y per cell [deg]: %g \n", phase_advance_y_cell*180/M_PI);
    fprintf(debug_f, "trans_x: %g \n", trans_x);
    fprintf(debug_f, "beta_x_old: %g \n", beta_x_old);
    fprintf(debug_f, "beta_x_new: %g \n", beta_x_new);
    fprintf(debug_f, "trans_y: %g \n", trans_y);
    fprintf(debug_f, "beta_y_old: %g \n", beta_y_old);
    fprintf(debug_f, "beta_y_new: %g \n", beta_y_new);
    fprintf(debug_f, "Elementindex: %d \n", element_index);

    // Determine the element type
    const char *type;
    if (element[element_index]->is_quad()) type="Quadrupole";
    else if (element[element_index]->is_multipole()) type="Multipole";
    else if (element[element_index]->is_dipole()) type="Dipole";
    else if (element[element_index]->is_cavity()) type="AccCavity";
    else if (element[element_index]->is_sbend()) type="SBend";
    else if (element[element_index]->is_drift()) type="Drift";
    else if (element[element_index]->is_bpm()) type="Bpm";
    else type="unknown";

    fprintf(debug_f, "Elementtype: %s \n\n", type);

    fclose(debug_f);*/
}

/****************************************************
 * function: phase_advance_write			    *
 * Juergen Pfingstner, 17. Dec. 2008		    *
 *						    *
 * Content: Convert the phase advances properly and  *
 * 	   write them into the file		    *
 ****************************************************/

void phase_advance_write(double phase_advance_x, double phase_advance_y, char *file_name)
{
  // Debug code
  //placet_printf(INFO,"phase_advance_x %g \n", phase_advance_x);
  //placet_printf(INFO,"phase_advance_y %g \n", phase_advance_y);

  phase_advance_x = phase_advance_x * 180 / M_PI; 
  phase_advance_y = phase_advance_y * 180 / M_PI; 
	
  int turns_x = int(phase_advance_x / 360);
  double remainder_x = phase_advance_x - double(360*turns_x);
  //placet_printf(INFO,"turns, remainder");
	
  placet_printf(INFO,"phase advance x: %d turns and %f degree \n", turns_x, remainder_x);

  int turns_y = int(phase_advance_y / 360);
  double remainder_y = phase_advance_y - double(360*turns_y);
  placet_printf(INFO,"phase advance y: %d turns and %f degree \n", turns_y, remainder_y);

  if(file_name)
    {
      FILE *file_ptr = open_file(file_name);
		
      fprintf(file_ptr, "phase advance x: \n");
      fprintf(file_ptr, "%f degree, which corresponds to \n", phase_advance_x);
      fprintf(file_ptr, "%d turns and\n", turns_x);
      fprintf(file_ptr, "%f degree \n\n", remainder_x);

      fprintf(file_ptr, "phase advance y: \n");
      fprintf(file_ptr, "%f degree, which corresponds to \n", phase_advance_y);
      fprintf(file_ptr, "%d turns and\n", turns_y);
      fprintf(file_ptr, "%f degree \n", remainder_y);

      close_file(file_ptr);
    }
}

/****************************************************
 * function: phase_advance		            *
 * Juergen Pfingstner, 18. Dec. 2008		    *
 *						    *
 * Content: Function calculates the phase advance    *
 *	   for a given element, with the help of    *
 *	   the response matrix and the beta function*
 *	   at the start and the end of the element. *
 *	   See CAS material for details		    *
 *						    *
 ****************************************************/

void phase_advance(BEAMLINE *beamline, BEAM *bunch0, char* file_result, char* file_detail)
{
  // Runtime check
  //placet_printf(INFO,"Runtime check start \n"); // ca. 3 sec. on my computer

  ELEMENT **element;
  FILE *fptr_detail = NULL;
  int element_index=0;
  double phase_advance_x=0.0, phase_advance_y=0.0; 
  //  double phase_advance_x_cell=0.0, phase_advance_y_cell=0.0;
  double beta_x_old = 0.0, beta_x_new = 0.0;
  double beta_y_old = 0.0, beta_y_new = 0.0;
  double energy = 0.0, ref_energy = 0.0;
  //double eps_y,beta_y,alpha,eps0,beta0,alpha0;
  //#ifdef TWODIM
  //double eps_x,beta_x,alpha_x,eps0_x,beta0_x,alpha0_x;
  //#endif
  double s=0.0;
  BEAM *bunch;

  // Erase the debug file
  // FILE *debug_f = fopen("debug.info","w");
  // fclose(debug_f);

  if(file_detail)
    {
      fptr_detail = open_file(file_detail);
    }

  // Init the objects
  bunch=bunch_remake(bunch0);
  beam_copy(bunch0,bunch);
  element=beamline->element;  
  // Calculate the beta_functions at the begin of the beam line
  calc_beta(bunch, &beta_x_old, &beta_y_old);
  while(element[element_index]!=NULL){
    s+=element[element_index]->get_length();
    // Track the beam to the end of the element
    element[element_index]->track(bunch);

    // Get the beta functions 
    calc_beta(bunch, &beta_x_new, &beta_y_new);

    // Set the right ref_engery to get the right transfer_matrix in phase_advance_calc
    for (int slice_index = 0; slice_index < bunch->slices; slice_index++) {
      energy += bunch->particle[slice_index].energy;
    }
    if (bunch->slices>0) {
      energy /= bunch->slices;
    }
    ref_energy = element[element_index]->get_ref_energy();
    element[element_index]->set_ref_energy(energy);

    // Calculate the phase advance per cell
    phase_advance_calc(element, element_index, &phase_advance_x, &phase_advance_y, beta_x_old, beta_y_old,
		       beta_x_new, beta_y_new);

    // Set back the ref_energy, just in case
    element[element_index]->set_ref_energy(ref_energy);

    // Update the overall phase advance
    beta_x_old = beta_x_new;
    beta_y_old = beta_y_new;

    // Write it to the file if necessary
    if(file_detail)
      {

	//	  if (element[element_index]->is_quad()){
	fprintf(fptr_detail, "%9.3f      %9.3f	%9.3f\n", s, phase_advance_x * 180 / M_PI, phase_advance_y * 180 / M_PI);
	//}
      }

    element_index++;
  }

  phase_advance_write(phase_advance_x, phase_advance_y, file_result);
  beam_delete(bunch);
  if(file_detail)
    {
      close_file(fptr_detail);
    }

  // Runtime check
  //placet_printf(INFO,"Runtime check end \n");
}
