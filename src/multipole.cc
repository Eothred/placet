#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <complex>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */


#include "conversion.h"
#include "function.h"
#include "multipole.h"

extern INTER_DATA_STRUCT inter_data;

/*
  Calculates the kick of a 2*n-pole with strength k on a particle
  at position x,y. The multipole is roted by c0=cos(phi), s0=sin(phi).
  Kicks are returned in kx, ky.
*/

MULTIPOLE::MULTIPOLE(int &argc, char **argv ) : ELEMENT(), tilt(0.0), strength(0.0), field(0), thick(true)
{
  attributes.add("strength_list", "Multipole strength list { S1, S2, ... , Sn } [GeV/m^(n-1))] [STRING]", OPT_STRING, this,
		 Set(&MULTIPOLE::set_strength_list_str), 
		 Get(&MULTIPOLE::get_strength_list_str));
  attributes.add("strength", "Multipole strength [GeV/m^(type-1)]", OPT_COMPLEX, &strength);
  attributes.add("type", "Multipole order (2: quadrupole, 3: sextupole, 4: octupole, ...) [INT]", OPT_INT, &field);
  attributes.add("steps", "Number of steps for tracking (default: 5) [INT]", OPT_INT, &flags.thin_lens);
  attributes.add("tilt", "Tilt angle [rad]", OPT_DOUBLE, &tilt);
  attributes.add("tilt_deg", "Tilt angle [deg]", OPT_DOUBLE, &tilt, deg2rad, rad2deg);
  attributes.add("thick", "Treat linear kicks as thick lenses", OPT_BOOL, &thick);
  set_attributes(argc, argv);
  if (flags.thin_lens==0) {
    flags.thin_lens=5;
  }
}

MULTIPOLE::MULTIPOLE(int type, double _strength )
{
  field=type;
  strength=_strength;
  flags.thin_lens=5;
  tilt=0.0;
  thick=true;
}

void MULTIPOLE::step_partial(BEAM *beam, double l, void (ELEMENT::*step_function)(BEAM*) )
{
    placet_cout << ERROR << "Partial stepping not implemented for multipoles" << endmsg;
}

void MULTIPOLE::step_twiss(BEAM *beam,FILE *file,double step, int j,double s0, int n1,int n2,void (*callback0)(FILE*,BEAM*,int,double,int,int)){
  if (abs(strength)<std::numeric_limits<double>::epsilon()) {
    drift_step_twiss(beam,file,step,j,s0,n1,n2,callback0);
    return;
  }
  int nstep=int(geometry.length/step)+1;
  double l=geometry.length;
  std::complex<double> s=strength;
  geometry.length/=nstep;
  strength/=double(nstep);
  if (callback0) callback0(file,beam,j,s0,n1,n2);
  for (int istep=0;istep<nstep;istep++) {
    s0+=geometry.length;
    if (flags.thin_lens) {
      if (flags.longitudinal) {
        if (flags.synrad) {
	  placet_printf(ERROR, " MULTIPOLE::step_twiss: cannot track with synchrotron radiation thin lens and second order \n");
	  exit(1);
	}
        else {
	  placet_printf(ERROR, " MULTIPOLE::step_twiss: no 6d tracking implemented for sliced beam  \n");
	  exit(1);
	}
      } else {
        if (flags.synrad) {
	  placet_printf(ERROR, " MULTIPOLE::step_twiss: cannot track with synchrotron radiation thin lens and second order \n");
	  exit(1);
	}
        else  MULTIPOLE::step_4d_tl(beam);
      }
    } else {
      if (flags.longitudinal) {
        if (flags.synrad){
	  placet_printf(ERROR, " MULTIPOLE::step_twiss: cannot track with synchrotron radiation thin lens and second order \n");
	  exit(1);
	}
        else {
	  placet_printf(ERROR, " MULTIPOLE::step_twiss: no 6d tracking implemented for sliced beam  \n");
	  exit(1);
	}            
      } else {
        if (flags.synrad) {
	  placet_printf(ERROR, " MULTIPOLE::step_twiss: cannot track with synchrotron radiation thin lens and second order \n");
	  exit(1);
	}
        else MULTIPOLE::step_4d_tl(beam);
      }
    }
    if (callback0) callback0(file,beam,j,s0,n1,n2);
  } 
  geometry.length=l;
  strength=s;
}

void MULTIPOLE::step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0, int n1,int n2,void (*callback0)(FILE*,BEAM*,int,double,int,int)){
  if (abs(strength)<std::numeric_limits<double>::epsilon()) {
    drift_step_twiss_0(beam,file,step,j,s0,n1,n2,callback0);
    return;
  }
  int nstep=int(geometry.length/step)+1;
  double l=geometry.length;
  std::complex<double> s=strength;
  geometry.length/=nstep;
  strength/=double(nstep);
  if (callback0) callback0(file,beam,j,s0,n1,n2);
  for (int istep=0;istep<nstep;istep++) {
    s0 += geometry.length;
    if (flags.longitudinal) {
      if (flags.synrad) MULTIPOLE::step_6d_tl_sr_0(beam);
      else              MULTIPOLE::step_6d_tl_0(beam);
    } else {
      if (flags.synrad) MULTIPOLE::step_4d_tl_sr_0(beam);
      else              MULTIPOLE::step_4d_tl_0(beam);
    }
    if (callback0) callback0(file,beam,j,s0,n1,n2);
  } 
  geometry.length=l;
  strength=s;
}

void MULTIPOLE::list(FILE *file) const
{
  if (strength_list.empty()) {
    fprintf(file,"Multipole -length %g -strength (%g,%g) -type %d -tilt %g\n",
	    geometry.length,
	    std::real(strength),
	    std::imag(strength),
	    field,
	    tilt);
  } else {
    std::string str_list = get_strength_list_str();
    fprintf(file,"Multipole -length %g -strength_list { %s } -tilt %g\n",
	    geometry.length,
	    str_list.c_str(),
	    tilt);
  }
}

int tk_MultipoleNumberList(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			   char *argv[])
{
  int error,i;
  ELEMENT **element;
  char buffer[100];
  int order=-1;

  Tk_ArgvInfo table[]={
    {(char*)"-order",TK_ARGV_INT,(char*)NULL,(char*)&order,
     (char*)"Order of the multipoles to search for"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  element=inter_data.beamline->element;

  if (order<0) {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      if (element[i]->is_multipole()){
	snprintf(buffer,100,"%d",i);
	Tcl_AppendElement(interp,buffer);
      }
    }
  } else {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      if (MULTIPOLE *mpole=element[i]->multipole_ptr()){
	if (order==mpole->get_field()) {
	  snprintf(buffer,100,"%d",i);
	  Tcl_AppendElement(interp,buffer);
	}
      }
    }
  }
  return TCL_OK;
}

int tk_MultipoleGetStrengthList(ClientData /*clientdata*/,Tcl_Interp *interp,
				int argc,char *argv[])
{
  int error,i;
  ELEMENT **element;
  char buffer[100];
  int order=-1;

  Tk_ArgvInfo table[]={
    {(char*)"-order",TK_ARGV_INT,(char*)NULL,(char*)&order,
     (char*)"Order of the multipoles to search for"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  element=inter_data.beamline->element;

  if (order<0) {
    for (i=0;i<inter_data.beamline->n_elements;i++) {
      if (MULTIPOLE *mpole=element[i]->multipole_ptr()) {
	std::complex<double> _strength=mpole->get_strength();
	if (fabs(std::imag(_strength))<std::numeric_limits<double>::epsilon()) {
	  snprintf(buffer,100,"%g",std::real(_strength));
	} else {
	  snprintf(buffer,100,"(%g,%g)",std::real(_strength),std::imag(_strength));
	}
	Tcl_AppendElement(interp,buffer);
      }
    }
  } else {
    for (i=0;i<inter_data.beamline->n_elements;i++) {
      if (MULTIPOLE *mpole=element[i]->multipole_ptr()) {
	if (order==mpole->get_field()) {
	  std::complex<double> _strength=mpole->get_strength();
	  if (fabs(std::imag(_strength))<std::numeric_limits<double>::epsilon()) {
	    snprintf(buffer,100,"%g",std::real(_strength));
	  } else {
	    snprintf(buffer,100,"(%g,%g)",std::real(_strength),std::imag(_strength));
	  }
	  Tcl_AppendElement(interp,buffer);
	}
      }
    }
  }
  return TCL_OK;
}

int tk_MultipoleSetStrengthList(ClientData /*clientdata*/,Tcl_Interp *interp,
				int argc,char *argv[])
{
  int error,i,j=0;
  double val;
  ELEMENT **element;
  int order=-1,n;
  char **v;

  Tk_ArgvInfo table[]={
    {(char*)"-order",TK_ARGV_INT,(char*)NULL,(char*)&order,
     (char*)"Order of the multipoles to search for"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  element=inter_data.beamline->element;

  if ((error=Tcl_SplitList(interp,argv[1],&n,&v))) return error;
  if (order<0) {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      if (MULTIPOLE *mpole=element[i]->multipole_ptr()){
	Tcl_GetDouble(interp,v[j++],&val);
	mpole->set_strength(val);
      }
    }
  } else {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      if (MULTIPOLE *mpole=element[i]->multipole_ptr()){
	if (order==mpole->get_field()) {
	  Tcl_GetDouble(interp,v[j++],&val);
          mpole->set_strength(val);
	}
      }
    }
  }
  Tcl_Free((char*)v);
  return TCL_OK;
}
//
// Get multipoles magnetic fields By + iBx = k(x + iy)^n-1 
//_________________________________________________________________
void MULTIPOLE::GetMagField(PARTICLE *particle, double *bfield){
  if(geometry.length<std::numeric_limits<double>::epsilon()){
    // to be done for the moment ignore zero length multipole
    bfield[0]=0.0;
    bfield[1]=0.0;
    bfield[2]=0.0;
  }
  else{
    double g=std::real(strength)*3.3356409520/get_ref_energy()/geometry.length;
    double x = (particle->x - offset.x) * 1.e-6;
    double y = (particle->y - offset.y) * 1.e-6;
    switch (field){
    case 3:
      g*=-0.5;
      bfield[0]=2*g*y*x;
      bfield[1]=g*(x*x-y*y);
      break;
    case 4:
      g*=1.0/6.0;
      bfield[0]=g*(3*x*x*y-y*y*y);
      bfield[1]=g*(x*x*x-3*x*y*y);
      break;
    case 5:
      g*=-1.0/24.0;
      bfield[0]=g*(4*x*y*(x*x-y*y*y));
      bfield[1]=g*(pow(x,4) - 6*x*x*y*y + pow(y,4));
    default: break;
    }
    bfield[2]=0.0;
  }
}

Matrix<6,6> MULTIPOLE::get_transfer_matrix_6d(double _energy ) const 
{
  Matrix<6,6> R(0.0);
  R[4][4]=1.0;
  R[5][5]=1.0;
  if (fabs(geometry.length)>std::numeric_limits<double>::epsilon()) {
    bool is_quad = false;
    if (strength_list.empty()&&field==2)
      is_quad = true;
    if (!is_quad&&strength_list.size()>1)
      is_quad = abs(strength_list[1])>std::numeric_limits<double>::epsilon();
    if (is_quad) {
      std::complex<double> _strength=strength_list.empty()?strength:strength_list[1];
      if (_energy == -1.0) {
        _energy = ref_energy;
      }
      double k=abs(_strength)/geometry.length/_energy;
      if (k>0.0) {
	double ksqrt=sqrt(k);
	double c,s;
	sincos(ksqrt*geometry.length, &s, &c);
	R[0][0]=c;
	R[0][1]=s/ksqrt;
	R[1][0]=-s*ksqrt;
	R[1][1]=c;
	sincosh(ksqrt*geometry.length,s,c);
	R[2][2]=c;
	R[2][3]=s/ksqrt;
	R[3][2]=s*ksqrt;
	R[3][3]=c;
      } else {
	double ksqrt=sqrt(-k);
	double c,s;
	sincosh(ksqrt*geometry.length,s,c);
	R[0][0]=c;
	R[0][1]=s/ksqrt;
	R[1][0]=s*ksqrt;
	R[1][1]=c;
	sincos(ksqrt*geometry.length, &s, &c);
	R[2][2]=c;
	R[2][3]=s/ksqrt;
	R[3][2]=-s*ksqrt;
	R[3][3]=c;
      }
      double _tilt = tilt+std::arg(_strength)/2;
      if (fabs(_tilt)>std::numeric_limits<double>::epsilon()) {
	double s,c;
	sincos(_tilt,&s,&c);
	Matrix<6,6> Rot(0.0);
	Rot[0][0]=Rot[1][1]=Rot[2][2]=Rot[3][3]=c;
	Rot[0][2]=Rot[1][3]=-s;
	Rot[2][0]=Rot[3][1]=s;
	Rot[4][4]=1.0;
	Rot[5][5]=1.0;
	R*=Rot;
	Rot[0][2]=Rot[1][3]=s;
	Rot[2][0]=Rot[3][1]=-s;
	R=Rot*R;
      }
    } else {
      R[0][0]=1.0;
      R[0][1]=geometry.length;
      R[1][1]=1.0;
      R[2][2]=1.0;
      R[2][3]=geometry.length;
      R[3][3]=1.0;
    }
  } else {
    R[0][0]=1.0;
    R[1][1]=1.0;
    R[2][2]=1.0;
    R[3][3]=1.0;
  }
  return R;
}
