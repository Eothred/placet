#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beam.h"
#include "particle.h"
#include "rmatrix.h"

/* Function prototypes */

#include "sort.h"

void
exchange(BEAM *beam,int i,int j)
{
  PARTICLE p_tmp;
  R_MATRIX r_tmp;
  double z_tmp;

  if (i==j) return;

  r_tmp=beam->sigma[i];
  beam->sigma[i]=beam->sigma[j];
  beam->sigma[j]=r_tmp;
#ifdef TWODIM
  r_tmp=beam->sigma_xx[i];
  beam->sigma_xx[i]=beam->sigma_xx[j];
  beam->sigma_xx[j]=r_tmp;
  r_tmp=beam->sigma_xy[i];
  beam->sigma_xy[i]=beam->sigma_xy[j];
  beam->sigma_xy[j]=r_tmp;
#endif
  p_tmp=beam->particle[i];
  beam->particle[i]=beam->particle[j];
  beam->particle[j]=p_tmp;
  z_tmp=beam->z_position[i];
  beam->z_position[i]=beam->z_position[j];
  beam->z_position[j]=z_tmp;
}

void
sort_beam(BEAM *beam)
{
  if (beam->particle_beam==true) {
    std::sort(beam->particle,beam->particle+beam->slices);
  } else {
    int n=beam->slices;
    int flag=1;
    while(flag) {
      flag=0;
      for (int i=1;i<n;i++){
        if (beam->z_position[i-1]>beam->z_position[i]) {
	  flag=1;
	  //	correct_swap(beam,i);
       	  exchange(beam,i-1,i);
	  /*	
	  if (beam->particle[i-1].energy<0.06) {
	    placet_printf(INFO,"first %d %g\n",i,beam->particle[i-1].energy);
	  }
	  if (beam->particle[i].energy<0.06) {
	    placet_printf(INFO,"second %d %g\n",i,beam->particle[i].energy);
	  }
	  */
        }
      }
    }
  }
}
