#include "dipole_kick.h"

#include "beam.h"
#include "cavbpm.h"
#include "cavity.h"
#include "element.h"
#include "structures_def.h"

#define CAV_MULTI_ANGLE
#define EQUAL_SLICE

/** 
    wake_data is the long range wake and is only applied when bunches > 1
 */
void dipole_kick_n(ELEMENT *element, WAKE_DATA *wake_data, BEAM *beam )
{
  double v_tesla;
  if (CAVITY *cavity=element->cavity_ptr()) {
    v_tesla = cavity->get_v_tesla();
  } else {
    v_tesla = 1.0;
  }
  if (beam->bunches==1) {
    double *rho_x=beam->rho_x[0];
    double *rho_y=beam->rho_y[0];
    double *p, factor;
    if (CAVBPM* cavbpm = element->cavbpm_ptr()) {
      p=cavbpm->get_field()[0].kick;
      factor = 1.; // the length has already been integrated over for cavity BPM.
    }
    else {
      p=beam->field[0].kick;
      factor=beam->transv_factor*element->get_length()*beam->factor;
    }
    int imacro1=0;
    int imacro2=0;
#ifdef HTGEN
    int imacro3=0; //haloparticles
#endif
    for (int j=0;j<beam->slices_per_bunch;j++) {//loop over slices
      rho_x[j]=0.0;
      rho_y[j]=0.0;
      for (int i=0;i<beam->particle_number[j];i++) {
        const PARTICLE &particle=beam->particle[imacro1];
	rho_x[j]+=particle.x*particle.wgt;
	rho_y[j]+=particle.y*particle.wgt;
        imacro1++;
      }
      double sumx=0.0;
      double sumy=0.0;
      for (int i=0;i<=j;i++) {
	sumx += rho_x[i] * *p;
	sumy += rho_y[i] * *p;
	p++;
      }
      //apply wakefield kick to the beam
      double kickx = sumx*factor;
      double kicky = sumy*factor;
      for (int i=0;i<beam->particle_number[j];i++) {
        PARTICLE &particle=beam->particle[imacro2];
	particle.xp+=kickx/particle.energy;
	particle.yp+=kicky/particle.energy;
        imacro2++;
      }
#ifdef HTGEN
      // apply wakefield kick to the halo
      for (int i_m=0;i_m<beam->particle_number_sec[j];i_m++) {
	PARTICLE &particle=beam->particle_sec[imacro3];
	particle.xp+=kickx/particle.energy;
	particle.yp+=kicky/particle.energy;
	imacro3++; 
      }
#endif
    }
  } else {
    if (!wake_data) {
      placet_cout << ERROR << "wake data not defined, do InjectorCavityDefine (before lattice definition)" << endmsg;
      return;
    }
    /* Initialise wakefields */
    int nstep=wake_data->n;
    struct {
      double x;
      double y;
      double xb;
      double yb;
    } multi[nstep];
#ifdef EQUAL_SLICE
    struct {
      double s,c;
    } d0[nstep];
#endif  
    double longfact[nstep];
    INJECT_BEAM_PARAM *param=beam->drive_data->param.inject;
    for (int i=0;i<nstep;i++) {
      multi[i].x=0.0;
      multi[i].y=0.0;
      multi[i].xb=0.0;
      multi[i].yb=0.0;
      longfact[i]=wake_data->a0[i]*(param->charge*ECHARGE*1e12)*1e-6*1e-3;
#ifdef EQUAL_SLICE
      sincos(v_tesla*(beam->z_position[1]-beam->z_position[0])*TWOPI*1e-6/wake_data->lambda[i],&d0[i].s,&d0[i].c);
#endif
    }
#ifdef EQUAL_SLICE
    double xz[nstep];
    double yz[nstep];
    if (CAVITY *cavity=element->cavity_ptr()) {
      cavity->fill_z_mode_xy(xz,yz,nstep);
    } else {
      for (int i=0;i<nstep;i++) {
        xz[i]=0.0;
        yz[i]=0.0;
      }
    }
#endif
    /* loop over all bunches */
    double d_z=element->get_length()/nstep;
    int imacro1=0;
    int imacro2=0;
#ifdef HTGEN
    int imacro3=0; //halo particles
#endif
    for (int k=0;k<beam->bunches;k++) {
      /* apply attenuation */
      if (k>0) {
        for (int step=0;step<nstep;step++) {
	  double tmp=exp(-PI*(param->bunch[k].z-param->bunch[k-1].z)*1e-6/(wake_data->lambda[step]*wake_data->Q[step]));
	  multi[step].y*=tmp;
	  multi[step].yb*=tmp;
#ifdef TWODIM
	  multi[step].x*=tmp;
	  multi[step].xb*=tmp;
#endif
        }
      }
      int m=k*beam->slices_per_bunch;
      double w[beam->slices_per_bunch];
      double *p, factor;
      if (CAVBPM* cavbpm = element->cavbpm_ptr()) {
	p=cavbpm->get_field()[0].kick;
	factor = 1.; // the length has already been integrated over for cavity BPM.
      }
      else {
	p=beam->field[0].kick;
	factor=beam->transv_factor*element->get_length()*beam->factor;
      }

      double *rho_x=beam->rho_x[0];
      double *rho_y=beam->rho_y[0];
      struct {
        double xp;
        double yp;
      } rho[beam->slices_per_bunch];
      struct {
        double x;
        double y;
      } kick[beam->slices_per_bunch];
      for (int j=0;j<beam->slices_per_bunch;j++) {
        w[j]=0.0;
        rho_x[j]=0.0;
        rho_y[j]=0.0;
        rho[j].yp=0.0;
        rho[j].xp=0.0;
        for (int i_m=0;i_m<beam->particle_number[j+k*beam->slices_per_bunch];i_m++) {
          const PARTICLE &particle=beam->particle[imacro1];
	  w[j]+=particle.wgt;
	  rho_x[j]+=particle.x*particle.wgt;
	  rho_y[j]+=particle.y*particle.wgt;
#ifdef CAV_MULTI_ANGLE
	  rho[j].xp+=particle.xp*particle.wgt;
	  rho[j].yp+=particle.yp*particle.wgt;
#endif
          imacro1++;
        }
        double sumy=0.0;
        double sumx=0.0;
        for (int i=0;i<=j;i++) {
	  sumx+=rho_x[i] * *p;
	  sumy+=rho_y[i] * *p;
	  p++;
        }
        kick[j].x=sumx;
        kick[j].y=sumy;
      }
      double z_mode=-0.5*(element->get_length()-d_z);
      struct {
        double x;
        double y;
        double xb;
        double yb;
      } pos;
      for (int step=0;step<nstep;step++) {
        pos.x=0.0;
        pos.y=0.0;
        pos.xb=0.0;
        pos.yb=0.0;
        double c, s;
        sincos(v_tesla*beam->z_position[m]*TWOPI*1e-6/wake_data->lambda[step],&s,&c);
#ifdef EQUAL_SLICE
        double ds=d0[step].s;
        double dc=d0[step].c;
#else
        sincos(v_tesla*(beam->z_position[m+1]-beam->z_position[m])*TWOPI*1e-6/wake_data->lambda[step],&ds,&dc);
#endif
        for (int j=0;j<beam->slices_per_bunch;j++) {
#ifdef CAV_MULTI_ANGLE
	  pos.x  += (rho_x[j]+z_mode*rho[j].xp-w[j]*xz[step])*c;
	  pos.y  += (rho_y[j]+z_mode*rho[j].yp-w[j]*yz[step])*c;
	  pos.xb -= (rho_x[j]+z_mode*rho[j].xp-w[j]*xz[step])*s;
	  pos.yb -= (rho_y[j]+z_mode*rho[j].yp-w[j]*yz[step])*s;
#else
	  pos.x  += rho_x[j]*c;
	  pos.y  += rho_y[j]*c;
	  pos.xb -= rho_x[j]*s;
	  pos.yb -= rho_y[j]*s;
#endif
	  kick[j].x+=(multi[step].x*s+multi[step].xb*c)*longfact[step];
	  kick[j].y+=(multi[step].y*s+multi[step].yb*c)*longfact[step];
	  double tmp=dc*c-ds*s;
	  s=ds*c+dc*s;
	  c=tmp;
        }
        multi[step].x  += pos.x;
        multi[step].y  += pos.y;
        multi[step].xb += pos.xb;
        multi[step].yb += pos.yb;
        z_mode+=d_z;
      }
      
      for (int j=0;j<beam->slices_per_bunch;j++) {
	//apply wakefield kick to the beam
        for (int i_m=0;i_m<beam->particle_number[j+k*beam->slices_per_bunch];i_m++) {
          PARTICLE &particle=beam->particle[imacro2];
	  particle.xp+=kick[j].x*factor/particle.energy;
	  particle.yp+=kick[j].y*factor/particle.energy;
          imacro2++;
        }
#ifdef HTGEN
	//apply wakefield kick to the halo
	for (int i_h=0;i_h<beam->particle_number_sec[j+k*beam->slices_per_bunch];i_h++) { 
	  PARTICLE &particle=beam->particle_sec[imacro3];
	  particle.xp+=kick[j].x*factor/particle.energy;
	  particle.yp+=kick[j].y*factor/particle.energy;
	  imacro3++; 
	}
#endif
      }
    }
  }
}

void dipole_kick(ELEMENT *element, WAKE_DATA *wake_data, BEAM *beam )
{
  // if number of macroparticles per slice is bigger than one call dipole_kick_n
  // for more macroparticles per slice slice position rho_x, rho_y needs to be calculated
  if (beam->macroparticles>1) {
    dipole_kick_n(element, wake_data, beam);
    return;
  }
  // number of macroparticles per slice = 1
  double v_tesla;
  if (CAVITY *cavity=element->cavity_ptr()) {
    v_tesla = cavity->get_v_tesla();
  } else {
    v_tesla = 1.0;
  }
  // number of bunches = 1
 if (beam->bunches==1) {
    double *p, factor;
    if (CAVBPM* cavbpm = element->cavbpm_ptr()) {
      p=cavbpm->get_field()[0].kick;
      factor = 1.; // the length has already been integrated over for cavity BPM.
      }
    else {
      p=beam->field[0].kick;
      factor=beam->transv_factor*element->get_length()*beam->factor;
    }
    double *rho_x=beam->rho_x[0];
    double *rho_y=beam->rho_y[0];
#ifdef HTGEN
    int imacro3=0; //haloparticles
#endif
    for (int j=0;j<beam->slices_per_bunch;j++) {
      rho_x[j]=beam->particle[j].x*beam->particle[j].wgt;
      rho_y[j]=beam->particle[j].y*beam->particle[j].wgt;
      double sumx=0.0;
      double sumy=0.0;
      for (int i=0;i<=j;i++) {
	sumx+=rho_x[i] * *p;
	sumy+=rho_y[i] * *p;
	p++;
      }
      beam->particle[j].xp+=sumx*factor/beam->particle[j].energy;
      beam->particle[j].yp+=sumy*factor/beam->particle[j].energy;
#ifdef HTGEN
      for (int i_h=0;i_h<beam->particle_number_sec[j];i_h++) { 
	PARTICLE &particle=beam->particle_sec[imacro3];
	particle.xp+=sumx*factor/particle.energy;
	particle.yp+=sumy*factor/particle.energy;
	imacro3++; 
      }
#endif
    }
  } else {// number of bunches > 1
    if (!wake_data) {
      placet_cout << ERROR << "wake data not defined, do InjectorCavityDefine (before lattice definition)" << endmsg;
      return;
    }
     /* Initialise wakefields */
    double *rho_x=beam->rho_x[0];
    double *rho_y=beam->rho_y[0];
    struct {
      double xp;
      double yp;
    } rho[beam->slices_per_bunch];
    struct {
      double x;
      double y;
    } kick[beam->slices_per_bunch];
    int nstep=wake_data->n;
    struct {
      double x;
      double y;
      double xb;
      double yb;
    } multi[nstep];
#ifdef EQUAL_SLICE
    struct {
      double s,c;
    } d0[nstep];
#endif  
    double xz[nstep];
    double yz[nstep];
    if (CAVITY *cavity=element->cavity_ptr()) {
      cavity->fill_z_mode_xy(xz,yz,nstep);
    } else {
      for (int i=0;i<nstep;i++) {
        xz[i]=0.0;
        yz[i]=0.0;
      }
    }
    double longfact[nstep];
    INJECT_BEAM_PARAM *param=beam->drive_data->param.inject;
    for (int step=0;step<nstep;step++) {
      multi[step].x=0.0;
      multi[step].y=0.0;
      multi[step].xb=0.0;
      multi[step].yb=0.0;
      longfact[step]=wake_data->a0[step]*(param->charge*ECHARGE*1e12)*1e-6*1e-3;
#ifdef EQUAL_SLICE
      sincos(v_tesla*(beam->z_position[1]-beam->z_position[0])*TWOPI*1e-6/wake_data->lambda[step],&d0[step].s,&d0[step].c);
#endif
    }
    /* loop over all bunches ****/
    double d_z=element->get_length()/nstep;
#ifdef HTGEN
    int imacro3=0; //haloparticles
#endif
    for (int k=0;k<beam->bunches;k++) {
      /* apply attenuation ***/
      if (k>0) {
	for (int step=0;step<nstep;step++) {
	  double tmp=exp(-PI*(param->bunch[k].z-param->bunch[k-1].z)*1e-6/(wake_data->lambda[step]*wake_data->Q[step]));
	  multi[step].x *=tmp;
	  multi[step].y *=tmp;
	  multi[step].xb*=tmp;
	  multi[step].yb*=tmp;
	}
      }
      double *p, factor;
      if (CAVBPM* cavbpm = element->cavbpm_ptr()) {
	p=cavbpm->get_field()[0].kick;
	factor = 1.; // the length has already been integrated over for cavity BPM.
      }
      else {
	p=beam->field[0].kick;
	factor=beam->transv_factor*element->get_length()*beam->factor;
      }
      double w[beam->slices_per_bunch];
      int m=k*beam->slices_per_bunch;
      for (int j=0;j<beam->slices_per_bunch;j++) {
	//nanu
	w[j]=beam->particle[m+j].wgt;
	rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
	rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
#ifdef CAV_MULTI_ANGLE
	rho[j].xp=beam->particle[m+j].xp*beam->particle[m+j].wgt;
	rho[j].yp=beam->particle[m+j].yp*beam->particle[m+j].wgt;
#endif
	double sumx=0.0;
	double sumy=0.0;
	for (int i=0;i<=j;i++) {
	  sumy+=rho_y[i] * *p;
	  sumx+=rho_x[i] * *p;
	  p++;
	}
	kick[j].x=sumx;
	kick[j].y=sumy;
      }
      struct {
        double x;
        double y;
        double xb;
        double yb;
      } pos;
      double z_mode=-0.5*(element->get_length()-d_z);
      for (int step=0;step<nstep;step++) {
	pos.x=0.0;
	pos.y=0.0;
	pos.xb=0.0;
	pos.yb=0.0;
	double c,s;
	sincos(v_tesla*beam->z_position[m]*TWOPI*1e-6/wake_data->lambda[step],&s,&c);
#ifdef EQUAL_SLICE
	double ds=d0[step].s;
	double dc=d0[step].c;
#else
	double ds, dc;
	sincos(v_tesla*(beam->z_position[m+1]-beam->z_position[m])*TWOPI*1e-6/wake_data->wake->lambda[step],&ds,&dc);
#endif
	for (int j=0;j<beam->slices_per_bunch;j++) {
#ifdef CAV_MULTI_ANGLE
	  pos.x  += (rho_x[j]+z_mode*rho[j].xp-w[j]*xz[step])*c;
	  pos.y  += (rho_y[j]+z_mode*rho[j].yp-w[j]*yz[step])*c;
	  pos.xb -= (rho_x[j]+z_mode*rho[j].xp-w[j]*xz[step])*s;
	  pos.yb -= (rho_y[j]+z_mode*rho[j].yp-w[j]*yz[step])*s;
#else
	  pos.x  += rho_x[j]*c;
	  pos.y  += rho_y[j]*c;
	  pos.xb -= rho_x[j]*s;
	  pos.yb -= rho_y[j]*s;
#endif
	  kick[j].x+=(multi[step].x*s+multi[step].xb*c)*longfact[step];
	  kick[j].y+=(multi[step].y*s+multi[step].yb*c)*longfact[step];
	  double tmp=dc*c-ds*s;
	  s=ds*c+dc*s;
	  c=tmp;
	}
	multi[step].x += pos.x;
	multi[step].y += pos.y;
	multi[step].xb += pos.xb;
	multi[step].yb += pos.yb;
	z_mode+=d_z;
      }
      for (int j=0;j<beam->slices_per_bunch;j++) {
	//apply wakefield kick to the beam
        beam->particle[m+j].yp+=kick[j].y*factor/beam->particle[m+j].energy;
	beam->particle[m+j].xp+=kick[j].x*factor/beam->particle[m+j].energy;
#ifdef HTGEN
	//apply wakefield kick to haloparticles
	for (int i_h=0;i_h<beam->particle_number_sec[j+k*beam->slices_per_bunch];i_h++) { 
	  PARTICLE &particle=beam->particle_sec[imacro3];
	  particle.xp+=kick[j].x*factor/particle.energy;
	  particle.yp+=kick[j].y*factor/particle.energy;
	  imacro3++; 
	}
#endif
      }
    }
  }
}

#undef CAV_MULTI_ANGLE
#undef EQUAL_SLICE
