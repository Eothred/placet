#include "cavbpm.h"

#include <string>

#include "dipole_kick.h"
//#include "wakekick.h"
#include "short_range.h"
#include "structures_def.h"
#include "placet_tk.h"

extern WAKEFIELD_DATA_STRUCT wakefield_data;

CAVBPM::CAVBPM(int &argc, char **argv, unsigned int ki, double * ch) : BPM(), kicks(ki), wake_data(NULL)
{

  attributes.add("wakelong", "Name of the longrange wakefield [STRING]", OPT_STRING, this, 
		 Set(&CAVBPM::set_wake_long),
		 Get(&CAVBPM::get_wake_long));
  // only for help message:
  // attributes.add("wakekicks", "List of wakefield kicks [urad GeV / um] for each particle to itself and those behind it{k_{00}, k_{01} ... k_{0n}, k_{11}, etc.}", OPT_NONE, this);
 
  set_attributes(argc, argv);
  
  if (kicks==0) {wakefield=NULL;}
  else {
    wakefield = new FIELD;
    wakefield->kick = new double [kicks];
    for (unsigned int k=0; k<kicks; k++){
      wakefield->kick[k] = ch[k];
    }
  }
}

CAVBPM::~CAVBPM() {
  if (wakefield) {
    delete[] wakefield->kick;
    delete wakefield;
  }
}

void CAVBPM::set_wake_long(const char *name )
{
  if (name) {
    wake_data=get_wake(name);
  }
}

std::string CAVBPM::get_wake_long() const { return wake_data ? wake_data->name : "[NONE]"; }

void CAVBPM::step_4d(BEAM *beam )
{
  /**
     Move beam to the centre of the structure
  */
  drift_step_4d(beam,0.5);

  /**
     Get BPM reading
  */
  calculate_bpm_reading(beam);
  
  /**
     Move beam to the end of the structure
  */
  drift_step_4d(beam,0.5);
}

void CAVBPM::step_4d_0(BEAM *beam)
{
  /**
     Move beam to the centre of the structure
  */
  drift_step_4d_0(beam,0.5);

  /**
     Get BPM reading
  */
  calculate_bpm_reading(beam);

  /**
     Move beam to the end of the structure
  */
  drift_step_4d_0(beam,0.5);
}

void CAVBPM::apply_short_range_wakefield(BEAM* beam) {
  if (short_range.wake || !short_range.name.empty()) {
    ELEMENT::apply_short_range_wakefield(beam);
  } else if (wakefield) {
    apply_wakefield_field(beam);
  }
}

void CAVBPM::apply_wakefield_field(BEAM* beam) {
  
  int nbunch = beam->slices_per_bunch;
  unsigned int triangle = nbunch*(nbunch+1)/2;
  
  if (kicks>0 && kicks<triangle){
    double* tmp_kick = new double [triangle];
    for (unsigned int k=0; k<triangle; k++){
      if (k<kicks) tmp_kick[k] = wakefield->kick[k];
      else tmp_kick[k] = 0.;
    }
    delete[] wakefield->kick;
    wakefield->kick = tmp_kick;
    kicks = triangle;
  }
  
  //  printf('%g \n', modes[0]);
  //   if(wakefield_data.transv) {
  //     if ((beam->which_field==1)||
  //         (beam->which_field==3)||
  //         (beam->which_field==4)||
  //         (beam->which_field==123)) {
  //       wake_kick(this,beam,geometry.length);
  //     } else {
  dipole_kick(this,wake_data,beam);
  //     }
  //   }
}
