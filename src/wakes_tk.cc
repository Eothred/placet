#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include "structures_def.h"
#include "short_range.h"
#include "spline.h"

extern Tcl_HashTable ShortRangeTable;

int
tk_ShortRangeWake(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error, type=1;
  char *tabx=NULL,*taby=NULL,*tabz=NULL;
  double wake_bunch_length=0.0;
  SPLINE *sx=NULL,*sy=NULL,*sz=NULL;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: ShortRangeWake name (use empty string \"\" for default cavity wakename)"},
    {(char*)"-wx",TK_ARGV_STRING,(char*)NULL,
     (char*)&tabx,
     (char*)"Name of horizontal wakefield (defined with SplineCreate) in [V/m^2C]; [V/mm/pC] vs [m] for elements"},
    {(char*)"-wy",TK_ARGV_STRING,(char*)NULL,
     (char*)&taby,
     (char*)"Name of vertical wakefield (defined with SplineCreate) in [V/m^2C]; [V/mm/pC] vs [m] for elements"},
    {(char*)"-wz",TK_ARGV_STRING,(char*)NULL,
     (char*)&tabz,
     (char*)"Name of longitudinal wakefield (defined with SplineCreate) [V/mC] for cavities; [V/pC] vs [m] for elements"},
    {(char*)"-type",TK_ARGV_INT,(char*)NULL,
     (char*)&type,
     (char*)"0: kick calculated with small kicks, 1: full bunch (default) 2: convolved wakes"},
    {(char*)"-wake_bunch_length",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&wake_bunch_length,
     (char*)"Simulated bunch length in splines [um] (only valid in combination with 'type' == 0)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_SetResult(interp,"Must give exactly one name in ShortRangeWake",TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (tabx) {
    sx=get_spline(tabx);
  }
  if (taby) {
    sy=get_spline(taby);
  }
  if (tabz) {
    sz=get_spline(tabz);
  }
  
  // create hash entry for table
  int ok;
  Tcl_HashEntry* entry=Tcl_CreateHashEntry(&ShortRangeTable,argv[1],&ok);
  if (!ok) {
    return TCL_ERROR;
  }

  SHORT_RANGE* short_range = new SHORT_RANGE(sx,sy,sz,type,wake_bunch_length);
  Tcl_SetHashValue(entry,short_range);
  return TCL_OK;
}
