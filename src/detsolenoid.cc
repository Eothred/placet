#include "detsolenoid.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cctype>
#include <unistd.h>
#include "placet.h"
#include "beam.h"

/**
 * Change the units of vector vec by the factor,
 * length is the number of elements in vec.
 */
void change_vector_units(double *vec, double factor,int length) {
    for (int i=0;i<length;i++) vec[i]*=factor;
}

void DetectorSolenoid::rotate_in(double* vec, double energy){
    // If we are tracking from IP and outwards, crossing angle should have 
    // opposite sign as per our convention...
      // electrons arriving from the left: negative z, positive x
      // positrons arriving from the right: positive z, positive x
      // approximation: all particles same z at each step  
    int charge = (energy<0) ? 1:-1;
    double tmp=vec[2]*cos_xangle + vec[0]*sin_xangle;
    vec[0]=charge*(vec[2]*sin_xangle-vec[0]*cos_xangle);
    vec[2]=tmp;
}

void DetectorSolenoid::rotate_out(double* vec, double energy){
    // If we are tracking from IP and outwards, crossing angle should have 
    // opposite sign as per our convention...
    int charge = (energy<0) ? 1:-1;
    double tmp = vec[2]*cos_xangle +charge*vec[0]*sin_xangle;
    vec[0]=vec[2]*sin_xangle - charge*vec[0]*cos_xangle;
    vec[2]=tmp;
}

//______________________________________
DetectorSolenoid::DetectorSolenoid(double crossing_angle)
{
 z.reserve(200);
 z_i=0;
 bz.reserve(200);
 br.reserve(200);
 bz_dz.reserve(200);

 xangle=crossing_angle;
 cos_xangle=cos(xangle);
 sin_xangle=sin(xangle);
}
//______________________________________
DetectorSolenoid::~DetectorSolenoid()
{
  // destructor
}

/**
 * Linear interpolation, returns b(z)
 * assumes that z1 < z < z2
 */
double GetBValue(double b1,double b2,double z1,double z2,double z) {
    if (z1>z2) placet_cout << ERROR << "Wrong usage of function GetBValue" << endmsg;
    if (z2==z1) {
        if (b1!=b2) placet_cout << ERROR << "Two different field values at same location!" << endmsg;
        return b2;
    }
    return ((z-z1)*b2+(z2-z)*b1)/(z2-z1);
}

//_____________________________________
int DetectorSolenoid::GetMapField1D(PARTICLE* particle, double posz, double int_length, double* bfield)
{  
  // necessary to copy this into a private variable because of parallelization.
  // There might be safer ways to do this?
  unsigned int _z_i = z_i;
  if(z.size()>1){ // if the vector of the map exists
    double v[3]={particle->x,particle->y,posz};

    int charge = (particle->energy < 0) ? (1):(-1); // positrons have negative energy..

    // rotate the particle position in the Map Reference System
    rotate_in(v,particle->energy);
    change_vector_units(v,1e-6,3);
    posz=fabs(v[2]); // we need this value in m from here on...

    std::size_t zsize=z.size();
    
    // Checking if the index should be changed..
    while(_z_i<zsize) {
        if (z[_z_i]>posz) break;
            _z_i++;
    }
    // This might happen if two particles are in two different "bins"
    while (_z_i>0 && z[_z_i-1]>posz) _z_i--;

    double brtemp,bztemp;
    if(_z_i==zsize) {
        brtemp=br[_z_i-1];
        bztemp=bz[_z_i-1];
    } else if(_z_i==0) {
        brtemp=br[_z_i];
        bztemp=bz[_z_i];
    } else {
        brtemp=GetBValue(br[_z_i-1],br[_z_i],z[_z_i-1],z[_z_i],posz);
        bztemp=GetBValue(bz[_z_i-1],bz[_z_i],z[_z_i-1],z[_z_i],posz);
    }

    z_i=_z_i;

    bfield[0]=-v[0]*brtemp;
    bfield[1]=charge*v[1]*brtemp;
    bfield[2]=-charge*bztemp;

    // rotate the field in the Beamline Reference System
    rotate_out(bfield,particle->energy);
  }

  return 0;
}
//_____________________________________
int DetectorSolenoid::ReadMapFile1D(char *filename)
{
  double _x,_y,_z,_bx,_by,_bz;
  std::cin.precision(15);
  std::ifstream in(filename);
  if(!in ){
    placet_cout << ERROR << "DetectorSolenoid:: Cannot open " << filename << endmsg;
    return 1;
  }
  // In the solenoid reference system
  // Map file description: x [m] y [m]  z [m]  Bx [T] By [T] Bz [T]
  // WARNING: for now we only use Br = sqrt(Bx**2+By**2)
  while(in >> _x >> _y >> _z >> _bx >> _by >> _bz) {
    z.push_back(fabs(_z)); bz.push_back(_bz);
    if((_bx !=0 && _by!=0) || (_x !=0 && _y!=0) ) {
      placet_cout << ERROR << "IRtracking module does not support this map style yet" << endmsg;
      return 1;
    }
    // NOTE: at this point we have either Bx OR By, not both
    if(_x!=0) br.push_back( _bx/_x );
    else if(_y!=0) br.push_back( _by/_y );
    else br.push_back(_bx+_by);
  }
  in.close();
  return 0;
}
