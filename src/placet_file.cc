#include "placet_file.h"
#include "placet_cout.hh"

FILE* open_file (const char* name, bool binary) {
  FILE* file;
  if (name) {
    if (binary) {
      file=fopen(name,"wb");
    } else {
      file=fopen(name,"w");
    }
    if (!file) {
      placet_cout << ERROR << "File: " << name << " could not be opened" << endmsg;
      exit(1);
    }
  } else {
    file=stdout;
  }
  return file;
}

FILE* append_file (const char* name, bool binary) {
  FILE* file;
  if (name) {
    if (binary) {
      file=fopen(name,"ab");
    } else {
      file=fopen(name,"a");
    }
    if (!file) {
      placet_cout << ERROR << "File: " << name << " could not be opened" << endmsg;
      exit(1);
    }
  } else {
    file=stdout;
  }
  return file;
}

FILE* read_file (const char* name, bool binary) {
  FILE* file;
  if (name) {
    if (binary) {
      file=fopen(name,"rb");
    } else {
      file=fopen(name,"r");
    }
    if (!file) {
      placet_cout << ERROR << "No such file: " << name << endmsg;
      exit(1);
    }
  } else {
    file=stdin;
  }
  return file;
}

void close_file (FILE* file) {
  if (file && file!=stdout && file!=stdin && file!=stderr) fclose(file);
}

void skipword(FILE *f) {
  int i;
  while ((i = fgetc(f)) != EOF) {
    if (i != ' ' && i != '\t' && i != '\n')
      break;
  }
  while ((i = fgetc(f)) != EOF) {
    if (i == ' ' || i == '\t' || i == '\n')
      break;
  }
}

void skipcurrentline(FILE *f) {
  int i;
  while ((i = fgetc(f)) != EOF) {
    if (i == '\n')
      break;
  }
}

/// close file f with name fname and print error message
void file_too_short(FILE* f, char* fname) {
  close_file(f);
  placet_cout << ERROR << "file " << fname << " is too short" << endmsg;
}

/// read next n doubles into array d from file f with name fname
bool file_read_doubles(FILE* f, char* fname, double* d, unsigned int n) {
  if (fread(d,sizeof(double),n,f)!=n) {
    file_too_short(f,fname);
    return false;
  }
  return true;
}
