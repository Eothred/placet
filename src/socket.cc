#include "socket.hh"

SocketAddress::SocketAddress(unsigned short port )
{
  sin_family = AF_INET;
  sin_port = htons(port);
  sin_addr.s_addr = htonl(INADDR_ANY);
  memset(sin_zero, 0, 8);
}

SocketAddress::SocketAddress(const in_addr &address, unsigned short port )
{
  sin_family = AF_INET;
  sin_port = htons(port);
  sin_addr = address;
  memset(sin_zero, 0, 8);
}

SocketAddress::SocketAddress(unsigned long address, unsigned short port )
{
  sin_family = AF_INET;
  sin_port = htons(port);
  sin_addr.s_addr = htonl(address);
  memset(sin_zero, 0, 8);
}

SocketAddress::SocketAddress(const char *hostname, unsigned short port )
{
  hostent *h = gethostbyname(hostname);
  sin_family = AF_INET;
  sin_port = htons(port);
  sin_addr.s_addr = h != NULL ? ((in_addr*)h->h_addr)->s_addr : inet_addr(hostname);
  memset(sin_zero, 0, 8);
}
			
SocketAddress TCP_Socket::get_peername() const 
{
  SocketAddress result;
  socklen_t len = sizeof(result);
  getpeername(sd, (sockaddr*)&result, &len);
  return result;
}

void TCP_Socket::close()
{ 
  shutdown(sd, SHUT_RDWR);
  sd = -1; 
}

bool TCP_Socket::connect(const SocketAddress &remote, int type )
{
  if (sd != -1) ::close(sd);
  if ((sd = socket(AF_INET, type, 0)) != -1) {
    if (::connect(sd, (const sockaddr*)&remote, sizeof(sockaddr)) == 0)
      return true;
    close();
  }
  return false;
}

bool TCP_Socket::bind(unsigned int _port )
{
  if (sd != -1) ::close(sd);
  if ((sd = socket(AF_INET, SOCK_STREAM, 0)) != -1) {
    SocketAddress local(_port);
    if (::bind(sd, (sockaddr*)&local, sizeof(sockaddr)) == 0)
      return true;
    close();
  }
  return false;
}

bool TCP_Socket::bind(int _type, unsigned int _port )
{
  if (sd != -1) ::close(sd);
  if ((sd = socket(AF_INET, _type, 0)) != -1) {
    SocketAddress local(_port);
    if (::bind(sd, (sockaddr*)&local, sizeof(sockaddr)) == 0)
      return true;
    close();
  }
  return false;
}

bool TCP_Socket::bind(const SocketAddress &local )
{
  if (sd != -1) ::close(sd);
  if ((sd = socket(AF_INET, SOCK_STREAM, 0)) != -1) {
    // lose the pesky "Address already in use" error message 
    if (::bind(sd, (const sockaddr*)&local, sizeof(sockaddr)) == 0)
      return true;
    close();
  }
  return false;
}

bool TCP_Socket::bind(int _type, const SocketAddress &local )
{
  if (sd != -1) ::close(sd);
  if ((sd = socket(AF_INET, _type, 0)) != -1) {
    if (::bind(sd, (sockaddr*)&local, sizeof(sockaddr)) == 0)
      return true;
    close();
  }
  return false;
}

bool TCP_Socket::listen(int _backlog )
{
  return ::listen(sd, _backlog) == 0;
}

TCP_Socket TCP_Socket::accept() const
{
  TCP_Socket new_socket;
  new_socket.sd = ::accept(sd, NULL, 0);
  return new_socket;
}

TCP_Socket TCP_Socket::accept(SocketAddress &remote ) const
{
  TCP_Socket new_socket;
  socklen_t len = sizeof(sockaddr);
  new_socket.sd = ::accept(sd, (sockaddr*)&remote, &len);
  return new_socket;
}

std::list<TCP_Socket>::iterator SocketSet::is_set(std::list<TCP_Socket>::iterator start, const std::list<Socket>::iterator &stop ) const
{
  while(start != stop) {
    if (FD_ISSET((*start).sd, const_cast<fd_set*>(&set)))
      break;
    start++;
  }
  return start;
}
