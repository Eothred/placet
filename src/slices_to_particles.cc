#include <cstdlib>
#include <cmath>
#include <tcl.h>
#include <tk.h>
#include <vector>

#include "placet.h"
#include "structures_def.h"

#include "beam.h"
#include "lattice.h"
#include "girder.h"
#include "select.h"
#include "slices_to_particles.h"
#include "random.hh"


extern INTER_DATA_STRUCT inter_data;

void slices_to_particles(BEAM *b,int dist)
{ 
  int i;
  PARTICLE *p,p0;
  R_MATRIX rxx,ryy,rxy;
  double er,zr;
  int ie1,ie2,iz1,iz2,ie,iz;
  double wgte,wgtz;
  double sum=0.0;
 
  if (b->particle_beam==true) {
    placet_cout << ERROR << "in slices_to_particles: this is not a sliced beam" << endmsg;
    exit(1);
  }
  // // 7681 is default seed for this generator!
  // if (seed!=7681) {
  //   placet_cout << ERROR << "Please use RandomReset to set and reset seeds" << endmsg;
  //   exit(1);
  // }
  
  //if(seed!=0) Select.SetSeed(seed);
 
  int n=b->slices;
  int *nc=b->particle_number_tmp;
  b->particle_number_tmp=b->particle_number;
  b->particle_number=nc;
  nc=new int[n]();
  std::vector<double> weights;

  for(i =0; i<n; i++)
  {
    weights.push_back(b->particle[i].wgt);
    sum+=b->particle[i].wgt;
  }
  sum/=b->n_max;
  
  Select.DiscreteDistributionSet(weights); 
  for (i=0;i<b->n_max;++i){
    n=Select.DiscreteDistribution();
    nc[n]++;
  }
 
  n=0;
  for(i=0;i<b->slices_per_bunch*b->bunches;++i) b->particle_number[i]=0;
  switch (dist) {
  case 0:
    for(i=0;i<b->slices;i++){
      for(int j=0;j<nc[i];++j){
	one_particle(b->sigma_xx+i,b->sigma_xy+i,b->sigma+i,b->particle+i,
		     b->p_tmp+n);
	b->p_tmp[n].wgt=sum;
	b->p_tmp[n].z=b->z_position[i/b->macroparticles];
	n++;
      }
      b->particle_number[i/b->macroparticles]+=nc[i];
    }
    break;
  case 1:
    i=0;
    for(iz=0;iz<b->slices/b->macroparticles;iz++){
      for(ie=0;ie<b->macroparticles;ie++){
	for (int j=0;j<nc[i];++j){
	  er=Select.Uniform();
	  if (er<0.5) {
	    ie1=ie-1;
	    ie2=ie;
	    wgte=0.5-er;
	    if (ie1<0) ie1=0;
	  }
	  else {
	    ie1=ie;
	    ie2=ie+1;
	    wgte=1.5-er;
	    if (ie2>=b->macroparticles) ie2=b->macroparticles-1;
	  }
	  zr=Select.Uniform();
	  if (zr<0.5) {
	    iz1=iz-1;
	    iz2=iz;
	    wgtz=0.5-zr;
	    if (iz1<0) iz1=0;
	  }
	  else {
	    iz1=iz;
	    iz2=iz+1;
	    wgtz=1.5-zr;
	    if (iz2>=b->slices/b->macroparticles) iz2=b->slices/b->macroparticles-1;
	  }
	  rxx.r11=b->sigma_xx[iz1*b->macroparticles+ie1].r11
	    *(wgte*wgtz)
	    +b->sigma_xx[iz1*b->macroparticles+ie2].r11
	    *((1.0-wgte)*wgtz)
	    +b->sigma_xx[iz2*b->macroparticles+ie1].r11
	    *(wgte*(1.0-wgtz))
	    +b->sigma_xx[iz2*b->macroparticles+ie2].r11
	    *((1.0-wgte)*(1.0-wgtz));
	  rxx.r12=b->sigma_xx[iz1*b->macroparticles+ie1].r12
	    *(wgte*wgtz)
	    +b->sigma_xx[iz1*b->macroparticles+ie2].r12
	    *((1.0-wgte)*wgtz)
	    +b->sigma_xx[iz2*b->macroparticles+ie1].r12
	    *(wgte*(1.0-wgtz))
	    +b->sigma_xx[iz2*b->macroparticles+ie2].r12
	    *((1.0-wgte)*(1.0-wgtz));
	  rxx.r21=b->sigma_xx[iz1*b->macroparticles+ie1].r21
	    *(wgte*wgtz)
	    +b->sigma_xx[iz1*b->macroparticles+ie2].r21
	    *((1.0-wgte)*wgtz)
	    +b->sigma_xx[iz2*b->macroparticles+ie1].r21
	    *(wgte*(1.0-wgtz))
	    +b->sigma_xx[iz2*b->macroparticles+ie2].r21
	    *((1.0-wgte)*(1.0-wgtz));
	  rxx.r22=b->sigma_xx[iz1*b->macroparticles+ie1].r22
	    *(wgte*wgtz)
	    +b->sigma_xx[iz1*b->macroparticles+ie2].r22
	    *((1.0-wgte)*wgtz)
	    +b->sigma_xx[iz2*b->macroparticles+ie1].r22
	    *(wgte*(1.0-wgtz))
	    +b->sigma_xx[iz2*b->macroparticles+ie2].r22
	    *((1.0-wgte)*(1.0-wgtz));
	  rxy.r11=b->sigma_xy[iz1*b->macroparticles+ie1].r11
	    *(wgte*wgtz)
	    +b->sigma_xy[iz1*b->macroparticles+ie2].r11
	    *((1.0-wgte)*wgtz)
	    +b->sigma_xy[iz2*b->macroparticles+ie1].r11
	    *(wgte*(1.0-wgtz))
	    +b->sigma_xy[iz2*b->macroparticles+ie2].r11
	    *((1.0-wgte)*(1.0-wgtz));
	  rxy.r12=b->sigma_xy[iz1*b->macroparticles+ie1].r12
	    *(wgte*wgtz)
	    +b->sigma_xy[iz1*b->macroparticles+ie2].r12
	    *((1.0-wgte)*wgtz)
	    +b->sigma_xy[iz2*b->macroparticles+ie1].r12
	    *(wgte*(1.0-wgtz))
	    +b->sigma_xy[iz2*b->macroparticles+ie2].r12
	    *((1.0-wgte)*(1.0-wgtz));
	  rxy.r21=b->sigma_xy[iz1*b->macroparticles+ie1].r21
	    *(wgte*wgtz)
	    +b->sigma_xy[iz1*b->macroparticles+ie2].r21
	    *((1.0-wgte)*wgtz)
	    +b->sigma_xy[iz2*b->macroparticles+ie1].r21
	    *(wgte*(1.0-wgtz))
	    +b->sigma_xy[iz2*b->macroparticles+ie2].r21
	    *((1.0-wgte)*(1.0-wgtz));
	  rxy.r22=b->sigma_xy[iz1*b->macroparticles+ie1].r22
	    *(wgte*wgtz)
	    +b->sigma_xy[iz1*b->macroparticles+ie2].r22
	    *((1.0-wgte)*wgtz)
	    +b->sigma_xy[iz2*b->macroparticles+ie1].r22
	    *(wgte*(1.0-wgtz))
	    +b->sigma_xy[iz2*b->macroparticles+ie2].r22
	    *((1.0-wgte)*(1.0-wgtz));
	  ryy.r11=b->sigma[iz1*b->macroparticles+ie1].r11
	    *(wgte*wgtz)
	    +b->sigma[iz1*b->macroparticles+ie2].r11
	    *((1.0-wgte)*wgtz)
	    +b->sigma[iz2*b->macroparticles+ie1].r11
	    *(wgte*(1.0-wgtz))
	    +b->sigma[iz2*b->macroparticles+ie2].r11
	    *((1.0-wgte)*(1.0-wgtz));
	  ryy.r12=b->sigma[iz1*b->macroparticles+ie1].r12
	    *(wgte*wgtz)
	    +b->sigma[iz1*b->macroparticles+ie2].r12
	    *((1.0-wgte)*wgtz)
	    +b->sigma[iz2*b->macroparticles+ie1].r12
	    *(wgte*(1.0-wgtz))
	    +b->sigma[iz2*b->macroparticles+ie2].r12
	    *((1.0-wgte)*(1.0-wgtz));
	  ryy.r21=b->sigma[iz1*b->macroparticles+ie1].r21
	    *(wgte*wgtz)
	    +b->sigma[iz1*b->macroparticles+ie2].r21
	    *((1.0-wgte)*wgtz)
	    +b->sigma[iz2*b->macroparticles+ie1].r21
	    *(wgte*(1.0-wgtz))
	    +b->sigma[iz2*b->macroparticles+ie2].r21
	    *((1.0-wgte)*(1.0-wgtz));
	  ryy.r22=b->sigma[iz1*b->macroparticles+ie1].r22
	    *(wgte*wgtz)
	    +b->sigma[iz1*b->macroparticles+ie2].r22
	    *((1.0-wgte)*wgtz)
	    +b->sigma[iz2*b->macroparticles+ie1].r22
	    *(wgte*(1.0-wgtz))
	    +b->sigma[iz2*b->macroparticles+ie2].r22
	    *((1.0-wgte)*(1.0-wgtz));
	  p0.x=b->particle[iz1*b->macroparticles+ie1].x*(wgte*wgtz)
	    +b->particle[iz1*b->macroparticles+ie2].x*((1.0-wgte)*wgtz)
	    +b->particle[iz2*b->macroparticles+ie1].x*(wgte*(1.0-wgtz))
	    +b->particle[iz2*b->macroparticles+ie2].x*((1.0-wgte)*(1.0-wgtz));
	  p0.xp=b->particle[iz1*b->macroparticles+ie1].xp*(wgte*wgtz)
	    +b->particle[iz1*b->macroparticles+ie2].xp*((1.0-wgte)*wgtz)
	    +b->particle[iz2*b->macroparticles+ie1].xp*(wgte*(1.0-wgtz))
	    +b->particle[iz2*b->macroparticles+ie2].xp*((1.0-wgte)*(1.0-wgtz));
	  p0.y=b->particle[iz1*b->macroparticles+ie1].y*(wgte*wgtz)
	    +b->particle[iz1*b->macroparticles+ie2].y*((1.0-wgte)*wgtz)
	    +b->particle[iz2*b->macroparticles+ie1].y*(wgte*(1.0-wgtz))
	    +b->particle[iz2*b->macroparticles+ie2].y*((1.0-wgte)*(1.0-wgtz));
	  p0.yp=b->particle[iz1*b->macroparticles+ie1].yp*(wgte*wgtz)
	    +b->particle[iz1*b->macroparticles+ie2].yp*((1.0-wgte)*wgtz)
	    +b->particle[iz2*b->macroparticles+ie1].yp*(wgte*(1.0-wgtz))
	    +b->particle[iz2*b->macroparticles+ie2].yp*((1.0-wgte)*(1.0-wgtz));
	  p0.energy=b->particle[iz1*b->macroparticles+ie1].energy*(wgte*wgtz)
	    +b->particle[iz1*b->macroparticles+ie2].energy*((1.0-wgte)*wgtz)
	    +b->particle[iz2*b->macroparticles+ie1].energy*(wgte*(1.0-wgtz))
	    +b->particle[iz2*b->macroparticles+ie2].energy*((1.0-wgte)*(1.0-wgtz));
	  p0.z=b->z_position[iz1]*wgtz+b->z_position[iz2]*(1.0-wgtz);

	  one_particle(&rxx,&rxy,&ryy,&p0,b->p_tmp+n);
	  b->p_tmp[n].wgt=sum;
	  b->p_tmp[n].z=p0.z;
	  n++;
	}
	b->particle_number[i/b->macroparticles]+=nc[i];
	i++;
      }
    }
    break;
  default: 
    placet_cout << WARNING << "slices_to_particles() wrong input for dist parameter" << endmsg;
    break;
  }

  delete[] nc;
  b->particle_beam=true;
  p=b->p_tmp;
  b->p_tmp=b->particle;
  b->particle=p;

  i=b->n_max;
  b->n_max=b->slices;
  b->slices=i;
}

int tk_SlicesToParticles(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  if (element_help_message<S2P>(argc,argv, "This command places a slices to particle element in the current girder.\n\nSlicesToParticles:", interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) return TCL_ERROR;
  S2P* element=new S2P(argc, argv);
  inter_data.girder->add_element(element);
  return TCL_OK;
}

void S2P::step_4d_0(BEAM* beam)
{
  //placet_printf(WARNING,"Warning SlicesToParticles used during correction:\nResults could be wrong\n");  //BD 16/04/2010
  slices_to_particles(beam,dist);
}

void S2P::step_4d(BEAM* beam)
{
//  placet_printf(INFO,"SlicesToParticles\n");
  slices_to_particles(beam,dist);
}

void
S2P::list(FILE* f)const
{
  fprintf(f,"SlicesToParticles\n");
}
