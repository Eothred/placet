#include "irtracking.h"

/// 1/(c*1e-9)
#define invNanoClight 3.3356409520

namespace {
  const double epsilon = std::numeric_limits<double>::epsilon();
}

static int binarySearch(const std::vector<double>v, int first, int last, double x,bool forward_tracking=true) {
//     placet_cout << DEBUG << "binsearch " << first <<", "<< last <<", "<< x <<", "<< v[first] <<", "<< v[last] << endmsg;
    if(forward_tracking) {
        if (x > v[last]) { 
            placet_cout << WARNING << " IRTracking in binarySearch : beam is out of the beamline "<<endmsg;
            exit(1);
        }
        if (x <= v[first] ) return first;
        else if (v[last] == x) return last;
        int mid = (first+last)/2;
        if (x > v[mid]) return binarySearch(v,mid+1,last,x,forward_tracking);
        else return binarySearch(v,first,mid,x,forward_tracking);
    } else {
        if (x < v[first]) return -1; // we're before the first crossing..
        if (x >= v[last] ) return last;
        else if (v[first] == x) return first;
        int mid = (first+last)/2;
        if(first==last) {
            placet_cout << ERROR << "STUPID!!!" << endmsg;
            exit(1);
        }
        if (x < v[mid]) return binarySearch(v,first,mid,x,forward_tracking);
        else return binarySearch(v,mid,last-1,x,forward_tracking);
    }
}

IRTracking::IRTracking(double step, double int_length,std::vector <double > bounds_list, DetectorSolenoid *point)
{
    IRTracking::step=step;
    boundcross=false;
    syn=0;
    backwards_tracking=false;
    sing_trk_stream=NULL;
    synrad_stream=NULL;
    current_int_length=0.0;
    beam=NULL;
    map=NULL;
    SetBoundList(bounds_list);
    SetIntLength(int_length);
    map=point;
    
    placet_cout << VERYVERBOSE << "irtracking.cc: constructor" << endmsg;
}


IRTracking::~IRTracking()
{
    // destructor
    if(sing_trk_stream) sing_trk_stream->close();
    delete sing_trk_stream;
    if(synrad_stream) synrad_stream->close();
    delete synrad_stream;
    // Note, map is not deleted because
    // it might be used multiple times...
    // when number of machines is larger than 1
}

void IRTracking::track(BEAM *beam,BEAMLINE *beamline)
{
    IRTracking::beam=beam;

    // set integration length if it has not yet been done.
    if(!int_length) SetIntLength(beamline->get_length());
    // WARNING: backwards tracking when int_length!=beamline length 
    // has not been debugged yet!
    if(backwards_tracking) {
        current_int_length=int_length;
        beam->s=int_length;
    }

    // We want an integer number of steps...
    SetStep(int_length/(int)(int_length/step));
    placet_cout << VERBOSE << "test_int_region Corrected Step Length: " << step << endmsg;

#ifdef _SOLENOID
    for (unsigned int i=0;i<(unsigned int)beam->slices;i++) beam->particle[i].id=i;
#endif
    if(get_verbosity()>=DEBUG)
        for (int i=0;i<beam->slices;i++)
            placet_cout << DEBUG <<"BEFORE, particle "<<i<<": "<< beam->particle[i] << endmsg;
    bool loop=true;
    double remaining_track_length; // used to check if remaining tracking is less than the step
    int step_error; // used to catch errors from step_particle()
    // the element index where beam currently reside
    int element_index=0;
    // zero length elements must force increase of index
    bool force_element_index=false;
    // WARNING: I think in exeptional cases, zero-length elements can be called twice
    // Not sure how to get around that at the moment
    do {
        if(backwards_tracking) remaining_track_length=current_int_length;
        else remaining_track_length=int_length-current_int_length;
        if(remaining_track_length<=fabs(step)+epsilon) {
	    // in case of backwards tracking, we need the step to be negative
            backwards_tracking ? SetStep(-remaining_track_length) : SetStep(remaining_track_length);
            loop=false;
            placet_cout << DEBUG << "IRTracking track loop ended.. " << current_int_length << " " << int_length << endmsg;
        }
        if(not force_element_index) {
            element_index=0;
            if (beam->s > beamline->element[beamline->n_elements-1]->get_s()) {
                element_index=beamline->n_elements-1;
            } else if (beam->s > beamline->element[0]->get_s()){
                int in = 1;
                while(beam->s > beamline->element[in]->get_s()) in++;
                element_index=in;
            }
        }
        force_element_index=false;
        if( beamline->element[element_index]->is_tclcall() || beamline->element[element_index]->get_length()==0 ) {
            placet_cout << VERBOSE << "irtracking.cc: Not using step_particle for " << beamline->element[element_index]->list() << endmsg;
            track_thin_elements(beam,beamline->element[element_index]);
            // We must force the element index in the next round (otherwise we just loop)
            if (element_index<beamline->n_elements-1) {
                force_element_index=true;
                element_index++;
            }
        }
        else {
            step_error = step_particle(beamline,element_index);
            if(step_error) {
                placet_cout << WARNING << "step_particle() finished unexpectedly..." << endmsg;
                return;
            } // step (tracking)
            beam->s+=step;
        }
    } while(loop);
    if(beamline->element[beamline->n_elements-1]->is_tclcall()) beamline->element[beamline->n_elements-1]->track(beam);
    for (int i=0;i<beam->slices;i++) placet_cout << DEBUG <<"AFTER, particle "<<i<<": "<< beam->particle[i] << endmsg;
}

int check_particle_position(BEAM *beam,PARTICLE particle)
{
    if(sqrt(particle.x*particle.x + particle.y*particle.y)> 10000.) {
        placet_cout << ERROR  << "IRTracking::step_particle offset too large (x,y,s): " << particle.x << " " << particle.y << " " << beam->s << endmsg;
        return 1;
    }
    return 0;
}

// sub-function that moves one particle length step defined in the class, inside element with index index...
// used by step_particle() internally
int IRTracking::part_step_particle(BEAMLINE* beamline, int particle_index, int index)
{
    PARTICLE &particle=beam->particle[particle_index];
    double xp,yp;
    double freepath =Radiation.Exponential();
    xp=particle.xp;
    yp=particle.yp;
    int nphot=0;
    double ene_loss=0.;
//     step_2nd_order(&particle,beamline,index);
    step_4th_order(&particle, particle_index, beamline, index);
    if(check_particle_position(beam,particle)) exit(1);
    if(syn) {
        double ang=sqrt((particle.xp-xp)*(particle.xp-xp)+(particle.yp-yp)*(particle.yp-yp))*1e-6;
        freepath-=1./SYNRAD::get_mean_free_path(ang,1.,particle.energy);
        while (freepath<0.0) {
            double energy_loss =SYNRAD::get_energy_loss(ang,step,particle.energy);
            particle.energy -= energy_loss;
            if(fabs(particle.energy)<epsilon) {
                particle.energy=0.0;
                break;
            }
            freepath+=Radiation.Exponential();
            nphot++;
            ene_loss+=energy_loss;
        }
        if (synrad_stream && nphot > 0 && ene_loss>0) *synrad_stream << current_int_length << " " <<nphot << " " << ene_loss << std::endl;
    }
    return 0;
}

bool IRTracking::ShouldWriteParticle(int particle_id) {
#ifdef _SOLENOID
            return IRTracking::beam->particle[particle_id].id==0;
#else
            return particle_id==IRTracking::beam->slices/2;
#endif
}

int IRTracking::step_particle(BEAMLINE* beamline, int index)
{
    // If at boundary, this is first part of step...
    double first_step=checkboundary();
    if(!GetBoundCross()) { // not at the crossing of an element
        //#pragma omp parallel for num_threads(4)
        for (int i=0; i<beam->slices; i++) { // loop over the particles
            part_step_particle(beamline,i,index);
        }
        current_int_length+=step;
    }
    else {
        double original_step=step;
        SetStep(first_step);
        //#pragma omp parallel for num_threads(4)
        for (int i=0; i<beam->slices; i++) { // loop over the particles
            // first part of the step
            part_step_particle(beamline,i,index);
            // here end an element
        }
        current_int_length+=step;
        int indi;
        if(original_step>0) {
            indi=1;
            while( index+indi<beamline->n_elements && beamline->element[index+indi]->get_length()==0 ) indi+=1; 
        } else {
            indi=-1;
            while( index+indi>0 && beamline->element[index+indi]->get_length()==0 ) indi-=1; 
        }
        if(beamline->n_elements<=index+indi) {
            placet_cout << WARNING << "Got to end of beamline..! " << index << ", " << indi << endmsg;
            return 1;
        }
        if(abs(indi)>1) { // we jumped some zero length elements..
            placet_cout << DEBUG << "IRTracking:: " << abs(indi)-1 << " zero length element(s) after element " << beamline->element[index]->get_name_as_string() << endmsg;
            if (indi>0)
                for (int i =1;i<indi;i++)
                    track_thin_elements(beam,beamline->element[index+i]);
            else
                for (int i =-1;i>indi;i--)
                    track_thin_elements(beam,beamline->element[index+i]);
        }
        SetStep(original_step-step);
        //#pragma omp parallel for num_threads(4)
        for (int i=0; i<beam->slices; i++) { // loop over the particles
            // second part of the step: here starts the following element
            part_step_particle(beamline,i,index+indi);
        }
        current_int_length+=step;
        SetStep(original_step);
    }
    return 0;
}

void IRTracking::track_thin_elements(BEAM* beam,ELEMENT* element) {

    placet_cout << VERYVERBOSE << "IRTracking:: thin lens tracking for " << element->get_name() << endmsg;

    if(backwards_tracking)
    {
        // we need to invert the beam, this is an alternative way to track backwards...
        // by inverting xp/yp before and after the tracking (kick), the effect is the
        // equivalent of inverting the sign of the magnetic field..
        invert_beam_for_backward(beam);
        element->track(beam);
        invert_beam_for_backward(beam);
    }
    else
        element->track(beam);
}

void IRTracking::invert_beam_for_backward(BEAM* beam)
{
    // invert xp/yp of all particles
    // called before and after call to normal track for thin elements
    // when we are doing backwards tracking..
    for(int i=0;i<beam->slices;i++)
    {
        beam->particle[i].xp*=-1;
        beam->particle[i].yp*=-1;
    }
}
//_____________________________________________________________________________________
void IRTracking::step_2nd_order(PARTICLE *particle, BEAMLINE *beamline, int index, double posz)
{
    double xp,yp,zp;
    double tmstep=step/2.;
    double factor;
    double bfield[3] = { 0.0, 0.0, 0.0};

    particle->x+=tmstep*particle->xp;
    particle->y+=tmstep*particle->yp;

    get_posz(particle);

    if ( particle->energy < 0 ) posz-=tmstep*particle->get_zp();
    else  posz+=tmstep*particle->get_zp();	

    particle->z+=tmstep*(particle->xp*particle->xp+particle->yp*particle->yp)*0.5e-6;
    xp=particle->xp; yp=particle->yp; zp=particle->get_zp();
    SetBfield(particle,beamline,index,bfield);
    if(map) {
      //double position = ldim;
      double bf[3];
      map->GetMapField1D(particle,posz,int_length,bf);
      
      factor=1/beamline->element[index]->get_ref_energy();
      bfield[0]+=bf[0]*factor;
      bfield[1]+=bf[1]*factor;
      bfield[2]+=bf[2]*factor;

    }
    double mom_div=particle->momentum_deviation(beamline->element[index]->get_ref_energy());

    factor=(-1./(1+mom_div)/invNanoClight)*step;
    particle->xp+=factor*(bfield[2]*yp-bfield[1]*zp);
    particle->yp+=factor*(bfield[0]*zp-bfield[2]*xp);
    particle->x+=tmstep*particle->xp;
    particle->y+=tmstep*particle->yp;
    particle->z+=tmstep*(particle->xp*particle->xp+particle->yp*particle->yp)*0.5e-6;

    if ( particle->energy < 0 ) posz-=tmstep*particle->get_zp();
    else  posz+=tmstep*particle->get_zp();
}

int IRTracking::fourth_order_mover(PARTICLE *particle,double coefficient, double& posz) {
    double step_coeff=step*coefficient;
    double xp=particle->xp,yp=particle->yp;

    particle->x+=step_coeff*xp;
    particle->y+=step_coeff*yp;
    particle->z+=step_coeff*(xp*xp+yp*yp)*0.5e-6;

    if ( particle->energy < 0 ) posz-=step_coeff*particle->get_zp();
    else posz+=step_coeff*particle->get_zp();
    return 0;
}

//
int IRTracking::fourth_order_kicker(PARTICLE* particle, int particle_index, BEAMLINE* beamline, int index, double mom_div, double coefficient, double posz, double *vp,bool writeKick)
{
    double xp,yp,zp;
    double bfield[3];
    double factor;

    xp = vp[0];
    yp = vp[1];
    zp = vp[2];

    SetBfield(particle,beamline,index, bfield);
    if(map) {
        double bf[3];
        map->GetMapField1D(particle,posz,int_length,bf);
        factor = 1/beamline->element[index]->get_ref_energy();
        bfield[0]+=factor*bf[0];
        bfield[1]+=factor*bf[1];
        bfield[2]+=factor*bf[2];
    }

    if(writeKick && sing_trk_stream && ShouldWriteParticle(particle_index)) *sing_trk_stream << std::scientific << particle->energy << " " << particle->x << " " << particle->y
                << " " << posz*1e-6 << " " << particle->xp << " " << particle->yp
                << " " << bfield[0] << " "  << bfield[1] <<  " " << bfield[2] << std::endl;

    int charge = (particle->energy<0)? 1:-1;
    
    factor = charge/(1+mom_div)/invNanoClight*step*coefficient;
    particle->xp+=factor*(bfield[2]*yp-bfield[1]*zp);
    particle->yp+=factor*(bfield[0]*zp-bfield[2]*xp);

    return 0;
}

double IRTracking::get_posz(PARTICLE* particle)
{
    double posz=int_length-current_int_length; //beam->s;
    if ( particle->energy > 0 ) posz*=-1;
    posz*=1e6;
    posz+=particle->z;
    return posz;
}


//_____________________________________________________________________________________
void IRTracking::step_4th_order(PARTICLE *particle, int particle_index, BEAMLINE *beamline, int index)
{
    double coeff[4] = {0.6756,-0.1756,1.3512,-1.7024};
    double posz = get_posz(particle);

    // momentum deviation of particle...
    double mom_div=particle->momentum_deviation(beamline->element[index]->get_ref_energy());
    // particle.energy contains the q as well ==> the sign convetion is - e+, + e-
    double vp[3] = {particle->xp,particle->yp,particle->get_zp()};


    fourth_order_mover(particle, coeff[0], posz);
    fourth_order_kicker(particle, particle_index, beamline, index, mom_div, coeff[2], posz, vp,false);

    fourth_order_mover(particle, coeff[1], posz);
    fourth_order_kicker(particle, particle_index, beamline, index, mom_div, coeff[3], posz, vp);

    fourth_order_mover(particle, coeff[1], posz);
    fourth_order_kicker(particle, particle_index, beamline, index, mom_div, coeff[2], posz, vp, false);

    fourth_order_mover(particle, coeff[0], posz);
    if (ShouldWriteParticle(particle_index)) fourth_order_kicker(particle, particle_index, beamline, index, mom_div, 0.0, posz, vp, true);

}

//_____________________________________
void IRTracking::SetBfield(PARTICLE *particle,BEAMLINE *beamline,int index, double *bfield)
{
    if(beamline->element[index]->is_sbend()) beamline->element[index]->GetMagField(particle,step,bfield);
    else beamline->element[index]->GetMagField(particle,bfield);
}

void IRTracking::SetSynrad(bool synrad,bool write_synrad)
{ 
    syn=synrad;
    if(syn and write_synrad)
    {
      synrad_stream = new std::ofstream("irtracking_synrad.dat");
      *synrad_stream << "# s nphot eloss" << std::endl;
    }
}

//_____________________________________
double IRTracking::checkboundary()
{
    double ltmp=fabs(beam->s); // beamline tracking
    bool forward_tracking = (step>0);
    int kb = binarySearch(boundlist,0,(int)boundlist.size()-1,ltmp,forward_tracking);
    // remember to use fabs() here, since we might be doing backwards tracking..
    // if kb==-1 it means we are "earlier" than first boundcross.. and going backwards
    if( kb!=-1 && 0<fabs(boundlist[kb]-ltmp) && fabs(step)-fabs(boundlist[kb]-ltmp)>1e-14 ) {
        placet_cout << VERYVERBOSE << "IRTracking::checkboundary at crossing, s = " << int_length - ltmp 
            << ", new step = " << (boundlist[kb] - ltmp) << endmsg;
        SetBoundCross(true);
        return (boundlist[kb] - ltmp);
    }
    else {
        SetBoundCross(false);
        return 0.0;
    }
}

int IRTracking::WriteFirstParticle()
{
    sing_trk_stream = new std::ofstream("singtrk.dat");
    *sing_trk_stream << "# Energy x y s xp yp Bx By Bz" << std::endl;
    return 0;
}

int IRTracking::SetBackwardsTracking(bool backward)
{
    if(backwards_tracking!=backward) {
        backwards_tracking=backward;
        if(step) step*=-1;
        else {
            placet_cout << ERROR << "Must set step before activating backwards tracking" << endmsg;
        }
    }
    return 0;
}

void IRTracking::SetBoundList(std::vector< double >& bound)
{
    boundlist.reserve(bound.size());
    for(unsigned int k=0;k< bound.size();k++) boundlist.push_back(bound[k]);
}
#undef invNanoClight
