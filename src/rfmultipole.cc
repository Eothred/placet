#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <complex>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"

/* Function prototypes */

#include "conversion.h"
#include "function.h"
#include "rfmultipole.h"

extern INTER_DATA_STRUCT inter_data;

/*
  Calculates the kick of a 2*n-pole with strength k on a particle
  at position x,y. The multipole is roted by c0=cos(phi), s0=sin(phi).
  Kicks are returned in kx, ky.
*/

RFMULTIPOLE::RFMULTIPOLE(int &argc, char **argv ) : ELEMENT(), tilt(0.0)
{
  attributes.add("strength_list", "Multipole strength list { E0*K1L, E0*K2L, ... E0*KnL } [GeV/m^(n-1)] [STRING]", OPT_STRING, this,
		 Set(&RFMULTIPOLE::set_strength_list_str), 
		 Get(&RFMULTIPOLE::get_strength_list_str));
  attributes.add("phase_list_deg", "Multipole phase list { (Arg(Bn)+iArg(An) } [deg] [STRING]", OPT_STRING, this,
		 Set(&RFMULTIPOLE::set_phase_list_deg_str), 
		 Get(&RFMULTIPOLE::get_phase_list_deg_str));
  attributes.add("phase_list_rad", "Multipole phase list { (Arg(Bn)+iArg(An) } [rad] [STRING]", OPT_STRING, this,
		 Set(&RFMULTIPOLE::set_phase_list_rad_str), 
		 Get(&RFMULTIPOLE::get_phase_list_rad_str));
  attributes.add("frequency", "Operating Frequency [GHz]", OPT_DOUBLE, &lambda, freq2lambda, lambda2freq);
  attributes.add("steps", "Number of steps for tracking (default: 5) [INT]", OPT_INT, &flags.thin_lens);
  attributes.add("tilt_rad", "Tilt angle [rad]", OPT_DOUBLE, &tilt);
  attributes.add("tilt_deg", "Tilt angle [deg]", OPT_DOUBLE, &tilt, deg2rad, rad2deg);
  set_attributes(argc, argv);
  if (flags.thin_lens<=0) {
    flags.thin_lens=5;
  }
}

void RFMULTIPOLE::list(FILE *file) const
{
  if (!strength_list.empty()) {
    std::string str_slist = get_strength_list_str();
    std::string str_plist = get_phase_list_deg_str();
    fprintf(file,"RfMultipole -length %g -frequency %g -strength_list { %s } -phase_list_deg { %s } -tilt %g\n",
	    geometry.length, lambda2freq(lambda),
	    str_slist.c_str(),
	    str_plist.c_str(),
	    tilt);
  }
}

Matrix<6,6> RFMULTIPOLE::get_transfer_matrix_6d(double _energy ) const 
{
  Matrix<6,6> R(0.0);
  R[4][4]=1.0;
  R[5][5]=1.0;
  if (fabs(geometry.length)>std::numeric_limits<double>::epsilon()) {
    bool is_quad = (strength_list.size()>1) && (abs(strength_list[1])>std::numeric_limits<double>::epsilon());
    if (is_quad) {
      std::complex<double> _strength=strength_list[1];
      if (_energy == -1.0) {
        _energy = ref_energy;
      }
      double k=abs(_strength)/geometry.length/_energy;
      if (k>0.0) {
	double ksqrt=sqrt(k);
	double c,s;
	sincos(ksqrt*geometry.length, &s, &c);
	R[0][0]=c;
	R[0][1]=s/ksqrt;
	R[1][0]=-s*ksqrt;
	R[1][1]=c;
	sincosh(ksqrt*geometry.length, s, c);
	R[2][2]=c;
	R[2][3]=s/ksqrt;
	R[3][2]=s*ksqrt;
	R[3][3]=c;
      } else {
	double ksqrt=sqrt(-k);
	double c,s;
	sincosh(ksqrt*geometry.length, s, c);
	R[0][0]=c;
	R[0][1]=s/ksqrt;
	R[1][0]=s*ksqrt;
	R[1][1]=c;
	sincos(ksqrt*geometry.length, &s, &c);
	R[2][2]=c;
	R[2][3]=s/ksqrt;
	R[3][2]=-s*ksqrt;
	R[3][3]=c;
      }
      double _tilt = tilt+std::arg(_strength)/2;
      if (fabs(_tilt)>std::numeric_limits<double>::epsilon()) {
	double s,c;
	sincos(_tilt,&s,&c);
	Matrix<6,6> Rot(0.0);
	Rot[0][0]=Rot[1][1]=Rot[2][2]=Rot[3][3]=c;
	Rot[0][2]=Rot[1][3]=-s;
	Rot[2][0]=Rot[3][1]=s;
	Rot[4][4]=1.0;
	Rot[5][5]=1.0;
	R*=Rot;
	Rot[0][2]=Rot[1][3]=s;
	Rot[2][0]=Rot[3][1]=-s;
	R=Rot*R;
      }
    } else {
      R[0][0]=1.0;
      R[0][1]=geometry.length;
      R[1][1]=1.0;
      R[2][2]=1.0;
      R[2][3]=geometry.length;
      R[3][3]=1.0;
    }
  } else {
    R[0][0]=1.0;
    R[1][1]=1.0;
    R[2][2]=1.0;
    R[3][3]=1.0;
  }
  return R;
}
