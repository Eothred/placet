#include <stdlib.h>
#include <stdio.h>

#include "malloc.h"
#include "placet_print.h"

#ifdef XMALLOC_DEBUG

struct
{
  size_t size_total;
} xmalloc_data;

void* xmalloc(size_t size)
{
  void *tmp;
  size_t *p;
  tmp=malloc(size+sizeof(size_t));
  if (tmp==NULL){
    placet_printf(ERROR,"Not enough memory for malloc\n");
    placet_printf(ERROR,"Bytes requested this call: %d\n",size);
    placet_printf(ERROR,"Bytes requested in total: %d\n",xmalloc_data.size_total);
    fflush(stderr);
    exit(1);
  }
  xmalloc_data.size_total+=size;
  placet_printf(INFO,"malloc: %d %d\n",xmalloc_data.size_total,size);
  p=(size_t*)tmp;
  //  placet_printf(INFO,"%d\n",p);
  *p=size;
  p++;
  return p;
}

void* xrealloc(void* pold,size_t size)
{
  void *tmp;
  size_t *p;
  p=(size_t*)pold;
  p--;
  xmalloc_data.size_total-=*p;
  p=realloc(p,size+sizeof(size_t));
  if (p==NULL){
    placet_printf(ERROR,"Not enough memory for malloc\n");
    placet_printf(ERROR,"Bytes requested this call: %d\n",size);
    placet_printf(ERROR,"Bytes requested in total: %d\n",xmalloc_data.size_total);
    fflush(stderr);
    exit(1);
  }
  xmalloc_data.size_total+=size;
  placet_printf(INFO,"malloc: %d %d\n",xmalloc_data.size_total,size);
  //  placet_printf(INFO,"%d\n",p);
  *p=size;
  p++;
  return p;
}

size_t xmalloc_size()
{
  return xmalloc_data.size_total;
}

void xmalloc_init()
{
  xmalloc_data.size_total=0;
}

void xfree(void* p)
{
  void *tmp;
  size_t *l;
  l=(size_t*)p;
  //  placet_printf(INFO,"%d\n",p);
  //  placet_printf(INFO,"%d\n",*(l-1));
  l--;
  xmalloc_data.size_total-=*l;
  placet_printf(INFO,"free: %d %d\n",xmalloc_data.size_total,*l);
  free(l);
}

#else

static struct
{
  size_t size_total;
} xmalloc_data;

void* xmalloc(size_t size)
{
  void *tmp;
  tmp=malloc(size);
  if (tmp==NULL){
    placet_printf(ERROR,"Not enough memory for malloc\n");
    placet_printf(ERROR,"Bytes requested this call: %lu\n",(unsigned long)size);
    placet_printf(ERROR,"Bytes requested in total: %lu\n",(unsigned long)xmalloc_data.size_total);
    fflush(stderr);
    exit(1);
  }
  return tmp;
}

void* xrealloc(void* pold,size_t size)
{
  size_t *p;
  p = (size_t *)realloc(pold,size);
  if (p==NULL){
    placet_printf(ERROR,"Not enough memory for malloc\n");
    placet_printf(ERROR,"Bytes requested this call: %lu\n",(unsigned long)size);
    placet_printf(ERROR,"Bytes requested in total: %lu\n",(unsigned long)xmalloc_data.size_total);
    fflush(stderr);
    exit(1);
  }
  return p;
}

size_t xmalloc_size()
{
  return xmalloc_data.size_total;
}

void xmalloc_init()
{
  xmalloc_data.size_total=0;
}

void xfree(void* p)
{
  free(p);
}

#endif
