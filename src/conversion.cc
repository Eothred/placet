#include <cmath>

#include "conversion.h"
#include "placet.h"

double deg2rad(double x)
{
  return x*M_PI/180.0;
}

double rad2deg(double x)
{
  return x*180.0/M_PI;
}

double freq2lambda(double x ) // GHz to meters
{
  return C_LIGHT/(x*1e9);
}

double lambda2freq(double x ) // meters to GHz
{
  return C_LIGHT/(x*1e9);
}

double x2ux(double x )  // X to microX (rad to microrad, m to microm)
{
  return x*1e6;
}

double ux2x(double x )  // microX to X (microrad to rad, microm to m)
{
  return x*1e-6;
}
