#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"
#include "random.hh"

/* Function prototypes */

#include "bpm.h"
#include "element.h"
#include "placet_tk.h"

extern INTER_DATA_STRUCT inter_data;

void BPM::setup_attributes_table()
{
  attributes.add("resolution", "BPM resolution [um]", OPT_DOUBLE, &resolution);
  attributes.add("reading_x", "Horizontal BPM reading [um] [READ-ONLY]", OPT_DOUBLE, this, Set(NULL), Get(&BPM::get_x_reading));
  attributes.add("reading_y", "Vertical BPM reading [um] [READ-ONLY]", OPT_DOUBLE, this, Set(NULL), Get(&BPM::get_y_reading));
  attributes.add("transmitted_charge", "Charge transmitted [bunch charge] [READ-ONLY]", OPT_DOUBLE, &pos.w);
  attributes.add("scale_x", "BPM scale factor in the horizontal direction [#]", OPT_DOUBLE, &scale.x);
  attributes.add("scale_y", "BPM scale factor in the vertical direction [#]", OPT_DOUBLE, &scale.y);
  attributes.add("store_bunches", "Number of bunches to store [INT]", OPT_INT, this, Set(&BPM::set_store_bunches), Get(&BPM::get_store_bunches));
  enable_hcorr("x", 0.0);
  enable_vcorr("y", 0.0);
}

BPM::BPM(double length, int n ) : ELEMENT(length), store_bunches(n)
{
  resolution=0.0;
  err.x=0.0;
  err.y=0.0;
  scale.x=1.0;
  scale.y=1.0;
  pos.x=0.0;
  pos.y=0.0;
  pos.w=0.0;
  setup_attributes_table();
}

BPM::BPM(int &argc, char **argv ) : ELEMENT(), resolution(0.0), store_bunches(0)
{
  err.x=0.0;
  err.y=0.0;
  scale.x=1.0;
  scale.y=1.0;
  pos.x=0.0;
  pos.y=0.0;
  pos.w=0.0;
  setup_attributes_table();
  set_attributes(argc, argv);
}

/**********************************************************************/
/*                      print BPM readings to a file                  */
/**********************************************************************/

int bpm_readings(Tcl_Interp *interp,BEAMLINE *beamline,char *fname,int corr0)
{
  FILE *file=NULL;
  int n,j=0;
  ELEMENT **element;
  double s=0.0;
  char buffer[128];

  n=beamline->n_elements;
  element=beamline->element;
  file=open_file(fname);

  for (int i=0;i<n;i++){
    s+=0.5*element[i]->get_length();
    if (BPM *pickup=dynamic_cast<BPM*>(element[i])) {
      double reading_x,reading_y;
      if (corr0) {
	reading_x = pickup->get_x_reading_exact();
	reading_y = pickup->get_y_reading_exact();
      } else {
	reading_x = pickup->get_x_reading();
	reading_y = pickup->get_y_reading();
      }
      if (file) {
	fprintf(file,"%d %g %g\n",j++,reading_x,reading_y);
      } else {
	snprintf(buffer,128,"%d %g %g",j++,reading_x,reading_y);
	Tcl_AppendElement(interp,buffer);
      }
    }
    s+=0.5*element[i]->get_length();
  }

  close_file(file);
  
  return TCL_OK;
}

/******   Tcl/Tk commands   ******/

int tk_BpmReadings(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  int error;
  char *file_name=NULL;
  int corr0=0;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file_name,
     (char*)"Name of the file where to store the results"},
    {(char*)"-correct",TK_ARGV_INT,(char*)NULL,(char*)&corr0,
     (char*)"If not 0 the results will be returned neglecting the BPM resolution (default 0)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  return bpm_readings(interp,inter_data.beamline,file_name,corr0);
}

void BPM::step_4d_0(BEAM *beam)
{
  /**
    Move beam to the centre of the structure
  */
  drift_step_4d_0(beam,0.5); 

  /**
    Calculate BPM reading
  */
  calculate_bpm_reading(beam);
  
  /**
    Move beam to the end of the structure
  */
  drift_step_4d_0(beam,0.5);
}

void BPM::step_4d(BEAM *beam)
{
  drift_step_4d(beam,0.5);
  calculate_bpm_reading(beam);
  drift_step_4d(beam,0.5);
}

void BPM::step_6d_0(BEAM *beam)
{
  drift_step_6d_0(beam,0.5);
  calculate_bpm_reading(beam);
  drift_step_6d_0(beam,0.5);
}

void BPM::calculate_bpm_reading(BEAM *beam)
{
  pos.y=0.0;
  pos.x=0.0;
  if (resolution>std::numeric_limits<double>::epsilon()) {
    err.x=resolution*Instrumentation.Gauss();
    err.y=resolution*Instrumentation.Gauss();
  } else {
    err.x=0.0;
    err.y=0.0;
  }
  
  double wsum=0.0;

  if (!store_bunches.size()) {
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
         double tmp=fabs(beam->particle[i].wgt);
	 wsum+=tmp;
	 pos.y+=beam->particle[i].y*tmp;
	 pos.x+=beam->particle[i].x*tmp;
      }
    }
    // adjust readings for last_wgt'ed bunch
    if( beam->last_wgt > 1.0 ) {
      int n_start = (beam->bunches-1)*(beam->slices/beam->bunches);
      for (int i=0;i<(beam->slices/beam->bunches);i++){
        double tmp=fabs(beam->particle[n_start+i].wgt) * (beam->last_wgt-1.0);
        wsum+=tmp;
        pos.y+=(beam->particle[n_start+i].y)*tmp;
        pos.x+=(beam->particle[n_start+i].x)*tmp;
      }
    }
    pos.y/=wsum;
    pos.y*=scale.y;
    pos.x/=wsum;
    pos.x*=scale.x;

  } else {
    if (store_bunches.size()<size_t(beam->bunches)) {
      store_bunches.resize(beam->bunches);
    }
    int nn=beam->slices/beam->bunches;
    int k=0;
    for (int i=0;i<beam->bunches;i++){
      double sumx=0.0;
      double sumy=0.0;
      double sum=0.0;
      for (int j=0;j<nn;j++) {
        PARTICLE &particle=beam->particle[k];
        if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
          double tmp=fabs(particle.wgt);
          sum+=tmp;
          sumy+=particle.y*tmp;
          sumx+=particle.x*tmp;
          ++k;
        }
      }
      sumy*=scale.y;
      store_bunches[i].y=sumy/sum;
      pos.y+=sumy;
      sumx*=scale.x;
      store_bunches[i].x=sumx/sum;
      pos.x+=sumx;
      store_bunches[i].w=sum;
      wsum+=sum;
    }
    // adjust readings for last_wgt'ed bunch
    if( beam->last_wgt > 1.0 ) {
      int n_start = (beam->bunches-1)*beam->slices_per_bunch;
      double sumx=0.0;
      double sumy=0.0;
      double sum=0.0;
      for (int i=0;i<beam->slices_per_bunch;i++){
        double tmp=fabs(beam->particle[n_start+i].wgt) * (beam->last_wgt-1.0);
        sum+=tmp;
        sumy+=(beam->particle[n_start+i].y)*tmp;
        sumx+=(beam->particle[n_start+i].x)*tmp;
      }
      wsum+=sum;
      pos.x+=sumx*scale.x;
      pos.y+=sumy*scale.y;
    }

    pos.y/=wsum;
    pos.x/=wsum;
  }
  
  pos.w=wsum;

  if (wsum==0.0) {
    pos.x=0.0;
    pos.y=0.0;
    if(!beam->is_lost_bpm) {
      placet_printf(WARNING, "%-15s the beam is lost in BPM at s = %g m\n", "WARNING", geometry.s);
      beam->is_lost_bpm=true;
    }
  }
}

int
BPM::display(Tcl_Interp *interp,int correct)
{
  char buffer[128];
  if (correct) {
    snprintf(buffer,128,"%g %g",get_x_reading_exact(),get_y_reading_exact());
  } else {
    snprintf(buffer,128,"%g %g",get_x_reading(),get_y_reading());
  }
  Tcl_AppendElement(interp,buffer);
  for (size_t i=0;i<store_bunches.size();i++) {
    snprintf(buffer,128,"%g %g",store_bunches[i].x,store_bunches[i].y);
    Tcl_AppendElement(interp,buffer);
  }
  return TCL_OK;
}

void BPM::set_store_bunches(int n)
{
  store_bunches.resize(n);
}

int tk_BpmDisplay(ClientData /*cd*/,Tcl_Interp *interp,int argc,char **argv)
{
  int corr0=0;
  Tk_ArgvInfo table[]={
    {(char*)"-correct",TK_ARGV_CONSTANT,(char*)1,(char*)&corr0,
     (char*)"If option is used the real BPM measurements are returned"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc<2) {
    Tcl_AppendResult(interp, "Usage: BpmDisplay <N>", NULL);
    return TCL_ERROR;
  }
  int i=strtol(argv[1],NULL,10);
  if (BPM *bpm=inter_data.beamline->element[i]->bpm_ptr()) {
    return bpm->display(interp,corr0);
  }
  return TCL_ERROR;
}
