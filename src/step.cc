#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "beam.h"
#include "beamline.h"
#include "matrix.h"
#include "placeti3.h"
#include "step.h"

/* This routine print the twiss parameters of beam into file  */

void
fprint_twiss(FILE *file,BEAM *beam,int j,double s,int n1,int n2)
{
  double eps=0.0,beta=0.0,alpha=0.0,e=0.0,ei=0.0,wgtsum=0.0;
  double eps0=0.0,alpha0=0.0,beta0=0.0,epsi=0.0,betai=0.0,alphai=0.0;
  double eps_x=0.0,beta_x=0.0,alpha_x=0.0;
  double eps0_x=0.0,alpha0_x=0.0,beta0_x=0.0, eps_xi=0.0, beta_xi=0.0, alpha_xi= 0.0;
  double disp_x=0.0, disp_xp=0.0, disp_y=0.0, disp_yp=0.0;
  double enesumi=0.0;
  int nc=beam->slices/2;
  if (beam->particle_beam || placet_switch.first_order) {
    double xsum=0.0,ysum=0.0,xpsum=0.0,ypsum=0.0,esum=0.0;
    double sxxsum=0.0,sxxpsum=0.0,sxpxpsum=0.0,syysum=0.0,syypsum=0.0,sypypsum=0.0;
    double sxesum=0.0,syesum=0.0,sxpesum=0.0,sypesum=0.0,seesum=0.0;
    for (int i=0;i<beam->slices;++i) {
      const PARTICLE &particle=beam->particle[i];
      double energy=fabs(particle.energy);
      double wgt=fabs(particle.wgt);
      wgtsum+=wgt;
      xsum+=wgt*particle.x;
      xpsum+=wgt*particle.xp;
      ysum+=wgt*particle.y;
      ypsum+=wgt*particle.yp;
      esum+=wgt*energy;
      sxxsum+=wgt*(particle.x*particle.x);
      sxxpsum+=wgt*(particle.x*particle.xp);
      sxpxpsum+=wgt*(particle.xp*particle.xp);
      syysum+=wgt*(particle.y*particle.y);
      syypsum+=wgt*(particle.y*particle.yp);
      sypypsum+=wgt*(particle.yp*particle.yp);
      sxesum+=wgt*(particle.x*energy);
      syesum+=wgt*(particle.y*energy);
      sxpesum+=wgt*(particle.xp*energy);
      sypesum+=wgt*(particle.yp*energy);
      seesum+=wgt*(energy*energy);
    }
    xsum/=wgtsum;
    xpsum/=wgtsum;
    ysum/=wgtsum;
    ypsum/=wgtsum;
    esum/=wgtsum;
    sxxsum/=wgtsum;
    sxxpsum/=wgtsum;
    sxpxpsum/=wgtsum;
    syysum/=wgtsum;
    syypsum/=wgtsum;
    sypypsum/=wgtsum;  
    sxxsum-=xsum*xsum;
    sxxpsum-=xsum*xpsum;
    sxpxpsum-=xpsum*xpsum;
    syysum-=ysum*ysum;
    syypsum-=ysum*ypsum;
    sypypsum-=ypsum*ypsum;
    sxesum/=wgtsum; 
    syesum/=wgtsum; 
    sxpesum/=wgtsum;
    sypesum/=wgtsum;
    seesum/= wgtsum;
    double emitt_x=sqrt(sxxsum*sxpxpsum-sxxpsum*sxxpsum);
    double emitt_y=sqrt(syysum*sypypsum-syypsum*syypsum);
    beta_x=sxxsum/emitt_x;
    beta=syysum/emitt_y;
    alpha_x=-sxxpsum/emitt_x;
    alpha=-syypsum/emitt_y;
    disp_x=(sxesum-xsum*esum)/(seesum-esum*esum)*1e-6;
    disp_xp=(sxpesum-xpsum*esum)/(seesum-esum*esum)*1e-6;
    disp_y=(syesum-ysum*esum)/(seesum-esum*esum)*1e-6;
    disp_yp=(sypesum-ypsum*esum)/(seesum-esum*esum)*1e-6;
    e=esum;
  } else{
    for (int i=0;i<beam->slices;i++) {
      double wgt = fabs(beam->particle[i].wgt);
      wgtsum+=wgt;
      eps+=(beam->sigma[i].r11*beam->sigma[i].r22-beam->sigma[i].r12*beam->sigma[i].r21)*wgt;
      beta+=beam->sigma[i].r11*wgt;
      alpha-=beam->sigma[i].r12*wgt;
#ifdef TWODIM
      eps_x+=(beam->sigma_xx[i].r11*beam->sigma_xx[i].r22-beam->sigma_xx[i].r12*beam->sigma_xx[i].r21)*wgt;
      beta_x+=beam->sigma_xx[i].r11*wgt;
      alpha_x-=beam->sigma_xx[i].r12*wgt;
#endif
    }
     eps/=wgtsum;
     beta/=wgtsum;
     alpha/=wgtsum;
     eps=sqrt(eps);
     beta/=eps;
     alpha/=eps;
     
     eps0=(beam->sigma[nc].r11*beam->sigma[nc].r22-beam->sigma[nc].r12*beam->sigma[nc].r21);
     beta0=beam->sigma[nc].r11;
     alpha0=-beam->sigma[nc].r12;
     eps0=sqrt(eps0);
     beta0/=eps0;
     alpha0/=eps0;
     
#ifdef TWODIM   
     eps_x/=wgtsum;
     beta_x/=wgtsum;
     alpha_x/=wgtsum;
     eps_x=sqrt(eps_x);
     beta_x/=eps_x;
     alpha_x/=eps_x;
     
     eps0_x=(beam->sigma_xx[nc].r11*beam->sigma_xx[nc].r22-beam->sigma_xx[nc].r12*beam->sigma_xx[nc].r21);
     beta0_x=beam->sigma_xx[nc].r11;
     alpha0_x=-beam->sigma_xx[nc].r12;
     eps0_x=sqrt(eps0_x);
     beta0_x/=eps0_x;
     alpha0_x/=eps0_x;
#endif
     e=beam->particle[nc].energy;   
     if (n1&&n2!=nc) {
          double wgtsi=0.0, wgts=0.0;
          int npart=0;
          for (int ni=n1;ni<=n2;ni++){
               npart++;  
               enesumi+=beam->particle[ni].energy;
               wgtsi=fabs(beam->particle[ni].wgt);
               wgts+=wgtsi;
               epsi+=(beam->sigma[ni].r11*beam->sigma[ni].r22-beam->sigma[ni].r12*beam->sigma[ni].r21)*wgtsi;
               betai+=beam->sigma[ni].r11*wgtsi;
               alphai-=beam->sigma[ni].r12*wgtsi;
          #ifdef TWODIM  
               eps_xi+=(beam->sigma_xx[ni].r11*beam->sigma_xx[ni].r22-beam->sigma_xx[ni].r12*beam->sigma_xx[ni].r21)*wgtsi;
               beta_xi+=beam->sigma_xx[ni].r11*wgtsi;
               alpha_xi-=-beam->sigma_xx[ni].r12*wgtsi;
          #endif  
          }
	  if (npart>0) {
	    ei=enesumi/npart;
	    epsi/=wgts;
	    betai/=wgts;
	    alphai/=wgts;
	    epsi=sqrt(epsi);
	    betai/=epsi;
	    alphai/=epsi;
#ifdef TWODIM   
	    eps_xi/=wgts;
	    beta_xi/=wgts;
	    alpha_xi/=wgts;
	    eps_xi=sqrt(eps_xi);
	    beta_xi/=eps_xi;
	    alpha_xi/=eps_xi;
#endif 
	  }
     }
  }
  if (file) {
     if (n1&&n2!=nc) {
#ifdef TWODIM
fprintf(file,"%d %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g\n",
        j,s,e,beta0_x,alpha0_x,beta_x,alpha_x,beta0,alpha0,beta,alpha,disp_x,disp_xp,disp_y,disp_yp,ei,beta_xi,alpha_xi,betai,alphai);
#else
fprintf(file,"%d %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g\n",
        j,s,e,beta0,alpha0,beta,alpha,ei,betai,alphai);
#endif
     } else {
#ifdef TWODIM
fprintf(file,"%d %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g\n",
        j,s,e,beta0_x,alpha0_x,beta_x,alpha_x,beta0,alpha0,beta,alpha,disp_x,disp_xp,disp_y,disp_yp);
#else
fprintf(file,"%d %.15g %.15g %.15g %.15g %.15g %.15g\n",
        j,s,e,beta0,alpha0,beta,alpha);
#endif
     } 
  }
  else {  
    char dummy[255];
    if (n1&&n2!=nc) {
#ifdef TWODIM
      snprintf(dummy,255,"{%d %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g}\n",
	       j,s,e,beta0_x,alpha0_x,beta_x,alpha_x,beta0,alpha0,beta,alpha,disp_x,disp_xp,disp_y,disp_yp,ei,beta_xi,alpha_xi,betai,alphai);
#else
      snprintf(dummy,255,"{%d %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g}\n",
	       j,s,e,beta0,alpha0,beta,alpha,ei,betai,alphai);
#endif
    } else {
#ifdef TWODIM
      snprintf(dummy,255,"{%d %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g}\n",
	       j,s,e,beta0_x,alpha0_x,beta_x,alpha_x,beta0,alpha0,beta,alpha,disp_x,disp_xp,disp_y,disp_yp);
#else
      snprintf(dummy,255,"{%d %.15g %.15g %.15g %.15g %.15g %.15g}\n",
	      j,s,e,beta0,alpha0,beta,alpha);
#endif
    }
    interp_append(dummy);
  }
}

void twiss_plot_step(BEAMLINE *beamline,BEAM *beam0,char *name,double step,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int), int *list, int nlist )
{
  BEAM *beam=bunch_remake(beam0);
  FILE *file=open_file(name);
  twiss_write_header(file);  
  beam_copy(beam0,beam);
  ELEMENT **element=beamline->element;
  int nc=(beam->slices)/2;
  if (n1&&n2!=nc) {
    fprintf(file,"# 16.) E_i(s): [GeV] (average energy over slices given by the option -start -end)\n");
    fprintf(file,"# 17.) beta_x_i(s) [m] (of average over slices given by the option -start -end)  \n");
    fprintf(file,"# 18.) alpha_x_i(s) [m] (of average over slices given by the option -start -end) \n");
    fprintf(file,"# 19.) beta_y_i(s) [m] (of average over slices given by the option -start -end)  \n");
    fprintf(file,"# 20.) alpha_y_i(s) [m] (of average over slices given by the option -start -end) \n\n");
  }
  double s=0.0;
  for (int j=0; element[j]!=NULL; j++){
    bool skip=false;
    if (list) {
      skip=(std::find(list,list+nlist,j)==(list+nlist));
    }
    if(skip || element[j]->is_tclcall() || element[j]->is_special()){
      //if(element[j]->is_special()) placet_cout << VERBOSE << " twiss_plot_step: special element " << endmsg;
      //if(element[j]->is_tclcall()) placet_cout << VERBOSE << " twiss_plot_step: tcl_call " << endmsg;
      element[j]->track(beam);
      // placet_cout << DEBUG << " twiss_plot_step: element  tracked" << endmsg;
    } else {
      if(placet_switch.first_order || beam->particle_beam){
	element[j]->step_twiss_0(beam,file,step,j,s,n1,n2,callback);
      } else {
	element[j]->step_twiss(beam,file,step,j,s,n1,n2,callback);
	// placet_cout << DEBUG << " twiss_plot_step: twiss element number "<< j << endmsg;
      }
      }
    s+=element[j]->get_length();
  }
  close_file(file);
  beam_delete(beam);
}
