/* applies the transverse wakefield kick */

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "wakekick.h"

extern int data_nstep;

void wake_kick(ELEMENT * /*element*/,BEAM *beam,double length)
{
  static int step=0;
  DRIVE_DATA *cavity_data=beam->drive_data;
  double factor=beam->transv_factor*length*beam->factor;
  switch(beam->which_field){
  case 1: {
    double *p=beam->field[0].kick;
    for (int j=0;j<beam->slices;j++){
      double sumy=0.0;
      double sumx=0.0;
      for (int i=0;i<=j;i++){
	sumy+=beam->particle[i].y*beam->particle[i].wgt * *p;
	sumx+=beam->particle[i].x*beam->particle[i].wgt * *p;
	p++;
      }
      beam->particle[j].yp+=sumy*factor/beam->particle[j].energy;
      beam->particle[j].xp+=sumx*factor/beam->particle[j].energy;
    }
  } break;
  case 3:
    for (int k=0;k<beam->bunches;k++){
      double multi_x=0.0;
      double multi_y=0.0;
      double *rho_x=cavity_data->xpos;
      double *rho_y=cavity_data->ypos;
      double *force=cavity_data->along;
      int j0=0;
      if (k>cavity_data->longrange_max){
	j0=k-cavity_data->longrange_max;
      }
      for (int j=j0;j<k;j++){
	multi_y+=rho_y[j]*force[k-j];
	multi_x+=rho_x[j]*force[k-j];
      }
      double *p=beam->field[0].kick;
      int m=k*beam->slices_per_bunch;
      double pos_y=0.0;
      double pos_x=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	double sumy=0.0;
	double sumx=0.0;
	pos_y+=beam->particle[m+j].y*beam->particle[m+j].wgt;
	pos_x+=beam->particle[m+j].x*beam->particle[m+j].wgt;
	for (int i=0;i<=j;i++){
	  sumy+=beam->particle[m+i].y*beam->particle[m+i].wgt * *p;
	  sumx+=beam->particle[m+i].x*beam->particle[m+i].wgt * *p;
	  p++;
	}
	beam->particle[m+j].yp+=(sumy+multi_y)*factor/beam->particle[m+j].energy;
	beam->particle[m+j].xp+=(sumx+multi_x)*factor/beam->particle[m+j].energy;
      }
      cavity_data->ypos[k]=pos_y;
      cavity_data->xpos[k]=pos_x;
    }
    break;
  case 4: {
    double *rho_x_s=beam->rho_x[0];
    double *rho_y_s=beam->rho_y[0];
    for (int k=0;k<beam->bunches;k++){
      double multi_x=0.0;
      double multi_y=0.0;
      double *rho_x=cavity_data->xpos;
      double *rho_y=cavity_data->ypos;
      double *force=cavity_data->along;
      int j0=0;
      if (k>cavity_data->longrange_max){
	j0=k-cavity_data->longrange_max;
      }
      for (int j=j0;j<k;j++){
	multi_y+=rho_y[j]*force[k-j];
	multi_x+=rho_x[j]*force[k-j];
      }
      double *p=beam->field[0].kick;
      int m=k*beam->slices_per_bunch*beam->macroparticles;
      int m2=m;
      double pos_y=0.0;
      double pos_x=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	double sumx=0.0;
	double sumy=0.0;
	for (int l=0;l<beam->macroparticles;l++){
	  sumy+=beam->particle[m].y*beam->particle[m].wgt;
	  sumx+=beam->particle[m].x*beam->particle[m].wgt;
	  m++;
	}
	pos_y+=sumy;
	rho_y_s[j]=sumy;
	sumy=0.0;
	pos_x+=sumx;
	rho_x_s[j]=sumx;
	sumx=0.0;
	for (int i=0;i<=j;i++){
	  sumy+=rho_y_s[i] * *p;
	  sumx+=rho_x_s[i] * *p;
	  p++;
	}
	for (int l=0;l<beam->macroparticles;l++){
	  beam->particle[m2].yp+=(sumy+multi_y)*factor/beam->particle[m2].energy;
	  beam->particle[m2].xp+=(sumx+multi_x)*factor/beam->particle[m2].energy;
	  m2++;
	}
      }
      cavity_data->ypos[k]=pos_y;
      cavity_data->xpos[k]=pos_x;
    }
  }  break;
    
    /*
      Multi bunch transverse wakefields for single modes per structure.
    */
    
  case 7: {
    /* Initialise wakefields */
    double *rho_y=beam->rho_y[0];
    double multi_y=0.0;
    double multi_y_b=0.0;
    double *rho_x=beam->rho_x[0];
    double multi_x=0.0;
    double multi_x_b=0.0;
    /* set pointer af and bf to the appropriate cell number */
    
    double *af=cavity_data->af+step*beam->bunches*beam->slices_per_bunch;
    double *bf=cavity_data->bf+step*beam->bunches*beam->slices_per_bunch;
    double *along=cavity_data->along+step*beam->bunches;
    double longfact=along[0];
    
    /* set new cell number if different cells */
    
    step++;
    if (step>=NSTEP) step=0;
    //    if (MODEL==0) step=0;
    
    /* loop over all bunches */
    
    for (int k=0;k<beam->bunches;k++){
      
      /* apply attenuation */
      
      if (k>0){
	multi_y*=along[k];
	multi_y_b*=along[k];
	multi_x*=along[k];
	multi_x_b*=along[k];
      }
      
      double *p=beam->field[0].kick;
      int m=k*beam->slices_per_bunch;
      double pos_y=0.0;
      double pos_yb=0.0;
      double pos_x=0.0;
      double pos_xb=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	rho_y[j]=beam->particle[m+j].y*beam->particle[m+j].wgt;
	rho_x[j]=beam->particle[m+j].x*beam->particle[m+j].wgt;
	double s=af[m+j];  /*sin(beam->z_position[m+j]);*/
	double c=bf[m+j];  /*cos(beam->z_position[m+j]);*/
	/*
	  s=sin(beam->z_position[m+j]*TWOPI*1e-5);
	  c=cos(beam->z_position[m+j]*TWOPI*1e-5);
	*/
	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
	/*
	  Single bunch contribution
	*/
	double sumy=0.0;
	double sumx=0.0;
	for (int i=0;i<=j;i++){
	  sumy+=rho_y[i] * *p;
	  sumx+=rho_x[i] * *p;
	  p++;
	}
	beam->particle[m+j].yp+=(sumy+(multi_y*s+multi_y_b*c)*longfact)*factor/beam->particle[m+j].energy;
	beam->particle[m+j].xp+=(sumx+(multi_x*s+multi_x_b*c)*longfact)*factor/beam->particle[m+j].energy;
      }
      multi_y+=pos_y;
      multi_y_b+=pos_yb;
      multi_x+=pos_x;
      multi_x_b+=pos_xb;
    }
  }  break;
  
  
  /*
      Multi bunch transverse wakefields for single modes per structure.
  */
  
  case 6: {
    int nmacro=beam->macroparticles;
    /* Initialise wakefields */
    double *rho_y=beam->rho_y[0];
    double multi_y=0.0;
    double multi_y_b=0.0;
    double *rho_x=beam->rho_x[0];
    double multi_x=0.0;
    double multi_x_b=0.0;
    /* set pointer af and bf to the appropriate cell number */
    
    double *af=cavity_data->af+step*beam->bunches*beam->slices_per_bunch;
    double *bf=cavity_data->bf+step*beam->bunches*beam->slices_per_bunch;
    double *along=cavity_data->along+step*beam->bunches;
    double longfact=along[0];
    
    /* set new cell number if different cells */
    
    step++;
    if (step>=NSTEP) step=0;
    //    if (MODEL==0) step=0;
    
    /* loop over all bunches */
    
    for (int k=0;k<beam->bunches;k++){
      
      /* apply attenuation */
      
      if (k>0){
	multi_y*=along[k];
	multi_y_b*=along[k];
	multi_x*=along[k];
	multi_x_b*=along[k];
      }
      
      double *p=beam->field[0].kick;
      int m=k*beam->slices_per_bunch;
      double pos_y=0.0;
      double pos_yb=0.0;
      double pos_x=0.0;
      double pos_xb=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	rho_y[j]=0.0;
	rho_x[j]=0.0;
	for (int ks=0;ks<nmacro;ks++){
	  rho_y[j]+=beam->particle[(m+j)*nmacro+ks].y
	    *beam->particle[(m+j)*nmacro+ks].wgt;
	  rho_x[j]+=beam->particle[(m+j)*nmacro+ks].x
	    *beam->particle[(m+j)*nmacro+ks].wgt;
	}
	double s=af[m+j];  /*sin(beam->z_position[m+j]);*/
	double c=bf[m+j];  /*cos(beam->z_position[m+j]);*/
	/*
	  s=sin(beam->z_position[m+j]*TWOPI*1e-5);
	  c=cos(beam->z_position[m+j]*TWOPI*1e-5);
	*/
	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
	/*
	  Single bunch contribution
	 */
	double sumy=0.0;
	double sumx=0.0;
	for (int i=0;i<=j;i++){
	  sumy+=rho_y[i] * *p;
	  sumx+=rho_x[i] * *p;
	  p++;
	}
	for (int ks=0;ks<nmacro;ks++) {
	  beam->particle[(m+j)*nmacro+ks].yp+=
	    (sumy+(multi_y*s+multi_y_b*c)*longfact)
	    *factor/beam->particle[(m+j)*nmacro+ks].energy;
	  beam->particle[(m+j)*nmacro+ks].xp+=
	    (sumx+(multi_x*s+multi_x_b*c)*longfact)
	    *factor/beam->particle[(m+j)*nmacro+ks].energy;
	}
      }
      multi_y+=pos_y;
      multi_y_b+=pos_yb;
      multi_x+=pos_x;
      multi_x_b+=pos_xb;
    }
    }  break;
  case 12: {
    int nmacro=beam->macroparticles;
    for (int k=0;k<beam->bunches;k++){
      double multi_y=0.0;
      double multi_y_b=0.0;
      double *rho_a=cavity_data->ypos;
      double *rho_b=cavity_data->yposb;
      double multi_x=0.0;
      double multi_x_b=0.0;
      double *rho_a_x=cavity_data->xpos;
      double *rho_b_x=cavity_data->xposb;
      double *force=cavity_data->along;
      int j0=0;
      if (k>cavity_data->longrange_max){
	j0=k-cavity_data->longrange_max;
      }
      for (int j=j0;j<k;j++){
	multi_y+=rho_a[j]*force[k-j];
	multi_y_b+=rho_b[j]*force[k-j];
	multi_x+=rho_a_x[j]*force[k-j];
	multi_x_b+=rho_b_x[j]*force[k-j];
      }
      int m=k*beam->slices_per_bunch;
      double pos_y=0.0;
      double pos_yb=0.0;
      double pos_x=0.0;
      double pos_xb=0.0;
      for (int j=0;j<beam->slices_per_bunch;j++){
	double tmpy=0.0;
	double tmpx=0.0;
	for (int i=0;i<nmacro;i++){
	  tmpy+=beam->particle[nmacro*(m+j)+i].y
	    *beam->particle[nmacro*(m+j)+i].wgt;
	  tmpx+=beam->particle[nmacro*(m+j)+i].x
	    *beam->particle[nmacro*(m+j)+i].wgt;
	}
	pos_y+=tmpy*cavity_data->bf[m+j];
	pos_yb-=tmpy*cavity_data->af[m+j];
	pos_x+=tmpx*cavity_data->bf[m+j];
	pos_xb-=tmpx*cavity_data->af[m+j];
	for (int i=0;i<nmacro;i++){
	  beam->particle[nmacro*(m+j)+i].yp+=((multi_y+pos_y*force[0])
					      *cavity_data->af[m+j]
					      +(multi_y_b+pos_yb*force[0])
					      *cavity_data->bf[m+j])
	    *factor/beam->particle[m+j].energy;
	  beam->particle[nmacro*(m+j)+i].xp+=((multi_x+pos_x*force[0])
					      *cavity_data->af[m+j]
					      +(multi_x_b+pos_xb*force[0])
					      *cavity_data->bf[m+j])
	    *factor/beam->particle[m+j].energy;
	}
	cavity_data->ypos[k]=pos_y;
	cavity_data->yposb[k]=pos_yb;
	cavity_data->xpos[k]=pos_x;
	cavity_data->xposb[k]=pos_xb;
      }
    }
  }  break;
  case 123: {
    int n=beam->slices_per_bunch;
    int m=beam->macroparticles;
    double *rho_y=beam->rho_y[0];
    double *rho_x=beam->rho_x[0];
    for (int i=0;i<n;i++){
      double pos_y=0.0;
      double pos_x=0.0;
      for (int j=0;j<m;j++){
	pos_y+=beam->particle[i*m+j].y*beam->particle[i*m+j].wgt;
	pos_x+=beam->particle[i*m+j].x*beam->particle[i*m+j].wgt;
      }
      rho_y[i]=pos_y;
      rho_x[i]=pos_x;
    }
    double *p=beam->field[0].kick;
    for (int i=0;i<n;i++){
      double sumy=0.0;
      double sumx=0.0;
      for (int j=0;j<=i;j++){
	sumy+=rho_y[j]* *p;
	sumx+=rho_x[j]* *p;
	p++;
      }
      for (int j=0;j<m;j++){
	beam->particle[i*m+j].yp+=sumy*factor/beam->particle[i*m+j].energy;
	beam->particle[i*m+j].xp+=sumx*factor/beam->particle[i*m+j].energy;
      }
    }
  } break;
  default:
    placet_cout << WARNING << "wakefield kick mode not known!" << endmsg;
  }
}
