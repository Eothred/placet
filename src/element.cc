#ifdef _OPENMP
#include <omp.h>
#endif
#include <limits>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <string>

#include <tcl.h>
#include <tk.h>

#include "element.h"

#define PLACET
#include "placet.h"
#include "beam.h"
#include "particle.h"
#include "short_range.h"
#include "spline.h"
#include "stream.hh"
#include "structures_def.h"

/* Function prototypes */

#include "placeti3.h"
#include "andrea.h"
#include "function.h"
#include "conversion.h"
#include "rmatrix.h"

extern INTER_DATA_STRUCT inter_data;

double ELEMENT::CORRECTOR::vary(double dx )
{
  if (enabled) {
    if (step_size!=0.0)
      dx=rint(dx/step_size)*step_size;
    leverage.set(leverage.get_double()+dx);
  }
  return dx;
}

void ELEMENT::init_defaults()
{
  aperture.inverse_x = 1.; //1./DEFAULT_APERTURE_X;
  aperture.inverse_y = 1.; //1./DEFAULT_APERTURE_Y;
  aperture.type = APERTURE::NONE;
  aperture.weight = 0.0;

  offset.x=0.0;
  offset.y=0.0;
  offset.xp=0.0;
  offset.yp=0.0;
  offset.roll=0.0;

  short_range.wake=NULL;

  geometry.z=0.0;
  geometry.length=0.0;

  flags.synrad=inter_data.synrad;
  flags.thin_lens=inter_data.thin_lens;
  flags.longitudinal=inter_data.longitudinal;
  
  callback=NULL;

  ref_energy=Andrea::ReferenceEnergy;
}

void ELEMENT::init_attributes()
{
#ifdef HTGEN
  attributes.reserve(29);
#else
  attributes.reserve(22);
#endif

  attributes.add("name", "Element name [STRING]", OPT_STRING, &id.name);
  attributes.add("s", "Longitudinal position [m] [READ-ONLY]", OPT_DOUBLE, &geometry.s);
  attributes.add("x", "Horizontal offset [um]", OPT_DOUBLE, &offset.x);
  attributes.add("y", "Vertical offset [um]", OPT_DOUBLE, &offset.y);
  attributes.add("xp", "Horizontal offset in angle [urad]", OPT_DOUBLE, &offset.xp);
  attributes.add("yp", "Vertical offset in angle [urad]", OPT_DOUBLE, &offset.yp);
  attributes.add("roll", "Roll angle [urad]", OPT_DOUBLE, &offset.roll, ux2x, x2ux);
  attributes.add("length", "Element length [m]", OPT_DOUBLE, &geometry.length);
  attributes.add("synrad", "Synchrotron Radiation emission [BOOL]", OPT_BOOL, &flags.synrad);
  attributes.add("six_dim", "Longitudinal motion (6d) [BOOL]", OPT_BOOL, &flags.longitudinal);  
  attributes.add("thin_lens", "Number of steps for Thin Lens approximation [INT]", OPT_INT, &flags.thin_lens);
  attributes.add("e0", "Reference energy [GeV]", OPT_DOUBLE, &ref_energy);

#ifdef HTGEN
  attributes.add("gas_A", "Atomic number [amu]", OPT_DOUBLE, &material.a);
  attributes.add("gas_Z", "Atomic charge [e]", OPT_DOUBLE, &material.z);
  attributes.add("gas_pressure", "Gas pressure [torr]", OPT_DOUBLE, &material.p);
  attributes.add("gas_temperature", "Gas temperature [K]", OPT_DOUBLE, &material.t);
  attributes.add("gas_minimum_theta", "Minimum scattering angle [rad]", OPT_DOUBLE, &material.theta);
  attributes.add("gas_minimum_k", "Minimal photon energy (E_photon/E_beam)", OPT_DOUBLE, &material.k);
  attributes.add("radiation_length", "Radiation length [m]", OPT_DOUBLE, &material.X0);
#endif

  attributes.add("aperture_x", "Horizontal aperture [m]", OPT_DOUBLE, &aperture.inverse_x, inverse);
  attributes.add("aperture_y", "Vertical aperture [m]", OPT_DOUBLE, &aperture.inverse_y, inverse);
  attributes.add("aperture_losses", "Fraction of lost particles [#] [READ-ONLY]", OPT_DOUBLE, &aperture.weight);
  attributes.add("aperture_shape", "Aperture shape [STRING]", OPT_STRING, this,
		 Set(&ELEMENT::set_aperture_type_str), 
		 Get(&ELEMENT::get_aperture_type_str));

  attributes.add("tclcall_entrance", "Tcl/Tk callback at element entrance [STRING]", OPT_STRING, &tcl_call.entrance);
  attributes.add("tclcall_exit", "Tcl/Tk callback at element exit [STRING]", OPT_STRING, &tcl_call.exit);

  attributes.add("short_range_wake", "Name of short range wakefield as defined with tk_ShortRangeWake", OPT_STRING, &short_range.name);
}

ELEMENT::ELEMENT(double l )
{
  init_defaults();
  init_attributes();
  geometry.length=l;
}

ELEMENT::ELEMENT(int &argc, char **argv )
{
  init_defaults();
  init_attributes();
  set_attributes(argc,argv);
}

int ELEMENT::set_aperture_type_str(const char *shape )
{
  if(!strcmp(shape,"rectangular")) {
    aperture.type = APERTURE::RECTANGULAR;
  } else if(!strcmp(shape,"oval")) {
    aperture.type = APERTURE::ELLIPTIC;
  } else if(!strcmp(shape,"elliptic")) {
    aperture.type = APERTURE::ELLIPTIC;
  } else if(!strcmp(shape,"circular")) {
    aperture.type = APERTURE::CIRCULAR;
  } else if(!strcmp(shape,"none")) {
    aperture.type = APERTURE::NONE;
  } else {
    placet_cout << ERROR << " `"<<shape<<"' is not a valid aperture type. Valid aperture types are: `rectangular', `oval', `elliptic', `circular' or `none'." << endmsg;
    aperture.type = APERTURE::NONE;
    return 1; 
  }
  return 0;
}

int ELEMENT::set_aperture_type(APERTURE::TYPE type )
{
  aperture.type = type;
  return 0;
}

int ELEMENT::set_aperture(APERTURE::TYPE type, double x, double y )
{
  set_aperture_dim(x, y);
  return set_aperture_type(type);
}

int ELEMENT::set_aperture(const char *shape, double x, double y )
{
  set_aperture_dim(x, y);
  return set_aperture_type_str(shape);
}

void ELEMENT::set_aperture_dim(double par1, double par2)
{
  if (fabs(par1) > std::numeric_limits<double>::epsilon())  {
    aperture.inverse_x = 1./par1;
  } else {
    aperture.inverse_x = std::numeric_limits<double>::infinity();
  }
  if (fabs(par2) > std::numeric_limits<double>::epsilon())  {
    aperture.inverse_y = 1./par2;
  } else {
    aperture.inverse_y = std::numeric_limits<double>::infinity();
  }
}

void ELEMENT::set_aperture_dim(double par1 )
{
  if (fabs(par1) > std::numeric_limits<double>::epsilon())  {
    aperture.inverse_x = 1./par1;
  } else {
    aperture.inverse_x = std::numeric_limits<double>::infinity();
  }
}

double ELEMENT::check_aperture(BEAM *beam) {
  if (aperture.type==APERTURE::NONE) return 0.0;
  double weight=0.0;
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
    if (!particle.is_lost() && is_lost(particle)) {
      weight+=fabs(particle.wgt);
      particle.set_lost();
    }
  }
  return weight;
}

void ELEMENT::apply_short_range_wakefield(BEAM* beam) {
  if (short_range.wake || !short_range.name.empty()) {
    if (!short_range.wake) {
      short_range.wake = get_short_range(short_range.name.c_str());
    }
    if (short_range.wake) {
      short_range.wake->apply_wakefield_spline(beam, geometry.length);
    }
  }
}

bool ELEMENT::is_lost(const PARTICLE &particle )
{
  if (aperture.type==APERTURE::NONE) {
    return false;
  }
  
  if (aperture.type==APERTURE::RECTANGULAR) {
    return (fabs(particle.x)*aperture.inverse_x>1e6) || (fabs(particle.y)*aperture.inverse_y>1e6);
  }
  
  if (aperture.type==APERTURE::ELLIPTIC) {
    double r1=particle.x*aperture.inverse_x*1e-6;
    double r2=particle.y*aperture.inverse_y*1e-6;
    return r1*r1+r2*r2>1.0;
  }
  // circular:
  double r1=particle.x*aperture.inverse_x*1e-6;
  double r2=particle.y*aperture.inverse_x*1e-6;
  return r1*r1+r2*r2>1.0;
}

void ELEMENT::check_beam_is_lost(BEAM *beam) {
  if (beam->is_lost) return;
  double wsum=0.0;
  int i=0;
  while (i<beam->slices && wsum < std::numeric_limits<double>::epsilon()) {
    PARTICLE &particle=beam->particle[i];
    wsum+=particle.wgt;
    i++;
  }
  if (fabs(wsum)<std::numeric_limits<double>::epsilon()) {
    placet_printf(WARNING, "%-15s the beam is lost in element '%s' ('%s').\n", "WARNING", id.name.c_str(), typeid(*this).name());
    beam->is_lost=true;
  }
}

/**********************************************************************/
/*                      Element methods                               */
/**********************************************************************/

IStream &operator>>(IStream &stream, ELEMENT &element )
{
  return stream >> element.id.name
		>> element.id.number
		>> element.offset.x
		>> element.offset.xp
		>> element.offset.y
		>> element.offset.yp
		>> element.geometry.length
		>> element.ref_energy;
}

OStream &operator<<(OStream &stream, const ELEMENT &element )
{
  if (element.ref_energy == -1) fputs("warning: Reference energy has not been set: use SetReferenceEnergy to set it\n", stderr);
    return stream << std::string(element.id.name)
		<< int(element.id.number)
		<< double(element.offset.x)
		<< double(element.offset.xp)
		<< double(element.offset.y)
		<< double(element.offset.yp)
		<< double(element.geometry.length)
		<< double(element.ref_energy);
}

void ELEMENT::survey_print(FILE *f,double *lsum)const
{
  fprintf(f,"%s %12.6f %16.9g %16.9g %16.9g\n","elem", geometry.length,0.0,0.0,0.0);
  fprintf(f,"%16.9g %16.9g %16.9g %16.9g %16.9g \n",0.0,0.0,0.0,0.0,0.0);
  fprintf(f,"%16.9e %16.9e %16.9e %16.9e\n",0.0,0.0,0.0,*lsum);
  fprintf(f,"%16.9e %16.9e %16.9e\n",0.0,0.0,0.0);
  *lsum+=geometry.length;
}

void ELEMENT::print(FILE *file)const
{
  fprintf(file,"%s %s %g\n",typeid(*this).name(), get_name(), get_length());
}

void ELEMENT::_drift_step(BEAM *beam, double factor, bool is_6d, bool is_sliced, bool do_wake)
{
  double flength = factor*geometry.length;
  if (fabs(flength)<std::numeric_limits<double>::epsilon()) return;
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
    particle.x+=flength*particle.xp;
    particle.y+=flength*particle.yp;
    if (is_6d) 
        particle.z+=flength*0.5e-6*(particle.xp*particle.xp+particle.yp*particle.yp);
    if (is_sliced) {
        drift_sigma_matrix(beam->sigma[i],flength);
        drift_sigma_matrix(beam->sigma_xx[i],flength);
    if (is_6d)
        drift_sigma_matrix(beam->sigma_xy[i],flength);
    }
  }  
  
  if(do_wake) {
  // Applying CSR wake kick
  //   calculate length factor
  if( beam->csrwake->wake_enabled ) {
    // disable driftwake kick if L > 10*attenuation_length
    if( beam->csrwake->distance_from_sbend > 10*beam->csrwake->attenuation_length ) {
      beam->csrwake->wake_enabled = 0;
    } else {
      //   calculate length factor
      if( beam->csrwake->attenuation_length > 0.0 ) {
        double lengthfactor = ( beam->csrwake->attenuation_length * exp(-beam->csrwake->distance_from_sbend /  beam->csrwake->attenuation_length ) ) * (1 - exp(-geometry.length /  beam->csrwake->attenuation_length) );
        int n_particles = 0;
        for (int nbin=0; nbin<beam->csrwake->nbins; nbin++) {
          double dE_ds_bin=(beam->csrwake->terminal_dE_ds[beam->csrwake->nbins - 1 - nbin]) * lengthfactor;
          for(int i=0; i<beam->csrwake->terminal_nlambda[nbin]; i++) {
            beam->particle[n_particles++].energy += dE_ds_bin; // apply kick [GeV]
          }
        }
      }
    }
  }
  }
}

void ELEMENT::drift_step_4d_0(BEAM *beam, double factor )
{
  _drift_step(beam, factor, false, false, true);
}

void ELEMENT::drift_step_4d(BEAM *beam, double factor )
{
  _drift_step(beam, factor, false, true, false);
}

void ELEMENT::drift_step_6d_0(BEAM *beam, double factor )
{
  _drift_step(beam, factor, true, false, false);
}

void ELEMENT::drift_step_6d(BEAM *beam, double factor )
{
  _drift_step(beam, factor, true, true, false);
}

void ELEMENT::step_partial(BEAM *beam, double l, void (ELEMENT::*step_function)(BEAM*) )
{
  double _length=geometry.length;
  geometry.length*=l;
  (this->*step_function)(beam);
  geometry.length=_length;
}

void ELEMENT::step_4d(BEAM *beam, double l ) { step_partial(beam, l, &ELEMENT::step_4d); };
void ELEMENT::step_4d_0(BEAM *beam, double l ) { step_partial(beam, l, &ELEMENT::step_4d_0); };
void ELEMENT::step_4d_sr(BEAM *beam, double l )  { step_partial(beam, l, &ELEMENT::step_4d_sr); };
void ELEMENT::step_4d_sr_0(BEAM *beam, double l )  { step_partial(beam, l, &ELEMENT::step_4d_sr_0); };

void ELEMENT::step_6d(BEAM *beam, double l )  { step_partial(beam, l, &ELEMENT::step_6d); };
void ELEMENT::step_6d_0(BEAM *beam, double l )  { step_partial(beam, l, &ELEMENT::step_6d_0); };
void ELEMENT::step_6d_sr(BEAM *beam, double l )  { step_partial(beam, l, &ELEMENT::step_6d_sr); };
void ELEMENT::step_6d_sr_0(BEAM *beam, double l )  { step_partial(beam, l, &ELEMENT::step_6d_sr_0); };

void ELEMENT::drift_step_twiss(BEAM *beam,FILE *file,double step,int j,
		 double s,int n1,int n2, void (*callback0)(FILE*,BEAM*,int,double,int,int))
{
  int nstep=int(geometry.length/step)+1;
  double ilength=geometry.length/nstep;
  if (callback0) callback0(file,beam,j,s,n1,n2);
  for (int istep=0;istep<nstep;istep++) {
    s+=ilength;
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      drift_sigma_matrix(beam->sigma[i],ilength);
      drift_sigma_matrix(beam->sigma_xx[i],ilength);
      drift_sigma_matrix(beam->sigma_xy[i],ilength);
    }
    if (callback0) callback0(file,beam,j,s,n1,n2);
  }
}

void ELEMENT::drift_step_twiss_0(BEAM *beam,FILE *file,double step,int j,
		 double s, int n1,int n2, void (*callback0)(FILE*,BEAM*,int,double,int,int))
{
  int nstep=int(geometry.length/step)+1;
  double l=geometry.length;
  geometry.length/=nstep;
  if (callback0) callback0(file,beam,j,s,n1,n2);
  for (int istep=0;istep<nstep;istep++) {
    s+=geometry.length;
    if (flags.thin_lens) {
      static bool warned = false;
      if (!warned) {
	placet_printf(WARNING," ELEMENT::drift_step_twiss_0: option thin_lens not implemented, normal track called \n");
	warned = true;
      }
      if (flags.longitudinal) {
        if (flags.synrad) {
	  placet_printf(ERROR," ELEMENT::drift_step_twiss_0: cannot track with synchrotron radiation in the drift \n");
	  exit(1);
	}
        else              drift_step_6d_0(beam);
      } else {
        if (flags.synrad) {
	  placet_printf(ERROR," ELEMENT::drift_step_twiss_0: cannot track with synchrotron radiation in the drift \n");
	  exit(1);
	}
        else              drift_step_4d_0(beam);
      }
    } else {
      if (flags.longitudinal) {
	if (flags.synrad) {
	  placet_printf(ERROR," ELEMENT::drift_step_twiss_0: cannot track with synchrotron radiation in the drift \n");
	  exit(1);	  
	}
	else              drift_step_6d_0(beam);
      } else {
	if (flags.synrad) {
	  placet_printf(ERROR," ELEMENT::drift_step_twiss_0: cannot track with synchrotron radiation in the drift \n");
	  exit(1);	  	  
	}
	else              drift_step_4d_0(beam);
      }   
    }
    if (callback0) callback0(file,beam,j,s,n1,n2);
  }
  geometry.length=l;
}

void ELEMENT::step_in(BEAM *beam )
{
  const double eps=std::numeric_limits<double>::epsilon();
  double _offset;
  if (fabs(_offset=0.5*geometry.length*offset.xp-offset.x)>eps || fabs(offset.xp)>eps) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.x +=_offset;
      particle.xp-= offset.xp;
    }
#ifdef HTGEN
    // shift halo particles in x as beam particles to take offsets from misalignment into account
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.x +=_offset;
      particle_sec.xp-= offset.xp;
    }
#endif
  }
  if (fabs(_offset=0.5*geometry.length*offset.yp-offset.y)>eps || fabs(offset.yp)>eps) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.y +=_offset;
      particle.yp-= offset.yp;
    }
#ifdef HTGEN
    // shift halo particles in y as beam particles to take offsets from misalignment into account
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.y +=_offset;
      particle_sec.yp-= offset.yp;
    }
#endif
  }
  bunch_rotate(beam,offset.roll); 
}

void ELEMENT::step_out(BEAM *beam )
{
  const double eps=std::numeric_limits<double>::epsilon();
  bunch_rotate(beam,-offset.roll); 
  double _offset;
  if (fabs(_offset=0.5*geometry.length*offset.yp+offset.y)>eps || fabs(offset.yp)>eps) {
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      particle.y +=_offset;
      particle.yp+= offset.yp;
    }
#ifdef HTGEN
    // shift halo particles in y as beam particles to take offsets from misalignment into account
    for (int j=0;j<beam->nhalo;j++){
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.y +=_offset;
      particle_sec.yp+= offset.yp;
    }
#endif
  }
  if (fabs(_offset=0.5*geometry.length*offset.xp+offset.x)>eps || fabs(offset.xp)>eps) {
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      particle.x +=_offset;
      particle.xp+= offset.xp;
    }
#ifdef HTGEN
    // shift halo particles in x as beam particles to take offsets from misalignment into account
    for (int j=0;j<beam->nhalo;j++){
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.x +=_offset;
      particle_sec.xp+= offset.xp;
    }
#endif
  }
}

void ELEMENT::track(BEAM *beam )
{
  step_in(beam);
  if (tcl_call.entrance!="") {
    inter_data.bunch=beam;
    if (Tcl_Eval(beamline_survey_hook_interp,tcl_call.entrance.c_str())==TCL_ERROR) {
      placet_printf(ERROR,"Error in element '%s' TclCall subroutine %s: %s\n", id.name.c_str(),  tcl_call.entrance.c_str(),Tcl_GetStringResult(beamline_survey_hook_interp));
      fflush(stderr);
      exit(1);
    }
  }
  aperture.weight=check_aperture(beam);

  bool trk=false;
  if(is_quad()) trk=track_with_error(beam);
  if(!trk){
    if (placet_switch.first_order || beam->particle_beam) {
      if (flags.thin_lens) {
	if (flags.longitudinal) {
	  if (flags.synrad) step_6d_tl_sr_0(beam);
	  else              step_6d_tl_0(beam);
	} else {
	  if (flags.synrad) step_4d_tl_sr_0(beam);
	  else              step_4d_tl_0(beam);
	}
      } else {
	if (flags.longitudinal) {
	  if (flags.synrad) step_6d_sr_0(beam);
	  else              step_6d_0(beam);
	} else {
	  if (flags.synrad) step_4d_sr_0(beam);
	  else              step_4d_0(beam);
	}
      }
    } else {
      if (flags.thin_lens) {
	if (flags.longitudinal) {
	  if (flags.synrad) step_6d_tl_sr(beam);
	  else              step_6d_tl(beam);
	} else {
	  if (flags.synrad) step_4d_tl_sr(beam);
	  else              step_4d_tl(beam);
	}
      } else {
	if (flags.longitudinal) {
	  if (flags.synrad) step_6d_sr(beam);
	  else              step_6d(beam);
	} else {
	  if (flags.synrad) step_4d_sr(beam);
	  else              step_4d(beam);
	}
      }
    }
  }
  /// apply short range wakefield effect (better in middle)
  apply_short_range_wakefield(beam);
  aperture.weight+=check_aperture(beam);
  check_beam_is_lost(beam);
  if (tcl_call.exit!="") {
    inter_data.bunch=beam;
    if (Tcl_Eval(beamline_survey_hook_interp,tcl_call.exit.c_str())==TCL_ERROR) {
      placet_printf(ERROR,"Error in element '%s' TclCall subroutine %s: %s\n", id.name.c_str(),  tcl_call.exit.c_str(),Tcl_GetStringResult(beamline_survey_hook_interp));
      fflush(stderr);
      exit(1);
    }
  }
  step_out(beam);
  beam->s+=geometry.length;
}

void ELEMENT::track_0(BEAM *beam )
{
  step_in(beam);
  if (tcl_call.entrance!="") {
    inter_data.bunch=beam;
    if (Tcl_Eval(beamline_survey_hook_interp,tcl_call.entrance.c_str())==TCL_ERROR) {
      placet_printf(ERROR,"Error in element '%s' TclCall subroutine %s: %s\n", id.name.c_str(),  tcl_call.entrance.c_str(),Tcl_GetStringResult(beamline_survey_hook_interp));
      fflush(stderr);
      exit(1);
    }
  }
  aperture.weight=check_aperture(beam);
  if (flags.thin_lens) {
    if (flags.longitudinal) {
      if (flags.synrad) step_6d_tl_sr_0(beam);
      else              step_6d_tl_0(beam);
    } else {
      if (flags.synrad) step_4d_tl_sr_0(beam);
      else              step_4d_tl_0(beam);
    }
  } else {
    if (flags.longitudinal) {
      if (flags.synrad) step_6d_sr_0(beam);
      else              step_6d_0(beam);
    } else {
      if (flags.synrad) step_4d_sr_0(beam);
      else              step_4d_0(beam);
    }
  }
  /// apply short range wakefield effect (better in middle)
  apply_short_range_wakefield(beam);
  aperture.weight+=check_aperture(beam);
  check_beam_is_lost(beam);
  if (tcl_call.exit!="") {
    inter_data.bunch=beam;
    if (Tcl_Eval(beamline_survey_hook_interp,tcl_call.exit.c_str())==TCL_ERROR) {
      placet_printf(ERROR,"Error in element '%s' TclCall subroutine %s: %s\n", id.name.c_str(),  tcl_call.exit.c_str(),Tcl_GetStringResult(beamline_survey_hook_interp));
      fflush(stderr);
      exit(1);
    }
  }
  step_out(beam);
  beam->s+=geometry.length;
}

void ELEMENT::enable_hcorr(const char *leverage_name, double step_size )
{
  if (hcorr.enabled) {
    disable_hcorr();
  }
  if (option_info *leverage=attributes.find(leverage_name)) {
    hcorr = CORRECTOR(*leverage, step_size);
    attributes.add("hcorrector", "Horizontal correction leverage [STRING] [READ-ONLY]", OPT_CHAR_PTR, const_cast<char*>(leverage->get_name()));
    attributes.add("hcorrector_step_size", "Horizontal corrector step size [DOUBLE]", OPT_DOUBLE, &hcorr.step_size);
  } else {
    placet_printf(ERROR, "ERROR: leverage '%s' not found.\n", leverage_name);
  }
}

void ELEMENT::enable_vcorr(const char *leverage_name, double step_size )
{
  if (vcorr.enabled) {
    disable_vcorr();
  }
  if (option_info *leverage=attributes.find(leverage_name)) {
    vcorr = CORRECTOR(*leverage, step_size);
    attributes.add("vcorrector", "Vertical correction leverage [STRING] [READ-ONLY]", OPT_CHAR_PTR, const_cast<char*>(leverage->get_name()));
    attributes.add("vcorrector_step_size", "Vertical corrector step size [DOUBLE]", OPT_DOUBLE, &vcorr.step_size);
  } else {
    placet_printf(ERROR, "ERROR: leverage '%s' not found.\n", leverage_name);
  }
}

void ELEMENT::disable_hcorr()
{
  if (hcorr.enabled) {
    attributes.remove("hcorrector");
    attributes.remove("hcorrector_step_size");
    hcorr.enabled = false;
  }
}

void ELEMENT::disable_vcorr()
{
  if (vcorr.enabled) {
    attributes.remove("vcorrector");
    attributes.remove("vcorrector_step_size");
    vcorr.enabled = false;
  }
}

double ELEMENT::vary_hcorr(double dx )
{
  if (hcorr.enabled)
    return hcorr.vary(dx);
  placet_printf(WARNING, "WARNING: element '%s' is not an horizontal corrector.\n", id.name.c_str());
  return 0.0;
}

double ELEMENT::vary_vcorr(double dx )
{
  if (vcorr.enabled)
    return vcorr.vary(dx);
  placet_printf(WARNING, "WARNING: element '%s' is not a vertical corrector.\n", id.name.c_str());
  return 0.0;
}

Matrix<6,6> ELEMENT::get_transfer_matrix_6d(double /* _energy */)  const
{
  return Matrix<6,6>(
   1.0, geometry.length, 0.0, 0.0, 0.0, 0.0,
   0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
   0.0, 0.0, 1.0, geometry.length, 0.0, 0.0,
   0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
   0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
   0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
}

void ELEMENT::step_4d(BEAM * ) { placet_cout << ERROR << "Cannot track a sliced beam through element-type '" << typeid(*this).name() << endmsg; exit(1); }
void ELEMENT::step_4d_0(BEAM * ) { placet_cout << ERROR << "Cannot track a particle beam through element-type '" << typeid(*this).name() << endmsg; exit(1); }
void ELEMENT::step_4d_sr(BEAM * ) { placet_cout << ERROR << "Cannot track a sliced beam through element-type '" << typeid(*this).name() << "' with synrad emission" << endmsg; exit(1); }
void ELEMENT::step_4d_sr_0(BEAM * ) { placet_cout << ERROR << "Cannot track a particle beam through element-type '" << typeid(*this).name() << "' with synrad emission" << endmsg; exit(1); }

void ELEMENT::step_6d(BEAM *b ) { placet_cout << ERROR << "Cannot track a sliced beam through element-type '" << typeid(*this).name() << "' with longitudinal motion" << endmsg; step_4d(b); }
void ELEMENT::step_6d_0(BEAM *b ) { placet_cout << ERROR << "Cannot track a particle beam through element-type '" << typeid(*this).name() << "' with longitudinal motion" << endmsg; step_4d_0(b); }
void ELEMENT::step_6d_sr(BEAM *b ) { placet_cout << ERROR << "Cannot track a sliced beam through element-type '" << typeid(*this).name() << "' with synrad emission and longitudinal motion" << endmsg; step_4d_sr(b); }
void ELEMENT::step_6d_sr_0(BEAM *b ) { placet_cout << ERROR << "Cannot track a particle beam through element-type '" << typeid(*this).name() << "' with synrad emission and longitudinal motion" << endmsg; step_4d_sr_0(b); }
  
// thin lens tracking using leapfrog
// defines ELEMENT::step_4/6d_tl(_sr)(_0)
#include "element_thin_lens.cc"

#define SLICED_BEAM
#include "element_thin_lens.cc"
#undef SLICED_BEAM

void ELEMENT::drift_sigma_matrix(R_MATRIX &a, double l) const
{
  /** optimised method to calculate the new sigma matrix for a drift space of length l
      more specifically: a = r1 * a * r2, with:
      r1 = [1 l; 0 1], r2 = [1 0;l 1]
      
      better inlined (not possible for now, as RMATRIX, which inherits from ELEMENT, and R_MATRIX are in same file.
      probably better in BEAM class
  */
  
  double a11 = a.r11, a12 = a.r12, a21 = a.r21;
  double l_a22 = l * a.r22;
  a.r11 = a11 + l*(a12+a21) + l*l_a22;
  a.r12 = a12 + l_a22;
  a.r21 = a21 + l_a22;
  // a.r22 = a.r22; (not necessary)
}

