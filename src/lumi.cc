#include <cctype>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <tcl.h>

#include "placet.h"
#include "lumi.h"

double lumi_calc_0(double z[],double w1[],double w2[],int n,
		   double y1[],double yp1[],
		   double y2[],double yp2[],double beta_y)
{
  int i1,i2;
  double z0,w0,wsum=0.0,sy,dy,lumi=0.0;
  //  double sw1=0.0,sw2=0.0,sy1=0.0,sy2=0.0,syp1=0.0,syp2=0.0;
      
  //for (i1=0;i1<n;i1++){
    //y2[i1] *= -1;
    //y2[i1] += 2.;
  //yp2[i1] *= -1; 
  //}
   
  
  /*
  for (i1=0;i1<n;i1++){
    sw1 += w1[i1];
    sy1 += y1[i1] * w1[i1];
    syp1 += yp1[i1] * w1[i1]; 
    sw2 += w2[i1];
    sy2 += y2[i1] * w2[i1];
    syp2 += yp2[i1] * w2[i1]; 
  } 
  sy1 /= sw1;
  syp1 /= sw1;
  sy2 /= sw2;
  syp2 /= sw2;  

  placet_printf(INFO,"sy1 %g sy2 %g\n",sy1,sy2);
  */

  /*
  for (i1=0;i1<n;i1++) {
    y1[i1] -= sy1;
    yp1[i1] -= syp1;
    y2[i1] -= sy2;
    yp2[i1] -= syp2; 
  }   
  */

  for (i1=0;i1<n;i1++) {
    for (i2=0;i2<n;i2++) {
      z0=0.5*(z[i1]-z[i2])/beta_y;
      w0=w1[i1]*w2[i2];
      wsum+=w0;
      sy=sqrt(1.0+z0*z0);
      dy=y1[i1]+yp1[i1]*z0-(y2[i2]-yp2[i2]*z0);
      dy/=sy;
      dy *=dy;
      lumi+=w0*exp(-0.25*dy)/sy;

      /* 
      if(i1==0&&i2==0)placet_printf(INFO,"y1 %g sy1 %g y2 %g sy2 %g z0 %g\n",
                             y1[i1],yp1[i1],y2[i2],yp2[i2],z0);

      if(i1==0&&i2==0)placet_printf(INFO,"sy is %g and dy %g and lumi %g\n",
                              sy,dy,exp(-0.25*dy*dy)/sy);
      */

    }
  }
  lumi/=wsum;
  return lumi;
}

double Comp_Lumi_0(char *b_1,char *b_2,int center)
{
  int i,n,n1=0,n2=0;
  double *x1,*xp1,*x2,*xp2,*y1,*yp1,*y2,*yp2,*w1,*w2,*z;
  double *rx11_1,*rx22_1,*ry11_1,*ry22_1;
  double *rx11_2,*rx22_2,*ry11_2,*ry22_2;
  double beta_y=150.0,l,l0;
  double ym1=0.0,sym1=0.0,xm1=0.0,sxm1=0.0,wsum1=0.0;
  double ym2=0.0,sym2=0.0,xm2=0.0,sxm2=0.0,wsum2=0.0;
  FILE *f1,*f2;
  char *point,buffer[1000];
  char ch;

  f1=read_file(b_1);
  f2=read_file(b_2);

  //point=fgets(buffer,1000,f1);
  //n=strtol(buffer,NULL,10);

  //point=fgets(buffer,1000,f2);

  //if (n!=strtol(buffer,NULL,10)){
  //  placet_printf(INFO,"files are not compatible\n");
  //  exit(1);
  // }


  while(!feof(f1)){ 
    fgets(buffer,1000,f1);
    sscanf(buffer,"%c",&ch);
    if(isxdigit(ch)||ch=='-') n1 += 1;
  }
  rewind(f1);

  while(!feof(f2)){ 
    fgets(buffer,1000,f2);
    sscanf(buffer,"%c",&ch);
    if(isxdigit(ch)||ch=='-') n2 += 1;
  }  
  rewind(f2);

  if(n1!=n2){
    placet_printf(INFO,"files are not compatible\n");
    exit(1);
  }


  n=n1;
  z=(double*)malloc(sizeof(double)*n);
  w1=(double*)malloc(sizeof(double)*n);
  w2=(double*)malloc(sizeof(double)*n);
  x1=(double*)malloc(sizeof(double)*n);
  xp1=(double*)malloc(sizeof(double)*n);
  x2=(double*)malloc(sizeof(double)*n);
  xp2=(double*)malloc(sizeof(double)*n);
  y1=(double*)malloc(sizeof(double)*n);
  yp1=(double*)malloc(sizeof(double)*n);
  y2=(double*)malloc(sizeof(double)*n);
  yp2=(double*)malloc(sizeof(double)*n);

  rx11_1=(double*)malloc(sizeof(double)*n);
  rx22_1=(double*)malloc(sizeof(double)*n);
  rx11_2=(double*)malloc(sizeof(double)*n);
  rx22_2=(double*)malloc(sizeof(double)*n);
  ry11_1=(double*)malloc(sizeof(double)*n);
  ry22_1=(double*)malloc(sizeof(double)*n);
  ry11_2=(double*)malloc(sizeof(double)*n);
  ry22_2=(double*)malloc(sizeof(double)*n);




  for(i=0;i<n;i++){

    point=fgets(buffer,1000,f1);
    z[i]=strtod(point,&point);
    w1[i]=strtod(point,&point);
    strtod(point,&point);
    x1[i]=strtod(point,&point);
    xp1[i]=strtod(point,&point);
    y1[i]=strtod(point,&point);
    yp1[i]=strtod(point,&point);

    rx11_1[i]=strtod(point,&point);
    strtod(point,&point);
    rx22_1[i]=strtod(point,&point);
    ry11_1[i]=strtod(point,&point);
    strtod(point,&point);
    ry22_1[i]=strtod(point,&point);

    point=fgets(buffer,1000,f2);
    z[i]=strtod(point,&point);

    w2[i]=strtod(point,&point);
    strtod(point,&point);
    x2[i]=strtod(point,&point);
    xp2[i]=strtod(point,&point);
    y2[i]=strtod(point,&point);
    yp2[i]=strtod(point,&point);

    rx11_2[i]=strtod(point,&point);
    strtod(point,&point);
    rx22_2[i]=strtod(point,&point);
    ry11_2[i]=strtod(point,&point);
    strtod(point,&point);
    ry22_2[i]=strtod(point,&point);

    //if(i==1) placet_printf(INFO,"Before y1 %g and %g \n",y1[i],yp1[i]);
    //if(i==1) placet_printf(INFO,"Before y2 %g and %g \n",y2[i],yp2[i]);

    //y1[i] /= sqrt(ry11_1[i]);
    //yp1[i] /= sqrt(ry22_1[i]);
    //y2[i] /= sqrt(ry11_2[i]);
    //yp2[i] /= sqrt(ry22_2[i]);

    //x1[i] /= sqrt(rx11_1[i]);
    //xp1[i] /= sqrt(rx22_1[i]);
    //x2[i] /= sqrt(rx11_2[i]);
    //xp2[i] /= sqrt(rx22_2[i]);

    
    if(center==0){
      ym1 += w1[i]*y1[i];
      sym1 += w1[i]*yp1[i];
      wsum1 += w1[i];

      ym2 += w2[i]*y2[i];
      sym2 += w2[i]*yp2[i]; 
      wsum2 += w2[i];

      xm1 += w1[i]*x1[i];
      sxm1 += w1[i]*xp1[i];  
      xm2 += w2[i]*x2[i];
      sxm2 += w2[i]*xp2[i];  
    }

    //if(i==1) placet_printf(INFO,"After y1 %g and %g \n",y1[i],yp1[i]);
    //if(i==1) placet_printf(INFO,"After y2 %g and %g \n",y2[i],yp2[i]);
  }

  close_file(f1);
  close_file(f2);

  /// Compute the luminosity when the beam is brought back to the beamline
  if(center==0){
    ym1 /= wsum1;
    sym1 /= wsum1;
    ym2 /= wsum2;
    sym2 /= wsum2;

    xm1 /= wsum1;
    sxm1 /= wsum1;
    xm2 /= wsum2;
    sxm2 /= wsum2;

    for(i=0;i<n;i++){
      y1[i] -= ym1;
      yp1[i] -= sym1; 
      y2[i] -= ym2;
      yp2[i] -= sym2; 

      x1[i] -= xm1;
      xp1[i] -= sxm1;
      x2[i] -= xm2;
      xp2[i] -= sxm2;    
    }
  }

  for(i=0;i<n;i++){
    y1[i] /= sqrt(ry11_1[i]);
    yp1[i] /= sqrt(ry22_1[i]);
    y2[i] /= sqrt(ry11_2[i]);
    yp2[i] /= sqrt(ry22_2[i]);
    
    x1[i] /= sqrt(rx11_1[i]);
    xp1[i] /= sqrt(rx22_1[i]);
    x2[i] /= sqrt(rx11_2[i]);
    xp2[i] /= sqrt(rx22_2[i]);
  }

  /// x,xp are not used !!!

  l=lumi_calc_0(z,w1,w2,n,y1,yp1,y2,yp2,beta_y);

  for (i=0;i<n;i++){
    y1[i]=0.0;
    yp1[i]=0.0;
    y2[i]=0.0;
    yp2[i]=0.0;
  }

  /*placet_printf(INFO,"\n The L_0 luminosity is %g and the relative L/L_0 is %g \n",
            lumi_calc_0(z,w1,w2,n,y1,yp1,y2,yp2,beta_y),
            l/lumi_calc_0(z,w1,w2,n,y1,yp1,y2,yp2,beta_y)); */

  l0=lumi_calc_0(z,w1,w2,n,y1,yp1,y2,yp2,beta_y);

  free(z);
  free(w1);
  free(x1);
  free(xp1);
  free(y1);
  free(yp1);
  free(rx11_1);
  free(rx22_1);
  free(ry11_1);
  free(ry22_1);

  free(w2);
  free(x2);
  free(xp2);
  free(y2);
  free(yp2);
  free(rx11_2);
  free(rx22_2);
  free(ry11_2);
  free(ry22_2);

  return l/l0;

#ifdef TWODIM   
  /// Compute  luminosity in x-y 
#endif

}

/* Emittance */

double Comp_Emitt_0(char *b_1,int /*center*/,double *em1,double *em2)
{
  int i,n,nb,ns,nm;
  double *x1,*xp1,*y1,*yp1,*w1,*z;
  double *rx11,*rx22,*rx21,*ry11,*ry22,*ry21;
  double ym1=0.0,ypm1=0.0,ym2=0.0,ypm2=0.0,ws1=0.0,ws2=0.0,e1=0.0,e2=0.0;
  double sum1_11=0.0,sum1_21=0.0,sum1_22=0.0,sum2_11=0.0,sum2_21=0.0,
    sum2_22=0.0,e;
  FILE *f1;
  char *point,buffer[1000];

  f1=read_file(b_1);

  point=fgets(buffer,1000,f1);
  nb=strtol(point,&point,10);
  ns=strtol(point,&point,10);
  nm=strtol(point,&point,10);

  n=nb*ns*nm;
  z=(double*)alloca(sizeof(double)*n);
  w1=(double*)alloca(sizeof(double)*n);
  x1=(double*)alloca(sizeof(double)*n);
  xp1=(double*)alloca(sizeof(double)*n);
  y1=(double*)alloca(sizeof(double)*n);
  yp1=(double*)alloca(sizeof(double)*n);

  rx11=(double*)alloca(sizeof(double)*n);
  rx21=(double*)alloca(sizeof(double)*n);
  rx22=(double*)alloca(sizeof(double)*n);
  ry11=(double*)alloca(sizeof(double)*n);
  ry21=(double*)alloca(sizeof(double)*n);
  ry22=(double*)alloca(sizeof(double)*n);

  for(i=0;i<n;++i){
    point=fgets(buffer,1000,f1);
    z[i]=strtod(point,&point);
    w1[i]=strtod(point,&point);
    e=strtod(point,&point);
    x1[i]=strtod(point,&point);
    xp1[i]=strtod(point,&point);
    y1[i]=strtod(point,&point);
    yp1[i]=strtod(point,&point);

    rx11[i]=strtod(point,&point);
    rx21[i]=strtod(point,&point);
    rx22[i]=strtod(point,&point);
    ry11[i]=strtod(point,&point);
    ry21[i]=strtod(point,&point);
    ry22[i]=strtod(point,&point);

    if (w1[i]>=0.0) {
      ws1+=w1[i];
      ym1+=w1[i]*y1[i];
      ypm1+=w1[i]*yp1[i];
      e1+=w1[i]*e;
    }
    else {
      ws2-=w1[i];
      ym2-=w1[i]*y1[i];
      ypm2-=w1[i]*yp1[i];
      e2+=w1[i]*e;
    }
  }

  close_file(f1);

  ym1/=ws1;
  ypm1/=ws1;
  e1/=ws1;
  if (ws2>0.0) {
    ym2/=ws2;
    ypm2/=ws2;
    e2/=ws2;
  }

  for(i=0;i<n;i++){
    if (w1[i]>=0.0) {
      y1[i] -= ym1;
      yp1[i] -= ypm1;
      sum1_11+=w1[i]*(y1[i]*y1[i]+ry11[i]);
      sum1_21+=w1[i]*(y1[i]*yp1[i]+ry21[i]);
      sum1_22+=w1[i]*(yp1[i]*yp1[i]+ry22[i]);
    }
    else {
      y1[i] -= ym2;
      yp1[i] -= ypm2;
      sum2_11-=w1[i]*(y1[i]*y1[i]+ry11[i]);
      sum2_21-=w1[i]*(y1[i]*yp1[i]+ry21[i]);
      sum2_22-=w1[i]*(yp1[i]*yp1[i]+ry22[i]);
    }
  }
  sum1_11/=ws1;
  sum1_21/=ws1;
  sum1_22/=ws1;
  if (ws2>0.0) {
    sum2_11/=ws2;
    sum2_21/=ws2;
    sum2_22/=ws2;
  }
  *em1=sqrt(sum1_11*sum1_22-sum1_21*sum1_21)*e1/EMASS*1e-12*1e9;
  *em2=sqrt(sum2_11*sum2_22-sum2_21*sum2_21)*e2/EMASS*1e-12*1e9;
  return 0.0;
}
