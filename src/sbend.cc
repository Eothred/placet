#ifdef _OPENMP
#include <omp.h>
#endif
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <cstdlib>
#include <cmath>
#include <limits>

#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "placeti3.h"
#include "structures_def.h"

#include "lattice.h"
#include "girder.h"
#include "sbend.h"
#include "photon_spectrum.h"
#include "photon.h"
#include "rmatrix.h"
#include "function.h"

// DEFINES FOR CSR
#define SI_perm 8.854e-12 

extern INTER_DATA_STRUCT inter_data;
extern PHOTON *ptracker;


void SBEND::step_partial(BEAM* beam, double l, void (ELEMENT::*step_function)(BEAM*))
{
    double _angle0=angle0;
    angle0*=l;
    ELEMENT::step_partial(beam,l,step_function);
    angle0=_angle0;
}

void SBEND::step_4d_sr_0(BEAM *beam )
{
  if (csr) {
    placet_cout << WARNING << "6D tracking will be performed for this sbend, since CSR is enabled" << endmsg;
    step_6d_sr_0(beam);
  } else {
    const double sector_length = geometry.length;
    const double sector_angle = angle0;
    size_t photons = 0;
#pragma omp parallel for
    for (int i=0; i<beam->slices; i++) {
      // reference to a particle 
      PARTICLE &particle = beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	// calculate the energy loss by synrad emission
	double s = 0.0;
	  for(;;) {
	    const double h=sector_angle/sector_length; // 1/m
	    const double kx_=h*ref_energy/particle.energy; // 1/m
	    double step_length = SYNRAD::get_free_path(sector_length * kx_, sector_length, particle.energy); //use of RNG
	    const bool done = (s+step_length>=sector_length);
	    if (done) step_length = sector_length-s;
	    if (step_length>std::numeric_limits<double>::epsilon()) {
	      const double step_angle = sector_angle * step_length / sector_length;
	      const double kyy=-k/particle.energy/geometry.length; // 1/m**2
	      const double kxx=h*kx_-kyy; // 1/m**2
	      const double delta=particle.momentum_deviation(ref_energy)*1e6; // micro
	      if (fabs(kxx)<std::numeric_limits<double>::epsilon()) {
		double __x = particle.xp*step_length + delta*step_angle*step_length;
		particle.xp += delta*step_angle;
		particle.x += __x;
	      } else if (kxx>0.) {
		const double sqrt_kxx = sqrt(kxx);
		double cx, sx;
		sincos(sqrt_kxx*step_length, &sx, &cx);
		const double sx_kx = sx/sqrt_kxx;
		const double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
		particle.xp = -particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
		particle.x = __x;
	      } else {
		const double sqrt_kxx = sqrt(-kxx);
		double cx,sx;
		sincosh(sqrt_kxx*step_length,sx,cx);
		const double sx_kx = sx/sqrt_kxx;
		const double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
		particle.xp = particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
		particle.x = __x;
	      }
	      if (fabs(kyy)<std::numeric_limits<double>::epsilon()) {
		particle.y  += particle.yp*step_length;
	      } else if (kyy > 0.) {
		const double sqrt_kyy = sqrt(kyy);
		double cy, sy;
		sincos(sqrt_kyy*step_length, &sy, &cy);
		double sy_ky = sy/sqrt_kyy;
		const double __y = particle.y*cy + particle.yp*sy_ky;
		particle.yp = -particle.y*sqrt_kyy*sy + particle.yp*cy;
		particle.y = __y;
	      } else {
		const double sqrt_kyy = sqrt(-kyy);
		double cy, sy;
		sincosh(sqrt_kyy*step_length,sy,cy);
		double sy_ky = sy/sqrt_kyy;
		double __y = particle.y*cy + particle.yp*sy_ky;
		particle.yp = particle.y*sqrt_kyy*sy + particle.yp*cy;
		particle.y = __y;
	      }
	      s += step_length;
	    }
	    if (done) break;
	    photons++;
	    particle.energy -= SYNRAD::get_energy_loss(step_length * kx_, step_length, particle.energy);
	  }
      }
    }
    placet_cout << VERYVERBOSE << "SBEND: average number of photons emitted: " 
		<< double(photons)/beam->slices << endmsg;
  }
}

void SBEND::step_4d_0(BEAM *beam)
{
  if (csr) {
    placet_cout << WARNING << "6D tracking will be performed for this sbend, since CSR is enabled" << endmsg;
    step_6d_0(beam);
  } else {
#pragma omp parallel for
    for (int i=0; i<beam->slices; i++) {
      // reference to a particle 
      PARTICLE &particle = beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	const double h=angle0/geometry.length; // 1/m
	const double kx_=h*ref_energy/particle.energy; // 1/m
	const double kyy=-k/particle.energy/geometry.length; // 1/m**2
	const double kxx=h*kx_-kyy; // 1/m**2
	if (fabs(kxx)<std::numeric_limits<double>::epsilon()) {
	  double delta = particle.momentum_deviation(ref_energy)*1e6;
	  double tmp   = particle.xp*geometry.length + delta*angle0*geometry.length;
	  particle.xp += delta*angle0;
	  particle.x  += tmp;
	} else if (kxx>0.) {
	  const double sqrt_kxx = sqrt(kxx);
	  double cx, sx;
	  sincos(sqrt_kxx*geometry.length, &sx, &cx);
	  double sx_kx = sx/sqrt_kxx;
	  double tmp  = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
	  particle.xp = -particle.x*sqrt_kxx*sx + particle.xp*cx + (kx_-h)*sx_kx*1e6;
	  particle.x  = tmp;
	} else {
	  const double sqrt_kxx = sqrt(-kxx);
	  double cx,sx;
	  sincosh(sqrt_kxx*geometry.length,sx,cx);
	  double sx_kx = sx/sqrt_kxx;
	  double tmp  = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
	  particle.xp = particle.x*sqrt_kxx*sx + particle.xp*cx + (kx_-h)*sx_kx*1e6;
	  particle.x = tmp;
	}
	if (fabs(kyy)<std::numeric_limits<double>::epsilon()) {
	  particle.y  += particle.yp*geometry.length;
	} else if (kyy>0.) {
	  const double sqrt_kyy = sqrt(kyy);
	  double cy, sy;
	  sincos(sqrt_kyy*geometry.length, &sy, &cy);
	  double sy_ky = sy/sqrt_kyy;
	  double tmp  = particle.y*cy + particle.yp*sy_ky;
	  particle.yp = -particle.y*sqrt_kyy*sy + particle.yp*cy;
	  particle.y  = tmp;
	} else {
	  const double sqrt_kyy = sqrt(-kyy);
	  double cy,sy;
	  sincosh(sqrt_kyy*geometry.length,sy,cy);
	  double sy_ky = sy/sqrt_kyy;
	  double tmp  = particle.y*cy + particle.yp*sy_ky;
	  particle.yp = particle.y*sqrt_kyy*sy + particle.yp*cy;
	  particle.y  = tmp;
	}
      }
    }
  }
}

void SBEND::step_4d(BEAM *beam)
{
  if (csr) {
    placet_cout << WARNING << "CSR not implemented for this case" << endmsg;
  }
  // tracks the single particles
  step_4d_0(beam);
  // tracks the sigma matrices
#pragma omp parallel for
  for (int i=0;i<beam->slices;i++){
    if (fabs(beam->particle[i].energy)>std::numeric_limits<double>::epsilon()) {
      drift_sigma_matrix(beam->sigma[i],geometry.length);
      drift_sigma_matrix(beam->sigma_xx[i],geometry.length);	
      drift_sigma_matrix(beam->sigma_xy[i],geometry.length);
    }
  }
}

void SBEND::step_in(BEAM *beam )
{
  placet_cout << DEBUG << " SBend attribute angle0: " << angle0 <<  "   e1 "<< e1 << endmsg;   
  double _offset;
  if (fabs(_offset=0.5*geometry.length*offset.xp-offset.x)>std::numeric_limits<double>::epsilon() || fabs(offset.xp)>std::numeric_limits<double>::epsilon()) {
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.x += _offset;
      particle.xp -= offset.xp;
    }
#ifdef HTGEN
    // shift halo particles in x as beam particles to take offsets from misalignment into account
#pragma omp parallel for    
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.x += _offset;
      particle_sec.xp -= offset.xp;
    }
#endif
  }
  if (fabs(_offset=0.5*geometry.length*offset.yp-offset.y)>std::numeric_limits<double>::epsilon()  || fabs(offset.yp)>std::numeric_limits<double>::epsilon()) {
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.y += _offset;
      particle.yp -= offset.yp;
    }
#ifdef HTGEN
    // shift halo particles in x as beam particles to take offsets from misalignment into account
#pragma omp parallel for    
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.y += _offset;
      particle_sec.yp -= offset.yp;
    }
#endif
  }
  bunch_rotate(beam,offset.roll+tilt);      
  double h=angle0/geometry.length;
  double corr=2*h*hgap*fint;
  if (fabs(e1)>std::numeric_limits<double>::epsilon()||fabs(corr)>std::numeric_limits<double>::epsilon()) {
    double t1x=tan(e1)*h*ref_energy;
    double t1y;
    // fringe field
    if (fabs(corr)>std::numeric_limits<double>::epsilon()) {
      double ce1, se1;
      sincos(e1, &se1, &ce1);
      t1y=tan(e1-corr*(1+se1*se1)/ce1)*h*ref_energy;
    } else {
      t1y=t1x;
    }
    // end of fringe field
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	const double fx=t1x/particle.energy;
    	const double fy=t1y/particle.energy;
    	if (placet_switch.first_order || beam->particle_beam) {
	  particle.xp+=fx*particle.x;
	  particle.yp-=fy*particle.y;
	} else {
          R_MATRIX &sigma_xx=beam->sigma_xx[i];
          R_MATRIX &sigma_xy=beam->sigma_xy[i];
          R_MATRIX &sigma_yy=beam->sigma[i];
	  R_MATRIX ryy;
	  ryy.r11=1.0;
	  ryy.r12=0.0;
	  ryy.r21=-fy;
	  ryy.r22=1.0;
	  particle.yp-=fy*particle.y;
	  R_MATRIX rxx;
	  rxx.r11=1.0;
	  rxx.r12=0.0;
	  rxx.r21=fx;
	  rxx.r22=1.0;
	  particle.xp+=fx*particle.x;
	  sigma_xx = rxx * sigma_xx * transpose(rxx);
	  sigma_xy = ryy * sigma_xy * transpose(rxx);
	  sigma_yy = ryy * sigma_yy * transpose(ryy);
        }
      }
    }
#ifdef HTGEN
    // shift halo particles as beam particles to take offsets from misalignment into account
#pragma omp parallel for 
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      if (fabs(particle_sec.energy)>std::numeric_limits<double>::epsilon()) {
	particle_sec.xp+=t1x*particle_sec.x/particle_sec.energy;
	particle_sec.yp-=t1y*particle_sec.y/particle_sec.energy;
      }
    }
#endif
  }
  // reset csrdrift wake (will be set to 1 again if CSR is enabled for this SBend)
  beam->csrwake->wake_enabled = 0;
  beam->csrwake->distance_from_sbend = 0;
}

void SBEND::step_out(BEAM *beam )
{
  double h=angle0/geometry.length;
  double corr=2*h*hgap*(fintx>=0.0?fintx:fint);
  if (fabs(e2)>std::numeric_limits<double>::epsilon()||fabs(corr)>std::numeric_limits<double>::epsilon()) {
    double t2x=tan(e2)*h*ref_energy;
    double t2y;
    // fringe field
    if (fabs(corr)>std::numeric_limits<double>::epsilon()) {
      double ce2, se2;
      sincos(e2, &se2, &ce2);
      t2y=tan(e2-corr*(1+se2*se2)/ce2)*h*ref_energy;
    } else {
      t2y=t2x;
    }
    // end of fringe field
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	const double fx=t2x/particle.energy;
	const double fy=t2y/particle.energy;
    	if (placet_switch.first_order || beam->particle_beam) {
	  particle.xp+=fx*particle.x;
	  particle.yp-=fy*particle.y;
	} else {
          R_MATRIX &sigma_xx=beam->sigma_xx[i];
          R_MATRIX &sigma_xy=beam->sigma_xy[i];
          R_MATRIX &sigma_yy=beam->sigma[i];
	  R_MATRIX ryy;
	  ryy.r11=1.0;
	  ryy.r12=0.0;
	  ryy.r21=-fy;
	  ryy.r22=1.0;
	  particle.yp-=fy*particle.y;
	  R_MATRIX rxx;
	  rxx.r11=1.0;
	  rxx.r12=0.0;
	  rxx.r21=fx;
	  rxx.r22=1.0;
	  particle.xp+=fx*particle.x;
	  sigma_xx = rxx * sigma_xx * transpose(rxx);
	  sigma_xy = ryy * sigma_xy * transpose(rxx);
	  sigma_yy = ryy * sigma_yy * transpose(ryy);
	}
      }
    }
#ifdef HTGEN
    // shift halo particles as beam particles to take offsets from misalignment into account
#pragma omp parallel for 
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      if (fabs(particle_sec.energy)>std::numeric_limits<double>::epsilon()) {
	particle_sec.xp+=particle_sec.x*t2x/particle_sec.energy;
	particle_sec.yp-=particle_sec.y*t2y/particle_sec.energy;
      }
    }
#endif
  }
  bunch_rotate(beam,-offset.roll-tilt);
  double _offset;
  if (fabs(_offset=0.5*geometry.length*offset.yp+offset.y)>std::numeric_limits<double>::epsilon() || fabs(offset.yp)>std::numeric_limits<double>::epsilon()) {
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      particle.y += _offset;
      particle.yp += offset.yp;
    }
#ifdef HTGEN
    // shift halo particles as beam particles to take offsets from misalignment into account
#pragma omp parallel for 
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.y += _offset;
      particle_sec.yp += offset.yp;
    }
#endif
  }
  if (fabs(_offset=0.5*geometry.length*offset.xp+offset.x)>std::numeric_limits<double>::epsilon() || fabs(offset.xp)>std::numeric_limits<double>::epsilon()) {
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.x += _offset;
      particle.xp += offset.xp;
    }
#ifdef HTGEN
    // shift halo particles as beam particles to take offsets from misalignment into account
#pragma omp parallel for 
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.x += _offset;
      particle_sec.xp += offset.xp;
    }
#endif
  }
}

void SBEND::step_6d_0(BEAM *beam )
{
  double *lambda, *dlambda;
  double *dE_ds;
  int *nlambda;
  if (csr) {
    // memory allocation for CSR wake
    nlambda = (int*) malloc(sizeof(int) * csr_nbins);   // histogram distribution 
    lambda = (double*) malloc(sizeof(double) * csr_nbins);   // distribtuion  
    dlambda = (double*) malloc(sizeof(double) * csr_nbins);  // differential distribution  
    dE_ds = (double*) malloc(sizeof(double) * csr_nbins); // CSR wake

    placet_printf(INFO,"Entering CSR bend (step_6d_0 with CSR).  csr_nbins: %d, csr_nhalffilter: %d, csr_nsectors: %d\n", csr_nbins,  csr_nhalffilter, csr_nsectors);

    // ERROR CHECKS
    if( csr_nbins < 10 ) {
      placet_printf(ERROR,"EA: ERROR, csr_nbins must be at least 10\n");
    }
    if( csr_nhalffilter < 1 ) {
      placet_printf(ERROR,"EA: ERROR, csr_nhalffilter must be at least 1\n");
    }
    if( csr_nsectors < 1 ) {
      placet_printf(ERROR,"EA: ERROR, 'csr_nsectors' must be at least 1\n");
    }
  } else {
    // if not CSR, track all dipole sector in one go
    csr_nsectors = 1;
  }

  const double sector_length = geometry.length/csr_nsectors;
  const double sector_angle = angle0/csr_nsectors;

  //
  // LOOP OVER SECTORS
  //  
  for (int nsector=0; nsector<csr_nsectors; nsector++) {
    if( 1 ) {
      //
      // 6D TRACK OF ONE SECTOR
      //
      // this->ref_energy = 0.69;
      //placet_printf(INFO,"EA: ref_energy: %g\n", ref_energy);     
#pragma omp parallel for
      for (int i=0; i<beam->slices; i++) {
	PARTICLE &particle = beam->particle[i];
	if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	  const double h=sector_angle/sector_length; // 1/m
	  const double kx_=h*ref_energy/particle.energy; // 1/m
	  const double kyy=-k/particle.energy/geometry.length; // 1/m**2
	  const double kxx=h*kx_-kyy; // 1/m**2
	  const double delta=particle.momentum_deviation(ref_energy)*1e6; // micro
	  if (fabs(kxx)<std::numeric_limits<double>::epsilon()) {
	    particle.z += particle.x*sector_angle + particle.xp*sector_angle*sector_length;
	    double __x = particle.xp*sector_length + delta*sector_angle*sector_length;
	    particle.xp += delta*sector_angle;
	    particle.x += __x;
	  } else if (kxx>0.) {
	    const double sqrt_kxx = sqrt(kxx);
	    double cx, sx;
	    sincos(sqrt_kxx*sector_length, &sx, &cx);
	    double sx_kx = sx/sqrt_kxx;
	    particle.z += particle.x*h*sx_kx + particle.xp*h*(1-cx)/kxx + delta*(sector_length-sx_kx);
	    double __x  = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
	    particle.xp = -particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
            particle.x = __x;
	  } else {
	    const double sqrt_kxx = sqrt(-kxx);
	    double cx,sx;
	    sincosh(sqrt_kxx*sector_length,sx,cx);
	    double sx_kx = sx/sqrt_kxx;
	    particle.z += particle.x*h*sx_kx + particle.xp*h*(1-cx)/kxx + delta*(sector_length-sx_kx);
	    double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
	    particle.xp = particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
	    particle.x = __x;
	  }
	  if (fabs(kyy)<std::numeric_limits<double>::epsilon()) {
	    particle.y  += particle.yp*sector_length;
	  } else if (kyy > 0.) {
	    const double sqrt_kyy = sqrt(kyy);
	    double cy, sy;
	    sincos(sqrt_kyy*sector_length, &sy, &cy);
	    double sy_ky = sy/sqrt_kyy;
	    double __y = particle.y*cy + particle.yp*sy_ky;
	    particle.yp = -particle.y*sqrt_kyy*sy + particle.yp*cy;
	    particle.y = __y;
	  } else {
	    const double sqrt_kyy = sqrt(-kyy);
	    double cy, sy;
	    sincosh(sqrt_kyy*sector_length,sy,cy);
	    double sy_ky = sy/sqrt_kyy;
	    double __y = particle.y*cy + particle.yp*sy_ky;
	    particle.yp = particle.y*sqrt_kyy*sy + particle.yp*cy;
	    particle.y = __y;
	  }
	}
      }
    }
    if( csr ) {
      // CALC AND APPLY CSR WAKE
      apply_csr_wake(sector_angle, sector_length, nsector, nlambda, lambda, dlambda, dE_ds, beam);
    } // if( 1 )
  }
  
  if( csr ) {
    //   PREPARE CSR TERMINAL WAKE, needed for calculating CSR drift effects [implementation assumes steady-state is reached at the end of the sbend]
    prepare_csr_drift_wake(sector_angle, nlambda, lambda, dlambda, dE_ds, beam);
    
    // free CSR arrays
    free(nlambda);
    free(lambda);
    free(dlambda); 
    free(dE_ds); 
  }
}

void SBEND::step_6d_sr_0(BEAM *beam )
{
  double *lambda, *dlambda;
  double *dE_ds;
  int *nlambda;
  if (csr) {
    // memory allocation for CSR wake
    nlambda = (int*) malloc(sizeof(int) * csr_nbins);   // histogram distribution 
    lambda = (double*) malloc(sizeof(double) * csr_nbins);   // distribtuion  
    dlambda = (double*) malloc(sizeof(double) * csr_nbins);  // differential distribution  
    dE_ds = (double*) malloc(sizeof(double) * csr_nbins); // CSR wake
    
    placet_printf(INFO,"Entering CSR + ISR Sbend (step_6d_sr_0 with CSR).  csr_nbins: %d, csr_nhalffilter: %d, csr_nsectors: %d\n", csr_nbins,  csr_nhalffilter, csr_nsectors);
    
    // ERROR CHECKS
    if( csr_nbins < 10 ) {
      placet_printf(ERROR,"EA: ERROR, csr_nbins must be at least 10\n");
    }
    if( csr_nhalffilter < 1 ) {
      placet_printf(ERROR,"EA: ERROR, csr_nhalffilter must be at least 1\n");
    }
    if( csr_nsectors < 1 ) {
      placet_printf(ERROR,"EA: ERROR, 'csr_nsectors' must be at least 1\n");
    }
  } else {
    // if not CSR, track all dipole sector in one go
    csr_nsectors = 1;
  }

  double sector_length = geometry.length/csr_nsectors;
  double sector_angle = angle0/csr_nsectors;

  //
  // LOOP OVER SECTORS
  //
  for (int nsector=0; nsector<csr_nsectors; nsector++) {
    if( 1 ) {
      //
      // 6D TRACK OF ONE SECTOR WITH ISR
      //
      size_t photons = 0;
# pragma omp parallel for
      for (int i=0; i<beam->slices; i++) {
	// reference to a particle	 
	PARTICLE &particle = beam->particle[i];
	if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	  // calculate the energy loss by synrad emission
	  double s = 0.0;
	  for(;;) {
	    const double h = sector_angle/sector_length; // 1/m
	    const double kx_ = h*ref_energy/particle.energy; // 1/m
	    double step_length = SYNRAD::get_free_path(sector_length * kx_, sector_length, particle.energy); //use of RNG
	    const bool done = (s+step_length>=sector_length);
	    if (done) step_length = sector_length-s;
	    if (step_length>std::numeric_limits<double>::epsilon()) {
	      const double step_angle = sector_angle * step_length / sector_length;
	      const double kyy=-k/particle.energy/geometry.length; // 1/m**2
	      const double kxx=h*kx_-kyy; // 1/m**2
	      const double delta=particle.momentum_deviation(ref_energy)*1e6; // micro
	      if (fabs(kxx)<std::numeric_limits<double>::epsilon()) {
		particle.z += particle.x*step_angle + particle.xp*step_angle*step_length;
		double __x = particle.xp*step_length + delta*step_angle*step_length;
		particle.xp += delta*step_angle;
		particle.x += __x;
	      } else if (kxx>0.) {
		const double sqrt_kxx = sqrt(kxx);;
		double cx, sx;
		sincos(sqrt_kxx*step_length, &sx, &cx);
		double sx_kx = sx/sqrt_kxx;
		particle.z += particle.x*h*sx_kx + particle.xp*h*(1-cx)/kxx + delta*(step_length-sx_kx);
		double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
		particle.xp = -particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
		particle.x = __x;
	      } else {
		const double sqrt_kxx = sqrt(-kxx);
		double cx,sx;
		sincosh(sqrt_kxx*step_length,sx,cx);
		double sx_kx = sx/sqrt_kxx;
		particle.z += particle.x*h*sx_kx + particle.xp*h*(1-cx)/kxx + delta*(step_length-sx_kx);
		double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
		particle.xp = particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
		particle.x = __x;
	      }
	      if (fabs(kyy)<std::numeric_limits<double>::epsilon()) {
		particle.y  += particle.yp*step_length;
	      } else if (kyy > 0.) {
		const double sqrt_kyy = sqrt(kyy);
		double cy, sy;
		sincos(sqrt_kyy*step_length, &sy, &cy);
		double sy_ky = sy/sqrt_kyy;
		double __y = particle.y*cy + particle.yp*sy_ky;
		particle.yp = -particle.y*sqrt_kyy*sy + particle.yp*cy;
		particle.y = __y;
	      } else {
		const double sqrt_kyy = sqrt(-kyy);
		double cy, sy;
		sincosh(sqrt_kyy*step_length,sy,cy);
		double sy_ky = sy/sqrt_kyy;
		double __y = particle.y*cy + particle.yp*sy_ky;
		particle.yp = particle.y*sqrt_kyy*sy + particle.yp*cy;
		particle.y = __y;
	      }
	      s += step_length;
	    }
	    if (done) break;
	    photons++;
	    particle.energy -= SYNRAD::get_energy_loss(step_length * kx_, step_length, particle.energy);
	  }
	}
      }
      placet_cout << VERYVERBOSE << "SBEND: average number of photons emitted per slice: " 
		  << double(photons)/beam->slices << endmsg;
    }
    
    if( csr ) {
      // CALC AND APPLY CSR WAKE
      apply_csr_wake(sector_angle, sector_length, nsector, nlambda, lambda, dlambda, dE_ds, beam);
    } // if( 1 )
  }
    
  if( csr ) {
    //   PREPARE CSR TERMINAL WAKE, needed for calculating CSR drift effects [implementation assumes steady-state is reached at the end of the sbend]
    prepare_csr_drift_wake(sector_angle, nlambda, lambda, dlambda, dE_ds, beam);

    // free CSR arrays
    free(nlambda);
    free(lambda);
    free(dlambda); 
    free(dE_ds); 
    //free(SG_coeff);
  }
}

void SBEND::list(FILE* f)const
{
  fprintf(f,"Sbend -length %g -angle %g -e0 %g -E1 %g -E2 %g\n",
	  geometry.length,angle0,ref_energy,e1,e2);
}

void SBEND::step_twiss(BEAM *beam,FILE * file,double /*step*/,
		       int j,double s0,int n1,int n2,
		       void (* callback)(FILE*,BEAM*,int,double,int,int))
{
  placet_cout << VERBOSE << "SBEND::step_twiss : twiss computed at the entrance and exit of the element "<< endmsg;
  if (callback) callback(file,beam,j,s0,n1,n2);
  s0+=geometry.length;
  track(beam);
  if (callback) callback(file,beam,j,s0,n1,n2);
}

void SBEND::step_twiss_0(BEAM *beam,FILE * file,double /*step*/,
			 int j,double s0,int n1,int n2,
			 void (* callback)(FILE*,BEAM*,int,double,int,int))
{
  placet_cout << VERBOSE << "SBEND::step_twiss_0 : twiss computed at the entrance and exit of the element "<< endmsg;
  if (callback) callback(file,beam,j,s0,n1,n2);
  s0+=geometry.length;
  track_0(beam);
  if (callback) callback(file,beam,j,s0,n1,n2);
}

int tk_Sbend(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	     char *argv[])
{
  if (element_help_message<SBEND>(argc,argv, "This command places a sbend in the current girder.\n\nSbend:",interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  SBEND *sbend = new SBEND(argc, argv);
  inter_data.girder->add_element(sbend);
  return TCL_OK;
}

// 
// calculate savitzky-golay coeffs ("optimal") for smoothing filter
//
void SBEND::calculate_savitzky_golay_coeffs(int SG_order, int SG_length, double *SG_coeffs, double *SG_norm) {
  double* SG_A = (double*) malloc(sizeof(double) * SG_order * SG_length);
  double* SG_AA = (double*) malloc(sizeof(double) * SG_order * SG_order);
  double* SG_inv_AA = (double*) malloc(sizeof(double) * SG_order * SG_order);
  double* SG_Ae = (double*) malloc(sizeof(double) * SG_order);
  double* SG_out = (double*) malloc(sizeof(double) * SG_order);
  //       placet_printf(INFO,"SG: order+1: %d\n", SG_order);
  //       placet_printf(INFO,"SG: halflength: %d\n", SG_length);

  for(int j=0; j < SG_length; j++) {
    for(int m=0; m < SG_order; m++) {
      SG_A[m + j*SG_order] = std::pow((double) j-csr_nhalffilter, m);
    }
  }
 
  int s;

  gsl_matrix_view matrix_A = gsl_matrix_view_array(SG_A, SG_length, SG_order);
  gsl_matrix_view matrix_AA = gsl_matrix_view_array(SG_AA, SG_order, SG_order);
  gsl_matrix_view matrix_inv_AA = gsl_matrix_view_array(SG_inv_AA, SG_order, SG_order);
 
  // A'*A
  gsl_blas_dgemm (CblasTrans, CblasNoTrans,
		  1.0, &matrix_A.matrix, &matrix_A.matrix,
		  0.0, &matrix_AA.matrix);
  // inv(A'*A)
  gsl_permutation * p = gsl_permutation_alloc (SG_order);
  gsl_linalg_LU_decomp (&matrix_AA.matrix, p, &s);
  gsl_linalg_LU_invert (&matrix_AA.matrix, p, &matrix_inv_AA.matrix);    
 
  //placet_printf(INFO,"EA: matrix_inv_AA: \n");
  //gsl_matrix_fprintf(stdout, &matrix_inv_AA.matrix, "%g");
    
  for(int j=0; j < SG_length; j++) {
    gsl_vector_view vector_Ae = gsl_vector_view_array(SG_Ae, SG_order);
    gsl_vector_view vector_out = gsl_vector_view_array(SG_out, SG_order);
    gsl_matrix_get_row (&vector_Ae.vector, &matrix_A.matrix, j);
    // inv(A'*A)*(A'*e_n)
    gsl_blas_dgemv (CblasNoTrans, 1.0, &matrix_inv_AA.matrix, &vector_Ae.vector, 0.0, &vector_out.vector);
    // c_n = (inv(A'*A)*(A'*e_n))_0
    SG_coeffs[j] = SG_out[0];
  }
  *SG_norm = 1.0;

  //     FILE* file=open_file("SG_matrix");
  //     gsl_matrix_fprintf(stdout, &matrix_AA.matrix, "%g");
  //     close_file(file);

  // debug
  //    for(int j=0; j < SG_length; j++) {
  //      placet_printf(INFO,"EA: SG_coeff %d: %g\n", j, SG_coeffs[j]);
  //   }

  free(SG_Ae);
  free(SG_out);
  free(SG_A);
  free(SG_AA);
  free(SG_inv_AA);
}  



// 
// CALC AND APPLY CSR WAKE
//
#define E_CUT_LOST 0.01
void SBEND::apply_csr_wake(double sector_angle, double sector_length, int nsector, int* nlambda, double* lambda, double* dlambda, double* dE_ds, BEAM* beam) {
  //reset allocated arrays
  for (int i=0; i<csr_nbins; i++) {
    nlambda[i]=0; 
    lambda[i]=0.0;
    dlambda[i]=0.0; 
    dE_ds[i]=0.0; 
  }

  bool need_sorting = false;
  for (size_t i=1; i<beam->slices; i++) {
    if (beam->particle[i].z<beam->particle[i-1].z) {
      need_sorting = true;
      break;
    }
  }
  if (need_sorting)
    std::sort(beam->particle,beam->particle+beam->slices);

  // set histogram limits, but prevent lost particles (low energy or E=0) from deciding the bin limits
  //   some approx for the moment: all particles still counted, and lost particles are put in end bins
  int n_lost_min = 0;
  while( beam->particle[n_lost_min].energy < E_CUT_LOST ) {
    n_lost_min++;
  }
  double z_min = beam->particle[n_lost_min].z;
  int n_lost_max = 0;
  while( beam->particle[beam->slices-1-n_lost_max].energy < E_CUT_LOST ) {
    n_lost_max++;
  }
  double z_max = beam->particle[beam->slices-1-n_lost_max].z;
  double binlength = (z_max - z_min) / csr_nbins;
  binlength /= 1e6; // [m] 
  // binning 
  
  int i = 0, nbin = 0, n_particles = 0;
  while( i < beam->slices ) {
    if( beam->particle[i].z < (z_min + (binlength*1e6)*(nbin+1)) ) {
      n_particles++;
      i++;
    } else {
      nlambda[csr_nbins - 1 - nbin] = n_particles;  // inverse order due to PLACET convention of lowest z to leading particle 
      //placet_printf(INFO,"CHANGE BIN: bin: %d:  part: %d   n_particles %d   dist: %g,  \n", nbin, i, n_particles, (z_min + (binlength*1e6)*(nbin+1)));
      nbin++;
      // catch particle(s) that falls out of last bin due to rounding
      if( nbin >= csr_nbins ) {
	nbin = csr_nbins-1;
	i++;
	n_particles++;
      } else {
	n_particles = 0;
      }
    }
  }
  
  nlambda[csr_nbins - 1 - nbin] = n_particles;
  //placet_printf(INFO,"LAST BIN: bin: %d:  part: %d   n_particles %d\n", nbin, i, n_particles);
  //  test bins: sum particles in bin;
  //         int sum=0;
  //         for(i=0; i < csr_nbins; i++) {
  //   	sum += nlambda[i];
  //         }
  //   placet_printf(INFO,"nlambda sum: %d\n", sum);
  //   placet_printf(INFO,"lost particles in total: %d\n", 40000 - sum);
  //   placet_printf(INFO,"n_lost_min + n_lost_max: %d\n", n_lost_min + n_lost_max);
  //   placet_printf(INFO,"binlength ds: %g\n", binlength);

  // conversion of nlambda to a line distribution ( integral lambda dn = 1 )
  for(i=0; i < csr_nbins; i++) {
    lambda[i] = (double) nlambda[i] / (beam->slices - 0);
  }
  //  test: sum up
  //         double dsum = 0;
  //         for(i=0; i < csr_nbins; i++) {
  //   	dsum += lambda[i];
  //         }
  //   placet_printf(INFO,"lambda sum: %g\n", dsum);
  

  // calculate geometric parameters and slippage length
  double traversed_angle = fabs(sector_angle) * (nsector+1);  // EA: we make the simplification that for the CSR calculations the particles travels as with ref energy TO UPDATE
  double radius0  = sector_length / fabs(sector_angle);
  double sL = radius0*traversed_angle*traversed_angle*traversed_angle / 24/1; // slippage length
  int nL = (int) round(sL / binlength );
  if( csr_enforce_steady_state ) {
    nL = nbin+1;
  }

  // some potential debugging info:
  //   	placet_printf(INFO,"sector_angle: %g\n", sector_angle);
  //    	placet_printf(INFO,"traversed_angle: %g\n", traversed_angle);
  //    	placet_printf(INFO,"radius0: %g\n", radius0);
  //   placet_printf(INFO,"nsector: %d\n", nsector);
  //   placet_printf(INFO,"sL: %g\n", sL);
  //   placet_printf(INFO,"nL: %d\n", nL);
      
  //Savitzky-Golay smoothing of distribution
  int SG_order = csr_filterorder+1;
  int SG_length = 2*csr_nhalffilter + 1;
  double SG_coeffs[SG_length];   
  double SG_norm;
  // create coefficients
  SBEND::calculate_savitzky_golay_coeffs(SG_order, SG_length, SG_coeffs, &SG_norm);

  double* templambda = (double*) malloc(sizeof(double) * csr_nbins );  
  for(i=0; i < csr_nbins; i++) {
    templambda[i] = 0;
  }

  int lambda_index;
  for(i=0; i < csr_nbins; i++) {
    for(int j=0; j < (2*csr_nhalffilter+1); j++) {
      lambda_index = i + j - csr_nhalffilter;
      if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
	templambda[i] = templambda[i] +  SG_coeffs[j]*lambda[lambda_index] / SG_norm;  
      }
    }
  }
  for(i=0; i < csr_nbins; i++) {
    lambda[i] = templambda[i];
    templambda[i] = 0;
  }


  // Savitzky-Golay differentiation of distribution
  // 1st order filter (auto generated)   [commmented code kep in case HO filters will be implemented]
  //     for(int j=0; j < (2*csr_nhalffilter+1); j++) {
  //       SG_coeff[j] = -csr_nhalffilter + j;
  //     }
  //csr_nhalffilter=1;
  int SGd_norm = 2*csr_nhalffilter*(csr_nhalffilter+1)*(2*csr_nhalffilter+1) / 6;
  // int SG_coeff[(2*csr_nhalffilter+1)] = {-573,2166,-1249,-3774,-3084,0,3084,3774,1249,-2166,573};   // example of higher-order filter
  // int SG_norm = 17160;
  for(i=0; i < csr_nbins; i++) {
    for(int j=0; j < (2*csr_nhalffilter+1); j++) {
      lambda_index = i + j - csr_nhalffilter;
      if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
	dlambda[i] +=  (-csr_nhalffilter + j)*lambda[lambda_index] / SGd_norm;
	//dlambda[i] = dlambda[i] +  SG_coeff[j]*lambda[lambda_index] / SG_norm;   // [generalization for HO filters]
      }
    }
    dlambda[i] /= binlength;
  }
  
  // smoothing of derivative distribution
  //   for(int i=0; i < csr_nbins; i++) {
  //     for(int j=0; j < (2*csr_nhalffilter+1); j++) {
  //       lambda_index = i + j - csr_nhalffilter;
  //       if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
  // 	templambda[i] = templambda[i] +  SG_coeffs[j]*dlambda[lambda_index] / SG_norm;  
  //       }
  //     }
  //   }
  //   for(int i=0; i < csr_nbins; i++) {
  //     dlambda[i] = templambda[i];
  //     templambda[i] = 0;
  //   }

      

  // calculate wake dE_ds [GeV/m], bin by bin
  double radius = radius0;
  double K = -2*ECHARGE*ECHARGE / pow(3*radius*radius, 1./3.) ;  // no minus
  double charge_w_loss = csr_charge; // * (n_lost / n_particles);
  double N_charge = charge_w_loss / ECHARGE; 
  K = K * N_charge;
  K = K / (4*M_PI*SI_perm);
  //placet_printf(INFO,"K: %g\n", K);
  
  //  double debug_T2=0;
  double L_trap, R_trap;
  for(nbin=0; nbin < csr_nbins; nbin++) {
    int nbin_nL = nbin - nL;
    if( nbin_nL < 0 ) {
      nbin_nL = 0;
    }
    int nbin_4nL = nbin - 4*nL;
    if( nbin_4nL < 0 ) {
      nbin_4nL = 0;
    }
    
    // transient dipole entry term
    double T2 = ( lambda[nbin_nL] -  lambda[nbin_4nL] ) / pow(sL, 1./3.) / binlength;
    //    if( abs(T2) > abs(debug_T2) ) {
    //       debug_T2 = T2;
    //    }

    // main term, trapezoidial integration
    double T1 = 0;
    if( nbin > nbin_nL) {
      // first bin
      int k0 = nbin_nL;
      L_trap =  dlambda[k0] * (1 / pow((nbin - k0)*binlength,1./3.) );
      if( nbin > nbin_nL + 1) {
	R_trap =  dlambda[k0+1] * (1 / pow((nbin - k0 - 1)*binlength,1./3.) );
	T1 += (L_trap + R_trap)/2;
      } else {
	R_trap = 0;
	T1 += L_trap;
      }
      // middle bins
      for(int k1=nbin_nL+1; k1 < nbin-1; k1++) {
	L_trap =  R_trap;
	R_trap =  dlambda[k1+1] * (1 / pow((nbin - k1 - 1)*binlength,1./3.) );
	T1 += (L_trap + R_trap)/2;
      }
      // last bin
      T1 += R_trap;
    }
    // calculate dE_cs [GeV/m]
    dE_ds[nbin] = ( K*T1 + K*T2 ) / ECHARGE / 1e9;
  }
  //placet_printf(INFO,"T2: %g\n", debug_T2);


  // smoothing of wake distribution
  //   for(int i=0; i < csr_nbins; i++) {
  //     for(int j=0; j < (2*csr_nhalffilter+1); j++) {
  //       lambda_index = i + j - csr_nhalffilter;
  //       if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
  // 	templambda[i] = templambda[i] +  SG_coeffs[j]*dE_ds[lambda_index] / SG_norm;  
  //       }
  //     }
  //   }
  //   for(int i=0; i < csr_nbins; i++) {
  //     dE_ds[i] = templambda[i];
  //     templambda[i] = 0;
  //   }

      
  // Write dists and dE_ds to file, if selected by user
  if( csr_savesectors ) {
    char filename[256];
    snprintf(filename, 256, "csr_savesectors.%d.dat", nsector);
    FILE* file=open_file(filename);
    for (nbin = 0; nbin < csr_nbins; nbin++) {
      // write in PLACET order, so that small values corresponds to head of bunch
      placet_fprintf(INFO,file,"%d  %g  %g   %g   %g\n", nbin, nbin*binlength, lambda[csr_nbins - 1 - nbin], dlambda[csr_nbins - 1 - nbin], dE_ds[csr_nbins - 1 - nbin]);
    }
    close_file(file);
  }


  // apply CSR wake to particles, bin per bin
  int n_lost_total = 0;
  n_particles = 0;
  for (nbin=0; nbin<csr_nbins; nbin++) {
    double dE_ds_bin = dE_ds[csr_nbins - 1 - nbin] * sector_length;
    //double dE_ds_bin = dE_ds[nbin] * sector_length;
    for(i=0; i<nlambda[csr_nbins - 1 - nbin]; i++) {
      if( beam->particle[n_particles].energy > E_CUT_LOST ) {
	beam->particle[n_particles++].energy += dE_ds_bin; // apply kick [GeV]
      } else {
	n_lost_total++;
	n_particles++;
      }
    }
  }

  free(templambda);

  //placet_printf(INFO,"EA: sector_length: %g\n", sector_length);
  // placet_printf(INFO,"EA: n_particles: %d\n", n_particles);
  //placet_printf(INFO,"EA: total particles lost: %d\n", n_lost_total);
}


//
//   PREPARE CSR TERMINAL WAKE, needed for calculating CSR drift effects [implementation assumes steady-state is reached at the end of the sbend]
//
void SBEND::prepare_csr_drift_wake(double /*sector_angle*/, int* nlambda, double* lambda, double* /*dlambda*/, double* dE_ds, BEAM* beam) {
  if( csr && csr_enable_driftwake) {
    beam->csrwake->wake_enabled = 1;
    // reset distance parameter
    beam->csrwake->distance_from_sbend = 0;
    // estimation of mu, sigma_z
    double mu = 0;
    for (int nbin=0; nbin<csr_nbins; nbin++) {
      mu += (nbin+1) * lambda[nbin];
    }
    placet_printf(INFO,"EA: csr estimated pdf mu: %g\n", mu);
    double sigma_z2 = 0;
    for (int nbin=0; nbin<csr_nbins; nbin++) {
      sigma_z2 += std::pow((nbin+1) - mu, 2) * lambda[nbin];
    }
    double z_min = beam->particle[0].z;
    double z_max = beam->particle[beam->slices-1].z;
    double binlength = (z_max - z_min) / csr_nbins;
    binlength /= 1e6; // [m] 
    double sigma_z = sqrt(sigma_z2) * (binlength);
    placet_printf(INFO,"EA: csr estimated pdf sigma_z: %g\n", sigma_z);
    // calculation of overtaking length
    double radius = geometry.length / fabs(angle0);
    placet_printf(INFO,"EA: csr radius: %g\n", radius);
    double overtaking_length = pow(24*sigma_z*radius*radius, (1./3.));
    placet_printf(INFO,"EA: csr overtaking length: %g\n", overtaking_length);
    if( csr_attenuation_length <= 0.0 ) { 
      beam->csrwake->attenuation_length = 0.25*overtaking_length; // default: 0.5 * 1.5*L0, based on [Dohlus et al., 1997], factor 1/2 added after benchmarking
    } else {
      beam->csrwake->attenuation_length = csr_attenuation_length;
    }
    placet_printf(INFO,"EA: csr attenuation length: %g\n", beam->csrwake->attenuation_length);
    beam->csrwake->nbins = csr_nbins;
    beam->csrwake->terminal_dE_ds = (double*) realloc(beam->csrwake->terminal_dE_ds, sizeof(double) * csr_nbins); // CSR terminal wake, will be accessed by trailing elements
    beam->csrwake->terminal_nlambda = (int*) realloc(beam->csrwake->terminal_nlambda, sizeof(int) * csr_nbins); // CSR terminal binning, will be used [as a good approximation] for CSR drift (we do not expect large particle z movement within the relevant distance)
    for (int nbin=0; nbin<csr_nbins; nbin++) {
      // store CSR terminal wake [GeV/m]
      beam->csrwake->terminal_dE_ds[csr_nbins - 1 - nbin] = dE_ds[csr_nbins - 1 - nbin];
      beam->csrwake->terminal_nlambda[csr_nbins - 1 - nbin] = nlambda[csr_nbins - 1 - nbin];
    }
  }
}

Matrix<6,6> SBEND::get_transfer_matrix_6d(double _energy ) const 
{
  Matrix<6,6> R(0.0);
  if (_energy == -1.0)
    _energy = ref_energy;
  const double h=angle0/geometry.length; // 1/m
  const double kx_=h*ref_energy/_energy; // 1/m
  const double kyy=-k/_energy/geometry.length; // 1/m**2
  const double kxx=h*kx_-kyy; // 1/m**2
  if (fabs(kxx)<std::numeric_limits<double>::epsilon()) {
    R[0][0] = 1.0;
    R[0][1] = geometry.length;
    R[0][5] = angle0*geometry.length;
    R[1][1] = 1.0;
    R[1][5] = angle0;
  } else if (kxx>0.) {
    const double sqrt_kxx = sqrt(kxx);;
    double cx, sx;
    sincos(sqrt_kxx*geometry.length, &sx, &cx);
    double sx_kx = sx/sqrt_kxx;
    R[0][0] = cx;
    R[0][1] = sx_kx;
    R[0][5] = h*(1-cx)/kxx;
    R[1][0] = -sqrt_kxx*sx;
    R[1][1] = cx;
    R[1][5] = h*sx_kx;
    R[4][0] = h*sx_kx;
    R[4][1] = h*(1-cx)/kxx;
    R[4][5] = (geometry.length-sx_kx);
  } else {
    const double sqrt_kxx = sqrt(-kxx);
    double cx, sx;
    sincosh(sqrt_kxx*geometry.length,sx,cx);
    double sx_kx = sx/sqrt_kxx;
    R[0][0] = cx;
    R[0][1] = sx_kx;
    R[0][5] = h*(1-cx)/kxx;
    R[1][0] = sqrt_kxx*sx;
    R[1][1] = cx;
    R[1][5] = h*sx_kx;
    R[4][0] = h*sx_kx;
    R[4][1] = h*(1-cx)/kxx;
    R[4][5] = (geometry.length-sx_kx);
  }
  if (fabs(kyy)<std::numeric_limits<double>::epsilon()) {
    R[2][2] = 1.0;
    R[2][3] = geometry.length;
    R[3][3] = 1.0; 
  } else if (kyy > 0.) {
    const double sqrt_kyy = sqrt(kyy);
    double cy, sy;
    sincos(sqrt_kyy*geometry.length, &sy, &cy);
    double sy_ky = sy/sqrt_kyy;
    R[2][2] = cy;
    R[2][3] = sy_ky;
    R[3][2] = -sqrt_kyy*sy;
    R[3][3] = cy;
  } else {
    const double sqrt_kyy = sqrt(-kyy);
    double cy, sy;
    sincosh(sqrt_kyy*geometry.length,sy,cy);
    double sy_ky = sy/sqrt_kyy;
    R[2][2] = cy;
    R[2][3] = sy_ky;
    R[3][2] = sqrt_kyy*sy;
    R[3][3] = cy;
  }
  R[4][4] = 1.0;
  R[5][5] = 1.0;  
  double corr=2*h*hgap*fint;
  if (fabs(e1)>std::numeric_limits<double>::epsilon()||fabs(corr)>std::numeric_limits<double>::epsilon()) {
    Matrix<6,6> T=Identity<6,6>();
    double t1x=tan(e1)*h;
    double t1y;
    if (fabs(corr)>std::numeric_limits<double>::epsilon()) {
      double ce1, se1;
      sincos(e1, &se1, &ce1);
      t1y=tan(e1-corr*(1+se1*se1)/ce1)*h;
    } else {
      t1y=t1x;
    }
    T[1][0]=t1x;
    T[3][2]=-t1y;
    R*=T;
  }
  corr=2*h*hgap*(fintx>=0.0?fintx:fint);
  if (fabs(e2)>std::numeric_limits<double>::epsilon()||fabs(corr)>std::numeric_limits<double>::epsilon()) {
    Matrix<6,6> T=Identity<6,6>();
    double t2x=tan(e2)*h;
    double t2y;
    if (fabs(corr)>std::numeric_limits<double>::epsilon()) {
      double ce2, se2;
      sincos(e2, &se2, &ce2);
      t2y=tan(e2-corr*(1+se2*se2)/ce2)*h;
    } else { 
      t2y=t2x;
    }
    T[1][0]=t2x;
    T[3][2]=-t2y;
    R=T*R;
  }
  if (fabs(tilt)>std::numeric_limits<double>::epsilon()) {
    double s,c;
    sincos(tilt,&s,&c);
    Matrix<6,6> Rot(0.0);
    Rot[0][0]=Rot[1][1]=Rot[2][2]=Rot[3][3]=c;
    Rot[0][2]=Rot[1][3]=-s;
    Rot[2][0]=Rot[3][1]=s;
    Rot[4][4]=1.0;
    Rot[5][5]=1.0;
    R*=Rot;
    Rot[0][2]=Rot[1][3]=s;
    Rot[2][0]=Rot[3][1]=-s;
    R=Rot*R;
  }
  return R;
}
//
// Get quadrupole magnetic field By + iBx = k(x + iy)
//_________________________________________________________________ 
void SBEND::GetMagField(PARTICLE *particle, double sector_length, double *bfield){
  ELEMENT::GetMagField(particle, bfield);
  if(geometry.length>std::numeric_limits<double>::epsilon()){
    double g=get_ref_energy()*3.3356409520*angle0/geometry.length;
    placet_cout << WARNING << "IRTracking code does not consider dipole fields correctly yet.." << endmsg;
    placet_cout << VERBOSE << "SBEND::GetMagField vertical field:" << g << endmsg;
    bfield[0]=0.0;
    bfield[1]=-g;
    bfield[2]=0.0;
  }
}

#undef E_CUT_LOST
#undef SI_perm
