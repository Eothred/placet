#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>
#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"
#include "girder.h"
#include "lattice.h"

#include "curvature.h"

CURVATURE::CURVATURE(double _angley, double _anglex ) : angley(_angley), anglex(_anglex) {}

void CURVATURE::step_4d_0(BEAM *bunch )
{
  if (fabs(angley) > std::numeric_limits<double>::epsilon()) {
    for (int i=0;i<bunch->slices;i++) {
      bunch->particle[i].yp += angley;
    }
  }
  
#ifdef TWODIM
  if (fabs(anglex) > std::numeric_limits<double>::epsilon()) {
    for (int i=0;i<bunch->slices;i++) {
      bunch->particle[i].xp += anglex;
    }
  }
#endif
}

/**********************************************************************/
/*                      Curvature                                     */
/**********************************************************************/

int tk_Curvature(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			char *argv[])
{
  int error;
  double angley = 0.0;
  double anglex = 0.0;
  
  CURVATURE *e;
  Tk_ArgvInfo table[]={
    {(char*)"-angle_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&angley, (char*)"bending angle along y"},
    {(char*)"-angle_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&anglex, (char*)"bending angle along x"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Curvature>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) return TCL_ERROR;
  
  e=new CURVATURE(angley, anglex);
  inter_data.girder->add_element(e);
  fflush(stdout);
  return TCL_OK;
}
