#ifdef HTGEN
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <limits>

#include <tcl.h>
#include <tk.h>

#include "HTErrorMessages.h"
#include "HTConfiguration.h"
#include "HTTextParser.h"
#include "HTBaseProcess.h"
#include "HTMottProcess.h"
#include "HTBremProcess.h"
#include "HTBaseInterface.h"
#include "HTPlacetInterface.h"
#include "placet.h"
#include "placet_tk.h"
#include "placeti3.h"
#include "background.h"
#include "beam.h"
#include "beamline.h"
#include "cavity.h"
#include "slices_to_particles.h"
#include "select.h"
#include "random.hh"
#include "malloc.h"
#include "multipole.h"
#include "andrea.h"
#include "element.h"
#include "beamenergy.h"


/**********************************************************************/
/*                      TrackBackground                               */
/**********************************************************************/

using namespace std;

//--- global section -- start         try to get rid of all of these
extern INTER_DATA_STRUCT inter_data; // allows to access beamline with elements

BEAM hb; // global beam. Only used in beamline_track_background.   Defined by tk_DummyHalo or in beamline_track_background

static bool dummyhalo_flg = false;
//--- global section -- end

string ElementInfo(int iel) //hbu, returns a string with info on aperture iel and material, used for debug info only
{
  BEAMLINE *beamline=inter_data.beamline; // hbu  from global
  ELEMENT **element=beamline->element;
  string ElInfostr;
  if(!element[iel]->is_tclcall())
  {
    ostringstream StrOut;
    StrOut << setw(5) << iel
    << setw(10) << element[iel]->get_name()
    << setw(10) << element[iel]->get_length()
    << setw(12) << element[iel]->get_aperture_type_str()
    << setw(12) << element[iel]->get_aperture_x() << " "
    << setw(12) << element[iel]->get_aperture_y();
    const MATERIAL &thismaterial = element[iel]->get_material();
    StrOut << setw(10) << thismaterial.a << setw(10) << thismaterial.z << setw(10) << thismaterial.p << setw(10) << thismaterial.t << setw(10) << thismaterial.theta << setw(10) << thismaterial.k << setw(10) << thismaterial.X0;
    ElInfostr=StrOut.str();
  }
  else
  {
    placet_cout << INFO << "Element " << iel << " is a Tcl call" << endmsg;
  }
  return ElInfostr;
}

void PrintElementList(void)
{
  BEAMLINE *beamline=inter_data.beamline; // hbu  from global
  ELEMENT **element=beamline->element;
  double length_tot=0;
  placet_cout << INFO << "PrintElementList beamline name=" << beamline->get_name() << " number of elements=" << beamline->n_elements << std::endl;
  placet_cout << " Element  Name    Length  ApertureType Aperture x   Aperture y"; // element info
  placet_cout << "         a         z         p         t  thetamin      kmin        X0 SumLength" << endmsg; // vacuum and material info, radiation length X0
  cout.flags(ios::floatfield | ios::right);
  for (int iel=0;iel<beamline->n_elements;++iel) // print aperture info from element and Vacuum and Material info extra lists defined via htgen
  {
    length_tot += element[iel]->get_length();
    placet_cout << ElementInfo(iel) << setw(10) << length_tot;
    placet_cout << endmsg;
  }
}

// ---------------------------------------------------------------------
int config_element(int iel, double BeamEnergy,HTPlacetInterface* htplacetinter,int debuglevel,int flg_linac)
{
  BEAMLINE *beamline=inter_data.beamline; // hbu  from global
  ELEMENT **element=beamline->element; // to allow acces to element[iel]
  if(!element[iel]->is_tclcall())
  {
    if(debuglevel>2) cout << "config_element " << iel << " BeamEnergy=" << BeamEnergy << " GeV" << '\n';
    const MATERIAL &thismaterial = element[iel]->get_material();
    htplacetinter->setPressure(thismaterial.p);
    htplacetinter->setTemperature(thismaterial.t);
    htplacetinter->setZ(thismaterial.z);
    htplacetinter->setKmin(thismaterial.k);
    if (flg_linac==0) // no linac, use directly thetamin
    {
      htplacetinter->setThetamin(thismaterial.theta);
    }
    else // linac, scale thetamin with energy --- to be checked
      htplacetinter->setThetamin(sqrt(thismaterial.theta/BeamEnergy));
  }
  return 0;
}

string ParticleInfo(const PARTICLE& thePart, double length_tot, int ip) // give a string with particle info for printout
{
  ostringstream part_str;
  // part_str << right << setw(9) << thePart.energy << setw(14) << thePart.x << setw(14) << thePart.y << setw(14) << thePart.z << setw(14) << thePart.xp << setw(14) << thePart.yp << setw(14) << length_tot << setw(10) << ip << setw(14) << thePart.wgt << setw(14) << thePart.id;
  part_str << right << setprecision(14) << thePart.energy << "  " << thePart.x << "  " << thePart.y << "  " << thePart.z << "  " << thePart.xp << "  " << thePart.yp << "  " << length_tot << "  " << ip << "  " << thePart.wgt << "  " << thePart.id;
  return part_str.str();
}

void CheckLoss(PARTICLE &particle, string beamType, double length_tot, int iel, ELEMENT* elem, HTPlacetInterface* htplacetinter, int debuglevel, int ip, int& nlost, ostringstream& loss_str)
// check if the particle hits an aperture
{
  if (fabs(particle.energy)>std::numeric_limits<double>::epsilon() && elem->is_lost(particle)) //check if particle was not lost before (energy > 0) and if it is hitting an aperture (function is_lost in element.cc)
  {
    if(debuglevel>2) cout << "make_loss " << beamType << " particle " << ip << " has hit aperture " << ElementInfo(iel) << '\n';
    double X0=elem->get_material().X0;
    if(X0<=0) //If the element is a spoiler do multiple scattering - for a spoiler, the radiation length is explicit.
    { // not a spoiler. Mark as lost
      if(debuglevel>2) cout << "Nonpositive X0 =" << X0 << " direct loss - set energy and weight to 0" << '\n';
      nlost++;
      loss_str << ParticleInfo(particle, length_tot, ip) << '\n';
      //should we also introduce something like an aperture weight ->
      //aperture.weight+=fabs(particle.wgt);
      // mark particle as lost energy = 0, weight = 0
      particle.set_lost();
    }
    else
    { // spoiler, do multiple scattering
      if(debuglevel>2) cout << " X0=" << X0 << " make_msc" << '\n';
      htplacetinter->makeMsc(particle, elem->get_length(),X0);
    }
  }
}

int make_loss(BEAM* thebeam,string outDir,string beamType, double length_tot, int iel, ELEMENT* elem, string outname,HTPlacetInterface* htplacetinter,int debuglevel,int flg_header)
{
  int nlost = 0;
  ostringstream loss_str; // define an output stream. Write lost particles there. Write this at the end to a file only in case particles were actually lost
  if(!elem->is_tclcall())
  {
    if (elem->get_aperture_type_str()!="none")
    {
      if (beamType=="beam")
      {
        // define an output stream. Write lost particles there. Write this at the end to a file with header only in case particles were actually lost
        if(debuglevel>15) cout << "make_loss start to check if there are any losses here. beam=" << beamType << " length_tot=" << length_tot << " iel=" << iel << " name=" << elem->get_name() << '\n';
		
        if(!thebeam->particle_beam) // sliced ( core ) beam   --   start the extra work to get a particle beam copy of this
        {
          BEAM* wbloss=bunch_remake(thebeam);
          beam_copy(thebeam,wbloss);  // wbloss is the temporary copy of (core) beam
          slices_to_particles(wbloss,0);// wbloss - slices_to_particles(type). If type=0 the z position of the particles is the z-coordinate of the slice, if type=1 the z-distribution of one slice is taken into account
          for(int ip=0; ip<wbloss->slices; ip++) CheckLoss(wbloss->particle[ip], beamType, length_tot, iel, elem, htplacetinter, debuglevel, ip, nlost,loss_str);
          beam_delete(wbloss);
        }
        else
        {
          for(int ip=0; ip<thebeam->slices; ip++) CheckLoss(thebeam->particle[ip], beamType, length_tot, iel, elem, htplacetinter, debuglevel, ip, nlost,loss_str);
        }
      }
      else if ((beamType=="halo")||(beamType=="test")) // equivalent for halo/testbeam
      {
        if(debuglevel>15) cout << "make_loss start to check if there are any losses here. beam=" << beamType << " length_tot=" << length_tot << " iel=" << iel << " name=" << elem->get_name() << '\n';
        for(int ip=0; ip<thebeam->nhalo; ip++) CheckLoss(thebeam->particle_sec[ip], beamType, length_tot, iel, elem, htplacetinter, debuglevel, ip, nlost,loss_str);
      }
    }
    if(nlost) // now only write if particles were lost
    {
      string loss_out_name=string(outDir)+beamType+"_"+outname; // Lossbeam_nn.txt or Losshalo_nn.txt
      ofstream loss_out(loss_out_name.c_str());
      if(flg_header)
      {
        loss_out << "# " << elem->get_name() << "  " << length_tot << "  " << "  " << beamType << "  " << nlost << "  " << elem->get_aperture_type() << "  " << elem->get_aperture_x() << "   " << elem->get_aperture_y() << '\n'; // Write the loss file header with element name parms
      }
      loss_out << loss_str.str(); // write the particles from the stream
      loss_out.close();
    }
  }
  return nlost;
}

void drift_step(BEAM *beam, double length) // Straight tracking of beam over length. Used in beamline_track_background
{
  for (int i=0;i<beam->slices;i++)
  {
    PARTICLE &particle=beam->particle[i];
    particle.x+=length*particle.xp;
    particle.y+=length*particle.yp;
  }
}

void BeamAlloc(int ndim, int n_bunch=1, int slices_per_bunch=1)
// alloc global beam
// done either in beamline_track_background or tk_DummyHalo
// put here to unify and make sure CSRWAKE_DATA is also there
{
  if(getenv("HTGEN_DEBUG")) cout << "BeamAlloc with ndim=" << ndim << ", n_bunch=" << n_bunch << ", slices_per_bunch=" << slices_per_bunch << '\n'; // defined in structures_def.h   const int DimHalo=1000000;
  hb.particle_beam=true;// beam is particle beam
  hb.macroparticles=1;//number of macroparticles per slice
  hb.slices=ndim; //total number of (macro)particles
  hb.bunches = n_bunch;//number of bunches
  hb.slices_per_bunch = slices_per_bunch;// slices per bunch
  hb.particle=(PARTICLE*)xmalloc(sizeof(PARTICLE)*ndim);// particles
  hb.particle_number=(int*)xmalloc(sizeof(int)*n_bunch*slices_per_bunch);// hb.particle_number[i]=number of particles in slice i
  hb.csrwake=(CSRWAKE_DATA*)xmalloc(sizeof(CSRWAKE_DATA));
  hb.csrwake->terminal_dE_ds = NULL;
  hb.csrwake->terminal_nlambda = NULL;
  hb.csrwake->wake_enabled = 0;
  hb.csrwake->distance_from_sbend = 0;
  hb.csrwake->attenuation_length = 0;
  hb.csrwake->nbins = 0;
  hb.s=0.0;
}

//print halo  particles at elements, depeding on setting of fulltracking
void print_halo_files(string halo_out_name, ELEMENT* elem, int iel, double length_tot, BEAM* beam,int flg_header)
{
  ofstream halo_out(halo_out_name.c_str());
  if(flg_header & 1) // print  halo_nnn.txt file
  {
	halo_out << "# " << elem->get_name() << "  " << length_tot << "  " << beam->nhalo << "  " << elem->get_aperture_type() << "  " << elem->get_aperture_x() << "   " << elem->get_aperture_y() << '\n';
  } 
  if(flg_header & 2) // column variable names in each halo_nnn.txt file
  {
	halo_out << "Ener             x                y                 z           xp                 yp                 length_tot      ip  weight            id" << '\n';
  } 
  for(int ip = 0; ip < beam->nhalo ; ip++) // halo_nnn.txt
  {
	
    PARTICLE halopart=beam->particle_sec[ip]; // for halo use secondary particles
    if(halopart.energy < 0 && getenv("HTGEN_DEBUG")) // warnings - keep in mind: lost halo particles have energy 0
    {
      cout << "*** warning *** for halopart " << ip << " energy = " << halopart.energy << " at " << length_tot << " element " << iel << " " << ElementInfo(iel) << " see also " << halo_out_name << '\n';
      cout << ParticleInfo(halopart, length_tot, ip) << '\n';
    }
    if( ( abs(halopart.xp)>1.e8 || abs(halopart.yp)>1.e8 ) && getenv("HTGEN_DEBUG") && halopart.energy!=0)
    {
      cout << "*** warning *** for halopart " << ip << " large scattering angle at " << length_tot << " element " << iel << " " << ElementInfo(iel) << " see also " << halo_out_name << endl;
      cout << ParticleInfo(halopart, length_tot, ip) << '\n';
    }
    if(halopart.energy>0) halo_out << ParticleInfo(halopart, length_tot, ip) << '\n';
  }
  halo_out.close();
}

//print beam particles at elements, depeding on setting of fulltracking
void print_beam_files(string beam_out_name, ELEMENT* elem, int iel, double length_tot, BEAM* beam,int flg_header,int flg_printsliced)
{
  ofstream beam_out(beam_out_name.c_str()); // now beam_nnn.txt
  if(flg_header & 1) // print element information at the beginning of each beam_nnn.txt file
  {
	beam_out << "# " << elem->get_name() << "  " << length_tot << "  " << beam->slices << "  at" << ElementInfo(iel) << endl;
  }
  if(flg_header & 2) // column variable names in each beam_nnn.txt file
  {
	beam_out << "Ener             x                y                 z           xp                 yp                 length_tot      ip  weight            id" << '\n';
  } 
  PARTICLE beampart;
  if(!beam->particle_beam) //for a sliced beam convert the beam into a particle beam for printout
  {
    if(flg_printsliced)//do not convert sliced beam
    {
      for(int ip = 0; ip < beam->slices ; ip++)
      {
        PARTICLE beampart=beam->particle[ip]; // for beam use primary beam particles
        int islice = beam->get_slice_number(ip);
        beampart.z=beam->z_position[islice];
        beam_out << ParticleInfo(beampart, length_tot, ip) << "  " << islice << '\n';
      }
    }
    else //do convert sliced beam
    {
      BEAM* beamprint=bunch_remake(beam);  // copy beam to convert it to a particle beam
      beam_copy(beam,beamprint);
      slices_to_particles(beamprint,0);// convert sliced beam - slices_to_particles(type). If type=0 the z position of the particles is the z-coordinate of the slice, if type=1 the z-distribution of one slice is taken into account
      for(int ip = 0; ip < beamprint->slices ; ip++)
      {
        beampart=beamprint->particle[ip];
        beam_out << ParticleInfo(beampart, length_tot, ip) << '\n';
      }
      beam_delete(beamprint);
    }
  }
  else
  {
    for(int ip = 0; ip < beam->slices ; ip++)
    {
      beampart=beam->particle[ip]; // for beam use primary beam particles
      beam_out << ParticleInfo(beampart, length_tot, ip) << '\n';
    }
  }
  beam_out.close();
}

// tracks beam and halo/testbeam (if defined) through one element
void element_track(BEAM *beam, ELEMENT *elem,int flg_linac)
{
  string type = elem->get_aperture_type_str(); // save the aperture type
  elem->set_aperture_type_str("none"); // remove aperture for tracking
  if(beam->nhalo) //tracking of beam and halo/testbeam if halo/testbeam is defined
  {
    if(inter_data.track_photon == 1 ) //work with global beam hb
    {
      hb.slices = beam->nhalo; // special global beam hb,   take nhalo from beam
      for(int ip = 0; ip<hb.slices; ip++)
      {
        hb.particle[ip] = beam->particle_sec[ip]; // copy halo particles to global beam hb     -  consider to check and also copy number of particles
      }
      inter_data.track_photon = 0; // trick to not track twice the photons
      if ((!elem->is_cavity())&&(!elem->is_cavity_pets())) // if not cavity
        elem->track(&hb); // then work on global beam  hb
      else
        drift_step(&hb,elem->get_length()); // if cavity -- do nothing, just treat for the moment as simple drift
      inter_data.track_photon = 1; // end of trick
    }
    else // no photon tracking, H E R E  - check if correct
    {
      if ((!elem->is_cavity())&&(!elem->is_cavity_pets())) // if not cavity or PETS
      {
        hb.slices = beam->nhalo;  // special global beam hb,   take nhalo from beam
        for(int ip = 0; ip<hb.slices; ip++)
        {
          hb.particle[ip] = beam->particle_sec[ip]; // copy halo particles to global beam hb     -  consider to check and also copy number of particles
        }
        elem->track(&hb); // then work on global beam  hb
      }
      else
      {
        elem->track(beam);
      }
    }
    //if element is not a PET or cavity or photon tracking was chosen, the global beam particles need to be copied back to the beam and the beam must be tracked
    if((inter_data.track_photon == 1)||((!elem->is_cavity_pets())&&(!elem->is_cavity())))
    {
      elem->track(beam);//track the beam core
      for(int ip = 0; ip<beam->nhalo; ip++)// copy the result from the special global beam back to the normal beam copy beam
      {
        beam->particle_sec[ip]= hb.particle[ip];
        // WARNING : ONLY APPLY TO LINAC
        // TO DO: SET SECONDARY ENERGY TO ITS MOTHER'S ENERGY
        if(flg_linac)
        {
          int ran = (int) (rand()/(double)RAND_MAX * beam->slices);
          beam->particle_sec[ip].energy= beam->particle[ran].energy;
        }
      }
    }
  }
  else //if there is no halo/testbeam defined, just track the beam
  {
    elem->track(beam); //track the beam
  }
  elem->set_aperture_type_str(type.c_str());// back to aperture type
  //      elem->set_aperture(type.c_str(),aperture_x,aperture_y);
}

int beamline_track_background(BEAM *beam,char* c_outdir, double charge, int istart, int flg_linac, int flg_header, int flg_printsliced, int fulltracking)
// called once from tk_TrackBackground when a TrackBackground card is read in the input file.
{
  BEAMLINE *beamline=inter_data.beamline; // hbu  from global
  ELEMENT **element=beamline->element;
  int nloss_halo = 0;
  int nloss_beam = 0;
  int nloss_beam_tot = 0;
  int nloss_halo_tot = 0;
  int nloss_tot = 0;
  int nhalo_tot = 0;
  beam->nhalo=0; // make sure that nhalo is zero at the beginning.
  BEAM* wb=bunch_remake(beam);  // using bunch_remake from placeti3.cc,   calls bunch_make_2 which does bunch=(BEAM*)xmalloc(sizeof(BEAM));
  beam_copy(beam,wb);  // using beam_copy function from placeti3.cc      wb is now a copy of beam
  
  int debuglevel=0; // see also  htgen/include/HTErrorMessages.h
  char* htgen_debug_cstr = getenv("HTGEN_DEBUG");
  if(htgen_debug_cstr)
  {
    string htgen_debug(getenv("HTGEN_DEBUG"));
    istringstream iss(htgen_debug);
    iss >> debuglevel;
    cout << "beamline_track_background htgen_debug=" << htgen_debug;
  }
  if(debuglevel>0)
  {
    cout << " debuglevel=" << debuglevel << " c_outdir=" << c_outdir << '\n';
  }
  
  std::string outDir;
  if(c_outdir==NULL)  outDir = "./outbkg";
  else outDir = c_outdir;
  outDir.insert(outDir.size(),"/");
  string command = "mkdir "+ outDir;
  system(command.c_str());
  
  // optionally write scattering angle of halo particles
  ostringstream scatt_str;
  if(getenv("HTGEN_DEBUG")) scatt_str << "# particle id           theta             phi " << endl;
  
  // set up the interface with htgen
  if(debuglevel>15) cout << "beamline_track_background.  Setting up HTPlacetInterface" << '\n';
  HTPlacetInterface htplacetinter;
  htplacetinter.addProcess("mott");
  htplacetinter.addProcess("brem");
  htplacetinter.setEbeam( beamenergy(beam) );
  htplacetinter.setNpart(charge);
  
  if(debuglevel>15) cout << "beamline_track_background BeamEnergy=" << beamenergy(beam) << '\n';
  if(!dummyhalo_flg)
  {
    BeamAlloc(DimHalo,1,1); // DimHalo defined in structures_def.h   const int DimHalo=100000;
  }
  
  string outname; // use for names of extra files loss_  etc
  if(debuglevel>15)
  {
    cout << "beamline_track_background start debuglevel=" << debuglevel << " print element list " << '\n';
    PrintElementList();
  }
  double length_tot=0; // length_tot is used for printout only
  
  for (int iel=0;iel<beamline->n_elements;++iel) // loop over elements
  {
    if(!element[iel]->is_tclcall())
    {
      // setup output names like loss_nnn.txt
      outname = "loss_";
      ostringstream thei;
      // write the element number in the file name, adding zeroes in front for small numbers to get always 3 digits
      if(iel>99) thei << iel;
      else if(iel>9) thei << "0" << iel;
      else thei << "00" << iel;
      outname += thei.str() + ".txt";
	  
      htplacetinter.setEbeam( beamenergy(wb) );
      double length = element[iel]->get_length();
      length_tot += length;
	  
      if(dummyhalo_flg && iel==istart) // special simple dummy case
      {
        wb->nhalo = hb.slices;
        nhalo_tot = hb.slices;
        for(int ipart = 0; ipart<wb->nhalo; ipart++) wb->particle_sec[ipart]= hb.particle[ipart];
      }
	  
      //start to make, printout and track halo
      //track the halo and beam separately in all elements (global beam hb is tracked) except PETS and cavities, where beam and halo are tracked together (beam copy wb is tracked)
      if(debuglevel>15) cout << '\n' << " in beamline_track_background "; // start a new line for each element with a lot of debug printout
      config_element(iel,beamenergy(wb),&htplacetinter,debuglevel,flg_linac);//configure element, if it is not a tcl call
      string type = element[iel]->get_aperture_type_str(); // save the aperture type
      element_track(wb, element[iel],flg_linac);
	  
      //generate halo and check losses at the end of each element beginning with the element istart
      if(iel>=istart)
      {
        nhalo_tot += htplacetinter.makeHalo(wb,length,scatt_str); // create halo
        nloss_beam = make_loss(wb, outDir, "beam", length_tot, iel, element[iel], outname, &htplacetinter, debuglevel,flg_header); // check losses on beam
        nloss_halo = make_loss(wb, outDir, "halo", length_tot, iel, element[iel], outname, &htplacetinter, debuglevel,flg_header); // check losses on halo
        nloss_beam_tot += nloss_beam;
        nloss_halo_tot += nloss_halo;
        nloss_tot += nloss_beam + nloss_halo;
      }
	  
      // print out losses and halo at the end of each element
      bool print_flg = false;
      if(iel==istart+1) print_flg=true; // print always at 1st element
      if(iel==beamline->n_elements-2) print_flg=true;  // print always at the real last element  (last element is tcl call)
      if(nloss_halo>0 || nloss_beam>0) print_flg=true;  // print in case of losses
      if(fulltracking==1) print_flg=true; // print at all elements
	  if(fulltracking>1 && fulltracking==iel) print_flg=true; // print at element given by fulltracking
      if(print_flg==true) // write halo_nnn.txt and beam_nnn.txt files
      {
        string halo_out_name=string(c_outdir)+"/halo_"+thei.str()+".txt";
        print_halo_files(halo_out_name, element[iel], iel, length_tot, wb,flg_header);
		
        string beam_out_name=string(c_outdir)+"/beam_"+thei.str()+".txt";
        print_beam_files(beam_out_name, element[iel], iel, length_tot, wb,flg_header,flg_printsliced);
      }
      cout << "Element: " << iel << " - Halo " << nhalo_tot << " - Loss " << nloss_tot << '\n';
    }
    else
    {
      if(getenv("HTGEN_DEBUG")) cout << "Element is a Tcl call" << endl;
    }
  } // end of loop over elements
  if(getenv("HTGEN_DEBUG"))
  {
    string outname=string(c_outdir)+"/halo_scatt_angle.txt";
    ofstream scatt_out(outname.c_str());
    scatt_out << scatt_str.str();
    scatt_out.close();
  }
  beam_delete(wb);  
  cout << "TOTAL LOST  : " << nloss_tot << '\n';
  cout << "BEAM LOSSES : " << nloss_beam_tot << '\n';
  cout << "HALO LOSSES : " << nloss_halo_tot << '\n';
  cout << "TOTAL HALO  : " << nhalo_tot << '\n';
  return 0;
}

// ---------------------------------------------------------------------
int tk_TrackBackground(ClientData clientdata, Tcl_Interp *interp,int argc, char *argv[])
// decode TrackBackground card in input file, define the beam and call beamline_track_background
{
  int error;
  char *beamname=NULL;
  static char *dir=NULL; // hbu made static 14/10/2007
  int istart = 0;
  double charge = 0.;
  int fulltracking = 0;
  int flg_linac = 0;
  int flg_header = 0;
  int flg_printsliced = 0;

  static BEAM *beam=NULL; // hbu made static 14/10/2007
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beamname,
	  (char*)"Name of the beam to be used for tracking"},
    {(char*)"-dir",TK_ARGV_STRING,(char*)NULL,(char*)&dir,
	  (char*)"Directory for the results defaults to NULL (no output)"},
    {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&istart,
	  (char*)"Starting element number for background tracking"},
    {(char*)"-fulltracking",TK_ARGV_INT,(char*)NULL,(char*)&fulltracking,
	  (char*)"tracking mode: 1 means print at all elements, >1 just at this element. Always print losses"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,(char*)&charge,
	  (char*)"Number of particles / bunch"},
    {(char*)"-linac",TK_ARGV_INT,(char*)NULL,(char*)&flg_linac,
	  (char*)"set 1 if linac tracking, defaults=0"},
    {(char*)"-header",TK_ARGV_INT,(char*)NULL,(char*)&flg_header,
	  (char*)"set 1 if header is printed for each output file, default=0"},
    {(char*)"-printsliced",TK_ARGV_INT,(char*)NULL,(char*)&flg_printsliced,
	  (char*)"set 1 if sliced beam is printed, 0 if particle beam is printed, default=0"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK) return error;
  
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;
  
  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to TrackBackground",
                  TCL_VOLATILE);
    return TCL_ERROR;
  }
  // now define the beam
  if (beamname!=NULL)
  {
    beam=get_beam(beamname);
  }
  else
  {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }
  
  if (error) return error;
  beamline_track_background(beam, dir, charge, istart, flg_linac, flg_header, flg_printsliced, fulltracking);
  return TCL_OK;
}

// -------------------------------------------------------
int tk_Vacuum(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[])
{
  int error;
  // char *file_name=NULL;
  char *gas=NULL;
  char **v,**v2;
  double temperature=293.0,a=0,z=0,p=0,thetamin=1e-8,kmin=0.01; // gas parameters with default values
  int iel=0, nel = 1, n ,m;
  Tk_ArgvInfo table[]={
    {(char*)"-gaslist",TK_ARGV_STRING,(char*)NULL,(char*)&gas,
	  (char*)"List of the gas mixture"},  // read info like     -gas {	{ 7 14 100000. }  }   and set the char* gas
    {(char*)"-temperature",TK_ARGV_FLOAT,(char*)NULL,(char*)&temperature,
	  (char*)"Temperature in K"},
    {(char*)"-thetamin",TK_ARGV_FLOAT,(char*)NULL,(char*)&thetamin,
	  (char*)"Mott scatt. cutoff (microradian)"},
    {(char*)"-kmin",TK_ARGV_FLOAT,(char*)NULL,(char*)&kmin,
	  (char*)"Brem scatt. cutoff"},
    {(char*)"-iel",TK_ARGV_INT,(char*)NULL,(char*)&iel,
	  (char*)"Element number (default 0)"},
    {(char*)"-nel",TK_ARGV_INT,(char*)NULL,(char*)&nel,
	  (char*)"Number of elements -- not required any more"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0)) !=TCL_OK) return error;
  
  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to Vacuum",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  // get access to element[iel] to set_gas a,z,pressure..
  BEAMLINE *beamline=inter_data.beamline; // hbu  from global
  ELEMENT **element=beamline->element;
  if(iel >= beamline->n_elements)
  {
    Tcl_SetResult(interp,(char*)"Element number > number of elements",TCL_VOLATILE);
    return TCL_ERROR;
  }
	
  if(element[iel]->is_tclcall()) // element should not be a Tcl call
    {return TCL_OK;}

  if( (error=Tcl_SplitList(interp,gas,&n,&v)) ) return error; // assignment of error and check

  for(int i=0; i<n; i++)
  {
    error=Tcl_SplitList(interp,v[i],&m,&v2);
    if (m!=3)
    {
      Tcl_SetResult(interp,(char*)"Each element of gas must have three values",TCL_VOLATILE);
      return TCL_ERROR;
    }
    Tcl_GetDouble(interp,v2[1],&a);
    Tcl_GetDouble(interp,v2[0],&z);
    Tcl_GetDouble(interp,v2[2],&p); // gas pressure as given as 3rd -gas argument
    p *= 1.333e-7;      //hbu - poor conversion, improve
    thetamin *=1e-6;    // thetamin in murad
    element[iel]->set_gas(a,z,p,temperature,thetamin,kmin); // in element.h  #ifdef HTGEN
    Tcl_Free((char*)v2);
  }
  
  Tcl_Free((char*)v);
  return TCL_OK;
}

// -------------------------------------------------------
int tk_Material(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[])
{
  int error;
  double X0=-1.;
  int iel=0, nel = 1;
  // number of elements not actually needed here - it is taken from the beamline
  Tk_ArgvInfo table[]={
    {(char*)"-X0",TK_ARGV_FLOAT,(char*)NULL,(char*)&X0,
	  (char*)"Radiation length of material"},
    {(char*)"-iel",TK_ARGV_INT,(char*)NULL,(char*)&iel,
	  (char*)"Element number (default 0)"},
    {(char*)"-nel",TK_ARGV_INT,(char*)NULL,(char*)&nel,
	  (char*)"Number of elements -- not required any more"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0)) !=TCL_OK)
  {
    return error;
  }
  
  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to Material",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  // access to element[iel], set X0
  BEAMLINE *beamline=inter_data.beamline; // from global
  ELEMENT **element=beamline->element;
  if(iel < beamline->n_elements)
  {
    if(!element[iel]->is_tclcall()) element[iel]->set_radiation_length(X0);
  }
  else
  {
    Tcl_SetResult(interp,(char*)"Element number iel > number of elements",TCL_VOLATILE);
    return TCL_ERROR;
  }
  return TCL_OK;
}

// -------------------------------------------------------
int tk_DummyHalo(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[])
// create a halo beam
// type
// 0 flat
// 1 gaussian
// 2 on ellipse
// 3 from file given with -file option
// 4 from file for tracking with testbeam (sliced beam) given with -file option -beam beam0
// 5 copies the beam particles into the global beam hb
{
  int error;
  double xbound=0., xpbound=0., ybound=0., ypbound=0., energy = 1., edis = 0.;
  int npart = 1;
  int type = 0;
  double betax = 0., alphax = 0., betay = 0., alphay = 0.;
  char *name=NULL, *beamname=NULL, *fname=NULL;
  double rx = 1., ry = 1.;
  BEAM *beam=NULL;

  Tk_ArgvInfo table[]={
    {(char*)"-x",TK_ARGV_FLOAT,(char*)NULL,(char*)&xbound,
	  (char*)"flat distribution: x bound"},
    {(char*)"-xp",TK_ARGV_FLOAT,(char*)NULL,(char*)&xpbound,
	  (char*)"flat distribution: xp bound"},
    {(char*)"-y",TK_ARGV_FLOAT,(char*)NULL,(char*)&ybound,
	  (char*)"flat distribution: y bound"},
    {(char*)"-yp",TK_ARGV_FLOAT,(char*)NULL,(char*)&ypbound,
	  (char*)"flat distribution: yp bound"},
    {(char*)"-energy",TK_ARGV_FLOAT,(char*)NULL,(char*)&energy,
	  (char*)"flat distribution: reference energy"},
    {(char*)"-edis",TK_ARGV_FLOAT,(char*)NULL,(char*)&edis,
	  (char*)"flat distribution: energy dispersion in percent"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&name,
	  (char*)"Name of the halo/test beam"},
    {(char*)"-mainbeam",TK_ARGV_STRING,(char*)NULL,(char*)&beamname,
	  (char*)"Name of the main beam to be used for tracking"},
    {(char*)"-emittx",TK_ARGV_FLOAT,(char*)NULL,(char*)&rx,
	  (char*)"norm emittance in x"},
    {(char*)"-emitty",TK_ARGV_FLOAT,(char*)NULL,(char*)&ry,
	  (char*)"norm. emittance in y"},
    {(char*)"-npart",TK_ARGV_INT,(char*)NULL,(char*)&npart,
	  (char*)"Number of halo particles"},
    {(char*)"-type",TK_ARGV_INT,(char*)NULL,(char*)&type,
	  (char*)"0: flat , 1: gaussian , 2 : on ellipse 3: from file (x,xp,y,yp,e) 4: from file for test beam with a sliced main beam (e,x,xp,y,yp,wgt,islice or z,id)"},
    {(char*)"-betax",TK_ARGV_FLOAT,(char*)NULL,(char*)&betax,
	  (char*)"betax [m]"},
    {(char*)"-alphax",TK_ARGV_FLOAT,(char*)NULL,(char*)&alphax,
	  (char*)"alphax [m]"},
    {(char*)"-betay",TK_ARGV_FLOAT,(char*)NULL,(char*)&betay,
	  (char*)"betay [m]"},
    {(char*)"-alphay",TK_ARGV_FLOAT,(char*)NULL,(char*)&alphay,
	  (char*)"alphay [m]"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
	  (char*)"Input file"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0)) !=TCL_OK) return error;
  
  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to DummyHalo",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  dummyhalo_flg = true;
  int dim_halo=((std::max((int) npart, 1)+DimHalo-1)/DimHalo)*DimHalo;
  if((type==0)||(type==1)||(type==2))
  {
    if(getenv("HTGEN_DEBUG")) cout << "BeamAlloc with dim_halo=" << dim_halo << ", n_bunch=" << 1 << ", slices_per_bunch=" << 1 << '\n'; // defined in structures_def.h   const int DimHalo=1000000;
    BeamAlloc(dim_halo,1,1);
    hb.slices = npart;
  }
  
  double ran;
  // FLAT HALO
  if (type==0) // flat halo
  {
    cout << "Create flat DummyHalo with " << npart << " particles" << '\n';
    for(int ipart=0; ipart<hb.slices; ipart++)
    {
      ran = rand()/(double)RAND_MAX;
      hb.particle[ipart].x  = 2*(ran-0.5)*xbound;
      ran = rand()/(double)RAND_MAX;
      hb.particle[ipart].xp = 2*(ran-0.5)*xpbound;
      ran = rand()/(double)RAND_MAX;
      hb.particle[ipart].y  = 2*(ran-0.5)*ybound;
      ran = rand()/(double)RAND_MAX;
      hb.particle[ipart].yp = 2*(ran-0.5)*ypbound;
      ran = rand()/(double)RAND_MAX;
      hb.particle[ipart].energy = (2*(ran-0.5)*edis/100. + 1)*energy;
    }
  } // flat halo
  
  // HALO on Ellipse
  if (name)
  {
    beam=get_beam(name);
  }
  if ((type==1 || type==2) && (betax==0. || betay==0.))
  {
    cout << "[DummyHalo] You must set beta functions ...  exit" << endl;
    return TCL_ERROR;
  }
  
  rx *= EMITT_UNIT/1e-12*EMASS/energy;
  ry *= EMITT_UNIT/1e-12*EMASS/energy;
  if(type == 1) cout << "Create Gaussian Halo with " << npart << " particles" << '\n';
  if(type == 2) cout << "Create Ellipse Halo with " << npart << " particles" << '\n';
  if(betax!=0. || betay!=0.)
  {
    cout << "      - BETAX  " << betax
    << "      - ALPHAX " << alphax
    << "      - BETAY  " << betay
    << "      - ALPHAY " << alphay << '\n';
    cout << "      - Emittance x :" << rx
    << "      - Emittance y :" << ry << '\n';
  }
  if(type == 1) // gaussian
  {
    for(int ipart=0; ipart < hb.slices ; ipart++)
    {
      one_particle_from_twiss(betax,alphax,rx,betay,alphay,ry,energy,edis/100.*energy,hb.particle+ipart);
    }
  }
  if(type==2) // on ellipse
  {
    double dphx=TWOPI/npart;
    double dphy=TWOPI/npart;
    double phx=0.0;
    double phy=0.0;
    for(int ipart=0; ipart<hb.slices ; ipart++)
    {
      one_particle_ellipse(betax,alphax,rx,betay,alphay,ry,phx,phy,hb.particle+ipart);
      ran = rand()/(double)RAND_MAX;
      hb.particle[ipart].energy = (2*(ran-0.5)*edis/100. + 1)*energy;
      phx+=dphx;
      phy+=dphy;
    }
  }
  if (type==3) // HALO DISTRIBUTION FROM FILE 4D
  {
    if (fname==NULL)
    {
      cout << "[DummyHalo] You must specify a file name!" << endl;
      return TCL_ERROR;
    }
	
    ifstream curfile;
    string line;
    curfile.open(fname);
    int npart = 0;
    vector<double> vx, vy, vxp, vyp, ve;
    double x,y,xp,yp,e;
    if (curfile.good())
    {
      while (getline(curfile,line))
      {
        istringstream iss(line);
        if(npart>0)
        {
          x = y = xp = yp = e = 0.;
          iss >> x >> xp >> y >> yp >> e;
          vx.push_back(x);
          vy.push_back(y);
          vxp.push_back(xp);
          vyp.push_back(yp);
          ve.push_back(e);
        }
        npart++;
      }
      curfile.close();
    }
    else
    {
      cout << "[DummyHalo] problem with " << fname << '\n';
      return 1;
    }
    int dim_halo=((std::max((int) vx.size(), 1) + DimHalo-1)/DimHalo)*DimHalo;
    BeamAlloc(dim_halo,1,1);
    hb.slices = vx.size();
    for(int ipart=0; ipart<(int)vx.size(); ipart++)
    {
      hb.particle[ipart].x    = vx[ipart];
      hb.particle[ipart].xp   = vxp[ipart];
      hb.particle[ipart].y    = vy[ipart];
      hb.particle[ipart].yp   = vyp[ipart];
      //    if(energy) hb.particle[ipart].energy = ((ran-0.5)*edis/100. + 1)*energy;
      if(energy) hb.particle[ipart].energy = energy*(1+Select.Gauss()*edis/100.); // a
      else hb.particle[ipart].energy = ve[ipart];
    }
  } // from file 4D
  if (type==4) // test beam distribution from file
  {
    if (fname==NULL)
    {
      cout << "[DummyHalo] You must specify a file name!" << endl;
      return TCL_ERROR;
    }
    if (beamname!=NULL)
    {
      beam=get_beam(beamname);
    }
    else
    {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"-mainbeam must be defined\n",NULL);
      error=TCL_ERROR;
    }
	
    ifstream curfile;
    string line;
    curfile.open(fname);
    int npart = 0;
    vector<double> vx, vy, vxp, vyp, ve, vislice, vwgt;
    vector<unsigned int> vid;
    double x,y,xp,yp,islice,e,wgt;
    unsigned int id;
    // set particle_numb_sec and so on
    if (curfile.good())
    {
      while (getline(curfile,line))
      {
        istringstream iss(line);
        x = y = xp = yp = e = wgt = islice = id = 0;
        iss >> e >> x >> xp >> y >> yp >> wgt >> islice >> id;
        vx.push_back(x);
        vy.push_back(y);
        vxp.push_back(xp);
        vyp.push_back(yp);
        vislice.push_back(islice);
        ve.push_back(e);
        vwgt.push_back(wgt);
        vid.push_back(id);
        npart++;
      }
      curfile.close();
    }
    else
    {
      cout << "[DummyHalo] problem with " << fname << '\n';
      return 1;
    }
    if(!beam->particle_beam)
    {
      //set up global beam hb
      int ndim = std::max((int) vx.size(), 1);
      if(getenv("HTGEN_DEBUG")) cout << "background.cc tk_DummyHalo malloc for global beam particle with ndim=" << ndim << ", n_bunch=" << beam->bunches << ", slices_per_bunch=" << beam->slices_per_bunch << '\n'; // defined in structures_def.h   const int DimHalo=400000;
      BeamAlloc(ndim,beam->bunches,beam->slices_per_bunch);//allocate memory for global beam hb
      int nslices = (hb.slices_per_bunch)*(hb.bunches); // total number of slices in the testbeam
      for(int it=0; it<nslices; it++) hb.particle_number[it]=0;
      for(int ipart=0; ipart<(int)vx.size(); ipart++)
      {
        hb.particle[ipart].x    = vx[ipart];
        hb.particle[ipart].xp   = vxp[ipart];
        hb.particle[ipart].y    = vy[ipart];
        hb.particle[ipart].yp   = vyp[ipart];
        hb.particle[ipart].energy = ve[ipart];
        hb.particle[ipart].wgt = 1;
        hb.particle[ipart].id=vid[ipart];
        int i_slice = (int) vislice[ipart]; //slice number of particle
        if((i_slice<nslices)&&(i_slice>-1))
        {
          hb.particle[ipart].z = beam->z_position[i_slice];//z-position of slice with slice number i_slice
          hb.particle_number[i_slice]++; //number of particles in this slice - has to be set for tracking in cavities and PETS!
        }
        else
        {
          cout << "[DummyHalo] Slice number of testbeam particle " << ipart << " is wrong!" << endl;
          return TCL_ERROR;
        }
      }
    }
    else
    {
      cout << "[DummyHalo] DummyHalo -type 4 only works for a sliced beam!" << endl;
      return TCL_ERROR;
    }
  } // from file
  if (type==5) // to check halo tracking
  {
    if (beamname!=NULL)
    {
      beam=get_beam(beamname);
    }
    else
    {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"-mainbeam must be defined\n",NULL);
      error=TCL_ERROR;
    }
	
    //set up global beam hb
    int ndim = beam->slices;//total number of particles
    if(getenv("HTGEN_DEBUG")) cout << "background.cc tk_DummyHalo malloc for global beam particle with ndim=" << ndim << ", n_bunch=" << beam->bunches << ", slices_per_bunch=" << beam->slices_per_bunch << '\n';
    BeamAlloc(ndim,beam->bunches,beam->slices_per_bunch);//allocate memory for global beam hb
    int nslices = (hb.slices_per_bunch)*(hb.bunches); // total number of slices in the testbeam
    //set the number of particles in each slice
    //for a sliced beam the number of macroparticles per slice is fixed. For a particle beam, this number can vary. get_slice_number assumes, that the number of macroparticles per slice is fixed and therefore gives the wrong result in case of a particle beam - so don't use it in the case of the particle beam
    for(int it=0; it<nslices; it++) hb.particle_number[it]=beam->particle_number[it];
    for(int ipart=0; ipart<ndim; ipart++)
    {
      hb.particle[ipart].x    = beam->particle[ipart].x;
      hb.particle[ipart].xp   = beam->particle[ipart].xp;
      hb.particle[ipart].y    = beam->particle[ipart].y;
      hb.particle[ipart].yp   = beam->particle[ipart].yp;
      hb.particle[ipart].energy = beam->particle[ipart].energy;
      hb.particle[ipart].wgt = 1;
      //hb.particle[ipart].wgt=beam->particle[ipart].wgt;
      hb.particle[ipart].id=(unsigned int)ipart;
      if(!beam->particle_beam) hb.particle[ipart].z = beam->z_position[(int)beam->get_slice_number(ipart)];//z-position of slice with slice number i_slice
      else hb.particle[ipart].z = beam->particle[ipart].z;
    }
  }
  
  return TCL_OK;
}

// -------------------------------------------------------
int Background_Init(Tcl_Interp *interp) // called from  placet_tk.cc,   creating (not calling) the commands
{
  Placet_CreateCommand(interp,(char*)"TrackBackground",tk_TrackBackground,NULL,NULL);
  Placet_CreateCommand(interp,(char*)"Vacuum",tk_Vacuum,NULL,NULL);
  Placet_CreateCommand(interp,(char*)"Material",tk_Material,NULL,NULL);
  Placet_CreateCommand(interp,(char*)"DummyHalo",tk_DummyHalo,NULL,NULL);
  return 0;
}
#endif
