#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_multimin.h>

#include <tcl.h>
#include <tk.h>
#include <cstring>
#include <iomanip>
#include <fstream>
#include <vector>
#include "placet.h"
#include "beamline.h"
#include "structures_def.h"
#include "placet_tk.h"
#include "placeti3.h"
#include "track.h"
#include "bpm.h"
#include "random.hh"
#include "peder.h"
#include "select.h"
#include "cavity.h"

extern Tcl_Interp* beamline_survey_hook_interp;

// unnamed namespace to limit scope
namespace {

BEAM *knob_ref=NULL;
int knobpenalty=0;

typedef struct {
  double *initcoords;
  double *knobcoords;
  double *range;
  //double ***cav_y_matrix, ***quaoffset.y_matrix;
  int nb, ns, nm, nknobs;
  double *knobmatrix;
  int ncols, nrows, nknobs_mod;
  double moverstepsize;
  double beta_x, beta_y, sigma_x, sigma_y;
  int nphotons;
  double penaltylimit;
} BUMP_INFO_STRUCT;

BUMP_INFO_STRUCT *bump_information=NULL;

int bump_iter_ctr=0;

typedef struct {
  //struct XY {
  //  double x;
  //  double y;
  //} quad_jitter, cav_jitter, quad_walk, cav_walk;
  //std::vector<XY> jitters;

  double quad_jitter_x, quad_jitter_y, cav_jitter_x, cav_jitter_y;
  double quad_walk_x, quad_walk_y, cav_walk_x, cav_walk_y;
  double beam_phase, beam_energy, rf_phase, rf_grad;
  int cav_n_group, cav_n_offset;
  double *last_jitter;
} DYNAMIC_EFFECTS_STRUCT;

DYNAMIC_EFFECTS_STRUCT *dynamic_effects;

void init_dyneff(DYNAMIC_EFFECTS_STRUCT* de)
{
  de->quad_jitter_x=0;
  de->quad_jitter_y=0;
  de->cav_jitter_x=0;
  de->cav_jitter_y=0;

  de->quad_walk_x=0;
  de->quad_walk_y=0;
  de->cav_walk_x=0;
  de->cav_walk_y=0;

  de->beam_phase=0;
  de->beam_energy=0;
  de->rf_phase=0;
  de->rf_grad=0;

  de->cav_n_group=1;
  de->cav_n_offset=0;

  de->last_jitter=NULL;
}

void free_dyneff(ClientData de)
{
  free((DYNAMIC_EFFECTS_STRUCT*) de);
}

void turnknob(char *name, double amount) 
{
  char script[100];
  snprintf(script, 100, "%s %g", name, amount);
  Tcl_Eval(beamline_survey_hook_interp, script);
}

double discretestep(double wanted, double minstep)
{
  if (minstep<=0) {
    return wanted;
  } else {
    return minstep*nearbyint(wanted/minstep);
  }
}

double calcemitt(const gsl_vector *knobvalues, void *params)
{
  double wgt;
  double wsum=0,emean=0,ymean=0,ypmean=0,xmean=0;
  
  bump_iter_ctr++;

  BUMP_INFO_STRUCT *bump_info = (BUMP_INFO_STRUCT *)params;
  //  DYNAMIC_EFFECTS_STRUCT *dyneff=dynamic_effects;
  
  int nlines=bump_info->nb*bump_info->ns*bump_info->nm;

  double coords[nlines][4];
  double change;
  int nknobs;

  nknobs=bump_info->nknobs;

  if (bump_info->knobmatrix) {
    int nrows=bump_info->nrows;
    int ncols=bump_info->ncols;
    gsl_vector *tmpknobvalues = gsl_vector_alloc(nknobs);
    for (int i=0;i<nrows;i++) {
      double tmp;
      tmp=0;
      for (int j=0;j<ncols;j++) {
	tmp+=bump_info->knobmatrix[i*ncols+j]*gsl_vector_get(knobvalues,j);
      }
      gsl_vector_set(tmpknobvalues,i,discretestep(tmp,bump_info->moverstepsize));
    }
    for (int i=0;i<nlines;i++) {
      for (int j=0;j<4;j++) {
	change=0;
	for (int k=0;k<nknobs;k++) {
	  change+=gsl_vector_get(tmpknobvalues,k)*bump_info->knobcoords[4*(nlines*k+i)+j];
	}
	coords[i][j]=bump_info->initcoords[17*i+j+3]+change;
      }
    }
  } else {
    for (int i=0;i<nlines;i++) {
      for (int j=0;j<4;j++) {
	change=0;
	for (int k=0;k<nknobs;k++) {
	  change+=gsl_vector_get(knobvalues,k)*bump_info->knobcoords[4*(nlines*k+i)+j];
	}
	coords[i][j]=bump_info->initcoords[17*i+j+3]+change;
      }
    }
  }

  /*
  if (bump_info->quaoffset.y_matrix) {
    double rndmn;
    for (int i=0;i<inter_data.beamline->n_quad;i++) {
      rndmn=Instrumentation.Gauss();
      for (int j=0;j<nlines;j++) {
	for (int k=0;k<4;k++) {
	  coords[j][k]=coords[j][k]+rndmn*dyneff->quad_jitter_y*bump_info->quaoffset.y_matrix[i][j][k];
	}
      }
    }
  }
  */

  double tuningsignal;

  if (!bump_info->nphotons) {
    for (int i=0;i<nlines;i++) {
      wgt = fabs(bump_info->initcoords[17*i+1]);
      wsum += wgt;
      emean += wgt * fabs(bump_info->initcoords[17*i+2]);
      ymean += wgt * coords[i][2];
      ypmean += wgt * coords[i][3];
    }
    
    emean /= wsum;
    ymean /= wsum;
    ypmean /= wsum;
    
    double yymean=0,yypmean=0,ypypmean=0;
    
    for (int i=0;i<nlines;i++) {
      wgt = fabs(bump_info->initcoords[17*i+1]);
      yymean += wgt * ((coords[i][2]-ymean)*(coords[i][2]-ymean)+bump_info->initcoords[17*i+10]);
      yypmean += wgt * ((coords[i][2]-ymean)*(coords[i][3]-ypmean)+bump_info->initcoords[17*i+11]);
      ypypmean += wgt * ((coords[i][3]-ypmean)*(coords[i][3]-ypmean)+bump_info->initcoords[17*i+12]);
    }
    
    yymean /= wsum;
    yypmean /= wsum;
    ypypmean /= wsum;
    
    tuningsignal = sqrt(yymean * ypypmean - yypmean * yypmean) * emean / 0.511;
  } else {
    int i,n;
    double sum=0.0;
    int nph=bump_info->nphotons;
    double sx=bump_info->sigma_x;
    double sy=bump_info->sigma_y;
    double bx=bump_info->beta_x;
    double by=bump_info->beta_y;

    R_MATRIX sxx, sxy, syy;
    PARTICLE p, p0;

    wsum=0;
    xmean=0;
    ymean=0;

    std::vector<double> weights; 
    for (i=0;i<nlines;++i){
      wgt = fabs(bump_info->initcoords[17*i+1]);
      weights.push_back(wgt);
      wsum += wgt;
      xmean += wgt * coords[i][0];
      ymean += wgt * coords[i][2];
    }

    xmean/=wsum;
    ymean/=wsum;

    Select.DiscreteDistributionSet(weights); 
    for (i=0;i<nph;++i){
      n=Select.DiscreteDistribution();
      p0.energy=bump_info->initcoords[17*n+2];
      p0.x=coords[n][0];
      p0.xp=coords[n][1];
      p0.y=coords[n][2];
      p0.yp=coords[n][3];

      sxx.r11=bump_info->initcoords[17*n+7];
      sxx.r12=bump_info->initcoords[17*n+8];
      sxx.r22=bump_info->initcoords[17*n+9];
      syy.r11=bump_info->initcoords[17*n+10];
      syy.r12=bump_info->initcoords[17*n+11];
      syy.r22=bump_info->initcoords[17*n+12];
      //sxy not used by one_particle...
      one_particle(&sxx,&sxy,&syy,&p0,&p);
      if (sx>0.0) {
	  if (sy>0.0) {
	      if (Default.Uniform()<exp(-0.5* ((p.x-xmean)*(p.x-xmean)/(sx*sx)+(p.y-ymean)*(p.y-ymean)/(sy*sy)))) {
		  sum+=1.0;// r.value old
	      }
	  }
      }
      else {
	  if (sy>0.0) {
	      if (Default.Uniform()<exp(-0.5*(p.y-ymean)*(p.y-ymean)/(sy*sy))) {
		  sum+=1.0;//r.value old
	      }
	  }
      }
    }

    double hx, hxp, hy, hyp;
    xmean=0;
    ymean=0;
    
    weights.clear();
    
    for (i=0;i<nlines;++i){
      hx=bx*coords[i][1];
      hxp=(-1/bx)*coords[i][0];
      hy=by*coords[i][3];
      hyp=(-1/by)*coords[i][2];
      coords[i][0]=hx;
      coords[i][1]=hxp;
      coords[i][2]=hy;
      coords[i][3]=hyp;
      wgt = fabs(bump_info->initcoords[17*i+1]);
      weights.push_back(wgt);
      xmean += wgt * coords[i][0];
      ymean += wgt * coords[i][2];
    }

    xmean/=wsum;
    ymean/=wsum;

    Select.DiscreteDistributionSet(weights); 
    for (i=0;i<nph;++i){
      n=Select.DiscreteDistribution();
      p0.energy=bump_info->initcoords[17*n+2];
      p0.x=coords[n][0];
      p0.xp=coords[n][1];
      p0.y=coords[n][2];
      p0.yp=coords[n][3];

      sxx.r11=bump_info->initcoords[17*n+7];
      sxx.r12=bump_info->initcoords[17*n+8];
      sxx.r22=bump_info->initcoords[17*n+9];
      syy.r11=bump_info->initcoords[17*n+10];
      syy.r12=bump_info->initcoords[17*n+11];
      syy.r22=bump_info->initcoords[17*n+12];
      //sxy not used by one_particle...
      one_particle(&sxx,&sxy,&syy,&p0,&p);
      if (bump_info->sigma_x>0.0) {
	  if (bump_info->sigma_y>0.0) {
	      if (Default.Uniform()<exp(-0.5* ((p.x-xmean)*(p.x-xmean)/(sx*sx)
				      +(p.y-ymean)*(p.y-ymean)/(sy*sy)))) {
		sum+=1.0;
	      }
	  }
      }
      else {
	  if (sy>0.0) {
	      if (Default.Uniform()<exp(-0.5*(p.y-ymean)*(p.y-ymean)/(sy*sy))) {
		sum+=1.0;
	      }
	  }
      }
    }
  
    //  placet_printf(INFO,"%g ", sum/nph);

    tuningsignal=sum/nph;
  }
  
  double penalty=1.0;
  double knobratio;
  
  if (bump_info->penaltylimit) {
    for (int k=0;k<nknobs;k++) {
      knobratio=fabs(gsl_vector_get(knobvalues,k))/bump_info->penaltylimit;
      if (knobratio>1.0) {
	penalty *= knobratio;
      }
    }
    
    //  placet_printf(INFO,"Penalty: %g\n",penalty);
    
    if (!bump_info->nphotons && knobpenalty) {
      tuningsignal = penalty*tuningsignal;
    }
  }

  return tuningsignal;
} 
} // end of namespace

/**********************************************************************/
/*                      DynamicEffectsSet                             */
/**********************************************************************/

int tk_DynamicEffectsSet(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  DYNAMIC_EFFECTS_STRUCT *dyneff;
  dyneff=(DYNAMIC_EFFECTS_STRUCT*)malloc(sizeof(DYNAMIC_EFFECTS_STRUCT));
  init_dyneff(dyneff);

  char *name=NULL;

  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"If a name is specified a command with that name is created that can then be used in the same way as ApplyJitter (optional)."},
    {(char*)"-quad_jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->quad_jitter_x,
     (char*)"Horizontal quadrupole jitter  [micro m]"},
    {(char*)"-quad_jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->quad_jitter_y,
     (char*)"Vertical quadrupole jitter  [micro m]"},
    {(char*)"-cav_jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->cav_jitter_x,
     (char*)"Horizontal cavity jitter  [micro m]"},
    {(char*)"-cav_jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->cav_jitter_y,
     (char*)"Vertical cavity jitter  [micro m]"},
    {(char*)"-quad_walk_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->quad_walk_x,
     (char*)"Horizontal quadrupole random walk  [micro m]"},
    {(char*)"-quad_walk_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->quad_walk_y,
     (char*)"Vertical quadrupole random walk  [micro m]"},
    {(char*)"-cav_walk_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->cav_walk_x,
     (char*)"Horizontal cavity random walk  [micro m]"},
    {(char*)"-cav_walk_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->cav_walk_y,
     (char*)"Vertical cavity random walk  [micro m]"},
    //    {(char*)"-beam_phase",TK_ARGV_FLOAT,(char*)NULL,
    // (char*)&dyneff->beam_phase,
    // (char*)"Incoming beam phase jitter [deg]"},
    //{(char*)"-beam_energy",TK_ARGV_FLOAT,(char*)NULL,
    // (char*)&dyneff->beam_energy,
    // (char*)"Incoming beam energy jitter [?]"},
    {(char*)"-rf_phase",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->rf_phase,
     (char*)"Main linac RF phase jitter [deg]"},
    {(char*)"-rf_grad",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&dyneff->rf_grad,
     (char*)"Main linac RF gradient jitter [GV/m]"},
    {(char*)"-cav_n_group",TK_ARGV_INT,(char*)NULL,
     (char*)&dyneff->cav_n_group,
     (char*)"Number of cavities per power supply (used w. rf_phase and rf_amp options)"},
    {(char*)"-cav_n_offset",TK_ARGV_INT,(char*)NULL,
     (char*)&dyneff->cav_n_offset,
     (char*)"Number of cavities powered by the first power supply (used w. rf_phase and rf_grad options)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    free(dyneff);
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    free(dyneff);
    return TCL_ERROR;
  }

  if (dyneff->cav_n_offset<0){
    dyneff->cav_n_offset=0;
  }
  if (dyneff->cav_n_group<=1){
    dyneff->cav_n_group=1;
    dyneff->cav_n_offset=0;
  }
  if (dyneff->cav_n_offset>=dyneff->cav_n_group){
    dyneff->cav_n_offset=dyneff->cav_n_offset%dyneff->cav_n_group;
  }
    
  if (name) {
    Tcl_CreateCommand(interp,name,&tk_ApplyJitter,dyneff,free_dyneff);
  } else {
    if (dynamic_effects) {
      free(dynamic_effects);
    }
    dynamic_effects=dyneff;
  }   

  return TCL_OK;
}


/**********************************************************************/

int tk_ApplyJitter(ClientData clientdata,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int reset=0;
 
  Tk_ArgvInfo table[]={
    {(char*)"-reset",TK_ARGV_INT,(char*)NULL,
     (char*)&reset,
     (char*)"If not 0 the jitter (but not the random walk) will be reset and nothing else will happen. Defaults to 0."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"Usage: ",argv[0],"\n",NULL);
    return TCL_ERROR;
  }

  DYNAMIC_EFFECTS_STRUCT *dyneff;

  if (clientdata) {
    dyneff=(DYNAMIC_EFFECTS_STRUCT*)clientdata;
  } else {    
    dyneff=dynamic_effects;
  }
  
  int n_jitter_values=((dyneff->quad_jitter_x>0)+(dyneff->quad_jitter_y>0))*inter_data.beamline->n_quad+((dyneff->cav_jitter_x>0)+(dyneff->cav_jitter_y>0))*inter_data.beamline->n_cav+((dyneff->rf_phase>0)+(dyneff->rf_grad>0))*((int)ceil(double(inter_data.beamline->n_cav)/dyneff->cav_n_group)+(dyneff->cav_n_offset>0 && !(inter_data.beamline->n_cav%dyneff->cav_n_group)));
  
  if (!dyneff->last_jitter){    
    dyneff->last_jitter=(double*)malloc(sizeof(double)*n_jitter_values);
    for (int i=0;i<n_jitter_values;i++){
      dyneff->last_jitter[i]=0;
      }
  }
  
  double jitter;
  double dphase, tmp_phase, dgrad, tmp_grad;
  
  int cav_no=0;
  int jv_ctr=0;
  
  int first=0;
  int last=inter_data.beamline->n_elements;

  if (reset){
    for (int i=first;i<last;i++){
      ELEMENT *element=inter_data.beamline->element[i];
      if (CAVITY *cavity=element->cavity_ptr()) {
  	if (dyneff->cav_jitter_x){
	  element->offset.x-=dyneff->last_jitter[jv_ctr];
	  jv_ctr++;
	} 
	if (dyneff->cav_jitter_y){
	  element->offset.y-=dyneff->last_jitter[jv_ctr];
	  jv_ctr++;
	}
	if (dyneff->rf_phase){
	  if (cav_no==0 || (cav_no % dyneff->cav_n_group)==dyneff->cav_n_offset){
	    jv_ctr++;
	  }
    cavity->set_phase(cavity->get_phase()-PI/180.0*dyneff->last_jitter[jv_ctr-1]);
	}
	if (dyneff->rf_grad){
	  if (cav_no==0 || (cav_no % dyneff->cav_n_group)==dyneff->cav_n_offset){
	    jv_ctr++;
	  }	
	  cavity->set_gradient(cavity->get_gradient()-dyneff->last_jitter[jv_ctr-1]);
	}
	cav_no++;
      } else if (element->is_quad()) {
	if (dyneff->quad_jitter_x){
	  element->offset.x-=dyneff->last_jitter[jv_ctr];
	  jv_ctr++;
	}
	if (dyneff->quad_jitter_y){
	  element->offset.y-=dyneff->last_jitter[jv_ctr];
	  jv_ctr++;
	}
      }
    }
  } else {
    for (int i=first;i<last;i++){
      ELEMENT *element=inter_data.beamline->element[i];
      if (CAVITY *cavity=element->cavity_ptr()) {
	if (dyneff->cav_jitter_x){
	  jitter=dyneff->cav_jitter_x*Instrumentation.Gauss();
	  element->offset.x+=jitter-dyneff->last_jitter[jv_ctr];
	  dyneff->last_jitter[jv_ctr]=jitter;
	  jv_ctr++;
	} 
	if (dyneff->cav_jitter_y){
	  jitter=dyneff->cav_jitter_y*Instrumentation.Gauss();
	  element->offset.y+=jitter-dyneff->last_jitter[jv_ctr];
	  dyneff->last_jitter[jv_ctr]=jitter;
	  jv_ctr++;
	}
	if (dyneff->cav_walk_x){
	  jitter=dyneff->cav_walk_x*Instrumentation.Gauss();
	  element->offset.x+=jitter;
	}
	if (dyneff->cav_walk_y){
	  jitter=dyneff->cav_walk_y*Instrumentation.Gauss();
	  element->offset.y+=jitter;
	}
	if (dyneff->rf_phase){
	  if (cav_no==0 || (cav_no % dyneff->cav_n_group)==dyneff->cav_n_offset){
	    dphase=dyneff->rf_phase*Instrumentation.Gauss();
	    tmp_phase=dyneff->last_jitter[jv_ctr];
	    dyneff->last_jitter[jv_ctr]=dphase;
	    jv_ctr++;
	  }
	  cavity->set_phase(cavity->get_phase()+PI/180.0*(dphase-tmp_phase));
	}
	if (dyneff->rf_grad){
	  if (cav_no==0 || (cav_no % dyneff->cav_n_group)==dyneff->cav_n_offset){
	    dgrad=dyneff->rf_grad*Instrumentation.Gauss();
	    tmp_grad=dyneff->last_jitter[jv_ctr];
	    dyneff->last_jitter[jv_ctr]=dgrad;
	    jv_ctr++;
	  }
	  cavity->set_gradient(cavity->get_gradient()+dgrad-tmp_grad);
	}
	cav_no++;
      } else if (element->is_quad()) {
	if (dyneff->quad_jitter_x){
	  jitter=dyneff->quad_jitter_x*Instrumentation.Gauss();
	  element->offset.x+=jitter-dyneff->last_jitter[jv_ctr];
	  dyneff->last_jitter[jv_ctr]=jitter;
	  jv_ctr++;
	}
	if (dyneff->quad_jitter_y){
	  jitter=dyneff->quad_jitter_y*Instrumentation.Gauss();
	  element->offset.y+=jitter-dyneff->last_jitter[jv_ctr];
	  dyneff->last_jitter[jv_ctr]=jitter;
	  jv_ctr++;
	}
	if (dyneff->quad_walk_x){
	  jitter=dyneff->quad_walk_x*Instrumentation.Gauss();
	  element->offset.x+=jitter;
	}
	if (dyneff->quad_walk_y){
	  jitter=dyneff->quad_walk_y*Instrumentation.Gauss();
	  element->offset.y+=jitter;
	}
      }
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                         GetResponseMatrix                          */
/**********************************************************************/

int tk_GetResponseMatrix(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		 char *argv[])
{
  int error, i, j;
  int noise = 0;
  int n_quads=0, n_bpms=0;
  char **quad_numlist, **bpm_numlist; 
  char *quads, *bpms;
  long from_element, to_element, last_element;
  long element_nr;

  double refval[90];
  double testval;
  char *beamname = NULL;

  BEAMLINE *beamline;
  ELEMENT **element;
  BEAM *b;
  BEAM *beam;
  BEAM *testbeam;
  BEAM *refbeam;

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: GetResponseMatrix"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Beam name"},
    {(char*)"-quads",TK_ARGV_STRING,(char*)NULL,
     (char*)&quads,
     (char*)"List of quadrupoles"},
    {(char*)"-bpms",TK_ARGV_STRING,(char*)NULL,
     (char*)&bpms,
     (char*)"List of bpms"},
    {(char*)"-noise",TK_ARGV_INT,(char*)NULL,
     (char*)&noise,
     (char*)"If not 0 the BPM resolution will be taken into account."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (beamname!=NULL) {
    b=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    return TCL_ERROR;
  }

  if (Tcl_SplitList(interp, quads, &n_quads, &quad_numlist) != TCL_OK) {
    return TCL_ERROR;
  }
  if (Tcl_SplitList(interp, bpms, &n_bpms, &bpm_numlist) != TCL_OK) {
    return TCL_ERROR;
  }

  beamline = inter_data.beamline;
  element = beamline->element;

  beam = bunch_remake(b);
  testbeam = bunch_remake(b);
  refbeam = bunch_remake(b);
  beam_copy(b, beam);
  beam_copy(b, refbeam);

  beamline_survey_hook_interp=interp;

  Tcl_ExprLong(interp,bpm_numlist[n_bpms-1], &last_element);
  
  bunch_track_0(beamline, refbeam, 0, last_element+1);
  for (j=0; j<n_bpms; j++) {
    Tcl_ExprLong(interp, bpm_numlist[j], &element_nr);
    if (noise)
      refval[j] = element[element_nr]->get_bpm_y_reading();
    else
      refval[j] = element[element_nr]->get_bpm_y_reading_exact();
  }

  beam_delete(refbeam);

  from_element = 0;

  double matrix[n_quads][n_bpms];

  for (i=0; i<n_quads; i++) {
    Tcl_ExprLong(interp, quad_numlist[i], &to_element);
    bunch_track_0(beamline, beam, from_element, to_element);
    from_element = to_element;
    beam_copy(beam, testbeam);
    
    //quad_value = element[quad_numlist[i]]->y();
    element[to_element]->offset.y += 1;
    bunch_track_0(beamline, testbeam, from_element,last_element+1);
    
    for (j=0; j<n_bpms; j++) {
      //testval[i*n_bpms+j] = element[bpm_num[j]]->info1;
      Tcl_ExprLong(interp, bpm_numlist[j], &element_nr);
      if (noise)
	testval = element[element_nr]->get_bpm_y_reading();
      else
	testval = element[element_nr]->get_bpm_y_reading_exact();
      testval = testval - refval[j];
      matrix[i][j]=testval;
    }
    element[to_element]->offset.y -= 1;
  }

  Tcl_ResetResult(interp);
  char val[20];
  char row[1800];

  for (i=0;i<n_quads;i++) {
    row[0]='\0';
    int row_length=0; // counter to avoid overflow
    for (j=0;j<n_bpms;j++) {
      int val_length = 0;
      if (j==0)
	val_length = snprintf(val, 20, "%g", matrix[i][j]);
      else
	val_length = snprintf(val, 20, " %g", matrix[i][j]);
      // assuming limit is not hit, val_length is length of val
      row_length += val_length;
      if (row_length < 1800) {strncat(row, val, val_length);}
      else {
	placet_cout << WARNING << "GetResponseMatrix: Matrix too large, increase buffer size" << endmsg;
	break;
      }
    }
    Tcl_AppendElement(interp, row);  
  }

  beam_delete(beam);
  beam_delete(testbeam);

  return TCL_OK;
}

/**********************************************************************/
/*                               CalcEmitt                            */
/**********************************************************************/

int tk_CalcEmitt(ClientData /*clientdata*/,Tcl_Interp *interp,
		 int argc,char *argv[])
{
  int error;
  int nrofcavs, n = 0, namelength, i;
  char filenr[10];
  char **cavoffset;
  double coffset;
  char *name;
  char filename[20];
  double y[1000], yp[1000], weight[1000];
  double dyv[10], dypv[10];
  double nemitt;
  double totalw = 0;
  double meany = 0, meanyp = 0, meane = 0, meanr11 = 0, meanr12 = 0, meanr22 = 0;
  double w, e, yv, ypv, r11, r12, r22;
  double yy = 0, yyp = 0, ypyp = 0;
  char result[100];

  FILE *fPtr[11];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command takes a number of particle.dat files and a list of cavity offsets and returns the emittance."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=3){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," file_name cav_offsets\n",NULL);
    return TCL_ERROR;
  }

  name = argv[1];
  
  if (error=Tcl_SplitList(interp,argv[2],&nrofcavs,&cavoffset)) return error;

  while ((name[n] != '.') && (name[n] != '\0')) {
    filename[n] = name[n];
    n++;
  }

  namelength = n;
  
  filename[namelength] = ' ';
  
  while (name[n] != '\0') {
    filename[n+1] = name[n];
    n++;
  }
  
  filename[n+1] = '\0';
  
  for (i = 0; i <= nrofcavs; i++) {
    snprintf(filenr, 10, "%u", i);
    filename[namelength] = filenr[0];
    fPtr[i] = read_file(filename);
  }
    
  int nrofmparts = 0;

  while (!feof(fPtr[0])) {
    skipword(fPtr[0]);
    fscanf(fPtr[0], "%lg%lg", &w, &e);
    if (feof(fPtr[0]))
      break;
    skipword(fPtr[0]);
    skipword(fPtr[0]);
    fscanf(fPtr[0], "%lg%lg", &yv, &ypv);
    skipword(fPtr[0]);
    skipword(fPtr[0]);
    skipword(fPtr[0]);
    fscanf(fPtr[0], "%lg%lg%lg", &r11, &r12, &r22);
    skipcurrentline(fPtr[0]);
    for (i = 1; i <= nrofcavs; i++) {
      skipword(fPtr[i]);
      skipword(fPtr[i]);
      skipword(fPtr[i]);
      skipword(fPtr[i]);
      skipword(fPtr[i]);
      fscanf(fPtr[i], "%lg%lg", &dyv[i-1], &dypv[i-1]);
      skipcurrentline(fPtr[i]);
    }
    
    totalw = totalw + w;
    weight[nrofmparts] = w;
    meane = meane + w * e;
    meanr11 = meanr11 + w * r11;
    meanr12 = meanr12 + w * r12;
    meanr22 = meanr22 + w * r22;
    
    y[nrofmparts] = yv;
    yp[nrofmparts] = ypv;

    for (i = 0; i < nrofcavs; i++) {
      coffset = atof(cavoffset[i]);
      y[nrofmparts] = y[nrofmparts] + coffset * (dyv[i] - yv);
      yp[nrofmparts] = yp[nrofmparts] + coffset * (dypv[i] - ypv);
    }
    
    meany = meany + w * y[nrofmparts];
    meanyp = meanyp + w * yp[nrofmparts];
  
    nrofmparts++;
  }
  
  for (i = 0; i <= nrofcavs; i++) {
    close_file(fPtr[i]);
  }
  
  meany = meany / totalw;
  meanyp = meanyp / totalw;
  meane = meane / totalw;
  meanr11 = meanr11 / totalw;
  meanr12 = meanr12 / totalw;
  meanr22 = meanr22 / totalw;
  
  for (i = 0; i < nrofmparts; i++) {
    yy = yy + weight[i] * pow((y[i] - meany), 2);
    yyp = yyp + weight[i] * (y[i] - meany) * (yp[i] - meanyp);
    ypyp = ypyp + weight[i] * pow((yp[i] - meanyp), 2);
  }

  yy = yy / totalw + meanr11;
  yyp = yyp /totalw + meanr12;
  ypyp = ypyp / totalw + meanr22;

  nemitt = sqrt(yy * ypyp - yyp * yyp) * meane / 0.511;
  snprintf(result, 100, "%.12g", nemitt);

  Tcl_AppendResult(interp, result, NULL);
  return TCL_OK;
}

/**********************************************************************/
/*                            CalcSquareFit                           */
/**********************************************************************/

int tk_CalcSquareFit(ClientData /*clientdata*/,Tcl_Interp *interp,
				 int argc,char *argv[])
{
	int error, nx, ny, na = 0;
	char no[100], **vx, **vy, **va;

	char *params = NULL;

	Tk_ArgvInfo table[]={
		{(char*)"-initial",TK_ARGV_STRING,(char*)NULL,
		(char*)&params,
		(char*)"Initial parameters"},
		{(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
		(char*)"This command accepts a list of x values and a list of corresponding y values. Then the best square fit is calculated."},
		{(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
	};

	if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
		return error;
	}

	if (argc!=3){
		Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
			     "Usage: ",argv[0]," value_list value_list\n",NULL);
		return TCL_ERROR;
	}
	if (error=Tcl_SplitList(interp,argv[1],&nx,&vx)) return error;
	if (error=Tcl_SplitList(interp,argv[2],&ny,&vy)) return error;
	if (params) {
		if (error=Tcl_SplitList(interp,params,&na,&va)) return error;
	}
	
	if (nx!=ny) {
		Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
			     "Number of x and y values have to be equal",
			     NULL);
		return TCL_ERROR;
	}
	
	if (na != 0 && na!=3) {
		Tcl_AppendResult(interp,"Error in ",argv[0],"\n",
  			     "Number of initial parameter values is incorrect.",
  			     NULL);

		return TCL_ERROR;
	}

	gsl_matrix *X = gsl_matrix_alloc(nx, 3);
	gsl_vector *y = gsl_vector_alloc(nx);
	gsl_vector *w = gsl_vector_alloc(nx);

	gsl_vector *c = gsl_vector_alloc(3);
	gsl_matrix *cov = gsl_matrix_alloc(3, 3);

	for (int i = 0; i < nx; i++) {
		double xi, yi, ei;
		
		if (error = Tcl_GetDouble(interp, vx[i], &xi)) return error;
		if (error = Tcl_GetDouble(interp, vy[i], &yi)) return error;
		ei = 1.0;

		gsl_matrix_set(X, i, 0, 1.0);
		gsl_matrix_set(X, i, 1, xi);
		gsl_matrix_set(X, i, 2, xi*xi);
      
		gsl_vector_set(y, i, yi);
      		gsl_vector_set(w, i, 1.0 / (ei*ei));
    	}

	double chisq;
	
	if (gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc(nx, 3))
	{
		gsl_multifit_wlinear(X, w, y, c, cov, &chisq, work);
		gsl_multifit_linear_free(work);

	} else return TCL_ERROR;

	snprintf(no,100,"%g %g %g %g",	gsl_vector_get(c, 0),
					gsl_vector_get(c, 1),
					gsl_vector_get(c, 2), 
					chisq);
  	
	Tcl_AppendResult(interp, no, NULL);

	gsl_matrix_free(X);
	gsl_vector_free(y);
	gsl_vector_free(w);
	
	gsl_vector_free(c);
	gsl_matrix_free(cov);

	return TCL_OK;
}

/**********************************************************************/
/*                         CavityNumberList                           */
/**********************************************************************/

int tk_CavityNumberList(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int error,i;
  ELEMENT **element;
  char buffer[100];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  element=inter_data.beamline->element;
  for (i=0;i<inter_data.beamline->n_elements;i++){
    if (element[i]->is_cavity()) {
      snprintf(buffer,100,"%d",i);
      Tcl_AppendElement(interp,buffer);
    }
  }
  return TCL_OK;
}

/**********************************************************************/
/*                          ElementGetOffset                          */
/**********************************************************************/

int tk_ElementGetOffset(ClientData /*clientdata*/,Tcl_Interp *interp,
			     int argc,char *argv[])
{
  int error,j=-1;
  
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command returns the offset of the element number specified."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," number\n",NULL);
    return TCL_ERROR;
  }
  if (error=Tcl_GetInt(interp,argv[1],&j)) return error;
  if (!inter_data.beamline->element_in_range(j)){return TCL_ERROR;}
  std::ostringstream str;
  str.precision(DBL_DIG);
#ifdef TWODIM
  str << inter_data.beamline->element[j]->offset.x << ' '<< inter_data.beamline->element[j]->offset.y;
#else
  str << inter_data.beamline->element[j]->offset.y;
#endif
  Tcl_SetResult(interp,const_cast<char*>(str.str().c_str()),TCL_VOLATILE);

  return TCL_OK;  
}

/**********************************************************************/
/*                                Svd                                 */
/**********************************************************************/

int tk_Svd(ClientData /*clientdata*/, Tcl_Interp *interp, int argc,
	   char *argv[])
{
  int error, nrows, ncols;
  char **rows, **elements;
  double value;
  char strvalue[20];

  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This takes a matrix and returns its SVD component matrices."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=2){
    Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		     "Usage: ",argv[0]," matrix\n",NULL);
    return TCL_ERROR;
  }

  if (error=Tcl_SplitList(interp,argv[1],&nrows,&rows)) return error;
  if (nrows<=0) return TCL_ERROR;

  gsl_matrix *a;

  for (int i = 0; i < nrows; i++)
    {
      if (error=Tcl_SplitList(interp,rows[i],&ncols,&elements)) return error;
      if (i==0) a = gsl_matrix_alloc(nrows, ncols);

      for (int j = 0; j < ncols; j++)
	{
	  if (error = Tcl_GetDouble(interp, elements[j], &value)) return error;
	  gsl_matrix_set(a, i, j, value);
	}
    }

  gsl_matrix *v = gsl_matrix_alloc(ncols, ncols);
  gsl_vector *w = gsl_vector_alloc(ncols);
  gsl_vector *t = gsl_vector_alloc(ncols);

  gsl_linalg_SV_decomp(a, v, w, t);

  Tcl_AppendResult(interp,"{",NULL);
  for (int i = 0; i < nrows; i++)
    {
      if (i == 0)
	Tcl_AppendResult(interp,"{",NULL);
      else
	Tcl_AppendResult(interp," {",NULL);

      for (int j = 0; j < ncols; j++)
	{
	  if (j == 0)
	    snprintf(strvalue,20,"%g",gsl_matrix_get(a, i, j));
	  else
	    snprintf(strvalue,20," %g",gsl_matrix_get(a, i, j));
	
	  Tcl_AppendResult(interp,strvalue,NULL);
	}
      Tcl_AppendResult(interp,"}",NULL);
    }
  Tcl_AppendResult(interp,"}",NULL);
  Tcl_AppendResult(interp," {",NULL);
  for (int i = 0 ; i < ncols; i++)
    {
      if (i == 0)
	snprintf(strvalue,20,"%g",gsl_vector_get(w, i));
      else
	snprintf(strvalue,20," %g",gsl_vector_get(w, i));
	
      Tcl_AppendResult(interp,strvalue,NULL);
    }
  Tcl_AppendResult(interp,"}",NULL);
  Tcl_AppendResult(interp," {",NULL);
  for (int i = 0; i < ncols; i++)
    {
      if (i == 0) 
	Tcl_AppendResult(interp,"{",NULL);
      else
	Tcl_AppendResult(interp," {",NULL);

      for (int j = 0; j < ncols; j++)
	{
	  if (j == 0)
	    snprintf(strvalue,20,"%g",gsl_matrix_get(v, i, j));
	  else
	    snprintf(strvalue,20," %g",gsl_matrix_get(v, i, j));
			
	  Tcl_AppendResult(interp,strvalue,NULL);
	}
      Tcl_AppendResult(interp,"}",NULL);
    }
  Tcl_AppendResult(interp,"}",NULL);

  gsl_matrix_free(a);
  gsl_matrix_free(v);
  gsl_vector_free(w);
  gsl_vector_free(t);

  Tcl_Free((char*)rows);
  Tcl_Free((char*)elements);

  return TCL_OK;
}

/**********************************************************************/
/*                         BpmSetResolution                           */
/**********************************************************************/

int tk_BpmSetResolution(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int error,i,nbpm,bpm_no=-1;
  double res=0.0;
  
  Tk_ArgvInfo table[]={
    {(char*)"-resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&res,
     (char*)"Bpm resolution"},
    {(char*)"-bpm_no",TK_ARGV_INT,(char*)NULL,
     (char*)&bpm_no,
     (char*)"Bpm number (if not set, apply new resolution to all bpms)"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"Sets the BPM resolution to the specified value in all BPMs."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  
  nbpm=inter_data.beamline->n_bpm;
  
  if (bpm_no==-1) { // set resolution for all bpms
    for (i=0;i<nbpm;i++) { 
      inter_data.beamline->bpm[i]->set_resolution(res);
    }
  }
  else if (bpm_no >=0 && bpm_no < nbpm) {
    inter_data.beamline->bpm[bpm_no]->set_resolution(res);
  }
  else {placet_cout << WARNING << "Bpm number " << bpm_no << " does not exist, resolution not set!" << endmsg;}

  return TCL_OK;
}

/**********************************************************************/
/*                        KnobReferenceSet                            */
/**********************************************************************/

int tk_KnobReferenceSet(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int error;
  char *beamname=NULL;
  BEAM *beam;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used as reference"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  
  if (beamname) {
    beam=get_beam(beamname);
  } else {
    beam=inter_data.bunch;
  }

  knob_ref=bunch_remake(beam);
  beam_copy(beam, knob_ref);

  return TCL_OK;
}

/**********************************************************************/
/*                        KnobReferenceZero                           */
/**********************************************************************/

int tk_KnobReferenceZero(ClientData /*clientdata*/,Tcl_Interp * /*interp*/,int /*argc*/, char * /*argv*/[])
{
  knob_ref=NULL;
  return TCL_OK;
}

/**********************************************************************/
/*                           KnobSaveAll                              */
/**********************************************************************/

int tk_KnobSave(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int error;
  char *filename=NULL, *beamname=NULL;
  BEAM *beam;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&filename,
     (char*)"File in which knob vector is to be saved."},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be saved"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (!filename) {
    Tcl_AppendResult(interp,"A filename has to be specified.",NULL);
    return TCL_ERROR;
  }

  if (beamname) {
    beam=get_beam(beamname);
  } else {
    beam=inter_data.bunch;
  }

  FILE *file=open_file(filename);

  int i,j,k,n,nb,nm,m=0;
  double buffer[4];
  n=beam->slices/beam->bunches;
  n/=beam->macroparticles;
  nm=beam->macroparticles;
  nb=beam->bunches;

  fprintf(file,"%d %d %d\n",nb,n,nm);

  if (knob_ref) {
    for (j=0;j<nb;j++){
      for (i=0;i<n;i++){
	for (k=0;k<nm;k++){
	  buffer[0]=beam->particle[m].x-knob_ref->particle[m].x;
	  buffer[1]=beam->particle[m].xp-knob_ref->particle[m].xp;
	  buffer[2]=beam->particle[m].y-knob_ref->particle[m].y;
	  buffer[3]=beam->particle[m].yp-knob_ref->particle[m].yp;
	  fwrite(buffer,sizeof(double),4,file);
	  m++;
	}
      }
      fprintf(file,"\n");
    }
  } else {
    for (j=0;j<nb;j++){
      for (i=0;i<n;i++){
	for (k=0;k<nm;k++){
	  buffer[0]=beam->particle[m].x;
	  buffer[1]=beam->particle[m].xp;
	  buffer[2]=beam->particle[m].y;
	  buffer[3]=beam->particle[m].yp;
	  fwrite(buffer,sizeof(double),4,file);
	  m++;
	}
      }
      fprintf(file,"\n");
    }
  }
  close_file(file);

  return TCL_OK;
}

/**********************************************************************/
/*                           BumpCorrection                           */
/**********************************************************************/

int tk_BumpCorrection(ClientData clientdata,Tcl_Interp *interp,int argc,
			    char *argv[])
{
  int error, i, j, k;
  char *initfile=NULL;
  char *dyneffects=NULL;
  char *name=NULL;
  int niter=0, real=1, nfitpoints=5;
  double resfactor=0.1;
  double nmminsize=1e-3;
  double knobstepsize=0.0;
  double moverstepsize=0.0;
  double signalnoise=0.0;
  char *knobmatrix=NULL;
  double beta_x=0.0, beta_y=0.0, sigma_x=0.0, sigma_y=0.0;
  int nphotons=0;
  double penaltylimit=0;
  char *beamname="beam0";
  
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,     
     (char*)"Name of the bumps to be used."},
    {(char*)"-initfile",TK_ARGV_STRING,(char*)NULL,
     (char*)&initfile,     
     (char*)"Name of the file containing the beam to be corrected."},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,     
     (char*)"Name of the beam to be used if no initfile is given."},
    {(char*)"-realistic",TK_ARGV_INT,(char*)NULL,
     (char*)&real,     
     (char*)"If 0 Nelder-Mead simplex will be used, otherwise a realistic optimisation will be carried out."},
    {(char*)"-iterations",TK_ARGV_INT,(char*)NULL,
     (char*)&niter,     
     (char*)"Number of iterations of the realistic optimisation."},
    {(char*)"-nmminsize",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&nmminsize,     
     (char*)"Nelder-Mead optimisation stops when simplex size drops to this level."},
    {(char*)"-resfactor",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&resfactor,     
     (char*)"A factor that adjusts the signal resolution by changing the scanning range. 0.1-0.3 reasonable for a noise free signal, otherwise a higher value might be needed."},
    {(char*)"-nfitpoints",TK_ARGV_INT,(char*)NULL,
     (char*)&nfitpoints,     
     (char*)"Number of fit points for the parabolic fits."},
    {(char*)"-dyneff",TK_ARGV_INT,(char*)NULL,
     (char*)&dyneffects,     
     (char*)"If not 0, dynamic effects will be taken into account."},
    {(char*)"-knobstepsize",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&knobstepsize,     
     (char*)"Minimum amount with which a knob can be adjusted."},
    {(char*)"-moverstepsize",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&moverstepsize,     
     (char*)"Minimum amount with which a quad or mover can be displaced."},
    {(char*)"-knobmatrix",TK_ARGV_STRING,(char*)NULL,   
     (char*)&knobmatrix,     
     (char*)"Matrix that causes linear combinations of the knob vectors to be used."},
    {(char*)"-signalnoise",TK_ARGV_FLOAT,(char*)NULL,   
     (char*)&signalnoise,     
     (char*)"Noise in the emittance measurement [%]."},
    {(char*)"-nphotons",TK_ARGV_INT,(char*)NULL,   
     (char*)&nphotons,     
     (char*)"Number of photons for laser wire measurements. Default is 0, i.e. laserwire is not used."},
    {(char*)"-beta_x_at_lw",TK_ARGV_FLOAT,(char*)NULL,   
     (char*)&beta_x,     
     (char*)"Horizontal beta at the first laser wire [m]."},
    {(char*)"-beta_y_at_lw",TK_ARGV_FLOAT,(char*)NULL,   
     (char*)&beta_y,     
     (char*)"Vertical beta at the first laser wire [m]."},
    {(char*)"-sigma_x_lw",TK_ARGV_FLOAT,(char*)NULL,   
     (char*)&sigma_x,     
     (char*)"Horizontal RMS size of the laser wire [um]."},
    {(char*)"-sigma_y_lw",TK_ARGV_FLOAT,(char*)NULL,   
     (char*)&sigma_y,     
     (char*)"Vertical RMS size of the laser wire [um]."},
    {(char*)"-penalty_limit",TK_ARGV_FLOAT,(char*)NULL,   
     (char*)&penaltylimit,     
     (char*)"Knob setting of which penalty function is activated."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  beamline_survey_hook_interp=interp;

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc>1){
    Tcl_SetResult(interp,"Too many arguments to <BumpCorrection>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  BUMP_INFO_STRUCT *bump_info;  
  if (clientdata) {
    bump_info=(BUMP_INFO_STRUCT *)clientdata;
  } else {
    if (bump_information) {
      bump_info=bump_information;
    } else {
      Tcl_SetResult(interp,"Error in <BumpCorrection>: No default bumps setup.", TCL_STATIC);
      return TCL_ERROR;
    }
  }
  
  bump_info->penaltylimit=penaltylimit;
  bump_info->moverstepsize=moverstepsize;
  bump_info->nphotons=nphotons;
  if (nphotons) {
    bump_info->beta_x=beta_x;
    bump_info->beta_y=beta_y;
    bump_info->sigma_x=sigma_x;
    bump_info->sigma_y=sigma_y;
  }

  int nknobs, nlines;
  nknobs=bump_info->nknobs;
  nlines=bump_info->nb*bump_info->ns*bump_info->nm;

  double *range;
  range=(double *)malloc(bump_info->nknobs*sizeof(double));
  if (bump_info->range) {
    for (i=0;i<nknobs;i++) {
      range[i]=bump_info->range[i];
    }
  } else {
    for (i=0;i<nknobs;i++) {
      range[i]=500.0;
    }
  }

  int nb,ns,nm,nlines_tmp;

  if(initfile) {
    FILE *f=read_file(initfile,true);
    fscanf(f,"%d %d %d\n",&nb,&ns,&nm);

    if (nb != bump_info->nb || ns != bump_info->ns || nm != bump_info->nm) {
      placet_cout << ERROR << "in tk_BumpCorrection: Dimensions of initfile does not match the knob vectors." << endmsg;
      free(range);
      close_file(f);
      return 1;
    }

    bump_info->initcoords=(double *)malloc(17*nlines*sizeof(double));

    for (j=0;j<nlines;j++) {
      for (k=0;k<17;k++) {
	fread(&(bump_info->initcoords[17*j+k]),sizeof(double),1,f);
      }
    }

    close_file(f);

  } else {
    BEAM* beam=get_beam(beamname); // range not freed in case of exit in get_beam

    BEAM* testbeam=bunch_remake(beam);
    
    nlines_tmp=beam->slices;
    nb=beam->bunches;
    nm=beam->macroparticles;
    ns=nlines_tmp/(nb*nm);

    if (nb != bump_info->nb || ns != bump_info->ns || nm != bump_info->nm) {
      placet_cout << ERROR << "in tk_BumpCorrection: Dimensions of beam does not match the knob vectors." << endmsg;
      free(range); beam_delete(testbeam);
      return 1;
    }

    bump_info->initcoords=(double *)malloc(17*nlines_tmp*sizeof(double));

    beam_copy(beam, testbeam);
    bunch_track(inter_data.beamline,testbeam,0,inter_data.beamline->n_elements);

    int ctr=0;

    for (i=0;i<nb;i++){
      for (j=0;j<ns;j++){
	for (k=0;k<nm;k++){
	  bump_info->initcoords[17*ctr]=testbeam->z_position[i*ns+j];
	  bump_info->initcoords[17*ctr+1]=testbeam->particle[ctr].wgt;
	  bump_info->initcoords[17*ctr+2]=testbeam->particle[ctr].energy;
	  bump_info->initcoords[17*ctr+3]=testbeam->particle[ctr].x;
	  bump_info->initcoords[17*ctr+4]=testbeam->particle[ctr].xp;
	  bump_info->initcoords[17*ctr+5]=testbeam->particle[ctr].y;
	  bump_info->initcoords[17*ctr+6]=testbeam->particle[ctr].yp;
	  bump_info->initcoords[17*ctr+7]=testbeam->sigma_xx[ctr].r11;
	  bump_info->initcoords[17*ctr+8]=testbeam->sigma_xx[ctr].r12;
	  bump_info->initcoords[17*ctr+9]=testbeam->sigma_xx[ctr].r22;
	  bump_info->initcoords[17*ctr+10]=testbeam->sigma[ctr].r11;
	  bump_info->initcoords[17*ctr+11]=testbeam->sigma[ctr].r12;
	  bump_info->initcoords[17*ctr+12]=testbeam->sigma[ctr].r22;
	  bump_info->initcoords[17*ctr+13]=testbeam->sigma_xy[ctr].r11;
	  bump_info->initcoords[17*ctr+14]=testbeam->sigma_xy[ctr].r12;
	  bump_info->initcoords[17*ctr+15]=testbeam->sigma_xy[ctr].r21;
	  bump_info->initcoords[17*ctr+16]=testbeam->sigma_xy[ctr].r22;

	  ctr++;
	}
      }
    }
    beam_delete(testbeam);
  }


  gsl_vector *knobvalues;

  double emitt;
  //emitt=calcemitt(knobvalues, bump_info);
  //placet_printf(INFO,"Initial emittance: %g\n", emitt);

  if(real) {
    bump_iter_ctr=0;
    if (!niter) {
      niter=10;
    }
    gsl_matrix *X = gsl_matrix_alloc(nfitpoints, 3);
    gsl_vector *y = gsl_vector_alloc(nfitpoints);
    gsl_vector *w = gsl_vector_alloc(nfitpoints);
  
    gsl_vector *c = gsl_vector_alloc(3);
    gsl_matrix *cov = gsl_matrix_alloc(3, 3);
    
    if (niter<-1) {
      niter=-1;
    }

    if (knobmatrix) {
      int nrows, ncols1, ncols;
      char **rows;
      char **elements;
      
      if (error=Tcl_SplitList(interp,knobmatrix,&nrows,&rows)) {free(range); return error;}
      if (nrows!=nknobs) {
	placet_printf(ERROR,"Error in tk_BumpCorrection: Dimension of knob matrix does not match number of knob files.\n");
	free(range);
	return 1;
      }
      
      for (i = 0; i < nrows; i++) {
	if (error=Tcl_SplitList(interp,rows[i],&ncols1,&elements)) {free(range); return error;}
	if (i==0) {
	  ncols=ncols1;
	  bump_info->knobmatrix=(double *)malloc(ncols*nrows*sizeof(double));
	}
	if (ncols1!=ncols) {
	  placet_printf(ERROR,"Error in tk_BumpCorrection: Rows have different number of columns.\n");
	  free(range);
	  return 1;
	}
	for (j = 0; j < ncols; j++) {
	  if (error = Tcl_GetDouble(interp, elements[j], &bump_info->knobmatrix[i*ncols+j])) {free(range); return error;}
      	}
	nknobs=ncols;
	bump_info->nknobs_mod=nknobs;
	bump_info->ncols=ncols;
	bump_info->nrows=nrows;
      }
    } else {
      bump_info->knobmatrix=NULL;
    }

    knobvalues = gsl_vector_alloc(nknobs);
    gsl_vector_set_all(knobvalues, 0.0);

    int niter2=abs(niter*nknobs);
    double emittances[niter2];

    for(k=0;k<niter;k++) {
      //placet_printf(INFO,"ITERATION %d:\n\n",k+1);
      for(i=0;i<nknobs;i++) {
	//placet_printf(INFO,"KNOB %d:\n",i+1);
	double step;
	step = 2*range[i]/(nfitpoints-1);
	for (j = 0; j < nfitpoints; j++) {
	  double xj, yj, ej;
	  if (j==0) {
	    gsl_vector_set(knobvalues,i,(gsl_vector_get(knobvalues,i)-discretestep(range[i],knobstepsize)));
	  } else {
	    gsl_vector_set(knobvalues,i,(gsl_vector_get(knobvalues,i)+discretestep(step,knobstepsize)));
	  }
      
	  xj = -range[i] + j * discretestep(step,knobstepsize);
	  if (nphotons) {
	    yj = - calcemitt(knobvalues, bump_info) * (1+Instrumentation.Gauss()*signalnoise/100.0);
	  } else {
	    knobpenalty=1;
	    yj = calcemitt(knobvalues, bump_info) * (1+Instrumentation.Gauss()*signalnoise/100.0);
	  }

	  ej = 1.0;
	  //placet_printf(INFO,"%10g %10g\n",xj,yj);

	  gsl_matrix_set(X, j, 0, 1.0);
	  gsl_matrix_set(X, j, 1, xj);
	  gsl_matrix_set(X, j, 2, xj*xj);
      
	  gsl_vector_set(y, j, yj);
	  gsl_vector_set(w, j, 1.0 / (ej*ej));
	}
	gsl_vector_set(knobvalues,i,(gsl_vector_get(knobvalues,i)-discretestep(range[i],knobstepsize)));
	
	double chisq;
	
	if (gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc(nfitpoints, 3)) {
	  gsl_multifit_wlinear(X, w, y, c, cov, &chisq, work);
	  gsl_multifit_linear_free(work);
	} else {free(range); return TCL_ERROR;}
	
	double opt;
	
	if (gsl_vector_get(c,2)<=0) {
	  double min=gsl_vector_get(y,0);
	  int minindex=0;
	  for (int ii=1;ii<nfitpoints;ii++) {
	    if (gsl_vector_get(y,ii)<min) {
	      min=gsl_vector_get(y,ii);
	      minindex=ii;
	    }	    
	  }
	  opt=gsl_matrix_get(X,minindex,1);
	} else {
	  opt=-0.5*gsl_vector_get(c,1)/gsl_vector_get(c,2);
	  double c1=gsl_vector_get(c,0)+gsl_vector_get(c,1)*opt/2;
	  double r=sqrt(resfactor*fabs(c1)/gsl_vector_get(c,2));
	  if (bump_info->range) {
	    if (opt>bump_info->range[i]) {
	      opt = bump_info->range[i];
	    } else if (opt<-bump_info->range[i]) {
	      opt = -bump_info->range[i];
	    }
	    if (r>bump_info->range[i]) {
	      range[i]=bump_info->range[i];
	    } else {
	      range[i]=r;
	    }
	  } else {
	    if (opt>500) {
	      opt = 500;
	    } else if (opt<-500) {
	      opt = -500;
	    }
	    if (r>500) {
	      range[i]=500;
	    } else {
	      range[i]=r;
	    }
	  }
	}

	gsl_vector_set(knobvalues,i,(gsl_vector_get(knobvalues,i)+discretestep(opt,knobstepsize)));
	if (nphotons) {
	  emittances[k*nknobs+i]=calcemitt(knobvalues, bump_info);
	} else {
	  knobpenalty=0;
	  emittances[k*nknobs+i]=calcemitt(knobvalues, bump_info);
	}
	//if (k>3 && (emittances[k*nknobs+i-3*nknobs]-emittances[k*nknobs+i])/emittances[k*nknobs+i]<0.01) {
	//niter2=k*nknobs+i+1;
	//break;
	//}
      }
    }
    
    gsl_matrix_free(X);
    gsl_vector_free(y);
    gsl_vector_free(w);
    
    gsl_vector_free(c);
    gsl_matrix_free(cov);
    
    //    for(i=0;i<nknobs;i++) {
    // placet_printf(INFO,"%g ",gsl_vector_get(knobvalues,i));
    //}
	if (nphotons) {
	  emitt=calcemitt(knobvalues, bump_info);
	} else {
	  knobpenalty=1;
	  emitt=calcemitt(knobvalues, bump_info);
	}
    //placet_printf(INFO,"\nOptimum: %g\n", calcemitt(knobvalues, bump_info));
    
    Tcl_ResetResult(interp);

    char tmp[100];
    const int result_size = 12*niter2;
    char result[result_size];

    snprintf(tmp, 100, "%d %.3f", niter2, emitt);
    //placet_printf(INFO,"%d %.3f", iter, emitt);
    Tcl_AppendResult(interp, tmp, NULL);

    result[0]='\0';
    int result_length = 0; // counter to avoid overflow
    for(i=0;i<nknobs;i++) {
      int tmp_length = snprintf(tmp, 100, "%.3e ", gsl_vector_get(knobvalues, i));
      // assuming limit is not hit, tmp_length is length of tmp
      result_length += tmp_length;
      if (result_length < result_size) {strncat(result, tmp, tmp_length);}
      else {
	placet_cout << WARNING << "BumpCorrection: buffer too small, increase buffer size" << endmsg;
	break;
      }
    }

    Tcl_AppendElement(interp, result);
    result[0]='\0';
    result_length = 0;
    for(i=0;i<niter2;i++) {
      int tmp_length = snprintf(tmp, 100, "%g ",emittances[i]);
      // assuming limit is not hit, tmp_length is length of tmp
      result_length += tmp_length;
      if (result_length < result_size) {strncat(result, tmp, tmp_length);}
      else {
	placet_cout << WARNING << "BumpCorrection: buffer too small, increase buffer size" << endmsg;
	break;
      }
    }
    Tcl_AppendElement(interp, result);
    
    placet_printf(INFO,"Pulses: %d\n",bump_iter_ctr);
    gsl_vector_free(knobvalues);
    free(range);
   
    if (bump_info->knobmatrix) {
      free(bump_info->knobmatrix);
    }

    return TCL_OK;
  
  } else {

    bump_iter_ctr=0;

    if (!niter) {
      niter=500;
    }

    knobvalues = gsl_vector_alloc(nknobs);
    gsl_vector_set_all(knobvalues, 0.0);
    
    size_t np = bump_info->nknobs;

    const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex;
    gsl_multimin_fminimizer *s = NULL;
    gsl_vector *ss, *x;
    gsl_multimin_function emitt_func;

    int iter = 0;
    int status;
    double size;

    // Initial vertex size vector
    ss = gsl_vector_alloc (np);
    gsl_vector_set_all (ss, 1.0);
  
    // Starting point
    x = gsl_vector_alloc (np);
    gsl_vector_set_all (x, 0.0);

    // Initialize method and iterate
    emitt_func.f = &calcemitt;
    emitt_func.n = np;
    emitt_func.params = (void *)bump_info;
  
    s = gsl_multimin_fminimizer_alloc (T, np);
    gsl_multimin_fminimizer_set (s, &emitt_func, x, ss);
  
    do
      {
	iter++;
	status = gsl_multimin_fminimizer_iterate(s);
      
	if (status) {
	  Tcl_AppendResult(interp, "Error in gsl_multimin_fminimizer_iterate!\n", NULL);
	  free(range);
	  return TCL_ERROR;
	}
      
	size = gsl_multimin_fminimizer_size (s);
	status = gsl_multimin_test_size (size, nmminsize);
      }
    while (status == GSL_CONTINUE && iter < niter);
  

    Tcl_ResetResult(interp);

    char result[100];
    snprintf(result, 100, "%lu %.3f", (unsigned long) iter, s->fval);
    //    placet_printf(INFO,"%d %.3f", iter, s->fval);
    Tcl_AppendResult(interp, result, NULL);

    for(i=0;(unsigned int)i<np;i++) {
      snprintf(result, 100, " %.3e", gsl_vector_get (s->x, i));
      //placet_printf(INFO," %.3e", gsl_vector_get (s->x, i));
      Tcl_AppendResult(interp, result, NULL);
    }

    gsl_vector_free(x);
    gsl_vector_free(ss);
    gsl_multimin_fminimizer_free (s);
     
    free(range);
    
    placet_printf(INFO,"Pulses: %d\n",bump_iter_ctr);

    return TCL_OK;
  }
}

int tk_BumpSetup(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  char *knobfiles=NULL, *knobscripts=NULL;
  char *dyneffects=NULL;
  char *ranges=NULL;
  char *name=NULL;
  char *knobmatrix=NULL;
  int save=1;
  int knobsall=0;
  char *beamname="beam0";

  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,
     (char*)&name,
     (char*)"Name of the new command created."},
    {(char*)"-save",TK_ARGV_INT,(char*)NULL,
     (char*)&save,     
     (char*)"If non-zero the new knob/dyneffects response data will be saved."},
    {(char*)"-knobfiles",TK_ARGV_STRING,(char*)NULL,
     (char*)&knobfiles,     
     (char*)"List of files containing beam knob response."},
    {(char*)"-longknobfile",TK_ARGV_INT,(char*)NULL,
     (char*)&knobsall,     
     (char*)"If non-zero the flag indicates that knob files are saved by \"BeamSaveAll -binary 1 -header 1\"."},
    {(char*)"-knobscripts",TK_ARGV_STRING,(char*)NULL,
     (char*)&knobscripts,     
     (char*)"List of Tcl knob scripts."},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&beamname,
     (char*)"Name of the beam to be used for the knob scripts and dynamic effects."},
    {(char*)"-knobmatrix",TK_ARGV_STRING,(char*)NULL,
     (char*)&knobmatrix,     
     (char*)"Matrix that causes linear combinations of the knob vectors to be used."},
    {(char*)"-ranges",TK_ARGV_STRING,(char*)NULL,
     (char*)&ranges,     
     (char*)"List of ranges to be used for the knobs."},
    {(char*)"-dyneff",TK_ARGV_STRING,(char*)NULL,
     (char*)&dyneffects,     
    (char*)"Specifies the name of the set of dynamic effects to be used. If \"1\" the current dynamic effects will be used. Default is 0, meaning that dynamic effects are not taken into account."},
    //{(char*)"-quadrespfile",TK_ARGV_STRING,(char*)NULL,
    // (char*)&qrespfile,     
    // (char*)"File containing the quadrupole offset response matrix."},
    //{(char*)"-cavrespfile",TK_ARGV_STRING,(char*)NULL,
    // (char*)&crespfile,     
    // (char*)"File containing the cavity offset response matrix."},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  beamline_survey_hook_interp=interp;

  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if(!knobfiles && !knobscripts) {
    placet_printf(ERROR,"Error in tk_BumpSetup: Knobfiles or knobscripts have to be specified!\n");
    return 1;
  }
  if (knobfiles && knobscripts) {
    placet_printf(INFO,"Since knobfiles have been specified, the knobscripts will be ignored.\n");
    knobscripts=NULL;
  }
   
  BUMP_INFO_STRUCT *bump_info;
  bump_info=(BUMP_INFO_STRUCT*)malloc(sizeof(BUMP_INFO_STRUCT));

  int i, j, k;
  int nb=0, ns=0, nm=0, nlines=0;
  int nb_tmp, ns_tmp, nm_tmp;
  FILE *f;
  BEAM *beam, *testbeam;

  if (knobscripts || dyneffects) {
    beam=get_beam(beamname);
  }

  int nknobs=0;
  char **scriptnames;

  if(knobfiles) {
    char **filenames;

    if(error=Tcl_SplitList(interp,knobfiles,&nknobs,&filenames)) {
      placet_printf(ERROR,"error\n");
      return error;
    }

    for (i=0;i<nknobs;i++) {
      f=read_file(filenames[i]);
      if (i==0) {
	fscanf(f,"%d %d %d\n",&nb,&ns,&nm);
	nlines=nb*ns*nm;
	bump_info->knobcoords=(double *)malloc(4*nlines*nknobs*sizeof(double));
      } else {
	fscanf(f,"%d %d %d\n",&nb_tmp,&ns_tmp,&nm_tmp);
	if (nb_tmp != nb || ns_tmp != ns || nm_tmp != nm) {
	  placet_printf(ERROR,"Error in tk_BumpSetup: Dimensions of files do not match\n");
	  close_file(f);
	  return 1;
	}
      }
      if (knobsall) {
	double *dummy = (double *)malloc(10*sizeof(double)); // for reading in non-used values
	for (j=0;j<nlines;j++) {
	  fread(dummy, sizeof(double), 3, f);
	  for (k=0;k<4;k++) {
	    fread(&(bump_info->knobcoords[4*(nlines*i+j)+k]),sizeof(double),1,f);
	  }
	  fread(dummy, sizeof(double), 10, f);
	}
	free (dummy);
      } else {
	for (j=0;j<nlines;j++) {
	  for (k=0;k<4;k++) {
	    fread(&(bump_info->knobcoords[4*(nlines*i+j)+k]),sizeof(double),1,f);
	  }
	}
      }
      close_file(f);
    }
  } else {
    nlines=beam->slices;
    nb=beam->bunches;
    nm=beam->macroparticles;
    ns=nlines/(nb*nm);
  
    if(error=Tcl_SplitList(interp,knobscripts,&nknobs,&scriptnames)) {
      placet_printf(ERROR,"error\n");
      return error;
    }

    bump_info->knobcoords=(double *)malloc(4*nlines*nknobs*sizeof(double));

    testbeam=bunch_remake(beam);
    beam_copy(beam, testbeam);
    bunch_track(inter_data.beamline,testbeam,0,inter_data.beamline->n_elements);

    double *icoords=(double *)malloc(4*nlines*sizeof(double));
    int ctr=0;    
    
    for (i=0;i<nb;i++){
      for (j=0;j<ns;j++){
	for (k=0;k<nm;k++){
	  icoords[4*ctr+0]=testbeam->particle[ctr].x;
	  icoords[4*ctr+1]=testbeam->particle[ctr].xp;
	  icoords[4*ctr+2]=testbeam->particle[ctr].y;
	  icoords[4*ctr+3]=testbeam->particle[ctr].yp;
	  ctr++;
	}
      }
    }
    
    for (i=0;i<nknobs;i++) {
      turnknob(scriptnames[i], 1.0);
      beam_copy_0(beam, testbeam);
      bunch_track_0(inter_data.beamline,testbeam,0,inter_data.beamline->n_elements);
      
      for (ctr=0;ctr<nlines;ctr++) {
	bump_info->knobcoords[4*(nlines*i+ctr)]=testbeam->particle[ctr].x-icoords[4*ctr+0];
	bump_info->knobcoords[4*(nlines*i+ctr)+1]=testbeam->particle[ctr].xp-icoords[4*ctr+1];
	bump_info->knobcoords[4*(nlines*i+ctr)+2]=testbeam->particle[ctr].y-icoords[4*ctr+2];
	bump_info->knobcoords[4*(nlines*i+ctr)+3]=testbeam->particle[ctr].yp-icoords[4*ctr+3];
      }
      
      turnknob(scriptnames[i], -1.0);
    }
    
    free(icoords);
  }


  if (knobmatrix) {
    double *knobmat=NULL;
    int nrows, ncols_tmp, ncols;
    char **rows;
    char **elements;
    
    if (error=Tcl_SplitList(interp,knobmatrix,&nrows,&rows)) return error;
    if (nrows!=nknobs) {
      placet_printf(ERROR,"Error in tk_BumpSetup: Dimension of knob matrix does not match number of knob files.\n");
      return 1;
    }
    
    for (i = 0; i < nrows; i++) {
      if (error=Tcl_SplitList(interp,rows[i],&ncols_tmp,&elements)) return error;
      if (i==0) {
	ncols=ncols_tmp;
	knobmat=(double *)malloc(ncols*nrows*sizeof(double));
      }
      if (ncols_tmp!=ncols) {
	placet_printf(ERROR,"Error in tk_BumpSetup: Not a valid matrix.\n");
	free(knobmat);
	return 1;
      }
      for (j = 0; j < ncols; j++) {
	if (error = Tcl_GetDouble(interp, elements[j], &knobmat[i*ncols+j])) return error;
      }
    }
    
    double *tmpcoords=(double *)malloc(4*nlines*ncols*sizeof(double));
    for (i=0;i<ncols;i++) {
      for (j=0;j<nlines;j++) {
	for (k=0;k<4;k++) {
	  tmpcoords[4*(nlines*i+j)+k]=0;
	  for (int l=0;l<nknobs;l++) {
	    tmpcoords[4*(nlines*i+j)+k]+=knobmat[l*ncols+i]*bump_info->knobcoords[4*(nlines*l+j)+k];
	  }
	}
      }
    }
    
    free(bump_info->knobcoords);
    bump_info->knobcoords=tmpcoords;
    nknobs=ncols;
    free(knobmat);
  }
  
  
  if(save && (knobmatrix || knobscripts || knobsall)) {
    char filename[50];
    for (i=0;i<nknobs;i++) {
      if (knobmatrix || knobsall) {
	snprintf(filename,50,"knob_%d.bin",i);
      } else {
	snprintf(filename,50,"knob_%s.bin",scriptnames[i]);
      }
      f=open_file(filename);
      fprintf(f,"%d %d %d\n",nb,ns,nm);
      for (j=0;j<nlines;j++) {
	for (k=0;k<4;k++) {
	  fwrite(&(bump_info->knobcoords[4*(nlines*i+j)+k]),sizeof(double),1,f);
	}
      }
      close_file(f);
    }
  }
  
  int nknobs_tmp=0;
  
  char **ranges_tmp;
  if (ranges) {
    if(error=Tcl_SplitList(interp,ranges,&nknobs_tmp,&ranges_tmp)) {
      placet_printf(ERROR,"error\n");
      return error;
    }
    
    if (nknobs_tmp!=nknobs) {
      Tcl_SetResult(interp,"Number of ranges and number of effective (taking knob matrix into account) knobs do not match.",
		    TCL_STATIC);
      return TCL_ERROR;
    }
    
    bump_info->range=(double *)malloc(nknobs*sizeof(double));
  
    for (int ii=0;ii<nknobs;ii++) {
      Tcl_GetDouble(interp,ranges_tmp[ii],&bump_info->range[ii]);
    }
  } else {
    bump_info->range=NULL;
  }

  /*

  if (dyneffects == "1") {
    if (dyneffects) {
      DYNAMIC_EFFECTS_STRUCT *dyneff;
      dyneff=&dynamic_effects;
      
      BEAM *testbeam2;
      testbeam2=bunch_remake(beam);

      if (dyneff->cav_jitter_x || dyneff->cav_walk_x) {
      }
      if (dyneff->cav_jitter_y || dyneff->cav_walk_y) {
	bump_info->cav_y_matrix=(double ***)malloc(inter_data.beamline->n_cav*sizeof(double **));
	for (i=0;i<inter_data.beamline->n_cav;i++) {
	  bump_info->cav_y_matrix[i]=(double **)malloc(nlines*sizeof(double *));
	  for(j=0;j<nlines;j++) {
	    bump_info->cav_y_matrix[i][j]=(double *)malloc(4*sizeof(double));
	  }
	}      
      }
      if (dyneff->quad_jitter_x || dyneff->quad_walk_x) {
      }
      if (dyneff->quad_jitter_y || dyneff->quad_walk_y) {
	bump_info->quaoffset.y_matrix=(double ***)malloc(inter_data.beamline->n_quad*sizeof(double **));
	for (i=0;i<inter_data.beamline->n_quad;i++) {
	  bump_info->quaoffset.y_matrix[i]=(double **)malloc(nlines*sizeof(double *));
	  for(j=0;j<nlines;j++) {
	    bump_info->quaoffset.y_matrix[i][j]=(double *)malloc(4*sizeof(double));
	  }
	}      
      }
      beam_copy_0(beam, testbeam);
      beam_copy_0(beam, testbeam2);
      //bunch_track(inter_data.beamline,testbeam,0,inter_data.beamline->n_elements);
      //bunch_track(inter_data.beamline,testbeam2,0,inter_data.beamline->n_elements);
    
      ELEMENT **element;
      element=inter_data.beamline->element;

      int ctr=0;
      int cavnumber[inter_data.beamline->n_cav];
      for (i=0;i<inter_data.beamline->n_elements;i++){
	if (element[i]->type==CAV){
	  cavnumber[ctr]=i;
	  ctr++;
	}
      }

      ctr=0;
      int quadnumber[inter_data.beamline->n_quad];
      for (i=0;i<inter_data.beamline->n_elements;i++){
	if (element[i]->type==QUAD){
	  quadnumber[ctr]=i;
	  ctr++;
	}
      }

      int cav_ctr=0;
      int quad_ctr=0;
    
      int from_element=0;
      int to_element=0;


      //placet_printf(INFO,"%d\n",inter_data.beamline->n_quad);
      //return 0;

      while ((false && cav_ctr < inter_data.beamline->n_cav) || quad_ctr < inter_data.beamline->n_quad) {
	placet_printf(INFO,"Quad: %d\n", quad_ctr);
	if (false && cavnumber[cav_ctr]<quadnumber[quad_ctr]) {
	if (dyneff->cav_jitter_y || dyneff->cav_walk_y) {
	to_element=cavnumber[cav_ctr];
	bunch_track_0(inter_data.beamline,testbeam,from_element,to_element);
	from_element=to_element;
	beam_copy_0(testbeam, testbeam2);
	inter_data.beamline->element[to_element]->add_y(1.0);
	bunch_track_0(inter_data.beamline,testbeam,from_element,inter_data.beamline->n_elements);
	inter_data.beamline->element[to_element]->add_y(-1.0);
	for (int ctr=0;ctr<nlines;ctr++) {
	bump_info->cav_y_matrix[cav_ctr][ctr][0]=testbeam->particle[ctr].x;
	bump_info->cav_y_matrix[cav_ctr][ctr][1]=testbeam->particle[ctr].xp;
	bump_info->cav_y_matrix[cav_ctr][ctr][2]=testbeam->particle[ctr].y;
	bump_info->cav_y_matrix[cav_ctr][ctr][3]=testbeam->particle[ctr].yp;
	}
	beam_copy_0(testbeam2, testbeam);
	}
	cav_ctr++;
	} else {
	if (dyneff->quad_jitter_y || dyneff->quad_walk_y) {
	to_element=quadnumber[quad_ctr];
	bunch_track(inter_data.beamline,testbeam,from_element,to_element);
	from_element=to_element;
	beam_copy(testbeam, testbeam2);
	inter_data.beamline->element[to_element]->add_y(1.0);
	bunch_track(inter_data.beamline,testbeam,from_element,inter_data.beamline->n_elements);
	inter_data.beamline->element[to_element]->add_y(-1.0);
	for (int ctr=0;ctr<nlines;ctr++) {
	bump_info->quaoffset.y_matrix[quad_ctr][ctr][0]=testbeam->particle[ctr].x;
	bump_info->quaoffset.y_matrix[quad_ctr][ctr][1]=testbeam->particle[ctr].xp;
	bump_info->quaoffset.y_matrix[quad_ctr][ctr][2]=testbeam->particle[ctr].y;
	bump_info->quaoffset.y_matrix[quad_ctr][ctr][3]=testbeam->particle[ctr].yp;
	if (ctr==0) {
	placet_printf(INFO,"%g %g %g %g\n",bump_info->quaoffset.y_matrix[quad_ctr][ctr][0],bump_info->quaoffset.y_matrix[quad_ctr][ctr][1],bump_info->quaoffset.y_matrix[quad_ctr][ctr][2],bump_info->quaoffset.y_matrix[quad_ctr][ctr][3]);
	}
	}
	beam_copy(testbeam2, testbeam);
	}
	quad_ctr++;
	}
	}
	FILE *f=open_file("quadresp.bin");
	fwrite(&inter_data.beamline->n_quad,sizeof(int),1,f);
	fwrite(&nlines,sizeof(int),1,f);
	//	  fwrite(bump_info->quaoffset.y_matrix,sizeof(double),n_quad*nlines,f); ??
	for (int i=0;i<inter_data.beamline->n_quad;i++) {
	for (int j=0;j<nlines;j++) {
	for (int k=0;k<4;k++) {
	fwrite(&(bump_info->quaoffset.y_matrix[i][j][k]),sizeof(double),1,f);
	}
	}
	}
	close_file(f);
     

      beam_delete(testbeam2);
    }
    }
  */
  
  /*
  if(qrespfile) {
    placet_printf(INFO,"Quadresp under construction\n");
    return 1;

    if (qrespfile) {
      FILE *f;
      f=read_file(qrespfile);
      int epinq=0,epinl=0;
      fread(&epinq,sizeof(int),1,f);
      fread(&epinl,sizeof(int),1,f);
      for (int i=0;i<epinq;i++) {
	for (int j=0;j<epinl;j++) {
	  for (int k=0;k<4;k++) {
	    fread(&(bump_info->quaoffset.y_matrix[i][j][k]),sizeof(double),1,f);
	  }
	}
      }
      close_file(f);
    }

  }
  */

  // This is not really correct...
  /*
  if(!qrespfile && !dyneffects) {
    bump_info->quaoffset.y_matrix=NULL;
  }
  if(!crespfile && !dyneffects) {
    bump_info->cav_y_matrix=NULL;
  }

  if (!knobfiles || dyneffects) {
    //beam_delete(beam);
    beam_delete(testbeam);
  }
  */

  bump_info->nb=nb;
  bump_info->ns=ns;
  bump_info->nm=nm;
  bump_info->nknobs=nknobs;

  if (name) {
    Tcl_CreateCommand(interp, name, &tk_BumpCorrection, bump_info, NULL);
  } else {
    if (bump_information) {
      free(bump_information->knobcoords);
      if (bump_information->initcoords) {
	free(bump_information->initcoords);
      }
      if (bump_information->range) {
	free(bump_information->range);
      }
      free(bump_information);
    }
    bump_information=bump_info;
  }
  
  return TCL_OK;
}

/**********************************************************************/
/*                              Initialize                            */
/**********************************************************************/

int Peder_Init(Tcl_Interp *interp)
{
  Placet_CreateCommand(interp, "CalcSquareFit", &tk_CalcSquareFit, NULL, NULL);
  Placet_CreateCommand(interp,"CavityNumberList", &tk_CavityNumberList,NULL,NULL);
  Placet_CreateCommand(interp, "ElementGetOffset", &tk_ElementGetOffset, NULL, NULL);
  Placet_CreateCommand(interp, "CalcEmitt", &tk_CalcEmitt, NULL, NULL);
  Placet_CreateCommand(interp, "Svd", &tk_Svd, NULL, NULL);
  Placet_CreateCommand(interp, "GetResponseMatrix", &tk_GetResponseMatrix, NULL, NULL);
  Placet_CreateCommand(interp,"BpmSetResolution", &tk_BpmSetResolution,NULL,NULL);
  Placet_CreateCommand(interp,"BumpCorrection", &tk_BumpCorrection,NULL,NULL);
  Placet_CreateCommand(interp,"BumpSetup", &tk_BumpSetup,NULL,NULL);
  Placet_CreateCommand(interp,"DynamicEffectsSet", &tk_DynamicEffectsSet,NULL,NULL);
  Placet_CreateCommand(interp,"ApplyJitter", &tk_ApplyJitter,NULL,NULL);
  Placet_CreateCommand(interp,"KnobReferenceSet", &tk_KnobReferenceSet,NULL,NULL);
  Placet_CreateCommand(interp,"KnobReferenceZero", &tk_KnobReferenceZero,NULL,NULL);
  Placet_CreateCommand(interp,"KnobSave", &tk_KnobSave,NULL,NULL);
  return TCL_OK;
}
