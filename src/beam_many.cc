#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cstring>

#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"
#include "file_stream.hh"

#include "slices_to_particles.h"
#include "placeti3.h"
#include "placet_tk.h"

extern INTER_DATA_STRUCT inter_data;
extern Tcl_HashTable BeamTable;

typedef int (*cmp_func_t)(const void *, const void * );

static int p_cmp(const double *d1, const double *d2)
{
  return d1[3] < d2[3] ? -1 : 1;
}

/** construct a particle beam from a dumped beam */
int beam_read_many(BEAM* beam,char *name,int binary)
{
    int i,j=0,n;
    FILE *f;
    char buffer[1024],*point;
    double *z_tmp,z_max;
    double wgt0=0.0;

    //    z_tmp=(double*)alloca(sizeof(double)*beam->slices*6);
    z_tmp=(double*)malloc(sizeof(double)*beam->slices*6);
    f = read_file(name);
    if (binary) {
      n=fread(z_tmp,sizeof(double)*6,beam->slices,f);
      if (n!=beam->slices) {
	/* do something */
      }
      for (i=0;i<beam->slices;i++){
	wgt0+=beam->particle[i].wgt;
      }
    }
    else {
      for (i=0;i<beam->slices;i++){
	point=fgets(buffer,1024,f);
	if (point==NULL)
	{
	  placet_cout << ERROR << "Only found " <<i << " particles in " << name <<", wanted " << beam->slices << endmsg;
	  close_file(f);
	  exit(1);
	}
	z_tmp[j++]=strtod(point,&point);
	z_tmp[j++]=strtod(point,&point);
	z_tmp[j++]=strtod(point,&point);
	z_tmp[j++]=strtod(point,&point);
	z_tmp[j++]=strtod(point,&point);
	z_tmp[j++]=strtod(point,&point);
	wgt0+=beam->particle[i].wgt;
      }
    }
    close_file(f);
    qsort(z_tmp,beam->slices,sizeof(double)*6,cmp_func_t(&p_cmp));
    /* check whether this is necessary */
    z_max=0.5*(beam->z_position[1]+beam->z_position[0]);
    for (i=0;i<beam->slices_per_bunch*beam->bunches;i++){
	beam->particle_number[i]=0;
    }
    j=0;
    for (i=0;i<beam->slices;i++){
	while (z_tmp[i*6+3]>z_max) {
	    j++;
	    if (j==beam->slices_per_bunch*beam->bunches) {
	      placet_printf(WARNING,"too large z %g\n",z_tmp[i*6+3]);
	      j=beam->slices_per_bunch*beam->bunches-1;
	      break;
	    }
	    if (j<beam->slices_per_bunch*beam->bunches-1) {
		z_max=0.5*(beam->z_position[j+1]+beam->z_position[j]);
	    }
	    else {
		z_max=beam->z_position[j]+0.5*(beam->z_position[j]
					       -beam->z_position[j-1]);
	    }
	}
	beam->particle[i].energy=z_tmp[i*6];
	beam->particle[i].x=z_tmp[i*6+1];
	beam->particle[i].y=z_tmp[i*6+2];
	beam->particle[i].z=z_tmp[i*6+3];
	beam->particle[i].xp=z_tmp[i*6+4];
	beam->particle[i].yp=z_tmp[i*6+5];
	beam->particle[i].wgt=wgt0/beam->slices;
	beam->particle_number[j]++;
    }
    free(z_tmp);
    beam->particle_beam=true;
    return 0;
}

void rotate_x(BEAM* beam,double theta_x)
{
  double s,c;
  sincos(theta_x,&s,&c);
  for (int i=0;i<beam->slices;i++){
    PARTICLE &particle=beam->particle[i];
    double tmp=c*particle.x-s*particle.z;
    particle.z=s*particle.x+c*particle.z;
    particle.x=tmp;
    double zp = 0.5e-6*(particle.xp*particle.xp+particle.yp*particle.yp);
    particle.xp=c*particle.xp-s*zp;
  }
}

void rotate_y(BEAM* beam,double theta_y)
{
  double s,c;
  sincos(theta_y,&s,&c);
  for (int i=0;i<beam->slices;i++){
    PARTICLE &particle=beam->particle[i];
    double tmp=c*particle.y-s*particle.z;
    particle.z=s*particle.y+c*particle.z;
    particle.y=tmp;
    double zp = 0.5e-6*(particle.xp*particle.xp+particle.yp*particle.yp);
    particle.yp=c*particle.yp-s*zp;
  }
}

void
beam_write(BEAM* beam,char *name,int binary,int axisx,int axisy, int loss,double theta_x,double theta_y)
{
  /// dump the particle beam as a particle beam in the GUINEA-PIG format En x y z x' y'
  /// int lost>0: also store lost particles
  int i=0,j,k,l;
  double xmean=0.0,xpmean=0.0,ymean=0.0,ypmean=0.0;
  double dummy[6];

  FILE *f = open_file(name);
  if (theta_x!=0.0) rotate_x(beam,theta_x);
  if (theta_y!=0.0) rotate_y(beam,theta_y);
     
  if (axisx||axisy) {
    for (l=0;l<beam->bunches;l++) {
      for (k=0;k<beam->slices_per_bunch;k++){
	for (j=0;j<beam->particle_number[k+l*beam->slices_per_bunch];j++){
	  xmean+=beam->particle[i].x;
	  xpmean+=beam->particle[i].xp;
	  ymean+=beam->particle[i].y;
	  ypmean+=beam->particle[i].yp;
	  i++;
	}
      }
    }
    if (i>0) {
      xmean/=i;
      xpmean/=i;
      ymean/=i;
      ypmean/=i;
    }
  }
  if (!axisx) {
    xmean=0.0;
    xpmean=0.0;
  }
  if (!axisy) {
    ymean=0.0;
    ypmean=0.0;
  }
  i=0;
  for (i=0,l=0;l<beam->bunches;l++) {
    for (k=0;k<beam->slices_per_bunch;k++){
      for (j=0;j<beam->particle_number[k+l*beam->slices_per_bunch];j++, i++){
	if (!loss && beam->particle[i].is_lost()) {continue;}
	dummy[0]=beam->particle[i].energy;
	dummy[1]=beam->particle[i].x-xmean;
	dummy[2]=beam->particle[i].y-ymean;
	//dummy[3]=beam->z_position[k+l*beam->slices_per_bunch];
	dummy[3]=beam->particle[i].z;
	dummy[4]=beam->particle[i].xp-xpmean;
	dummy[5]=beam->particle[i].yp-ypmean;	  
	if (binary) { 
	  fwrite(dummy,sizeof(double),6,f);
	} else {
	  fprintf(f,"%.15g %.15g %.15g %.15g %.15g %.15g\n",
		  dummy[0],dummy[1],dummy[2],dummy[3],dummy[4],dummy[5]);
	}
      }
    }
  }
  if (theta_x!=0.0) rotate_x(beam,-theta_x);
  if (theta_y!=0.0) rotate_y(beam,-theta_y);
  close_file(f);
}

void
beam_write_many(BEAM* beam,char *name,int binary,int axisx,int axisy, int loss, int dist,double theta_x,double theta_y)
{
  // dump the sliced beam as a particle beam in the GUINEA-PIG format En x y z x' y' 
  //  int i,j=0,k,l;
  // double xmean=0.0,xpmean=0.0,ymean=0.0,ypmean=0.0;
  //double dummy[6];

  if(placet_switch.first_order || beam->particle_beam){
    beam_write(beam,name,binary,axisx,axisy, loss, theta_x,theta_y);   
  }
  else{
  // take a copy of the beam in order to do not change the original one
  
    BEAM *beamcopy=bunch_remake(beam);
    beam_copy(beam,beamcopy);
  
    // if it is a sliced beam first convert it into a particles one  
  
    slices_to_particles(beamcopy,dist);
    beam_write(beamcopy,name,binary,axisx,axisy, loss,theta_x,theta_y);
    beam_delete(beamcopy);
  }
}


int tk_BeamRead(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
    int error;
    BEAM *beam;
    int bin=0;
    char *fname=NULL,*bname=NULL;
    char *binfile=NULL;    
    Tk_ArgvInfo table[]={
	{(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
	 (char*)"Name of the file from where to read the particles"},
	{(char*)"-binary",TK_ARGV_INT,(char*)NULL,(char*)&bin,
	 (char*)"If not zero read in binary format"},
	{(char*)"-binary_stream",TK_ARGV_INT,(char*)NULL,(char*)&binfile,
	 (char*)"Name of the file where to read particles from as a binary stream"},
	{(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&bname,
	 (char*)"Name of the beam to be read"},
	{(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
	return error;
    }
    if (argc!=1) {
	Tcl_AppendResult(interp,"Usage: ",argv[0],"options\n",NULL);
	return TCL_ERROR;
    }
    if (bname) {
      if (strcmp(bname,"workbeam")==0){
	beam=inter_data.bunch;
	beam_read_many(beam,fname,bin);
      }
      else {
	beam=get_beam(bname);
	beam_read_many(beam,fname,bin);
	if (binfile){
	  placet_cout << ERROR <<"IStream filename: " << binfile << endmsg;
	  File_IStream file(binfile);
	  file >> *beam;
	}
      }
    }
    else {
      Tcl_AppendResult(interp,"ERROR: in BeamRead you must specify a beam.",
		       NULL);
      return TCL_ERROR;
    }
    return TCL_OK;
}

int tk_BeamDump(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
    int error;
    BEAM *beam;
    int bin=0,xaxis=0,yaxis=0,loss=0;
    int dist=0;
    char *fname=NULL,*bname=NULL;
    char *binfile=NULL;
    double theta_x=0.0; double theta_y=0.0;
    Tk_ArgvInfo table[]={
	{(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
	 (char*)"Name of the file from where to write the particles"},
	{(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&bname,
	 (char*)"Name of the beam into which to read the particles"},
	{(char*)"-xaxis",TK_ARGV_INT,(char*)NULL,(char*)&xaxis,
	 (char*)"If not zero remove mean horizontal angle and offset"},
	{(char*)"-yaxis",TK_ARGV_INT,(char*)NULL,(char*)&yaxis,
	 (char*)"If not zero remove mean vertical angle and offset"},
	{(char*)"-binary",TK_ARGV_INT,(char*)NULL,(char*)&bin,
	 (char*)"If not zero write in binary format"},
	{(char*)"-binary_stream",TK_ARGV_STRING,(char*)NULL,(char*)&binfile,
	 (char*)"Name of the file where to write particles to as a binary stream"},
	{(char*)"-losses",TK_ARGV_INT,(char*)NULL,(char*)&loss,
	 (char*)"If not zero write also the lost particles into the file"},
	// {(char*)"-seed",TK_ARGV_INT,(char*)NULL,(char*)&seed,
	//  (char*)"Seed to be used for transforming slices to rays"},
	{(char*)"-type",TK_ARGV_INT,(char*)NULL,(char*)&dist,
	 (char*)"If 1 the particles distribution in energy and z comes from continuous slices"},
	{(char*)"-rotate_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&theta_x,
	 (char*)"rotate the bunch in the s x plane [rad]"},
	{(char*)"-rotate_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&theta_y,
	 (char*)"rotate the bunch in the s y plane [rad]"},
	{(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };
    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
	return error;
    }
    if (argc!=1) {
	Tcl_AppendResult(interp,"Usage: ",argv[0],"options\n",NULL);
	return TCL_ERROR;
    }
    if (bname) {
      beam=get_beam(bname);
    }
    else {
      beam=inter_data.bunch;
    }
    if (fname) beam_write_many(beam,fname,bin,xaxis,yaxis,loss,dist,theta_x,theta_y);
    
   if (binfile){
     placet_cout << ERROR << "OStream filename: " << binfile << endmsg;
     File_OStream file(binfile);
     file << *beam;
   }
    
   return TCL_OK;
}

int tk_BeamDelete(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		char *argv[])
{
    int error;
    BEAM *beam;
    char *bname=NULL;
    Tk_ArgvInfo table[]={
	{(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&bname,
	 (char*)"Name of the beam to delete"},
	{(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
    };

    if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
	return error;
    }

    if (bname) {
      Tcl_HashEntry* entry=Tcl_FindHashEntry(&BeamTable,bname); 	       
      if (entry!=NULL){ 	 
	beam=(BEAM*)Tcl_GetHashValue(entry); 	 
	beam_delete(beam);
	Tcl_DeleteHashEntry(entry);	 
      } 	 
      else { 	 
	Tcl_AppendResult(interp,"-beam name not found\n",NULL); 	 
	return TCL_ERROR; 	 
      }
    }
    else {
      Tcl_AppendResult(interp,"-no beam name specified\n",NULL);
      return TCL_ERROR;
    }
    
    return TCL_OK;
}
