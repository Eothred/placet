#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include "survey.c"

int
tk_SetBpmScaleError(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error=TCL_OK;
  int seed=0;
  double sx=0.0,sy=0.0;
  Tk_ArgvInfo table[]={
    {(char*)"-seed",TK_ARGV_INT,(char*)NULL,(char*)&seed,
     (char*)"Seed for the random number generator"},
    {(char*)"-sigma_x",TK_ARGV_FLOAT,(char*)NULL,(char*)&sx,
     (char*)"Sigma of the scale factor error in x"},
    {(char*)"-sigma_y",TK_ARGV_FLOAT,(char*)NULL,(char*)&sy,
     (char*)"Sigma of the scale factor error in y"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (argc!=1) {
    Tcl_AppendResult(interp,"Error in ",argv[0],":\n",NULL);
    Tcl_AppendResult(interp,"Usage: ",argv[0]," [-seed number]\n",NULL);
    return TCL_ERROR;
  }
  
  if (seed) {
    Survey.SetSeed(seed);
  }
  beamline_survey_bpmscale(inter_data.beamline,sx,sy);
  return TCL_OK;
}

int tk_Tesla(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
	     char *argv[])
{
  int error=TCL_OK;
  int seed=0;
  static double sigma=0.001;
  Tk_ArgvInfo table[]={
    {(char*)"-seed",TK_ARGV_INT,(char*)NULL,(char*)&seed,
     (char*)"Seed for the random number generator"},
    {(char*)"-sigma",TK_ARGV_FLOAT,(char*)NULL,(char*)&sigma,
     (char*)"Sigma of the relative detuning"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_AppendResult(interp,"Error in ",argv[0],":\n",NULL);
    Tcl_AppendResult(interp,"Usage: ",argv[0]," [-seed number]\n",NULL);
    return TCL_ERROR;
  }
  
    if (seed) {
      Survey.SetSeed(seed);
    }
  beamline_survey_tesla2(inter_data.beamline,sigma);
  return TCL_OK;
}

int
Survey_Init(Tcl_Interp* interp)
{
  Tcl_CreateCommand(interp,"Tesla",tk_Tesla,(ClientData)NULL,
		    (Tcl_CmdDeleteProc*)NULL);  
  Tcl_CreateCommand(interp,"SetBpmScaleError",tk_SetBpmScaleError,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  return TCL_OK;
}
