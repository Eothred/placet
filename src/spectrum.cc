#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "placet_tk.h"
#include "placeti3.h"
#include "placeti4.h"
#include "spectrum.h"

extern INTER_DATA_STRUCT inter_data;

/******   Tcl/Tk commands   ******/

/**********************************************************************/
/*                      TestSpectrum                                  */
/**********************************************************************/

int tk_TestSpectrum(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  char *file_name=NULL,name0[]="spectrum.dat";
  char *beamname=NULL;
  BEAM *beam=NULL;
  int nstep=500,do_girder=0;
  double l0=1.0,l1=100.0;
  Tk_ArgvInfo table[]={
    {(char*)"-steps",TK_ARGV_INT,(char*)NULL,(char*)&nstep,
     (char*)"Number of steps to simulate for the jitter amplitude"},
    {(char*)"-lambda0",TK_ARGV_FLOAT,(char*)NULL,(char*)&l0,
     (char*)"Minimal wavelength of error [m]"},
    {(char*)"-lambda1",TK_ARGV_FLOAT,(char*)NULL,(char*)&l1,
     (char*)"Maximal wavelength of error [m]"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for evaluation of the emittance growth"},
    {(char*)"-girders",TK_ARGV_INT,(char*)NULL,
     (char*)&do_girder,
     (char*)"Move the girders rather than individual elements"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  file_name=name0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to TestSpectrum",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;

  test_no_spectrum(inter_data.beamline,beam,l0,l1,nstep,file_name,do_girder);
  return TCL_OK;
}

/**********************************************************************/
/*                      TestSpectrumSimpleCorrection                  */
/**********************************************************************/

int tk_TestSpectrumSimpleCorrection(ClientData /*clientdata*/,Tcl_Interp *interp,
				    int argc,char *argv[])
{
  int error;
  char *file_name=NULL,name0[]="spectrum.dat";
  char *beamname=NULL;
  BEAM *beam=NULL;
  BIN **bin;
  int nbin;
  int nstep=500;
  double l0=1.0,l1=100.0;
  Tk_ArgvInfo table[]={
    {(char*)"-steps",TK_ARGV_INT,(char*)NULL,(char*)&nstep,
     (char*)"Number of steps to simulate for the jitter amplitude"},
    {(char*)"-lambda0",TK_ARGV_FLOAT,(char*)NULL,(char*)&l0,
     (char*)"Minimal wavelength of error [m]"},
    {(char*)"-lambda1",TK_ARGV_FLOAT,(char*)NULL,(char*)&l1,
     (char*)"Maximal wavelength of error [m]"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname,
     (char*)"Name of the beam to be used for evaluation of the emittance growth"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  file_name=name0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_SetResult(interp,(char*)"Too many arguments to TestSpectrumSimpleCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (beamname!=NULL){
    beam=get_beam(beamname);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (error) return error;

  bin=(BIN**)alloca(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide_1(inter_data.beamline,1,0,bin,&nbin);
  test_simple_spectrum(inter_data.beamline,bin,nbin,beam,l0,l1,nstep,
		       file_name);
  return TCL_OK;
}

