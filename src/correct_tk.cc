#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cstring>

#include <tcl.h>
#include <tk.h>

#include "correct_tk.h"
#include "placeti3.h"

#include "placet_print.h"

// unnamed namespace to limit scope
namespace {

typedef struct {
  double **res,*a,**sig,*stiff,**meas,*corr,*state,**target,*est,*norm;
  double gain;
  int nres,nknob,nmeas,ncorr,*indx,itype,use_wgt;
} CORRECTION_DATA;

double*
make_hessian(double res[],double sig[],double stiff[],int nknob,int nsens)
{
  int i,j,k;
  double *a,sum;

  a=(double*)malloc(sizeof(double)*nknob*nknob);
  for(j=0;j<nknob;++j){
    for(i=0;i<nknob;++i){
      sum=0.0;
      for(k=0;k<nsens;++k){
	sum+=res[k+i*nsens]*res[k+j*nsens]*sig[k]*sig[k];
      }
      a[i+j*nknob]=sum;
    }
    a[j*(nknob+1)]+=stiff[j]*stiff[j];
  }
  return a;
}

double*
make_hessian_n(double *res0[],double *sig0[],double stiff[],int nmat,int nknob,
	       int nsens)
{
  int i,j,k,l;
  double *a,sum,*res,*sig;

  a=(double*)malloc(sizeof(double)*nknob*nknob);
  for(j=0;j<nknob;++j){
    for(i=0;i<nknob;++i){
      a[i+j*nknob]=0.0;
    }
  }
  for(l=0;l<nmat;++l){
    res=res0[l];
    sig=sig0[l];
    for(j=0;j<nknob;++j){
      for(i=0;i<nknob;++i){
	sum=0.0;
	for(k=0;k<nsens;++k){
	  sum+=res[k+i*nsens]*res[k+j*nsens]*sig[k]*sig[k];
	}
	a[i+j*nknob]+=sum;
      }
    }
  }
  for(j=0;j<nknob;++j){
    a[j*(nknob+1)]+=stiff[j]*stiff[j];
  }
  return a;
}

void
right_hand_side(double res[],double sig[],double stiff[],double state[],
		double val[],int nknob,int nsens,double b[])
{
  int i,j;
  double sum;

  for(j=0;j<nknob;++j){
    sum=0.0;
    for(i=0;i<nsens;++i){
      sum+=res[i+j*nsens]*val[i]*sig[i]*sig[i];
    }
    b[j]=-(sum+stiff[j]*stiff[j]*state[j]);
  }
}

void
right_hand_side_n(double *res0[],double *sig0[],double stiff[],double state[],
		  double *val0[],int nmat,int nknob,int nsens,double b[])
{
  int i,j,l;
  double sum,*res,*sig,*val;

  for(j=0;j<nknob;++j){
    b[j]=0.0;
  }
  for(l=0;l<nmat;++l){
    res=res0[l];
    sig=sig0[l];
    val=val0[l];
    for(j=0;j<nknob;++j){
      sum=0.0;
      for(i=0;i<nsens;++i){
	sum+=res[i+j*nsens]*val[i]*sig[i]*sig[i];
      }
      b[j]-=sum;
    }
  }
  for(j=0;j<nknob;++j){
    b[j]-=stiff[j]*stiff[j]*state[j];
  }
}

void
matrix_multiply(double res[],double /*meas*/[],double corr[],
		int nknob,int nsens,double gain,double est[])
{
  int i,j;
  double sum;

  for(j=0;j<nsens;++j){
    sum=0.0;
    for(i=0;i<nknob;++i){
      sum+=res[i*nsens+j]*corr[i]*gain;
    }
    est[j]=sum;
  }
}

int
Tcl_MatrixCorrectionApply(ClientData clientData,Tcl_Interp *interp,
			  int argc,char *argv[])
{
  CORRECTION_DATA *correction_data;
  char *meas,**v,buffer[100],*l_state=NULL,*t=NULL;
  int error,i,j,m,n;
  static double gain;
  int ret=1;
  Tk_ArgvInfo table[]={
    {(char*)"-measured",TK_ARGV_STRING,(char*)NULL,
     (char*)&meas,
     (char*)"List of lists with the measurements"},
    {(char*)"-state",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_state,
     (char*)"List of lists with the state of the correctors"},
    {(char*)"-return_value",TK_ARGV_INT,(char*)NULL,
     (char*)&ret,
     (char*)"Choice of return value: 1: corrector, 2: estimated"},
    {(char*)"-target",TK_ARGV_STRING,(char*)NULL,
     (char*)&t,
     (char*)"List of target values for the measurement values"},
    {(char*)"-gain",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gain,
     (char*)"Gain to be applied to the calculated correction"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  correction_data=(CORRECTION_DATA*)clientData;
  gain=correction_data->gain;

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc>1){
    Tcl_SetResult(interp,"Too many arguments to <MatrixCorrectionApply>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (ret==1) {
    if (!meas) {
      Tcl_AppendResult(interp,"Need a list",NULL);
      return TCL_ERROR;
    }
    
    if (t) {
      if(error=Tcl_SplitList(interp,t,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -target",NULL);
	return error;
      }
      if ((m!=correction_data->nres)&&(m!=1)) {
	Tcl_AppendResult(interp,"Number of target values does not match",NULL);
	return TCL_ERROR;
      }
      for (j=0;j<m;++j) {
	if (error=Tcl_GetDouble(interp,v[j],correction_data->target[0]+j)) {
	  return error;
	}
      }
    }
    if(error=Tcl_SplitList(interp,meas,&n,&v)) {
      Tcl_AppendResult(interp,"Cannot read list",NULL);
      return error;
    }
    
    if (n!=correction_data->nres) {
      Tcl_AppendResult(interp,
		       "List does not have the right number of elements",
		       NULL);
      return TCL_ERROR;
    }
    
    for(i=0;i<n;++i){
      if (error=Tcl_GetDouble(interp,v[i],correction_data->meas[0]+i)) {
	return error;
      }
      correction_data->meas[0][i]-=correction_data->target[0][i];
    }
    Tcl_Free((char*)v);
    
    if (l_state) {
      if(error=Tcl_SplitList(interp,l_state,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -state",NULL);
	return error;
      }
      if ((m!=correction_data->nknob)&&(m!=1)) {
	Tcl_AppendResult(interp,"",NULL);
	return TCL_ERROR;
      }
      for (j=0;j<m;++j) {
	if (error=Tcl_GetDouble(interp,v[j],correction_data->state+j)) {
	  return error;
	}
      }
      if ((m==1)&&(correction_data->nknob>1)) {
	for (i=1;i<n;++i){
	  correction_data->state[i]=correction_data->state[0];
	}
      }
      Tcl_Free((char*)v);
    } else {
      for (i=0;i<correction_data->nknob;++i) {
	correction_data->state[i]=0.0;
      }
    }
    
    right_hand_side_n(correction_data->res,correction_data->sig,
		      correction_data->stiff,
		      correction_data->state,
		      correction_data->meas,
		      correction_data->nmeas,
		      correction_data->nknob,
		      correction_data->nres,
		      correction_data->corr);
    lubksb(correction_data->a,correction_data->nknob,correction_data->indx,
	   correction_data->corr);
    for (i=0;i<correction_data->nknob;++i){
      snprintf(buffer,100,"%g",correction_data->corr[i]*gain);
      Tcl_AppendElement(interp,buffer);
    }
  }
  else {
    switch (ret) {
    case 2:
      matrix_multiply(correction_data->res[0],correction_data->meas[0],
		      correction_data->corr,correction_data->nknob,
		      correction_data->nres,gain,
		      correction_data->est);
      for (i=0;i<correction_data->nres;++i){
	snprintf(buffer,100,"%g",correction_data->est[i]);
	Tcl_AppendElement(interp,buffer);
      }    
      break;
    case 3:
      matrix_multiply(correction_data->res[0],correction_data->meas[0],
		      correction_data->corr,correction_data->nknob,
		      correction_data->nres,gain,
		      correction_data->est);
      for (i=0;i<correction_data->nres;++i){
	snprintf(buffer,100,"%g",correction_data->est[i]+correction_data->meas[0][i]
		+correction_data->target[0][i]);
	Tcl_AppendElement(interp,buffer);
      }    
      break;
    }
  }
  //  correction_data->gain=gain;
  return TCL_OK;
}

int
Tcl_MatrixCorrectionInit(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  char **v,**v2;
  int i,n,j,m,m1,k,nmeas=1,nreal;
  CORRECTION_DATA *correction_data;
  char *init,*type="linear",*l_stiff=NULL,*l_sigma=NULL,*name,*l_state=NULL,
      *t=NULL;
  double d;
  Tk_ArgvInfo table[]={
    {(char*)"-response",TK_ARGV_STRING,(char*)NULL,
     (char*)&init,
     (char*)"List of lists with the response matrices"},
    {(char*)"-number",TK_ARGV_INT,(char*)NULL,
     (char*)&nmeas,
     (char*)"Number of response matrices used"},
    {(char*)"-stiff",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_stiff,
     (char*)"List of the stiffness of the knobs"},
    {(char*)"-sigma",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_sigma,
     (char*)"List of the weights for the optimisation"},
    {(char*)"-state",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_state,
     (char*)"List of the state of the knobs"},
    {(char*)"-target",TK_ARGV_STRING,(char*)NULL,
     (char*)&t,
     (char*)"List of target values for the measurement values"},
    {(char*)"-type",TK_ARGV_STRING,(char*)NULL,
     (char*)&type,
     (char*)"type of correction (sofar linear only)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc>2){
    Tcl_SetResult(interp,"Too many arguments to <MatrixCorrectionInit>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<2){
    Tcl_SetResult(interp,"<MatrixCorrectionInit> requires a name",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  name=argv[1];

  if (strcmp(type,"linear")!=0) {
    return TCL_ERROR;
  }

  if (nmeas!=1) {
    if(error=Tcl_SplitList(interp,init,&nreal,&v)) {
      placet_printf(INFO,"Cannot read list\n");
      return error;
    }
    if (nreal!=nmeas) {
      Tcl_SetResult(interp,
		    "The specified number of matrices does not agree with the real one",
		    TCL_VOLATILE);      
      return TCL_ERROR;
    }
  }
  else {
    if(error=Tcl_SplitList(interp,init,&n,&v)) {
      placet_printf(INFO,"Cannot read list\n");
      return error;
    }
    
    if(error=Tcl_SplitList(interp,v[0],&m1,&v2)) {
      Tcl_AppendResult(interp,"Cannot read sub-list",NULL);
      return error;
    }
  }

  correction_data=(CORRECTION_DATA*)malloc(sizeof(CORRECTION_DATA));
  correction_data->res=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->res[0]=(double*)malloc(sizeof(double)*n*m1);
  correction_data->stiff=(double*)malloc(sizeof(double)*n);
  correction_data->state=(double*)malloc(sizeof(double)*n);
  correction_data->corr=(double*)malloc(sizeof(double)*n);
  correction_data->est=(double*)malloc(sizeof(double)*m1);
  correction_data->sig=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->sig[0]=(double*)malloc(sizeof(double)*m1);
  correction_data->target=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->target[0]=(double*)malloc(sizeof(double)*m1);
  correction_data->indx=(int*)malloc(sizeof(int)*n);
  correction_data->nmeas=nmeas;
  correction_data->nknob=n;
  correction_data->nres=m1;
  correction_data->meas=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->meas[0]=(double*)malloc(sizeof(double)*m1);
  correction_data->gain=1.0;

  k=0;
  for (j=0;j<m1;++j) {
    if (error=Tcl_GetDouble(interp,v2[j],&(correction_data->res[0][k]))) {
      return error;
    }
    k++;
  }
  Tcl_Free((char*)v2);

  for (i=1;i<n;++i){
    if(error=Tcl_SplitList(interp,v[i],&m,&v2)) {
      Tcl_AppendResult(interp,"Cannot read sub-list",NULL);
      return error;
    }

    if (m!=m1) {
      Tcl_AppendResult(interp,"The matrix needs to be rectangular",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v2[j],&(correction_data->res[0][k]))) {
	return error;
      }
      k++;
    }
    Tcl_Free((char*)v2);
  }
  Tcl_Free((char*)v);
  if (l_stiff) {
    if(error=Tcl_SplitList(interp,l_stiff,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -stiff",NULL);
      return error;
    }
    if ((m!=n)&&(m!=1)) {
      Tcl_AppendResult(interp,"Number of stiffness values does not match",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->stiff+j)) {
	return error;
      }
    }
    if ((m==1)&&(n>1)) {
      for (i=1;i<n;++i){
	correction_data->stiff[i]=correction_data->stiff[0];
      }
    }
    Tcl_Free((char*)v);
  }
  else {
    for (i=0;i<n;++i) {
      correction_data->stiff[i]=0.0;
    }
  }
  if (t) {
    if(error=Tcl_SplitList(interp,t,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -target",NULL);
	return error;
    }
    if ((m!=m1)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->target[0]+j)) {
	return error;
      }
    }
  }
  else {
    for (j=0;j<m1;++j) {
	correction_data->target[0][j]=0.0;
    }
  }
  if (l_sigma) {
    if(error=Tcl_SplitList(interp,l_sigma,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -sigma",NULL);
      return error;
    }
    if ((m!=m1)&&(m!=1)) {
      Tcl_AppendResult(interp,"Number of weights does not match",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->sig[0]+j)) {
	return error;
      }
    }
    if ((m==1)&&(m1>1)) {
      for (i=1;i<m1;++i){
	correction_data->sig[0][i]=correction_data->sig[0][0];
      }
    }
    Tcl_Free((char*)v);
  } else {
    for (i=0;i<m1;++i) {
      correction_data->sig[0][i]=1.0;
    }
  }
  if (l_state) {
    if(error=Tcl_SplitList(interp,l_state,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -state",NULL);
      return error;
    }
    if ((m!=n)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->state+j)) {
	return error;
      }
    }
    if ((m==1)&&(n>1)) {
      for (i=1;i<n;++i){
	correction_data->state[i]=correction_data->state[0];
      }
    }
    Tcl_Free((char*)v);
  } else {
    for (i=0;i<n;++i) {
      correction_data->state[i]=0.0;
    }
  }
  correction_data->a=make_hessian_n(correction_data->res,
				    correction_data->sig,
				    correction_data->stiff,
				    correction_data->nmeas,
				    correction_data->nknob,
				    correction_data->nres);
  ludcmp(correction_data->a,correction_data->nknob,correction_data->indx,&d);
  Tcl_CreateCommand(interp,name,Tcl_MatrixCorrectionApply,
		    (ClientData)correction_data,(Tcl_CmdDeleteProc*)NULL);
  return TCL_OK;
}

void
find_eigen(double res[],double sig[],double norm[],
	   int ncorr,int nbpm,double meas[],
	   double e[])
{
  int i,j,k=0;
  double sum;
  if (sig) {
    for (i=0;i<ncorr;++i) {
      sum=0.0;
      for (j=0;j<nbpm;++j) {
	sum+=meas[j]*res[k++]*sig[j]*sig[j];
      }
      e[i]=sum*norm[i];
    }
  }
  else {
    for (i=0;i<ncorr;++i) {
      sum=0.0;
      for (j=0;j<nbpm;++j) {
	sum+=meas[j]*res[k++];
      }
      e[i]=sum*norm[i];
    }
  }
}

void
find_eigen_high(double res[],double sig[],double norm[],
		int ncorr,int nbpm,double meas[],
		int jcorr,int ie[])
{
  int i,j,k=0,l;
  double sum,sm;
  for (i=0;i<ncorr;++i) {
    ie[i]=i;
  }
  for (l=0;l<jcorr;++l) {
    sm=0.0;
    for (i=l;i<ncorr;++i) {
      k=ie[i]*nbpm;
      sum=0.0;
      if (sig) {
	for (j=0;j<nbpm;++j) {
	  sum+=meas[j]*res[k++]*sig[j]*sig[j];
	}
      }
      else {
	for (j=0;j<nbpm;++j) {
	  sum+=meas[j]*res[k++];
	}
      }
      sum*=norm[ie[i]];
      if (sum>sm) {
	sm=sum;
	k=ie[i];
	ie[i]=ie[l];
	ie[l]=k;
      }
    }
    k=ie[l]*nbpm;    
    sm*=norm[ie[l]];
    for (i=0;i<nbpm;++i) {
      meas[i]-=res[k++]*sm;
    }
  }
}

void
find_eigen_high_2(double res[],double sig[],double norm[],
		  int ncorr,int nbpm,double meas[],
		  int jcorr,int ie[])
{
  int i,j,k=0,l;
  double sum,sm,sum2,sm2,tmp;
  for (i=0;i<ncorr;++i) {
    ie[i]=i;
  }
  for (l=0;l<jcorr;++l) {
    sm=0.0;
    sm2=1e300;
    for (i=l;i<ncorr;++i) {
      k=ie[i]*nbpm;
      sum=0.0;
      for (j=0;j<nbpm;++j) {
	sum+=meas[j]*res[k++]*sig[j]*sig[j];
      }
      sum*=norm[ie[i]]*norm[ie[i]];
      sum2=0.0;
      k=ie[i]*nbpm;
      for (j=0;j<nbpm;++j) {
	tmp=(meas[j]-res[k++]*sum)*sig[j];
	sum2+=tmp*tmp;
      }
      if (sum2<sm2) {
	sm2=sum2;
	sm=sum;
	k=ie[i];
	ie[i]=ie[l];
	ie[l]=k;
      }
    }
    k=ie[l]*nbpm;    
    for (i=0;i<nbpm;++i) {
      meas[i]-=res[k++]*sm;
    }
  }
}

void
sort_eigen(double e[],int ie[],int n,int ns)
{
  int i,j,tmp;
  double m;

  for (i=0;i<n;++i) {
    ie[i]=i;
  }
  for (i=0;i<ns;++i) {
    for (j=i+1;j<n;++j) {
      if (e[i]<e[j]) {
	m=e[i];
	e[i]=e[j];
	e[j]=m;
	tmp=ie[i];
	ie[i]=ie[j];
	ie[j]=tmp;
      }
    }
  }
}

void
propose_solution(double res[],double norm[],double sig[],double stiff[],
		 double state[],double meas[],int ncorr,int nbpm,
		 double c[],int ic[],int jcorr)
{
  double e[nbpm],res_tmp[nbpm*jcorr],*a,d;
  int ie[nbpm],k=0,ib,indx[jcorr];
  find_eigen(res,sig,norm,ncorr,nbpm,meas,e);
  sort_eigen(e,ie,ncorr,jcorr);
  for (int i=0;i<jcorr;++i) {
    ib=nbpm*ie[i];
    ic[i]=ie[i];
    for (int j=0;j<nbpm;++j) {
      res_tmp[k++]=res[ib++];
    }
  }
  a=make_hessian(res_tmp,sig,stiff,jcorr,nbpm);
  ludcmp(a,jcorr,indx,&d);
  right_hand_side(res_tmp,sig,stiff,state,meas,jcorr,nbpm,c);
  lubksb(a,jcorr,indx,c);
  free(a);
}

void
propose_solution_2(double res[],double norm[],double sig[],double stiff[],
		   double state[],double meas[],int ncorr,int nbpm,
		   double c[],int ic[],int jcorr)
{
  double res_tmp[nbpm*jcorr],*a,d,meas1[nbpm],stiff_tmp[jcorr];
  int ie[nbpm],k=0,ib,indx[jcorr];

  for (int i=0;i<nbpm;++i) {
    meas1[i]=meas[i];
  }
  find_eigen_high(res,sig,norm,ncorr,nbpm,meas1,jcorr,ie);
  //  find_eigen_high(res,NULL,norm,ncorr,nbpm,meas1,jcorr,ie);
  for (int i=0;i<jcorr;++i) {
    ib=nbpm*ie[i];
    ic[i]=ie[i];
    for (int j=0;j<nbpm;++j) {
      res_tmp[k++]=res[ib++];
    }
    stiff_tmp[i]=stiff[ie[i]];
  }
  a=make_hessian(res_tmp,sig,stiff_tmp,jcorr,nbpm);
  ludcmp(a,jcorr,indx,&d);
  right_hand_side(res_tmp,sig,stiff_tmp,state,meas,jcorr,nbpm,c);
  lubksb(a,jcorr,indx,c);
  free(a);
}

void
propose_solution_0(double res[],double norm[],double sig[],double stiff[],
		   double state[],double meas[],int ncorr,int nbpm,
		   double c[],int ic[],int jcorr)
{
  double res_tmp[nbpm*jcorr],*a,d,meas1[nbpm],stiff_tmp[jcorr];
  int ie[nbpm],k=0,ib,indx[jcorr];

  for (int i=0;i<nbpm;++i) {
    meas1[i]=meas[i];
  }
  find_eigen_high_2(res,sig,norm,ncorr,nbpm,meas1,jcorr,ie);
  for (int i=0;i<jcorr;++i) {
    ib=nbpm*ie[i];
    ic[i]=ie[i];
    for (int j=0;j<nbpm;++j) {
      res_tmp[k++]=res[ib++];
    }
    stiff_tmp[i]=stiff[ie[i]];
  }
  a=make_hessian(res_tmp,sig,stiff_tmp,jcorr,nbpm);
  ludcmp(a,jcorr,indx,&d);
  right_hand_side(res_tmp,sig,stiff_tmp,state,meas,jcorr,nbpm,c);
  lubksb(a,jcorr,indx,c);
  free(a);
}

int
Tcl_MicadoCorrectionApply(ClientData clientData,Tcl_Interp *interp,
			  int argc,char *argv[])
{
  double *tmp_corr;
  CORRECTION_DATA *correction_data;
  char *meas,**v,buffer[100],*l_state=NULL,*t=NULL;
  int error,i,j,m,n,*ic,ncorr;
  static double gain;
  int ret=1;
  Tk_ArgvInfo table[]={
    {(char*)"-measured",TK_ARGV_STRING,(char*)NULL,
     (char*)&meas,
     (char*)"List of lists with the measurements"},
    {(char*)"-correctors",TK_ARGV_INT,(char*)NULL,
     (char*)&ncorr,
     (char*)"Number of correctors to be used"},
    {(char*)"-state",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_state,
     (char*)"List of lists with the state of the correctors"},
    {(char*)"-return_value",TK_ARGV_INT,(char*)NULL,
     (char*)&ret,
     (char*)"Choice of return value: 1: corrector, 2: estimated"},
    {(char*)"-target",TK_ARGV_STRING,(char*)NULL,
     (char*)&t,
     (char*)"List of target values for the measurement values"},
    {(char*)"-gain",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&gain,
     (char*)"Gain to be applied to the calculated correction"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  correction_data=(CORRECTION_DATA*)clientData;
  gain=correction_data->gain;
  ncorr=correction_data->ncorr;

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc>1){
    Tcl_SetResult(interp,"Too many arguments to <MicadoCorrectionApply>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  ic=(int*)alloca(sizeof(int)*ncorr);

  if (ret==1) {
    if (!meas) {
      Tcl_AppendResult(interp,"Need a list",NULL);
      return TCL_ERROR;
    }
    
    if (t) {
      if(error=Tcl_SplitList(interp,t,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -target",NULL);
	return error;
      }
      if ((m!=correction_data->nres)&&(m!=1)) {
	Tcl_AppendResult(interp,"Number of target values does not match",NULL);
	return TCL_ERROR;
      }
      for (j=0;j<m;++j) {
	if (error=Tcl_GetDouble(interp,v[j],correction_data->target[0]+j)) {
	  return error;
	}
      }
    }
    if(error=Tcl_SplitList(interp,meas,&n,&v)) {
      Tcl_AppendResult(interp,"Cannot read list",NULL);
      return error;
    }
    
    if (n!=correction_data->nres) {
      Tcl_AppendResult(interp,
		       "List does not have the right number of elements",
		       NULL);
      return TCL_ERROR;
    }
    
    for(i=0;i<n;++i){
      if (error=Tcl_GetDouble(interp,v[i],correction_data->meas[0]+i)) {
	return error;
      }
      correction_data->meas[0][i]-=correction_data->target[0][i];
    }
    Tcl_Free((char*)v);
    
    if (l_state) {
      if(error=Tcl_SplitList(interp,l_state,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -state",NULL);
	return error;
      }
      if ((m!=correction_data->nknob)&&(m!=1)) {
	Tcl_AppendResult(interp,"",NULL);
	return TCL_ERROR;
      }
      for (j=0;j<m;++j) {
	if (error=Tcl_GetDouble(interp,v[j],correction_data->state+j)) {
	  return error;
	}
      }
      if ((m==1)&&(correction_data->nknob>1)) {
	for (i=1;i<n;++i){
	  correction_data->state[i]=correction_data->state[0];
	}
      }
      Tcl_Free((char*)v);
    } else {
      for (i=0;i<correction_data->nknob;++i) {
	correction_data->state[i]=0.0;
      }
    }
    tmp_corr=(double*)alloca(sizeof(double)*correction_data->nknob);
    if (correction_data->use_wgt) {
      switch (correction_data->itype) {
      case 0:
	propose_solution_0(correction_data->res[0],
			   correction_data->norm,
			   correction_data->sig[0],
			   correction_data->stiff,
			   correction_data->state,
			   correction_data->meas[0],
			   correction_data->nknob,correction_data->nres,
			   tmp_corr,ic,ncorr);
	break;
      case 1:
	propose_solution(correction_data->res[0],correction_data->norm,
			 correction_data->sig[0],
			 correction_data->stiff,correction_data->state,
			 correction_data->meas[0],
			 correction_data->nknob,correction_data->nres,
			 tmp_corr,ic,ncorr);
	break;
      case 2:
	propose_solution_2(correction_data->res[0],
			   correction_data->norm,
			   correction_data->sig[0],
			   correction_data->stiff,
			   correction_data->state,
			   correction_data->meas[0],
			   correction_data->nknob,correction_data->nres,
			   tmp_corr,ic,ncorr);
	break;
      case 3:
	propose_solution(correction_data->res[0],correction_data->norm,
			 correction_data->sig[0],
			 correction_data->stiff,correction_data->state,
			 correction_data->meas[0],
			 correction_data->nknob,correction_data->nres,
			 tmp_corr,ic,ncorr);
	break;
      }
    }
    else {
    }
    for (i=0;i<correction_data->nknob;++i){
      correction_data->corr[i]=0.0;
    }
    for (i=0;i<ncorr;++i){
      correction_data->corr[ic[i]]=tmp_corr[i];
    }
    for (i=0;i<correction_data->nknob;++i){
      snprintf(buffer,100,"%g",correction_data->corr[i]*gain);
      Tcl_AppendElement(interp,buffer);
    }
  }
  else {
    switch (ret) {
    case 2:
      matrix_multiply(correction_data->res[0],correction_data->meas[0],
		      correction_data->corr,correction_data->nknob,
		      correction_data->nres,gain,
		      correction_data->est);
      for (i=0;i<correction_data->nres;++i){
	snprintf(buffer,100,"%g",correction_data->est[i]);
	Tcl_AppendElement(interp,buffer);
      }    
      break;
    case 3:
      matrix_multiply(correction_data->res[0],correction_data->meas[0],
		      correction_data->corr,correction_data->nknob,
		      correction_data->nres,gain,
		      correction_data->est);
      for (i=0;i<correction_data->nres;++i){
	snprintf(buffer,100,"%g",correction_data->est[i]+correction_data->meas[0][i]
		+correction_data->target[0][i]);
	Tcl_AppendElement(interp,buffer);
      }    
      break;
    }
  }
  //  correction_data->gain=gain;
  return TCL_OK;
}

int
Tcl_MicadoCorrectionInit(ClientData /*clientData*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  char **v,**v2;
  int i,n,j,m,m1,k,nmeas=1,nreal,ncorr=1,itype=0,use_wgt=1;
  CORRECTION_DATA *correction_data;
  double sum;
  char *init,*l_stiff=NULL,*l_sigma=NULL,*name,*l_state=NULL,
      *t=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-response",TK_ARGV_STRING,(char*)NULL,
     (char*)&init,
     (char*)"List of lists with the response matrices"},
    {(char*)"-correctors",TK_ARGV_INT,(char*)NULL,
     (char*)&ncorr,
     (char*)"Number of correctors to be used"},
    {(char*)"-stiff",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_stiff,
     (char*)"List of the stiffness of the knobs"},
    {(char*)"-sigma",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_sigma,
     (char*)"List of the weights for the optimisation"},
    {(char*)"-state",TK_ARGV_STRING,(char*)NULL,
     (char*)&l_state,
     (char*)"List of the state of the knobs"},
    {(char*)"-target",TK_ARGV_STRING,(char*)NULL,
     (char*)&t,
     (char*)"List of target values for the measurement values"},
    {(char*)"-type",TK_ARGV_INT,(char*)NULL,
     (char*)&itype,
     (char*)"type of correction (0: MICADO 1: mod. MICADO)"},
    {(char*)"-use_wgt",TK_ARGV_INT,(char*)NULL,
     (char*)&use_wgt,
     (char*)"If not zero use the weights to determine which correctors to use"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc>2){
    Tcl_SetResult(interp,"Too many arguments to <MicadoCorrectionInit>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<2){
    Tcl_SetResult(interp,"<MicadoCorrectionInit> requires a name",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  name=argv[1];

  if (nmeas!=1) {
    if(error=Tcl_SplitList(interp,init,&nreal,&v)) {
      placet_printf(INFO,"Cannot read list\n");
      return error;
    }
    if (nreal!=nmeas) {
      Tcl_SetResult(interp,
		    "The specified number of matrices does not agree with the real one",
		    TCL_VOLATILE);      
      return TCL_ERROR;
    }
  }
  else {
    if(error=Tcl_SplitList(interp,init,&n,&v)) {
      placet_printf(INFO,"Cannot read list\n");
      return error;
    }
    
    if(error=Tcl_SplitList(interp,v[0],&m1,&v2)) {
      Tcl_AppendResult(interp,"Cannot read sub-list",NULL);
      return error;
    }
  }

  correction_data=(CORRECTION_DATA*)malloc(sizeof(CORRECTION_DATA));
  correction_data->res=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->res[0]=(double*)malloc(sizeof(double)*n*m1);
  correction_data->norm=(double*)malloc(sizeof(double)*n);
  correction_data->stiff=(double*)malloc(sizeof(double)*n);
  correction_data->state=(double*)malloc(sizeof(double)*n);
  correction_data->corr=(double*)malloc(sizeof(double)*n);
  correction_data->est=(double*)malloc(sizeof(double)*m1);
  correction_data->sig=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->sig[0]=(double*)malloc(sizeof(double)*m1);
  correction_data->target=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->target[0]=(double*)malloc(sizeof(double)*m1);
  correction_data->indx=(int*)malloc(sizeof(int)*n);
  correction_data->nmeas=nmeas;
  correction_data->nknob=n;
  correction_data->nres=m1;
  correction_data->meas=(double**)malloc(sizeof(double*)*nmeas);
  correction_data->meas[0]=(double*)malloc(sizeof(double)*m1);
  correction_data->gain=1.0;

  correction_data->ncorr=ncorr;
  correction_data->itype=itype;
  correction_data->use_wgt=use_wgt;

  k=0;
  for (j=0;j<m1;++j) {
    if (error=Tcl_GetDouble(interp,v2[j],&(correction_data->res[0][k]))) {
      return error;
    }
    k++;
  }
  Tcl_Free((char*)v2);

  for (i=1;i<n;++i){
    if(error=Tcl_SplitList(interp,v[i],&m,&v2)) {
      Tcl_AppendResult(interp,"Cannot read sub-list",NULL);
      return error;
    }

    if (m!=m1) {
      Tcl_AppendResult(interp,"The matrix needs to be rectangular",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v2[j],&(correction_data->res[0][k]))) {
	return error;
      }
      k++;
    }
    Tcl_Free((char*)v2);
  }
  Tcl_Free((char*)v);
  if (l_stiff) {
    if(error=Tcl_SplitList(interp,l_stiff,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -stiff",NULL);
      return error;
    }
    if ((m!=n)&&(m!=1)) {
      Tcl_AppendResult(interp,"Number of stiffness values does not match",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->stiff+j)) {
	return error;
      }
    }
    if ((m==1)&&(n>1)) {
      for (i=1;i<n;++i){
	correction_data->stiff[i]=correction_data->stiff[0];
      }
    }
    Tcl_Free((char*)v);
  }
  else {
    for (i=0;i<n;++i) {
      correction_data->stiff[i]=0.0;
    }
  }
  if (t) {
    if(error=Tcl_SplitList(interp,t,&m,&v)) {
	Tcl_AppendResult(interp,"Cannot read list -target",NULL);
	return error;
    }
    if ((m!=m1)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->target[0]+j)) {
	return error;
      }
    }
  }
  else {
    for (j=0;j<m1;++j) {
	correction_data->target[0][j]=0.0;
    }
  }
  if (l_sigma) {
    if(error=Tcl_SplitList(interp,l_sigma,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -sigma",NULL);
      return error;
    }
    if ((m!=m1)&&(m!=1)) {
      Tcl_AppendResult(interp,"Number of weights does not match",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->sig[0]+j)) {
	return error;
      }
    }
    if ((m==1)&&(m1>1)) {
      for (i=1;i<m1;++i){
	correction_data->sig[0][i]=correction_data->sig[0][0];
      }
    }
    Tcl_Free((char*)v);
  } else {
    for (i=0;i<m1;++i) {
      correction_data->sig[0][i]=1.0;
    }
  }
  if (l_state) {
    if(error=Tcl_SplitList(interp,l_state,&m,&v)) {
      Tcl_AppendResult(interp,"Cannot read list -state",NULL);
      return error;
    }
    if ((m!=n)&&(m!=1)) {
      Tcl_AppendResult(interp,"",NULL);
      return TCL_ERROR;
    }
    for (j=0;j<m;++j) {
      if (error=Tcl_GetDouble(interp,v[j],correction_data->state+j)) {
	return error;
      }
    }
    if ((m==1)&&(n>1)) {
      for (i=1;i<n;++i){
	correction_data->state[i]=correction_data->state[0];
      }
    }
    Tcl_Free((char*)v);
  } else {
    for (i=0;i<n;++i) {
      correction_data->state[i]=0.0;
    }
  }
  k=0;
  if (correction_data->use_wgt) {
    for (i=0;i<n;++i) {
      sum=0.0;
      for (j=0;j<m1;++j) {
	sum+=correction_data->res[0][k]*correction_data->res[0][k]
	  *correction_data->sig[0][j]*correction_data->sig[0][j];
	++k;
      }
      correction_data->norm[i]=1.0/sqrt(sum);
    }
  }
  else {
    for (i=0;i<n;++i) {
      sum=0.0;
      for (j=0;j<m1;++j) {
	sum+=correction_data->res[0][k]*correction_data->res[0][k];
	++k;
      }
      correction_data->norm[i]=1.0/sqrt(sum);
    }
  }
  Tcl_CreateCommand(interp,name,Tcl_MicadoCorrectionApply,
		    (ClientData)correction_data,(Tcl_CmdDeleteProc*)NULL);
  return TCL_OK;
}

} // end of namespace

int
Correct_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"MatrixCorrectionInit",Tcl_MatrixCorrectionInit,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_CreateCommand(interp,"MicadoCorrectionInit",Tcl_MicadoCorrectionInit,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Correct","0.3");
  return TCL_OK;
}

