#include <stdlib.h>
#include <stdio.h>
#include <math.h>

main(int argc,char *argv[])
{
    char buffer[1024],*point;
    FILE *f,*fout;
    double e,x,y,xp,yp,z;
    double e0=251.6;
    int i=0;

    f=fopen("particles.out","r");
    fout=fopen("madtrack.in","w");
    if (argc>1) {
	e0=strtod(argv[1],NULL);
    }
   while(point=fgets(buffer,1024,f)) {
//	printf("%d\n",i); i++;
	e=strtod(point,&point);
	x=strtod(point,&point);
	y=strtod(point,&point);
	z=strtod(point,&point);
	xp=strtod(point,&point);
	yp=strtod(point,&point);

	fprintf(fout,"START, X=%g, PX=%g,&\n",x*1e-6,xp*1e-6*e/e0);
	fprintf(fout,"Y=%g, PY=%g,&\n",y*1e-6,yp*1e-6*e/e0);
	fprintf(fout,"t=%g, deltap=%g\n",-z*1e-6,(e-e0)/e0);

	/*
	fprintf(fout,"START, X=%g, PX=%g,&\n",x*1e-6,xp*1e-6);
	fprintf(fout,"Y=%g, PY=%g,&\n",y*1e-6,yp*1e-6);
	fprintf(fout,"t=%g, deltap=%g\n",-z*1e-6,(e-e0)/e0);
	*/
    }
    fclose(f);
    fclose(fout);
}
