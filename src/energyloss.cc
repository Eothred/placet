#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include <gsl/gsl_randist.h>

#include "placet.h"
#include "structures_def.h"
#include "girder.h"
#include "lattice.h"

#include "energyloss.h"

ENERGYLOSS::ENERGYLOSS(double _sigma ) : sigma(_sigma)
{
  rng = gsl_rng_alloc(gsl_rng_mt19937);
}

void ENERGYLOSS::step_4d_0(BEAM *bunch )
{
  for (int i=0;i<bunch->slices;i++) {
    double energy_loss = gsl_ran_gaussian(rng, sigma);
    if (energy_loss > 0) 	bunch->particle[i].energy -= energy_loss;
    else			bunch->particle[i].energy += energy_loss;
  }
}

/**********************************************************************/
/*                      Energy Loss                                   */
/**********************************************************************/

int tk_EnergyLoss(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			char *argv[])
{
  double sigma = 0.0;
  
  Tk_ArgvInfo table[]={
    {(char*)"-sigma",TK_ARGV_FLOAT,(char*)NULL,(char*)&sigma, (char*)"sigma energy [GeV]"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <EnergyLoss>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],error)) return TCL_ERROR;
  
  ENERGYLOSS *e=new ENERGYLOSS(sigma);
  inter_data.girder->add_element(e);
  return TCL_OK;
}
