#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "bin.h"
#include "beamline.h"
#include "cavity.h"
#include "bpm.h"
#include "girder.h"
#include "malloc.h"
#include "matrix.h"
#include "placeti3.h"
#include "placeti4.h"
#include "placeti5.h"
#include "random.hh"
#include "quadrupole.h"
#include "track.h"

extern BALLISTIC_DATA  ballistic_data;
extern ERRORS errors;
extern SURVEY_ERRORS_STRUCT survey_errors;

void ballistic_init()
{
  ballistic_data.do_rf=0;
  ballistic_data.do_atl=0;
  ballistic_data.measure_online=0;
  ballistic_data.ntn=1;
  ballistic_data.ntc=1;
  ballistic_data.nta=1;
  // Not really used at the moment:
  ballistic_data.offset_max=0.0;
  ballistic_data.ntn0=1;
  ballistic_data.nquad=12;
  ballistic_data.time=0.0;
  ballistic_data.gain=1.0;
  ballistic_data.last_quad=0.5;
}

void ballistic_set_atl(int do_atl,double time)
{
  ballistic_data.time=time;
  ballistic_data.do_atl=do_atl;
}

void ballistic_set(int ntn,int ntc,int nta,int do_rf,int nquad,
	      int measure_online,double gain)
{
  ballistic_data.do_rf=do_rf;
  ballistic_data.measure_online=measure_online;
  ballistic_data.ntn=ntn;
  ballistic_data.ntc=ntc;
  ballistic_data.nta=nta;
  // Not really used at the moment:
  ballistic_data.offset_max=0.0;
  ballistic_data.ntn0=ntn;
  ballistic_data.nquad=nquad;
  ballistic_data.gain=gain;
}

void
ballistic_correct(BEAMLINE *beamline,double quad[],double quad0[],
		  double position_x[],double position[],BIN **bin1,int nbin1,
		  BIN **bin2,int /*nbin2*/,BEAM *tb0,BEAM *tb1,BEAM *tb1_x,
		  BEAM *workbunch,BEAM *testbunch,int do_emitt,int do_log,
		  int measure_online,int no_acc)
{
  int j,ipos=0,ita,itn,itc,is;
  int do_jitter,do_position=0,nta,ntn,ntc,ntn0,do_field_error;
  double offset_max,jitter,field_error,field_error_zero_rms,tmp,tmp2;
  FILE *testit,*size_file;
  double eps=1e-300;

  if (ballistic_data.size_file){
    size_file=open_file(ballistic_data.size_file);
  }
  if (do_log){
    testit=open_file("testit.log");
  }
  ntn=ballistic_data.ntn;
  ntn0=ballistic_data.ntn0-2;
  ntc=ballistic_data.ntc;
  nta=ballistic_data.nta;
  offset_max=ballistic_data.offset_max;
  do_jitter=errors.do_jitter;
  jitter=errors.jitter_y;
  if (jitter<eps) {
    do_jitter=-do_jitter;
  }
  do_field_error=errors.do_field;
  field_error_zero_rms=errors.field_zero_rms;
  field_error=errors.field_res;
  if (position!=NULL) do_position=1;

  quadrupoles_set(beamline,quad);
  if (do_emitt) bunch_track_emitt_start();
  for (j=0;j<nbin1;j++){
    if (do_emitt){
      bunch_track_emitt(beamline,tb0,ipos,bin2[j]->start);
    }
    else{
      bunch_track(beamline,tb0,ipos,bin2[j]->start);
    }
    if(do_jitter<0){
      bunch_track_0(beamline,tb1,ipos,bin2[j]->start);
#ifdef TWODIM
      bunch_track_0(beamline,tb1_x,ipos,bin2[j]->start);
#endif
    }
    ipos=bin1[j]->start;
/* Loop over all correction steps */
    for (ita=0;ita<nta;ita++){
      if (do_position){
#ifdef TWODIM
	quadrupoles_bin_set_position_error_b_x(beamline,bin2[j],position_x,
					       position,1);
#else
	quadrupoles_bin_set_position_error_b(beamline,bin2[j],position,1);
#endif
      }
      if(do_field_error){
	quadrupoles_bin_set_error_b(beamline,bin2[j],quad0,quad,
				    field_error_zero_rms);
      }
      else{
	quadrupoles_bin_set_b(beamline,bin2[j],quad0,quad);
      }
      if (((measure_online==1)&&(ita==0))||(measure_online==2)){
	if(do_jitter){
	  //	  bin_response_b(beamline,tb0,tb1,testbunch,0,bin1[j]);
#ifdef TWODIM
       	  bin_response_b_calc_x(beamline,tb0,tb1,tb1_x,testbunch,0,bin1[j]);
#else
       	  bin_response_b_calc(beamline,tb0,tb1,testbunch,0,bin1[j]);
#endif
	}
	else{
	  //	  bin_response_b(beamline,tb0,NULL,testbunch,0,bin1[j]);
	  bin_response_b_calc(beamline,tb0,NULL,testbunch,0,bin1[j]);
	}
	bin_finish(bin1[j],0);
      }
      /* Ballistic correction loop */
      for (itn=0;itn<ntn;itn++){
	if (ballistic_data.size_file&&(itn==ntn-1)&&(ita==nta-1))
	  {
	    if(do_jitter){
	      if (do_jitter<0) {
#ifdef TWODIM
		bunch_join_x(tb0,tb1_x,tb1,Instrumentation.Gauss(),Instrumentation.Gauss(),workbunch);
#else
		bunch_join(tb0,tb1,Instrumentation.Gauss(),workbunch);
#endif
	      }
	      else {
		beam_copy(tb0,workbunch);
		tmp=Instrumentation.Gauss(jitter);
		for (is=0;is<tb0->slices;is++){
		  workbunch->particle[is].y+=tmp;
		}
	      }
	    }
	    else{
	      beam_copy(tb0,workbunch);
	    }
	    bin_measure(beamline,workbunch,0,bin1[j],true,no_acc);
	    
	    fprintf(size_file,"%d ",j);
#ifdef TWODIM
	    fprintf(size_file,"%g ",beam_rms_x(workbunch));
#endif
	    fprintf(size_file,"%g\n",beam_rms_y(workbunch));
	  }
	else
	  {
	    if(do_jitter){
	      if (do_jitter<0) {
#ifdef TWODIM
		bunch_join_0_x(tb0,tb1_x,tb1,Instrumentation.Gauss(),Instrumentation.Gauss(),workbunch);
#else
		bunch_join_0(tb0,tb1,Instrumentation.Gauss(),workbunch);
#endif
	      }
	      else {
		beam_copy(tb0,workbunch);
		tmp=Instrumentation.Gauss(jitter);
		for (is=0;is<tb0->slices;is++){
		  workbunch->particle[is].y+=tmp;
		}
	      }
	    }
	    else{
	      bunch_copy_0(tb0,workbunch);
	    }
	    bin_measure(beamline,workbunch,0,bin1[j],true,no_acc);
	  }
//include larger readerrors for ballistic beam
//	for (i=0;i<bin1[j]->nbpm;i++){
//	  bin1[j]->b0[i]+=Instrumentation.Gauss(2.0);
//	}

	if (do_log){
	  fprintf(testit,"BIN %d ITER %d NLC %d\n",j,ita,itn);
	  print_position_bin(testit,beamline,bin2[j]);
	}
	tmp=errors.quad_move_res;
	tmp2=errors.quad_step_size;
	errors.quad_move_res=0.0;
	errors.quad_step_size=0.0;
	bin_correct_step(beamline,0,bin1[j],ballistic_data.gain);
	errors.quad_move_res=tmp;
	errors.quad_step_size=tmp2;
	if ((fabs((beamline->element[bin1[j]->bpm[bin1[j]->nbpm-1]])->get_bpm_y_reading_exact())
	     <offset_max)&&(itn>ntn0)) break;
      }
      /* Switch on quadrupoles and reset positions */
      if (do_position){
#ifdef TWODIM
	quadrupoles_bin_set_position_error_b_x(beamline,bin2[j],position_x,
					       position,-1);
#else
	quadrupoles_bin_set_position_error_b(beamline,bin2[j],position,-1);
#endif
      }
      if (do_field_error){
	/* to be changed */
	quadrupoles_bin_set_error(beamline,bin2[j],quad,field_error);
      }
      else{
	quadrupoles_bin_set(beamline,bin2[j],quad);
      }
      /* Normal correction loop */
      for (itc=0;itc<ntc;itc++){
	/*
	  Maybe switch off
	 */
	if(do_jitter){
	  if (do_jitter<0) {
#ifdef TWODIM
	    bunch_join_0_x(tb0,tb1_x,tb1,Instrumentation.Gauss(),Instrumentation.Gauss(),workbunch);
#else
	    bunch_join_0(tb0,tb1,Instrumentation.Gauss(),workbunch);
#endif
	  }
	  else {
	    beam_copy(tb0,workbunch);
	    tmp=Instrumentation.Gauss(jitter);
	    tmp=0.0;
	    // XXX
	    for (is=0;is<tb0->slices;is++){
	      workbunch->particle[is].y+=tmp;
	    }
	  }
	}
	else{
	  bunch_copy_0(tb0,workbunch);
	}
	bin_measure(beamline,workbunch,0,bin2[j]);
	if (do_log){
	  fprintf(testit,"BIN %d ITER %d CLIC %d\n",j,ita,itc);
	  print_position_bin(testit,beamline,bin2[j]); 
	}
//errors.quad_move_res=0.5;
	bin_correct(beamline,0,bin2[j]);
//errors.quad_move_res=0.0;
      }
      if (do_log){
	fprintf(testit,"BIN %d ITER %d FINAL\n",j,ita);
	print_position_bin(testit,beamline,bin2[j]);
      }
    }
  }
  if (do_emitt){
    bunch_track_emitt(beamline,tb0,ipos,beamline->n_elements);
    bunch_track_emitt_end(tb0,beamline->get_length());
  }
  if (do_log){
    close_file(testit);
  }
  if (ballistic_data.size_file){
    close_file(size_file);
  }
}

double test_ballistic_2(BEAMLINE *beamline,double quad[],double quad0[],
			double quad_bump[],BEAM *bunch0,BEAM *probe_bump,
			BEAM *probe_all,int loop[],int btype[],int niter,
			int repeat,int no_acc,
			void (*survey)(BEAMLINE*),char *name,const char *format,char *logname)
{
  BEAM *tb0,*tb1,*tb1_x,*workbunch,*testbunch,*p_b=NULL,*p_a,*p_b1,*p_b2;
  BIN **bin1,**bin2,**bin_plot,**bin_fdbck;
  int nbin1,nbin2,nbin_plot,i,j,l;
  int do_log=0,i_rf,i_repeat;
  BUMP **bump;
  double e,esum=0.0,e_x,esum_x=0.0,time,help,dx,dy;
  double jitter,jitter_x,field_error,field_error_zero;
  int do_jitter=0,do_field_error=0,do_atl=0,do_rf=0;
  FILE *logfile;
  double *position,*position_x,position_error;
  int do_position,nbump=0;
  int measure_online;
  int do_emitt_ballist=0,do_emitt_bump=0,do_emitt_all=0,do_emitt_atl=0;

  do_rf=ballistic_data.do_rf;
  do_atl=ballistic_data.do_atl;
  time=ballistic_data.time;
  if (loop){
    while (loop[nbump]>=0) nbump++;
  }
  if (loop||do_atl){
    if (probe_bump==NULL) {
      probe_bump=bunch0;
    }
  }
  if (name!=NULL){
    if ((probe_bump==NULL)&&(probe_all==NULL)){
      do_emitt_ballist=1;
    }
    else{
      if (probe_all!=NULL){
	do_emitt_all=1;
      }
      else{
	do_emitt_bump=1;
      }
    }
  }
  bump=(BUMP**)xmalloc(sizeof(BUMP*)*nbump);
  bin_fdbck=(BIN**)xmalloc(sizeof(BIN*)*nbump);
  measure_online=ballistic_data.measure_online;
  do_jitter=errors.do_jitter;
  jitter=errors.jitter_y;
  jitter_x=errors.jitter_x;
  do_field_error=errors.do_field;
  field_error_zero=errors.field_zero;
  //  field_error_zero_rms=errors.field_zero_rms;
  field_error=errors.field_res;
  do_position=errors.do_position;
  if(do_position){
    position=(double*)alloca(sizeof(double)*beamline->n_quad);
#ifdef TWODIM
    position_x=(double*)alloca(sizeof(double)*beamline->n_quad);
#endif
  }
  else {
    position=NULL;
    position_x=NULL;
  }
  position_error=errors.position_error;

  placet_printf(INFO,"Test of ballistic correction\n");
  placet_printf(INFO,"No of iterations required %d\n",niter);
  placet_printf(INFO,"Number of iterations for ballistic step: %d\n",
	  ballistic_data.ntn);
  placet_printf(INFO,"Number of iterations for few-to-few: %d\n",
	  ballistic_data.ntc);
  placet_printf(INFO,"Method to determine response coefficients: ");
  switch(ballistic_data.measure_online){
  case -1:
    placet_printf(INFO,"Without wakefields\n");
    break;
  case 0:
    placet_printf(INFO,"Perfect knowledge\n");
    break;
  case 1:
    placet_printf(INFO,"Measure for first case\n");
    break;
  case 2:
    placet_printf(INFO,"Measure for each case\n");
    break;
  }
  if (do_field_error){
    placet_printf(INFO,"field errors are included (type %d)\n",do_field_error);
    placet_printf(INFO,"relative field error for quadrupoles switched on: %g\n",
	   field_error);
    if (do_field_error==1){
      placet_printf(INFO,"relative field error for quadrupoles switched off: %g\n",
	     field_error_zero);
    }
    else{
      placet_printf(INFO,"absolute field error for quadrupoles switched off: %g [T/m]\n",
	     field_error_zero/0.3);
    }
  }
  else{
    placet_printf(INFO,"field errors are not included\n");
  }
  if (do_jitter){
    placet_printf(INFO,"assumed beam jitter during correction: %g [micro m]\n",jitter);
  }
  else{
    placet_printf(INFO,"no beam jitter assumed\n");
  }
   
  if (logname!=NULL) logfile=open_file(logname);
/* Prepare memory */
  bin1=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  bin2=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  bin_plot=(BIN**)xmalloc(sizeof(BIN*)*MAX_BIN);
  beamline_bin_divide_ballistic(beamline,ballistic_data.nquad,0,bin1,&nbin1);
  beamline_bin_divide(beamline,ballistic_data.nquad,0,bin2,&nbin2);
  if (probe_bump){
    beamline_bin_divide_1(beamline,1,0,bin_plot,&nbin_plot);
  }
  else{
    beamline_bin_divide(beamline,1,0,bin_plot,&nbin_plot);
  }
  tb0=bunch_remake(bunch0);
  tb1_x=NULL;
  if(do_jitter){
    tb1=bunch_remake(bunch0);
#ifdef TWODIM
    tb1_x=bunch_remake(bunch0);
#endif
  }
  else{
    tb1=NULL;
  }
  workbunch=bunch_remake(bunch0);
  testbunch=bunch_remake(bunch0);
  if (probe_bump){
    p_b=bunch_remake(probe_bump);
    p_b1=bunch_remake(probe_bump);
    p_b2=bunch_remake(probe_bump);
  }
  if (probe_all){
    p_a=bunch_remake(probe_all);
  }
  beam_copy(bunch0,tb0);
  placet_printf(INFO,"filling bin2\n");
  // scd 2000
  //beamline_set_zero(beamline);
  simple_bin_fill(beamline,bin2,nbin2,tb0,workbunch);
  if (probe_bump==NULL){
    // now
    quadrupoles_set(beamline,quad_bump);
    beam_copy(bunch0,tb0);
    simple_bin_fill(beamline,bin_plot,nbin_plot,tb0,workbunch);
    // now
    quadrupoles_set(beamline,quad);
  }
  else{
    // now
    quadrupoles_set(beamline,quad_bump);
    beam_copy(probe_bump,p_b);
    simple_bin_fill(beamline,bin_plot,nbin_plot,p_b,p_b1);
    // now
    quadrupoles_set(beamline,quad);
  }

  if (loop){
    // now    
    quadrupoles_set(beamline,quad_bump);
    bump_prepare_types(beamline,bin_plot,nbin_plot,bump,loop,btype,probe_bump,
		       p_b,p_b1,p_b2);
    for (i=0;i<nbump;i++){
      bin_fdbck[i]=bump[i]->feedback;
    }
    // now
    quadrupoles_set(beamline,quad);
  }
  if (do_emitt_ballist||do_emitt_bump||do_emitt_all){
    emitt_store_init(beamline->n_quad);
  }

  for (j=0;j<beamline->n_quad;j++){
    quad0[j]=0.0;
  }
  /* to be changed */
  /*
  for (j=0;j<nbin1;j++){
    k=beamline->element[bin1[j]->quad[0]]->number;
    quad0[k]=0.5*quad[k];
  }
  */
  beam_copy(bunch0,tb0);
  if (measure_online<=0){
    quadrupoles_set(beamline,quad);
    if (measure_online<0) wakefield_init(1,0);
    beamline_set_zero(beamline);
    placet_printf(INFO,"filling bin1\n");
    placet_printf(INFO,"%d\n",nbin1);
    //    simple_bin_fill(beamline,bin1,nbin1,tb0,workbunch);
    simple_bin_fill_b(beamline,quad0,quad,bin1,bin2,nbin1,tb0,workbunch);
    if (measure_online<0) wakefield_init(1,1);
  }

  for (i=0;i<niter;i++){
    /*
      misalign structures
     */
    cav_scatter(beamline,survey_errors.cav_error_realign,
		survey_errors.cav_error_realign_x);
/*
/   Prepare constant remnant field errors.
/   The first quadrupoles of each bin has no errors.
*/
    switch (do_field_error){
    case 0: // off
      for (j=0;j<beamline->n_quad;j++){
	quad0[j]=0.0;
      }
      break;
    case 1: // relative
      for (j=0;j<beamline->n_quad;j++){
	quad0[j]=quad[j]*(2.0*Default.Uniform()-1.0)*field_error_zero;
      }
      break;
    case 2: // absolute
      for (j=0;j<beamline->n_quad;j++){
	if (quad[j]>0.0){
	  quad0[j]=2.0*Default.Uniform()*field_error_zero;
	}
	else{
	  quad0[j]=-2.0*Default.Uniform()*field_error_zero;
	}
      }
      break;
    case 3: // relative, no randomisation, not documented
      for (j=0;j<beamline->n_quad;j++){
	quad0[j]=quad[j]*field_error_zero;
      }
      break;
    }
    /* to be changed */
    /*
    for (j=0;j<nbin1;j++){
      k=beamline->element[bin1[j]->quad[0]]->number;
      quad0[k]=quad[k];
    }
    */
    if (do_position){
      errors_fill(position,beamline->n_quad,position_error,0.0);
#ifdef TWODIM
      errors_fill(position_x,beamline->n_quad,position_error,0.0);
#endif
      /* to be changed */
      /*
      for (j=0;j<nbin1;j++){
	position[beamline->element[bin1[j]->quad[0]]->number]=0.0;
      }
      */
    }

//    errors.quad_move_res=0.0;
    survey(beamline);
    dy=errors.offset_y*Instrumentation.Gauss();
    dx=errors.offset_x*Instrumentation.Gauss();

    for (i_repeat=0;i_repeat<repeat;i_repeat++){
      beam_copy(bunch0,tb0);
      beam_add_offset_y(tb0,dy);
      beam_add_offset_x(tb0,dx);
      if(do_jitter){
	bunch_copy_0(tb0,tb1);
	beam_add_offset_y(tb1,errors.jitter_y);
#ifdef TWODIM
	bunch_copy_0(tb0,tb1_x);
	beam_add_offset_x(tb1,errors.jitter_x);
#endif
      }
    /* The actual correction */
      switch(measure_online){
      case -1:
      case 0:
	ballistic_correct(beamline,quad,quad0,position_x,position,bin1,nbin1,
			  bin2,nbin2,tb0,tb1,tb1_x,workbunch,testbunch,
			  do_emitt_ballist,do_log,0,no_acc);
	break;
      case 1:
	if (i==0){
	  ballistic_correct(beamline,quad,quad0,position_x,position,bin1,nbin1,
			    bin2,nbin2,tb0,tb1,tb1_x,workbunch,testbunch,
			    do_emitt_ballist,do_log,1,no_acc);
	}
	else{
	  ballistic_correct(beamline,quad,quad0,position_x,position,bin1,nbin1,
			    bin2,nbin2,tb0,tb1,tb1_x,workbunch,testbunch,
			    do_emitt_ballist,do_log,0,no_acc);
	}
	break;
      case 2:
	ballistic_correct(beamline,quad,quad0,position_x,position,bin1,nbin1,
			  bin2,nbin2,tb0,tb1,tb1_x,workbunch,testbunch,
			  do_emitt_ballist,do_log,1,no_acc);
	break;
      case 3:
	ballistic_correct(beamline,quad,quad0,position_x,position,bin1,nbin1,
			  bin2,nbin2,tb0,tb1,tb1_x,workbunch,testbunch,
			  do_emitt_ballist,do_log,2,no_acc);
	break;
      }
      /* Do RF-alignment if required */

      if (do_rf) {
	for (i_rf=0;i_rf<do_rf;i_rf++){
	  beam_copy(bunch0,tb0);
	  beam_add_offset_y(tb0,dy);
	  beam_add_offset_x(tb0,dx);
	  align_rf(beamline,bin_plot,nbin_plot,tb0,workbunch,testbunch,NULL);
	  placet_printf(INFO,"emitt_inter %g\n",emitt_y(tb0));
	}
      }
    }
/* Do bump correction if required */
    if (loop){
// now
    quadrupoles_set(beamline,quad_bump);
      beam_copy(probe_bump,p_b);
      beam_add_offset_y(p_b,dy);
      beam_add_offset_x(p_b,dx);
//      errors.quad_move_res=0.0;
      bump_correct(beamline,bin_plot,nbin_plot,bump,loop,p_b,p_b1,p_b2,
			do_emitt_bump);
/*
	  bump_correct(beamline,bin_plot,nbin_plot,bump,loop,p_b,p_b1,p_b2,
		       do_emitt_bump);
      */

// now
    quadrupoles_set(beamline,quad);
    }
/* Do time dependence */
    if(do_atl){
      beamline_move_atl(beamline,0.5e-6,time,true);
      switch(do_atl){
      case 1:
	break;
      case 2:
	help=errors.bpm_resolution;
	errors.bpm_resolution=0.0;
	if (probe_bump!=NULL){
// now
	  quadrupoles_set(beamline,quad_bump);
	  beam_copy(probe_bump,p_b);
	  beam_add_offset_y(p_b,dy);
	  beam_add_offset_x(p_b,dx);
	  simple_correct(beamline,bin_fdbck,nbump,p_b,p_b1);
// now
	  quadrupoles_set(beamline,quad);
	}
	else{
	  beam_copy(bunch0,tb0);
	  beam_add_offset_y(tb0,dy);
	  beam_add_offset_x(tb0,dx);
// now
	  quadrupoles_set(beamline,quad_bump);
	  feedback_correct_3(beamline,bin_plot,nbin_plot,tb0,workbunch,
			     bin_plot[nbin_plot-1]->s_pos/(double)(nbump+1));
// now
	  quadrupoles_set(beamline,quad);
	}
	errors.bpm_resolution=help;
	break;
      case 3:
	beam_copy(probe_bump,p_b);
	beam_add_offset_y(p_b,dy);
	beam_add_offset_x(p_b,dx);
	placet_printf(INFO,"quad_move_res %g %g\n",errors.quad_move_res,
	       errors.quad_step_size);
// now
        quadrupoles_set(beamline,quad_bump);
	simple_correct(beamline,bin_plot,nbin_plot,p_b,p_b1);
// now
	quadrupoles_set(beamline,quad);
	break;
      case 4:	
	beam_copy(probe_bump,p_b);
	beam_add_offset_y(p_b,dy);
	beam_add_offset_x(p_b,dx);
// now
        quadrupoles_set(beamline,quad_bump);
	bump_correct(beamline,bin_plot,nbin_plot,bump,loop,p_b,p_b1,p_b2,
		     do_emitt_atl);
/*
	bump_correct_long(beamline,bin_plot,nbin_plot,bump,loop,p_b,p_b1,p_b2,
			  do_emitt_atl);

*/
// now
	quadrupoles_set(beamline,quad_bump);
	break;
      case 5:
	beam_copy(bunch0,tb0);
	beam_add_offset_y(tb0,dy);
	beam_add_offset_x(tb0,dx);
	if(do_jitter){
	  bunch_copy_0(bunch0,tb1);
	  for (l=0;l<tb1->slices;l++){
	    tb1->particle[l].y+=jitter;
	  }
#ifdef TWODIM
	  bunch_copy_0(bunch0,tb1_x);
	  for (l=0;l<tb1->slices;l++){
	    tb1_x->particle[l].x+=jitter_x;
	  }
#endif
	}
	ballistic_correct(beamline,quad,quad0,position_x,position,bin1,nbin1,
			  bin2,nbin2,tb0,tb1,tb1_x,workbunch,testbunch,
			  do_emitt_ballist,do_log,0,no_acc);
/* Do RF-alignment if required */
	if (do_rf){
	  beam_copy(bunch0,tb0);
	  beam_add_offset_y(tb0,dy);
	  beam_add_offset_x(tb0,dx);
	  align_rf(beamline,bin_plot,nbin_plot,tb0,workbunch,testbunch,NULL);
	}
/* Do bump correction if required */
	if (probe_bump!=NULL){
	  beam_copy(probe_bump,p_b);
	  beam_add_offset_y(p_b,dy);
	  beam_add_offset_x(p_b,dx);
// now
	  quadrupoles_set(beamline,quad_bump);
//	  errors.quad_move_res=0.0;
	  bump_correct(beamline,bin_plot,nbin_plot,bump,loop,p_b,p_b1,p_b2,
		       do_emitt_bump);
/*
	  bump_correct_long(beamline,bin_plot,nbin_plot,bump,loop,p_b,p_b1,p_b2,
			    do_emitt_bump);
*/
// now
          quadrupoles_set(beamline,quad);
	}
	break;
      }
    }

/* Test emittance growth of probe beam if required */ 
    if (probe_all!=NULL){
      beam_copy(probe_all,p_a);
      beam_add_offset_y(p_a,dy);
      beam_add_offset_x(p_a,dx);
// now
      quadrupoles_set(beamline,quad_bump);
      if (do_emitt_all){
        bunch_track_emitt_start();
	bunch_track_emitt(beamline,p_a,0,beamline->n_elements);
	bunch_track_emitt_end(p_a,beamline->get_length());
      }
      else{
	bunch_track(beamline,p_a,0,beamline->n_elements);
      }
// now
      quadrupoles_set(beamline,quad);
    }
//scd tmp
    // if (niter==1){
    //   if (probe_all){
    // 	placet_printf(INFO,"save 3\n");
    // 	bunch_save("bunchlong.dat",p_a);
    // 	bunch_test_plot(p_a,"be.dat");
    //   }
    //   else{
    // 	if (p_b){
    // 	  placet_printf(INFO,"save 1\n");
    // 	  bunch_save("bunchlong.dat",p_b);
    // 	}
    // 	else{
    // 	  placet_printf(INFO,"save 2\n");
    // 	  bunch_save("bunchlong.dat",tb0);
    // 	}
    //   }
    // }
/* Collect information */
    e=emitt_y(tb0);
    if (probe_bump!=NULL) e=emitt_y(p_b);
    if (probe_all!=NULL) e=emitt_y(p_a);
    esum+=e;
#ifdef TWODIM
    e_x=emitt_x(tb0);
    if (probe_bump!=NULL) e_x=emitt_x(p_b);
    if (probe_all!=NULL) e_x=emitt_x(p_a);
    esum_x+=e_x;
#endif
    if (logname!=NULL) {
      fprintf(logfile,"%g\n",e);
      fflush(logfile);
    }
    placet_printf(INFO,"%d ",i);
#ifdef TWODIM
    placet_printf(INFO,"%g %g ",e_x,esum_x/(double)(i+1));
#endif
    placet_printf(INFO,"%g %g\n",e,esum/(double)(i+1));
  }
  /* Save results */
  emitt_print(name,format);
  if (logname!=NULL) close_file(logfile);

/* Free memory */
  if (probe_all!=NULL){
    beam_delete(p_a);
  }
  if (probe_bump!=NULL){
    beam_delete(p_b2);
    beam_delete(p_b1);
    beam_delete(p_b);
  }
  beam_delete(testbunch);
  beam_delete(workbunch);
  if(do_jitter){
#ifdef TWODIM
    beam_delete(tb1_x);
#endif
    beam_delete(tb1);
  }
  beam_delete(tb0);
  for (i=0;i<nbin_plot;i++){
    bin_delete(bin_plot[i]);
  }
  for (i=0;i<nbin2;i++){
    bin_delete(bin2[i]);
  }
  for (i=0;i<nbin1;i++){
    bin_delete(bin1[i]);
  }
  free(bin_plot);
  free(bin2);
  free(bin1);
  free(bump);
  free(bin_fdbck);
  return esum/(double)(niter);
}

int fill_factor(BEAMLINE *beamline,char *name)
{
  int i,n;
  double c_sum=0.0,t_sum=0.0,t_step=0.0,c_step=0.0;
  FILE *file;
  if (name){
    file=open_file(name);
  }
  n=beamline->n_elements;
  for (i=0;i<n;i++){
    ELEMENT *element=beamline->element[i];
    if (element->is_cavity() || element->is_cavity_pets()) {
      c_sum+=beamline->element[i]->get_length();
      t_sum+=beamline->element[i]->get_length();
      c_step+=beamline->element[i]->get_length();
      t_step+=beamline->element[i]->get_length();
    } else if (element->is_quad()) {
      if (t_sum>0.0){
	if (name){
	  fprintf(file,"%g %g %g\n",t_sum,c_sum/t_sum,c_step/t_step);
	}
	else{
	  placet_printf(INFO,"%g %g %g\n",t_sum,c_sum/t_sum,c_step/t_step);
	}
      }
      t_sum+=beamline->element[i]->get_length();
      t_step=beamline->element[i]->get_length();
      c_step=0.0;
    } else {
      t_sum+=beamline->element[i]->get_length();
      t_step+=beamline->element[i]->get_length();
    }
  }
  if (t_sum>0.0){
    if (name){
      fprintf(file,"%g %g %g\n",t_sum,c_sum/t_sum,c_step/t_step);
    }
    else{
      placet_printf(INFO,"%g %g %g\n",t_sum,c_sum/t_sum,c_step/t_step);
    }
  }
  if (name){
    close_file(file);
  }
  return 0;
}

int divide_linac(BEAMLINE *beamline,char *name,int ncav,double length)
{
  int i,n,k=0,l=0;
  double c_sum=0.0,t_sum=0.0,t_old=0.0;
  FILE *file;
  file=open_file(name);
  n=beamline->n_elements;
  for (i=0;i<n;i++){
    ELEMENT *element=beamline->element[i];
    if (element->is_cavity() || element->is_cavity_pets()) {
      c_sum+=beamline->element[i]->get_length();
      t_sum+=beamline->element[i]->get_length();
      k++;
      if (k==ncav){
	k=0;
	placet_fprintf(INFO,file,"%d %g %g %g %g\n",l,t_sum,t_sum-t_old,
		       t_sum-(l+1)*length,t_sum/(double)(l+1));
	t_old=t_sum;
	l++;
      }
    } else {
      t_sum+=beamline->element[i]->get_length();
    }
  }
  placet_fprintf(INFO,file,"%d %g %g %g %g\n",l,t_sum,t_sum-t_old,
		 t_sum-(l+1)*length,t_sum/(double)(l+1));
  close_file(file);
  return 0;
}

int divide_linac2(BEAMLINE *beamline,char *name,int /*ncav*/,double length)
{
  int i,n,k=0,l=0;
  double c_sum=0.0,t_sum=0.0;
  FILE *file;
  file=open_file(name);
  n=beamline->n_elements;
  for (i=0;i<n;i++){
    ELEMENT *element=beamline->element[i];
    if (element->is_cavity() || element->is_cavity_pets())
      {
	c_sum+=element->get_length();
	t_sum+=element->get_length();
	k++;
      } else {
      t_sum+=element->get_length();
    }

    if (t_sum>=length){
      placet_fprintf(INFO,file,"%d %d %g\n",l,k,c_sum);
      t_sum-=length;
      l++;
      k=0;
    }
  }
  placet_fprintf(INFO,file,"%d %d %g\n",l,k,c_sum);
  close_file(file);
  return 0;
}

/* The same routine as beam_position_store (in obsolete) but takes the average position and angle out */

void beam_position_store_2(BEAM *beam,char *name)
{
  FILE *file;
  int i,j,n,nb,m=0;
  double y=0.0,yp=0.0,wgt=0.0,x=0.0,xp=0.0;
  
  n=beam->slices/beam->bunches;
  nb=beam->bunches;
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
      wgt+=beam->particle[m].wgt;
      y+=beam->particle[m].y*beam->particle[m].wgt;
      yp+=beam->particle[m].yp*beam->particle[m].wgt;
#ifdef TWODIM
      x+=beam->particle[m].x*beam->particle[m].wgt;
      xp+=beam->particle[m].xp*beam->particle[m].wgt;
#endif
      m++;
    }
  }
  y/=wgt;
  yp/=wgt;
  y=0.0;
  yp=0.0;
#ifdef TWODIM
  x/=wgt;
  xp/=wgt;
  x=0.0;
  xp=0.0;
#endif
  file=open_file(name);
  m=0;
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
#ifdef TWODIM
      placet_fprintf(INFO,file,"%g %g %g %g %g\n",
		     beam->particle[m].energy,
		     (beam->particle[m].x-x)/sqrt(beam->sigma_xx[m].r11),
		     (beam->particle[m].xp-xp)/sqrt(beam->sigma_xx[m].r22),
		     (beam->particle[m].y-y)/sqrt(beam->sigma[m].r11),
		     (beam->particle[m].yp-yp)/sqrt(beam->sigma[m].r22));
#else
      placet_fprintf(INFO,file,"%g %g %g %g\n",
		     beam->particle[m].energy,
		     (beam->particle[m].y-y)/sqrt(beam->sigma[m].r11),
		     (beam->particle[m].yp-yp)/sqrt(beam->sigma[m].r22),
		     beam->sigma[m].r12/sqrt(beam->sigma[m].r11*beam->sigma[m].r22-
					     beam->sigma[m].r12*beam->sigma[m].r21));
#endif
      m++;
    }
    placet_fprintf(INFO,file,"\n");
  }
  close_file(file);
}

void beam_position_store_absolute(BEAM *beam,char *name)
{
  FILE *file;
  int i,j,n,nb,m=0;
  double y=0.0,yp=0.0,x=0.0,xp=0.0;
  
  n=beam->slices/beam->bunches;
  nb=beam->bunches;
  y=0.0;
  yp=0.0;
#ifdef TWODIM
  x=0.0;
  xp=0.0;
#endif
  file=open_file(name);
  m=0;
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
#ifdef TWODIM
      placet_fprintf(INFO,file,"%g %g %g %g %g %g %g %g\n",
		     beam->particle[m].energy,
		     (beam->particle[m].x-x),
		     (beam->particle[m].xp-xp),
		     (beam->particle[m].y-y),
		     (beam->particle[m].yp-yp),
		     beam->sigma[m].r11,
		     beam->sigma[m].r12,
		     beam->sigma[m].r22);
#else
      placet_fprintf(INFO,file,"%g %g %g %g\n",
		     beam->particle[m].energy,
		     (beam->particle[m].y-y),
		     (beam->particle[m].yp-yp),
		     beam->sigma[m].r12);
#endif
      m++;
    }
    placet_fprintf(INFO,file,"\n");
  }
  close_file(file);
}

void beam_save_all(BEAM *beam,char *name,int head,int axis)
{
  FILE *file;
  int i,j,k,n,nb,nm,m=0;
  double y0=0.0,yp0=0.0,wgt0=0.0,x0=0.0,xp0=0.0;
  
  n=beam->slices/beam->bunches;
  n/=beam->macroparticles;
  nm=beam->macroparticles;
  nb=beam->bunches;
  file=open_file(name);
  m=0;
  x0=0.0;
  xp0=0.0;
  y0=0.0;
  yp0=0.0;
  if (axis) {
    for (i=0;i<beam->slices;i++){
      x0+=beam->particle[i].wgt*beam->particle[i].x;
      xp0+=beam->particle[i].wgt*beam->particle[i].xp;
      y0+=beam->particle[i].wgt*beam->particle[i].y;
      yp0+=beam->particle[i].wgt*beam->particle[i].yp;
      wgt0+=beam->particle[i].wgt;
    }
    x0/=wgt0;
    xp0/=wgt0;
    y0/=wgt0;
    yp0/=wgt0;
  }
  if (head) {
    fprintf(file,"%d %d %d\n",nb,n,nm);
  }
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
      for (k=0;k<nm;k++){
	placet_fprintf(INFO,file,"%.15g %.15g %.15g ",beam->z_position[j*n+i],
		       beam->particle[m].wgt,
		       beam->particle[m].energy);
#ifdef TWODIM
	placet_fprintf(INFO,file,"%.15g %.15g ",
		       beam->particle[m].x-x0,
		       beam->particle[m].xp-xp0);
#endif
	placet_fprintf(INFO,file,"%.15g %.15g ",
		       beam->particle[m].y-y0,
		       beam->particle[m].yp-yp0);
#ifdef TWODIM
	placet_fprintf(INFO,file,"%.15g %.15g %.15g ",
		       beam->sigma_xx[m].r11,
		       beam->sigma_xx[m].r12,
		       beam->sigma_xx[m].r22);
#endif
	placet_fprintf(INFO,file,"%.15g %.15g %.15g",
		       beam->sigma[m].r11,
		       beam->sigma[m].r12,
		       beam->sigma[m].r22);
#ifdef TWODIM
	placet_fprintf(INFO,file," %.15g %.15g %.15g %.15g",	
		       beam->sigma_xy[m].r11,
		       beam->sigma_xy[m].r12,
		       beam->sigma_xy[m].r21,
		       beam->sigma_xy[m].r22);
#endif
	placet_fprintf(INFO,file,"\n");
	m++;
      }
    }
    placet_fprintf(INFO,file,"\n");
  }
  close_file(file);
}

//_________________________________________________________________
void beam_save_all_bunches(BEAM *beam,char *name,int head,int axis)
{
  char fname[100];
  int i,j,k,n,nb,nm,m=0;
  double y0=0.0,yp0=0.0,wgt0=0.0,x0=0.0,xp0=0.0;
  
  n=beam->slices/beam->bunches;
  n/=beam->macroparticles;
  nm=beam->macroparticles;
  nb=beam->bunches;
  m=0;
  x0=0.0;
  xp0=0.0;
  y0=0.0;
  yp0=0.0;
  if (axis) {
    for (i=0;i<beam->slices;i++){
      x0+=beam->particle[i].wgt*beam->particle[i].x;
      xp0+=beam->particle[i].wgt*beam->particle[i].xp;
      y0+=beam->particle[i].wgt*beam->particle[i].y;
      yp0+=beam->particle[i].wgt*beam->particle[i].yp;
      wgt0+=beam->particle[i].wgt;
    }
    x0/=wgt0;
    xp0/=wgt0;
    y0/=wgt0;
    yp0/=wgt0;
  }
  for (j=0;j<nb;j++){
    FILE *file;
    if (name){
      snprintf(fname,100,"%s.%d",name,j);
      file=open_file(fname);
    }
    else{
      file=stdout;
    }
    if (head) {
      placet_fprintf(INFO,file,"%d %d %d %d\n",nb,n,nm,j);
    }
    for (i=0;i<n;i++){
      for (k=0;k<nm;k++){
 	placet_fprintf(INFO,file,"%.15g %.15g %.15g ",beam->z_position[j*n+i],
		       beam->particle[m].wgt,
		       beam->particle[m].energy);
#ifdef TWODIM
 	placet_fprintf(INFO,file,"%.15g %.15g ",
		       beam->particle[m].x-x0,
		       beam->particle[m].xp-xp0);
#endif
 	placet_fprintf(INFO,file,"%.15g %.15g ",
		       beam->particle[m].y-y0,
		       beam->particle[m].yp-yp0);
#ifdef TWODIM
 	placet_fprintf(INFO,file,"%.15g %.15g %.15g ",
		       beam->sigma_xx[m].r11,
		       beam->sigma_xx[m].r12,
		       beam->sigma_xx[m].r22);
#endif
 	placet_fprintf(INFO,file,"%.15g %.15g %.15g",
		       beam->sigma[m].r11,
		       beam->sigma[m].r12,
		       beam->sigma[m].r22);
#ifdef TWODIM
 	placet_fprintf(INFO,file," %.15g %.15g %.15g %.15g",	
		       beam->sigma_xy[m].r11,
		       beam->sigma_xy[m].r12,
		       beam->sigma_xy[m].r21,
		       beam->sigma_xy[m].r22);
#endif
 	placet_fprintf(INFO,file,"\n");
 	m++;
      }
    }
    placet_fprintf(INFO,file,"\n");
    close_file(file);
  }
}

void beam_save_all_bin(BEAM *beam,char *name,int head,int axis)
{
  FILE *file;
  int i,j,k,n,nb,nm,m=0;
  double y0=0.0,yp0=0.0,wgt0=0.0,x0=0.0,xp0=0.0;
  double buffer[17];
  
  n=beam->slices/beam->bunches;
  n/=beam->macroparticles;
  nm=beam->macroparticles;
  nb=beam->bunches;
  file=open_file(name);
  m=0;
  x0=0.0;
  xp0=0.0;
  y0=0.0;
  yp0=0.0;
  if (axis) {
    for (i=0;i<beam->slices;i++){
      x0+=beam->particle[i].wgt*beam->particle[i].x;
      xp0+=beam->particle[i].wgt*beam->particle[i].xp;
      y0+=beam->particle[i].wgt*beam->particle[i].y;
      yp0+=beam->particle[i].wgt*beam->particle[i].yp;
      wgt0+=beam->particle[i].wgt;
    }
    x0/=wgt0;
    xp0/=wgt0;
    y0/=wgt0;
    yp0/=wgt0;
  }
  if (head) {
    placet_fprintf(INFO,file,"%d %d %d\n",nb,n,nm);
  }
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
      for (k=0;k<nm;k++){
	buffer[0]=beam->z_position[j*n+i];
	buffer[1]=beam->particle[m].wgt;
	buffer[2]=beam->particle[m].energy;
#ifdef TWODIM
	buffer[3]=beam->particle[m].x-x0;
	buffer[4]=beam->particle[m].xp-xp0;
	buffer[5]=beam->particle[m].y-y0;
	buffer[6]=beam->particle[m].yp-yp0;
	buffer[7]=beam->sigma_xx[m].r11;
	buffer[8]=beam->sigma_xx[m].r12;
	buffer[9]=beam->sigma_xx[m].r22;
	buffer[10]=beam->sigma[m].r11;
	buffer[11]=beam->sigma[m].r12;
	buffer[12]=beam->sigma[m].r22;
	buffer[13]=beam->sigma_xy[m].r11;
	buffer[14]=beam->sigma_xy[m].r12;
	buffer[15]=beam->sigma_xy[m].r21;
	buffer[16]=beam->sigma_xy[m].r22;
	fwrite(buffer,sizeof(double),17,file);
#else
	buffer[3]=beam->particle[m].y-y0;
	buffer[4]=beam->particle[m].yp-yp0;
	buffer[5]=beam->sigma[m].r11;
	buffer[6]=beam->sigma[m].r12;
	buffer[7]=beam->sigma[m].r22;
	fwrite(buffer,sizeof(double),8,file);
#endif
	m++;
      }
    }
    placet_fprintf(INFO,file,"\n");
  }
  close_file(file);
}

void beam_load_all(BEAM *beam,char *name,int head,int axis,int read_structure)
{
  FILE *file;
  int i,j,k,n,nb,nm,m=0,ntest;
  char buffer[1024],*point;
  double x0=0.0,xp0=0.0,y0=0.0,yp0=0.0,wgt0=0.0;
  
  n=beam->slices/beam->bunches;
  n/=beam->macroparticles;
  nm=beam->macroparticles;
  nb=beam->bunches;
  file=read_file(name);
  
  if (head) {
    point=fgets(buffer,1024,file);
    ntest=strtol(point,&point,10);
    if (ntest!=nb) exit(-1);
    ntest=strtol(point,&point,10);
    if (ntest!=n) exit(-1);
    ntest=strtol(point,&point,10);
    if (ntest!=nm) exit(-1);
  }
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
      for (k=0;k<nm;k++){
	point=fgets(buffer,1024,file);
	if (read_structure) {
	  beam->z_position[j*n+i]=strtod(point,&point);
	  beam->particle[m].wgt=strtod(point,&point);
	}
	else {
	  strtod(point,&point);
	  strtod(point,&point);
	}
	beam->particle[m].energy=strtod(point,&point);
#ifdef TWODIM
	beam->particle[m].x=strtod(point,&point);
	beam->particle[m].xp=strtod(point,&point);
#endif
	beam->particle[m].y=strtod(point,&point);
	beam->particle[m].yp=strtod(point,&point);
#ifdef TWODIM
	beam->sigma_xx[m].r11=strtod(point,&point);
	beam->sigma_xx[m].r12=strtod(point,&point);
	beam->sigma_xx[m].r21=beam->sigma_xx[m].r12;
	beam->sigma_xx[m].r22=strtod(point,&point);
#endif
	beam->sigma[m].r11=strtod(point,&point);
	beam->sigma[m].r12=strtod(point,&point);
	beam->sigma[m].r21=beam->sigma[m].r12;
	beam->sigma[m].r22=strtod(point,&point);

#ifdef TWODIM
	beam->sigma_xy[m].r11=strtod(point,&point);
	beam->sigma_xy[m].r12=strtod(point,&point);
	beam->sigma_xy[m].r21=strtod(point,&point);
	beam->sigma_xy[m].r22=strtod(point,&point);
#endif
	m++;
      }
    }
    fgets(buffer,1024,file);
  }
  close_file(file);
  if (axis) {
    for (i=0;i<beam->slices;i++){
      x0+=beam->particle[i].wgt*beam->particle[i].x;
      xp0+=beam->particle[i].wgt*beam->particle[i].xp;
      y0+=beam->particle[i].wgt*beam->particle[i].y;
      yp0+=beam->particle[i].wgt*beam->particle[i].yp;
      wgt0+=beam->particle[i].wgt;
    }
    x0/=wgt0;
    xp0/=wgt0;
    y0/=wgt0;
    yp0/=wgt0;
    for (i=0;i<beam->slices;i++){
      beam->particle[i].x-=x0;
      beam->particle[i].xp-=xp0;
      beam->particle[i].y-=y0;
      beam->particle[i].yp-=yp0;
    }
  }
}

void beam_load_all_bin(BEAM *beam,char *name,int head,int axis,
		       int read_structure)
{
  FILE *file;
  int i,j,k,n,nb,nm,m=0,ntest;
  double y0=0.0,yp0=0.0,wgt0=0.0,x0=0.0,xp0=0.0;
  double buffer[17];
  char buf1024[1024];
  char *point;
  
  n=beam->slices/beam->bunches;
  n/=beam->macroparticles;
  nm=beam->macroparticles;
  nb=beam->bunches;
  file=read_file(name);
  if (head) {
    point=fgets(buf1024,1024,file);
    ntest=strtol(point,&point,10);
    if (ntest!=nb) exit(-1);
    ntest=strtol(point,&point,10);
    if (ntest!=n) exit(-1);
    ntest=strtol(point,&point,10);
    if (ntest!=nm) exit(-1);
  }
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
      for (k=0;k<nm;k++){
#ifdef TWODIM
	fread(buffer,sizeof(double),17,file);
	if (read_structure) {
	  beam->z_position[j*n+i]=buffer[0];
	  beam->particle[m].wgt=buffer[1];
	}
	beam->particle[m].energy=buffer[2];
	beam->particle[m].x=buffer[3];
	beam->particle[m].xp=buffer[4];
	beam->particle[m].y=buffer[5];
	beam->particle[m].yp=buffer[6];
	beam->sigma_xx[m].r11=buffer[7];
	beam->sigma_xx[m].r12=buffer[8];
	beam->sigma_xx[m].r22=buffer[9];
	beam->sigma[m].r11=buffer[10];
	beam->sigma[m].r12=buffer[11];
	beam->sigma[m].r22=buffer[12];
	beam->sigma_xy[m].r11=buffer[13];
	beam->sigma_xy[m].r12=buffer[14];
	beam->sigma_xy[m].r21=buffer[15];
	beam->sigma_xy[m].r22=buffer[16];
#else
	fread(buffer,sizeof(double),17,file);
	if (read_structure) {
	  beam->z_position[j*n+i]=buffer[0];
	  beam->particle[m].wgt=buffer[1];
	}
	beam->particle[m].energy=buffer[2];
	beam->particle[m].y=buffer[3];
	beam->particle[m].yp=buffer[4];
	beam->sigma[m].r11=buffer[5];
	beam->sigma[m].r12=buffer[6];
	beam->sigma[m].r22=buffer[7];
#endif
	m++;
      }
    }
    fgets(buf1024,1024,file);
  }
  close_file(file);
  x0=0.0;
  xp0=0.0;
  y0=0.0;
  yp0=0.0;
  if (axis) {
    for (i=0;i<beam->slices;i++){
      x0+=beam->particle[i].wgt*beam->particle[i].x;
      xp0+=beam->particle[i].wgt*beam->particle[i].xp;
      y0+=beam->particle[i].wgt*beam->particle[i].y;
      yp0+=beam->particle[i].wgt*beam->particle[i].yp;
      wgt0+=beam->particle[i].wgt;
    }
    x0/=wgt0;
    xp0/=wgt0;
    y0/=wgt0;
    yp0/=wgt0;
    for (i=0;i<beam->slices;i++){
      beam->particle[i].x-=x0;
      beam->particle[i].xp-=xp0;
      beam->particle[i].y-=y0;
      beam->particle[i].yp-=yp0;
    }
  }
}

void beam_rotate_straight(BEAM *beam)
{
  // JS: can be done faster with ELEMENT::drift_sigma_matrix() method
  double l,r22=0.0,r21=0.0,wgt=0.0;
  int i;
  R_MATRIX r1,r2;

  for (i=0;i<beam->slices;i++){
    wgt+=beam->particle[i].wgt;
    r22+=beam->particle[i].wgt*beam->sigma[i].r22;
    r21+=beam->particle[i].wgt*beam->sigma[i].r21;
  }
  r22/=wgt;
  r21/=wgt;
  l=-r21/r22;
  r1.r11=1.0;
  r1.r12=l;
  r1.r21=0.0;
  r1.r22=1.0;
  r2.r11=1.0;
  r2.r12=0.0;
  r2.r21=l;
  r2.r22=1.0;
  for (i=0;i<beam->slices;i++){
    beam->particle[i].y+=l*beam->particle[i].yp;
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
    mult_M_M(beam->sigma+i,&r2,beam->sigma+i);
  }
}

void beam_save_gp(BEAM *beam,char *name,int axis,int bunch,
	     double fixed_energy)
{
  FILE *file;
  int i,j,n,nb,m=0;
  double y=0.0,yp=0.0,wgt=0.0,x=0.0,xp=0.0;
  
  beam_rotate_straight(beam);

  n=beam->slices/beam->bunches;
  nb=beam->bunches;
  file=open_file(name);
  m=0;
  for (j=0;j<nb;j++){
    for (i=0;i<n;i++){
#ifdef TWODIM
      x+=beam->particle[m].x*beam->particle[m].wgt;
      xp+=beam->particle[m].xp*beam->particle[m].wgt;
#endif
      y+=beam->particle[m].y*beam->particle[m].wgt;
      yp+=beam->particle[m].yp*beam->particle[m].wgt;
      wgt+=beam->particle[m].wgt;
      m++;
    }
  }
  x/=wgt;
  xp/=wgt;
  y/=wgt;
  yp/=wgt;
  if (axis) {
    x=0.0;
    xp=0.0;
    y=0.0;
    yp=0.0;
  }
  fprintf(file,"%d\n",n);
  m=n*bunch;
  if (fixed_energy<=0.0){
    for (i=0;i<n;i++){
      placet_fprintf(INFO,file,"%g %g ",beam->z_position[bunch*n+i],
		     beam->particle[m].energy);
#ifdef TWODIM
      placet_fprintf(INFO,file,"%g %g ",
		     (beam->particle[m].x-x)/sqrt(beam->sigma_xx[m].r11),
		     (beam->particle[m].xp-xp)/sqrt(beam->sigma_xx[m].r22));
#endif
      placet_fprintf(INFO,file," %g %g %g\n",
		     (beam->particle[m].y-y)/sqrt(beam->sigma[m].r11),
		     (beam->particle[m].yp-yp)/sqrt(beam->sigma[m].r22),
		     beam->particle[m].wgt);
      m++;
    }
  }
  else{
    for (i=0;i<n;i++){
      placet_fprintf(INFO,file,"%g %g ",beam->z_position[bunch*n+i],
		     fixed_energy);
#ifdef TWODIM
      placet_fprintf(INFO,file,"%g %g ",
		     (beam->particle[m].x-x)/sqrt(beam->sigma_xx[m].r11),
		     (beam->particle[m].xp-xp)/sqrt(beam->sigma_xx[m].r22));
#endif
      placet_fprintf(INFO,file," %g %g %g\n",
		     (beam->particle[m].y-y)/sqrt(beam->sigma[m].r11),
		     (beam->particle[m].yp-yp)/sqrt(beam->sigma[m].r22),
		     beam->particle[m].wgt);
      m++;
    }
  }
  placet_fprintf(INFO,file,"\n");
  close_file(file);
}

void beam_save_gp_2(BEAM *beam,char *name,int bunch,double x,double xp,double y,
	       double yp,double fixed_energy)
{
  FILE *file;
  int i,n,m=0;
  
  beam_rotate_straight(beam);

  n=beam->slices/beam->bunches;
  file=open_file(name);
  placet_fprintf(INFO,file,"%d\n",n);
  m=n*bunch;
  if (fixed_energy<=0.0){
    for (i=0;i<n;i++){
      placet_fprintf(INFO,file,"%g %g ",beam->z_position[bunch*n+i],
		     beam->particle[m].energy);
#ifdef TWODIM
      placet_fprintf(INFO,file,"%g %g ",
		     (beam->particle[m].x-x)/sqrt(beam->sigma_xx[m].r11),
		     (beam->particle[m].xp-xp)/sqrt(beam->sigma_xx[m].r22));
#endif
      placet_fprintf(INFO,file," %g %g %g\n",
		     (beam->particle[m].y-y)/sqrt(beam->sigma[m].r11),
		     (beam->particle[m].yp-yp)/sqrt(beam->sigma[m].r22),
		     beam->particle[m].wgt);
      m++;
    }
  }
  else{
    for (i=0;i<n;i++){
      placet_fprintf(INFO,file,"%g %g ",beam->z_position[bunch*n+i],
		     fixed_energy);
#ifdef TWODIM
      placet_fprintf(INFO,file,"%g %g ",
		     (beam->particle[m].x-x)/sqrt(beam->sigma_xx[m].r11),
		     (beam->particle[m].xp-xp)/sqrt(beam->sigma_xx[m].r22));
#endif
      placet_fprintf(INFO,file," %g %g %g\n",
		     (beam->particle[m].y-y)/sqrt(beam->sigma[m].r11),
		     (beam->particle[m].yp-yp)/sqrt(beam->sigma[m].r22),
		     beam->particle[m].wgt);
      m++;
    }
  }
  placet_fprintf(INFO,file,"\n");
  close_file(file);
}

void beam_get_size(BEAM *beam,char *name)
{
  FILE *file;
  int i,n;
  double x_min=1e300,x_max=-1e300,xp_min=1e300,xp_max=-1e300,
    y_min=1e300,y_max=-1e300,yp_min=1e300,yp_max=-1e300;
#ifdef TWODIM
  double x,xp;
#else
  double y,yp;
#endif

  n=beam->slices;
  file=append_file(name);
  for (i=0;i<n;i++){
#ifdef TWODIM
    x=beam->particle[i].x;
    if (x<x_min) x_min=x;
    if (x>x_max) x_max=x;
    xp=beam->particle[i].xp;
    if (xp<xp_min) xp_min=xp;
    if (xp>xp_max) xp_max=xp;
#else
    y=beam->particle[i].y;
    if (y<y_min) y_min=y;
    if (y>y_max) y_max=y;
    yp=beam->particle[i].yp;
    if (yp<yp_min) yp_min=yp;
    if (yp>yp_max) yp_max=yp;
#endif
  }
#ifdef TWODIM
  placet_fprintf(INFO,file,"%g %g %g %g ",x_min,x_max,xp_min,xp_max);
#endif
  placet_fprintf(INFO,file,"%g %g %g %g\n",y_min,y_max,yp_min,yp_max);
  close_file(file);
}

void beam_position_load(BEAM *beam,char *name)
{
  FILE *file;
  int i,n;
  char *point,buffer[1024];
  file=read_file(name);
  n=beam->slices;
  for (i=0;i<n;i++){
    point=fgets(buffer,1024,file);
    beam->particle[i].energy=strtod(point,&point);
    beam->particle[i].y=strtod(point,&point)*sqrt(beam->sigma[i].r11);
    beam->particle[i].yp=strtod(point,&point)*sqrt(beam->sigma[i].r22);
  }
  close_file(file);
}

void beamline_correct_z(BEAMLINE *beamline)
{
  ELEMENT *element;
  GIRDER *girder;
  double dz;
  girder=beamline->first;
  while (girder) {
    element=girder->element();
    dz=0.5*girder->get_length();
    while (element) {
      element->set_z(element->get_z()-dz);
      element=element->next;
    }
    girder=girder->next();
  }
}

void wavelength_extract(BEAM *beam,double lambda,double *cr,double *sr,
			double *ci,double *si,double *cw,double *sw,
			double *cr2,double *sr2)
{
  int ibunch,islice,nbunch,nslice,k=0,nm,im;
  double c0=1.0,s0=0.0,c,s,c1,s1,c2,s2,sum_s=0.0,sum_c=0.0,dz,tmp,ts,tc,
    sum_ws=0.0,sum_wc=0.0,sum_ci=0.0,sum_si=0.0,sum_c2=0.0,sum_s2=0.0;

  nbunch=beam->bunches;
  nslice=beam->slices_per_bunch;
  nm=beam->macroparticles;
  sincos(beam->z_position[0]*TWOPI/lambda,&s0,&c0);
  dz=beam->z_position[1]-beam->z_position[0];
  dz*=TWOPI/lambda*1e-6;
  sincos(dz,&s2,&c2);
  //  dz=beam->z_position[nslice]-beam->z_position[nslice-1]
  //    -(beam->z_position[1]-beam->z_position[0]);
  dz=beam->z_position[nslice]-beam->z_position[0];
  dz*=TWOPI/lambda*1e-6;
  sincos(dz,&s1,&c1);
  for (ibunch=0;ibunch<nbunch;ibunch++){
    c=c0;
    s=s0;
    for (islice=0;islice<nslice;islice++){
      for (im=0;im<nm;++im) {
	ts=s*beam->particle[k].y;
	tc=c*beam->particle[k].y;
	sum_s+=ts*beam->particle[k].wgt;
	sum_c+=tc*beam->particle[k].wgt;
	sum_s2+=ts*ts*beam->particle[k].wgt;
	sum_c2+=tc*tc*beam->particle[k].wgt;
	sum_si+=s*beam->particle[k].yp*beam->particle[k].wgt;
	sum_ci+=c*beam->particle[k].yp*beam->particle[k].wgt;
	sum_ws+=s*beam->particle[k].wgt;
	sum_wc+=c*beam->particle[k].wgt;
	k++;
      }
      tmp=c2*s-s2*c;
      c=c2*c+s2*s;
      s=tmp;
    }
    tmp=c1*s0-s1*c0;
    c0=c1*c0+s1*s0;
    s0=tmp;
  }
  *cr=sum_c;
  *sr=sum_s;
  *cr2=sum_c2;
  *sr2=sum_s2;
  *ci=sum_ci;
  *si=sum_si;
  *cw=sum_wc;
  *sw=sum_ws;
}

void wavelength_spectrum_extract(BEAM *beam,double l0,double l1,int n,
				 double beta,int flag,char *filename)
{
  int i;
  double dl,l,c,s,cw,sw,ci,si,c2,s2;
  FILE *file=open_file(filename);
  switch(flag){
  case 0:
    if (n>1){
      dl=(l1-l0)/(double)(n-1);
    }
    else{
      dl=0.0;
    }
    for (i=0;i<n;i++){
      l=l0+dl*(double)i;
      wavelength_extract(beam,l,&c,&s,&ci,&si,&cw,&sw,&c2,&s2);
      placet_fprintf(INFO,file,"%g %g %g %g %g %g %g %g %g\n",
		     l,c,s,ci*beta,si*beta,cw,sw,c2,s2);
    }
    break;
  case 1:
    if (n>1){
      dl=(log(l1)-log(l0))/(double)(n-1);
    }
    else{
      dl=0.0;
    }
    for (i=0;i<n;i++){
      l=l0*exp(dl*(double)i);
      wavelength_extract(beam,l,&c,&s,&ci,&si,&cw,&sw,&c2,&s2);
      placet_fprintf(INFO,file,"%g %g %g %g %g %g %g %g %g\n",
		     l,c,s,ci*beta,si*beta,cw,sw,c2,s2);
    }
    break;
  }
  close_file(file);
}
