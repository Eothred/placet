#include "dl_element.h"

#include <dlfcn.h>

DLELEMENT::DLELEMENT(int &argc, char **argv ) : ELEMENT()
{
  attributes.add("filename", OPT_STRING, &filename);
  set_attributes(argc, argv);

  if ((handle = dlopen(filename.c_str(), RTLD_LAZY))==NULL) throw dlerror();
 
  dlerror();
  *(void **)(&dl_init) = dlsym(handle, "dl_element_init");  
  if (const char *error = dlerror())  throw error;
  
  dlerror();
  *(void **)(&dl_fini) = dlsym(handle, "dl_element_fini");
  if (const char *error = dlerror())  throw error;

  *(void **)(&dl_step_in) = dlsym(handle, "dl_element_step_in");
  *(void **)(&dl_step_out) = dlsym(handle, "dl_element_step_out");

  *(void **)(&dl_step_4d) = dlsym(handle, "dl_element_step_4d");
  *(void **)(&dl_step_4d_0) = dlsym(handle, "dl_element_step_4d_0");
  *(void **)(&dl_step_4d_sr) = dlsym(handle, "dl_element_step_4d_sr");
  *(void **)(&dl_step_4d_sr_0) = dlsym(handle, "dl_element_step_4d_sr_0");

  *(void **)(&dl_step_6d) = dlsym(handle, "dl_element_step_6d");
  *(void **)(&dl_step_6d_0) = dlsym(handle, "dl_element_step_6d_0");
  *(void **)(&dl_step_6d_sr) = dlsym(handle, "dl_element_step_6d_sr");
  *(void **)(&dl_step_6d_sr_0) = dlsym(handle, "dl_element_step_6d_sr_0");

  if (!(*dl_init)(*this, argc, argv)) throw "cannot initialize dynamic loadable element";
}

DLELEMENT::~DLELEMENT()
{
  (*dl_fini)(*this);
  
  dlclose(handle);
}
