#include "placet_cout.hh"

extern struct PRINT_DATA print_data;

/// initialisation of global printing object placet_cout
PLACET_COUT* PLACET_COUT::instance = 0;
PLACET_COUT& placet_cout=*(PLACET_COUT::getInstance());

PLACET_COUT::PLACET_COUT():active(false),print_level(true),stream(std::ostringstream::out),current_level(INFO){
}

PLACET_COUT& PLACET_COUT::operator<<(VERBOSITY verbosity) {
  current_level = verbosity;
  if (verbosity==ERROR) {print_data.num_errors++;}
  else if (verbosity==WARNING) {print_data.num_warnings++;}
  if (verbosity<=print_data.verbosity) {
    active = true;
    if (print_level) {
      std::string level;
      switch (verbosity) {
      case ALWAYS:
	level="PLACET"; break;
#define SET_LEVEL(x)  case x:  level=#x;  break
	SET_LEVEL(ERROR);
	SET_LEVEL(WARNING);
	SET_LEVEL(INFO);
	SET_LEVEL(VERBOSE);
	SET_LEVEL(VERYVERBOSE);
	SET_LEVEL(DEBUG);
      default:
	level = "UNKNOWN";
      }
#undef SET_LEVEL
      stream << std::setw(15) << std::left << level << std::right;
    }
  } else {
    active = false;
  }
  return *this;
}

void PLACET_COUT::end_message(){
  int num_errors=print_data.num_errors;
  int num_warnings=print_data.num_warnings;
  if (num_errors && print_data.exit_on_error) placet_cout << ALWAYS << "Exiting as an ERROR is encountered" << std::endl;
  std::cout << std::endl;
  if(num_warnings) {
    placet_cout << ALWAYS << "There " << (num_warnings==1 ? "has" : "have") << " been " << num_warnings << " WARNING";
    if (num_warnings>1) placet_cout << "S";
    if (print_data.verbosity<WARNING) placet_cout <<" (suppressed)";
    placet_cout << std::endl;
  }
  if(num_errors) {
    placet_cout << ALWAYS << "There " << (num_errors==1 ? "has" : "have") << " been " << num_errors << " ERROR";
    if (num_errors>1) placet_cout << "S";
    if (print_data.verbosity<ERROR) placet_cout <<" (suppressed)";
    placet_cout << std::endl;
    placet_cout << ALWAYS << "Please consider submitting a bug report at https://savannah.cern.ch/projects/placet/" << std::endl;
  }
  placet_cout << INFO << "Thank you for using PLACET! Goodbye!" << endmsg;
}

