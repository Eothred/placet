#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "random.hh"
#include "structures_def.h"
#include "beamline.h"

/* Function prototypes */

#include "position.h"
#include "girder.h"
#include "cavity.h"
#include "dipole.h"
#include "ground.h"
#include "placet_tk.h"
#include "placeti3.h"
#include "quadrupole.h"
#include "quadbpm.h"

int check_beamline_changeable(Tcl_Interp*,char*,int);

int tk_SaveAllPositions(ClientData /*clientdata*/,Tcl_Interp *interp,int argc, char *argv[])
{
  int error;
  char *fname=NULL;
  int i;
  int l=0,nd=0,vo=0,po=0,bpm=0,grph=0;
  FILE *f;
  double d[5];
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"File to write"},
    {(char*)"-binary",TK_ARGV_INT,(char*)NULL,(char*)&l,
     (char*)"If not 0 save as binary file"},
    {(char*)"-nodrift",TK_ARGV_INT,(char*)NULL,(char*)&nd,
     (char*)"If not 0 drift positions will not be saved"},
    {(char*)"-vertical_only",TK_ARGV_INT,(char*)NULL,(char*)&vo,
     (char*)"If not 0 only vertical information will be saved"},
    {(char*)"-positions_only",TK_ARGV_INT,(char*)NULL,(char*)&po,
     (char*)"If not 0 only positions will be saved"},
    {(char*)"-cav_bpm",TK_ARGV_INT,(char*)NULL,(char*)&bpm,
     (char*)"If not 0 positions of structure BPMs will be saved"},
    {(char*)"-cav_grad_phas",TK_ARGV_INT,(char*)NULL,(char*)&grph,
     (char*)"If not 0 gradient and phase of structure will be saved"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <SaveAllPositions>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (!fname) {
    Tcl_SetResult(interp,"Error : type `SaveAllPositions -help'", TCL_VOLATILE);
    return TCL_ERROR;
  }

  //  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  f=open_file(fname);
  if (l) {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      ELEMENT *element=inter_data.beamline->element[i];
      if (!element->is_special()) {
	if ((!nd)||(!element->is_drift())) {
	  if (DIPOLE *dipole=element->dipole_ptr()) {
#ifdef TWODIM
	    if (!vo) {
	      d[0]=dipole->get_strength_y();
	      d[1]=dipole->get_strength_x();
	      fwrite(d,sizeof(double),2,f);
	    } else {
	      d[0]=dipole->get_strength_y();
	      fwrite(d,sizeof(double),1,f);
	    }
#else
	    d[0]=dipole->get_strength_y();
	    fwrite(d,sizeof(double),1,f);
#endif
	  } else {
#ifdef TWODIM
	    if (!vo) {
	      if (CAVITY *cavity=element->cavity_ptr()) {
		  if (bpm) {
                  d[0]=cavity->get_bpm_offset_y();
		  d[1]=cavity->get_bpm_offset_x();
		  fwrite(d,sizeof(double),2,f);
	        }
                if (grph) {
                  d[0]=cavity->get_gradient();
                  d[1]=cavity->get_phase();
                  fwrite(d,sizeof(double),2,f); 
	        }		  
              }
	      if (QUADBPM *qbpm=element->quadbpm_ptr()) {
		d[0]=qbpm->bpm_offset.y;
		d[1]=qbpm->bpm_offset.x;
		fwrite(d,sizeof(double),2,f);		  
	      } 
              if (element->is_quad()){
		d[0]=element->offset.roll;
		fwrite(d,sizeof(double),1,f);
	      }
	      if (!po) {
		d[0]=element->offset.y;
		d[1]=element->offset.yp;
		d[2]=element->offset.x;
		d[3]=element->offset.xp;
		fwrite(d,sizeof(double),4,f);
	      } else {
		d[0]=element->offset.y;
		d[1]=element->offset.x;
		fwrite(d,sizeof(double),2,f);
	      }
	    } else {
	      if (CAVITY *cavity=element->cavity_ptr()) {
		if (bpm) {
		  d[0]=cavity->get_bpm_offset_y();
		  fwrite(d,sizeof(double),1,f);
	        }
		if (grph) {
                  d[0]=cavity->get_gradient();
                  d[1]=cavity->get_phase();
                  fwrite(d,sizeof(double),2,f);
                } 
	      }
	      if (QUADBPM *qbpm=element->quadbpm_ptr()){
		d[0]=qbpm->bpm_offset.y;
		fwrite(d,sizeof(double),1,f);		  
	      }
              if (element->is_quad()){
		d[0]=element->offset.roll;
		fwrite(d,sizeof(double),1,f);
	      }
	      if (!po) {
		d[0]=element->offset.y;
		d[1]=element->offset.yp;
		fwrite(d,sizeof(double),2,f);
	      } else {
		d[0]=element->offset.y;
		fwrite(d,sizeof(double),1,f);
	      }
	    }
#else
	    if (CAVITY *cavity=element->cavity_ptr()) {
	      if (bpm) {
		d[0]=((*CAVITY)element)->bpm_y;
		fwrite(d,sizeof(double),1,f);
	      }
              if (grph) {
                d[0]=cavity->get_gradient();
                d[1]=cavity->get_phase();
                fwrite(d,sizeof(double),2,f);
              }		
            }
	    if (QUADBPM *qbpm=element->quadbpm_ptr()){
	      d[0]=qbpm->bpm_offset.y;
	      fwrite(d,sizeof(double),1,f);		  
	    }
	    if (element->is_quad()){
	      d[0]=element->offset.roll;
	      fwrite(d,sizeof(double),1,f);
	    }
	    if (!po) {
	      d[0]=element->offset.y;
	      d[1]=element->offset.yp;
	      fwrite(d,sizeof(double),2,f);
	    } else {
	      d[0]=element->offset.y;
	      fwrite(d,sizeof(double),1,f);
	    }
#endif
	  }
	}
      }
    }
  }
  else {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      ELEMENT *element=inter_data.beamline->element[i];
      if (!element->is_special()) {
	if ((!nd)||(!element->is_drift())) {
	  if (DIPOLE *dipole=element->dipole_ptr()){
#ifdef TWODIM
	    if (!vo) {
	      fprintf(f,"%g %g\n",dipole->get_strength_y(),
		      dipole->get_strength_x());
	    }
	    else {
	      fprintf(f,"%g\n",dipole->get_strength_y());
	    }
#else
	    fprintf(f,"%g\n",dipole->get_strength_y());
#endif
	  }
	  else {
#ifdef TWODIM
	    if (!vo) {
	      if (!po) {
		fprintf(f,"%g %g %g %g",element->offset.y,
			element->offset.yp,
			element->offset.x,
			element->offset.xp);
	      }
	      else {
		fprintf(f,"%g %g",element->offset.y,
			element->offset.x);
	      }
	      if (CAVITY *cavity=element->cavity_ptr()) {
                if (bpm && grph) {
		  fprintf(f," %g %g %g %g\n",
			  cavity->get_bpm_offset_y(),
			  cavity->get_bpm_offset_x(),
			  cavity->get_gradient(),
			  cavity->get_phase());
	        } else if(bpm && !grph) {
		  fprintf(f," %g %g \n",
			  cavity->get_bpm_offset_y(),
			  cavity->get_bpm_offset_x());
                } else if(!bpm && grph) {
		  fprintf(f," %g %g\n",
			  cavity->get_gradient(),
			  cavity->get_phase());		  
		} else {
		  fprintf(f,"\n");
		}
	      } else if (element->is_quad()) {	
		fprintf(f," %g \n",
			element->offset.roll);
	      } else {
 		  fprintf(f,"\n");		
 	      }
	    } else {
	      if (!po) {
		fprintf(f,"%g %g",element->offset.y,
			element->offset.yp);
	      }
	      else {
		fprintf(f,"%g",element->offset.y);
	      }
              if (CAVITY *cavity=element->cavity_ptr()) {
                if (bpm && grph){
		  fprintf(f," %g %g %g\n",
			  cavity->get_bpm_offset_y(),
			  cavity->get_gradient(),
			  cavity->get_phase());		  
		} else if (!bpm && grph){
		  fprintf(f," %g %g\n",
			  cavity->get_gradient(),
			  cavity->get_phase());		  		  
                }  else if (bpm && !grph) {
		  fprintf(f," %g\n",
			  cavity->get_bpm_offset_y());		  
	        } else {
		  fprintf(f,"\n");
	        }
	      } else if (element->is_quad()) {	
		fprintf(f," %g \n",
			element->offset.roll);
              }
	      else {
		fprintf(f,"\n");
	      } 
	    }
#else
	    if (!po) {
	      fprintf(f,"%g %g\n",element->offset.y,
		      element->offset.yp);
	    }
	    else {
	      fprintf(f,"%g\n",element->offset.y);
	    }
#endif
	  }
	}
      }
    }
  }
  close_file(f);
  
  return TCL_OK;
}

int tk_ReadAllPositions(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,char *argv[])
{
  int error;
  char *fname=NULL;
  int i;
  int l=0,nd=0,nm=0,vo=0,po=0,bpm=0,grph=0;
  FILE *f;
  double d[5];
  char buffer[1024],*point;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"File to read"},
    {(char*)"-binary",TK_ARGV_INT,(char*)NULL,(char*)&l,
     (char*)"If not 0 read as binary file"},
    {(char*)"-nodrift",TK_ARGV_INT,(char*)NULL,(char*)&nd,
     (char*)"If not 0 drift positions are not in the file"},
    {(char*)"-nomultipole",TK_ARGV_INT,(char*)NULL,(char*)&nm,
     (char*)"If not 0 multipole positions are not in the file"},
    {(char*)"-vertical_only",TK_ARGV_INT,(char*)NULL,(char*)&vo,
     (char*)"If not 0 only vertical information is in the file"},
    {(char*)"-positions_only",TK_ARGV_INT,(char*)NULL,(char*)&po,
     (char*)"If not 0 only positions are in the file"},
    {(char*)"-cav_bpm",TK_ARGV_INT,(char*)NULL,(char*)&bpm,
     (char*)"If not 0 structure bpm positions are in the file"},
    {(char*)"-cav_grad_phas",TK_ARGV_INT,(char*)NULL,(char*)&grph,
     (char*)"If not 0 gradient and phase of structure will be saved"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <ReadAllPositions>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  //  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  f=read_file(fname);

  if (l) {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      ELEMENT *element=inter_data.beamline->element[i];
      if (!element->is_special()) {
	if ((!nd)||(!element->is_drift())) {
	  if ((!nm)||(!element->is_multipole())) {
	    if (DIPOLE *dipole=element->dipole_ptr()) {
#ifdef TWODIM
	      if (!vo) {
		if (file_read_doubles(f,fname,d,2)==false) {
		  return TCL_ERROR;
		}
		dipole->set_strength_y(d[0]);
		dipole->set_strength_x(d[1]);
	      } else {
		if (file_read_doubles(f,fname,d,1)==false) {
		  return TCL_ERROR;
		}
		element->offset.y=d[0];
	      }
#else
	      if (file_read_doubles(f,fname,d,1)==false) {
		return TCL_ERROR;
	      }
	      dipole->set_strength_y(d[0]);
#endif
	    } else {
#ifdef TWODIM
	      if (!vo) {
		if (CAVITY *cavity=element->cavity_ptr()) {
		  if (bpm) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
		    cavity->set_bpm_offset_y(d[0]);
		    cavity->set_bpm_offset_x(d[1]);
		  }
	          if (grph) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
                    cavity->set_gradient(d[0]);
                    cavity->set_phase(d[1]); 	           
	          }
                }
		if (QUADBPM *qbpm=element->quadbpm_ptr()) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
		  qbpm->bpm_offset.y=d[0];
		  qbpm->bpm_offset.x=d[1];
		}
                if (element->is_quad()) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		  element->offset.roll=d[0];
		}
		if (!po) {
		    if (file_read_doubles(f,fname,d,4)==false) {
		      return TCL_ERROR;
		    }
		  element->offset.y=d[0];
		  element->offset.yp=d[1];
		  element->offset.x=d[2];
		  element->offset.xp=d[3];
		} else {
		  if (file_read_doubles(f,fname,d,2)==false) {
		    return TCL_ERROR;
		  }
		  element->offset.y=d[0];
		  element->offset.x=d[1];
		}
	      } else {
		if (CAVITY *cavity=element->cavity_ptr()) {
		  if (bpm) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		    cavity->set_bpm_offset_y(d[0]);
		  }
                  if (grph) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
                    cavity->set_gradient(d[0]);
                    cavity->set_phase(d[1]);
                  }                   		  
		}
		if (QUADBPM *qbpm=element->quadbpm_ptr()) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		  qbpm->bpm_offset.y=d[0];
		}
		if (element->is_quad()) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		  element->offset.roll=d[0];
		}
		if (!po) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
		  element->offset.y=d[0];
		  element->offset.yp=d[1];
		} else {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		  element->offset.y=d[0];
		}
	      }
#else
	      if (CAVITY *cavity=element->cavity_ptr()) {
		if (bpm) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		  cavity->get_bpm_offset_y()=d[0];
		}
                if (grph) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
                   cavity->set_gradient(d[0]);
                   cavity->set_phase(d[1]);
                }		
	      }
	      if (QUADBPM *qbpm=element->quadbpm_ptr()) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		qbpm->bpm_offset.y=d[0];
	      }
	      if (element->is_quad()) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		element->offset.roll=d[0];
	      }
	      if (!po) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
		element->offset.y=d[0];
		element->offset.yp=d[1];
	      }
	      else {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		element->offset.y=d[0];
	      }
#endif
	    }
	  }
        }
      }
    }
    if (file_read_doubles(f,fname,d,1)==true) {
      close_file(f);
      placet_cout << ERROR << "file " << fname << " is too long" << endmsg;
      return TCL_ERROR;
    }
  }
  else {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      ELEMENT *element=inter_data.beamline->element[i];
      if (!element->is_special()) {
	if ((!nd)||(!element->is_drift())) {
	  if ((!nm)||(!element->is_multipole())) {
	    point=fgets(buffer,1024,f);
	    if (DIPOLE *dipole=element->dipole_ptr()) {
	      dipole->set_strength_y(strtod(point,&point));
#ifdef TWODIM
	      if (!vo) {
		dipole->set_strength_x(strtod(point,&point));
	      }
#endif
	    } else {
	      element->offset.y=strtod(point,&point);
	      if (!po) {
		element->offset.yp=strtod(point,&point);
	      }
#ifdef TWODIM
	      if (!vo) {
		element->offset.x=strtod(point,&point);
		if (!po) {
		  element->offset.xp=strtod(point,&point);
		}
	      }
#endif
              if (CAVITY *cavity=element->cavity_ptr()) {
              	if (bpm && grph) {
		  cavity->set_bpm_offset_y(strtod(point,&point));
#ifdef TWODIM
		  if (!vo) {
		    cavity->set_bpm_offset_x(strtod(point,&point));
		  }
#endif 
		  double grad = strtod(point,&point);
		  double phas = strtod(point,&point);
		  // 		  cavity->set_gradient(strtod(point,&point));
		  // 		  cavity->set_phase(strtod(point,&point));
		  cavity->set_gradient(grad);
		  cavity->set_phase(phas);
		  placet_cout << VERYVERBOSE << "cav grad and phase " << grad << " " << phas << endmsg;
 	      	} else if(bpm && !grph){
		  cavity->set_bpm_offset_y(strtod(point,&point));
#ifdef TWODIM
		  if (!vo) {
		    cavity->set_bpm_offset_x(strtod(point,&point));
		  }
#endif 		  
		} else if(!bpm && grph){
		  double grad = strtod(point,&point);
		  double phas = strtod(point,&point);
		  // 		  cavity->set_gradient(strtod(point,&point));
		  // 		  cavity->set_phase(strtod(point,&point));
		  cavity->set_gradient(grad);
		  cavity->set_phase(phas);
		  placet_cout << VERYVERBOSE << "cav grad and phase " << grad << " " << phas << endmsg;
		}
	      } else if (element->is_quad()) {
		element->offset.roll=(strtod(point,&point));
	      }
	    }
	  }
	}
      }
    }
  }
  close_file(f);

  return TCL_OK;
}

int tk_ReadAllMotion(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		     char *argv[])
{
  int error;
  char *fname=NULL;
  int i;
  int l=0,nd=0,vo=0,po=0,bpm=0;
  FILE *f;
  double d[5];
  char buffer[1024],*point;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"File to read"},
    {(char*)"-binary",TK_ARGV_INT,(char*)NULL,(char*)&l,
     (char*)"If not 0 read as binary file"},
    {(char*)"-nodrift",TK_ARGV_INT,(char*)NULL,(char*)&nd,
     (char*)"If not 0 drift positions are not in the file"},
    {(char*)"-vertical_only",TK_ARGV_INT,(char*)NULL,(char*)&vo,
     (char*)"If not 0 only vertical information is in the file"},
    {(char*)"-positions_only",TK_ARGV_INT,(char*)NULL,(char*)&po,
     (char*)"If not 0 only positions are in the file"},
    {(char*)"-cav_bpm",TK_ARGV_INT,(char*)NULL,(char*)&bpm,
     (char*)"If not 0 structure bpm positions are in the file"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <ReadAllMotion>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  //  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  f=read_file(fname);
  if (l) {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      ELEMENT *element=inter_data.beamline->element[i];
      if ((!nd)||(!element->is_drift())) {
	if (DIPOLE *dipole=element->dipole_ptr()) {
#ifdef TWODIM
	  if (!vo) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
	      dipole->set_strength_y(d[0]);
	      dipole->set_strength_x(d[1]);
	  }
	  else {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
	    element->offset.y=d[0];
	  }
#else
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
	  dipole->set_strength_y(d[0]);
#endif
	}
	else {
#ifdef TWODIM
	  if (!vo) {
	      if (bpm) {
		  if (CAVITY *cavity=element->cavity_ptr()) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
		  cavity->set_bpm_offset_x(d[0]);
		  cavity->set_bpm_offset_y(d[1]);
	      }
            }
	    if (!po) {
		    if (file_read_doubles(f,fname,d,4)==false) {
		      return TCL_ERROR;
		    }
	      element->offset.y=d[0];
	      element->offset.yp=d[1];
	      element->offset.x=d[2];
	      element->offset.xp=d[3];
	    }
	    else {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
	      element->offset.y=d[0];
	      element->offset.x=d[1];
	    }
	  }
	  else {
	      if (bpm) {
		  if (CAVITY *cavity=element->cavity_ptr()) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		cavity->set_bpm_offset_y(d[0]);
	      }
            }
	    if (!po) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
	      element->offset.y=d[0];
	      element->offset.yp=d[1];
	    }
	    else {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
	      element->offset.y=d[0];
	    }
	  }
#else
	      if (bpm) {
		  if (CAVITY *cavity=element->cavity_ptr()) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
		cavity->get_bpm_offset_y()=d[0];
	      }
              }
	  if (!po) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
	    element->offset.y=d[0];
	    element->offset.yp=d[1];
	  }
	  else {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
	    element->offset.y=d[0];
	  }
#endif
	}
      }
    }
    if (file_read_doubles(f,fname,d,1)==true) {
      close_file(f);
      placet_cout << ERROR << "file " << fname << " is too long" << endmsg;
      return TCL_ERROR;
    }
  }
  else {
    for (i=0;i<inter_data.beamline->n_elements;i++){
      ELEMENT *element=inter_data.beamline->element[i];
      if ((!nd)||(!element->is_drift())) {
	point=fgets(buffer,1024,f);
	if (DIPOLE *dipole=element->dipole_ptr()) {
	  dipole->set_strength_y(strtod(point,&point));
#ifdef TWODIM
	  if (!vo) {
	    dipole->set_strength_x(strtod(point,&point));
	  }
#endif
	}
	else {
	  element->offset.y=strtod(point,&point);
	  if (!po) {
	    element->offset.yp=strtod(point,&point);
	  }
#ifdef TWODIM
	  if (!vo) {
	    element->offset.x=strtod(point,&point);
	    if (!po) {
	      element->offset.xp=strtod(point,&point);
	    }
	  }
#endif
	}
      }
    }
  }
  close_file(f);
  return TCL_OK;
}

int tk_WriteGirderLength(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
			 char *argv[])
{
  int error;
  char *fname=NULL;
  int l=0,b=0,a=1;
  FILE *f;
  double d[5],s=0.0;
  GIRDER *g;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"File to write"},
    {(char*)"-binary",TK_ARGV_INT,(char*)NULL,(char*)&l,
     (char*)"If not 0 write as binary file"},
    {(char*)"-beginning_only",TK_ARGV_INT,(char*)NULL,(char*)&b,
     (char*)"If not 0 print only beginning of girder"},
    {(char*)"-absolute_position",TK_ARGV_INT,(char*)NULL,(char*)&a,
     (char*)"If not 0 print abolute position of girders"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <WriteGirderLength>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  //  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  f=open_file(fname);

  g=inter_data.beamline->first;
  if (a) {
    if (b) {
      if (l) {
	d[0]=0.0;
	fwrite(d,sizeof(double),1,f);
	while (g) {
	  s+=g->distance_to_prev_girder()+g->get_length();
	  d[0]=s;
	  fwrite(d,sizeof(double),1,f);
	  g=g->next();
	}
      }
      else {
	fprintf(f,"%15.15g\n",s);
	while (g) {
	  s+=g->distance_to_prev_girder()+g->get_length();
	  fprintf(f,"%15.15g\n",s);
	  g=g->next();
	}
      }
    }
    else {
      if (l) {
	while (g) {
	  s+=g->distance_to_prev_girder();
	  d[0]=s;
	  s+=g->get_length();
	  d[1]=s;
	  fwrite(d,sizeof(double),2,f);
	  g=g->next();
	}
      }
      else {
	while (g) {
	  s+=g->distance_to_prev_girder();
	  fprintf(f,"%15.15g\n",s);
	  s+=g->get_length();
	  fprintf(f,"%15.15g\n",s);
	  g=g->next();
	}
      }
    }
  }
  else {
    if (b) {
      if (l) {
	while (g) {
	  d[0]=g->distance_to_prev_girder()+g->get_length();
	  fwrite(d,sizeof(double),1,f);
	  g=g->next();
	}
      }
      else {
	while (g) {
	  fprintf(f,"%g\n",g->distance_to_prev_girder()+g->get_length());
	  g=g->next();
	}
      }
    }
    else {
      if (l) {
	while (g) {
	  d[0]=g->distance_to_prev_girder();
	  d[1]=g->get_length();
	  fwrite(d,sizeof(double),2,f);
	  g=g->next();
	}
      }
      else {
	while (g) {
	  fprintf(f,"%g\n",g->distance_to_prev_girder());
	  fprintf(f,"%g\n",g->get_length());
	  g=g->next();
	}
      }
    }
  }
  close_file(f);
  return TCL_OK;
}

int tk_ReadQuadMotion(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error;
  char *fname=NULL;
  int i;
  int l=0,vo=0,po=0;
  FILE *f;
  BEAMLINE *beamline;
  double d[5];
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"File to read"},
    {(char*)"-vertical_only",TK_ARGV_INT,(char*)NULL,(char*)&vo,
     (char*)"If not 0 only vertical plane is in the file"},
    {(char*)"-position_only",TK_ARGV_INT,(char*)NULL,(char*)&po,
     (char*)"If not 0 only positions are in the file"},
    {(char*)"-binary",TK_ARGV_INT,(char*)NULL,(char*)&l,
     (char*)"If not 0 read as binary file"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };


  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <ReadQuadMotion>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  f=read_file(fname);

  beamline=inter_data.beamline;
  if (l) {
    for (i=0;i<beamline->n_quad;i++){
#ifdef TWODIM
      if (po) {
	if (!vo) {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
	  beamline->quad[i]->offset.x+=d[0];
	  beamline->quad[i]->offset.y+=d[1];
	}
	else {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
	  beamline->quad[i]->offset.y+=d[0];
	}
#else
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
	beamline->quad[i]->offset.y+=d[0];
#endif
      }
      else {
#ifdef TWODIM
	if (!vo) {
		    if (file_read_doubles(f,fname,d,4)==false) {
		      return TCL_ERROR;
		    }
	  beamline->quad[i]->offset.y+=d[0];
	  beamline->quad[i]->offset.yp+=d[1];
	  beamline->quad[i]->offset.x+=d[2];
	  beamline->quad[i]->offset.xp+=d[3];
	}
	else {
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
	  beamline->quad[i]->offset.y+=d[0];
	  beamline->quad[i]->offset.x+=d[1];
	}
#else
		    if (file_read_doubles(f,fname,d,2)==false) {
		      return TCL_ERROR;
		    }
	beamline->quad[i]->offset.y+=d[0];
	beamline->quad[i]->offset.x+=d[1];
#endif
      }
    }
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
  }
  else {
  }
  close_file(f);
  return TCL_OK;
}

/** Move beamline based on girder position file 
    Those can e.g. be generated with the standalone ground program
*/

int tk_MoveGirder(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  char *fname=NULL;
  int l=0,vo=0;
  FILE *f;
  BEAMLINE *beamline;
  double d[5],y0,scale=1.0;
  GIRDER *girder;
  char buffer[1024],*point;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"File to read"},
    {(char*)"-vertical_only",TK_ARGV_INT,(char*)NULL,(char*)&vo,
     (char*)"If not 0 only vertical plane is in the file"},
    {(char*)"-binary",TK_ARGV_INT,(char*)NULL,(char*)&l,
     (char*)"If not 0 read as binary file"},
    {(char*)"-scale",TK_ARGV_FLOAT,(char*)NULL,(char*)&scale,
     (char*)"Scaling factor for the motion"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <MoveGirder>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  f=read_file(fname);

  beamline=inter_data.beamline;
  
  girder=beamline->first;
  if (l) {
    if (vo) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
      y0=d[0]*scale;
      while (girder) {
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
	d[0]*=scale;
	girder->move_ends(0.0,0.0,y0,d[0]);
	y0=d[0];
	girder=girder->next();
      }
    } else {
    }
		    if (file_read_doubles(f,fname,d,1)==false) {
		      return TCL_ERROR;
		    }
  } else {
    if (vo) {
      if (point=fgets(buffer,1024,f)) {
	strtod(point,&point);
	strtod(point,&point);
	d[1]=strtod(point,&point);
      } else {
	file_too_short(f,fname);
	return TCL_ERROR;
      }
      d[1]*=scale;
      while (girder) {
	if (point=fgets(buffer,1024,f)) {
	  strtod(point,&point);
	  strtod(point,&point);
	  d[3]=strtod(point,&point);
	} else {
	  file_too_short(f,fname);
	  return TCL_ERROR;
	}
	d[3]*=scale;
	girder->move_ends(0.0,0.0,d[1],d[3]);
	d[1]=d[3];
	girder=girder->next();
      }
    } else {
      if (point=fgets(buffer,1024,f)) {
	strtod(point,&point);
	d[0]=strtod(point,&point);
	d[1]=strtod(point,&point);
      } else {
	file_too_short(f,fname);
	return TCL_ERROR;
      }
      d[0]*=scale;
      d[1]*=scale;
      while (girder) {
	if (point=fgets(buffer,1024,f)) {
	  strtod(point,&point);
	  d[2]=strtod(point,&point);
	  d[3]=strtod(point,&point);
	} else {
	  file_too_short(f,fname);
	  return TCL_ERROR;
	}
	d[2]*=scale;
	d[3]*=scale;
	girder->move_ends(d[0],d[2],d[1],d[3]);
	d[0]=d[2];
	d[1]=d[3];
	girder=girder->next();
      }
    }
  }
  close_file(f);
  return TCL_OK;
}

/** apply ground motion ATL
    similar to survey 'Atl', but possible to give options
*/

int tk_GroundMotionATL(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  char *beamlinename=NULL;
  BEAMLINE* beamline=NULL;
  int apply_x=1,apply_y=1,move_girders=1,end_fixed=1;
  double t_step=3000,amplitude=0.5e-6;
  int start=-1,end=-1,start_element=-1,end_element=-1;
  Tk_ArgvInfo table[]={
    {(char*)"-beamline",TK_ARGV_STRING,(char*)NULL,(char*)&beamlinename,
     (char*)"Name of the beamline"},
    {(char*)"-a",TK_ARGV_FLOAT,(char*)NULL,(char*)&amplitude,
     (char*)"amplitude of ATL ground motion in um^2/s/m (default 0.5e-6)"},
    {(char*)"-t",TK_ARGV_FLOAT,(char*)NULL,(char*)&t_step,
     (char*)"time step in seconds (default 3000)"},
    {(char*)"-x",TK_ARGV_INT,(char*)NULL,(char*)&apply_x,
     (char*)"If not 0 horizontal plane motion is applied (default 1)"},
    {(char*)"-y",TK_ARGV_INT,(char*)NULL,(char*)&apply_y,
     (char*)"If not 0 vertical plane motion is applied (default 1)"},
    {(char*)"-move_girders",TK_ARGV_INT,(char*)NULL,(char*)&move_girders,
     (char*)"If 0 individual elements are moved, else girders are moved (default 1)"},
    {(char*)"-end_fixed",TK_ARGV_INT,(char*)NULL,(char*)&end_fixed,
     (char*)"fix start or end of beamline (default 1)"},
    {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&start,
     (char*)"girder number where filter starts (default 0)"},
    {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&end,
     (char*)"last girder number of filter (default end of beamline)"},
    {(char*)"-start_element",TK_ARGV_INT,(char*)NULL,(char*)&start_element,
     (char*)"element number where filter starts at corresponding girder (default 0),is overridden by -start #girdernumber"},
    {(char*)"-end_element",TK_ARGV_INT,(char*)NULL,(char*)&end_element,
     (char*)"last element number of filter at corresponding girder (default end of beamline), is overridden by -end #girdernumber"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command adds ground motion based on the ATL law for a given or current beamline"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <GroundMotionATL>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  // search for beamline
  if (beamlinename) beamline = get_beamline(beamlinename);
  // if not found or not set use beamline currently in use
  if (!beamline) beamline = inter_data.beamline;
  
  // use element numbers only when girder numbers not set:
  if (start==-1 && start_element >= 0) {
    start=beamline->girder_number(start_element);
  }
  if (end==-1 && end_element >= 0) {
    end=beamline->girder_number(end_element);
  }

  beamline_move_atl(beamline,amplitude,t_step,move_girders,apply_x,apply_y,end_fixed,start,end);

  return TCL_OK;
  
}

/** initialise ground motion */

int tk_GroundMotionInit(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  char *fname=NULL,*beamlinename=NULL;
  BEAMLINE* beamline=NULL;
  int apply_x=1,apply_y=1,s_abs=1;
  double s_start=0.0,s_sign=1.0,last_start=0.0;
  double t_start=0.0,t_step=0.01*100.0;
  GIRDER *girder;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"File containing ground motion data to read"},
    {(char*)"-beamline",TK_ARGV_STRING,(char*)NULL,(char*)&beamlinename,
     (char*)"Name of the beamline"},
    {(char*)"-x",TK_ARGV_INT,(char*)NULL,(char*)&apply_x,
     (char*)"If not 0 horizontal plane motion is applied (default 1)"},
    {(char*)"-y",TK_ARGV_INT,(char*)NULL,(char*)&apply_y,
     (char*)"If not 0 vertical plane motion is applied (default 1)"},
    {(char*)"-t_start",TK_ARGV_FLOAT,(char*)NULL,(char*)&t_start,
     (char*)"starting time in seconds (default 0)"},
    {(char*)"-s_start",TK_ARGV_FLOAT,(char*)NULL,(char*)&s_start,
     (char*)"s-value for start of beamline (default 0)"},
    {(char*)"-s_sign",TK_ARGV_FLOAT,(char*)NULL,(char*)&s_sign,
     (char*)"sign of addition from start of beamline (default 1)"},
    {(char*)"-s_abs",TK_ARGV_INT,(char*)NULL,(char*)&s_abs,
     (char*)"s=0.0 at start of beamline (default 1), overwrites all other settings"},
    {(char*)"-last_start",TK_ARGV_FLOAT,(char*)NULL,(char*)&last_start,
     (char*)"reverse order of beamline, s=0.0 (-s_start) at end of beamline, negative value at start of beamline (default 0)"},
    {(char*)"-t_step",TK_ARGV_FLOAT,(char*)NULL,(char*)&t_step,
     (char*)"time step in seconds (default 1)"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command initializes the ground motion for a given or current beamline"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <GroundMotionInit>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  // search for beamline
  if (beamlinename) beamline = get_beamline(beamlinename);
  // if not found or not set use beamline currently in use
  if (!beamline) beamline = inter_data.beamline;
  
  girder=beamline->first;
  
  // delete old ground motion:
  if (beamline->gx) {delete beamline->gx; beamline->gx=0;}
  if (beamline->gy) {delete beamline->gy; beamline->gy=0;}

  // initialise ground motion
  if (apply_x) {
    beamline->gx = new GROUND_DATA(fname,0,t_start,t_step,s_start,last_start,s_sign,s_abs);
    GROUND_DATA* gx=beamline->gx;
    gx->init(girder);
  }
  if (apply_y) {
    beamline->gy = new GROUND_DATA(fname,0,t_start,t_step,s_start,last_start,s_sign,s_abs);
    GROUND_DATA* gy=beamline->gy;
    gy->init(girder);
  }

  return TCL_OK;
}

/** apply ground motion */

int tk_GroundMotion(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  double d[4],d_cav[4],scale=1.0;
  char *output_file=NULL,*output_file2=NULL;
  char *beamlinename=NULL;
  BEAMLINE* beamline=NULL;
  int error, ip0=1, cav_stab=0;
  GIRDER *girder;
  Tk_ArgvInfo table[]={
    {(char*)"-beamline",TK_ARGV_STRING,(char*)NULL,(char*)&beamlinename,
     (char*)"Name of the beamline"},
    {(char*)"-scale",TK_ARGV_FLOAT,(char*)NULL,(char*)&scale,
     (char*)"Scaling factor for the motion"},
    {(char*)"-ip0",TK_ARGV_INT,(char*)NULL,(char*)&ip0,
     (char*)"Move with respect to unfiltered IP ground motion (IP = 0) (default true), if false then absolute motion"},
    {(char*)"-output_file",TK_ARGV_STRING,(char*)NULL,(char*)&output_file,
     (char*)"Write filtered/stabilised relative ground motion positions file to disk"},
    {(char*)"-output_file2",TK_ARGV_STRING,(char*)NULL,(char*)&output_file2,
     (char*)"Write unfiltered/unstabilised relative ground motion positions file to disk"},
    {(char*)"-cav_stab",TK_ARGV_INT,(char*)NULL,(char*)&cav_stab,
     (char*)"Stabilise cavities (default off, only quad stabilisation)"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command applies the ground motion for one iteration to a given or current beamline"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <GroundMotion>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  // search for beamline
  if (beamlinename) beamline = get_beamline(beamlinename);
  // if not found or not set use beamline currently in use
  if (!beamline) beamline = inter_data.beamline;

  GROUND_DATA* gx = beamline->gx;
  GROUND_DATA* gy = beamline->gy;
  if (!gy && !gx) {
    Tcl_SetResult(interp,"Ground motion not initialised, use <GroundMotionInit> first for this beamline!",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  //  open output file
  FILE* f_position=0;
  if (output_file) {
    char name[128];
    snprintf(name,128,"%s",output_file);
    f_position=open_file(name);
  }

  //  open output file
  FILE* f_position2=0;
  if (output_file2) {
    char name[128];
    snprintf(name,128,"%s",output_file2);
    f_position2=open_file(name);
  }

  girder=beamline->first;
  int gn=0; // girder count
  double dx0=0.0; // movement at IP in x
  double dy0=0.0; // movement at IP in y
  bool apply_x = gx ? 1 : 0; // apply x direction, if gx is defined
  bool apply_y = gy ? 1 : 0; // apply y direction, if gy is defined

  // increase iteration
  int iter=0;
  if (apply_y) iter = gy->next_iteration();
  if (apply_x) iter = gx->next_iteration();
  // start ground motion
  if (apply_x) {
    d[0]=gx->dxpos(gn);
    // unfiltered movement at IP in x:
    dx0 = ip0 ? gx->dxpos_0() : 0.0;
    if (!cav_stab || output_file2) {
      d_cav[0]=gx->dxpos(gn,false);
    }
  } else {
    d[0]=0.0; d[2]=0.0;
    d_cav[0]=0.0; d_cav[2]=0.0;
  }

  if (apply_y) {
    d[1]=gy->dxpos(gn);
    // unfiltered movement at IP in y:
    dy0 = ip0 ? gy->dxpos_0() : 0.0;
    if (!cav_stab || output_file2) {
      d_cav[1]=gy->dxpos(gn,false);
    }
  } else {
    d[1]=0.0; d[3]=0.0;
    d_cav[1]=0.0; d_cav[3]=0.0;
  }
  
  bool preisolator = (apply_y && gy->has_preisolator() ) || (apply_x && gx->has_preisolator() );

  while (girder) {
    gn++;
    // preisolator is next to last girder
    if (preisolator && girder->next()!=0 && girder->next()->next()==0) {
      double pos_qd0 = 5.15; // s-positions of qd0 and qf1, should be as an option (in ground?)
      double pos_qf1 = 12.0;
      double ip_dist = girder->get_length() + girder->next()->get_length();
      if (apply_x) {
	std::pair<double,double> pre_pos_x = gx->preisolator_dxpos();
	d[0]=pre_pos_x.second + (pre_pos_x.first-pre_pos_x.second)/(pos_qd0-pos_qf1) * (ip_dist-pos_qf1);
	d[2]=pre_pos_x.second + (pre_pos_x.first-pre_pos_x.second)/(pos_qd0-pos_qf1) * (girder->next()->get_length()-pos_qf1);
      }
      if (apply_y) {
	std::pair<double,double> pre_pos_y = gy->preisolator_dxpos();
	d[1]=pre_pos_y.second + (pre_pos_y.first-pre_pos_y.second)/(pos_qd0-pos_qf1) * (ip_dist-pos_qf1);
	d[3]=pre_pos_y.second + (pre_pos_y.first-pre_pos_y.second)/(pos_qd0-pos_qf1) * (girder->next()->get_length()-pos_qf1);
      }
      if (!cav_stab || output_file2) {
	if (apply_x) {d_cav[0] = d[0]; d_cav[2] = d[2];}
	if (apply_y) {d_cav[1] = d[1]; d_cav[3] = d[3];}
      }
    }
    // ip is at 0, dxpos might give filtered motion:
    // else if (girder->next()==0) {
    //   if (apply_x) {d[2] = ip0 ? dx0 : gx->dxpos_0();}
    //   if (apply_y) {d[3] = ip0 ? dy0 : gy->dxpos_0();}
    //   if (!cav_stab || output_file2) {
    // 	if (apply_x) {d_cav[2] = d[2];}
    // 	if (apply_y) {d_cav[3] = d[3];}
    //   }
    // }
    else {
      if (apply_x) d[2]=gx->dxpos(gn);
      if (apply_y) d[3]=gy->dxpos(gn);
      if (!cav_stab || output_file2) {
	// unfiltered motion
	if (apply_x) d_cav[2]=gx->dxpos(gn,false);
	if (apply_y) d_cav[3]=gy->dxpos(gn,false);
      }
    }
    // first iteration 0:
    if (iter==0) {
      d[0]=dx0; d[2]=dx0; d[1]=dy0; d[3]=dy0;
      if (!cav_stab || output_file2) {
	d_cav[0]=dx0; d_cav[2]=dx0; d_cav[1]=dy0; d_cav[3]=dy0;
      }
    }

    girder->move_ends(scale*(d[0]-dx0),scale*(d[2]-dx0),scale*(d[1]-dy0),scale*(d[3]-dy0));
    if (!cav_stab) {
      // move cavities back and add unfiltered motion
      girder->move_ends_cav(scale*(d_cav[0]-d[0]),scale*(d_cav[2]-d[2]),scale*(d_cav[1]-d[1]),scale*(d_cav[3]-d[3]));
    }
    double girder_z = gy ? gy->get_gs0(gn-1) : gx->get_gs0(gn-1);
    if (output_file)  fprintf(f_position ,"%.15g %.15g %.15g\n",girder_z,scale*(d[0]-dx0),scale*(d[1]-dy0));
    if (output_file2) fprintf(f_position2,"%.15g %.15g %.15g\n",girder_z,scale*(d_cav[0]-dx0),scale*(d_cav[1]-dy0));

    // prepare for next girder:
    if (apply_x) d[0]=d[2];
    if (apply_y) d[1]=d[3];
    if (!cav_stab || output_file2) {
      if (apply_x) d_cav[0]=d_cav[2];
      if (apply_y) d_cav[1]=d_cav[3];
    }
    girder=girder->next();
  }

  double girder_z = gy ? gy->get_gs0(gn) : gx->get_gs0(gn);
  if (output_file) { // save also last girder end position
    fprintf(f_position,"%.15g %.15g %.15g\n",girder_z,scale*(d[0]-dx0),scale*(d[1]-dy0));
    close_file(f_position);
  }
  if (output_file2) {
    fprintf(f_position2,"%.15g %.15g %.15g\n",girder_z,scale*(d_cav[0]-dx0),scale*(d_cav[1]-dy0));
    close_file(f_position2);
  }
  return TCL_OK;
}

int tk_AddPreIsolator(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  char *beamlinename=NULL,*file_mnt1QD0=NULL,*file_mnt2QD0=NULL,*file_mnt1QF1=NULL,*file_mnt2QF1=NULL;
  char *file_mnt1QD0_x=NULL,*file_mnt2QD0_x=NULL,*file_mnt1QF1_x=NULL,*file_mnt2QF1_x=NULL;
  double pos_mnt1=8.306,pos_mnt2=10.946,pos_qd0=5.15,pos_qf1=12.0;
  int girder_nr=-1; // not used
  int error;

  Tk_ArgvInfo table[]={
    {(char*)"-beamline",TK_ARGV_STRING,(char*)NULL,(char*)&beamlinename,
     (char*)"Name of the beamline"},
    {(char*)"-file_mnt1QD0",TK_ARGV_STRING,(char*)NULL,(char*)&file_mnt1QD0,
     (char*)"File containing transfer function from mount 1 to QD0 in y direction"},
    {(char*)"-file_mnt2QD0",TK_ARGV_STRING,(char*)NULL,(char*)&file_mnt2QD0,
     (char*)"File containing transfer function from mount 2 to QD0 in y direction"},
    {(char*)"-file_mnt1QF1",TK_ARGV_STRING,(char*)NULL,(char*)&file_mnt1QF1,
     (char*)"File containing transfer function from mount 1 to QF1 in y direction"},
    {(char*)"-file_mnt2QF1",TK_ARGV_STRING,(char*)NULL,(char*)&file_mnt2QF1,
     (char*)"File containing transfer function from mount 2 to QF1 in y direction"},
    {(char*)"-file_mnt1QD0_x",TK_ARGV_STRING,(char*)NULL,(char*)&file_mnt1QD0_x,
     (char*)"File containing transfer function from mount 1 to QD0 in x direction"},
    {(char*)"-file_mnt2QD0_x",TK_ARGV_STRING,(char*)NULL,(char*)&file_mnt2QD0_x,
     (char*)"File containing transfer function from mount 2 to QD0 in x direction"},
    {(char*)"-file_mnt1QF1_x",TK_ARGV_STRING,(char*)NULL,(char*)&file_mnt1QF1_x,
     (char*)"File containing transfer function from mount 1 to QF1 in x direction"},
    {(char*)"-file_mnt2QF1_x",TK_ARGV_STRING,(char*)NULL,(char*)&file_mnt2QF1_x,
     (char*)"File containing transfer function from mount 2 to QF1 in x direction"},
    {(char*)"-pos_mnt1",TK_ARGV_FLOAT,(char*)NULL,(char*)&pos_mnt1,
     (char*)"position of mount 1 from IP in meters (default 8.306)"},
    {(char*)"-pos_mnt2",TK_ARGV_FLOAT,(char*)NULL,(char*)&pos_mnt2,
     (char*)"position of mount 2 from IP in meters (default 10.946)"},
    {(char*)"-pos_QD0",TK_ARGV_FLOAT,(char*)NULL,(char*)&pos_qd0,
     (char*)"position of QD0 from IP in meters (default 5.15)"},
    {(char*)"-pos_QF1",TK_ARGV_FLOAT,(char*)NULL,(char*)&pos_qf1,
     (char*)"position of QF1 from IP in meters (default 12.0)"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command adds the preisolator to the last two girders of the beamline"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <AddPreIsolator>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  // search for beamline
  BEAMLINE *beamline = get_beamline(beamlinename);
  // if not found or not set use beamline currently in use
  if (!beamline) beamline = inter_data.beamline;
  
  // get ground motion:
  GROUND_DATA* gx = beamline->gx;
  GROUND_DATA* gy = beamline->gy;
  
  if (!gx && !gy) {
    Tcl_SetResult(interp,"Ground motion not initialised, use <GroundMotionInit> first for this beamline!",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  bool apply_x = gx ? 1 : 0; // horizontal direction, if gx is defined
  bool apply_y = gy ? 1 : 0; // vertical direction, if gy is defined

  if (apply_x) {
    if (file_mnt1QD0_x==0) file_mnt1QD0_x=file_mnt1QD0;
    if (file_mnt2QD0_x==0) file_mnt2QD0_x=file_mnt2QD0;
    if (file_mnt1QF1_x==0) file_mnt1QF1_x=file_mnt1QF1;
    if (file_mnt2QF1_x==0) file_mnt2QF1_x=file_mnt2QF1;
    gx->preisolator_filter(girder_nr,pos_mnt1,pos_mnt2,file_mnt1QD0_x,file_mnt2QD0_x,file_mnt1QF1_x,file_mnt2QF1_x);
  }
  if (apply_y) {
    gy->preisolator_filter(girder_nr,pos_mnt1,pos_mnt2,file_mnt1QD0,file_mnt2QD0,file_mnt1QF1,file_mnt2QF1);
  }
 
  return TCL_OK;
}

int tk_AddGMFilter(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  int error;
  char *fname=NULL,*beamlinename=NULL,*filtername=NULL,*filtername_x=NULL,*filtername_y=NULL;
  BEAMLINE* beamline=NULL;
  int start=-1,end=-1,start_element=-1,end_element=-1;
  double cutoff_low=-1.0,cutoff_high=-1.0;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"File containing ground motion data to read"},
    {(char*)"-beamline",TK_ARGV_STRING,(char*)NULL,(char*)&beamlinename,
     (char*)"Name of the beamline"},
    {(char*)"-filter",TK_ARGV_STRING,(char*)NULL,(char*)&filtername,
     (char*)"File containing filter data to read (both x and y)"},
    {(char*)"-filter_x",TK_ARGV_STRING,(char*)NULL,(char*)&filtername_x,
     (char*)"File containing filter data in x direction to read"},
    {(char*)"-filter_y",TK_ARGV_STRING,(char*)NULL,(char*)&filtername_y,
     (char*)"File containing filter data in y direction to read"},
    {(char*)"-cutoff_low",TK_ARGV_FLOAT,(char*)NULL,(char*)&cutoff_low,
     (char*)"frequencies lower than this value are cut off"},
    {(char*)"-cutoff_high",TK_ARGV_FLOAT,(char*)NULL,(char*)&cutoff_high,
     (char*)"frequencies higher than this value are cut off."
     // implemented like this, because this use case is supposed to happen less frequently
            "If cutoff_high lower than cutoff_low then only consider cut off frequencies in the interval [high,low]"},
    {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&start,
     (char*)"girder number where filter starts (default 0)"},
    {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&end,
     (char*)"last girder number of filter (default end of beamline)"},
    {(char*)"-start_element",TK_ARGV_INT,(char*)NULL,(char*)&start_element,
     (char*)"element number where filter starts at corresponding girder (default 0),is overridden by -start #girdernumber"},
    {(char*)"-end_element",TK_ARGV_INT,(char*)NULL,(char*)&end_element,
     (char*)"last element number of filter at corresponding girder (default end of beamline), is overridden by -end #girdernumber"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command adds a filter to ground motion of the beamline. Subsequent filters overrule each other and are not additive"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <AddGMFilter>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  // search for beamline
  if (beamlinename) beamline = get_beamline(beamlinename);
  // if not found or not set use beamline currently in use
  if (!beamline) beamline = inter_data.beamline;
  
  // get ground motion:
  GROUND_DATA* gx = beamline->gx;
  GROUND_DATA* gy = beamline->gy;
  
  if (!gx && !gy) {
    Tcl_SetResult(interp,"Ground motion not initialised, use <GroundMotionInit> first for this beamline!",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  bool apply_x = gx ? 1 : 0; // horizontal direction, if gx is defined
  bool apply_y = gy ? 1 : 0; // vertical direction, if gy is defined
  // use element numbers only when girder numbers not set:
  if (start==-1 && start_element >= 0) {
    start=beamline->girder_number(start_element);
  }
  if (end==-1 && end_element >= 0) {
    end=beamline->girder_number(end_element);
  }
  if (start==-1) start=0;

  GIRDER* girder=beamline->first;
  
  if (filtername && (!filtername_x || !filtername_y) ) {
    if (!filtername_x) filtername_x = filtername;
    if (!filtername_y) filtername_y = filtername;
  }

  if (apply_x && filtername_x) gx->add_filter(fname,girder,filtername_x,start,end,cutoff_low,cutoff_high);
  if (apply_y && filtername_y) gy->add_filter(fname,girder,filtername_y,start,end,cutoff_low,cutoff_high);
  
  return TCL_OK;
}
