/**********************************************************************/
/*                      TestMeasuredCorrection                        */
/**********************************************************************/

// based on TestFreeCorrection (placet_tk.cc)
int tk_TestMeasuredCorrection(ClientData /*clientdata*/,
			      Tcl_Interp *interp,int argc,
			      char *argv[])
{
  int error;
  int nquad=1,noverlap=0,nbin,i,nbin_rf,do_rf=0,noacc=0,nc=0;
  void (*survey)(BEAMLINE*);
  char *file_name=NULL;
  char *format="%s %ex %sex %x 0 %ey %sey %Env 0 %n";
  char *beamname0=NULL,*beamname1=NULL,*beamname2=NULL,
    *survey_name,survey0[]="Clic";
  char *cb0=NULL,*cb1=NULL,*cb2=NULL;
  BIN **bin,**bin_rf;
  BEAM *beam0=NULL,*beam1=NULL,*beam2=NULL,*cbeam0=NULL,
      *cbeam1=NULL,*cbeam2=NULL;
  int iter=1,bin_iter=1,n,start=0,end=-1;
  double *q0,*q1,*q2,*gl0=NULL,*gl1=NULL,*gl2=NULL;
  char **v,*lq0=NULL,*lq1=NULL,*lq2=NULL,*bin_store=NULL,*bin_load=NULL;
  double jitter_y=0.0,jitter_x=0.0,wgt0=1.0,wgt2=1.0,wgt1=-1.0,pwgt=0.0;
  double g1=1.0,g2=1.0;
  int beamline_iter=1,full_bin_length=0;
  char *call0=NULL,*call1=NULL,*call2=NULL;
  char *correctors_str=NULL;
  int *correctors, ncorrectors;
  Tk_ArgvInfo table[]={
    {(char*)"-machines",TK_ARGV_INT,(char*)NULL,(char*)&iter,
     (char*)"Number of machines to simulate"},
    {(char*)"-start",TK_ARGV_INT,(char*)NULL,(char*)&start,
     (char*)"First element to be corrected"},
    {(char*)"-end",TK_ARGV_INT,(char*)NULL,(char*)&end,
     (char*)"Last element but one to be corrected (<0: go to the end)"},
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nquad,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-correct_full_bin",TK_ARGV_INT,(char*)NULL,
     (char*)&full_bin_length,
     (char*)"If not zero the whole bin will be corrected"},
    {(char*)"-binoverlap",TK_ARGV_INT,(char*)NULL,
     (char*)&noverlap,
     (char*)"Overlap of bins in no of quadrupoles"},
    {(char*)"-jitter_y",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_y,
     (char*)"Vertical beam jitter [micro meter]"},
    {(char*)"-jitter_x",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&jitter_x,
     (char*)"Horizontal beam jitter [micro meter]"},
    {(char*)"-bpm_resolution",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&errors.bpm_resolution,
     (char*)"BPM resolution [micro meter]"},
    {(char*)"-rf_align",TK_ARGV_INT,(char*)NULL,(char*)&do_rf,
     (char*)"Align the RF after the correction?"},
    {(char*)"-no_acc",TK_ARGV_INT,(char*)NULL,(char*)&noacc,
     (char*)"Switch RF off in corrected section?"},
    {(char*)"-beam0",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname0,
     (char*)"Name of the main beam to be used for correction"},
    {(char*)"-beam1",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname1,
     (char*)"Name of the first help beam to be used for correction"},
    {(char*)"-beam2",TK_ARGV_STRING,(char*)NULL,
     (char*)&beamname2,
     (char*)"Name of the second help beam to be used for correction"},
    {(char*)"-cbeam0",TK_ARGV_STRING,(char*)NULL,
     (char*)&cb0,
     (char*)"Name of the main beam to be used for correction"},
    {(char*)"-cbeam1",TK_ARGV_STRING,(char*)NULL,
     (char*)&cb1,
     (char*)"Name of the first help beam to be used for correction"},
    {(char*)"-cbeam2",TK_ARGV_STRING,(char*)NULL,
     (char*)&cb2,
     (char*)"Name of the second help beam to be used for correction"},
    {(char*)"-gradient1",TK_ARGV_FLOAT,(char*)NULL,(char*)&g1,
     (char*)"Gradient for beam1"},
    {(char*)"-gradient2",TK_ARGV_FLOAT,(char*)NULL,(char*)&g2,
     (char*)"Gradient for beam2"},
    {(char*)"-survey",TK_ARGV_STRING,(char*)NULL,
     (char*)&survey_name,
     (char*)"Type of prealignment survey to be used defaults to CLIC"},
    {(char*)"-emitt_file",TK_ARGV_STRING,(char*)NULL,
     (char*)&file_name,
     (char*)"Filename for the results defaults to NULL (no output)"},
    {(char*)"-format",TK_ARGV_STRING,(char*)NULL,(char*)&format,
     (char*)"emittance file format"},
    {(char*)"-wgt0",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt0,
     (char*)"Weight for the BPM position"},
    {(char*)"-wgt1",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt1,
     (char*)"Weight for the BPM resolution"},
    {(char*)"-wgt2",TK_ARGV_FLOAT,(char*)NULL,(char*)&wgt2,
     (char*)"Second weight for the BPM resolution"},
    {(char*)"-pwgt",TK_ARGV_FLOAT,(char*)NULL,(char*)&pwgt,
     (char*)"Weight for the old position"},
    {(char*)"-quad_set0",TK_ARGV_STRING,(char*)NULL,
     (char*)&lq0,
     (char*)"List of quadrupole strengths to be used"},
    {(char*)"-quad_set1",TK_ARGV_STRING,(char*)NULL,
     (char*)&lq1,
     (char*)"List of quadrupole strengths to be used"},
    {(char*)"-quad_set2",TK_ARGV_STRING,(char*)NULL,
     (char*)&lq2,
     (char*)"List of quadrupole strengths to be used"},
    {(char*)"-load_bins",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_load,
     (char*)"File with bin information to be loaded"},
    {(char*)"-save_bins",TK_ARGV_STRING,(char*)NULL,
     (char*)&bin_store,
     (char*)"File with bin information to be stored"},
    {(char*)"-gradient_list0",TK_ARGV_STRING,(char*)NULL,(char*)&call0,
     (char*)"Cavity gradients for beam 0"},
    {(char*)"-gradient_list1",TK_ARGV_STRING,(char*)NULL,(char*)&call1,
     (char*)"Cavity gradients for beam 1"},
    {(char*)"-gradient_list2",TK_ARGV_STRING,(char*)NULL,(char*)&call2,
     (char*)"Cavity gradients for beam 2"},
    {(char*)"-bin_iterations",TK_ARGV_INT,(char*)NULL,(char*)&bin_iter,
     (char*)"Number of iterations for each bin"},
    {(char*)"-beamlines_iterations",TK_ARGV_INT,(char*)NULL,
     (char*)&beamline_iter,(char*)"Number of iterations for each machine"},
    {(char*)"-correctors",TK_ARGV_STRING,(char*)NULL,
     (char*)&correctors_str,
     (char*)"List of correctors to be used"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  survey_name=survey0;
  errors.bpm_resolution=0.0;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }

  beamline_survey_hook_interp=interp;
  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to TestFreeCorrection",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (beamname0!=NULL){
    beam0=get_beam(beamname0);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam0 must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (beamname1!=NULL){
    beam1=get_beam(beamname1);
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-beam1 must be defined\n",NULL);
    error=TCL_ERROR;
  }

  if (call0||call1||call2) {
    if (error=Tcl_SplitList(interp,call0,&n,&v)) return error;
    //    if (n!=inter_data.beamline->n_cavity) {
    //      check_error(interp,argv[0],error);
    //      Tcl_AppendResult(interp,"length of argument to -gradient_list0 does not match number of cavities\n",NULL);
      //    }
    gl0=(double*)alloca(sizeof(double)*n);
    nc=n;
    for (i=0;i<n;i++){
      if (error=Tcl_GetDouble(interp,v[i],gl0+i)) return error;
    }
    Tcl_Free((char*)v);
    if (call1) {
      if (error=Tcl_SplitList(interp,call1,&n,&v)) return error;
      //    if (n!=inter_data.beamline->n_cavity) {
      //      check_error(interp,argv[0],error);
      //      Tcl_AppendResult(interp,"length of argument to -gradient_list1 does not match number of cavities\n",NULL);
      //    }
      gl1=(double*)alloca(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_GetDouble(interp,v[i],gl1+i)) return error;
      }
      Tcl_Free((char*)v);
    }
    else {
      gl1=gl0;
    }
    if (call2) {
      if (error=Tcl_SplitList(interp,call2,&n,&v)) return error;
      //    if (n!=inter_data.beamline->n_cavity) {
      //      check_error(interp,argv[0],error);
      //      Tcl_AppendResult(interp,"length of argument to -gradient_list2 does not match number of cavities\n",NULL);
      //    }
      gl2=(double*)alloca(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_GetDouble(interp,v[i],gl2+i)) return error;
      }
      Tcl_Free((char*)v);   
    }
    else {
      gl2=gl1;
    }
  }
  
  if (beamname2!=NULL){
    beam2=get_beam(beamname2);
  }
  else{
    beam2=NULL;
  }

  if (cb0!=NULL){
    cbeam0=get_beam(cb0);
  }

  if (cb1!=NULL){
    cbeam1=get_beam(cb1);
  }

  if (cb2!=NULL){
    cbeam2=get_beam(cb2);
  }

  if (iter<1) {
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-iterations must be larger than 0\n",NULL);
    error=TCL_ERROR;
  }
  if (noverlap>=nquad){
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-binlength must be larger than -binoverlap\n",
		     NULL);
    error=TCL_ERROR;
  }
  if (survey=(void (*)(BEAMLINE *))survey_find(survey_name)){
  }
  else{
    check_error(interp,argv[0],error);
    Tcl_AppendResult(interp,"-survey ",survey_name," not found\n",NULL);
  }

  if (lq0) {
    if (error=Tcl_SplitList(interp,lq0,&n,&v)) return error;
    if (n!=inter_data.beamline->n_quad) {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"length of argument to -quad_set0 does not match number of quadrupoles\n",NULL);
    }
    else {
      q0=(double*)alloca(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_GetDouble(interp,v[i],q0+i)) return error;
      }
    }
    Tcl_Free((char*)v);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q0[i]/=-inter_data.beamline->quad[i]->get_length();
    }
  }
  else {
    q0=(double*)alloca(sizeof(double)*inter_data.beamline->n_quad);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q0[i]=inter_data.beamline->quad[i]->get_strength()/
      inter_data.beamline->quad[i]->get_length();
    }
  }
  if (lq1) {
    if (error=Tcl_SplitList(interp,lq1,&n,&v)) return error;
    if (n!=inter_data.beamline->n_quad) {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"length of argument to -quad_set1 does not match number of quadrupoles\n",NULL);
    }
    else {
      q1=(double*)alloca(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_GetDouble(interp,v[i],q1+i)) return error;
      }
    }
    Tcl_Free((char*)v);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q1[i]/=-inter_data.beamline->quad[i]->get_length();
    }
  }
  else {
    q1=(double*)alloca(sizeof(double)*inter_data.beamline->n_quad);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q1[i]=inter_data.beamline->quad[i]->get_strength()
	/inter_data.beamline->quad[i]->get_length();
    }
  }
  if (lq2) {
    if (error=Tcl_SplitList(interp,lq2,&n,&v)) return error;
    if (n!=inter_data.beamline->n_quad) {
      check_error(interp,argv[0],error);
      Tcl_AppendResult(interp,"length of argument to -quad_set2 does not match number of quadrupoles\n",NULL);
    }
    else {
      q2=(double*)alloca(sizeof(double)*n);
      for (i=0;i<n;i++){
	if (error=Tcl_GetDouble(interp,v[i],q2+i)) return error;
      }
    }
    Tcl_Free((char*)v);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q2[i]/=-inter_data.beamline->quad[i]->get_length();
    }
  }
  else {
    q2=(double*)alloca(sizeof(double)*inter_data.beamline->n_quad);
    for (i=0;i<inter_data.beamline->n_quad;i++){
      q2[i]=inter_data.beamline->quad[i]->get_strength()
	/inter_data.beamline->quad[i]->get_length();
    }
  }
  if (correctors_str) {
    if (error=Tcl_SplitList(interp,correctors_str,&ncorrectors,&v)) return error;
    correctors=new int[ncorrectors];
    for (i=0;i<ncorrectors;i++){
      if (error=Tcl_GetInt(interp,v[i],&correctors[i])) return error;
    }
    Tcl_Free((char*)v);
  }
  if (error) return error;

  errors.do_jitter=0;
  errors.jitter_y=jitter_y;
  if (jitter_y>0.0) errors.do_jitter=1;

  corr.pwgt=pwgt;
  corr.w=wgt1;
  corr.w2=wgt2;
  corr.w0=wgt0;

  bin=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);

  if (correctors_str) {
    placet_printf(INFO,"using generic correctors\n");
    beamline_bin_divide_limit_correctors(inter_data.beamline,start,end,
					 correctors,ncorrectors,nquad,
					 noverlap,bin,&nbin);
  } else {
    beamline_bin_divide_limit(inter_data.beamline,start,end,nquad,noverlap,
			      bin,&nbin);
  }

  if (full_bin_length) {
    for (i=0;i<nbin;++i) {
      bin[i]->qlast=bin[i]->nq;
    }
  }

  if (do_rf) {
    if (correctors_str) {
      placet_printf(ERROR,"Use of both -correctors and -rf_align is not yet implemented\n");
      exit(123);
    }
    bin_rf=(BIN**)malloc(sizeof(BIN*)*MAX_BIN);
    beamline_bin_divide(inter_data.beamline,1,0,bin_rf,&nbin_rf);
    test_measured_correction(inter_data.beamline,start,end,q0,q1,q2,
			     bin,nbin,beam0,beam1,beam2,
			     cbeam0,cbeam1,cbeam2,gl0,gl1,gl2,nc,g1,g2,
			     iter,beamline_iter,bin_iter,
			     survey,0.0,jitter_y,
			     file_name,format);
    for (i=0;i<nbin_rf;i++){
      bin_delete(bin_rf[i]);
    }
    free(bin_rf);
  } else {
    test_measured_correction(inter_data.beamline,start,end,q0,q1,q2,
			     bin,nbin,beam0,beam1,beam2,cbeam0,cbeam1,cbeam2,
			     gl0,gl1,gl2,nc,g1,g2,iter,beamline_iter,bin_iter,
			     survey,0.0,jitter_y,
			     file_name,format);
  }
  for (i=0;i<nbin;i++){
    bin_delete(bin[i]);
  }
  free(bin);
  return TCL_OK;
}
