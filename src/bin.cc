#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "bin.h"
#include "malloc.h"
#include "placet_tk.h"

#include "quadrupole.h"

extern INTER_DATA_STRUCT inter_data;

/******   Tcl/Tk commands   ******/

/**********************************************************************/
/*                      BeamlineBinDivide                             */
/**********************************************************************/

int tk_BeamlineBinDivide(ClientData /*clientdata*/,Tcl_Interp *interp,
			 int argc,char *argv[])
{
  int error;
  int nq=0,overlapp=0;
  Tk_ArgvInfo table[]={
    {(char*)"-binlength",TK_ARGV_INT,(char*)NULL,(char*)&nq,
     (char*)"Number of quadrupoles per bin"},
    {(char*)"-overlapp",TK_ARGV_INT,(char*)NULL,(char*)&overlapp,
     (char*)"Number of overlapping quadrupoles per bin"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to BeamlineBinDivide",
                  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_ready(interp,argv[0],error)) return TCL_ERROR;

  beamline_bin_divide_tcl(interp,inter_data.beamline,nq,overlapp);

  return TCL_OK;
}

void bin_define(BEAMLINE *beamline,int start,int nquad,int bpm[],int *nbpm,
		int quad[],int *nq)
{
  ELEMENT **element;

  *nq=0;
  *nbpm=0;
  element=beamline->element;
//  while (element[start]!=NULL){  
  while (start<beamline->n_elements){
    if (element[start]->is_quad()) break;
    start++;
  }
//  if (element[start]==NULL){
  if (start>=beamline->n_elements){
    return;
  }
  /** /
  if (element[start]->is_quadbpm()){
    bpm[(*nbpm)++]=start;
  }
  / **/
  quad[0]=start++;
  (*nq)++;
  while((element[start]!=NULL)&&(nquad)){
    if (element[start]->is_quad()){
      nquad--;
      if (nquad) quad[(*nq)++]=start;
    }
    if (element[start]->is_bpm()){
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
}

void bin_define_limit_correctors(BEAMLINE *beamline,int start,int end,
				 int *correctors, int ncorrectors,
				 int nquad,int bpm[],
				 int *nbpm,int quad[],int *nq)
{
  ELEMENT **element;
  
  *nq=0;
  *nbpm=0;
  element=beamline->element;
  if (end<0) end=beamline->n_elements;
  // fastest way to implement it
  bool *beamline_mask = new bool[beamline->n_elements];
  for (int i=0;i<beamline->n_elements;i++) beamline_mask[i]=false;
  for (int i=0;i<ncorrectors;i++) beamline_mask[correctors[i]]=true;
  //  while (element[start]!=NULL){  
  while (start<end){
    if (beamline_mask[start]) break;
    start++;
  }
  //  if (element[start]==NULL){
  if (start>=end){
    delete []beamline_mask;
    return;
  }
  /** /
      if (element[start]->is_quadbpm()){
      bpm[(*nbpm)++]=start;
      }
      / **/
  quad[0]=start++;
  (*nq)++;
  while((element[start]!=NULL)&&(nquad)&&(start<end)){
    if (beamline_mask[start]){
      nquad--;
      if (nquad) quad[(*nq)++]=start;
    }
    if (element[start]->is_bpm()){
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
  delete []beamline_mask;
}

void bin_define_limit(BEAMLINE *beamline,int start,int end,int nquad,int bpm[],
		      int *nbpm,int quad[],int *nq)
{
  ELEMENT **element;

  *nq=0;
  *nbpm=0;
  element=beamline->element;
  if (end<0) end=beamline->n_elements;
//  while (element[start]!=NULL){  
  while (start<end){
    if (element[start]->is_quad()) break;
    start++;
  }
//  if (element[start]==NULL){
  if (start>=end){
    return;
  }
  /** /
  if (element[start]->is_quadbpm()){
    bpm[(*nbpm)++]=start;
  }
  / **/
  quad[0]=start++;
  (*nq)++;
  while((element[start]!=NULL)&&(nquad)&&(start<end)){
    if (element[start]->is_quad()){
      nquad--;
      if (nquad) quad[(*nq)++]=start;
    }
    if (element[start]->is_bpm()){
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
}


void bin_define_dipole(BEAMLINE *beamline,int start,int nquad,int bpm[],
		       int *nbpm,int quad[],int *nq)
{
  ELEMENT **element;

  *nq=0;
  *nbpm=0;
  element=beamline->element;
  while (element[start]!=NULL){
    if (element[start]->is_dipole()) break;
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  quad[0]=start++;
  (*nq)++;
  while((element[start]!=NULL)&&(nquad)){
    if (element[start]->is_dipole()){
      nquad--;
      if (nquad) quad[(*nq)++]=start;
    }
    if (element[start]->is_bpm()){
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
}

void bin_define_1(BEAMLINE *beamline,int start,int nquad,int bpm[],int *nbpm,
             int quad[],int *nq)
{
  ELEMENT **element;
  *nq=0;
  *nbpm=0;
  element=beamline->element;
  while (element[start]!=NULL){
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr()) {
      if (quad_ptr->get_strength()>0.0) break;
    }
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  /*
  if (element[start]->is_quadbpm()){
    bpm[(*nbpm)++]=start;
  }
  */
  quad[0]=start++;
  (*nq)++;

  while(element[start]!=NULL){
    if (element[start]->is_quad()) break;
    start++;
  }

  while((element[start]!=NULL)&&(nquad)){
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr()){
      if (quad_ptr->get_strength()>0.0){
        nquad--;
        if (nquad) quad[(*nq)++]=start;
      }
    }
    if (element[start]->is_bpm()){
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
}


void bin_define_2(BEAMLINE *beamline,int start,int nquad,int nnbpm,
             int bpm[],int *nbpm,int quad[],int *nq)
{
  ELEMENT **element;
  int mb,first_bpm=0;
  int nq1,nq2;
  *nq=0;
  *nbpm=0;
  element=beamline->element;

  nq1=1;
  nq2=nquad-nq1;

  mb=nnbpm;


  /*** First F quadrupole ***/
  while (element[start]!=NULL)
  {
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr()){
       if (quad_ptr->get_strength()>0.0) break;
    }
    start++;
  }
  if (element[start]==NULL){
    return;
  }

  // Quadrupole is in the feedback , so :
  //  element[start]->set_feedback(1);

  //placet_printf(INFO,"\n************* BIN BEGIN *************\n");
  //placet_printf(INFO,"QF on %d\n",start);
  
  quad[0]=start++;
  (*nq)++;

  /*** nb BPM and a F quadrupole ***/

  first_bpm = -1;

  while((element[start]!=NULL)&&(nq1)){
    if (element[start]->is_bpm()&&(mb))
    {
      if(first_bpm == 0) {
        mb--;
        bpm[(*nbpm)++]=start;

	//        element[start]->set_feedback(1);
        //placet_printf(INFO,"BPM a on %d\n",start);
      }
      first_bpm = 0;
    }
          
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr())
    {
      if (quad_ptr->get_strength()>0.0)
      {
        nq1--;
        nquad--;
        if (nquad) {
          quad[(*nq)++]=start; 
	  //          element[start]->set_feedback(1);
	// test
        //placet_printf(INFO,"QF on %d\n",start);
        }
      }
    }
    start++;
  }
  
  first_bpm = -1;

  /*** nb BPM and a F quadrupole ***/ 
 
  // first_bpm = 0;

  while((element[start]!=NULL)&&(nq2)){
    if (element[start]->is_bpm()&&(mb)){
      if(first_bpm == 1) {
        mb--;
        bpm[(*nbpm)++]=start;
	//        element[start]->set_feedback(1);
	//placet_printf(INFO,"BPM b on %d\n",start);
      }
      first_bpm *= -1;
    }
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr()) {
      if (quad_ptr->get_strength()>0.0) {
        nq2--;
        nquad--;
        if (nquad) {
          quad[(*nq)++]=start;
	  //          element[start]->set_feedback(1);
        } 
        //if (nquad) placet_printf(INFO,"QF on %d\n",start);
      }
    }
    start++;
  }


  first_bpm = -1;

  while((element[start]!=NULL)&&(mb)){
    if (element[start]->is_bpm()){
      if(first_bpm == 1) {
        mb--;
        bpm[(*nbpm)++]=start;
	//        element[start]->set_feedback(1);
        //placet_printf(INFO,"BPM c on %d\n",start);
      }
      first_bpm = (-1)*first_bpm;
    }
    start++;
  }

  //placet_printf(INFO,"nq = %d and nbpm = %d\n",(*nq),(*nbpm));  
}

/* defines a bin consisting of two quadrupoles and two corresponding BPMs
for an interleaved one-to-one correction */

void bin_define_one_to_one(BEAMLINE *beamline,int start,int bpm[],
		      int *nbpm,int quad[],int *nq)
{
  ELEMENT **element;
  int nb=2;
  *nq=0;
  *nbpm=0;
  element=beamline->element;

  /*find first two quadrupoles */

  while (element[start]!=NULL){
    if (element[start]->is_quad()) {
      break;
    }
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  quad[(*nq)++]=start++;

  while(element[start]!=NULL){
    if (element[start]->is_quad()) break;
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  quad[(*nq)++]=start++;

  while((element[start]!=NULL)&&(nb)){
    if (element[start]->is_bpm()){
      nb--;
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
}

void bin_define_dipole_one_to_one(BEAMLINE *beamline,int start,int bpm[],
			     int *nbpm,int quad[],int *nq)
{
  ELEMENT **element;
  int nb=2;
  *nq=0;
  *nbpm=0;
  element=beamline->element;

  /*find first two quadrupoles */

  while (element[start]!=NULL){
    if (element[start]->is_dipole()) {
      break;
    }
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  quad[(*nq)++]=start++;

  while(element[start]!=NULL){
    if (element[start]->is_dipole()) break;
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  quad[(*nq)++]=start++;

  while((element[start]!=NULL)&&(nb)){
    if (element[start]->is_bpm()){
      nb--;
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
}

void bin_define_feedback(BEAMLINE *beamline,int start,int nquad,int bpm[],int *nbpm,
		    int quad[],int *nq)
{
  ELEMENT **element;
  *nq=0;
  *nbpm=0;
  element=beamline->element;
  while (element[start]!=NULL){
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr()) {
      if (quad_ptr->get_strength()>0.0) break;
    }
    start++;
  }
  if (element[start]==NULL){
    return;
  }
  /*
  if (element[start]->is_quadbpm()){
    bpm[(*nbpm)++]=start;
  }
  */
  quad[0]=start++;
  (*nq)++;

  while(element[start]!=NULL){
    if (element[start]->is_quad()) break;
    start++;
  }

  while((element[start]!=NULL)&&(nquad)){
    if (QUADRUPOLE *quad_ptr=element[start]->quad_ptr()){
      if (quad_ptr->get_strength()>0.0){
	nquad--;
	if (nquad) quad[(*nq)++]=start;
      }
    }
    if (element[start]->is_bpm()){
      bpm[(*nbpm)++]=start;
    }
    start++;
  }
}

void beamline_bin_divide_tcl(Tcl_Interp *interp,BEAMLINE *beamline,int nq,
			     int overlapp)
{
  int start,nquad,qlast,nbpm,quad[BIN_MAX_QUAD],bpm[BIN_MAX_BPM];
  int bin_number=0,i;
  start=0;
  while (start>=0){
    bin_define(beamline,start,nq,bpm,&nbpm,quad,&nquad);
    if (nquad>nbpm){
      nquad=nbpm;
    }
    if ((nquad>=nq-overlapp+1)||((nquad>=overlapp+1)&&(overlapp==0))){
      qlast=nquad-overlapp;
      start=quad[qlast-1]+1;
    }
    else{
      start=-1;
      qlast=nquad;
    }
    if (nquad>0){
      Tcl_AppendResult(interp,print_int("{ {%d} ",start),NULL);
      Tcl_AppendResult(interp,print_int("{%d",quad[0]),NULL);
      for(i=1;i<nquad;i++){
	Tcl_AppendResult(interp,print_int(" %d",quad[i]),NULL);
      }
      Tcl_AppendResult(interp,print_int("} {%d",bpm[0]),NULL);
      for(i=1;i<nbpm;i++){
	Tcl_AppendResult(interp,print_int(" %d",bpm[i]),NULL);
      }
      Tcl_AppendResult(interp,"} }\n",NULL);
      bin_number++;
    }
  }
  placet_printf(INFO,"beamline divided into %d bins\n",bin_number);
}


BIN *bin_make(int nq,int nbpm)
{
  BIN *bin;
  int i;
  bin=(BIN*)xmalloc(sizeof(BIN));
  bin->quad=(int*)xmalloc(sizeof(int)*nq);
  bin->pw=(double*)xmalloc(sizeof(double)*nq);
  bin->w=(double*)xmalloc(sizeof(double)*nbpm);
  bin->w2=(double*)xmalloc(sizeof(double)*nbpm);
  bin->w0=(double*)xmalloc(sizeof(double)*nbpm);
  bin->bpm=(int*)xmalloc(sizeof(int)*nbpm);
  bin->indx=(int*)xmalloc(sizeof(int)*nq);
  bin->a0=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->a1=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->a2=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->a=(double*)xmalloc(sizeof(double)*nq*nq);
  bin->b0=(double*)xmalloc(sizeof(double)*nbpm);
  bin->b1=(double*)xmalloc(sizeof(double)*nbpm);
  bin->b2=(double*)xmalloc(sizeof(double)*nbpm);
  bin->t0=(double*)xmalloc(sizeof(double)*nbpm);
  bin->t1=(double*)xmalloc(sizeof(double)*nbpm);
  bin->t2=(double*)xmalloc(sizeof(double)*nbpm);
  bin->correct=(double*)xmalloc(sizeof(double)*nq);
  bin->cor0=(double*)xmalloc(sizeof(double)*nq);
#ifdef TWODIM
  bin->indx_x=(int*)xmalloc(sizeof(int)*nq);
  bin->pw_x=(double*)xmalloc(sizeof(double)*nq);
  bin->w_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->w2_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->w0_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->a0_x=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->a1_x=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->a2_x=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->a_x=(double*)xmalloc(sizeof(double)*nq*nq);
  bin->b0_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->b1_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->b2_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->t0_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->t1_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->t2_x=(double*)xmalloc(sizeof(double)*nbpm);
  bin->correct_x=(double*)xmalloc(sizeof(double)*nq);
  bin->quad_x=bin->quad;
  bin->bpm_x=bin->bpm;
  bin->cor0_x=(double*)xmalloc(sizeof(double)*nq);
#endif
  for (i=0;i<nq;i++){
      bin->cor0[i]=0.0;
#ifdef TWODIM
      bin->cor0_x[i]=0.0;
#endif
  }
  for (i=0;i<nbpm;++i) {
    bin->t0[i]=0.0;
    bin->t1[i]=0.0;
    bin->t2[i]=0.0;
#ifdef TWODIM
    bin->t0_x[i]=0.0;
    bin->t1_x[i]=0.0;
    bin->t2_x[i]=0.0;
#endif
  }
  bin->gain=1.0;
  bin->dtau=0.0;
  return bin;
}

BIN *bin_make_x(int nq,int nbpm,int nqx,int nbpmx)
{
  BIN *bin;
  bin=(BIN*)xmalloc(sizeof(BIN));
  bin->quad=(int*)xmalloc(sizeof(int)*nq);
  bin->pw=(double*)xmalloc(sizeof(double)*nq);
  bin->w=(double*)xmalloc(sizeof(double)*nbpm);
  bin->w0=(double*)xmalloc(sizeof(double)*nbpm);
  bin->bpm=(int*)xmalloc(sizeof(int)*nbpm);
  bin->indx=(int*)xmalloc(sizeof(int)*nq);
  bin->a0=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->a1=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->a2=(double*)xmalloc(sizeof(double)*nq*nbpm);
  bin->t0=(double*)xmalloc(sizeof(double)*nbpm);
  bin->t1=(double*)xmalloc(sizeof(double)*nbpm);
  bin->t2=(double*)xmalloc(sizeof(double)*nbpm);
  if(nq>0){
    bin->a=(double*)xmalloc(sizeof(double)*nq*nq);
  }else{
    bin->a=NULL;
  }
  bin->b0=(double*)xmalloc(sizeof(double)*nbpm);
  bin->b1=(double*)xmalloc(sizeof(double)*nbpm);
  bin->b2=(double*)xmalloc(sizeof(double)*nbpm);
  bin->correct=(double*)xmalloc(sizeof(double)*nq);
  bin->nq=nq;
  bin->nbpm=nbpm;
  bin->cor0=(double*)xmalloc(sizeof(double)*nq);
#ifdef TWODIM
  bin->indx_x=(int*)xmalloc(sizeof(int)*nqx);
  bin->pw_x=(double*)xmalloc(sizeof(double)*nqx);
  bin->w_x=(double*)xmalloc(sizeof(double)*nbpmx);
  bin->w0_x=(double*)xmalloc(sizeof(double)*nbpmx);
  bin->a0_x=(double*)xmalloc(sizeof(double)*nqx*nbpmx);
  bin->a1_x=(double*)xmalloc(sizeof(double)*nqx*nbpmx);
  bin->a2_x=(double*)xmalloc(sizeof(double)*nqx*nbpmx);
  bin->t0_x=(double*)xmalloc(sizeof(double)*nbpmx);
  bin->t1_x=(double*)xmalloc(sizeof(double)*nbpmx);
  bin->t2_x=(double*)xmalloc(sizeof(double)*nbpmx);
  if(nqx>0){
    bin->a_x=(double*)xmalloc(sizeof(double)*nqx*nqx);
  }else{
    bin->a_x=NULL;
  }
  bin->b0_x=(double*)xmalloc(sizeof(double)*nbpmx);
  bin->b1_x=(double*)xmalloc(sizeof(double)*nbpmx);
  bin->b2_x=(double*)xmalloc(sizeof(double)*nbpmx);
  bin->correct_x=(double*)xmalloc(sizeof(double)*nqx);
  bin->quad_x=(int*)xmalloc(sizeof(int)*nqx);
  bin->bpm_x=(int*)xmalloc(sizeof(int)*nbpmx);
  bin->nq_x=nqx;
  bin->nbpm_x=nbpmx;
  bin->cor0_x=(double*)xmalloc(sizeof(double)*nqx);
#endif
  bin->gain=1.0;
  bin->dtau=0.0;
  for (int i=0;i<nbpm;++i) {
    bin->t0[i]=0.0;
    bin->t1[i]=0.0;
    bin->t2[i]=0.0;
  }
#ifdef TWODIM
  for (int i=0;i<nbpmx;++i) {
    bin->t0_x[i]=0.0;
    bin->t1_x[i]=0.0;
    bin->t2_x[i]=0.0;
  }
#endif
  return bin;
}

void
bin_delete(BIN *bin)
{
#ifdef TWODIM
  if (bin->bpm_x!=bin->bpm){
    free(bin->bpm_x);
  }
  if (bin->quad_x!=bin->quad){
    free(bin->quad_x);
  }
  free(bin->w_x);
  free(bin->w0_x);
  free(bin->pw_x);
  free(bin->cor0_x);
  free(bin->correct_x);
  free(bin->b2_x);
  free(bin->b1_x);
  free(bin->b0_x);
  free(bin->t2_x);
  free(bin->t1_x);
  free(bin->t0_x);
  if (bin->a_x){
    free(bin->a_x);
  }
  free(bin->a2_x);
  free(bin->a1_x);
  free(bin->a0_x);
  free(bin->indx_x);
#endif
  free(bin->w);
  free(bin->w0);
  free(bin->pw);
  free(bin->cor0);
  free(bin->correct);
  free(bin->b2);
  free(bin->b1);
  free(bin->b0);
  free(bin->t2);
  free(bin->t1);
  free(bin->t0);
  if(bin->a){
    free(bin->a);
  }
  free(bin->a2);
  free(bin->a1);
  free(bin->a0);
  free(bin->indx);
  free(bin->bpm);
  free(bin->quad);
  free(bin);
}

void bin_set_elements(BIN *bin,int quad[],int nq,int bpm[],int nbpm,int qlast)
{
  int i;
  bin->start=quad[0];
  bin->stop=bpm[nbpm-1]+1;
  for (i=0;i<nq;i++){
    bin->quad[i]=quad[i];
  }
  bin->nq=nq;
  for (i=0;i<nbpm;i++){
    bin->bpm[i]=bpm[i];
  }
  bin->nbpm=nbpm;
  bin->qlast=qlast;
#ifdef TWODIM
  bin->nq_x=nq;
  bin->nbpm_x=nbpm;
  bin->qlast_x=qlast;
#endif
}

void bin_set_elements_x(BIN *bin,int quad[],int nq,int bpm[],int nbpm,
			int quad_x[],int nq_x,int bpm_x[],int nbpm_x,
			int qlast,int qlast_x)
{
  int i;
  
  bin->start=999999999;
  if (nq>0){
    bin->start=quad[0];
  }
  if (nq_x>0){
    bin->start=std::min(bin->start,quad_x[0]);
  }
  
  bin->stop=0;
  if (nbpm>0){
    bin->stop=bpm[nbpm-1]+1;
  }
  if (nbpm_x>0){
    bin->stop=std::max(bin->stop,bpm_x[nbpm_x-1]+1);
  }

  for (i=0;i<nq;i++){
    bin->quad[i]=quad[i];
  }
  bin->nq=nq;
  for (i=0;i<nbpm;i++){
    bin->bpm[i]=bpm[i];
  }
  bin->nbpm=nbpm;
  bin->qlast=qlast;

#ifdef TWODIM
  for (i=0;i<nq_x;i++){
    bin->quad_x[i]=quad_x[i];
  }
  bin->nq_x=nq_x;
  for (i=0;i<nbpm_x;i++){
    bin->bpm_x[i]=bpm_x[i];
  }
  bin->nbpm_x=nbpm_x;
  bin->qlast_x=qlast_x;
#endif
}

void bin_set_gain(BIN *bin,double gain)
{
  bin->gain=gain;
}

void bin_set_dtau(BIN *bin,double dtau)
{
  bin->dtau=dtau;
}

void
bin_write(FILE *f,BIN* bin)
{
    fwrite(&(bin->nq),sizeof(int),1,f);
    fwrite(&(bin->nbpm),sizeof(int),1,f);
    fwrite(&(bin->nq_x),sizeof(int),1,f);
    fwrite(&(bin->nbpm_x),sizeof(int),1,f);
    fwrite(&(bin->qlast),sizeof(int),1,f);
    fwrite(&(bin->start),sizeof(int),1,f);
    fwrite(&(bin->stop),sizeof(int),1,f);
    fwrite((bin->quad),sizeof(int),bin->nq,f);
    fwrite((bin->bpm),sizeof(int),bin->nbpm,f);
    fwrite((bin->indx),sizeof(int),bin->nq,f);
#ifdef TWODIM
    fwrite(&(bin->qlast_x),sizeof(int),1,f);
    fwrite((bin->quad_x),sizeof(int),bin->nq_x,f);
    fwrite((bin->bpm_x),sizeof(int),bin->nbpm_x,f);
    fwrite((bin->indx_x),sizeof(int),bin->nq_x,f);
    fwrite((bin->pw_x),sizeof(double),bin->nq_x,f);
    fwrite((bin->w_x),sizeof(double),bin->nbpm_x,f);
    fwrite((bin->w0_x),sizeof(double),bin->nbpm_x,f);    
#endif
    fwrite((bin->pw),sizeof(double),bin->nq,f);
    fwrite((bin->w),sizeof(double),bin->nbpm,f);
    fwrite((bin->w0),sizeof(double),bin->nbpm,f);    
    fwrite((bin->a0),sizeof(double),bin->nbpm*bin->nq,f);    
    fwrite((bin->a1),sizeof(double),bin->nbpm*bin->nq,f);    
    fwrite((bin->a2),sizeof(double),bin->nbpm*bin->nq,f);    
    fwrite((bin->a),sizeof(double),bin->nq*bin->nq,f);    
    fwrite((bin->b0),sizeof(double),bin->nbpm,f);    
    fwrite((bin->b1),sizeof(double),bin->nbpm,f);    
    fwrite((bin->b2),sizeof(double),bin->nbpm,f);    
    fwrite((bin->t0),sizeof(double),bin->nbpm,f);    
    fwrite((bin->t1),sizeof(double),bin->nbpm,f);    
    fwrite((bin->t2),sizeof(double),bin->nbpm,f);    
    fwrite((bin->correct),sizeof(double),bin->nq,f);    
#ifdef TWODIM
    fwrite((bin->a0_x),sizeof(double),bin->nbpm_x*bin->nq_x,f);    
    fwrite((bin->a1_x),sizeof(double),bin->nbpm_x*bin->nq_x,f);    
    fwrite((bin->a2_x),sizeof(double),bin->nbpm_x*bin->nq_x,f);    
    fwrite((bin->a_x),sizeof(double),bin->nq_x*bin->nq_x,f);    
    fwrite((bin->b0_x),sizeof(double),bin->nbpm_x,f);    
    fwrite((bin->b1_x),sizeof(double),bin->nbpm_x,f);    
    fwrite((bin->b2_x),sizeof(double),bin->nbpm_x,f);    
    fwrite((bin->t0_x),sizeof(double),bin->nbpm_x,f);    
    fwrite((bin->t1_x),sizeof(double),bin->nbpm_x,f);    
    fwrite((bin->t2_x),sizeof(double),bin->nbpm_x,f);    
    fwrite((bin->correct_x),sizeof(double),bin->nq_x,f);    
#endif
    fwrite(&(bin->gain),sizeof(double),1,f);    
}

BIN*
bin_read(FILE *f)
{
    BIN *bin;
    int nq,nbpm,nqx,nbpmx;
    fread(&nq,sizeof(int),1,f);
    fread(&nbpm,sizeof(int),1,f);
    fread(&nqx,sizeof(int),1,f);
    fread(&nbpmx,sizeof(int),1,f);
//    placet_printf(INFO,"%d %d %d %d\n",nq,nbpm,nqx,nbpmx);
    bin=bin_make_x(nq,nbpm,nqx,nbpmx);
    fread(&(bin->qlast),sizeof(int),1,f);
    fread(&(bin->start),sizeof(int),1,f);
    fread(&(bin->stop),sizeof(int),1,f);
    fread((bin->quad),sizeof(int),bin->nq,f);
    fread((bin->bpm),sizeof(int),bin->nbpm,f);
    fread((bin->indx),sizeof(int),bin->nq,f);
    fread((bin->pw_x),sizeof(double),bin->nq_x,f);
    fread((bin->w_x),sizeof(double),bin->nbpm_x,f);
    fread((bin->w0_x),sizeof(double),bin->nbpm_x,f);    
#ifdef TWODIM
    fread(&(bin->qlast_x),sizeof(int),1,f);
    fread((bin->quad_x),sizeof(int),bin->nq_x,f);
    fread((bin->bpm_x),sizeof(int),bin->nbpm_x,f);
    fread((bin->indx_x),sizeof(int),bin->nq_x,f);
#endif
    fread((bin->pw),sizeof(double),bin->nq,f);
    fread((bin->w),sizeof(double),bin->nbpm,f);
    fread((bin->w0),sizeof(double),bin->nbpm,f);    
    fread((bin->a0),sizeof(double),bin->nbpm*bin->nq,f);    
    fread((bin->a1),sizeof(double),bin->nbpm*bin->nq,f);    
    fread((bin->a2),sizeof(double),bin->nbpm*bin->nq,f);    
    fread((bin->a),sizeof(double),bin->nq*bin->nq,f);    
    fread((bin->b0),sizeof(double),bin->nbpm,f);    
    fread((bin->b1),sizeof(double),bin->nbpm,f);    
    fread((bin->b2),sizeof(double),bin->nbpm,f);    
    fread((bin->t0),sizeof(double),bin->nbpm,f);    
    fread((bin->t1),sizeof(double),bin->nbpm,f);    
    fread((bin->t2),sizeof(double),bin->nbpm,f);    
    fread((bin->correct),sizeof(double),bin->nq,f);    
#ifdef TWODIM
    fread((bin->a0_x),sizeof(double),bin->nbpm_x*bin->nq_x,f);    
    fread((bin->a1_x),sizeof(double),bin->nbpm_x*bin->nq_x,f);    
    fread((bin->a2_x),sizeof(double),bin->nbpm_x*bin->nq_x,f);    
    fread((bin->a_x),sizeof(double),bin->nq_x*bin->nq_x,f);    
    fread((bin->b0_x),sizeof(double),bin->nbpm_x,f);    
    fread((bin->b1_x),sizeof(double),bin->nbpm_x,f);    
    fread((bin->b2_x),sizeof(double),bin->nbpm_x,f);    
    fread((bin->t0_x),sizeof(double),bin->nbpm_x,f);    
    fread((bin->t1_x),sizeof(double),bin->nbpm_x,f);    
    fread((bin->t2_x),sizeof(double),bin->nbpm_x,f);    
    fread((bin->correct_x),sizeof(double),bin->nq_x,f);    
#endif
    fread(&(bin->gain),sizeof(double),1,f);    
    return bin;
}

void
binarray_write_file(char *name,BIN** bin,int nbin)
{
    int i;
    FILE *f = open_file(name);
    fwrite(&nbin,sizeof(int),1,f);
    for(i=0;i<nbin;i++){
	bin_write(f,bin[i]);
    }
    close_file(f);
}

int
binarray_read_file(char *name,BIN*** bin)
{
    int i,nbin;
    BIN **tmp;
    FILE *f = read_file(name);
    fread(&nbin,sizeof(int),1,f);
    tmp=(BIN**)malloc(sizeof(BIN*)*nbin);
    for(i=0;i<nbin;i++){
	tmp[i]=bin_read(f);
    }
    close_file(f);
    *bin=tmp;
    return nbin;
}

