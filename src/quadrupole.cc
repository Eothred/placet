#ifdef _OPENMP
#include <omp.h>
#endif
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>
#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "photon_spectrum.h"
#include "matrix.h"
#include "placeti3.h"
#include "quadrupole.h"
#include "photon.h"

extern INTER_DATA_STRUCT inter_data;
extern PHOTON *ptracker;

template <typename T> T pow(T p, int n )
{
  if (n==0) return T(1);
  T b=p;
  for (int np=n-1; np >= 1; ) {
    if (np&1) {
      np = (np - 1) >> 1;
      b *= p;
    } else {
      np >>= 1;
    }
    p *= p;
  }
  return b;
}

void QUADRUPOLE::multipolar_error(BEAM *beam){
  // add a multipolar kick to the particles
  if (fabs(kn)<std::numeric_limits<double>::epsilon()) {
    drift_step_4d_0(beam);
    return;
  }
  if(placet_switch.first_order || beam->particle_beam){
    for (int i=2;i<field;i++){
      kn/=i;
    }
    kn*=std::pow(1e-6,field-2);
    double c0, s0;
#ifdef TWODIM
    sincos(tilt,&s0,&c0);
#else
    c0=1.0;
    s0=0.0;
#endif
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++){
      PARTICLE &particle=beam->particle[i];
      double kx, ky;
#ifdef TWODIM
      multipole_kick(field,kn,s0,c0,particle.x,particle.y,kx,ky);
#else
      multipole_kick(field,kn,s0,c0,0.0,particle.y,kx,ky);
#endif
      kx/=particle.energy;
      ky/=particle.energy;
#ifdef TWODIM
      particle.xp+=kx;
#endif
      particle.yp+=ky; 
    }
  } else {
    placet_cout << WARNING << "No multipolar error implemented for sliced beam " << endmsg;
  }
}

bool QUADRUPOLE::track_with_error(BEAM *beam){
  bool trk=false;
  if(get_type()!=0 && get_kn()!=0.0){
    step_half(beam);
    multipolar_error(beam);
    step_half(beam);
    trk = true;
  }
  return trk;
}

void QUADRUPOLE::step_4d_sr_0(BEAM *beam)
{
  if(inter_data.track_photon) {
    ptracker->track(this);
  }
  if (fabs(strength)<std::numeric_limits<double>::epsilon()) {
    drift_step_4d_0(beam);
    return;
  }
  if(geometry.length<std::numeric_limits<double>::epsilon()) {
    placet_cout << VERBOSE << "Zero length quadrupole with radiation: using tracking without radiation" << endmsg;
    step_4d_0(beam);
    return;
  }

  int nphot=0;
  double k0=strength/geometry.length;
#ifdef LONGITUDINAL
  if (beam->macroparticles==1) {
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      beam->z_position[i]+=geometry.length*0.5e-6*(beam->particle[i].yp*beam->particle[i].yp+beam->particle[i].xp*beam->particle[i].xp);
      beam->z_position[i]+=geometry.length*0.5*1e6*(EMASS*EMASS)/(beam->particle[i].energy*beam->particle[i].energy);
    }
  }
#endif
  if (beam->slices==1) {
    PARTICLE &particle=beam->particle[0];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      double k=k0/particle.energy;
      int n=20;
      double l=geometry.length/n;
      if (k>0.0) { // this if / else can be reduced? JS
	double ksqrt=sqrt(k);
	double cy,sy;
	sincos(ksqrt*l,&sy,&cy);
#ifdef TWODIM
	double cx,sx;
	sincosh(ksqrt*l,sx,cx);
#endif
	for (int j=0;j<n;j++) {
	  double tmp=-sy*ksqrt*particle.y+particle.yp*cy;
	  particle.y=particle.y*cy+particle.yp*sy/ksqrt;
	  double ty=tmp-particle.yp;
	  particle.yp=tmp;
#ifdef TWODIM
	  tmp=sx*ksqrt*particle.x+particle.xp*cx;
	  particle.x=particle.x*cx+particle.xp*sx/ksqrt;
	  double tx=tmp-particle.xp;
	  particle.xp=tmp;
#endif
	  double _angle=sqrt(tx*tx+ty*ty)*1e-6;
	  double EEangle = particle.energy * particle.energy * _angle;
	  double energy_loss = ENERGY_LOSS * EEangle * EEangle / l;
	  particle.energy -= energy_loss;
	  if (fabs(particle.energy)<std::numeric_limits<double>::epsilon())
	    break;
	  k=k0/particle.energy;
	  ksqrt=sqrt(k);
	  sincos(ksqrt*l,&sy,&cy);
#ifdef TWODIM
	  sincosh(ksqrt*l,sx,cx);
#endif
	}
      } else {
	double ksqrt=sqrt(-k);
	double cy,sy;
	sincosh(ksqrt*l,sy,cy);
#ifdef TWODIM
	double cx,sx;
	sincos(ksqrt*l,&sx,&cx);
#endif
	for (int j=0;j<n;j++) {
	  double tmp=sy*ksqrt*particle.y+particle.yp*cy;
	  particle.y=particle.y*cy
	    +particle.yp*sy/ksqrt;
	  double ty=tmp-particle.yp;
	  particle.yp=tmp;
#ifdef TWODIM
	  tmp=-sx*ksqrt*particle.x+particle.xp*cx;
	  particle.x=particle.x*cx
	    +particle.xp*sx/ksqrt;
	  double tx=tmp-particle.xp;
	  particle.xp=tmp;
#endif
	  double _angle=sqrt(tx*tx+ty*ty)*1e-6;
	  double EEangle = particle.energy * particle.energy * _angle;
	  double energy_loss = ENERGY_LOSS * EEangle * EEangle / l;
	  particle.energy -= energy_loss;	
	  if (fabs(particle.energy)<std::numeric_limits<double>::epsilon())
	    break;
	  k=k0/particle.energy;
	  ksqrt=sqrt(-k);
	  sincosh(ksqrt*l,sy,cy);
#ifdef TWODIM
	  sincos(ksqrt*l,&sx,&cx);
#endif
	}
      }
    }
  }
   else {
#pragma omp parallel for  
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	double free_path=Radiation.Exponential();
	double k=k0/particle.energy;
	int n=std::max(20, (int)(SYNRAD::synrad_probability(particle.energy,(fabs(particle.x)+fabs(particle.y))*1e-6*k*geometry.length)*10.0+3.0));
	double l=geometry.length/n;
	if (k>0.0) { // this if / else can be reduced? JS
	  double ksqrt=sqrt(k);
	  double cy,sy;
	  sincos(ksqrt*l,&sy,&cy);
#ifdef TWODIM
	  double cx,sx;
	  sincosh(ksqrt*l,sx,cx);
#endif
	  for (int j=0;j<n;j++) {
	    double tmp=-sy*ksqrt*particle.y+particle.yp*cy;
	    particle.y=particle.y*cy+particle.yp*sy/ksqrt;
	    double ty=tmp-particle.yp;
	    particle.yp=tmp;
#ifdef TWODIM
	    tmp=sx*ksqrt*particle.x+particle.xp*cx;
	    particle.x=particle.x*cx+particle.xp*sx/ksqrt;
	    double tx=tmp-particle.xp;
	    particle.xp=tmp;
#endif
	    tmp=sqrt(tx*tx+ty*ty)*1e-6;
	    if (tmp>0.0001) {
	      placet_printf(VERBOSE, "angle: %g %g %g %g\n%g %g\n%g\n",tmp,
			     particle.energy,tx,ty,
			     particle.x,particle.y,k);
	      particle.set_lost();
	      break;
	    }
	    free_path-=SYNRAD::synrad_probability(particle.energy,tmp);
	    while (free_path<0.0) {
	      double eloss = SYNRAD::get_energy_loss(tmp,l,particle.energy);
	      particle.energy-=eloss;
	      if (fabs(particle.energy)<std::numeric_limits<double>::epsilon()) {
		particle.set_lost();
		break;
	      }
	      free_path+=Radiation.Exponential();
	      k=k0/particle.energy;
	      ksqrt=sqrt(k);
	      sincos(ksqrt*l,&sy,&cy);
#ifdef TWODIM
	      sincosh(ksqrt*l,sx,cx);
#endif
	      nphot++;
	      if(inter_data.track_photon) {
		ptracker->add_photon(particle.x,particle.y,particle.xp,particle.yp,eloss,j*l,this);
	      }
	    }
	    if (fabs(particle.energy)<std::numeric_limits<double>::epsilon())
	      break;
	  }
	} else {
	  double ksqrt=sqrt(-k);
	  double cy,sy;
	  sincosh(ksqrt*l,sy,cy);
#ifdef TWODIM
	  double cx,sx;
	  sincos(ksqrt*l,&sx,&cx);
#endif
	  for (int j=0;j<n;j++) {
	    double tmp=sy*ksqrt*particle.y+particle.yp*cy;
	    particle.y=particle.y*cy+particle.yp*sy/ksqrt;
	    double ty=tmp-particle.yp;
	    particle.yp=tmp;
#ifdef TWODIM
	    tmp=-sx*ksqrt*particle.x+particle.xp*cx;
	    particle.x=particle.x*cx+particle.xp*sx/ksqrt;
	    double tx=tmp-particle.xp;
	    particle.xp=tmp;
#endif
	    tmp=sqrt(tx*tx+ty*ty)*1e-6;
	    if (tmp>0.0001) {
	      placet_printf(VERBOSE, "angle: %g %g %g \n%g %g %g\n%g\n",tmp,
			     particle.energy,tx,ty,
			     particle.x,particle.y,k);
	      fflush(stdout);
	      particle.set_lost();
	      break;
	    }
	    free_path-=SYNRAD::synrad_probability(particle.energy,tmp);
	    while (free_path<0.0) {
	      double eloss = SYNRAD::get_energy_loss(tmp,l,particle.energy);
	      particle.energy-=eloss;
	      if (fabs(particle.energy)<std::numeric_limits<double>::epsilon()) {
		particle.set_lost();
		break;
	      }
	      free_path+=Radiation.Exponential();
	      k=k0/particle.energy;
	      ksqrt=sqrt(-k);
	      sincosh(ksqrt*l,sy,cy);
#ifdef TWODIM
	      sincos(ksqrt*l,&sx,&cx);
#endif
	      nphot++;
	      if(inter_data.track_photon) {
		ptracker->add_photon(particle.x,particle.y,particle.xp,particle.yp,eloss,j*l,this);
	      }
	    }
	    if (fabs(particle.energy)<std::numeric_limits<double>::epsilon())
	      break;
	  }
	}
      }
    }
  }

#ifdef EARTH_FIELD
#pragma omp parallel for
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      particle.xp+=earth_field.x*geometry.length/particle.energy;
      particle.yp+=earth_field.y*geometry.length/particle.energy;
    }
  }
#endif
  placet_cout << VERYVERBOSE << "QUADRUPOLE: average number of photons per slice: " 
	      << (double)nphot/(double)beam->slices << endmsg;
}

void QUADRUPOLE::step_4d_0(BEAM *beam)
{
  if(geometry.length>std::numeric_limits<double>::epsilon()) {
    double k0=strength/geometry.length;
    if (fabs(k0)<std::numeric_limits<double>::epsilon()) {
      drift_step_4d_0(beam);
      return;
    }
#ifdef LONGITUDINAL
    if (beam->macroparticles==1) {
#pragma omp parallel for
      for (int i=0;i<beam->slices;i++) {
	beam->z_position[i]+=geometry.length*0.5e-6
	  *(beam->particle[i].yp*beam->particle[i].yp
	    +beam->particle[i].xp*beam->particle[i].xp);
	beam->z_position[i]+=geometry.length*0.5*1e6
	  *(EMASS*EMASS)/(beam->particle[i].energy
			  *beam->particle[i].energy);
      }
    }
#endif
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	double c,s;
	double k=k0/particle.energy;
	if (k>0.0) {
	  double ksqrt=sqrt(k);
	  sincos(ksqrt*geometry.length,&s,&c);
	  double tmp=particle.y*c+particle.yp*s/ksqrt;
	  particle.yp=-s*ksqrt*particle.y
	    +particle.yp*c;
	  particle.y=tmp;
#ifdef TWODIM
	  sincosh(ksqrt*geometry.length,s,c);
	  tmp=particle.x*c+particle.xp*s/ksqrt;
	  particle.xp=s*ksqrt*particle.x
	    +particle.xp*c;
	  particle.x=tmp;
#endif
	} else {
	  double ksqrt=sqrt(-k);
	  sincosh(ksqrt*geometry.length,s,c);
	  double tmp=particle.y*c+particle.yp*s/ksqrt;
	  particle.yp=s*ksqrt*particle.y
	    +particle.yp*c;
	  particle.y=tmp;
#ifdef TWODIM
	  sincos(ksqrt*geometry.length,&s,&c);
	  tmp=particle.x*c+particle.xp*s/ksqrt;
	  particle.xp=-s*ksqrt*particle.x
	    +particle.xp*c;
	  particle.x=tmp;
#endif
	}
      }
    }
  } else {
    double k0=strength;
    if (fabs(k0)<std::numeric_limits<double>::epsilon()) {
      drift_step_4d_0(beam);
      return;
    }
#ifdef LONGITUDINAL
    if (beam->macroparticles==1) {
#pragma omp parallel for
      for (int i=0;i<beam->slices;i++) {
        PARTICLE &particle=beam->particle[i];
        if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
          beam->z_position[i]+=geometry.length*0.5e-6*(particle.yp*particle.yp+particle.xp*particle.xp);
	  beam->z_position[i]+=geometry.length*0.5*1e6*(EMASS*EMASS)/(particle.energy*particle.energy);
        }
      }
    }
#endif
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
        double k=k0/particle.energy;
        particle.xp+=k*particle.x;
        particle.yp-=k*particle.y;
      }
    }
  }
#ifdef EARTH_FIELD
#pragma omp parallel for
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      particle.xp+=earth_field.x*geometry.length/particle.energy;
      particle.yp+=earth_field.y*geometry.length/particle.energy;
    }
  }
#endif 
}

void QUADRUPOLE::step_4d(BEAM *beam)
{
  if (geometry.length>std::numeric_limits<double>::epsilon()) {
    double k0=strength/geometry.length;
    if (fabs(k0)<std::numeric_limits<double>::epsilon()) {
      drift_step_4d(beam);
      return;
    }
#ifdef LONGITUDINAL
    if (beam->macroparticles==1) {
#pragma omp parallel for
      for (int i=0;i<beam->slices;i++) {
        PARTICLE &particle=beam->particle[i];
	beam->z_position[i]+=geometry.length*0.5e-6
	  *(particle.yp*particle.yp
	    +particle.xp*particle.xp);
	beam->z_position[i]+=geometry.length*0.5*1e6
	  *(EMASS*EMASS)/(particle.energy
			  *particle.energy);
      }
    }
#endif
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {

	R_MATRIX &sigma_xx=beam->sigma_xx[i];
	R_MATRIX &sigma_xy=beam->sigma_xy[i];
	R_MATRIX &sigma_yy=beam->sigma[i];
	double k=k0/particle.energy;
	if (k>0.0) {
	  double ksqrt=sqrt(k);
	  double c,s;
	  sincos(ksqrt*geometry.length,&s,&c);
	  R_MATRIX ryy;
	  ryy.r11=c;
	  ryy.r12=s/ksqrt;
	  ryy.r21=-s*ksqrt;
	  ryy.r22=c;
	  double tmp=particle.y*c+particle.yp*s/ksqrt;
	  particle.yp=-s*ksqrt*particle.y+particle.yp*c;
	  particle.y=tmp;
	  sincosh(ksqrt*geometry.length,s,c);
	  R_MATRIX rxx;
	  rxx.r11=c;
	  rxx.r12=s/ksqrt;
	  rxx.r21=s*ksqrt;
	  rxx.r22=c;
	  tmp=particle.x*c+particle.xp*s/ksqrt;
	  particle.xp=s*ksqrt*particle.x+particle.xp*c;
	  particle.x=tmp;
 	  sigma_xx = rxx * sigma_xx * transpose(rxx);
	  sigma_xy = ryy * sigma_xy * transpose(rxx);
	  sigma_yy = ryy * sigma_yy * transpose(ryy);
	} else {
	  double ksqrt=sqrt(-k);
	  double c,s;
	  sincosh(ksqrt*geometry.length,s,c);
	  R_MATRIX ryy;
	  ryy.r11=c;
	  ryy.r12=s/ksqrt;
	  ryy.r21=s*ksqrt;
	  ryy.r22=c;
	  double tmp=particle.y*c+particle.yp*s/ksqrt;
	  particle.yp=s*ksqrt*particle.y+particle.yp*c;
	  particle.y=tmp;
	  sincos(ksqrt*geometry.length,&s,&c);
	  R_MATRIX rxx;
	  rxx.r11=c;
	  rxx.r12=s/ksqrt;
	  rxx.r21=-s*ksqrt;
	  rxx.r22=c;
	  tmp=particle.x*c+particle.xp*s/ksqrt;
	  particle.xp=-s*ksqrt*particle.x+particle.xp*c;
	  particle.x=tmp;
	  sigma_xx = rxx * sigma_xx * transpose(rxx);
	  sigma_xy = ryy * sigma_xy * transpose(rxx);
	  sigma_yy = ryy * sigma_yy * transpose(ryy);
	}
      }
    }
  } else {
    double k0=strength;
    if (fabs(k0)<std::numeric_limits<double>::epsilon()) {
      drift_step_4d(beam);
      return;
    }
#ifdef LONGITUDINAL
    if (beam->macroparticles==1) {
#pragma omp parallel for
      for (int i=0;i<beam->slices;i++) {
        PARTICLE &particle=beam->particle[i];
        if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
          beam->z_position[i]+=geometry.length*0.5e-6*(particle.yp*particle.yp+particle.xp*particle.xp);
	  beam->z_position[i]+=geometry.length*0.5*1e6*(EMASS*EMASS)/(particle.energy*particle.energy);
	}
      }
    }
#endif
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	R_MATRIX &sigma_xx=beam->sigma_xx[i];
	R_MATRIX &sigma_xy=beam->sigma_xy[i];
	R_MATRIX &sigma_yy=beam->sigma[i];
	double k=k0/particle.energy;
	R_MATRIX ryy;
	ryy.r11=1.0;
	ryy.r12=0.0;
	ryy.r21=-k;
	ryy.r22=1.0;
	particle.yp-=k*particle.y;
	R_MATRIX rxx;
	rxx.r11=1.0;
	rxx.r12=0.0;
	rxx.r21=k;
	rxx.r22=1.0;
	particle.xp+=k*particle.x;
	sigma_xx = rxx * sigma_xx * transpose(rxx);
	sigma_xy = ryy * sigma_xy * transpose(rxx);
	sigma_yy = ryy * sigma_yy * transpose(ryy);
      }
    }
  }
#ifdef EARTH_FIELD
#pragma omp parallel for
  for (int i=0;i<beam->slices;i++) {
    PARTICLE &particle=beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      particle.yp+=earth_field.y*geometry.length/particle.energy;
      particle.xp+=earth_field.x*geometry.length/particle.energy;
    }
  }
#endif
}

void QUADRUPOLE::step_twiss(BEAM *beam,FILE *file,double step,
			    int j,double s0, int n1,int n2,
			    void (*callback0)(FILE*,BEAM*,int,double,int,int))
{
  if (fabs(strength)<std::numeric_limits<double>::epsilon()) {
    drift_step_twiss(beam,file,step,j,s0,n1,n2,callback0);
    return;
  }
  int nstep=int(geometry.length/step)+1;
  double l=geometry.length/nstep;
  callback0(file,beam,j,s0,n1,n2);
  if (geometry.length>std::numeric_limits<double>::epsilon()) {
    double k0=strength/geometry.length;
    for (int istep=0;istep<nstep;istep++) {
      s0+=l;
#pragma omp parallel for
      for (int i=0;i<beam->slices;i++) {
        PARTICLE &particle=beam->particle[i];
        if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
	  R_MATRIX &sigma_xx=beam->sigma_xx[i];
	  R_MATRIX &sigma_xy=beam->sigma_xy[i];
	  R_MATRIX &sigma_yy=beam->sigma[i];
	  double k=k0/particle.energy;
	  if (k>0.0) {
	    double ksqrt=sqrt(k);
	    double c,s;
	    sincos(ksqrt*l,&s,&c);
	    R_MATRIX ryy;
	    ryy.r11=c;
	    ryy.r12=s/ksqrt;
	    ryy.r21=-s*ksqrt;
	    ryy.r22=c;
	    sincosh(ksqrt*l,s,c);
	    R_MATRIX rxx;
	    rxx.r11=c;
	    rxx.r12=s/ksqrt;
	    rxx.r21=s*ksqrt;
	    rxx.r22=c;
	    sigma_xx = rxx * sigma_xx * transpose(rxx);
	    sigma_xy = ryy * sigma_xy * transpose(rxx);
	    sigma_yy = ryy * sigma_yy * transpose(ryy);
	  } else {
	    double ksqrt=sqrt(-k);
	    double c,s;
	    sincosh(ksqrt*l,s,c);
	    R_MATRIX ryy;
	    ryy.r11=c;
	    ryy.r12=s/ksqrt;
	    ryy.r21=s*ksqrt;
	    ryy.r22=c;
	    sincos(ksqrt*l,&s,&c);
	    R_MATRIX rxx;
	    rxx.r11=c;
	    rxx.r12=s/ksqrt;
	    rxx.r21=-s*ksqrt;
	    rxx.r22=c;
	    sigma_xx = rxx * sigma_xx * transpose(rxx);
	    sigma_xy = ryy * sigma_xy * transpose(rxx);
	    sigma_yy = ryy * sigma_yy * transpose(ryy);
	  }
	}
      }
      callback0(file,beam,j,s0,n1,n2);
    }
  } else {
    double k0=strength;
#pragma omp parallel for
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
        R_MATRIX &sigma_xx=beam->sigma_xx[i];
        R_MATRIX &sigma_xy=beam->sigma_xy[i];
        R_MATRIX &sigma_yy=beam->sigma[i];
        double k=k0/particle.energy;
        R_MATRIX ryy;
        ryy.r11=1.0;
        ryy.r12=0.0;
        ryy.r21=-k;
        ryy.r22=1.0;
        R_MATRIX rxx;
        rxx.r11=1.0;
        rxx.r12=0.0;
        rxx.r21=k;
        rxx.r22=1.0;
        sigma_xx = rxx * sigma_xx * transpose(rxx);
        sigma_xy = ryy * sigma_xy * transpose(rxx);
        sigma_yy = ryy * sigma_yy * transpose(ryy);
      }
    }
    callback0(file,beam,j,s0,n1,n2);
  }
}
void QUADRUPOLE::step_twiss_0(BEAM *beam,FILE *file,double step,
			      int j,double s0,int n1,int n2,
			      void (*callback0)(FILE*,BEAM*,int,double,int,int))
{
  if (fabs(strength)<std::numeric_limits<double>::epsilon()) {
    drift_step_twiss_0(beam,file,step,j,s0,n1,n2,callback0);
    return;
  }
  int nstep=int(geometry.length/step)+1;
  double l=geometry.length;
  double s=strength;
  geometry.length/=nstep;
  strength/=nstep;
  if (callback0) callback0(file,beam,j,s0,n1,n2);
  for (int istep=0;istep<nstep;istep++) {
    s0+=geometry.length;
    if (flags.thin_lens) {
      if (flags.longitudinal) {
        if (flags.synrad) QUADRUPOLE::step_6d_tl_sr_0(beam);
        else              QUADRUPOLE::step_6d_tl_0(beam);
      } else {
        if (flags.synrad) QUADRUPOLE::step_4d_tl_sr_0(beam);
        else              QUADRUPOLE::step_4d_tl_0(beam);
      }
    } else {
      if (flags.longitudinal) {
        if (flags.synrad) QUADRUPOLE::step_6d_sr_0(beam);
        else              QUADRUPOLE::step_6d_0(beam);
      } else {
        if (flags.synrad) QUADRUPOLE::step_4d_sr_0(beam);
        else              QUADRUPOLE::step_4d_0(beam);
      }
    }
    if (callback0) callback0(file,beam,j,s0,n1,n2);
  } 
  geometry.length=l;
  strength=s;
}

void QUADRUPOLE::step_4d_tl(BEAM *beam )
{
  if (geometry.length<=std::numeric_limits<double>::epsilon()) return;
# pragma omp parallel for  
  for (int i=0; i<beam->slices; i++) {
    PARTICLE &particle = beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      double k = strength/geometry.length/particle.energy;
      if (fabs(k)<=std::numeric_limits<double>::epsilon()) {
	particle.x += geometry.length*particle.xp;
	particle.y += geometry.length*particle.yp;
      } else {
	const int N = flags.thin_lens;
	double length_n = geometry.length/N;
	double inverse_f = k * length_n;
	R_MATRIX &sigma_xx=beam->sigma_xx[i];
	R_MATRIX &sigma_xy=beam->sigma_xy[i];
	R_MATRIX &sigma_yy=beam->sigma[i];
	R_MATRIX rxx;
	rxx.r11=1 + length_n * inverse_f / 2;
	rxx.r12=length_n * (1 + length_n * inverse_f / 4);
	rxx.r21=inverse_f;
	rxx.r22=1 + length_n * inverse_f / 2;
	rxx=pow(rxx,N);
	R_MATRIX ryy;
	ryy.r11=1 - length_n * inverse_f / 2;
	ryy.r12=length_n * (1 - length_n * inverse_f / 4);
	ryy.r21=-inverse_f;
	ryy.r22=1 - length_n * inverse_f / 2;
	ryy=pow(ryy,N);
	double Kick_x =  particle.x * inverse_f;
	double Kick_y = -particle.y * inverse_f;
	for (int j=0; j<N; j++) {
	  double vxh = particle.xp + Kick_x * 0.5;
	  double vyh = particle.yp + Kick_y * 0.5;
	  particle.x += vxh * length_n;
	  particle.y += vyh * length_n;
	  Kick_x =  particle.x * inverse_f;
	  Kick_y = -particle.y * inverse_f;
	  particle.xp = vxh + Kick_x * 0.5;
	  particle.yp = vyh + Kick_y * 0.5;
	}
	sigma_xx = rxx * sigma_xx * transpose(rxx);
	sigma_xy = ryy * sigma_xy * transpose(rxx);
	sigma_yy = ryy * sigma_yy * transpose(ryy);
      }
    }
  }
}

void QUADRUPOLE::step_6d_0(BEAM *beam )
{
#pragma omp parallel for
  for (int i=0; i<beam->slices; i++) {
    PARTICLE &particle = beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      double kk = strength/geometry.length/particle.energy;
      if (fabs(kk)<=std::numeric_limits<double>::epsilon()) {
	particle.x += geometry.length*particle.xp;
	particle.y += geometry.length*particle.yp;
	particle.z += geometry.length*0.5e-6*(particle.xp*particle.xp+particle.yp*particle.yp);
      } else if (kk>0.0) {
	double ksqrt = sqrt(kk);
	double c,s;
	sincosh(ksqrt*geometry.length,s,c);
	double tmp = particle.x*c + particle.xp*s/ksqrt;
	particle.xp = particle.x*s*ksqrt + particle.xp*c;
	particle.x = tmp;
	sincos(ksqrt*geometry.length,&s,&c);
	tmp = particle.y*c + particle.yp*s/ksqrt;
	particle.yp = -particle.y*s*ksqrt + particle.yp*c;
	particle.y = tmp;
	particle.z += geometry.length*0.5e-6*(particle.xp*particle.xp+particle.yp*particle.yp);
      } else {
	double ksqrt = sqrt(-kk);
	double c,s;
	sincos(ksqrt*geometry.length,&s,&c);
	double tmp = particle.x*c + particle.xp*s/ksqrt;
	particle.xp = -particle.x*s*ksqrt + particle.xp*c;
	particle.x = tmp;
	sincosh(ksqrt*geometry.length,s,c);
	tmp = particle.y*c + particle.yp*s/ksqrt;
	particle.yp = particle.y*s*ksqrt + particle.yp*c;
	particle.y = tmp;
	particle.z += geometry.length*0.5e-6*(particle.xp*particle.xp+particle.yp*particle.yp);
      }
    }
  }
}

void QUADRUPOLE::step_partial(BEAM *beam, double l, void (ELEMENT::*step_function)(BEAM*) )
{
  double _strength=strength;
  strength*=l;
  ELEMENT::step_partial(beam,l,step_function);
  strength=_strength;
}


void QUADRUPOLE::step_6d_tl(BEAM *beam )
{
  if (geometry.length<=std::numeric_limits<double>::epsilon()) return;
#pragma omp parallel for
  for (int i=0; i<beam->slices; i++) {
    PARTICLE &particle = beam->particle[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      double k = strength/geometry.length/particle.energy;
      if (fabs(k)<=std::numeric_limits<double>::epsilon()) {
	particle.x += geometry.length*particle.xp;
	particle.y += geometry.length*particle.yp;
        particle.z += geometry.length*0.5e-6*(particle.xp*particle.xp+particle.yp*particle.yp);
      } else {
	const int N = flags.thin_lens;
	double length_n = geometry.length/N;
	double inverse_f = k * length_n;
	R_MATRIX &sigma_xx=beam->sigma_xx[i];
	R_MATRIX &sigma_xy=beam->sigma_xy[i];
	R_MATRIX &sigma_yy=beam->sigma[i];
	R_MATRIX rxx;
	rxx.r11=1 + length_n * inverse_f / 2;
	rxx.r12=length_n * (1 + length_n * inverse_f / 4);
	rxx.r21=inverse_f;
	rxx.r22=1 + length_n * inverse_f / 2;
	rxx=pow(rxx,N);
	R_MATRIX ryy;
	ryy.r11=1 - length_n * inverse_f / 2;
	ryy.r12=length_n * (1 - length_n * inverse_f / 4);
	ryy.r21=-inverse_f;
	ryy.r22=1 - length_n * inverse_f / 2;
	ryy=pow(ryy,N);
	double Kick_x =  particle.x * inverse_f;
	double Kick_y = -particle.y * inverse_f;
	for (int j=0; j<N; j++) {
	  double vxh = particle.xp + Kick_x * 0.5;
	  double vyh = particle.yp + Kick_y * 0.5;
	  particle.x += vxh * length_n;
	  particle.y += vyh * length_n;
	  particle.z += length_n*0.5e-6*(particle.xp*particle.xp+particle.yp*particle.yp);
	  Kick_x =  particle.x * inverse_f;
	  Kick_y = -particle.y * inverse_f;
	  particle.xp = vxh + Kick_x * 0.5;
	  particle.yp = vyh + Kick_y * 0.5;
	}
	sigma_xx = rxx * sigma_xx * transpose(rxx);
	sigma_xy = ryy * sigma_xy * transpose(rxx);
	sigma_yy = ryy * sigma_yy * transpose(ryy);
      }
    }
  }
}

void QUADRUPOLE::step_half(BEAM *beam )
{
  double l=geometry.length;
  double s=strength;
  geometry.length/=2;
  strength/=2;
  if (placet_switch.first_order || beam->particle_beam) {
    if (flags.thin_lens) {
      if (flags.longitudinal) {
        if (flags.synrad) QUADRUPOLE::step_6d_tl_sr_0(beam);
        else              QUADRUPOLE::step_6d_tl_0(beam);
      } else {
        if (flags.synrad) QUADRUPOLE::step_4d_tl_sr_0(beam);
        else              QUADRUPOLE::step_4d_tl_0(beam);
      }
    } else {
      if (flags.longitudinal) {
        if (flags.synrad) QUADRUPOLE::step_6d_sr_0(beam);
        else              QUADRUPOLE::step_6d_0(beam);
      } else {
        if (flags.synrad) QUADRUPOLE::step_4d_sr_0(beam);
        else              QUADRUPOLE::step_4d_0(beam);
      }
    }
  } else {
    if (flags.thin_lens) {
      if (flags.longitudinal) {
        if (flags.synrad) QUADRUPOLE::step_6d_tl_sr(beam);
        else              QUADRUPOLE::step_6d_tl(beam);
      } else {
        if (flags.synrad) QUADRUPOLE::step_4d_tl_sr(beam);
        else              QUADRUPOLE::step_4d_tl(beam);
      }
    } else {
      if (flags.longitudinal) {
        if (flags.synrad) QUADRUPOLE::step_6d_sr(beam);
        else              QUADRUPOLE::step_6d(beam);
      } else {
        if (flags.synrad) QUADRUPOLE::step_4d_sr(beam);
        else              QUADRUPOLE::step_4d(beam);
      }
    }
  }
  geometry.length=l;
  strength=s;
}

void QUADRUPOLE::step_in(BEAM *beam )
{
  double _offset=0.5*geometry.length*offset.xp-offset.x;
  if (fabs(_offset)>std::numeric_limits<double>::epsilon() || fabs(offset.xp)>std::numeric_limits<double>::epsilon()) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.x +=_offset;
      particle.xp-= offset.xp;
    }
#ifdef HTGEN
    // shift halo particles in x as beam particles to take offsets from misalignment into account
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.x +=_offset;
      particle_sec.xp-= offset.xp;
    }
#endif
  }
  _offset=0.5*geometry.length*offset.yp-offset.y;
  if (fabs(_offset)>std::numeric_limits<double>::epsilon() || fabs(offset.yp)>std::numeric_limits<double>::epsilon()) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.y +=_offset;
      particle.yp-= offset.yp;
    }
#ifdef HTGEN
    // shift halo particles in y as beam particles to take offsets from misalignment into account
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.y +=_offset;
      particle_sec.yp-= offset.yp;
    }
#endif
  }
  bunch_rotate(beam,offset.roll+tilt);      
}

void QUADRUPOLE::step_out(BEAM *beam )
{
  bunch_rotate(beam,-offset.roll-tilt);
  double _offset=0.5*geometry.length*offset.yp+offset.y;
  if (fabs(_offset)>std::numeric_limits<double>::epsilon() || fabs(offset.yp)>std::numeric_limits<double>::epsilon()) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.y +=_offset;
      particle.yp+= offset.yp;
    }
#ifdef HTGEN
    // shift halo particles in y as beam particles to take offsets from misalignment into account
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.y +=_offset;
      particle_sec.yp+= offset.yp;
    }
#endif
  }
  _offset=0.5*geometry.length*offset.xp+offset.x;
  if (fabs(_offset)>std::numeric_limits<double>::epsilon() || fabs(offset.xp)>std::numeric_limits<double>::epsilon()) {
    for (int i=0;i<beam->slices;i++) {
      PARTICLE &particle=beam->particle[i];
      particle.x +=_offset;
      particle.xp+= offset.xp;
    }
#ifdef HTGEN
    // shift halo particles in x as beam particles to take offsets from misalignment into account
    for (int j=0;j<beam->nhalo;j++) {
      PARTICLE &particle_sec=beam->particle_sec[j];
      particle_sec.x +=_offset;
      particle_sec.xp+= offset.xp;
    }
#endif
  }
}

Matrix<6,6> QUADRUPOLE::get_transfer_matrix_6d(double _energy ) const 
{
  Matrix<6,6> R(0.0);
  R[4][4]=1.0;
  R[5][5]=1.0;
  if (fabs(strength)<std::numeric_limits<double>::epsilon()) {
    R[0][0]=1.0;
    R[0][1]=geometry.length;
    R[1][1]=1.0;
    R[2][2]=1.0;
    R[2][3]=geometry.length;
    R[3][3]=1.0;
  } else if (fabs(geometry.length)<std::numeric_limits<double>::epsilon()) {
    if (_energy == -1.0)
      _energy = ref_energy;
    double k=strength/_energy;
    R[0][0]=1.0;
    R[1][0]=+k;
    R[1][1]=1.0;
    R[2][2]=1.0;
    R[3][2]=-k;
    R[3][3]=1.0;
  } else {
    if (_energy == -1.0)
      _energy = ref_energy;
    double k=strength/geometry.length/_energy;
    if (k>0.0) {
      double ksqrt=sqrt(k);
      double c,s;
      sincosh(ksqrt*geometry.length, s, c);
      R[0][0]=c;
      R[0][1]=s/ksqrt;
      R[1][0]=s*ksqrt;
      R[1][1]=c;
      sincos(ksqrt*geometry.length, &s, &c);
      R[2][2]=c;
      R[2][3]=s/ksqrt;
      R[3][2]=-s*ksqrt;
      R[3][3]=c;
    } else {
      double ksqrt=sqrt(-k);
      double c;
      double s;
      sincos(ksqrt*geometry.length, &s, &c);
      R[0][0]=c;
      R[0][1]=s/ksqrt;
      R[1][0]=-s*ksqrt;
      R[1][1]=c;
      sincosh(ksqrt*geometry.length, s, c);
      R[2][2]=c;
      R[2][3]=s/ksqrt;
      R[3][2]=s*ksqrt;
      R[3][3]=c;
    }
    if (fabs(tilt)>std::numeric_limits<double>::epsilon()) {
      double s,c;
      sincos(tilt,&s,&c);
      Matrix<6,6> Rot(0.0);
      Rot[0][0]=Rot[1][1]=Rot[2][2]=Rot[3][3]=c;
      Rot[0][2]=Rot[1][3]=-s;
      Rot[2][0]=Rot[3][1]=s;
      Rot[4][4]=1.0;
      Rot[5][5]=1.0;
      R*=Rot;
      Rot[0][2]=Rot[1][3]=s;
      Rot[2][0]=Rot[3][1]=-s;
      R=Rot*R;
    }
  }
  return R;
}

void QUADRUPOLE::GetMagField(PARTICLE *particle, double *bfield){
  if(geometry.length<std::numeric_limits<double>::epsilon()){
    bfield[0]=0.0;
    bfield[1]=0.0;
    bfield[2]=0.0;    
  }
  else{
    // get_ref_energy() gives the reference energy for the given element
    double g=-strength*3.3356409520e-6/get_ref_energy()/geometry.length; 
    bfield[0]=-g*(particle->y-offset.y);
    bfield[1]=-g*(particle->x-offset.x);
    bfield[2]=0.0;
  }
}

