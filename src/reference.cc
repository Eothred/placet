#include <cstdlib>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"

#include "lattice.h"
#include "girder.h"
#include "reference.h"

extern INTER_DATA_STRUCT inter_data;

int tk_ReferencePoint(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		      char *argv[])
{
  int error,sens=1,angle=0;
  REFERENCE *element;
  const char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-angle",TK_ARGV_INT,(char*)NULL,(char*)&angle,
     (char*)"if not 0, the element will also be an angle reference point"},
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,    
     (char*)"Name to be assigned to the element"},
    {(char*)"-sense",TK_ARGV_INT,(char*)NULL,(char*)&sens,
     (char*)"sense of the reference point (1=first, -1 =last)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <ReferencePoint>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  
  element=new REFERENCE(sens, angle);
  element->set_name(element_name);
  inter_data.girder->add_element(element);
  return TCL_OK;
}

void
REFERENCE::step_4d_0(BEAM* beam)
{
  if (sens>0) {
    for (int i=0;i<beam->slices;i++){
      beam->particle[i].x+=offset.x;
      beam->particle[i].y+=offset.y;
      if (angle!=0) {
	beam->particle[i].xp+=offset.xp;
	beam->particle[i].yp+=offset.yp;
      }
    }
  } else {
    for (int i=0;i<beam->slices;i++){
      beam->particle[i].x-=offset.x;
      beam->particle[i].y-=offset.y;
      if (angle!=0) {
	beam->particle[i].xp-=offset.xp;
	beam->particle[i].yp-=offset.yp;
      }
    }
  }
}

void
REFERENCE::step_4d(BEAM* beam)
{
  if (sens>0) {
    for (int i=0;i<beam->slices;i++){
      beam->particle[i].x+=offset.x;
      beam->particle[i].y+=offset.y;
      if (angle!=0) {
	beam->particle[i].xp+=offset.xp;
	beam->particle[i].yp+=offset.yp;
      }
    }
  } else {
    for (int i=0;i<beam->slices;i++){
      beam->particle[i].x-=offset.x;
      beam->particle[i].y-=offset.y;
      if (angle!=0) {
	beam->particle[i].xp-=offset.xp;
	beam->particle[i].yp-=offset.yp;
      }
    }
  }
}

void
REFERENCE::list(FILE* f)const
{
  fprintf(f,"ReferencePoint -angle %d -sens %d\n",angle,sens);
}
