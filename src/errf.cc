#include <cmath>
#include "errf.h"

#ifndef PI
#define PI 3.141592653589793
#endif

/* the error function */

namespace errf { // put in namespace since cmath has an erfc method as well (should we use that one? reference for this implementation?)
  double erfc(double x)
  {
    double t,z,ret_val;
    
    z=fabs(x);
    t=1.0/(z*0.5+1.0);
    ret_val=t*exp(-z*z-1.26551223+t*(t*(t*(t*(t*(t*(t*(t*
						       (t*0.17087277-0.82215223)+1.48851587)-1.13520398)
	      +0.27886807)-0.18628806)+0.09678418)+0.37409196)+1.00002368));
    if (x<0.0) {
      ret_val=2.0-ret_val;
    }
    return ret_val;
  }
}

/* The integral of a Gaussian distribution
 a,b are given in terms of sigma 
*/

double gauss_bin(double a,double b)
{
  double s;
  if (a>=b) return 0.0;
  s=1.0/sqrt(2.0);
  a*=s;
  b*=s;
  return 0.5*(errf::erfc(a)-errf::erfc(b));
}
