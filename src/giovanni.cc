#include <cmath>
#include <fstream>
#include <typeinfo>

#include <tcl.h>
#include <tk.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>

#include "beamline.h"
#include "collimator.h"
#include "lattice.h"
#include "spline.h"
#include "structures_def.h"

#include "collimatortable.h"

/* Function prototypes */

#include "girder.h"

#include "placet_prefix.hh"

using namespace std;

static double alfat, kt, s0_loc;       /* needed global, they appear in functions */

static gsl_integration_workspace * w1, *w2;
#define WORKSPACE_LIMIT 50000
struct _dummy {
  _dummy() { w1 = gsl_integration_workspace_alloc (WORKSPACE_LIMIT); w2 = gsl_integration_workspace_alloc (WORKSPACE_LIMIT); }
  ~_dummy() { gsl_integration_workspace_free (w1); gsl_integration_workspace_free (w2); }
} __dummy; 

static inline double res_lr_dc2(double z, double zz)
{
  return 1/sqrt(z-zz);
}

static inline double res_sr_ac2(double z, double zz)
{
  return -s0_loc/3/(kt*kt+alfat*alfat)*
    (exp(-alfat*(z-zz)/s0_loc)*(kt*sin(kt*(z-zz)/s0_loc)-alfat*cos(kt*(z-zz)/s0_loc))+alfat);
}

static double fint(double x, void * params)
{
  double alpha = *(double *) params;
  double xx=x*x;
  return xx*exp(-xx*alpha)/(xx*xx*xx+8);
}

static double intmed(double zp, void * params)
{
  double alpha,result,error;
  double alpha1 = *(double *) params;
  
  static gsl_function F;
  
  //  gsl_integration_workspace * w 
  //    = gsl_integration_workspace_alloc (WORKSPACE_LIMIT);  
  
  //alpha1=s0_loc
  
  //placet_printf(INFO,"\n upper bound = %lg \n",2.*sqrt(alpha1/2./fabs(zp)));
  alpha=fabs(zp)/alpha1;
  F.function = &fint;
  F.params = &alpha;
  gsl_integration_qag (&F, 1e-20, 2.*sqrt(alpha1/2./fabs(zp)), 1e-8, 1e-5, WORKSPACE_LIMIT, 6, w1, &result, &error);

	// http://www.gnu.org/software/gsl/manual/html_node/QAG-adaptive-integration.html
	/* Function: int gsl_integration_qag (const gsl_function *f, double a, double b, double epsabs, double epsrel, size_t limit, int key, gsl_integration_workspace * workspace, double * result, double * abserr)
	 
	 This function applies an integration rule adaptively until an estimate of the integral of f over (a,b) is achieved within the desired absolute and relative error limits, epsabs and epsrel. The function returns the final approximation, result, and an estimate of the absolute error, abserr. The integration rule is determined by the value of key, which should be chosen from the following symbolic names,
	 
	 GSL_INTEG_GAUSS15  (key = 1)
	 GSL_INTEG_GAUSS21  (key = 2)
	 GSL_INTEG_GAUSS31  (key = 3)
	 GSL_INTEG_GAUSS41  (key = 4)
	 GSL_INTEG_GAUSS51  (key = 5)
	 GSL_INTEG_GAUSS61  (key = 6)
	 
	 corresponding to the 15, 21, 31, 41, 51 and 61 point Gauss-Kronrod rules. The higher-order rules give better accuracy for smooth functions, while lower-order rules save time when the function contains local difficulties, such as discontinuities.
	 
	 On each iteration the adaptive integration strategy bisects the interval with the largest error estimate. The subintervals and their results are stored in the memory provided by workspace. The maximum number of subintervals is given by limit, which may not exceed the allocated size of the workspace. 
	 */	
	// source code: http://gsl.sourcearchive.com/documentation/1.6/qag_8c-source.html
	
	// should a test if the integration reached the right error be done?
  return result;

  //  intmed=result;
//  gsl_integration_workspace_free (w);
  //return intmed;
}

static double intmed22(double z, double zz, double blength)
{ 
  //double intmed2;
  double alpha1,result,error;
  static gsl_function F;
    
  //    gsl_integration_workspace * w 
  //      = gsl_integration_workspace_alloc (WORKSPACE_LIMIT);  
  
  //set2[0]=s0_loc
  //set2[1]=zp
  //set2[2]=sz0
  
  //placet_printf(INFO,"\n lower bound = %lg  upper bound = %lg \n", 1.e-5*set2[2],fabs(z));
  alpha1=s0_loc;
  F.function = &intmed; // using the static function intmed
  F.params = &alpha1;
  /* gsl_integration_qag (&F, 1.e-5*blength, blength, 0, 1e-5, WORKSPACE_LIMIT, 5, w2, &result, &error);
     inttemp=result;
     gsl_integration_qag (&F, fabs(z-zz), blength, 0, 1e-5, WORKSPACE_LIMIT, 5, w2, &result, &error);
     inttemp-=result;
  */
  
  gsl_integration_qag (&F, 1.e-5*blength, fabs(z-zz), 1e-8, 1e-5, WORKSPACE_LIMIT, 6, w2, &result, &error);
  // see intmed() for explanation of this function

  return result;

  //  inttemp=result;
  //intmed2=inttemp;
  //    gsl_integration_workspace_free (w);
  //return intmed2;

}

static inline void findpoles(double cg)
{  
  if (cg<0.2) {
    alfat=1.;
    kt=SQRT3;
  }
  else if (cg>=0.2 && cg<0.4) {
    alfat=0.74;
    kt=1.8;
  }
  else if (cg>=0.4 && cg<0.7) {
    alfat=0.52;
    kt=1.87;
  }
  else if (cg>=0.7) {
    alfat=1./4./cg;
    kt=pow(8./cg,0.25);
  }
}

COLLIMATOR::COLLIMATOR(double bb, double gg, double ww, double LT, double Lflat, double sig_ch, double tau_ch, double _charge, int corr, bool vert ) : correct_offset(corr), in_height(bb), fin_height(gg), width(ww), taper_length(LT),flat_length(Lflat), sigma(sig_ch), tau(tau_ch), Nb(_charge), giovanni(true), vertical(vert)
{
  // initialise other members:
  dz=0., dzi=0., zmin=0., zmax=0., wtx=0, wty=0, rhox=0, rhoy=0, spx=0, spy=0; 
  
}

void COLLIMATOR::giovanni_step_4d_0(BEAM *beam )
{
  double s0_in=pow((2*pow(in_height,2)/Z0/sigma),1./3.);
  double s0_fin=pow((2*pow(fin_height,2)/Z0/sigma),1./3.);
  //  placet_printf(INFO,"\n s0_fin = %lg  s0_in = %lg \n", s0_fin,s0_in);
  double cap_gam=C_LIGHT*tau/s0_in;
  double lambda=1/Z0/sigma;
  double angle=atan((in_height-fin_height)/taper_length);         /* tapering angle        */
  const int imax = 10; // number of steps for the taper
  drift_step_4d_0(beam, 0.5); // track beam as drift the first half of the collimator
  int k=0;
  for (int ibunch=0;ibunch<beam->bunches;ibunch++) {
    const int NSL=beam->slices_per_bunch;
    double deltayarray[NSL];
    double Np[NSL]; // number of particles per slice
    if (vertical) {
      if (beam->particle_beam) {
	for (int i=0;i<beam->slices_per_bunch; i++) {
	  double deltay=0.0;
	  double wgtsum=0.0;
	    for (int j=0;j<beam->particle_number[i]; j++) {
	      PARTICLE &particle=beam->particle[k];
	      double wgt=particle.wgt;
	      deltay += particle.y * wgt;
	      wgtsum += wgt;
	      k++;
	    }
	    deltayarray[i]=deltay/wgtsum*1e-6;
	    Np[i]=wgtsum*Nb;
	  }
	} else {
	  for (int i=0;i<beam->slices_per_bunch; i++) {
	    double deltay=0.0;
	    double wgtsum=0.0;
	    for (int j=0;j<beam->macroparticles; j++) {
	      PARTICLE &particle=beam->particle[ibunch*beam->slices_per_bunch*beam->macroparticles+i*beam->macroparticles+j];
	      double wgt=particle.wgt;
	      deltay += particle.y * wgt;
	      wgtsum += wgt;
	    }
	    deltayarray[i]=deltay/wgtsum*1e-6;
	    Np[i]=wgtsum*Nb;
	  }
	}
    } else {
      if (beam->particle_beam) {
	for (int i=0;i<beam->slices_per_bunch; i++) {
	  double deltay=0.0;
	  double wgtsum=0.0;
	    for (int j=0;j<beam->particle_number[i]; j++) {
	      PARTICLE &particle=beam->particle[k];
	      double wgt=particle.wgt;
	      deltay += particle.x * wgt;
	      wgtsum += wgt;
	      k++;
	    }
	    deltayarray[i]=deltay/wgtsum*1e-6;
	    Np[i]=wgtsum*Nb;
	}
      } else {
	for (int i=0;i<beam->slices_per_bunch; i++) {
	  double deltay=0.0;
	  double wgtsum=0.0;
	  for (int j=0;j<beam->macroparticles; j++) {
	    PARTICLE &particle=beam->particle[ibunch*beam->slices_per_bunch*beam->macroparticles+i*beam->macroparticles+j];
	    double wgt=particle.wgt;
	    deltay += particle.x * wgt;
	    wgtsum += wgt;
	  }
	  deltayarray[i]=deltay/wgtsum*1e-6;
	  Np[i]=wgtsum*Nb;
	}
      }
    }    
    
    double kickyrwt=0.;
    double kickyrwt2=0.;
    double kickyrwf=0.;
    double kickyrwf2=0.;
    double kickyrwt_a=0.;
    double kickyrwt2_a=0.;
    double kickyrwf_a=0.;
    double kickyrwf2_a=0.;
    double kickyrwt_b=0.;
    double kickyrwt2_b=0.;
    double kickyrwf_b=0.;
    double kickyrwf2_b=0.;

    double sz0=bunch_get_length(beam, ibunch)*1e-6;
    double diffz=(beam->z_position[1+ibunch*beam->slices_per_bunch]-beam->z_position[0+ibunch*beam->slices_per_bunch])*1e-6;
    double blength=diffz*beam->slices_per_bunch;

    int regime_geometric=0;
    if (angle<0.1*fin_height*sz0/width/width) {               /* inductive regime      */
      regime_geometric=1;
      //      placet_printf(INFO,"\n geometric regime is inductive\n");
    } else if (angle>10.*fin_height*sz0/width/width) {        /* diffractive regime    */ 
      regime_geometric=2;
      //      placet_printf(INFO,"\n geometric regime is diffractive\n");
      //    } else {                                                  /* intermediate regime   */
      //      placet_printf(INFO,"\n geometric regime is intermediate\n");
    }
      
    int regime_resistive=0;                                   /* intermediate regime   */
    if (sz0>10.*s0_in) {                                      /* long range regime     */
      regime_resistive=1;
      //      placet_printf(INFO,"\n resistive regime is long-range\n");
    } else if (sz0<0.1*s0_fin) {                              /* short range regime    */
      regime_resistive=2;
      //      placet_printf(INFO,"\n resistive regime is short-range\n");
      //    } else {                                                  /* intermediate regime   */
      //      placet_printf(INFO,"\n resistive regime is intermediate\n");
    }
      
    findpoles(cap_gam);
    //    placet_printf(INFO,"\n alfat = %lg  kt = %lg \n", alfat, kt);

    k=0; // if there is more than 1 bunch??
    
    if (beam->particle_beam) {
      for (int i=0;i<beam->slices_per_bunch;i++) {
	if (regime_resistive == 0) {
	  kickyrwt_a=0.;
	  kickyrwf_a=0.;
	  kickyrwt2_a=0.;
	  kickyrwf2_a=0.;
	  kickyrwt_b=0.;
	  kickyrwf_b=0.;
	  kickyrwt2_b=0.;
	  kickyrwf2_b=0.;
	  double bb_loc;
	  double bbbb_loc;
	  double zp=-beam->z_position[ibunch*beam->slices_per_bunch+i]*1e-6;
	  for(int imain=0;imain<=imax;imain++) { 
	    bb_loc=in_height-(in_height-fin_height)/(double)imax*(double)imain;
            bbbb_loc=bb_loc*bb_loc;
	    s0_loc=pow((2*bbbb_loc/Z0/sigma),1./3.);
	    double cap_gam_loc=C_LIGHT*tau/s0_loc;
	    findpoles(cap_gam_loc);
	    double coeffrwt=32*RE*(taper_length/(double)imax)/(bbbb_loc*bbbb_loc);
	    double coeffrwt2=32*RE*(taper_length/(double)imax)*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
	    /* kmain=0 is the head of the bunch */
	    for(int kmain=0;kmain<i;kmain++) {
              double _res_sr_ac2=res_sr_ac2(blength/2-zp, (double)kmain*diffz);
	      kickyrwt_a+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain])*_res_sr_ac2;
	      double _intmed22=intmed22(blength/2-zp, (double)kmain*diffz, blength);
              kickyrwt2_a+=Np[kmain]*coeffrwt2*(0.822*deltayarray[kmain])*_intmed22;
	      kickyrwt_b+=Np[kmain]*coeffrwt*(0.411)*_res_sr_ac2;
	      kickyrwt2_b+=Np[kmain]*coeffrwt2*(0.411)*_intmed22;
	    } 
	  }	   
	  if(flat_length>0.) {
	    // double coeffrwf=32*RE*flat_length/pow(fin_height,4);
	    // double coeffrwf2=32*RE*flat_length*M_SQRT2/M_PI/pow(fin_height,4);
	    double coeffrwf=32*RE*flat_length/(bbbb_loc*bbbb_loc);
	    double coeffrwf2=32*RE*flat_length*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
	    for(int kmain=0;kmain<i;kmain++) {
              double _res_sr_ac2=res_sr_ac2(blength/2-zp, (double)kmain*diffz);
	      kickyrwf_a+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain])*_res_sr_ac2;
	      double _intmed22=intmed22(blength/2-zp, (double)kmain*diffz, blength);
	      kickyrwf2_a+=Np[kmain]*coeffrwf2*(0.822*deltayarray[kmain])*_intmed22;
	      kickyrwf_b+=Np[kmain]*coeffrwf*(0.411)*_res_sr_ac2;
	      kickyrwf2_b+=Np[kmain]*coeffrwf2*(0.411)*_intmed22;
	    }   
	  }
	}
	for (int jj=0; jj<beam->particle_number[i];jj++) {
	  PARTICLE &particle=beam->particle[k++];
	  double yp;
	  if (vertical) {
	    yp=particle.y*1e-6; /* coordinates of the particle that feels the kick */
	  } else {
	    yp=particle.x*1e-6; /* coordinates of the particle that feels the kick */
	  }
	  double zp=-beam->z_position[ibunch*beam->slices_per_bunch+i]*1e-6;
	  double gam=particle.energy/EMASS;
	  double kicky;
	  //	  double kicky_old;
	  
	  switch(regime_geometric) {
	  case 1:
	    
	    kicky=RE/gam*
	      ((M_PI*width*(in_height-fin_height)*(in_height-fin_height)*
		(in_height+fin_height)/taper_length/in_height/in_height/
		fin_height/fin_height-2*(in_height-fin_height)*
		(in_height-fin_height)/in_height/fin_height/taper_length)*deltayarray[i]
	       +2*(in_height-fin_height)*(in_height-fin_height)/
	       in_height/fin_height/taper_length*yp)*Np[i]/diffz;
	    //	    kicky_old=2*RE/gam/(in_height-fin_height)*(in_height-fin_height)/
	    //	      in_height/fin_height/taper_length*(0.822*deltayarray[i]+0.411*yp)*Np[i]/diffz;
	    break;
	    
	  case 0:
	    
	    kicky=RE*2.7*sqrt(2*angle)/gam*sqrt(2*M_PI*sz0)/sqrt(fin_height)*(0.822*deltayarray[i]+0.411*yp)*Np[i]/diffz;
	    break;
	    
	  case 2: {
            double fih=fin_height/in_height;
            fih*=fih;
	    kicky=2*RE*sz0*SQRT_PI*(1-(fih*fih))/fin_height/fin_height/gam
	      *(0.822*deltayarray[i]+0.411*yp)*Np[i]/diffz;
            }
	    break;    
	  }
	  
	  switch(regime_resistive) {
	  case 1:	  
	    {
	      kickyrwt=0.;
	      kickyrwf=0.;
              double ffh=fin_height*fin_height;
	      double coeffrwt=2*RE*sqrt(lambda/M_PI)/gam*(in_height+fin_height)*taper_length/ffh/in_height/in_height;
	      double coeffrwf=2*RE*sqrt(lambda/M_PI)/gam*flat_length/(fin_height*ffh);
	      int ind_slice=int(floor((blength/2-zp)/diffz));
	      
	      /* kmain=0 is the head of the bunch */
	      
	      for(int kmain=0;kmain<ind_slice;kmain++) {
		
		kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*res_lr_dc2(blength/2-zp, (double)kmain*diffz);
		if(flat_length>0.)
		  kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*res_lr_dc2(blength/2-zp, (double)kmain*diffz);
		
	      }
	    }
	    break;
	    
	  case 2:
	    {
	      int ind_slice=int(floor((blength/2-zp)/diffz));
	      kickyrwt=0.;
	      kickyrwf=0.;
	      double bb_loc;
	      double bbbb_loc;
	      for(int imain=0;imain<imax;imain++) {
		bb_loc=in_height-(in_height-fin_height)/(double)imax*(double)imain;
		bbbb_loc=bb_loc*bb_loc;
		s0_loc=pow((2*bbbb_loc/Z0/sigma),1./3.);
		double cap_gam_loc=C_LIGHT*tau/s0_loc;
		findpoles(cap_gam_loc);
		double coeffrwt=32*RE*(taper_length/(double)imax)/gam/(bbbb_loc*bbbb_loc);
		/* kmain=0 is the head of the bunch */
		for(int kmain=0;kmain<ind_slice;kmain++)
		  kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*res_sr_ac2(blength/2-zp, (double)kmain*diffz);
	      }
	      if(flat_length>0.) {
		double coeffrwf=32*RE*flat_length/gam/(bbbb_loc*bbbb_loc);
		for(int kmain=0;kmain<ind_slice;kmain++)
		  kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*res_sr_ac2(blength/2-zp, (double)kmain*diffz);
	      }
	    }
	    break;
	    
	  case 0:
	    {
	      // 	      kickyrwt_xa=0.;
	      // 	      kickyrwt_xb=0.;
	      // unused:    int ind_slice=int(floor((blength/2-zp)/diffz));
	      /*	      
  	      kickyrwt=0.;
	      kickyrwf=0.;
	      kickyrwt2=0.;
	      kickyrwf2=0.;
	      double bb_loc;
	      for(int imain=0;imain<=imax;imain++) {
	      bb_loc=in_height-(in_height-fin_height)/(double)imax*(double)imain;
	      s0_loc=pow((2*bbbb_loc/Z0/sigma),1./3.);
	      double cap_gam_loc=C_LIGHT*tau/s0_loc;
	      findpoles(cap_gam_loc);
	      double coeffrwt=64*RE*(taper_length/(double)imax)/gam/(bbbb_loc*bbbb_loc);
	      double coeffrwt2=64*RE*(taper_length/(double)imax)/gam*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
	      // kmain=0 is the head of the bunch 
	      for(int kmain=0;kmain<ind_slice;kmain++) {
	      kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*
	      _res_sr_ac2;
	      kickyrwt_xa+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain])*
	      _res_sr_ac2;
	      kickyrwt_xb+=Np[kmain]*coeffrwt*(0.411)*
	      _res_sr_ac2;
	      kickyrwt2+=Np[kmain]*coeffrwt2*(0.822*deltayarray[kmain]+0.411*yp)*
	      intmed22(blength/2-zp, (double)kmain*diffz, blength);
	      } 
	      }
	      
	      if(flat_length>0.) {
	      double coeffrwf=32*RE*flat_length/gam/(bbbb_loc*bbbb_loc);
	      double coeffrwf2=32*RE*flat_length/gam*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
	      for(int kmain=0;kmain<ind_slice;kmain++) {   
	      kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*
	      _res_sr_ac2;
	      kickyrwf2+=Np[kmain]*coeffrwf2*(0.822*deltayarray[kmain]+0.411*yp)*
	      intmed22(blength/2-zp, (double)kmain*diffz, blength);
	      }   
	      }
	      */
	      kickyrwt=(kickyrwt_a+kickyrwt_b*yp)/gam;
	      kickyrwt2=(kickyrwt2_a+kickyrwt2_b*yp)/gam;
	      if(flat_length>0.) {
		kickyrwf=(kickyrwf_a+kickyrwf_b*yp)/gam;
		kickyrwf2=(kickyrwf2_a+kickyrwf2_b*yp)/gam;
	      }
	    }
	    break;	 
	  }
	  
	  if (vertical) 
	    particle.yp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  else
	    particle.xp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  
	  //	placet_printf(INFO,"kick: %g %g %g %g %g\n", zp*1e6, kicky*1e6, (kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6, kickyrwf*1e6,  kickyrwf2*1e6);
	} 
      }
    } else {
      for (int slice=0;slice<beam->slices_per_bunch;slice++) {
	if (regime_resistive == 0) {
	  kickyrwt_a=0.;
	  kickyrwf_a=0.;
	  kickyrwt2_a=0.;
	  kickyrwf2_a=0.;
	  kickyrwt_b=0.;
	  kickyrwf_b=0.;
	  kickyrwt2_b=0.;
	  kickyrwf2_b=0.;
	  double bb_loc;
	  double bbbb_loc;
	  // negative z
	  double zp=-beam->z_position[ibunch*beam->slices_per_bunch+slice]*1e-6;
	  for(int imain=0;imain<=imax;imain++) { 
	    bb_loc=in_height-(in_height-fin_height)/(double)imax*(double)imain;
	    bbbb_loc=bb_loc*bb_loc;
	    s0_loc=pow((2*bbbb_loc/Z0/sigma),1./3.);
	    double cap_gam_loc=C_LIGHT*tau/s0_loc;
	    findpoles(cap_gam_loc); // why is this done? alfat and kt are not used here? // JS
	    double coeffrwt=32*RE*(taper_length/(double)imax)/(bbbb_loc*bbbb_loc);
	    double coeffrwt2=32*RE*(taper_length/(double)imax)*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
	    /* kmain=0 is the head of the bunch */
	    for(int kmain=0;kmain<slice;kmain++) {
              double _res_sr_ac2=res_sr_ac2(blength/2-zp, (double)kmain*diffz);
	      kickyrwt_a+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain])*_res_sr_ac2;
	      double _intmed22=intmed22(blength/2-zp, (double)kmain*diffz, blength);
	      kickyrwt2_a+=Np[kmain]*coeffrwt2*(0.822*deltayarray[kmain])*_intmed22;
	      kickyrwt_b+=Np[kmain]*coeffrwt*(0.411)*_res_sr_ac2;
	      kickyrwt2_b+=Np[kmain]*coeffrwt2*(0.411)*_intmed22;
	    }
	  }

	  if(flat_length>0.) {
	    // double coeffrwf=32*RE*flat_length/pow(fin_height,4);
	    // double coeffrwf2=32*RE*flat_length*M_SQRT2/M_PI/pow(fin_height,4);
	    double coeffrwf=32*RE*flat_length/(bbbb_loc*bbbb_loc);
	    double coeffrwf2=32*RE*flat_length*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
	    for(int kmain=0;kmain<slice;kmain++) {   
              double _res_sr_ac2=res_sr_ac2(blength/2-zp, (double)kmain*diffz);
	      kickyrwf_a+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain])*_res_sr_ac2;
	      double _intmed22=intmed22(blength/2-zp, (double)kmain*diffz, blength);
	      kickyrwf2_a+=Np[kmain]*coeffrwf2*(0.822*deltayarray[kmain])*_intmed22;
	      kickyrwf_b+=Np[kmain]*coeffrwf*(0.411)*_res_sr_ac2;
	      kickyrwf2_b+=Np[kmain]*coeffrwf2*(0.411)*_intmed22;
	    }
	  }
	}

	for (int jj=0;jj<beam->macroparticles;jj++) {
	  PARTICLE &particle=beam->particle[(ibunch*beam->slices_per_bunch+slice)*beam->macroparticles+jj];
	  double yp;
	  if (vertical) {
	    yp=particle.y*1e-6; /* coordinates of the particle that feels the kick */
	  } else {
	    yp=particle.x*1e-6; /* coordinates of the particle that feels the kick */
	  }
	  double zp=beam->z_position[ibunch*beam->slices_per_bunch + slice]*1e-6;
	  double gam=particle.energy/EMASS;
	  double kicky;
	  //	  double kicky_old;
	  
	  switch(regime_geometric) {
	  case 1:
	    
	    kicky=RE/gam*
	      ((M_PI*width*(in_height-fin_height)*(in_height-fin_height)*
		(in_height+fin_height)/taper_length/in_height/in_height/
		fin_height/fin_height-2*(in_height-fin_height)*
		(in_height-fin_height)/in_height/fin_height/taper_length)*deltayarray[slice]
	       +2*(in_height-fin_height)*(in_height-fin_height)/
	       in_height/fin_height/taper_length*yp)*Np[slice]/diffz;
	    //	    kicky_old=2*RE/gam/(in_height-fin_height)*(in_height-fin_height)/
	    //	      in_height/fin_height/taper_length*(0.822*deltayarray[slice]+0.411*yp)*Np[slice]/diffz;
	    break;
	    
	  case 0:
	    
	    kicky=RE*2.7*sqrt(2*angle)/gam*sqrt(2*M_PI*sz0)/sqrt(fin_height)*(0.822*deltayarray[slice]+0.411*yp)*Np[slice]/diffz;
	    break;
	    
	  case 2: {
            double fih=fin_height/in_height;
            fih*=fih;
	    kicky=RE*sz0*sqrt(4*M_PI)*(1-fih*fih)/fin_height/fin_height/gam
	      *(0.822*deltayarray[slice]+0.411*yp)*Np[slice]/diffz;
	    }
            break;    
	  }
	  
	  switch(regime_resistive) {
	  case 1:	  
	    {
	      kickyrwt=0.;
	      kickyrwf=0.;
              double ffh=fin_height*fin_height;
	      double coeffrwt=2*RE*sqrt(lambda/M_PI)/gam*(in_height+fin_height)*taper_length/ffh/in_height/in_height;
	      double coeffrwf=2*RE*sqrt(lambda/M_PI)/gam*flat_length/(fin_height*ffh);
	      int ind_slice=int(floor((blength/2-zp)/diffz));
	      
	      /* kmain=0 is the head of the bunch */
	      
	      for(int kmain=0;kmain<ind_slice;kmain++) {
		
		kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*res_lr_dc2(blength/2-zp, (double)kmain*diffz);
		if(flat_length>0.)
		  kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*res_lr_dc2(blength/2-zp, (double)kmain*diffz);
		
	      }
	    }
	    break;
	    
	  case 2:
	    {
	      int ind_slice=int(floor((blength/2-zp)/diffz));
	      kickyrwt=0.;
	      kickyrwf=0.;
              double bb_loc;
              double bbbb_loc;
	      for(int imain=0;imain<imax;imain++) {
		bb_loc=in_height-(in_height-fin_height)/(double)imax*(double)imain;
                bbbb_loc=bb_loc*bb_loc;
		s0_loc=pow((2*bbbb_loc/Z0/sigma),1./3.);
		double cap_gam_loc=C_LIGHT*tau/s0_loc;
		findpoles(cap_gam_loc);
		double coeffrwt=32*RE*(taper_length/(double)imax)/gam/(bbbb_loc*bbbb_loc);
		/* kmain=0 is the head of the bunch */
		for(int kmain=0;kmain<ind_slice;kmain++)
		  kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*res_sr_ac2(blength/2-zp, (double)kmain*diffz);
	      }
	      if(flat_length>0.) {
		double coeffrwf=32*RE*flat_length/gam/(bbbb_loc*bbbb_loc);
		for(int kmain=0;kmain<ind_slice;kmain++)
		  kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*res_sr_ac2(blength/2-zp, (double)kmain*diffz);
	      }
	    }
	    break;
	    
	  case 0:
	    {
	      // 	      kickyrwt_xa=0.;
	      // 	      kickyrwt_xb=0.;
	      // unused:    int ind_slice=int(floor((blength/2-zp)/diffz));
	      /*	      
			     kickyrwt=0.;
			     kickyrwf=0.;
			     kickyrwt2=0.;
			     kickyrwf2=0.;
			     double bb_loc;
			     for(int imain=0;imain<imax;imain++) {
			     bb_loc=in_height-(in_height-fin_height)/(double)imax*(double)imain;
			     s0_loc=pow((2*pow(bb_loc,2.)/Z0/sigma),1./3.);
			     double cap_gam_loc=C_LIGHT*tau/s0_loc;
			     findpoles(cap_gam_loc);
			     double coeffrwt=64*RE*(taper_length/(double)imax)/gam/(bbbb_loc*bbbb_loc);
			     double coeffrwt2=64*RE*(taper_length/(double)imax)/gam*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
			     // kmain=0 is the head of the bunch 
			     for(int kmain=0;kmain<ind_slice;kmain++) {
			     kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*
			     _res_sr_ac2;
			     kickyrwt2+=Np[kmain]*coeffrwt2*(0.822*deltayarray[kmain]+0.411*yp)*
			     intmed22(blength/2-zp, (double)kmain*diffz, blength);	 
			     } 
			     }
			     if(flat_length>0.) {
			     double coeffrwf=32*RE*flat_length/gam/(bbbb_loc*bbbb_loc);
			     double coeffrwf2=32*RE*flat_length/gam*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
			     for(int kmain=0;kmain<ind_slice;kmain++) {   
			     kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*
			     _res_sr_ac2;
			     kickyrwf2+=Np[kmain]*coeffrwf2*(0.822*deltayarray[kmain]+0.411*yp)*
			     intmed22(blength/2-zp, (double)kmain*diffz, blength);
			     }   
			     }
	      */
	      kickyrwt=(kickyrwt_a+kickyrwt_b*yp)/gam;
	      kickyrwt2=(kickyrwt2_a+kickyrwt2_b*yp)/gam;
	      if(flat_length>0.) {
		kickyrwf=(kickyrwf_a+kickyrwf_b*yp)/gam;
		kickyrwf2=(kickyrwf2_a+kickyrwf2_b*yp)/gam;
	      }
	    }
	    break;	 
	  }
	  
	  if (vertical) 
	    particle.yp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  else
	    particle.xp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  
	  //	placet_printf(INFO,"kick: %g %g %g\n", zp*1e6, kicky*1e6, (kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6);
	}
      }
    }
  }
  drift_step_4d_0(beam, 0.5);
}

void COLLIMATOR::adina_step_4d_0(BEAM *beam )
{

  //USE ADINA'S CODE ? (0=NO,1=YES)
  int ADINA=1;
  if (ADINA) placet_printf(INFO,"Use Chao kick\n");
  else placet_printf(INFO,"Use Giovani kick\n");
  
  //ADINA OPEN FILE FOR SAVING KICKS DATA
  FILE *KickFile;
  char KickFileName[1024];
  if (ADINA) snprintf(KickFileName,1024,"aresults.txt");
  else snprintf(KickFileName,1024,"./gresults.txt");
  placet_printf(INFO,"OPEN FILE FOR SAVING KICKS RESULTS: %s\n",KickFileName);
  KickFile=open_file(KickFileName);
  //fprintf(KickFile,"\nNEXT COLLIMATOR\n");
  if (KickFile==NULL) placet_printf(INFO,"Cannot open file \"%s\"\n",KickFileName);
  

  double s0_in=pow((2*pow(in_height,2)/Z0/sigma),1./3.);
  double s0_fin=pow((2*pow(fin_height,2)/Z0/sigma),1./3.);
  placet_printf(INFO,"\n s0_fin = %lg  s0_in = %lg M_PI= %lg Z0 = %lg \n", s0_fin,s0_in,M_PI,Z0);
  
  double Gamma=C_LIGHT*tau/s0_fin;
  double xi=s0_fin*s0_fin/fin_height/fin_height;
  placet_printf(INFO,"\n Gamma = %lg  xi = %lg \n", Gamma, xi);
  
  if(get_wake_table()==NULL){
    placet_cout << ERROR << " adina_step_4d_0 called without transverse wake fields table! " << std::endl;
    placet_cout << ERROR << " exiting: table of transverse collimator wakefields is needed " << endmsg;
    exit(1);
  }
  //  collimatortable *Ctable=new collimatortable("table_m1.txt",Gamma,xi);
  collimatortable *Ctable=new collimatortable(get_wake_table(),Gamma,xi);
  ofstream f("adinatest.txt");
  for(float x=0; Ctable->inrange(x);x+=0.1)
    //placet_cout<<DEBUG<<x<<" "<<Ctable->interpolate(x)<<endmsg;
    f <<x<<" "<<Ctable->interpolate(x)<<endl;
  f.close();
  
  //  double cap_gam=C_LIGHT*tau/s0_in;
  double lambda=1/Z0/sigma;
  double angle=atan((in_height-fin_height)/taper_length);         /* tapering angle        */
  drift_step_4d_0(beam, 0.5);
  int k=0;
  for (int ibunch=0;ibunch<beam->bunches;ibunch++) {
    const int NSL=beam->slices_per_bunch;
    double deltayarray[NSL];
    double Np[NSL]; // numero particelle per slice
    if (vertical) {
      if (beam->particle_beam) {
	for (int i=0;i<beam->slices_per_bunch; i++) {
	  double deltay=0.0;
	  double wgtsum=0.0;
	    for (int j=0;j<beam->particle_number[i]; j++) {
	      PARTICLE &particle=beam->particle[k];
	      double wgt=particle.wgt;
	      deltay += particle.y * wgt;
	      wgtsum += wgt;
	      k++;
	    }
	    if(beam->particle_number[i]>0) deltayarray[i]=deltay/wgtsum*1e-6; else deltayarray[i]=0;
	    // RJB and AT fix 29 Oct 2008
	    Np[i]=wgtsum*Nb;
	    //   fprintf(ChaoFile,"test1: %g %g %g \n", deltay, deltayarray[i], Np[i]);
	    
	}
      } else {
	for (int i=0;i<beam->slices_per_bunch; i++) {
	  double deltay=0.0;
	  double wgtsum=0.0;
	  for (int j=0;j<beam->macroparticles; j++) {
	    PARTICLE &particle=beam->particle[ibunch*beam->slices_per_bunch*beam->macroparticles+i*beam->macroparticles+j];
	    double wgt=particle.wgt;
	    deltay += particle.y * wgt;
	    wgtsum += wgt;
	  }
	  if(beam->particle_number[i]>0) deltayarray[i]=deltay/wgtsum*1e-6; else deltayarray[i]=0;
	  // RJB and AT fix 29 Oct 2008
	  Np[i]=wgtsum*Nb;
	  //   fprintf(ChaoFile,"test2: %g %g %g \n", deltay, deltayarray[i], Np[i]);
	}
      }
    } else {
      if (beam->particle_beam) {
	for (int i=0;i<beam->slices_per_bunch; i++) {
	  double deltay=0.0;
	  double wgtsum=0.0;
	  for (int j=0;j<beam->particle_number[i]; j++) {
	    PARTICLE &particle=beam->particle[k];
	    double wgt=particle.wgt;
	    deltay += particle.x * wgt;
	    wgtsum += wgt;
	    k++;
	  }
	  if(beam->particle_number[i]>0) deltayarray[i]=deltay/wgtsum*1e-6; else deltayarray[i]=0;
	  // RJB and AT fix 29 Oct 2008
	  Np[i]=wgtsum*Nb;
	  //   fprintf(ChaoFile,"test3: %g %g %g \n", deltay, deltayarray[i], Np[i]);
	}
      } else {
	for (int i=0;i<beam->slices_per_bunch; i++) {
	  double deltay=0.0;
	  double wgtsum=0.0;
	  for (int j=0;j<beam->macroparticles; j++) {
	    PARTICLE &particle=beam->particle[ibunch*beam->slices_per_bunch*beam->macroparticles+i*beam->macroparticles+j];
	    double wgt=particle.wgt;
	    deltay += particle.x * wgt;
	    wgtsum += wgt;
	  }
	  if(beam->particle_number[i]>0) deltayarray[i]=deltay/wgtsum*1e-6; else deltayarray[i]=0;
	  // RJB and AT fix 29 Oct 2008
	  Np[i]=wgtsum*Nb;
	  //   fprintf(ChaoFile,"test4: %g %g %g \n", deltay, deltayarray[i], Np[i]);
	}
      }
    }

    double kickyrwt=0.;
    double kickyrwt2=0.;
    double kickyrwf=0.;
    double kickyrwf2=0.;

    double sz0=bunch_get_length(beam, ibunch)*1e-6;
    double diffz=(beam->z_position[1+ibunch*beam->slices_per_bunch]-beam->z_position[0+ibunch*beam->slices_per_bunch])*1e-6;
    double blength=diffz*beam->slices_per_bunch;

    int regime_geometric=0;
    if (angle<0.1*fin_height*sz0/width/width) {               /* inductive regime      */
      regime_geometric=1;
      placet_printf(INFO,"\n geometric regime is inductive\n");
    } else if (angle>10.*fin_height*sz0/width/width) {        /* diffractive regime    */
      regime_geometric=2;
      placet_printf(INFO,"\n geometric regime is diffractive\n");
    } else {
      if (regime_geometric!=1)                                /* intermediate regime   */
	placet_printf(INFO,"\n geometric regime is intermediate\n");
    }


    k=0; // if there is more than 1 bunch??
    
    if (beam->particle_beam) {
       for (int i=0;i<beam->slices_per_bunch;i++) {
         for (int jj=0; jj<beam->particle_number[i];jj++) {

	  PARTICLE &particle=beam->particle[k++];
	  double yp;
	  if (vertical) {
	    yp=particle.y*1e-6; /* coordinates of the particle that feels the kick */
	  } else {
	    yp=particle.x*1e-6; /* coordinates of the particle that feels the kick */
	  }
	  double zp=-beam->z_position[ibunch*beam->slices_per_bunch+i]*1e-6;
	  double gam=particle.energy/EMASS;
	  double kicky;
	  //	  double kicky_old;

	  switch(regime_geometric) {
	  case 1:

	    kicky=RE/gam*
	      ((M_PI*width*(in_height-fin_height)*(in_height-fin_height)*
		(in_height+fin_height)/taper_length/in_height/in_height/
		fin_height/fin_height-2*(in_height-fin_height)*
		(in_height-fin_height)/in_height/fin_height/taper_length)*deltayarray[i]
	       +2*(in_height-fin_height)*(in_height-fin_height)/
	       in_height/fin_height/taper_length*yp)*Np[i]/diffz;
	    //	    kicky_old=2*RE/gam/(in_height-fin_height)*(in_height-fin_height)/
	    //	      in_height/fin_height/taper_length*(0.822*deltayarray[i]+0.411*yp)*Np[i]/diffz;
	    break;
	    
	  case 0:
	    
	    kicky=RE*2.7*sqrt(2*angle)/gam*sqrt(2*M_PI*sz0)/sqrt(fin_height*fin_height*fin_height)*(0.822*deltayarray[i]+0.411*yp)*Np[i]/diffz;
	    break;
	    
	  case 2: {
            double fih=fin_height/in_height;
            fih*=fih;
	    kicky=2*RE*sz0*SQRT_PI*(1-(fih*fih))/fin_height/fin_height/gam
	      *(0.822*deltayarray[i]+0.411*yp)*Np[i]/diffz;
	  }
	    break;
	  }

	  
	  
	  /* starts the resistive regime */
	  
	  kickyrwt=0.;
	  kickyrwf=0.;
	  
	  double E;
	  int ind_slice=int(floor((blength/2-zp)/diffz));
	  double ffh=fin_height*fin_height;
	  double coeffrwt=4*RE*sqrt(lambda/M_PI)/gam*(in_height+fin_height)*taper_length/ffh/in_height/in_height;
	  double coeffrwf=RE/gam*flat_length/(ffh*ffh)*s0_fin*sqrt(s0_fin/2/M_PI);
	  // placet_printf(INFO,"\n coefff= %lg \n", coeffrwf);
	  
	  for(int kmain=0;kmain<ind_slice;kmain++) {
	    
	    double z=blength/2-zp-(double) kmain*diffz;
	    double absz= abs(z);
	    double s=z/s0_fin;
	    double Chao=-2*coeffrwf*sqrt(1/absz);
	    
	    if (Ctable->inrange(s)){
	      E=Ctable->interpolate(s);
	      E=E*s0_fin*RE/gam*flat_length/(ffh*ffh);
	    }else{
	      
	      E=Chao;
	    }
	    
	    /* kmain=0 is the head of the bunch */
	    kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*E;
	    if(flat_length>0.)
	      kickyrwf+=Np[kmain]*(0.822*deltayarray[kmain]+0.411*yp)*E;
	  }
	  
	  if (vertical)
	    particle.yp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  else
	    particle.xp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  
	  
	  //ADINA PRINT KICKS FOR PARTICLE BEAM
	  if (KickFile!=NULL)
	    fprintf(KickFile,"%g %g %g %g %g\n",zp*1e6, kicky*1e6, (kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6, kickyrwf*1e6,  kickyrwf2*1e6);
	 }
       }
    } else {
      for (int slice=0;slice<beam->slices_per_bunch;slice++) {
	for (int jj=0;jj<beam->macroparticles;jj++) {
	  
	  
	  PARTICLE &particle=beam->particle[(ibunch*beam->slices_per_bunch+slice)*beam->macroparticles+jj];
	  double yp;
	  if (vertical) {
	    yp=particle.y*1e-6; /* coordinates of the particle that feels the kick */
	  } else {
	    yp=particle.x*1e-6; /* coordinates of the particle that feels the kick */
	  }
	  double zp=beam->z_position[ibunch*beam->slices_per_bunch + slice]*1e-6;
	  double gam=particle.energy/EMASS;
	  double kicky;
	  //	  double kicky_old;
	  
	  switch(regime_geometric) {
	  case 1:
	    
	    kicky=RE/gam*
	      ((M_PI*width*(in_height-fin_height)*(in_height-fin_height)*
		(in_height+fin_height)/taper_length/in_height/in_height/
		fin_height/fin_height-2*(in_height-fin_height)*
		(in_height-fin_height)/in_height/fin_height/taper_length)*deltayarray[slice]
	       +2*(in_height-fin_height)*(in_height-fin_height)/
	       in_height/fin_height/taper_length*yp)*Np[slice]/diffz;
	    //	    kicky_old=2*RE/gam/(in_height-fin_height)*(in_height-fin_height)/
	    //	      in_height/fin_height/taper_length*(0.822*deltayarray[slice]+0.411*yp)*Np[slice]/diffz;
	    break;

	  case 0:

	    kicky=RE*2.7*sqrt(2*angle)/gam*sqrt(2*M_PI*sz0)/sqrt(fin_height*fin_height*fin_height)*(0.822*deltayarray[slice]+0.411*yp)*Np[slice]/diffz;
	    break;
	    
	  case 2: {
            double fih=fin_height/in_height;
            fih*=fih;
	    kicky=2*RE*sz0*sqrt(4*M_PI)*(1-fih*fih)/fin_height/fin_height/gam
	      *(0.822*deltayarray[slice]+0.411*yp)*Np[slice]/diffz;
	    }
            break;
	  }

	  /*starts the resistive regime*/
	  
	  kickyrwt=0.;
	  kickyrwf=0.;
	  
	  double E;
	  int ind_slice=int(floor((blength/2-zp)/diffz));
	  double ffh=fin_height*fin_height;
	  double coeffrwt=4*RE*sqrt(lambda/M_PI)/gam*(in_height+fin_height)*taper_length/ffh/in_height/in_height;
	  double coeffrwf=RE/gam*flat_length/(ffh*ffh)*s0_fin*sqrt(s0_fin/2/M_PI);
	  // placet_printf(INFO,"\n coefff= %lg \n", coeffrwf);
	  
	  for(int kmain=0;kmain<ind_slice;kmain++) {
	    
	    double z=blength/2-zp-(double) kmain*diffz;
	    double absz= abs(z);
	    double s=z/s0_fin;
	    double Chao=-2*coeffrwf*sqrt(1/absz);
	    
	    if (Ctable->inrange(s)){
	      E=Ctable->interpolate(s);
	      E=E*s0_fin*RE/gam*flat_length/(ffh*ffh);
	    }else{
	      
	      E=Chao;
	    }
	    
	    /* kmain=0 is the head of the bunch */
	    kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*E;
	    if(flat_length>0.)
	      kickyrwf+=Np[kmain]*(0.822*deltayarray[kmain]+0.411*yp)*E;  
	  }
	  
	  if (vertical)
	    particle.yp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  else
	    particle.xp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  //ADINA PRINT KICKS FOR PARTICLE BEAM
	  if (KickFile!=NULL)
	    fprintf(KickFile,"%g %g %g %g %g \n",zp*1e6, kicky*1e6, (kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6, kickyrwf*1e6,  kickyrwf2*1e6);
	}
      }
    }
  }
  close_file(KickFile);
  delete Ctable;

  drift_step_4d_0(beam, 0.5); // track last half of collimator as drift
}

void COLLIMATOR::giovanni_step(BEAM *beam )
{
  giovanni_step_4d_0(beam);
}

int tk_Collimator(ClientData /*clientdata*/, Tcl_Interp *interp, int argc, char **argv )
{
  double bb=-1; 
  double gg=-1; 
  double ww=-1; 
  double LT=-1;
  double Lflat=-1;
  double sig_ch=-1; 
  double tau_ch=-1;
  double charge=-1;
  int horiz=0;
  int vert=1;
  int corr=1;
  int error;
  int n;

  int wake_method=0;
  char *_wake_table=NULL;
  double zmin,zmax,length=0.0;
  char *spline_name_x=NULL,*spline_name_y=NULL;
  char *element_name="";
  Tk_ArgvInfo table[]={
    {(char*)"-name",TK_ARGV_STRING,(char*)NULL,(char*)&element_name,
     (char*)"Name to be assigned to the element"},
    {(char*)"-length",TK_ARGV_FLOAT,(char*)NULL,(char*)&length,
     (char*)"Length of the element [m]"},

	// Daniel's implementation parameters

    {(char*)"-x_wakefield",TK_ARGV_STRING,(char*)NULL,(char*)&spline_name_x,
     (char*)"name of the spline that contains the transverse wakefield in x"},
    {(char*)"-y_wakefield",TK_ARGV_STRING,(char*)NULL,(char*)&spline_name_y,
     (char*)"name of the spline that contains the transverse wakefield in y"},
    {(char*)"-n",TK_ARGV_INT,(char*)NULL,(char*)&n,
     (char*)"Number of bins in z direction"},
    {(char*)"-zmin",TK_ARGV_FLOAT,(char*)NULL,(char*)&zmin,
     (char*)"Lowest position in z direction"},
    {(char*)"-zmax",TK_ARGV_FLOAT,(char*)NULL,(char*)&zmax,
     (char*)"Largest position in z direction"},

	// Giovanni's implementation parameters

    {(char*)"-in_height",TK_ARGV_FLOAT,(char*)NULL,(char*)&bb,
     (char*)"initial height of the collimator [m]"},
    {(char*)"-fin_height",TK_ARGV_FLOAT,(char*)NULL,(char*)&gg,
     (char*)"final height of the collimator [m]"},
    {(char*)"-width",TK_ARGV_FLOAT,(char*)NULL,(char*)&ww,
     (char*)"Width of the collimator [m]"},
    {(char*)"-taper_length",TK_ARGV_FLOAT,(char*)NULL,(char*)&LT,
     (char*)"Length of the taper [m]"},
    {(char*)"-flat_length",TK_ARGV_FLOAT,(char*)NULL,(char*)&Lflat,
     (char*)"Length of the flat part of the collimator [m]"},
    {(char*)"-sigma",TK_ARGV_FLOAT,(char*)NULL,(char*)&sig_ch,
     (char*)"Electrical conductivity of the collimator material [(Ohm m)^-1] (i.e. graphite=6.e4)"},
    {(char*)"-tau",TK_ARGV_FLOAT,(char*)NULL,(char*)&tau_ch,
     (char*)"Relaxation time of the collimator material [s]"},
    {(char*)"-charge",TK_ARGV_FLOAT,(char*)NULL,(char*)&charge,
     (char*)"Bunch charge [e]"},    
    {(char*)"-horizontal", TK_ARGV_CONSTANT, (char *) 1, (char *) &horiz, 
     (char*)"Horizontal collimator"},
   {(char*)"-vertical", TK_ARGV_CONSTANT, (char *) 1, (char *) &vert, 
     (char*)"Vertical collimator (default)"},
    {(char*)"-correct_offset",TK_ARGV_INT,(char*)NULL,(char*)&corr,
     (char*)"If not zero take into account the offset of the collimator"},

	// Adina's implementation parameters

    {(char*)"-wake_method",TK_ARGV_INT,(char*)NULL,(char*)&wake_method,
     (char*)"If not zero calculates the resistive transversal wakefields kicks using RJB & AT method"},
    {(char*)"-wake_table",TK_ARGV_STRING,(char*)NULL,(char*)&_wake_table,
     (char*)"If wake_method not zero file name which contains the resistive transversal wakefields"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK) {
    return error;
  }
  
  if (argc!=1) {
    Tcl_SetResult(interp,"Too many arguments to <Collimator>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  
  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;
  
  COLLIMATOR *e;
  if(horiz) vert=0;
  if (bb!=-1 || gg!=-1 || ww!=-1 || LT!=-1 || Lflat!=-1 || sig_ch!=-1  || tau_ch!=-1 || charge!=-1) { // comparing floats is unsafe! JS
    e = new COLLIMATOR(bb, gg, ww, LT, Lflat, sig_ch, tau_ch, charge, corr, vert);
    e->set_wake_meth(wake_method);
    if(wake_method) {
        // if _wake_table is not given, take default table.
        if(!_wake_table) {
            std::string wake_table = get_placet_sharepath("table_m1.txt");
            e->set_wake_table(wake_table.c_str());
        }
        else e->set_wake_table(_wake_table);
    }
  } else {
    SPLINE* spx = get_spline(spline_name_x);
    SPLINE* spy = get_spline(spline_name_y);
    e=new COLLIMATOR(spx,spy);
    e->set_bin(zmin,zmax,n);
    e->set_correct_offset(corr);
  }

  e->set_name(element_name);
  e->set_length(length);
  
  inter_data.girder->add_element(e);  
  
  return TCL_OK;
}

int tk_CollimatorNumberList(ClientData /*clientdata*/, Tcl_Interp *interp, int argc, char **argv )
{
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK) {
    return error;
  }
  
  ELEMENT **element=inter_data.beamline->element;
  for (int i=0;i<inter_data.beamline->n_elements;i++){
    if (typeid(*element[i])==typeid(COLLIMATOR)) {
      char buffer[100];
      snprintf(buffer,100,"%d",i);
      Tcl_AppendElement(interp,buffer);
    }
  }
  return TCL_OK;
}
