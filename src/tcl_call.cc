#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "tcl_call.h"
#include "placet_tk.h"

extern Tcl_Interp* beamline_survey_hook_interp;
extern INTER_DATA_STRUCT inter_data;

TCLCALL::TCLCALL(const char *script_name) : correct_offset(0)
{
  if (script_name)  script=script_name;
}

void TCLCALL::step_4d_0(BEAM *beam)
{
  // The first command may not be necessary (needs to be checked)
  inter_data.bunch=beam;
  if (Tcl_Eval(beamline_survey_hook_interp,script.c_str())==TCL_ERROR) {
    placet_printf(ERROR,"Error in TclCall subroutine %s: %s\n",script.c_str(),Tcl_GetStringResult(beamline_survey_hook_interp));
    fflush(stderr);
    exit(1);
  }
}
