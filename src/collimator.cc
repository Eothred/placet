#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"
#include "spline.h"

/* Function prototypes */

#include "collimator.h"

extern Tcl_Interp* beamline_survey_hook_interp;
extern INTER_DATA_STRUCT inter_data;

COLLIMATOR::COLLIMATOR(SPLINE *spl) : giovanni(false)
{
  set_spline(spl);
  correct_offset=0;
  wtx=NULL;
  wty=NULL;
  rhox=NULL;
  rhoy=NULL;
}

COLLIMATOR::COLLIMATOR(SPLINE *splx,SPLINE *sply) : giovanni(false)
{
  set_spline_x(splx);
  set_spline_y(sply);
  correct_offset=0;
  wtx=NULL;
  wty=NULL;
}

void
beam_bin(BEAM *beam,double hx[],double hy[])
{
  int i,j,k=0;

  for (i=0;i<beam->slices_per_bunch;++i) {
    hx[i]=0.0;
    hy[i]=0.0;
    for (j=0;j<beam->particle_number[i];++j){
      hx[i]+=beam->particle[k].wgt*beam->particle[k].x;
      hy[i]+=beam->particle[k].wgt*beam->particle[k].y;
      k++;
    }
    fflush(stdout);
  }
}

void
fold_kick(double rx[],double ry[],double x[],double y[],int n)
{
  int i,j;
  for (i=n-1;i>=0;--i){
    x[i]*=rx[0];
    y[i]*=ry[0];
    for (j=0;j<i;++j) {
      x[i]+=x[j]*rx[i-j];
      y[i]+=y[j]*ry[i-j];
    }
    x[i]*=ECHARGE; /* *1e9*1e-9 */
  }
}

void
beam_kick(BEAM *beam,double hx[],double hy[])
{
  int i,j,k=0;
  for (i=0;i<beam->slices_per_bunch;++i){
    for (j=0;j<beam->particle_number[i];++j){
      beam->particle[k].xp+=hx[i]/beam->particle[k].energy;
      beam->particle[k].yp+=hy[i]/beam->particle[k].energy;
      k++;
    }
  }
}

void
COLLIMATOR::daniel_step_4d_0(BEAM *beam)
{
  // The first command may not be necessary (needs to be checked)

  inter_data.bunch=beam;
  /*
    Actual collimator kick
   */

  rhox=(double*)alloca(sizeof(double)*beam->slices_per_bunch); // allocate memory on the stack, freed automagically
  rhoy=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  wtx=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  wty=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  spx->get_table(0.0,
		 beam->z_position[1]-beam->z_position[0],wtx,
		 beam->slices_per_bunch);
  spy->get_table(0.0,
		 beam->z_position[1]-beam->z_position[0],wty,
		 beam->slices_per_bunch);
  beam_bin(beam,rhox,rhoy);
  fold_kick(wtx,wty,rhox,rhoy,beam->slices_per_bunch);
  beam_kick(beam,rhox,rhoy);
}

void
COLLIMATOR::daniel_step(BEAM *beam)
{
  daniel_step_4d_0(beam);
}

void
COLLIMATOR::set_spline(SPLINE *spl)
{
  spx=spl; spy=spl;
}

void
COLLIMATOR::set_spline_x(SPLINE *spl)
{
  spx=spl;
}

void
COLLIMATOR::set_spline_y(SPLINE *spl)
{
  spy=spl;
}
