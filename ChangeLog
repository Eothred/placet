14-10-2013 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* remove n_phases and phase from BEAMLINE and acc_field from BEAM
	* move commands MainPhases, SetBeamEnergy and SetCentreGradient to obsolete
02-09-2013 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* introduce placet_file.h for safe file manipulation
02-09-2013 Andrea Latina <andrea.latina@cern.ch>
	* tag 0.99.05 created
02-07-2013 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* no special treatment for last girder for GroundMotion (GM)
	* reverse behaviour of cav_stab in GM to match description
	* placet_cout/placet_printf: print WARNINGS and ERRORS to std::cerr
16-06-2013 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* merge branch placet-beamline-class
	- put beamline methods inside class
	- change reference test files, due to slight change in calculation
	method of beamline length (no recalculation)
03-06-2013 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* fix Random8 bugs (101644, 101658)
23-05-2013 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* merge branch placet-shortrangewake (r2422)
	- add short range wake to ELEMENT using class SHORT_RANGE
03-04-2013 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	- CavityBPM:
	* add SPLINE option (r2302)
	* add short bunch approximation method (r2303)
	* add possibility to differentiate between x and y (r2307)
18-03-2013 (r2276)
	- Francis Cullinan <Francis.Cullinan.2010@live.rhul.ac.uk>
	- Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* add CavityBPM class that include wakefields
XX-XX-2013 (rXXXX):
	* thick tracking in the sbend has replaced thin-tracking (4d/6d w/
	w/o sr)
27-02-2013 (r2243):
	* tag 0.99.01 released:
	- buffering in the communications between PLACET and its
	sub-processes has been introduced 
	- fixed the problem that dramatically slowed down placet_set_beam()
27-02-2013 (r2236):
	* tag 0.99.00 released
25-09-2012 (r1921) Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* add Clang compiler support
02-09-2012 (r1899) Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* switch off preprocessor flag EARTH_FIELD
29-08-2012 (r1897) Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* update physical constants to NIST data
	* remove duplication and more consistent use of constants
	* cleanup of unused functions (moved to obsolete)
	* fix duplication of powell.c and minimise.cc by making a class object POWELL
24-08-2012 (r1893) Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* split r_matrix.h (R_MATRIX) from rmatrix.h (RMATRIX element)
	* small fixes
24-08-2012 (r1892) Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
        * particle.id: double -> unsigned int
	* small improvements
16-08-2012 (r1872) Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
        * several improvements and fixes
	* add -error option
13-08-2012 (r1869) Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* add error messages for invalid command options
	* add beamline_element_in_range function
10-08-2012 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* introduce sincosh function for fast sinh/cosh calculation
08-08-2012 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* sbend.cc: small bug fix
07-08-2012 Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
	* add ELEMENT::drift_sigma_matrix to speed up sigma calculation
	* small bugfix in BPMSetToOffset
18-04-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* updates to ground motion commands
16-04-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* add command GroundMotionATL (similar to survey ATL, but possible
	to give options)
21-03-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* tag PLACET-00-95-11
15-03-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* fix Coverity bugs DIVIDE_BY_ZERO
14-03-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* replace unsafe sprintf by snprintf
	* replace direct writing to interp->result by Tcl_SetResult
12-03-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* option_set.cc: small improvement
	* move some commented out code to obsolete
24-02-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* add class PLACET_COUT
	* change all std::cout to placet_cout
	* add ALWAYS to enum VERBOSITY
	* add error and warning count and goodbye message
16-02-2012 Yngve Inntjore Levinsen <Yngve.Inntjore.Levinsen@cern.ch>
	* verbosity implemented
	   placet_printf(),placet_fprintf() and placet_fputs() are available.
	   they take same arguments as printf,fprintf and fputs, but with 
	   the added VERBOSITY, see placet_print.h
	   They can be switched on using the
	   input arguments: -s -v -vv -dbg
	   alternatively using the Verbosity command
01-02-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* moved some non-used methods to obsolete
23-01-2012 Yngve Inntjore Levinsen <Yngve.Inntjore.Levinsen@cern.ch>
	* step_6d_0 in crab.cc added&tested
21-01-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* small cleaning
18-01-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* add BEAM::get_slice_number_generic for HTGEN usage
13-01-2012 Andrea Latina <andrea.latina@cern.ch>
	* Introduced new element: RfMultipole
13-01-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* cleanup of includes and global variables
13-01-2012 Helmut Burkhardt <Helmut.Burkhardt@cern.ch>
	* background.cc: cleanup for htgen, remove tk_TrackTestbeam, beamline_track_testbeam
12-01-2012 Andrea Latina <andrea.latina@cern.ch>
	* Build system improved: new makefiles
12-01-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* small improvements
11-01-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* fixed Coverity bugs 24097,104,107,30258,39972,3
11-01-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* beam.h: particle_beam from int to bool
	* include HTGEN preprocessor directives more consistently
06-01-2012 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* slices_to_particles.cc: add check on particle beam
12-12-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* move unused files to obsolete, part4
	* update includes, Makefile.in and Make_htgen.in
12-12-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* move unused files to obsolete, part3
09-12-2011 Yngve Inntjore Levinsen <Yngve.Inntjore.Levinsen@cern.ch>
	* configure.ac: added -ldl to default link flags
	* cmake: placet-octave should now be included
08-12-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* move unused files to obsolete, part2
07-12-2011 Yngve Inntjore Levinsen <Yngve.Inntjore.Levinsen@cern.ch>
	* added cmake files mainly for ctest/cdash purposes
	* minor fix for octave includes, affects octave_embed and
	  configure.ac/Makefile.in
07-12-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* move unused files to obsolete
25-11-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* ground.h/cc: improve memory usage by using map
06-11-2011 Andrea Latina <andrea.latina@cern.ch>
	* added fringe fields simulation in Sbends inspired by MAD-X
	implementation (new attributes -hgap, -fint, -fintx )
	* modified madx2placet.pl and madx2placet_bds.pl to take into
	account the MADX attributes : HGAP, FINT, FINTX
03-11-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* introduce get_beam and get_beamline functions
16-09-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* cleanup headers: remove declared but not implemented functions
	* moved some non-used methods to obsolete
15-09-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* fixed Coverity bugs 24249,334
14-09-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/octave_placet.cc: fix array overflow
	* fixed Coverity bugs 24286,89,97,325,26,28
13-09-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/placeti5.cc: fix bug in test_ballistic_2
	* fixed Coverity bugs 24077,24330-33,35-36,30259-62
31-08-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/position.cc: add option to not filter ground motion for cavities in the beamline (on by default)
29-08-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* fixed Coverity bugs (24)042,044-047,072,082-083,091-094,098-103,105-106,108-132,225
	* moved some non-used methods to obsolete
26-08-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* fixed Coverity bugs (24)117,283,287-88,291,292,296,313-320,322-324,344-45
	* moved some non-used methods to obsolete
25-08-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* fixed exotic integer overflow of random number generators that caused memory corruption
05-08-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* update tk_SlicesToParticles and S2P class
	* tag PLACET-00-95-10
15-06-2011 Andrea Latina <andrea.latiba@cern.ch>
	* Multipoles accept strength as a complex number
	* Multipoles apply multiple kicks when new option is used
	"-strength_list { 1 2 3 4 (5,6) }"
19-04-2011 Barbara Dalena <Barbara.Dalena@cern.ch>
	* EnergySpreadPlot file now contains 5 columns:
	  index of elem., s position along beamline [m], energy [GeV] , relative energy, RMS energy spread
	* fixes in the TwissPlot and TwissPlotStep for particle beam
12-04-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/position.cc add code for preisolator transfer functions in x
06-04-2011 Andrea Latina <andrea.latina@cern.ch>
	* src/octave_placet.h|cc, src/octave.cc: added new command placet_get_transfer_matrix_fit()
01-04-2011 Andrea Latina <andrea.latina@cern.ch>
	* src/octave_placet.h|cc, src/octave.cc: added new command placet_get_reponse_matrix_fit()
15-03-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/octave.cc: add error message
10-03-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/position.cc: add option to GroundMotion to move absolute motion
03-03-2011 Barbara Dalena <Barbara.Dalena@cern.ch>
	* added rotate_x and rotate_y to the BeamDump command
	* added -start -end to the TwissPlotStep and TwissPlot command in order to compute the
	  twiss functions for a slice different from the central one. The twiss.dat output contains
	  20 columns in this case. 
15-02-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* girder.cc: relax tolerances on moving girder with zero length and remove exit statement
08-02-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* ground/position: bugfixes and move rel. to IP motion
	again (description put from GROUND class to tk_GroundMotion)
07-02-2011 Barbara Dalena <Barbara.Dalena@cern.ch>
	* configure.AFS fixed for SLC5 release
06-02-2011 Andrea Latina <Andrea.Latina@cern.ch>
	* MANY FILES: Thin-Lens Tracking reviewed (for single-particle beams), with SR
	* MANY FILES: Dipoles implements the same change, thus they emitt SR
30-01-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* ground/position: include more realistic implementation of preisolator
27-01-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* beamline.cc: fix girder_number()
25-01-2011 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* octave_dummy.cc: print line and exit when called from placet-development
01-12-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* placet_tk.cc: add savannah line to intro print screen
24-11-2010 Jurgen Pfingster
	* witness.c: add energy spread and z to BeamMeasure
09-11-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* ground/position: improve interface of ground.h and AddGMFilter
16-10-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* ground: fix bug in filter, change to radial frequencies
15-10-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* ground: add method get_gs0, fix positions for s_abs==0
	* position.cc: GroundMotion, fix output file and comments
28-09-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* cleanup and comments
24-09-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* lattice.cc: add command AddDipolesToBeamline
21-09-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* position.cc: add possibility to add different filters for x and y direction
	* spline.cc: check on taking log of negative values
	* ground.cc: change from 10000 to arbitrary number of lines in filter file
17-09-2010 Barbara Dalena <Barbara.Dalena@cern.ch>
	* quad roll correctly saved and read in Save/ReadAllPosition command
	* bug in slices_to_particles fixed
14-09-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* memory reduction:
	- revised option_info class
	- replace BPM in CAVITY by simple struct
14-09-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* tag PLACET-00-95-07
13-09-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* add new command AddGMFilter and restructuring of GROUND class (in progress)
10-09-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/bpm+graph: move some external functions from bpm to graph
01-09-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* cleanup and typos
30-08-2010 Guido Sterbini <guido.sterbini@cern.ch>
	* SBEND: step_in and step_out, modified for sliced beam to take into account the edge effect.
	* CAVITIY: get_transfer_matrix_6d, modified to take into account the focusing due to RF acceleration.
26-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* improve interface of ground motion by splitting MoveGirderPoints into new commands GroundMotionInit and GroundMotion
	* move old ground files to src/obsolete
25-08-2010 Andrea Latina <alatina@fnal.gov>
	* Fixed small bugs comparing char *
11-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* small improvements
10-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* unreachable code (checked with Wunreachable-code): comment out or move to src/obsolete
09-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* move obsolete standalone tracking and element creation methods to src/obsolete
09-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/lattice.cc: correct rf_data initialization, comment out unreachable code
06-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/dipole_kick.cc: add cerr to diagnose segfault
05-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* GIRDER class: change distance_to_next_girder() to distance_to_prev_girder()
04-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* introduce GIRDER class
04-08-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* tag PLACET-00-95-06
28-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/beam_many.cc: fix uninitialized variable and small bug
22-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/placet_tk.cc: fix bug RandomReset -seed
21-07-2010 Andrea Latina <alatina@fnal.gov>
	* improvements in SetReferenceEnergy
21-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/placeti3: remove unused superfluous function
21-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/ground.h/cc: fix compiler warning
15-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* add BeamlineDelete command, which deletes a beamline and its girders and elements in order to free memory
14-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/ground.h/cc: fix destructor, add comments, prepare filter functionality
	* src/position.cc: add option to MoveGirderPoints to restart ground motion (with different seed)
	* src/track.cc: fix memory leak
13-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/element.cc: reduce memory reservation for attributes
06-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/position.cc: WriteGirderLength writes out more significant digits now
	* src/ground.cc: make GROUND_DATA 1-dimensional, add destructor
05-07-2010 Andrea Latina <alatina@fnal.gov>
	* src/quadrupole.cc src/sbend.cc: Quadrupoles and Sbends did not take into account the tilt angle in get_transfer_matrix(). Now they do.
05-07-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/beamline: add ground motion object to beamline
	* src/position.cc: improvements to MoveGirderPoints
	* src/ground.cc src/ground_new2.C: bugfix
28-06-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/peder.cc: add option for BpmSetResolution to set individual bpms
25-06-2010 Barbara Dalena <Barbara.Dalena@cern.ch>
	* object select fixed (to use it need just to include the select.h) 
	* Tcl command Random8 added to extract different random number to generate
          particle distributions, option seed forseen but not fully implemented  
07-06-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* clear variables that are set, but never used
31-05-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* fix use of uninitialized variables and several small compiler warnings
17-05-2010 Erik Adli
	* src/power.cc: Added damping (ohmic losses) to the power and field quick-calc routines. (Default value is a Q of 1e20, thus no damping)
07-05-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/beamline.cc: beamline_set(): set beamline.length
29-04-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/lattice.cc: bugfix in add_dipole_grid()
21-04-2010 Erik Adli
	* src/quadrupole.cc: bugfix
	* src/rfkick.cc: memb. init
19-04-2010 Barbara Dalena <Barbara.Dalena@cern.ch>
	* small bug fix in position.cc of my update of friday 17/04/2010
25-03-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/quadrupole.cc: fix small bug in step_6d_tl_0

25-03-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/beam.h: fix get_index method
	* src/placet_tk: 
	- put private tk_ methods in anonymous namespace
	- speed up tk_Beam method
	* src/placeti3: improve bunch_set_slice methods
	* src/placeti5: move divide_linac2 from placet_tk

22-03-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* remove redundant declarations in header files
	* src/placeti3.cc: some code duplication cleanup

15-03-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/placet_tk.cc: add options to SetDipoleStrength
	
19-02-2010 Barbara Dalena <Barbara.Dalena@cern.ch>
	* added multipolar error to the quadrupole	
	* tag PLACET-00-95-05

17-02-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/*.h: add include guards

15-02-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/lattice.cc: improve AddDipoleGridToBeamline and AddBPMsToBeamline
	
12-02-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/errf.h/cc: put erfc in namespace, fix for gcc45
	
04-02-2010 Barbara Dalena <Barbara.Dalena@cern.ch>
    	* developing version of new ground motion implementation
       	  added but still to check and improve
	* little fix in step.cc
	* option -bunches added to BeamSaveAll in order to store 
	  the different bunches of a train in different files 

03-02-2010 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* cleanup some dependencies

17-12-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* configure: remove compiler option -Wno-long-double for apple (obsolete)
	
25-11-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/lattice.h/cc: add two commands: AddDipoleGridToBeamline and AddBPMsToBeamline 

21-11-2009 Juergen Pfingster
	* src/phase_advance.cc: added position in outputfile and now considering also the weighting of the slices (small bug before) 

08-11-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/matrix.h: fix bug
	* tag PLACET-00-95-04

29-10-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* add compiler warning flags

22-10-2009 Barbara Dalena
	* tag PLACET-00-95-03

22-09-2009 Andrea Latina <alatina@fnal.gov>
	* Added two placet-octave functions: 
	- [mux, muy] = placet_get_phase_advance(T, alfax, betax, alfay, betay);
	- [mux, muy] = placet_get_phase_advance_fodo(T);


17-09-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* tag PLACET-00-95-02

15-09-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* fix compiler warnings, including some bugfixes, all of -Wall -Wextra -Wundef -Wno-parentheses -fno-builtin-sprintf -Wno-write-strings and some of -Wshadow -Wunused-macros
	- -Wall -Wextra -Wundef: enables most warnings
	- -Wno-parentheses: allow for assignments within if statements (without having to add brackets)
	- -fno-builtin-sprintf: allow sprintf statements with '\0' (occurs in witness.c)
	- -Wno-write-strings: allow casting of strings to non-const char pointers (tcl does this all the time)
	- -Wshadow: warn for shadowed variables
	- -Wunused-macros: warn for unused macros (all fixed except for #define PLACET)
	* replace some c-headers by their c++ counterparts
	* move some non-used buggy methods to src/obsolete
	* remove: src/add_offset src/quadrupole2.c src/QUESTIONS src/survey_tk.C src/s.cc src/set_offset src/short.c src/test.m
	
20-08-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* src/giovanni.cc: fix call to gsl_integration_qag
	* src/option_set.h: remove using namespace std from global header, fix resulting files
	* src/option_set.cc: remove assert

19-08-2009 Barbara Dalena
	* add Adina collimator wake fields.
	* remove non-linearities from giovanni.cc
	
11-08-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* tag PLACET-00-95-01

06-08-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* change parsing of args method for elements, so that non-existing arguments will print a warning message
	* fix many compile warnings (-Wall -Wshadow), including some small bugfixes
	* add: sincos.h (for fast sincos routine) and fpesvc.h (for enabling floating point exceptions)
	* remove: cmd.c octaveold.c cavityold.c cavold.c set_offset

24-07-2009 Barbara Dalena & Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* tag PLACET-00-95-00

11-06-2009 Jochem Snuverink <Jochem.Snuverink@cern.ch>
	* examples/htgen/htgen.dummy.tcl: fix number of elements in beamline
