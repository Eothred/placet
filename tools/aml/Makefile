# makefile to build mad2placet and placet2mad programs.
#
# author: Yves Renier
# release history:
# 2007/08/31: Initial version
#
#dependencies:
# XERCESCROOT env variable set to location of xerces library.
# AMLROOT env variable set to location of AML library

AMLROOT=/home/clic/yves_renier/accelerator-ml/uap/trunk

FULL_LINK    = -L$(AMLROOT)/lib -Llib -luap -lantlr -L$(XERCESCROOT)/lib  -lxerces-c  
PLATFORM_TAG := $(shell uname -s)
ifneq "$(PLATFORM_TAG)" "Darwin"
  FULL_LINK    += -Wl,-rpath $(XERCESCROOT)/lib -Wl,-rpath $(AMLROOT)/lib
endif

INC  = -I ANTLR270  -I. 
INC += -I $(XERCESCROOT)/include
INC += -I $(AMLROOT)

CPPFLAGS  += -O3 -DLINUX -D_REENTRANT -DNO_STATIC_CONSTS -MMD
FFLAGS     = -O3 -Imodules
FLINKFLAGS = -L/usr/lib/gcc-lib/i386-redhat-linux/3.2.3/ -lstdc++

COMPILE    = g++
FCOMPILE   = g95



#---------------------------

#MAD2PLACET convert example
MAD2PLACET_SRC += mad2placet.cpp 
MAD2PLACET_OBJ := $(MAD2PLACET_SRC:.cpp=.o)
MAD2PLACET_EXE  = mad2placet

#PLACETDRIVER convert example
PLACETDRIVER_SRC += placet_driver.cpp 
PLACETDRIVER_OBJ := $(MAD2PLACET_SRC:.cpp=.o)
PLACETDRIVER_EXE  = placet_driver

PLACETCOMMAND_EXE = importlatticeaml.so
PLACETCOMMAND_SRC = importlatticeaml_tk.cc
PLACETCOMMAND_OBJ = importlatticemal_tk.o

#-------------------------------------------------

# UAP support library 
UAP_CPP = $(wildcard Bmad/*.cpp AML/*.cpp UAP/*.cpp Fortran/*.cpp)
UAP_HPP = $(UAP_CPP:.cpp=.hpp)
UAP_F90 = $(wildcard Fortran/*.f90)
UAP_OBJ = $(UAP_CPP:.cpp=.o) $(UAP_F90:.f90=.o) 
UAP_MOD = $(UAP_F90:.f90=.mod)

#antlr library.
ANTLR_CPP = $(wildcard ANTLR270/*.cpp)
ANTLR_HPP = $(wildcard ANTLR270/antlr/*.hpp)
ANTLR_OBJ = $(ANTLR_CPP:.cpp=.o)

#
# we want to build static libraries
# and statically linked executables. 
#

# build targets and rules

all:       setup libs $(MAD2PLACET_EXE) $(PLACETDRIVER_EXE) $(PLACETCOMMAND_EXE)

importlatticeaml.so: importlatticeaml_tk.o
	g++ -shared importlatticeaml_tk.o $(FULL_LINK) -o importlatticeaml.so

mad2placet: setup libs $(MAD2PLACET_EXE)

placetdriver: setup libs $(PLACETDRIVER_EXE)

libs:      setup lib/libantlr.a lib/libuap.a

setup:
	@mkdir -p bin lib modules

.SUFFIXES:
.SUFFIXES: .cpp .f90
.PHONY:	clean setup

%.o : %.cc
	@echo "Compiling" $<
	@$(COMPILE) $(CPPFLAGS) $(INC) -c $< -o $@

%.o : %.cpp
	@echo "Compiling" $<
	@$(COMPILE) $(CPPFLAGS) $(INC) -c $< -o $@

%.o : %.f90
	@echo "Compiling" $<
	@$(FCOMPILE) $(FFLAGS) -c $< -o $@
	-@mv *.mod modules

$(MAD2PLACET_EXE) : $(MAD2PLACET_OBJ)  lib/libuap.a
	 @echo "Linking mad2placet example"
	 @$(COMPILE) -o $@ $(MAD2PLACET_OBJ)  $(FULL_LINK)

$(PLACETDRIVER_EXE) : $(PLACETDRIVER_OBJ)  lib/libuap.a
	 @echo "Linking placet_driver example"
	 @$(COMPILE) -o $@ $(PLACETDRIVER_OBJ)  $(FULL_LINK)
	 
clean:
	@rm -f  */*.o
	@rm -f  */*.d

lib/libantlr.a: $(ANTLR_OBJ)
	@echo "Building ANTLR 270 library"
	@ar cr $@ $(ANTLR_OBJ)

lib/libuap.a: $(UAP_OBJ) lib/libantlr.a
	@echo "Building UAP library"
	@ar cr $@ $(UAP_OBJ)

include DEPENDENCIES
