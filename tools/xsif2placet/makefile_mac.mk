CC		= gcc
CXX		= g++

BINDIR  	= ../bin
LIBDIR  	= ../lib

TCL_STUB	= tclstub8.4
TCL_VERSION	= tcl8.4
TK_VERSION	= tk8.4

TCL_LDFLAGS	= -l$(TCL_VERSION) 
TK_LDFLAGS	= -l$(TK_VERSION)

GSL_CFLAGS	= `gsl-config --cflags`
GSL_LDFLAGS	= `gsl-config --libs`

CFLAGS  	= -fast -mcpu=7450 \
		  -I../include\
		  -pipe -Wno-long-long\
		  -ffast-math -DUSE_NON_CONST \
		  -finline -finline-limit=4000 $(GSL_CFLAGS)# -fomit-frame-pointer 

LDFLAGS 	= -L/usr/X11R6/lib -lX11 $(GSL_LDFLAGS) $(TCL_LDFLAGS) $(TK_LDFLAGS) -ldl
LDFLAGS_SO	= 
