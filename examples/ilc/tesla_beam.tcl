set ch {1.0}

#
# transverse wakefield
# s is given in micro metres
# return value is in V/pCm^2
#

proc w_transv {s} {
    return [expr 1.0*(1290.0*sqrt($s*1e-6)-2600.0*$s*1e-6)]
}

#
# longitudinal wakefield
# s is given in micro metres
# return value is in V/pCm
#

proc w_long {s} {
    return [expr 1.0*38.1*(1.165*exp(-sqrt($s/3.65e3))-0.165)]
}

if {![info exists nominal]} {
    set nominal 1
}
if {$nominal} {
    set f [open $scripts/sr_wake_table_new.tesla r]
} {
    set f [open $scripts/sr_wake_table_ll.tesla r]
}

for {set i 0} {$i<6} {incr i} {
    gets $f line
}
set ll ""
set lt ""
while {![eof $f]} {
    lappend ll "[expr -1e6*[lindex $line 0]] [expr 1e-12*[lindex $line 1]]"
    lappend lt "[expr -1e6*[lindex $line 0]] [expr -1e-12*[lindex $line 2]]"
    gets $f line
}

SplineCreate s_long $ll
SplineCreate s_transv $lt

proc w_long {s} {
    s_long $s
}

proc w_transv {s} {
    s_transv $s
}

source $scripts/wake_calc.tcl

#
# Define the beams
#

source $scripts/make_beam.tcl
