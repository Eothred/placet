set e0 14.990402211586
set e_initial $e0

set match(emitt_x) 100.0
set match(emitt_y) 0.4
set match(phase) 0.0
set charge 2e10
set match(charge) $charge

set match(beta_x) 1.009429903009E2
set match(beta_y) 51.470740190609
set match(alpha_x) -0.31372129251
set match(alpha_y) 0.64506517098
set match(sigma_z) 300
set match(e_spread) 1.07
