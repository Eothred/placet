set script_dir .

##
# If you want to run this script stand-alone you
# need to manually set these variables:
#
# set name_synrad particle.dist.only_synrad.out
# set name_ideal particle.dist.no_solenoid.out
# set name_final particle.dist.solenoid_and_synrad.out
# set n 2000
# set n_slice 40
# set n_total [expr $n_slice*$n] 
##
array set match {
    alpha_x 0.59971622
    alpha_y -1.93937335
    beta_x  18.382571
    beta_y  64.450775
}

#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 4.0e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0
#set match(e_spread) 0.0

################################################################
# GUINEA-PIG  
################################################################

array set gp_param "
    energy 1500.0
    particles [expr $match(charge)*1e-10]
    sigmaz $match(sigma_z)
    cut_x 200.0
    cut_y 25.0
    n_x 64
    n_y 256
    do_coherent 1
    n_t 1
    charge_sign -1.0"



source $script_dir/clic_guinea.tcl
proc run_guinea {off angle} {
    global gp_param
    set res [exec grid]
    set yoff [expr -0.5*([lindex $res 2]+[lindex $res 3])]
    set xoff [expr -0.5*([lindex $res 0]+[lindex $res 1])] 
    set tx $gp_param(cut_x)
    set ty $gp_param(cut_y)
    if {[lindex $res 1]-[lindex $res 0]>2.0*$tx} {
        set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])]
       }
    if {[lindex $res 3]-[lindex $res 2]>2.0*$ty} {
        set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])]
       }
    puts "yoff $yoff"
    puts "xoff $xoff"
    write_guinea_correct $xoff $yoff
    exec guinea++ default default result.out
    set gp_param(cut_x) $tx
    set gp_param(cut_y) $ty
    return [get_results result.out]
}
################################################################


if { [file exists "electron.ini"] } {exec rm electron.ini positron.ini}
exec ln -s $name_ideal electron.ini
exec ln -s $name_ideal positron.ini
set res [run_guinea 0.0 0.0]
puts "With ideal: $res"

exec rm electron.ini positron.ini
exec ln -s $name_synrad electron.ini
exec ln -s $name_synrad positron.ini
set res [run_guinea 0.0 0.0]
puts "With synrad: $res"

exec rm electron.ini positron.ini
exec ln -s $name_final electron.ini
exec ln -s $name_final positron.ini
set res [run_guinea 0.0 0.0]
puts "With solenoid&synrad: $res"
