set terminal pdfcairo enhanced size 12cm,9cm
set output "synrad_sims_singtrk.pdf"
mapdist=-16.2117

set xlabel "Dist. from IP [m]"

set ylabel "x [{/Symbol m}m]"
plot 'singtrk.1.dat' u 4:2 w l t 'first', 'singtrk.2.dat' u 4:($2) w l t 'back', 'singtrk.3.dat' u 4:2 w l t 'w/sol\&synrad'
set ylabel "y [{/Symbol m}m]"
plot 'singtrk.1.dat' u 4:3 w l t 'first', 'singtrk.2.dat' u 4:($3) w l t 'back', 'singtrk.3.dat' u 4:3 w l t 'w/sol\&synrad'

set ylabel "B_x"
plot 'singtrk.1.dat' u 4:7 w l t 'first', 'singtrk.2.dat' u 4:($7) w l t 'back', 'singtrk.3.dat' u 4:7 w l t 'w/sol\&synrad'
set ylabel "B_y"
plot 'singtrk.1.dat' u 4:8 w l t 'first', 'singtrk.2.dat' u 4:($8) w l t 'back', 'singtrk.3.dat' u 4:8 w l t 'w/sol\&synrad'
set ylabel "B_z"
plot 'singtrk.1.dat' u 4:9 w l t 'first', 'singtrk.2.dat' u 4:($9) w l t 'back', 'singtrk.3.dat' u 4:9 w l t 'w/sol\&synrad'

#set title 'ildantinoobuck.txt'
#set ytics nomirror
#set y2tics
#set xlabel 'Dist from IP [cm]'
#set ylabel 'B_z'
#set y2label 'B_r'
#plot 'ildantinobuck.txt' u 2:3 w l axes x1y2 t 'B_r', '' u 2:4 w l ls 3 t 'B_z'

#set title 'ildantinoobuck.txt.inverted'
#set ytics nomirror
#set y2tics
#set xlabel 'Dist from IP [cm]'
#set ylabel 'B_z'
#set y2label 'B_r'
#plot 'ildantinobuck.txt.inverted' u 2:3 w l axes x1y2 t 'B_r', '' u 2:4 w l ls 3 t 'B_z'
