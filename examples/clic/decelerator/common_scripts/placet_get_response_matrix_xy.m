
function [R,b0]=placet_get_response_matrix_xy(beamline, beam, B, C, survey, vertical)
    
    Res=placet_element_get_attribute(beamline, B, "resolution");
    placet_element_set_attribute(beamline, B, "resolution", 0.0);
    placet_test_no_correction(beamline, beam, survey);
    b0=placet_get_bpm_readings(beamline, B);
    if( vertical )
      b0=b0(:,2);
    else
      b0=b0(:,1);
    end
    R=zeros(size(b0,1), size(C,2));
    for i=1:size(C,2)
    if( vertical )
      k0 = placet_element_get_attribute("$mysimname", C(i), "strength_y");
      placet_element_set_attribute("$mysimname", C(i), "strength_y", k0+1);
    else
      k0 = placet_element_get_attribute("$mysimname", C(i), "strength_x");
      placet_element_set_attribute("$mysimname", C(i), "strength_x", k0+1);
    end
      placet_test_no_correction(beamline, beam, "None");
    if( vertical )
       k0 = placet_element_get_attribute("$mysimname", C(i), "strength_y");
      placet_element_set_attribute("$mysimname", C(i), "strength_y", k0-1);
    else
      k0 = placet_element_get_attribute("$mysimname", C(i), "strength_x");
      placet_element_set_attribute("$mysimname", C(i), "strength_x", k0-1);
    end
    if( vertical )
      b1=placet_get_bpm_readings(beamline, B);
      b1=b1(:,2);
      R(:,i)=(b1-b0);
    else
      b1=placet_get_bpm_readings(beamline, B);
      b1=b1(:,1);
      R(:,i)=(b1-b0);
    end
    endfor
    placet_element_set_attribute(beamline, B, "resolution", Res);
endfunction
