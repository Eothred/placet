function L = genrandlist(interval, N)
L = [];
while( N > 0 )
  fail_unit = ceil(interval.*rand(1,1));
  if( (max(L == fail_unit) == 0) || (length(L)==0) ) 
        L(length(L)+1) = fail_unit;
        N = N - 1;    
  end% if
end% while

return;


