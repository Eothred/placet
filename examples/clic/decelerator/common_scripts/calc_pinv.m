%% calculate (pseudo) inverse, 1.0 cut corresponds to use all
%information (normal (least-squares) inverse
function R0_pinv = calc_pinv(R0, svd_cut)
[U,S,V]=svd(R0);
% use only high sing-values to don't overcorrect
n_diag = min(size(S));
n_start = round(n_diag*svd_cut)+1;
for n=n_start:n_diag,
  S(n,n);
  S(n,n) = 0;
end
R0_pinv = V*pinv(S)*U';

return;

