% strcmpi does not exist for octave! 
% we write our own instead... (EA)
function tr = strcmpi(s1, s2)

tr = strcmp(tolower(s1), tolower(s2));

return
