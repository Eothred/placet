# DECELERATOR scripts quick-start, E. Adli
# Requires placet-octave


This file describes how to start with the CLIC decelerator scripts.  Please refer to the general PLACET manual for principles of the beamlines, wake fields and the tracking.    To get started :


1) select/change desired options in the user_options.tcl (each option well documented in the file)

.../dec_scripts/user_options.tcl

In particular, select which system to be simulated :

# a 1050 m long decelerator sector with 1492 PETS (CLIC baseline)
set mysimname clic12
# the Test Beam Line, including matching and instrumentation sections, with 16 PETS
#set mysimname tbl12



2) run the main script

cd results/
placet-octave ../dec_scripts/main.tcl



3) retrieve data in the main dump-file (matlab/octave format), using e.g.
octave

load "../results/decsimdata.1.1.1.1.1.dat"
% contains emittances along the lattices, envelopes along the lattices the final beam, and more
%   see item 4)

% For the TBL: an additional particle beam is dumped at the end of the TBL-lattice
load "../results/beam_particles.1.1.1.1.1.dump"
% - contains the particle beam dumped at the end of the TBL lattice, in
%  Guinea Pig format



4) examples of data in dump-file

plot(Bs, y0_NC) % plot BPM readings in y for a Non Corrected machine
plot(Bs, y0_SC) % plot BPM readings in y for a 1-to-1 (Simple Corrected) machine
plot(Bs, y0_DFS) % plot BPM readings in y for a Dispersion Free Steered machine
% Bs: longitudinal distance along lattice in [m]
% y0_NC: BPM y-readings after tracking a non-corrected (NC) machine, in [um]

plot(Qs, env_NC(1:end-1)) % 3-sigma beam envelope at each quadrupole for a Non Corrected machine
plot(Qs, env_SC(1:end-1)) % 3-sigma beam envelope at each quadrupole for a 1-to-1 (Simple Corrected) machine
plot(Qs, env_DFS(1:end-1)) % 3-sigma beam envelope at each quadrupole for a Dispersion Free Steered machine
% Qs: longitudinal distance along lattice in [m]
% env_NC:  3-sigma beam envelope in each quad after tracking a non-corrected (NC) machine, in [um]

plot(b0nc(:,1), b0nc(:,3), 'x') % beam energy profile
% b0nc: the sliced beam after tracking a non-corrected (NC) machine.
% col. 1: longitudinal distance along the bunch, in [um]
% col. 2: macro particle weight, [-]
% col. 3: macro particle energy, in [GeV]
% col. 6: macro particle y, in [um]
% col. 7: macro particle yp, in [urad]
%   for full description of the sliced beam output format please refer
% to the PLACET manual Section 4.3 ("Output files")

% plot beta function calculated with lattice
load latticebeta.1.1.1.1.1;
plot(s, beta_x, '-+b;x;');
xlabel('s [m]'); ylabel('beta [m]');

