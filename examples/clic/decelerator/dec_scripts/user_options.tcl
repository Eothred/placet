# DECELERATOR AND TBL scripts
# E. Adli

# basic user options

# which machine to simulate (uncomment a single choice)
# a 1050 m long decelerator sector
set mysimname clic12
# the Test Beam Line, including matching and instrumentation sections
#set mysimname tbl12
# the TBTS PETS
#set mysimname tbts

# TBL online exhange?
# 0: use nominal ctf3 parameters, for baseline studies
# 1: read tbl magnet currents, initial energy and initial energy from files  (for online use in CTF3)
set tbl_online_exchange 0

# inital normalized emittance [m]
set emitt_x 150.0e-6
set emitt_y 150.0e-6

# bunch length [um]
set sigma_z 1000

# number of random machines to simulate (random misalignment is applied for each machine)
set n_machines 1

# default machine rms misalignments [um], [urad]
set sigma_quad 20
set sigma_quad_yp 1.0e3 
set sigma_bpm 20
set sigma_bpm_yp 1.0e3
set sigma_pets 100
set sigma_pets_yp 1.0e3
set sigma_quad_roll 0.0
set sigma_bpm_roll 0.0

# 1: force all misalignments to zero (perfect machine)
set zero_all_misalignments 0

# 1: do not apply initial survey command (leave misalignment as defined in lattice)
set skip_inital_survey 0
if { $tbl_online_exchange } {
    set skip_inital_survey 1
}

# BPM resolution [um]
set bpm_resolution 2.0

# mover corrector step size [um]
set  corrector_step_size 1

# performing tracking with selected correction schemes (1: selected,  multiple schemes can be selected, and the machine will then apply them in the order marked below)
set track_no_correction 1
set track_1to1_correction 0
set track_DFS_correction 0

# Options to use only a sub-set of BPMs and/or correctors 
#  use every n'th BPM for correction (1: use all BPMs)
set B_BBA_interval 1
#  use every m'th QUAD for correction (1: use all quads)
set Q_BBA_interval 1
#  use every k'th QUAD/BPM combo for correction (1: use all combos) - if > 1    above B and Q are IGNORED
set QB_BBA_interval 1
#set QB_BBA_interval $loop_param2
#  if 0: use corr-BPM  1 3 5 7 9 .... if 1: use corr-BPM  1 2 4 6 8 ...
#  note: first BPM is in front of a F-quad (DEFOC in y), therefore best effect in y when starting using 2 4 6
set BBA_interval_start2 0

# new batch of sub-set BPM variations  (here: keep correctors) 
#   every n'th BPM missing (1: no BPMs missing)
set B_BBA_interval_missing 1
#   grouping, n present, then n missing (1: no BPMs missing)
set B_BBA_grouping 1

# load R-matrix if exists, save R-matrix when generated
set do_multisim_load_save_R 1
# load and save machines
set do_multisim_load_mach 0
set do_multisim_save_mach 0

# MAIN BEAM: static beam offset and jitter beam offset (in y) in quantities of sigma_y  (total is partioned over available modes)
set static_mainbeam_offset [expr 0.0]
#set static_mainbeam_offset [expr 0.0/1.944582]
#set static_mainbeam_offset [expr ($loop_param1-1)/10.0]
set jitter_mainbeam_offset_total 0.0
# How to distribute the beam jitter: choose one of the following options :
# first: jitter distribuited on all mode freqs
set jitter_all_mode_frequencies 0
# second: all jitter one one mode freq, specified by param1
set jitter_param1_mode_frequency 0
# third: or all jitter on one freq, specified by loop_freq_offset+param1*loop_freq_multiple [GHz]  (combine with looping to get a full frequency scan)  
#    
set jitter_param1_frequency 0
set jitter_param1_loop_freq_offset 10.0
set jitter_param1_loop_freq_multiple 0.1
#set static_mainbeam_offset [expr 1000.0/333.562347162]

# do charge (changes beam charge distribution to imported "charge_ramp.dat")
set do_charge_ramp 0

# scaling factors for RQ, Q and w.  All modes scaled simultainously. Scalong factor of 1: nominal PETS parameters
set RQ_scaling_factor [expr 1.0e-0]
set w_t_scaling_factor [expr 1.0e-0]
set Q_t_scaling_factor [expr 1.0e-0]
set beta_t_scaling [expr 1.0e-0]
#set Q_t_scaling_factor $loop_param2

# 1: include coupler modes (included a seconds "PETS" element, "def_pets_cavity.m", with different modes, scaled as the PETS modes)
set include_PETS_coupler 0

# 1: Replace ALL PETS with drift  - also sets de0=0 (below) -  in order to simulate a lattice w/o PETS
set deactivate_PETS 0

# 1: track only centroid of each macroparticle (typical for wake field studies)  0: track macroparticle distributions as well
set set_zero_macroparticle_emittance 0


# component failure  

# number of quads failed
set n_failure_quad 0
# fail N quads in series for each failure (in total: n_failure_quad x quadfail_in_series are failed)
set quadfail_in_series 1
# effect of failure: reduction of current the following fraction of original current [-]
set quad_fail_current_reduction 0.0
# fraction of BPMs failed (e.g. 0.9: 90%)
set n_failure_BPM_percent 0
# set quadrupole error (from e.g. power supply)
set quad_strength_jitter 0.0


#
# initial beam parameters
#


# 1: automate initial current and energy calculations 
#    if 0:  initial current and energy will be applied (req power and energy spread will be ignored)
#    if 1:  first, the current will be adjusted to match the required power
#           then, the energy will be adjusted to yield the required energy spread spread (eta_extr)
#           (initial current and energy settings in this file will be ignored)
# CLIC decelerator options
if { $mysimname == "clic12" } {
    set activate_power_optimization 1
    # default current [A]
    set initial_current [expr 100.85]
    # default decelerator initial energy [GeV]
    set initial_energy 2.41
    # default power [MW]
    set requested_PETS_power 135.0
    # default maximum energy spread [-]
    set requested_E_spread 0.90
# TBL options
} elseif { $mysimname == "tbl12" } {
    set activate_power_optimization 0
    # default current [A]
    set initial_current [expr 28.0e-0]
    # default decelerator initial energy [GeV]
    set initial_energy 0.150
    # default power [MW]
    set requested_PETS_power 135.0
    # default maximum energy spread [-]
    set requested_E_spread 0.55
# TBTS options
} elseif { $mysimname == "tbts" } {
    set activate_power_optimization 0
    # default current [A]
    set initial_current 28
    # default decelerator initial energy [GeV]
    set initial_energy 0.150
    # default power [MW]
    set requested_PETS_power 135.0
    # default maximum energy spread [-]
    set requested_E_spread 0.55
}



# initial RMS uncorrelated relative energy spread (0.01 means 1 %)
#   NB: increases # of macroparticles and simulation time by n_E_macros_per_slice)
#   NB: calculation of eta_extr is based on mean energy (min. energy is not autmatically adjusted if simulating beam with energy spread)
# CLIC decelerator options
if { $mysimname == "clic12" } {
    # do not include in CLIC: gives conservative spread for wake damping
    set sigma_E_upon_E 0.0
    set n_E_macros_per_slice 5
# TBL options
} elseif { $mysimname == "tbl12" } {
    # important to include in TBL for beam size / dump measurements
    set sigma_E_upon_E 0.01
    set n_E_macros_per_slice 13
    #set n_E_macros_per_slice 7
# TBTS options
} elseif { $mysimname == "tbts" } {
    set sigma_E_upon_E 0.0
    set n_E_macros_per_slice 5
}



# Set arbitrary initial beam ellipse (default: use periodic solution of FODO cell)
# If set to 0, the CS parameters below will ignored
if { $mysimname == "tbl12" } {
    set use_arbitrary_initial_beam_ellipse 1
    set beta_x_initial 7.61907
    set alpha_x_initial -2.45418
    set beta_y_initial 7.24891
    set alpha_y_initial 2.53982
} else {
    set use_arbitrary_initial_beam_ellipse 0
}

# TBL only parameters 

# indicate where a PETS is installed (0: no PETS in this slot, 1: PETS installed in this slot)
#set tbl_PETS_list "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
#set tbl_PETS_list "1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
#set tbl_PETS_list "1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0"
#set tbl_PETS_list "1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0"
set tbl_PETS_list "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1"



#
# save extra information
# 

# save final beam
set savebeam_active 1
# save initial beam 
set savebeam_initial_active 0
# save lattice geometry
set savelattice_active 0
# save beta for lowest energy particle
set savebeta_multisim_active 0
# save transfer matrices M0 to M1 ... Mn-1 to Mn
#    where one point (0...n) is defined by a marker named "MATRIX.n"
set savetransfermatrices_active 0
# save response matrix
#    (from each corr to each BPM)
#    NB: response matrix does not use tranf. matrices, but beam
set saveresponsematrix_active 0



#
# simulation specific parameters
# 

# number of bunches to simulate (to reach steady-state, at least 20 for clic and at least 
if { $mysimname == "clic12" } {
    set n_bunches_sim 50
} elseif { $mysimname == "tbl12" } {
    #set n_bunches_sim 240
    set n_bunches_sim 60
    # full CTF3 train length
    #set n_bunches_sim 1680
} elseif { $mysimname == "tbts" } {
    set n_bunches_sim 500
}


# # of particles for particle beam - IF the beam is converted to a particle beam
#    CLIC: sliced beam by default, so this number is not used
#    TBL: beam is converted to a particle beam before the spectrometer
set n_particle_beam 50000
#set n_particle_beam 6000


#
#
#
# other parameterized values (change this file only if you know what you are doing)
source "$deceleratorrootpath/dec_scripts/advanced_options.tcl"
