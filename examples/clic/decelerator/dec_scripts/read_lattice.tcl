#set deceleratorrootpath "/afs/cern.ch/user/e/eadli/work/myPlacet/12ghz/"
set infile [open "$deceleratorrootpath/dec_scripts/drive.lattice" r]
#set n_PETS [expr 1372]
#set n_PETS [expr 10]

# read complete linac pattern
while { [gets $infile line] >= 0 } {
    #puts $line
    set n_PETSperFODO 0
    foreach element $line {
	lappend PETSlist_all $element
	incr n_PETSperFODO
    }
}
close $infile


# generate PETSpattern for one decelerator from whole lattice

# extract one and one decelerator until the required is found
set n_station_sofar 0
set n_FODO_sofar 0
while { $n_station_sofar < $n_decelerator_station  } {
    set n_PETS_sofar 0
    set PETSlist {}
    while { ($n_PETS_sofar < $n_PETS) && ([expr $n_FODO_sofar] < 99999)  } {
	# read a full FODO cell (two qpoles)
	for {set j 0} {$j < $n_PETSperFODO} {incr j } {
	    set element [lindex $PETSlist_all [expr $j+$n_PETSperFODO*$n_FODO_sofar]]
	    lappend PETSlist $element
	    if { $element == "1" } {
		incr n_PETS_sofar
	    }
	}
	incr n_FODO_sofar
    }
    # emergency break if error in file
    if { [expr $n_FODO_sofar] >= 99999 } {
	puts "ERROR: not enough PETS found\n"
	fdfd
    }
    incr n_station_sofar
}

# remove "surplus" PETS (should not exist for correctly defined decelerator? so give warning)
set n_backtrack 0
while { $n_PETS_sofar > $n_PETS  } {
    set n_index [expr [llength $PETSlist]-$n_backtrack]
    set element [lindex $PETSlist $n_index]
    if { $element == "1" } {
	incr n_PETS_sofar -1
	set PETSlist [lreplace $PETSlist $n_index $n_index "0"]
	puts "WARNING: PETS removed at end of decelerator to reach correct number within one FODO"
    }
    incr n_backtrack
}

set n_slots [llength $PETSlist]

# build mapping between PETS # and slot #   (NB: starting from 0]
set PETS_slot_mapping {}
set n_index 0
set n_empty 0
while { $n_index < $n_slots } {
    set element [lindex $PETSlist $n_index]
    if { $element == "1" } {
	lappend PETS_slot_mapping [expr $n_index+$n_empty]
    }
    incr n_index
}


#puts $PETSlist
#puts $PETS_slot_mapping
#puts [expr [llength $PETSlist]/$n_PETSperFODO*2]
