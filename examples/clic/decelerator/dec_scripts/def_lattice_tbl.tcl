# if in online exchange mode,
#   read virtual currents (CTF3 online exhange)
#
if { $tbl_online_exchange} {
Octave {
    addpath("$deceleratorrootpath/online_exchange");
    addpath("$deceleratorrootpath/online_exchange/ctf3_common"); % this contains the necessary files form the ctf3 common folder
    % read machine currents
    [quads, dipols, hcors, vcors hmovers vmovers Monitors] = tbl_extended();
    if( size(dipols,1) == 0 )
      devices = [quads hcors vcors hmovers vmovers];
    else
      devices = [quads hcors vcors dipols hmovers vmovers];
    end% if
    n_devices = length(devices);
    currents.name = cell(1,1);
    currents.myname = 'currents';
    for i=1:n_devices
    [devices(i).prefix '.' devices(i).name];
      value = readvmdevice([devices(i).prefix '.' devices(i).name]);
    madname = devices(i).madname;
      currents.name(:,i) = [devices(i).prefix '.' devices(i).name];
    currents.value(i) = value; % NB: THIS is the value that is saved by the 'write' routines (not the Tcl value)
     Tcl_SetVar(toupper(madname), value);
    #disp(toupper(madname));a
    end% for

% read energy from file
    energies.myname = 'energies';
    energies.name = cell(1,1);
    energies.name(:,1) = 'e0';
    n_energies = length(energies.name)
    for i=1:n_energies
      value = readvmparameters('energies', [energies.name(i)]);
    value = value
      energies.value(i) = value; % NB: THIS is the value that is saved by the 'write' routines (not the Tcl value)
      Tcl_SetVar(char(energies.name(i)),value );
    end% for

% read beamcurrent from file
    beamcurrents.myname = 'beamcurrents';
    beamcurrents.name = cell(1,1);
    beamcurrents.name(:,1) = 'I0';
    n_beamcurrents = length(beamcurrents.name)
    for i=1:n_beamcurrents
      value = readvmparameters('beamcurrents', [beamcurrents.name(i)]);
    value = value
      beamcurrents.value(i) = value; % NB: THIS is the value that is saved by the 'write' routines (not the Tcl value)
      Tcl_SetVar(char(beamcurrents.name(i)),value );
    end% for

}
# update initial energy and charge from input files
set e0 [expr $e0/1000.0]
set charge [expr $I0 / $fundamental_mode_f / $SI_e]

} else {
# if not in online exchange mode,
#   use baseline values for beam dynamics simulations 
set e0 $initial_energy
set e0_example_energy 0.111
set {CC.IQDD0820} 1.600000
set {CC.IQFD0840} 10.600000
set {CC.IQFL0910} 7.162361835
set {CC.IQDL0920} 8.034945386
set {CB.IQFM0110} 5.278015763
set {CB.IQDM0120} 4.873702441
set {CB.IQFR0200} 2.39516
set {CB.IQDR0240} 2.39516
set {CB.IQFR0300} 2.39516
set {CB.IQDR0340} 2.39516
set {CB.IQFR0400} 2.39516
set {CB.IQDR0440} 2.39516
set {CB.IQFR0500} 2.39516
set {CB.IQDR0540} 2.39516
set {CB.IQFR0600} 2.39516
set {CB.IQDR0640} 2.39516
set {CB.IQFR0700} 2.39516
set {CB.IQDR0740} 2.39516
set {CB.IQFR0800} 2.39516
set {CB.IQDR0840} 2.39516
set {CB.IQFR0900} 2.39516
set {CB.IQDR0940} 2.39516
set {CB.IQFN1010} 32.625
set {CB.IQDN1020} 30.375
set {CC.IDHD0940} 0.000000
set {CB.IDHD0130} 0.000000
set {CB.IDHD0140} 0.000000
set {CB.IDHD1040} 0.000000
set {CC.IDVD0940} 0.000000
set {CB.IDVD0130} 0.000000
set {CB.IDVD0140} 0.000000
set {CB.IDVD1040} 0.000000
set {CB.IMOV0205-H} 4000.000000
set {CB.IMOV0245-H} 4000.000000
set {CB.IMOV0305-H} 4000.000000
set {CB.IMOV0345-H} 4000.000000
set {CB.IMOV0405-H} 4000.000000
set {CB.IMOV0445-H} 4000.000000
set {CB.IMOV0505-H} 4000.000000
set {CB.IMOV0545-H} 4000.000000
set {CB.IMOV0605-H} 4000.000000
set {CB.IMOV0645-H} 4000.000000
set {CB.IMOV0705-H} 4000.000000
set {CB.IMOV0745-H} 4000.000000
set {CB.IMOV0805-H} 4000.000000
set {CB.IMOV0845-H} 4000.000000
set {CB.IMOV0905-H} 4000.000000
set {CB.IMOV0945-H} 4000.000000
set {CB.IMOV0205-V} 10000.000000
set {CB.IMOV0245-V} 10000.000000
set {CB.IMOV0305-V} 10000.000000
set {CB.IMOV0345-V} 10000.000000
set {CB.IMOV0405-V} 10000.000000
set {CB.IMOV0445-V} 10000.000000
set {CB.IMOV0505-V} 10000.000000
set {CB.IMOV0545-V} 10000.000000
set {CB.IMOV0605-V} 10000.000000
set {CB.IMOV0645-V} 10000.000000
set {CB.IMOV0705-V} 10000.000000
set {CB.IMOV0745-V} 10000.000000
set {CB.IMOV0805-V} 10000.000000
set {CB.IMOV0845-V} 10000.000000
set {CB.IMOV0905-V} 10000.000000
set {CB.IMOV0945-V} 10000.000000
# 1) downscale current downscaled with number of preceeding PETS
# 2) scale currents from example energy of 111 MeV to user defined energy
Octave {
    PETSlist_arr = "[$PETSlist]";
    PETSlist_arr = str2num(PETSlist_arr);
    Tcl_SetVar("CC.IQDD0820", ${CC.IQDD0820} * ($e0/$e0_example_energy) );
    Tcl_SetVar("CC.IQFD0840", ${CC.IQFD0840} * ($e0/$e0_example_energy) );
    Tcl_SetVar("CC.IQFL0910", ${CC.IQFL0910} * ($e0/$e0_example_energy) );
    Tcl_SetVar("CC.IQDL0920", ${CC.IQDL0920} * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFM0110", ${CB.IQFM0110} * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDM0120", ${CB.IQDM0120} * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFR0200", ${CB.IQFR0200} * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDR0240", ${CB.IQDR0240}*($e0-$de0*sum(PETSlist_arr(1:1)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFR0300", ${CB.IQFR0300}*($e0-$de0*sum(PETSlist_arr(1:2)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDR0340", ${CB.IQDR0340}*($e0-$de0*sum(PETSlist_arr(1:3)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFR0400", ${CB.IQFR0400}*($e0-$de0*sum(PETSlist_arr(1:4)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDR0440", ${CB.IQDR0440}*($e0-$de0*sum(PETSlist_arr(1:5)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFR0500", ${CB.IQFR0500}*($e0-$de0*sum(PETSlist_arr(1:6)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDR0540", ${CB.IQDR0540}*($e0-$de0*sum(PETSlist_arr(1:7)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFR0600", ${CB.IQFR0600}*($e0-$de0*sum(PETSlist_arr(1:8)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDR0640", ${CB.IQDR0640}*($e0-$de0*sum(PETSlist_arr(1:9)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFR0700", ${CB.IQFR0700}*($e0-$de0*sum(PETSlist_arr(1:10)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDR0740", ${CB.IQDR0740}*($e0-$de0*sum(PETSlist_arr(1:11)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFR0800", ${CB.IQFR0800}*($e0-$de0*sum(PETSlist_arr(1:12)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDR0840", ${CB.IQDR0840}*($e0-$de0*sum(PETSlist_arr(1:13)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFR0900", ${CB.IQFR0900}*($e0-$de0*sum(PETSlist_arr(1:14)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDR0940", ${CB.IQDR0940}*($e0-$de0*sum(PETSlist_arr(1:15)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQFN1010", ${CB.IQFN1010}*($e0-$de0*sum(PETSlist_arr(1:16)))/$e0 * ($e0/$e0_example_energy) );
    Tcl_SetVar("CB.IQDN1020", ${CB.IQDN1020}*($e0-$de0*sum(PETSlist_arr(1:16)))/$e0 * ($e0/$e0_example_energy) );
}


}

#   magnet excitation constants
# ORIGINALLY FROM MAD :
#
puts "e0: $e0"
set P08 [expr sqrt($e0*1e3*$e0*1e3 - 0.511*0.511)]

##########  Define magnets and BPMs  ##########
#  ! excitation constants dB/dx/I = 0.056 T/Am

#! ---- QL3 quads ----
#  ! for new QL3 type, QXD, water cooled
set LQL 0.220
set ECQD  0.056
set FQD [expr $SI_c*1e-6*$ECQD]   

# ! ---- Terwilliger Quad ----
set LQLT 0.380
set ECQDT   0.0359
set FQDT  [expr $SI_c*1e-6*$ECQDT]

# ! ---- TSL Celsius Quad ----
set LQLS  0.295
set ECQDS  0.106
set FQDS [expr $SI_c*1e-6*$ECQDS]

# ! ---- LURE round Quad ----
#              ! excitation constant for 100 mm aperture
set LQLL 0.340
set LQLM 0.300
set ECQDL1  0.0543
set FQDL1   [expr $SI_c*1e-6*$ECQDL1]
#              ! excitation constant for 80 mm aperture
set ECQDL2 0.0847
set FQDL2  [expr $SI_c*1e-6*$ECQDL2]
#              ! excitation constant for 110 mm aperture
set ECQDL3  0.0449
set FQDL3  [expr $SI_c*1e-6*$ECQDL3]
#              ! excitation constant for 60 mm aperture
set ECQDL4 0.335
set FQDL4 [expr $SI_c*1e-6*$ECQDL4]

# ! ---- TBL quad ----
set LQLX  0.186
set ECQDX  0.873
set FQDX  [expr $SI_c*1e-6*$ECQDX]

#
# calculate normalized magnet strengths
#
set {CC.KQDD0820}  [expr -$FQD * ${CC.IQDD0820} / $P08]
set {CC.KQFD0840}  [expr $FQD * ${CC.IQFD0840} / $P08]
set {CC.KQFL0910}  [expr $FQDS * ${CC.IQFL0910} / $P08]
set {CC.KQDL0920}  [expr -$FQDS * ${CC.IQDL0920} / $P08]
set {CB.KQFM0110}  [expr $FQDL4 * ${CB.IQFM0110} / $P08]
set {CB.KQDM0120}  [expr -$FQDL4 * ${CB.IQDM0120} / $P08]
set {CB.KQFR0200}  [expr $FQDX* ${CB.IQFR0200} / $P08]
set {CB.KQDR0240}  [expr -$FQDX * ${CB.IQDR0240} / $P08]
set {CB.KQFR0300}  [expr $FQDX* ${CB.IQFR0300} / $P08]
set {CB.KQDR0340}  [expr -$FQDX * ${CB.IQDR0340} / $P08]
set {CB.KQFR0400}  [expr $FQDX* ${CB.IQFR0400} / $P08]
set {CB.KQDR0440}  [expr -$FQDX * ${CB.IQDR0440} / $P08]
set {CB.KQFR0500}  [expr $FQDX* ${CB.IQFR0500} / $P08]
set {CB.KQDR0540}  [expr -$FQDX * ${CB.IQDR0540} / $P08]
set {CB.KQFR0600}  [expr $FQDX* ${CB.IQFR0600} / $P08]
set {CB.KQDR0640}  [expr -$FQDX * ${CB.IQDR0640} / $P08]
set {CB.KQFR0700}  [expr $FQDX* ${CB.IQFR0700} / $P08]
set {CB.KQDR0740}  [expr -$FQDX * ${CB.IQDR0740} / $P08]
set {CB.KQFR0800}  [expr $FQDX* ${CB.IQFR0800} / $P08]
set {CB.KQDR0840}  [expr -$FQDX * ${CB.IQDR0840} / $P08]
set {CB.KQFR0900}  [expr $FQDX* ${CB.IQFR0900} / $P08]
set {CB.KQDR0940}  [expr -$FQDX * ${CB.IQDR0940} / $P08]
set {CB.KQFN1010}  [expr $FQDL3 * ${CB.IQFN1010} / $P08]
set {CB.KQDN1020}  [expr -$FQDL3 * ${CB.IQDN1020} / $P08]
#
# calculate quadrupole positions
#
set MOV_SCALE_H [expr 1.0]
set MOV_OFFSET_H [expr 4000.0]
set MOV_SCALE_V [expr 0.196]
set MOV_OFFSET_V [expr 10000.0*$MOV_SCALE_V]
set {CB.PMOV0205-H} [expr ${CB.IMOV0205-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0245-H} [expr ${CB.IMOV0245-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0305-H} [expr ${CB.IMOV0305-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0345-H} [expr ${CB.IMOV0345-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0405-H} [expr ${CB.IMOV0405-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0445-H} [expr ${CB.IMOV0445-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0505-H} [expr ${CB.IMOV0505-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0545-H} [expr ${CB.IMOV0545-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0605-H} [expr ${CB.IMOV0605-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0645-H} [expr ${CB.IMOV0645-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0705-H} [expr ${CB.IMOV0705-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0745-H} [expr ${CB.IMOV0745-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0805-H} [expr ${CB.IMOV0805-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0845-H} [expr ${CB.IMOV0845-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0905-H} [expr ${CB.IMOV0905-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0945-H} [expr ${CB.IMOV0945-H}*$MOV_SCALE_H - $MOV_OFFSET_H]
set {CB.PMOV0205-V} [expr ${CB.IMOV0205-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0245-V} [expr ${CB.IMOV0245-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0305-V} [expr ${CB.IMOV0305-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0345-V} [expr ${CB.IMOV0345-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0405-V} [expr ${CB.IMOV0405-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0445-V} [expr ${CB.IMOV0445-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0505-V} [expr ${CB.IMOV0505-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0545-V} [expr ${CB.IMOV0545-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0605-V} [expr ${CB.IMOV0605-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0645-V} [expr ${CB.IMOV0645-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0705-V} [expr ${CB.IMOV0705-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0745-V} [expr ${CB.IMOV0745-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0805-V} [expr ${CB.IMOV0805-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0845-V} [expr ${CB.IMOV0845-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0905-V} [expr ${CB.IMOV0905-V}*$MOV_SCALE_V - $MOV_OFFSET_V]
set {CB.PMOV0945-V} [expr ${CB.IMOV0945-V}*$MOV_SCALE_V - $MOV_OFFSET_V]


#
# call the TBL lattice definition
#
source "$deceleratorrootpath/dec_scripts/user_tblbeamline.tcl"

