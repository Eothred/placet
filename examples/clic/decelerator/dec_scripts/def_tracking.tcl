# Define beam initial offset values as beam rms size ( based on initial emittance and beta )
set sigma_y [expr sqrt($emitt_y_sigma_calc*1e-7*$twiss_match(beta_y)*0.511e-3/$e0)*1e6]
puts "Offset (sigma_y): $sigma_y"
puts ""
set sigma_yp [expr sqrt($emitt_y_sigma_calc*1e-7/$twiss_match(beta_y)*0.511e-3/$e0)*1e6]
set sigma_x [expr sqrt($emitt_x_sigma_calc*1e-7*$twiss_match(beta_x)*0.511e-3/$e0)*1e6]
puts "Offset (sigma_x): $sigma_x"
puts ""
set sigma_xp [expr sqrt($emitt_x_sigma_calc*1e-7/$twiss_match(beta_x)*0.511e-3/$e0)*1e6]

puts "Starting tracking with FirstOrder: $is_first_order\n"
FirstOrder $is_first_order


# choose main beam (eventually with missing bunches)
if { $main_beam_missing_bunches } {
   set beamname1 $mainbeamM1
}

# response matrix depends on beam type (macro or not)
#  m0: macroparticle beam with dist
#  m9: transverse macroparticles to simulate dist
if { $macroparticle_init_y } {
    if { $minibeam } {
	set Rsuffix m9_minibeam
    } else {
	set Rsuffix m9
    }
} else {
    if { $minibeam } {
	set Rsuffix m0_minibeam
    } else {
	set Rsuffix m0
    }
}
if { $deactivate_PETS } {
    set Rsuffix ${Rsuffix}_0PETS
}


if { $mysimname == "clic12" } {
    # sigma_y OVERRIDE
    #set sigma_y 318.726824799
    # DFS parameters
    if { $DFS_one_bin } {
	set binlength 2048
	set binoverlap 1024
    } else {
	set binlength 48
	set binoverlap 24
    }
} elseif { $mysimname == "tbl12" } {
    # sigma_y OVERRIDE
    #set sigma_y 1553.31867947   
    # DFS parameters (TBL: always one bin)
    set binlength 36
    set binoverlap 18
}   



# Random reset OCTAVE random generator (will NOT affect PLACET random numbers)
Octave {
    rand("seed", 1999);
}


proc my_survey {} {
    Zero
    Clic
#     ElementSetToOffset 1 -y 10
}

# proc my_staticsurvey {} {
#     Zero
#     Octave {
# 	beamlinename = Tcl_GetVar("mysimname")
# 	C = placet_get_number_list(beamlinename, "quadrupole")
# 	C_after = placet_element_get_attribute(beamlinename, C, "y")'
# 	y_C = [zeros(length(C), 1) ones(length(C), 1)]*10
# 	placet_vary_corrector(beamlinename, C, y_C);
# 	C_after = placet_element_get_attribute(beamlinename, C, "y")'
#     }
# }

#set seedlist "12 34 56 79"
#RandomReset -seed $seedlist
#RandomReset -seed {3 65 23 1}
#     ScatterGirder -scatter_y 200 -scatter_yp 20


###
# define N sigma beam envelope
set envelope_cut [expr $envelope_sigma_cut]


puts "\nn_machines: $n_machines"

puts "\next_param1: $ext_param1"

set param1_n 1
foreach param1 $param1_list {
puts "param1: $param1"

set param2_n 1
foreach param2 $param2_list {
puts "param2: $param2\n"
# alternative loop: separate lambda_t per run
# foreach mode_t $modes_t 

    for {set run 1} {$run <= $n_runs} {incr run} {

	# SurveyError 
 	SurveyErrorSet \
            -cavity_x $sigma_pets_x \
            -cavity_y $sigma_pets \
 	    -cavity_xp $sigma_pets_xp \
 	    -cavity_yp $sigma_pets_yp \
	    -quadrupole_x [expr $sigma_quad_x] \
	    -quadrupole_y [expr $sigma_quad] \
	    -quadrupole_xp $sigma_quad_xp \
	    -quadrupole_yp $sigma_quad_yp \
		-quadrupole_roll $sigma_quad_roll \
	        -bpm_x [expr $sigma_bpm_x] \
	        -bpm_y [expr $sigma_bpm] \
	        -bpm_xp [expr $sigma_bpm_xp] \
	        -bpm_yp [expr $sigma_bpm_yp] \
	        -bpm_roll [expr $sigma_bpm_roll] 

	# SurveyError - PERFECT MACHINE
	if { $zero_all_misalignments } {
 	SurveyErrorSet \
            -cavity_x 0 \
            -cavity_y 0 \
 	    -cavity_xp 0 \
 	    -cavity_yp 0 \
	    -quadrupole_x 0 \
	    -quadrupole_y 0 \
	    -quadrupole_xp 0 \
	    -quadrupole_yp 0 \
		-quadrupole_roll 0 \
	        -bpm_x 0 \
	        -bpm_y 0 \
	        -bpm_xp 0 \
	        -bpm_yp 0 \
	        -bpm_roll 0
	}	    
	
	# SurveyError - X OFFSET, FOR DIPOLE
#  	SurveyErrorSet \
#             -cavity_y 0 \
#             -cavity_x 200 \
#  	    -cavity_yp 0 \
# 	    -quadrupole_x [expr 20] \
# 	    -quadrupole_y [expr 0] \
# 		-quadrupole_yp 0 \
# 		-quadrupole_roll 0 \
# 	        -bpm_y [expr 0]


# #   JITTER PETS/QUAD AS IF AFTER CORRECION, BOTH X AND Y
# 	SurveyErrorSet -cavity_y 200 \
# 	    -cavity_x  200 \
# 	    -cavity_yp 0  \
# 	    -cavity_xp 0  \
# 		-quadrupole_y 20 \
# 		-quadrupole_x 20 \
# 		-quadrupole_yp 0 \
# 		-quadrupole_xp 0 \
# 		-quadrupole_roll 0 \
# 	        -bpm_y [expr 0 ] \
# 	        -bpm_x [expr 0 ]


#            -cavity_y $loop_param2 \
# 	    -cavity_yp  [expr 4*($loop_param2/$cavitylength) ]  \


	# KNOWN OFFSET OVERRIDE
#	    ElementSetToOffset 9 -y 10

   # JITTER ALL ELEMENTS  Y
# 	SurveyErrorSet -cavity_y $param1 \
# 	    -cavity_yp [expr 4*($param1/$cavitylength) ] \
# 		-quadrupole_y $param1 \
# 		-quadrupole_yp 0 \
# 		-quadrupole_roll 0 \
# 	        -bpm_y [expr $param1 ]

# CLICNOTE12 ALL IN:  JITTER ALL ELEMENTS BOTH X AND Y - BUT NOT IN QUADX  (until we implement DFS for that as well.... :/  )
# 	SurveyErrorSet -cavity_y [expr 1*$param1] \
# 	    -cavity_x [expr 1*$param1] \
# 	    -cavity_yp [expr 4*($param1/$cavitylength) ] \
# 	    -cavity_xp [expr 4*($param1/$cavitylength) ] \
# 	    -quadrupole_y [expr $param1/5]  \
# 	    -quadrupole_x [expr 0*$param1/5] \
# 		-quadrupole_yp 0 \
# 		-quadrupole_xp 0 \
# 		-quadrupole_roll 0 \
# 	        -bpm_y [expr $param1/5 ] \
# 	        -bpm_x [expr 0*$param1/5 ]




	    #
	    # INITIAL BEAM SETTINGS
	    #
	    
	    # Choose main beam
	    #set $beamname1 mainbeamM1


	    # Choose beams for DFS  ( wo PETS: energy beam,  w PETS: current beam )
	if {$deactivate_PETS} { 	
	    set testbeam1 $testbeamE1 
	} else {
	    set testbeam1 $testbeamM1 
	}
	# testbeam2 is not used for the moment
	set testbeam2 $testbeamI2   

	if {$macroparticle_init_y} {
	} else {
	    # RESET Beam Offsets (from prev. loops)
	    BeamSetToOffset -beam $beamname1 -y 0
	    #BeamSetToOffset -beam $beamname1 -x 0
	    BeamSetToOffset -beam $testbeamNOM -y 0
	    BeamSetToOffset -beam $testbeam1 -y 0
	    BeamSetToOffset -beam $testbeam2 -y 0
	}

	    # Static offset

	    # offset relative to sigma_y
	    #set static_offset 0.5

	    # absolute offset (1 mm) for neutral comparision  (OVERRIDES options)
	    #set static_mainbeam_offset [expr 100/$sigma_y]

	if {$macroparticle_init_y} {
	} else {
	    BeamSetToOffset -beam $beamname1 -y [expr $static_mainbeam_offset*$sigma_y]
	    BeamAddOffset -beam $beamname1 -x [expr $static_mainbeam_offset_x*$sigma_x]
	    BeamSetToOffset -beam $testbeamNOM -y [expr $static_testbeamnom_offset*$sigma_y]
	    BeamSetToOffset -beam $testbeam1 -y [expr $static_testbeam_offset*$sigma_y]
	    BeamSetToOffset -beam $testbeam2 -y [expr $static_testbeam_offset*$sigma_y]
	}

	   #     BeamSetOffsetSine -beam $beamname1 -lambda 0.015 -y [expr 0.5*$sigma_y] \
	   #        -phi_0 [expr acos(-1.0)*0.5]

	# separate jitter, or all frequencies jittered?
	    if {[info exists couplerlength]} {        
		set n_frequencies [expr [llength $modes_t] + [llength $modes_coupler_t]]
	    } else {
		set n_frequencies [expr [llength $modes_t]]
	    }
	    # MAIN BEAM JITTER
 	    set jitter_mainbeam_offset [expr $jitter_mainbeam_offset_total / sqrt($n_frequencies)]
	    set jitter_mainbeam_offset_x [expr $jitter_mainbeam_offset_total_x / sqrt($n_frequencies)]

	if { $jitter_all_mode_frequencies == "1" } {

	    foreach mode_t $modes_t {
		set lambda_t [lindex $mode_t 0]
		set lambda_t [expr abs($lambda_t)*1.00]
		#puts "\'lambda_t_cav\' is: $lambda_t"
		BeamSetOffsetSine -beam $beamname1 -lambda [expr $lambda_t] -y [expr $jitter_mainbeam_offset*$sigma_y] \
		    -phi_0 [expr acos(-1.0)*0.5]
           }
	    foreach mode_t $modes_t {
		set lambda_t [lindex $mode_t 0]
		set lambda_t [expr abs($lambda_t)*1.00]
		#puts "\'lambda_t_cav\' is: $lambda_t"
		BeamSetOffsetSine -beam $beamname1 -lambda [expr $lambda_t] -x [expr $jitter_mainbeam_offset_x*$sigma_x] \
		    -phi_0 [expr acos(-1.0)*0.5]

            }

	    #
	    puts "\nBeam offsets induced (multiples of sigma_y: $sigma_y \[um\]): static: $static_mainbeam_offset, jitter each wavelength: $jitter_mainbeam_offset"
	    foreach mode_t $modes_t {
		set lambda_t [lindex $mode_t 0]
		set lambda_t [expr abs($lambda_t)]
		    puts "  Jitter frequency induced \[GHz\]: [expr ($SI_c/$lambda_t/1e9)]"
	    }
	    if {[info exists couplerlength]} {        
		foreach mode_coupler_t $modes_coupler_t {
		    set lambda_t [lindex $mode_coupler_t 0]
		    set lambda_t [expr abs($lambda_t)]
		    puts "  Jitter frequency induced \[GHz\]: [expr ($SI_c/$lambda_t/1e9)]"
		}
	    }
	    puts "\n"

	}

	# separate jitter
	if { $jitter_param1_mode_frequency == "1" } {
	#NB: this line sets first mode freq
 	set lambda_t [lindex [lindex $modes_t [expr 0]] 0]
	# comment next line to let mode depend on loop_param1
 	#set lambda_t [lindex [lindex $modes_t [expr $loop_param1]] 0]
 		#puts "\'lambda_t_cav\' is: $lambda_t"
	BeamSetOffsetSine -beam $beamname1 -lambda [expr $lambda_t] -y [expr $jitter_mainbeam_offset_total*$sigma_y] \
 		    -phi_0 [expr acos(-1.0)*0.5]
	BeamSetOffsetSine -beam $beamname1 -lambda [expr $lambda_t] -x [expr $jitter_mainbeam_offset_total_x*$sigma_x] \
 		    -phi_0 [expr acos(-1.0)*0.5]

	    puts "  \nSsingle jitter frequency induced \[GHz\]: [expr ($SI_c/$lambda_t/1e9)]\n"
}


	# separate jitter, loop_param2 decides the frequency (typical for loop scan)
	if { $jitter_param1_frequency == "1" } {
 	    set lambda_t [expr $SI_c/(($jitter_param1_loop_freq_offset + $loop_param1*$jitter_param1_loop_freq_multiple)*1e9)]
 		#puts "\'lambda_t_cav\' is: $lambda_t"
	BeamSetOffsetSine -beam $beamname1 -lambda [expr $lambda_t] -y [expr $jitter_mainbeam_offset_total*$sigma_y] \
 		    -phi_0 [expr acos(-1.0)*0.5]
	BeamSetOffsetSine -beam $beamname1 -lambda [expr $lambda_t] -x [expr $jitter_mainbeam_offset_total_x*$sigma_x] \
 		    -phi_0 [expr acos(-1.0)*0.5]

	    puts "  \nSsingle jitter frequency induced \[GHz\]: [expr ($SI_c/$lambda_t/1e9)]\n"
}


      
	    #set ... .$loop_param1_n.$loop_param2_n.$param1_n.$run     : use this in order to save spectrum for each beam, not for the moment in order to save disk space
	    set dummy "_spectrum_qpole"
	    set bpmspectrumname $mysimname$dummy

	    #set eoffset "[ElementGetOffset 2] [ElementGetOffset 10]  [ElementGetOffset 114] [ElementGetOffset 122] [ElementGetOffset 130]"
	    #puts "1 effset $eoffset"





#
#
#   
#     TRACKING STARTS HERE !
#
#
#


	#
	# MULTISIM.m  is THE main tracking script (fully Octave)
	#
	if { $do_multisim } {
	    # For TBL we must generate two response matrices, one with coils and the other with quad movers
	    if { $mysimname == "tbl12" && $saveresponsematrix_active } {
		set dipole_correctors 0
		source "$deceleratorrootpath/dec_scripts/multisim.m"
		set dipole_correctors 1
		set Rsuffix "${Rsuffix}_coils"
		source "$deceleratorrootpath/dec_scripts/multisim.m"
	    } else {
	    # normal execution
	       source "$deceleratorrootpath/dec_scripts/multisim.m"
	    }
	    exit
	}


   
	#
	# The following is old TCL-based stuff - please disregard  (kept here in case someone one day should be so stupid to kick Octave out of placet),  Best regards, EA
	#
	   
	    if { $track_no_correction_wo_octave } {
		puts "  \n\n...starting do_no_correction...\n\n"
		# RANDOM RESET
		RandomReset

	       # NO CORRECTION
	       TestNoCorrection -beam  $beamname1  -emitt_file emitt.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run -survey my_survey -machines $n_machines
		if { !$macroparticle_init_y } {
		    BpmReadings -file BPM.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run -correct 1
		}
#	       set myreads [BpmReadings]
#	       puts $myreads
#		set myreads [BpmDisplay 132]
#	       puts $myreads
#	       BpmDisplay 15 -file BPM15.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run -correct 1  # not working
	    


	    # Octave visualization
	    Octave {
		beamlinename = Tcl_GetVar("mysimname")
		beamname1 = Tcl_GetVar("beamname1")

		# display quadrupole position after correction
		C = placet_get_number_list(beamlinename, "quadrupole");
		B = placet_get_number_list(beamlinename, "bpm");
		P = placet_get_number_list(beamlinename, "cavity_pets");
		E = placet_get_name_number_list(beamlinename, "*");
		# PETS not yet implemented as octave type....
		#P = [5   14   23   32   41   50   59   68   77   86   95  104  113  122  131  140];
		disp('Saving quad positions.');
		#placet_element_set_attribute(beamlinename, P, "y", 3);
		C_after = placet_element_get_attribute(beamlinename, C, "y")';
		B_after = placet_element_get_attribute(beamlinename, B, "y")';
		E_after = placet_element_get_attribute(beamlinename, E, "y")';
		if( length(P)>0 )
		   P_after = placet_element_get_attribute(beamlinename, P, "y")';
		endif
		#[R, S, I] = placet_get_bpm_readings(beamlinename);
		#R
		#S
		#I
		
		#P_after = E_after(P);
		save -text "Cafter.dat" C_after
		save -text "Bafter.dat" B_after
		if( length(P)>0 )
		   save -text "Pafter.dat" P_after
		endif
		#save -text "Eafter.dat" E_after
	    }
	   }

  	    if { $track_1to1_correction_wo_octave } {
		puts "  \n\n...starting do_simple_correction...\n\n"

	        # RESET MACHINE
 	        TestNoCorrection -beam $beamname1    -emitt_file   /dev/null    -survey Zero   -machines 1

		# RANDOM RESET
		RandomReset 


	    # MACHINE CORRECTED - SC
 	    #TestSimpleCorrection -beam $beamname1  -emitt_file emitt_corr.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run -survey my_survey -bpm_resolution $bpm_resolution  -machines $n_machines
		# SC with BINS
 	    TestSimpleCorrection -beam $beamname1  -emitt_file emitt_corr.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run -survey my_survey -bpm_resolution $bpm_resolution  -machines $n_machines -binlength 600
		if { !$macroparticle_init_y } {
		    BpmReadings -file BPM_corr.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run -correct 1
		}

	    # Octave visualization
	    Octave {
		beamlinename = Tcl_GetVar("mysimname")
		beamname1 = Tcl_GetVar("beamname1")

		# display quadrupole position after correction
		C = placet_get_number_list(beamlinename, "quadrupole");
		B = placet_get_number_list(beamlinename, "bpm");
		E = placet_get_name_number_list(beamlinename, "*");
		# PETS not yet implemented as octave type....
		#P = [5   14   23   32   41   50   59   68   77   86   95  104  113  122  131  140];   ONLY WITH BEAM SAVE
		disp('Saving quad positions.');
		C_after = placet_element_get_attribute(beamlinename, C, "y")';
		B_after = placet_element_get_attribute(beamlinename, B, "y")';
		E_after = placet_element_get_attribute(beamlinename, E, "y")';
		#P_after = placet_element_get_attribute(beamlinename, P, "y")';
		#P_after = E_after(P);
		save -text "Cafter_corr.dat" C_after
		save -text "Bafter_corr.dat" B_after
		#save -text "Pafter_corr.dat" P_after
		#save -text "Eafter.dat" E_after
	    }
 
	    }





 	    if { $track_DFS_correction_wo_octave } {
		puts "  \n\n...starting do_DFS...\n\n"

	    # RESET MACHINE
 	    TestNoCorrection -beam $beamname1     -emitt_file   /dev/null    -survey Zero   -machines 1

	    # RANDOM RESET
	    RandomReset


	    # MACHINE CORRECTED - DSF
	    #set ql [QuadrupoleGetStrengthList]   
	    #set correctors [QuadrupoleNumberList]  # correctors $correctors
	    puts "Running DFS with test beams: $testbeam1 $testbeam2\n"
	    puts "BPM resolution: $bpm_resolution"
	    set jitter_y [expr $jitter_DFS*$sigma_y]
	    puts "Jitter_DFS: $jitter_y"
 	    TestMeasuredCorrection -beam0 $beamname1 -beam1 $testbeam1  -emitt_file emitt_DFS.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run  -bpm_resolution $bpm_resolution -wgt0 1.0 -wgt1 [expr $ext_param1]   -binlength $binlength -binoverlap $binoverlap  -correct_full_bin 1  -beamlines_iterations 1  -bin_iterations 1 -survey my_survey -jitter_y $jitter_y -machines $n_machines
		# example with working parameters below (very difficult to obtain for these old placet functions .....)
 	    #TestMeasuredCorrection -beam0 $beamname1 -beam1 $testbeam1 -beam2 $testbeam2  -emitt_file emitt_DFS.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run  -bpm_resolution $bpm_resolution -wgt0 1.0 -wgt1 [expr $ext_param1] -wgt2 [expr $ext_param1]   -binlength 36 -binoverlap 18  -correct_full_bin 1  -beamlines_iterations 1  -bin_iterations 1 -survey my_survey  -machines 10
		if { !$macroparticle_init_y } {
		    BpmReadings -file BPM_DFS.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run -correct 1
		}
		# Octave visualization
	    Octave {
		beamlinename = Tcl_GetVar("mysimname")
		beamname1 = Tcl_GetVar("beamname1")

		# display quadrupole position after correction
		C = placet_get_number_list(beamlinename, "quadrupole");
		B = placet_get_number_list(beamlinename, "bpm");
		E = placet_get_name_number_list(beamlinename, "*");
		# PETS not yet implemented as octave type....
		#P = [5   14   23   32   41   50   59   68   77   86   95  104  113  122  131  140];
		disp('Saving quad positions.');
		C_after = placet_element_get_attribute(beamlinename, C, "y")';
		B_after = placet_element_get_attribute(beamlinename, B, "y")';
		E_after = placet_element_get_attribute(beamlinename, E, "y")';
		#P_after = placet_element_get_attribute(beamlinename, P, "y")';
		#P_after = E_after(P);
		save -text "Cafter_DFS.dat" C_after
		save -text "Bafter_DFS.dat" B_after
		#save -text "Pafter_DFS.dat" P_after
		#save -text "Eafter.dat" E_after
	    }

	    }
#
	    #TwissPlot -beam $beamname1 -file twiss.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run
	    #EnergySpreadPlot -beam $beamname1 -file energyspread.$param1_n.$run
	    #BeamEnergyPlot -beam $beamname1 -file energy.$param1_n.$run
	    #BeamEnergyProfilePlot -beam $beamname1 -file energyprofile.save
	    

	    #puts [array get beam_measure]
	    #set filename [format "beammeasure.%s.%s.%s.%s.%s" $loop_param1_n $loop_param2_n $param1_n $param2_n $run]
	    #set fileId [open $filename "w"]
	    #foreach {quan value} [array get beam_measure] {
	#	puts $fileId "$value"
	    #}
	    #close $fileId
	    
	#set cmd "tar cf res.tar spect.* env_off* env_sin* env_cos* res.param1.*"
	#exec bash << $cmd
	#exec gzip -f -9 res.tar
    }
    incr param2_n
}
incr param1_n
}

#
# Twissplot of last run
#
#TwissPlot -beam $beamname1 -file twiss.$loop_param1_n.$loop_param2_n.[expr $param1_n-1].[expr $param2_n-1].[expr $run-1]


#
# Write overview of final lattice elements
#
if { $savelattice_active } {
Octave {
     filename = "lattice.txt";
     fid = fopen (filename, "wt");
     # Do the actual I/O here...
   E = placet_get_name_number_list("$mysimname", "*");
   n_E = length(E);
   #lattice = zeros(n_E, 1);
   for n=0:(n_E-1)
    fprintf(fid, "%5s", num2str(n));
    fprintf(fid, "%15s", placet_element_get_attribute("$mysimname", n, "type_name"));
    fprintf(fid, "%10s", num2str(placet_element_get_attribute("$mysimname", n, "s")));
    fprintf(fid, "%10s", num2str(placet_element_get_attribute("$mysimname", n, "length")));
    fprintf(fid, "%10s\n", num2str(placet_element_get_attribute("$mysimname", n, "y")));
   endfor
    #
    fclose (fid);
}
}





# 
# HTGEN example
#
# TestNoCorrection -beam $beamname1     -emitt_file   /dev/null    -survey Zero   -machines 1
# set nelements [llength [NameNumberList "*"]] 
# puts "nelements: $nelements"
# set i 0
# Vacuum -nel $nelements -iel $i -temperature 273 -thetamin 0.1 -gas { { 7. 14. 50. } }
 #TrackBackground -beam $beamname1 -dir /afs/cern.ch/user/e/eadli/work/myPlacet/12ghz/afsres1 -charge 100000 -start 0 -fulltracking 1 -linac 0
# puts "TrackBackground START: $nelements"
# TrackBackground -beam $beamname1  -charge 10000 -start 0 -fulltracking 1 -linac 0
# puts "TrackBackground OK: $nelements"
#exit
# HTGEN end

