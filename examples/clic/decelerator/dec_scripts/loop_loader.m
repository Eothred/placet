%
% LOOP LOADER for decelerator looped sims
%   (set the following arguments equals to # of loops of param1 and param2)
placet_loop_param1 = 41;
placet_loop_param2 = 1;
n_res = [1:placet_loop_param1];
env_NC_max = 0;
env_SC_max = 0;
env_DFS_max = 0;
env_NC_avg = 0;
env_SC_avg = 0;
env_DFS_avg = 0;
i = 1;
filename = ['decsimdata.' num2str(i) '.1.1.1.1.dat'];
 load([filename]);
env_NC_0 = max(envmax_NC);
env_SC_0 = max(envmax_SC);
env_DFS_0 = max(envmax_DFS);
for i = 1:placet_loop_param1
for j = 1:placet_loop_param2
  filename = ['decsimdata.' num2str(i) '.' num2str(j) '.1.1.1.dat'];
  load([filename]);
  env_NC_max(i,j) = max(envmax_NC)/1e3;
  env_SC_max(i,j) = max(envmax_SC)/1e3;
  env_DFS_max(i,j) = max(envmax_DFS)/1e3;
  env_NC_avg(i,j) = max(env_NC)/1e3;
  env_SC_avg(i,j) = max(env_SC)/1e3;
  env_DFS_avg(i,j) = max(env_DFS)/1e3;
end% for
end% for


