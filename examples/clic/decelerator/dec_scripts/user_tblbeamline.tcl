#
#  TBL lattice definition
#

# intital elements
set n_pets_slot 0
set n_pets_installed 0
Girder
if { $savebeam_initial_active } {
    TclCall -name "tcl dump" -script {BeamSaveAll -file beam_initial.dat}
}
Bpm -name "CB.OBSERVER" -length 0

#
# PART IMPORTED FROM MADX
#
#   Manual CHANGES:
#   - comment out SBends in sliced part
#   - Remove a few BPMs 
#   - Replace Drift -name "CB.DCELL4" -length 0.986 with "insert_TBL_PETS.tcl"
#   - Replace Quadrupole "*$e0" with "*($e0-$de0*$n_pets_installed)"
#   - insert save beam final
#   - insert SlicesToParticles before MDX SBend
#   - insert PLACET MDX bend manually (should be an RBend)
set sbend_synrad 0
set quad_synrad 0
set mult_synrad 0
SetReferenceEnergy $e0
Bpm -name "TBLSTART" -length 0
#puts "EA: [expr ${CC.KQDD0820}*$LQL]"
Drift -name "MATRIXPOINT.0" -length 0.0
Quadrupole -name "CC.QDD0820" -synrad $quad_synrad -length $LQL -strength [expr ${CC.KQDD0820}*$LQL*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CC.D0B" -length 0.25
#puts "EA: [expr ${CC.KQFD0840}*$LQL]"
Quadrupole -name "CC.QFD0840" -synrad $quad_synrad -length $LQL -strength [expr ${CC.KQFD0840}*$LQL*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CC.D0D" -length 3.231471
Bpm -name "ZEROPOS" -length 0
Drift -name "CC.D01" -length 0.2
#puts "EA: [expr ${CC.KQFL0910}*$LQLS]"
Quadrupole -name "CC.QFL0910" -synrad $quad_synrad -length $LQLS -strength [expr ${CC.KQFL0910}*$LQLS*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CC.D02" -length 0.137
Drift -name "CC.BPR0915" -length 0.126
Drift -name "CC.D02" -length 0.137
#puts "EA: [expr ${CC.KQDL0920}*$LQLS]"
Quadrupole -name "CC.QDL0920" -synrad $quad_synrad -length $LQLS -strength [expr ${CC.KQDL0920}*$LQLS*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CC.D04" -length 0.086
Bpm -name "CC.BPM0930" -length 0.126
Drift -name "CC.D04A" -length 0.178
# CORRECTOR -name "CC.DHD0940" -length 0.24
Dipole -name "CC.DHD0940" -length 0.24 -strength_x 0.0 -strength_y 0.0
Drift -name "CC.D04B" -length 0.97
Drift -name "CC.MTV0970" -length 0.25
# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2
# WARNING: original length was 0.45
#Sbend -name "CM.BHL0100" -synrad $sbend_synrad -length 0 -angle  -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
# EA: out with Sbend (macroparticles and sbend did not go well)
Drift -name "CM.BHL0100" -length 0.45
Drift -name "CB.D01" -length 1.41
#puts "EA: [expr ${CB.KQFM0110}*$LQLM]"
Quadrupole -name "CB.QFM0110" -synrad $quad_synrad -length $LQLM -strength [expr ${CB.KQFM0110}*$LQLM*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.D02" -length 0.2
#puts "EA: [expr ${CB.KQDM0120}*$LQLM]"
Quadrupole -name "CB.QDM0120" -synrad $quad_synrad -length $LQLM -strength [expr ${CB.KQDM0120}*$LQLM*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.D03" -length 0.11
# CORRECTOR -name "CB.DHD0130" -length 0.24
Dipole -name "CB.DHD0130" -length 0.24 -strength_x 0.0 -strength_y 0.0
Drift -name "CB.D03A" -length 0.96
# CORRECTOR -name "CB.DHD0140" -length 0.24
Dipole -name "CB.DHD0140" -length 0.24 -strength_x 0.0 -strength_y 0.0
Drift -name "CB.D03B" -length 0.007
Bpm -name "CB.BPM0150" -length 0.126
Drift -name "CB.D03C" -length 0.2205
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQFR0200}*$LQLX]"
#Quadrupole -name "CB.QFR0200_A" -synrad $quad_synrad -length $LQLX -strength [expr 0.5*${CB.KQFR0200}*$LQLX*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
#Quadrupole -name "CB.QFR0200_B" -synrad $quad_synrad -length $LQLX -strength [expr 0.5*${CB.KQFR0200}*$LQLX*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
puts "EAEA: [expr ${CB.PMOV0205-H}]"
Quadrupole -name "CB.QFR0200" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQFR0200}*$LQLX*$e0] -x [expr ${CB.PMOV0205-H}] -y [expr ${CB.PMOV0205-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0210" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQDR0240}*$LQLX]"
Quadrupole -name "CB.QDR0240" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQDR0240}*$LQLX*$e0] -x [expr ${CB.PMOV0245-H}] -y [expr ${CB.PMOV0245-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0250" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQFR0300}*$LQLX]"
Quadrupole -name "CB.QFR0300" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQFR0300}*$LQLX*$e0] -x [expr ${CB.PMOV0305-H}] -y [expr ${CB.PMOV0305-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0310" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQDR0340}*$LQLX]"
Quadrupole -name "CB.QDR0340" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQDR0340}*$LQLX*$e0] -x [expr ${CB.PMOV0345-H}] -y [expr ${CB.PMOV0345-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0350" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQFR0400}*$LQLX]"
Quadrupole -name "CB.QFR0400" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQFR0400}*$LQLX*$e0] -x [expr ${CB.PMOV0405-H}] -y [expr ${CB.PMOV0405-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0410" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQDR0440}*$LQLX]"
Quadrupole -name "CB.QDR0440" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQDR0440}*$LQLX*$e0] -x [expr ${CB.PMOV0445-H}] -y [expr ${CB.PMOV0445-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0450" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQFR0500}*$LQLX]"
Quadrupole -name "CB.QFR0500" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQFR0500}*$LQLX*$e0] -x [expr ${CB.PMOV0505-H}] -y [expr ${CB.PMOV0505-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0510" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQDR0540}*$LQLX]"
Quadrupole -name "CB.QDR0540" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQDR0540}*$LQLX*$e0] -x [expr ${CB.PMOV0545-H}] -y [expr ${CB.PMOV0545-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0550" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQFR0600}*$LQLX]"
Quadrupole -name "CB.QFR0600" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQFR0600}*$LQLX*$e0] -x [expr ${CB.PMOV0605-H}] -y [expr ${CB.PMOV0605-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0610" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQDR0640}*$LQLX]"
Quadrupole -name "CB.QDR0640" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQDR0640}*$LQLX*$e0] -x [expr ${CB.PMOV0645-H}] -y [expr ${CB.PMOV0645-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0650" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQFR0700}*$LQLX]"
Quadrupole -name "CB.QFR0700" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQFR0700}*$LQLX*$e0] -x [expr ${CB.PMOV0705-H}] -y [expr ${CB.PMOV0705-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0710" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQDR0740}*$LQLX]"
Quadrupole -name "CB.QDR0740" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQDR0740}*$LQLX*$e0] -x [expr ${CB.PMOV0745-H}] -y [expr ${CB.PMOV0745-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0750" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQFR0800}*$LQLX]"
Quadrupole -name "CB.QFR0800" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQFR0800}*$LQLX*$e0] -x [expr ${CB.PMOV0805-H}] -y [expr ${CB.PMOV0805-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0810" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQDR0840}*$LQLX]"
Quadrupole -name "CB.QDR0840" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQDR0840}*$LQLX*$e0] -x [expr ${CB.PMOV0845-H}] -y [expr ${CB.PMOV0845-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0850" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQFR0900}*$LQLX]"
Quadrupole -name "CB.QFR0900" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQFR0900}*$LQLX*$e0] -x [expr ${CB.PMOV0905-H}] -y [expr ${CB.PMOV0905-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0910" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
Drift -name "CB.DQUAD" -length 0.032
#puts "EA: [expr ${CB.KQDR0940}*$LQLX]"
Quadrupole -name "CB.QDR0940" -synrad $quad_synrad -length $LQLX -strength [expr ${CB.KQDR0940}*$LQLX*$e0] -x [expr ${CB.PMOV0945-H}] -y [expr ${CB.PMOV0945-V}] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.DQUADBPS" -length 0.054
Bpm -name "CB.BPS0950" -length 0.126
Drift -name "CB.DBPSTANK" -length 0.022
Drift -name "CB.DTANKPETS" -length 0.07136
source "$deceleratorrootpath/dec_scripts/insert_TBL_PETS.tcl"
#source "$deceleratorrootpath/dec_scripts/calc_betas_all_particles.m"
#TclCall -name "tclcalcbetasall" -script "my_calc_betas_all_particles" 
#SlicesToParticles -seed 1504
#my_save_tbl_beam

if { 1 } {

Drift -name "CB.D05A" -length 0.53
Drift -name "CB.D05" -length 0.1775
#puts "EA: [expr ${CB.KQFN1010}*$LQLL]"
Drift -name "MATRIXPOINT.3" -length 0.0
Quadrupole -name "CB.QFP1010" -synrad $quad_synrad -length $LQLL -strength [expr ${CB.KQFN1010} * $LQLL * $e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.D06" -length 0.16
#puts "EA: [expr ${CB.KQDN1020}*$LQLL]"
Quadrupole -name "CB.QDP1020" -synrad $quad_synrad -length $LQLL -strength [expr ${CB.KQDN1020} * $LQLL * $e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "CB.D07A" -length 0.167
Bpm -name "CB.BPM1030" -length 0.126
Drift -name "CB.D07" -length 0.017
# CORRECTOR -name "CB.DHD1040" -length 0.24
Dipole -name "CB.DHD1040" -length 0.24 -strength_x 0.0 -strength_y 0.0
Drift -name "CB.D07A" -length 0.4705
Drift -name "CB.MTV1070_a" -length [expr 0.273/2]
Drift -name "MATRIXPOINT.4" -length 0.0
Drift -name "CB.MTV1070_b" -length [expr 0.273/2]
Drift -name "CB.D08" -length 0.3336
# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2
# WARNING: original length was 0.4796085062
#Sbend -name "CB.BHB1100" -synrad $sbend_synrad -length 0 -angle  -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
#
# Manually insert spectrometer bend
#   - enable sbend also converts beam to particles
set enable_mdx_bend 1
set L_B [expr 0.479*1e-0]
 # 10 degrees=0.1745 rad
set alpha_0 [expr 0.1745*1e-0]
set s_B [expr ($L_B*$alpha_0) / (2*sin($alpha_0/2))]
puts "s_B: $s_B"
# we make an RBend : 0 entry angle, -alpha_0 exit angle
set alpha_E [expr 0.5*$alpha_0*2.0]
if { $enable_mdx_bend } { 
    SlicesToParticles -seed 1504
    set e_max [expr $e0*(1+3*$sigma_E_upon_E)]
    set e_min [expr $e0*(1-3*$sigma_E_upon_E)-$de0*$n_pets_installed*1.0]
    set ideal_bend_momentum [expr 1e-0*2*($e_max*$e_min)/($e_max+$e_min)]
    #set ideal_bend_momentum [expr 1e-0*$e_min]
    puts "ideal_bend_momentum: $ideal_bend_momentum"
    # Dipole spectormeter, half-half skewed
    Sbend -synrad 0 -length $s_B -angle [expr -$alpha_0] -e0 [expr $ideal_bend_momentum] -E1 [expr -$alpha_E*0.5] -E2 [expr -$alpha_E*0.5] -six_dim 1  
} else {
    Drift -name "CB.BHB1100" -length 0.479
}
#
# End manual insert
#
Drift -name SBEND_TO_MTV -length 0.95038
Drift -name "CBS.MTV0300_a" -length [expr 0.217/2]
Drift -name "MATRIXPOINT.5" -length 0.0
Drift -name "CBS.MTV0300_b" -length [expr 0.217/2]
Drift -name MTV_TO_COLLIMATOR -length 0.600
Drift -name COLLIMATOR_TO_DUMP -length 0.160
Drift -name "MATRIXPOINT.6" -length 0.0
#
}
#
# END PART IMPORTED FROM MADX
#

# Final envelope observers
Bpm -name "TBLEND" -length 0
Quadrupole -strength 0 -length 0
Quadrupole -strength 0 -length 0

# Final beam save slices
#    if { $savebeam_active } {
#	TclCall -name "tcl dump" -script {BeamSaveAll -file beam_final.dat}
#    }
# save particle beam
if { $savebeam_active } {
    my_save_tbl_beam
}

