%
% matching script (for TBL)
%
% input data
% (example we want to match betas at the end of line)
    %global beta_x_req = 1e+0;
    %global beta_y_req = 1e+0;
    global beta_x_req = 4;
    global beta_y_req = 6;
    n_endquads_to_use = 4;
    n_end_false_quads = 2; % two 'observer quads' added at TBL end

%
% matching function body
%
function merit = def_matching(beamline)
    global beta_x_req beta_y_req Q_match;
     n_E = length(placet_get_name_number_list(beamline, "*"));
     beta_x_initial = str2num(Tcl_GetVar("beta_x_initial"));
     beta_y_initial = str2num(Tcl_GetVar("beta_y_initial"));
     alpha_x_initial = str2num(Tcl_GetVar("alpha_x_initial"));
     alpha_y_initial = str2num(Tcl_GetVar("alpha_y_initial"));
    [s, beta_x, beta_y, alpha_x, alpha_y, mu_x, mu_y] = placet_evolve_beta_function( beta_x_initial, alpha_x_initial, beta_y_initial, alpha_y_initial, 0, n_E-1);
    %Qstrength_match = placet_element_get_attribute(beamline, Q_match, "strength")    
    %beta_x(end)
    %beta_y(end)
    merit = sum((1.0 - [ beta_x(end)/beta_x_req, beta_y(end)/beta_y_req]).**2);
  endfunction
    

    mysimname = Tcl_GetVar("mysimname");
    Q = placet_get_number_list(mysimname, "quadrupole");
    global Q_match = Q(end-n_end_false_quads-n_endquads_to_use+1:end-n_end_false_quads); # use last (real) quads for matching

      
      %
% Core script
%
      Corrector = Q_match;
      Leverage = [  "strength" ];
      for(n=1:(length(Q_match)-1)),
        Leverage = [Leverage;  "strength"];
      end%for
         %Leverage = "strength";
         %Leverage = [  "length"; "length" ];
    %Constraint = [ -100, 100; -100, 100];
      Constraint = [ -0, 0; 0, 0; 0, 0; 0, 0];

          Qstrength_match = placet_element_get_attribute(mysimname, Q_match, "strength")    

  [optimum, merit] = placet_optimize_constraint(mysimname, "def_matching", Corrector, Leverage, Constraint)
    Qstrength_match = placet_element_get_attribute(mysimname, Q_match, "strength")    
