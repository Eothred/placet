clear all;

N=1492;
E0 = 2.37249551111;
S_grad = 0.90% E spread
%N_P = round(N/4);
N_T = 50; % max: ~ 300, min: 4

do_plot = 1; % plot if variable exists
%for N_T=1:500,
  run calc_quad_gradients.m
%  pause;
%end% for
%N=100;
%E0 = 1;



%
% some plotting routines
%
if(0)
  load -mat taper_plots.mat;
  N_T_max = 500;
  % GENERIC COST
  PS_weight = 10;
  plot(N_P_array*PS_weight+(1:N_T_max));
  xlabel('Number of quadrupole types [-]'); ylabel('Generic cost [a.u.]');
  axis([0 N_T_max 0 1000]);
  grid on;
  % NUMBER OF PERM TYPES
  PS_weight = 10;
  plot((1:N_T).*N_P_array);
  xlabel('Number of quadrupole types [-]'); ylabel('{N_T}{N_P} [-]');
  axis([0 N_T_max 0 1000]);
  grid on;
  % max(beat)
  plot(max_dev*100)
  xlabel('Number of quadrupole types [-]'); ylabel('Max |{/Symbol D}K|  [%]');
  axis([0 N_T_max 0 1000]);
  grid on;
  % k's
end% if