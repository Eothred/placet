function [quads, dipols, hcors, vcors, mons] = tl2()

prefix = 'CC';
  
amoni = 0;
adipo = 1;
aquad = 2;
ahcorr = 5;
avcorr = 6;


n=1;


device(n).name='BHH0200';      device(n).type=adipo;  device(n).madname='IBHH0200'; device(n).spos=-1.0;  device(n).isfesa  = -1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BVA0300-S';    device(n).type=adipo;  device(n).madname='IBVA0300'; device(n).spos=-1.0;  device(n).isfesa  = -1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BVB0400';      device(n).type=adipo;  device(n).madname='IBVB0400'; device(n).spos=-1.0;  device(n).isfesa  = -1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BHG0500-S';    device(n).type=adipo;  device(n).madname='IBHG0500'; device(n).spos=-1.0;  device(n).isfesa  = -1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BHH0600-S';    device(n).type=adipo;  device(n).madname='IBHH0600'; device(n).spos=-1.0;  device(n).isfesa  = -1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;

device(n).name='BPI0135'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPI0185'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPM0235'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPM0275'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPM0365'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPM0475'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPI0535'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPI0645'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPI0685'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPI0775'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPM0845'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='BPM0930'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;

device(n).name='QFG0140';      device(n).type=aquad;  device(n).madname='CC.IQFG0140'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDG0160';      device(n).type=aquad;  device(n).madname='CC.IQDG0160'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFH0180';      device(n).type=aquad;  device(n).madname='CC.IQFH0180'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFH0210';      device(n).type=aquad;  device(n).madname='CC.IQFH0210'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDH0220';      device(n).type=aquad;  device(n).madname='CC.IQDH0220'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFH0230';      device(n).type=aquad;  device(n).madname='CC.IQFH0230'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFL0270';      device(n).type=aquad;  device(n).madname='CC.IQFL0270'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDL0280';      device(n).type=aquad;  device(n).madname='CC.IQDL0280'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDH0330';      device(n).type=aquad;  device(n).madname='CC.IQDH0330'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFH0350';      device(n).type=aquad;  device(n).madname='CC.IQFH0350'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDH0370';      device(n).type=aquad;  device(n).madname='CC.IQDH0370'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDL0430';      device(n).type=aquad;  device(n).madname='CC.IQDL0430'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFL0450';      device(n).type=aquad;  device(n).madname='CC.IQFL0450'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDL0470';      device(n).type=aquad;  device(n).madname='CC.IQDL0470'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='FDL0530-S';    device(n).type=aquad;  device(n).madname='CC.IQFL0530'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDL0550';      device(n).type=aquad;  device(n).madname='CC.IQDL0550'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFL0570';      device(n).type=aquad;  device(n).madname='CC.IQFL0570'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFL0620-S';    device(n).type=aquad;  device(n).madname='CC.IQFL0620'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDL0650';      device(n).type=aquad;  device(n).madname='CC.IQDL0650'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFL0730';      device(n).type=aquad;  device(n).madname='CC.IQFL0730'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDL0750';      device(n).type=aquad;  device(n).madname='CC.IQDL0750'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDD0820';      device(n).type=aquad;  device(n).madname='CC.IQDD0820'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFD0840';      device(n).type=aquad;  device(n).madname='CC.IQFD0840'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QFL0910';      device(n).type=aquad;  device(n).madname='CC.IQFL0910'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;
device(n).name='QDL0920';      device(n).type=aquad;  device(n).madname='CC.IQDL0920'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;


device(n).name='DHD0125';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0125'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0175';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0175'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0225';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0225'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0265';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0265'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0345';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0345'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0435';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0435'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0525';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0525'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0615';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0615'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0655';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0655'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0765';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0765'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHF0855';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0855'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DHD0940';      device(n).type=ahcorr;  device(n).madname='CC.IDHD0940'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 

device(n).name='DVD0125';      device(n).type=avcorr;  device(n).madname='CC.IDVD0125'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0175';      device(n).type=avcorr;  device(n).madname='CC.IDVD0175'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0225';      device(n).type=avcorr;  device(n).madname='CC.IDVD0225'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0265';      device(n).type=avcorr;  device(n).madname='CC.IDVD0265'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0345';      device(n).type=avcorr;  device(n).madname='CC.IDVD0345'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0435';      device(n).type=avcorr;  device(n).madname='CC.IDVD0435'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0525';      device(n).type=avcorr;  device(n).madname='CC.IDVD0525'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0615';      device(n).type=avcorr;  device(n).madname='CC.IDVD0615'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0655';      device(n).type=avcorr;  device(n).madname='CC.IDVD0655'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0765';      device(n).type=avcorr;  device(n).madname='CC.IDVD0765'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 
device(n).name='DVF0855';      device(n).type=avcorr;  device(n).madname='CC.IDVD0855'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1;  
device(n).name='DVD0940';      device(n).type=avcorr;  device(n).madname='CC.IDVD0940'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CC'; n=n+1; 

nd = length(device);

ndipols = 0;
nquads  = 0;
nhcorrs  = 0;
nvcorrs  = 0;
nmons   = 0;

for i=1:nd
  switch device(i).type
    case adipo
      ndipols = ndipols + 1;
      dipols(ndipols).name = device(i).name;
      dipols(ndipols).isfesa = device(i).isfesa;
      dipols(ndipols).spos = device(i).spos;
      dipols(ndipols).faulty = device(i).faulty;
      dipols(ndipols).madname = device(i).madname;
      dipols(ndipols).prefix = device(i).prefix;
      
    case aquad
      nquads =  nquads + 1;
      quads(nquads).name = device(i).name;
      quads(nquads).isfesa = device(i).isfesa;
      quads(nquads).spos = device(i).spos;
      quads(nquads).faulty = device(i).faulty;
      quads(nquads).madname = device(i).madname;
      quads(nquads).prefix = device(i).prefix;
      
    case ahcorr
      nhcorrs =  nhcorrs + 1;
      hcors(nhcorrs).name = device(i).name;
      hcors(nhcorrs).isfesa = device(i).isfesa;
      hcors(nhcorrs).spos = device(i).spos;
      hcors(nhcorrs).faulty = device(i).faulty;
      hcors(nhcorrs).madname = device(i).madname;
      hcors(nhcorrs).prefix = device(i).prefix;

    case avcorr
      nvcorrs =  nvcorrs + 1;
      vcors(nvcorrs).name = device(i).name;
      vcors(nvcorrs).isfesa = device(i).isfesa;
      vcors(nvcorrs).spos = device(i).spos;
      vcors(nvcorrs).faulty = device(i).faulty;
      vcors(nvcorrs).madname = device(i).madname;
      vcors(nvcorrs).prefix = device(i).prefix;
      
     case amoni
       nmons = nmons + 1;
       mons(nmons).name  = device(i).name;
       mons(nmons).isfesa = device(i).isfesa;
       mons(nmons).spos = device(i).spos;
       mons(nmons).faulty = device(i).faulty;
       mons(nmons).madname = device(i).madname;
       mons(nmons).prefix = device(i).prefix;
       
   end
end  

%amoni = mons;
