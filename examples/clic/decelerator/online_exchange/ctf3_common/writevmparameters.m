function writevmparameters(params)

filename = ['virtual.' params.myname];
fh = fopen(filename, "wt");
    n_params = length(params.name);
    for i=1:n_params
      fprintf(fh, '%s   %f\n', params.name(i), params.value(i) ); 
    end% for
fclose(fh);
