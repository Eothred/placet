function [quads, dipols, hcors, vcors, hmovers, vmovers, mons] = tbl()

% 19jul2010,EA : added madnames (for placet online-model)
% 11aug2010,EA : added TBL quad movers

%prefix = 'CB';
  
amoni = 0;
adipo = 1;
aquad = 2;
ahcorr = 5;
avcorr = 6;
ahmov = 65;
avmov = 66;


n=1;

% Dipoles
device(n).name='BHB1100';      device(n).type=adipo;  device(n).madname='CB.IBHB1100'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;

% BPMs
device(n).name='BPM0150'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0210'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0250'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0310'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0350'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0410'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0450'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0510'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0550'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0610'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0650'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0710'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0750'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0810'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0850'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0910'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPS0950'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='BPM1030'; device(n).type=amoni;  device(n).madname=' '; device(n).spos=-1.0;  device(n).isfesa  = 0; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;

% Quads
device(n).name='QFM0110';      device(n).type=aquad;  device(n).madname='CB.IQFM0110'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDM0120';      device(n).type=aquad;  device(n).madname='CB.IQDM0120'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFR0200';      device(n).type=aquad;  device(n).madname='CB.IQFR0200'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDR0240';      device(n).type=aquad;  device(n).madname='CB.IQDR0240'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFR0300';      device(n).type=aquad;  device(n).madname='CB.IQFR0300'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDR0340';      device(n).type=aquad;  device(n).madname='CB.IQDR0340'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFR0400';      device(n).type=aquad;  device(n).madname='CB.IQFR0400'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDR0440';      device(n).type=aquad;  device(n).madname='CB.IQDR0440'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFR0500';      device(n).type=aquad;  device(n).madname='CB.IQFR0500'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDR0540';      device(n).type=aquad;  device(n).madname='CB.IQDR0540'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFR0600';      device(n).type=aquad;  device(n).madname='CB.IQFR0600'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDR0640';      device(n).type=aquad;  device(n).madname='CB.IQDR0640'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFR0700';      device(n).type=aquad;  device(n).madname='CB.IQFR0700'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDR0740';      device(n).type=aquad;  device(n).madname='CB.IQDR0740'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFR0800';      device(n).type=aquad;  device(n).madname='CB.IQFR0800'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDR0840';      device(n).type=aquad;  device(n).madname='CB.IQDR0840'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFR0900';      device(n).type=aquad;  device(n).madname='CB.IQFR0900'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDR0940';      device(n).type=aquad;  device(n).madname='CB.IQDR0940'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QFN1010';      device(n).type=aquad;  device(n).madname='CB.IQFN1010'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='QDN1020';      device(n).type=aquad;  device(n).madname='CB.IQDN1020'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;


% Correctors - horizontal
device(n).name='DHD0130';      device(n).type=ahcorr;  device(n).madname='CB.IDHD0130'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='DHD0140';      device(n).type=ahcorr;  device(n).madname='CB.IDHD0140'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='DHD1040';      device(n).type=ahcorr;  device(n).madname='CB.IDHD1040'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;

% Correctors - vertical
device(n).name='DVD0130';      device(n).type=avcorr;  device(n).madname='CB.IDVD0130'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='DVD0140';      device(n).type=avcorr;  device(n).madname='CB.IDVD0140'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='DVD1040';      device(n).type=avcorr;  device(n).madname='CB.IDVD1040'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;

% movers
device(n).name='MOV0205-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0205-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0245-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0245-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0305-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0305-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0345-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0345-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0405-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0405-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0445-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0445-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0505-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0505-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0545-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0545-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0605-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0605-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0645-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0645-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0705-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0705-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0745-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0745-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0805-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0805-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0845-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0845-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0905-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0905-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0945-H';      device(n).type=ahmov;  device(n).madname='CB.IMOV0945-H'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;

device(n).name='MOV0205-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0205-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0245-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0245-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0305-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0305-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0345-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0345-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0405-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0405-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0445-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0445-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0505-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0505-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0545-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0545-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0605-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0605-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0645-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0645-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0705-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0705-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0745-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0745-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0805-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0805-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0845-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0845-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0905-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0905-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;
device(n).name='MOV0945-V';      device(n).type=avmov;  device(n).madname='CB.IMOV0945-V'; device(n).spos=-1.0;  device(n).isfesa  = 1; device(n).faulty = 0; device(n).prefix = 'CB'; n=n+1;

nd = length(device);

ndipols = 0;
nquads  = 0;
nhcorrs  = 0;
nvcorrs  = 0;
nmons   = 0;
nhmovers = 0;
nvmovers = 0;

for i=1:nd
  switch device(i).type
    case adipo
      ndipols = ndipols + 1;
      dipols(ndipols).name = device(i).name;
      dipols(ndipols).isfesa = device(i).isfesa;
      dipols(ndipols).spos = device(i).spos;
      dipols(ndipols).faulty = device(i).faulty;
      dipols(ndipols).madname = device(i).madname;
      dipols(ndipols).prefix = device(i).prefix;
      
    case aquad
      nquads =  nquads + 1;
      quads(nquads).name = device(i).name;
      quads(nquads).isfesa = device(i).isfesa;
      quads(nquads).spos = device(i).spos;
      quads(nquads).faulty = device(i).faulty;
      quads(nquads).madname = device(i).madname;
      quads(nquads).prefix = device(i).prefix;
      
    case ahcorr
      nhcorrs =  nhcorrs + 1;
      hcors(nhcorrs).name = device(i).name;
      hcors(nhcorrs).isfesa = device(i).isfesa;
      hcors(nhcorrs).spos = device(i).spos;
      hcors(nhcorrs).faulty = device(i).faulty;
      hcors(nhcorrs).madname = device(i).madname;
      hcors(nhcorrs).prefix = device(i).prefix;

    case avcorr
      nvcorrs =  nvcorrs + 1;
      vcors(nvcorrs).name = device(i).name;
      vcors(nvcorrs).isfesa = device(i).isfesa;
      vcors(nvcorrs).spos = device(i).spos;
      vcors(nvcorrs).faulty = device(i).faulty;
      vcors(nvcorrs).madname = device(i).madname;
      vcors(nvcorrs).prefix = device(i).prefix;
      
    case ahmov
      nhmovers =  nhmovers + 1;
      hmovers(nhmovers).name = device(i).name;
      hmovers(nhmovers).isfesa = device(i).isfesa;
      hmovers(nhmovers).spos = device(i).spos;
      hmovers(nhmovers).faulty = device(i).faulty;
      hmovers(nhmovers).madname = device(i).madname;
      hmovers(nhmovers).prefix = device(i).prefix;
   
    case avmov
      nvmovers =  nvmovers + 1;
      vmovers(nvmovers).name = device(i).name;
      vmovers(nvmovers).isfesa = device(i).isfesa;
      vmovers(nvmovers).spos = device(i).spos;
      vmovers(nvmovers).faulty = device(i).faulty;
      vmovers(nvmovers).madname = device(i).madname;
      vmovers(nvmovers).prefix = device(i).prefix;
   
   case amoni
       nmons = nmons + 1;
       mons(nmons).name  = device(i).name;
       mons(nmons).isfesa = device(i).isfesa;
       mons(nmons).spos = device(i).spos;
       mons(nmons).faulty = device(i).faulty;
       mons(nmons).madname = device(i).madname;
       mons(nmons).prefix = device(i).prefix;
       
   end
end  