# by Miriam Fitterer

#load scripts
set script_dir .

# define SI constants
set SI_c 2.998e8
set SI_e 1.602e-19
#number of bunches
set n_bunches 800
#number of slices
set n_slices 51
#number of macroparticles
set n_macros 1
#distance between bunches
set d_bunch 0.025
#RMS bunch length
#set sigma_bunch 400
set sigma_bunch 1000
#gauss cut of the bunch distribution = gauss_cut * sigma
set gauss_cut 3
set par 5000
#number of particles per bunch
set charge 1.4575e10
#beam energy
set e0 0.150
#normalized emittance
set emitt_x 1500.0
set emitt_y 1500.0


set slice_dist [GaussList -n_slices $n_slices -min [expr -$gauss_cut] -max $gauss_cut -sigma $sigma_bunch -charge 1]
set energy_dist [GaussList -n_slices $n_macros -min -3 -max 3 -sigma [expr $e0*0.01] -charge 1]

set cavitylength 0.8

# Define the longitudinal mode 
#set beta_l 0.74
set beta_l 0.4529
#set RQ 575
set RQ [expr 2294.7/2]
#set lambda_l 0.01
set lambda_l 0.025

# Define the transverse mode
#                 lambda_t [m]     w_t [V/(pC*m^2)]     Q [-]         beta_t [-]
lappend mode1_t " 0.010925           45.0e0             300.0           1.0e-10"
lappend mode1_t " 0.010688           19.0e0             180.0           1.0e-10"
lappend mode1_t " 0.009109           17.0e0             290.0           1.0e-10"
lappend mode1_t " 0.007663          200.0e0             085.0           1.0e-10"
lappend mode1_t " 0.007167           30.0e0             120.0           1.0e-10"
lappend mode1_t " 0.006129           15.0e0             380.0           1.0e-10"
lappend mode1_t " 0.029979          850.0e0             003.7           1.0e-10"
lappend mode1_t " 0.022373         4820.0e0             003.8           1.0e-10"
lappend mode1_t " 0.019391         2630.0e0             006.2           1.0e-10"

# Divide the cavity into n steps ( y, y' is calculated for every steps, several steps needed for large y')
set n_steps 1

#this aperture can be ignored - it is just used for some higher order field corrections
set cavityaperture 11.5e3

CavityDefine -name DecCavity \
    -length $cavitylength \
    -transverse $mode1_t \
    -beta_group_l $beta_l \
    -r_over_q $RQ \
    -lambda_longitudinal $lambda_l \
    -rf_kick 0 \
    -rf_long 0 \
    -rf_a0 [expr $cavityaperture] \
    -rf_order 4 \
    -rf_size 0 \
    -steps $n_steps

puts "\nDecelerating cavity created with the following paramaters:"
puts "    length \[m\]: $cavitylength"
puts "    steps \[\#\]: $n_steps"
puts "    rf order (order of field non-uniformity) \[\\#\]: 4"
puts "    rf size (amplitude of the different orders \[\\#\]: 0"
puts "    Non-uni. RF kick on? (non-uniformity of long.field accounted for)  \[0\\1\]: 0"
puts "    Non-uni. RF long. effect on? (non-uniformity of long.field accounted for)  \[0\\1\]: 0"
puts "    half-aperture of structure \[um\]: [expr $cavityaperture]"
puts "    longitudinal wakefield mode:"
puts "      - lambda_l \[m\]: $lambda_l" 
puts "      - group_velocity, beta_l \[-\]: $beta_l"
puts "      - R' / Q _l \[circuit Ohms per meter \(Ohm/m\)\]: $RQ"
puts "    transverse wakefield mode 1:"
puts "      - lambda_t \[m\]: [lindex [lindex $mode1_t 0] 0]"
puts "      - group_velocity, beta_t \[-\]: [lindex [lindex $mode1_t 0] 3]"
puts "      - amplitude_t \[V\\\(m\^2*pC\)\]: [lindex [lindex $mode1_t 0] 1]"
puts "      - Q_factor_t \[-\]: [lindex [lindex $mode1_t 0] 2]"
puts ""

# Calculate cavity output power
set pow [Power -beta $beta_l -length $cavitylength \
	 -lambda $lambda_l \
	 -r_over_q $RQ \
	 -distance $d_bunch \
	 -bunchlength $sigma_bunch \
	 -charge $charge  \
	 -xmin 2.0 -xmax [expr 4.0]]

# Calculate the maximum deceleration per cavity [GeV] of a macroparticle, in order to reduce focusing strength as the beam is moving through the lattice
set de0 [expr [MaxField -beta $beta_l -length $cavitylength \
		     -lambda $lambda_l \
		     -r_over_q $RQ \
		     -distance $d_bunch \
		     -bunchlength $sigma_bunch \
		     -charge $charge  ]]


puts "Resulting Cavity Output power:"
puts "    Output power per cavity \[\MW\]: [lindex $pow 0]"
puts "    maximal de0 per cavity \[\GeV\]: $de0"
puts ""


# Define the lattice

# Define geometric parameters (one unit consists of one cavity, one bpm and one qpole. The qpole strength is reduced as the beam is decelerated)
set quadrupolelength 0.15
set bpmlength 0.15
set coupler_drift 0.07
set qpole_drift 0.115
set girderlength [expr $cavitylength + $quadrupolelength + $bpmlength + 2*$qpole_drift + $coupler_drift ]

# Normalized quadrupole strength; gives phase advance of ~ 90 deg 
set k_qpole 7

#use circular aperture shape and the same aperture for all elements
#aperture [m]
set apertureshape "circular"
set aperture 11.5e-3

puts "   Producing lattice..."
puts "   Girderlength (unitlength): $girderlength"
puts "   Qpolelength: $quadrupolelength"
puts "   Qpole driftlength: $qpole_drift"
puts "   Qpolestrength, normalized: $k_qpole \[m^-2]"
puts "   Qpolaperture: $aperture \[m]"
puts "   Apertureshape: $apertureshape "
puts "   BPMlength: $bpmlength"
puts "   Cavity length: $cavitylength"
puts "   Cavity coupler length: $coupler_drift"
puts "   Dec per cavity (1 per quadrupole), de0: $de0 \[GeV\]"
puts ""

set bpm_count 0
set total_length 0
set deccavity_count 0
set count 0
# This routine will be called every time the beam goes through the middle of a quadrupole (see lattice definition below)
# Put your own routine here if desired (e.g. a beam dump)
proc save_beam {bpm_count} {
    # the bpm_count variable is updated very time the beam hits a new quadrupole center
    # fill in action to be performed in the middle of each quadrupole
    #puts $bpm_count
    BeamSaveAll -file beam_bet_$bpm_count.dat
}

proc put_bpm {} {
    global bpm_count
    TclCall -script "save_beam $bpm_count"
    incr bpm_count
}

#calculate total length of tbl
proc add_length {length} {
global total_length
set total_length [expr {$total_length + $length}]
} 

#print out emittance and sigma matrix, count is justed needed to see how often the procedure is called
#if TwissMain and BeamEnergyPlot are uncommented, one gets a wrong output from print_beam_param while one goes through the lattice by calling the routines
proc print_beam_param {} {
global count
puts "print out beam parameters:"
puts "count: $count"
incr count

} 
#
# Define the lattice: 8 identical FODO cells, starting in the middle of an F qpole, and with focusing strength adjusted so that the lowest energy particle has constant phase-advance
#

set e $e0
set first 1

for {set i 0} {$i < 8} {incr i } {
    Girder
    if {$first == 0} {
	# Bpm element: acts as drift space during tracking
	Bpm -name BPm -length $bpmlength
	Drift -name DRIFT1 -length $qpole_drift
	# F quadrupole
	Quadrupole -name Qd1 -length [expr 0.5*$quadrupolelength] \
 		   -strength [expr -0.5 * $k_qpole * $quadrupolelength * $e] -aperture_x $aperture -aperture_y $aperture -aperture_shape $apertureshape
	#put_bpm
	Quadrupole -name Qd2 -length [expr 0.5*$quadrupolelength] \
 		   -strength [expr -0.5 * $k_qpole * $quadrupolelength * $e] -aperture_x $aperture -aperture_y $aperture -aperture_shape $apertureshape
	Drift -name DRIFT2 -length $qpole_drift
#	puts "...loop on drive beam elements   $i"


    } else {
	# FIRST PASS (to start at a symmetry point in lattice)
	# Save initial beam
	#TclCall -script {BeamDump -file beam_initial.dat}
	#TclCall -script "print_beam_param"
	# F quadrupole
	#put_bpm
	Quadrupole -name QDFIRST -length [expr 0.5*$quadrupolelength] \
	           -strength [expr -0.5 * $k_qpole * $quadrupolelength * $e] -aperture_x $aperture -aperture_y $aperture -aperture_shape $apertureshape
	Drift -name DTFIRST -length $qpole_drift
	set first 0
#	puts "...loop on drive beam elements else loop   $i"
	
    }

    # PETS
    DecCavity -name DCAVITY1 -length $cavitylength
    incr deccavity_count
#    Drift -length $cavitylength
    Drift -name DRIFT1 -length $coupler_drift
#    puts "...loop on drive beam elements,outside loop   $i"

    # Focusing adjustment
    set e [expr $e - $de0]

    # D quadrupole
    Bpm -name BPM1 -length $bpmlength
    Drift -name DRIFT2 -length $qpole_drift
    Quadrupole -name QD1 -length [expr 0.5*$quadrupolelength] \
 	       -strength [expr 0.5 * $k_qpole * $quadrupolelength * $e] -aperture_x $aperture -aperture_y $aperture -aperture_shape $apertureshape
    #put_bpm
    Quadrupole -name QD2 -length [expr 0.5*$quadrupolelength] \
 	       -strength [expr 0.5 * $k_qpole * $quadrupolelength * $e] -aperture_x $aperture -aperture_y $aperture -aperture_shape $apertureshape
    Drift -name DRIFT3 -length $qpole_drift

#    PETS
    DecCavity -name DCAVITY2 -length $cavitylength
    incr deccavity_count
#    Drift -length $cavitylength
    Drift -name DRIFT4 -length $coupler_drift
    # Focusing adjustment
    set e [expr $e - $de0]

}

#SlicesToParticles

Girder
Bpm -name BPMX -length $bpmlength
Drift -name DRIFTX -length $qpole_drift
# F quadrupole
Quadrupole -name QDX -length [expr 0.5*$quadrupolelength] \
	-strength [expr -0.5 * $k_qpole * $quadrupolelength * $e] -aperture_x $aperture -aperture_y $aperture -aperture_shape $apertureshape
#put_bpm
# Final beam save, be careful: if BeamEnergyPlot and TwissPlot should be uncommented otherwise placet goes through the lattice several times
#TclCall -script {BeamSaveAll -file beam_final.dat}
#TclCall -script "print_beam_param"
# Fix beamline
#SlicesToParticles
BeamlineSet -name tbl
puts "...BeamLine \'tbl' created OK."
puts "Information about the beamline"
array set tbl_info [BeamlineInfo]
#quick printout of the array tbl_info
#parray tbl_info

puts "total length: $tbl_info(length)"
puts "total number of elements: $tbl_info(n_element)"
puts "number of quadrupoles: $tbl_info(n_quadrupole)"
puts "number of BPMs: $tbl_info(n_bpm)"
puts "number of cavities: $tbl_info(n_cavity)"
puts "number of drifts: $tbl_info(n_drift)"
puts "number of decelerating cavities: $deccavity_count"
puts "bpm count: $bpm_count"
puts ""


# Match initial Twiss parameters to initial quadrupoles
array set twiss_match [MatchFodo -l1 $quadrupolelength \
		           -l2 $quadrupolelength \
		           -K1 [expr -$k_qpole*$quadrupolelength] \
	                   -K2 [expr $k_qpole*$quadrupolelength] \
	                   -L [expr $girderlength]]

puts "Initial Twiss parameters:"
puts "  beta_x: $twiss_match(beta_x)"
puts "  beta_y: $twiss_match(beta_y)"
puts "  mu_x: [expr 180.0*$twiss_match(mu_x)/acos(-1.0)]"
puts "  mu_y: [expr 180.0*$twiss_match(mu_y)/acos(-1.0)]"
puts ""


# Create drive beam
DriveBeam beam1 -bunches $n_bunches \
    -macroparticles $n_macros \
    -e0 $e0 \
    -particles 5000 \
    -slice_list $slice_dist \
    -energy_distribution $energy_dist \
    -charge [expr $charge] \
    -ramp 0 \
    -distance $d_bunch \
    -alpha_y $twiss_match(alpha_y) \
    -beta_y $twiss_match(beta_y) \
    -alpha_x $twiss_match(alpha_x) \
    -beta_x $twiss_match(beta_x) \
    -emitt_x $emitt_x \
    -emitt_y $emitt_y 

puts "\nDriveBeam \'tbl' created with the following paramaters:"
puts "    bunches \[\#\]: $n_bunches"
puts "    slices per bunch \[\#\]: $n_slices"
puts "    bunchlength, sigma \[\um\]: $sigma_bunch"
puts "    macroparticles per slice \[\#\]: $n_macros"
puts "      - transverse energy distribution \[ {energy, weight} \]: $energy_dist"
puts "    particles per bunch (charge) \[\#\]: $charge"
puts "      -> charge per bunch \[C\]: [expr $charge*$SI_e]"
puts "    distance between bunches \[m\]: $d_bunch"
puts "      -> resulting f \[GHz\]: [expr (($SI_c / $d_bunch)*1e-9)]"
puts "      -> resulting I \[A\]: [expr ($charge*$SI_e*$SI_c/$d_bunch) ]"
puts "    initial nominal energy \[GeV\]: $e0"
puts "    initial emittance x \[um\]: [expr $emitt_x*0.1]"
puts "    initial emittance y \[um\]: [expr $emitt_y*0.1]"
puts ""


#
# Part 4
#
# Perform the tracking
#


# Define beam initial offset values as beam rms size ( based on initial emittance and beta )
set sigma_yp [expr sqrt($emitt_y*1e-7/$twiss_match(beta_y)*0.511e-3/$e0)*1e6]
set sigma_y [expr sqrt($emitt_y*1e-7*$twiss_match(beta_y)*0.511e-3/$e0)*1e6]


set sigma_x [expr sqrt($emitt_x*1e-7*$twiss_match(beta_x)*0.511e-3/$e0)*1e6]
set sigma_xp [expr sqrt($emitt_x*1e-7/$twiss_match(beta_x)*0.511e-3/$e0)*1e6]
puts "Offset (sigma_x)= $sigma_x, (sigma_xp)= $sigma_xp"


FirstOrder 0
BeamlineUse -name tbl

# Set static offset for initial beam [um]
BeamSetToOffset -beam beam1 -y 0. -angle_y 0. -x 0 -angle_x 0
#BeamSetToOffset -beam beam1 -y $sigma_y -angle_y $sigma_yp -x $sigma_x -angle_x $sigma_xp
#BeamSetToOffset -beam beam1 -y $sigma_y  

# Do tracking (TestNoCorrection: standard tracking, w/o any automatic alignment procedures)
#SurveyErrorSet -cavity_y 200 \
#               -quadrupole_y 20

#TestNoCorrection -beam beam1 -emitt_file emitt.dat -survey Clic
#TestNoCorrection -beam beam1 -emitt_file emitt.dat -survey Zero

#print out the twissparameters at the end of the lattice
#TwissMain -file twiss_main.dat

#save the twissparameters along the beamline in twiss.dat
#TwissPlot -beam beam1 -file twiss.dat
#TwissPlotStep -beam beam1 -file twiss.dat -step 0.2


#print the Beamenergy along the beamline
#BeamEnergyPlot -beam beam1 -file beam_energy.dat

#exit


set mysimname tbl

# htgen parts starts from here
#

set extension output


# Number of elements in the lattice
set nelements $tbl_info(n_element)
# Vacuum structure
for {set i 0} { $i<$nelements } {incr i} {

Vacuum -nel $nelements -iel $i -temperature 300 -thetamin 329 -kmin 0.01 -gas {
    { 7 14 50 }
}
# if X0=0 the particles are lost, if they hit the beampipe
# define X0!=0 to simulate multiple scattering for expamle in spoilers
Material -X0 0. -nel $nelements  -iel $i
}
# now start halo generation and tracking
TrackBackground -beam beam1 -dir $extension -charge $charge -start 0 -fulltracking 1 -linac 0

