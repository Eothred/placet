# can override environment vars with
# input arguments:
array set args {
    PLACET_DIR ""
}

if { [lsearch [array names env] PLACET_DIR] > 0 } {
  array set args [array get env PLACET_DIR]
}

array set args $argv

# general
set script_dir $args(PLACET_DIR)/examples/htgen/
set n_total 1000
set scale 1

# switch for SR
set synrad 0
set quad_synrad  0
set mult_synrad 0
set sbend_synrad 0

source $script_dir/clic_basic_single.tcl
# linac
source $script_dir/dummy_linac.tcl

# bds
SlicesToParticles

set e_initial 250.
set e0 250.;
source $script_dir/ilc_bds.20mr.tcl

# Generate beam line
BeamlineSet -name test

# Match linac parameters
array set match {beta_x 45.898599000636 beta_y 10.717784959269 alpha_x -2.247727284193 alpha_y 0.642859107717}
set match(emitt_x) 100.
set match(emitt_y) 0.4
set match(sigma_z) 300.0
set match(e_spread) 0.1
set charge 2e10

source $script_dir/clic_beam.tcl

set n_slice 30
set n 10
make_beam_many beam0 $n_slice $n

# name of output directory
set extension 10

# Description of Beam Delivery System
#-------------------------------------
#-------  Number of elements in the lattice
set nelements 1087
#-------  Vacuum structure
for {set i 0} { $i < 855 } {incr i} {
# warm part
Vacuum -nel $nelements -iel $i -temperature 273 -thetamin 0.1 -gas {
    { 7 14 50 } 
}
# By default material elements are vacuum
Material -X0 0. -nel $nelements  -iel $i
}
for {set i 855} { $i<$nelements } {incr i} {
# cold part
Vacuum -nel $nelements -iel $i -temperature 2 -thetamin 0.05 -gas {
    { 2 4 10. } 
}
# By default material elements are vacuum
Material -X0 0. -nel $nelements  -iel $i
}

# Here you define your spoilers
Material -X0 0.35 -nel $nelements  -iel 530
Material -X0 0.35 -nel $nelements  -iel 550
Material -X0 0.35 -nel $nelements  -iel 582
Material -X0 0.35 -nel $nelements  -iel 611
Material -X0 0.35 -nel $nelements  -iel 631

#---------- Option to track photons 
# TrackPhoton -file photon.dat -aperture 2.5 -fulltracking 1 -sampling 0.01

#hbu next generates a lot of out_10/..txt files
TrackBackground -beam beam0 -dir out_$extension -charge $charge -start 0 -fulltracking 0

exit
