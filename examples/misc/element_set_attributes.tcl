# here we define a beamline

Girder
Quadrupole
Bpm
Drift
Quadrupole
Bpm
Drift
BeamlineSet

# we set some attributes for all quadrupoles

ElementSetAttributes [QuadrupoleNumberList] -synrad 1 -strength 321.0

# here we set some attributes for all the elements

ElementSetAttributes [NameNumberList "*"] -length 123.0 -gas_A 12.0
