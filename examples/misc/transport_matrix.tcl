SetReferenceEnergy 8.0
Girder
Drift -l 123
Quadrupole -strength 6 -length 3
Sbend -length 10 -angle 30
Cavity -gradie 10.0 -length 10 -phase 20
BeamlineSet -name test

puts [GetTransferMatrix -beamline test]

Octave {
  for i=0:3
    placet_element_get_attribute("test", i, "type_name")
    placet_get_transfer_matrix("test", i,i)
    disp("det=")
    det(placet_get_transfer_matrix("test", i,i))
    disp("");
  end
}
