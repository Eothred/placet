## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_emittance (@var{B})
## Return the horizontal and vertical emittances of the beam `B', where `B' is a 6-columns matrix in the guinea-pig format.
## @end deftypefn

function E=placet_get_emittance(B)
  if (nargin == 1 && ismatrix(B) && columns(B)==6)
    C=covm(B);
    E=sqrt([C(2,2)*C(5,5)-C(2,5)**2,C(3,3)*C(6,6)-C(3,6)**2])*mean(B(:,1))/0.5109989e-3*1e-5;
  else
    help placet_get_emittance
  endif
endfunction
