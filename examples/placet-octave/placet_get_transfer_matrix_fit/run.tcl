#
# It calculates R# with the final doublet quadrupoles halved in intensity
#

set e_initial 1500
set e0 $e_initial
set script_dir .

array set args {
	deltae 0.001
	deltak 0.001
	sr 0
}
   
array set args $argv

set deltae $args(deltae)
set deltak $args(deltak)
set sr $args(sr)

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr
set sbend_synrad $sr

SetReferenceEnergy $e0
source $script_dir/lattices/bds.match.linac4b_noape
#Girder
#Quadrupole -length 11 -e0 $e0  -strength [expr $strength * 11] 
BeamlineSet -name test

array set match {
	alpha_x 0.59971622
	alpha_y -1.93937335
	beta_x  18.382571
	beta_y  64.450775
}

#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 4.0e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

FirstOrder 1

set e0 $e_initial

set n_slice 50
set n 1000
set n_total [expr $n_slice*$n]
make_beam_many beam0 $n_slice $n


set n_total 3
set n_slice 1
set n 3
make_beam_many beam3 $n_slice $n

set n_total 4
set n_slice 1
set n 4
make_beam_many beam4 $n_slice $n

Octave {

function [T,R]=placet_get_transfer_matrix2(beamline,begin_ele,end_ele,sigef)
if(~exist('sigef','var'))
    sigef=1e-6;
end
T=zeros(6,6,6);
R=zeros(6,6);

for i=1:6
    for j=1:6
        beam_usi=zeros(3,6);
        beam_usi(:,j)=[-sigef;0;sigef];
        beam=[ $e0 * (1+beam_usi(:,6)) [beam_usi(:,1) beam_usi(:,3) beam_usi(:,5) beam_usi(:,2) beam_usi(:,4)]*1e6];
        placet_set_beam("beam3", beam);
        [E, B] = placet_test_no_correction(beamline,"beam3", 'Zero',1,begin_ele,end_ele);
        B_usi=[[B(:,2) B(:,5) B(:,3) B(:,6) B(:,4)]*1e-6 (B(:,1)-$e0)/$e0];
        h=sigef;
        #the second derivative gives 2*coeff => .5 factor          
        T(i,j,j)=.5*(B_usi(3,i)-2*B_usi(2,i)+B_usi(1,i))/(h*h);
        R(i,j)=(B_usi(3,i)-B_usi(1,i))/(2*h);
        for k=(j+1):6
            beam_usi=zeros(4,6);
            beam_usi(:,[j k])=[sigef sigef;sigef -sigef;-sigef sigef;-sigef -sigef];
            beam=[ $e0 * (1+beam_usi(:,6)) [beam_usi(:,1) beam_usi(:,3) beam_usi(:,5) beam_usi(:,2) beam_usi(:,4)]*1e6];
            placet_set_beam("beam4", beam);            
            [E, B] = placet_test_no_correction(beamline,"beam4", 'Zero',1,begin_ele,end_ele);
            B_usi=[[B(:,2) B(:,5) B(:,3) B(:,6) B(:,4)]*1e-6 (B(:,1)-$e0)/$e0];
            h=sigef;
            T(i,j,k)=(B_usi(1,i)-B_usi(2,i)-B_usi(3,i)+B_usi(4,i))/(4*h*h);
            T(i,k,j)=T(i,j,k);
        end
    end
end
end

[T,R]=placet_get_transfer_matrix2("test",0,-1,1e-9);
RD=R
printf("det(RD) =  %g\n\n", det(RD));

placet_element_set_attribute("test", 0, "six_dim", 1);
[T,R]=placet_get_transfer_matrix2("test",0,-1,1e-9);
RTL=R
printf("det(RTL) =  %g\n\n", det(RTL));

placet_element_set_attribute("test", 0, "six_dim", 1);
placet_element_set_attribute("test", 0, "thin_lens", 30);
[T,R]=placet_get_transfer_matrix2("test",0,-1,1e-9);
RtL=R
printf("det(RtL) =  %g\n\n", det(RtL));

RM = placet_get_transfer_matrix("test")
printf("det(RM) =  %g\n\n", det(RM));

[RF,TF] = placet_get_transfer_matrix_fit("test", "beam0", "None");
RF
printf("det(RF) =  %g\n\n", det(RF));

save -text R.dat RF
save -text T.dat TF
}
