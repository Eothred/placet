### #      betax          alfax       betax         alfay
### twiss0 170.71067812   2.41421356  29.28932188  -0.41421356
### twiss1   5.85786438   0.41421356  34.14213562  -2.41421356
### length 90
### # drift,quad,drift,quad,drift , drift : L in [m], quad : f in [m]
### # last drift fixed by the code to fit total 'length' (see 2 lines above)
### seq 50 -30 30 10

array set twiss0 {
  beta_x 170.71067812
  beta_y 29.28932188 
  alpha_x 2.41421356  
  alpha_y -0.41421356
}

array set twiss1 {
  beta_x 5.85786438
  beta_y 34.14213562
  alpha_x 0.41421356
  alpha_y -2.41421356
}

set e_initial 9.0
set e0 $e_initial

set script_dir .

source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl

BeamlineNew
Girder
  SlicesToParticles ; # 0
  Drift -length 50 ; # 1
  Quadrupole -length 1.0 -strength [expr $e_initial / (-30.0)] ; # 2
  Drift -length 30
  Quadrupole -length 1.0 -strength [expr $e_initial / (+10.0)]
  Drift -length 30
BeamlineSet -name beamline

array set match [array get twiss0]

array set match {
  emitt_x 1.0
  emitt_y 1.0
}

set match(e_spread) 2.0
set match(charge) 2.56e9
set charge $match(charge)
set match(sigma_z) 0.0

set n_slice 31
set n 9
set n_total 15000

#
# Create the beam
#

make_beam_slice beam0 $n_slice $n

Octave {

  global twiss0 = zeros(4,4);
  twiss0(1:2,1:2) = [ $twiss0(beta_x), -($twiss0(alpha_x)); -($twiss0(alpha_x)), (1+($twiss0(alpha_x))*($twiss0(alpha_x)))/($twiss0(beta_x)) ];
  twiss0(3:4,3:4) = [ $twiss0(beta_y), -($twiss0(alpha_y)); -($twiss0(alpha_y)), (1+($twiss0(alpha_y))*($twiss0(alpha_y)))/($twiss0(beta_y)) ];

  global twiss1 = zeros(4,4);
  twiss1(1:2,1:2) = [ $twiss1(beta_x), -($twiss1(alpha_x)); -($twiss1(alpha_x)), (1+($twiss1(alpha_x))*($twiss1(alpha_x)))/($twiss1(beta_x)) ];
  twiss1(3:4,3:4) = [ $twiss1(beta_y), -($twiss1(alpha_y)); -($twiss1(alpha_y)), (1+($twiss1(alpha_y))*($twiss1(alpha_y)))/($twiss1(beta_y)) ];
  
  function merit = TwissMatching(beamline)
    global twiss1;
    [emitt,beam] = placet_test_no_correction(beamline, "beam0", "None");
    plot(beam(:,2), beam(:,5), '.');
    twiss = placet_get_twiss_matrix(beam);
    beta_x = twiss(1,1)/twiss1(1,1);
    beta_y = twiss(3,3)/twiss1(3,3);
    alpha_x = twiss(1,2)/twiss1(1,2);
    alpha_y = twiss(3,4)/twiss1(3,4);
    merit = sum((1.0 - [ beta_x, beta_y, alpha_x, alpha_y ]).**2);
  endfunction

  Corrector = [ 1, 2, 3, 4, 5];
  Leverage = [  "length"; "strength"; "length"; "strength" ; "length"  ];
  Constraint = [ 
    1 100
    0 0
    1 100
    0 0
    1 100
  ];
  
  #
  # here we optimize using the correctors
  # 
  
  [optimum, merit] = placet_optimize_constraint("beamline", "TwissMatching", Corrector, Leverage, Constraint)

  Leverage

  [emitt, beam] = placet_test_no_correction("beamline", "beam0", "None");

  twiss = placet_get_twiss_matrix(beam)

}
