#
# simple lattice for Placet tests
#
set usesynrad 0
set usesixdim 1
set usethinlens 0

set pi 3.141592653589793

set theta [expr 1.147/180*$pi]
set lbend 2.0
set rho [expr $lbend/sin($theta)]
set lbendarc [expr $theta*$rho]
set refenergy 9.0

set d12 [expr 15.5/cos($theta)]
set d23 1.0
set d34 [expr $d12]

Girder
Begin6d
Sbend -name B1 -length $lbendarc -angle $theta -synrad $usesynrad -six_dim $usesixdim -thin_lens $usethinlens -e0 $refenergy -E1 0.0 -E2 $theta
Drift -length $d12
Sbend -name B2 -length $lbendarc -angle -$theta -synrad $usesynrad -six_dim $usesixdim -thin_lens $usethinlens -e0 $refenergy -E1 -$theta -E2 0.0
Drift -length $d23
Sbend -name B3 -length $lbendarc -angle -$theta -synrad $usesynrad -six_dim $usesixdim -thin_lens $usethinlens -e0 $refenergy -E1 0.0 -E2 -$theta
Drift -length $d34
Sbend -name B4 -length $lbendarc -angle $theta -synrad $usesynrad -six_dim $usesixdim -thin_lens $usethinlens -e0 $refenergy -E1 $theta -E2 0.0
Drift -length 1.0
End6d