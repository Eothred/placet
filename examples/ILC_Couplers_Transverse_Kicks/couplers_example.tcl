### EXAMPLE OF 1 CRYOMODULE IN BUNCH COMPRESSOR STAGE 1

## boolean variables 

set rfkick 1  ; # set to 1 to simulate the couplers' RF-kick
set wakes 1   ; # set to 1 to simulate the couplers' wakefield kick


## bunch charge in number of particles

set charge 2e10


## cavity parameters

set cav_length    1.0362      ; # length in meters
set cav_gradient  0.0180383   ; # gradient in GV/m
set cav_phase     104.9       ; # phase, changed of sign


## RF-Kick (new) values (see presentation by Solyak at ICAP2009)

set kick_up 	"-45.3+4.7i"
set kick_down	"38.5+13.7i"

### OLD VALUES
##set kick_up 	"-48.3-3.4i"
##set kick_down	"41.0+14.5i"

## complex voltage for the crab cavity that simulates the RF-Kick (see RF-Kick.pdf)

proc rfkick_voltage { kick length gradient phase } {
 Octave {
  voltage = ($kick) * $length * $gradient * exp(-i * deg2rad($phase)) * 1e-3;
  Tcl_SetVar("ret", sprintf("(%g,%g)", real(voltage), imag(voltage)));
 }
 return $ret
}


###
### ACCELERATION CAVITY with COUPLERS: AccCavity_wCouplers
###
### this procedure must replace the PLACET's builtin cavity definition.
###
### it replaces a normal cavity (AccCavity) with a sequence
###
### UPSTREAM RF-KICK (1cm long) + ACCELERATING CAVITY +
### + WAKEFIELD KICK + DOWNSTREAM RF-KICK (1cm long)
###

proc AccCavity_wCouplers {} {
  global rfkick wakes
  global kick_up kick_down
  global cav_length cav_gradient cav_phase
  if { $rfkick } {
    Drift -length -0.01
    CrabCavity -name RFKICK -length 0.01 -voltage [rfkick_voltage $kick_up $cav_length $cav_gradient $cav_phase] -frequency 1.3 -tilt -90 -phase 90
  }
  AccCavity -name BC1CAV -length $cav_length -gradient $cav_gradient -phase $cav_phase
  if { $wakes } {
    TclCall -name WAKE -correct_offset 1 -script {
      Octave {
         placet_set_beam(CouplerWakes(placet_get_beam(), "wakes/wakes_sigZ9.dat", $charge));
      }
    }
  }
  if { $rfkick } {
    CrabCavity -name RFKICK -length 0.01 -voltage [rfkick_voltage $kick_down $cav_length $cav_gradient $cav_phase] -frequency 1.3 -tilt -90 -phase 90
    Drift -length -0.01
  }
}


##
##
## definitions for lattice parsing (we currently correct only in the vertical axis)
##

proc HCORRECTOR { a b c d e f } { Drift $a $b $c $d }
proc VCORRECTOR { a b c d e f } { Dipole $a $b $c $d }
proc Marker { a b } { Drift $a $b }


##
## The lattice (note that all cavities are now "AccCavity_wCouplers" )
##
## BC1's first cryomodule 
##
##

SetReferenceEnergy 5
Marker -name BEGINEBC1
Drift -name DBC1_1A -length 2.04408
Quadrupole -name QFBC1_1 -length 0.2 -strength 1.635822 
Bpm -name BPMQ079
HCORRECTOR -name XCOR -length 0 -kick 0
VCORRECTOR -name YCOR -length 0 -kick 0
Drift -name DBC1_1B -length 4.80661
Quadrupole -name QDBC1_1 -length 0.2 -strength -1.386096 
Bpm -name BPMQ079
VCORRECTOR -name YCOR -length 0 -kick 0
Drift -name DBC1_2 -length 2
Girder
Drift -name DEND -length 0.1056
AccCavity_wCouplers
SetReferenceEnergy 4.99519
Drift -name DCC -length 0.2907
AccCavity_wCouplers
SetReferenceEnergy 4.99039
Drift -name DCC -length 0.2907
AccCavity_wCouplers
SetReferenceEnergy 4.98558
Drift -name DCC -length 0.2907
AccCavity_wCouplers
SetReferenceEnergy 4.98078
Drift -name DCMQU -length 0.4418
Quadrupole -name QFBC1CM1 -length 0.2 -strength 0.732862 
Drift -name DCMBPMU -length 0.077
Drift -name DCMBPM -length 0.033
Bpm -name BPMCM
Drift -name DCMBPM -length 0.033
Drift -name DCMXYD -length 0.056
Drift -name DCMXY -length 0.1675
HCORRECTOR -name XCOR -length 0 -kick 0
VCORRECTOR -name YCOR -length 0 -kick 0
Drift -name DCMXY -length 0.1675
Drift -name DCMQD -length 0.4418
AccCavity_wCouplers
SetReferenceEnergy 4.97597
Drift -name DCC -length 0.2907
AccCavity_wCouplers
SetReferenceEnergy 4.97116
Drift -name DCC -length 0.2907
AccCavity_wCouplers
SetReferenceEnergy 4.96636
Drift -name DCC -length 0.2907
AccCavity_wCouplers
SetReferenceEnergy 4.96155
Drift -name DEND -length 0.1056
Drift -name DMM -length 0.7894
