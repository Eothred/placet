#ifndef emitt_data_hh
#define emitt_data_hh

#include <vector>
#include "particle.hh"
#include "stream.hh"

struct EMITT_DATA_LINE {
  double ex, ey,  ex2, ey2, x, y, xp, yp, Env,  sx, sy,  sxp, syp; 
  int n; 
  double s;
  EMITT_DATA_LINE() : ex(0.0), ey(0.0), ex2(0.0), ey2(0.0), x(0.0), y(0.0), xp(0.0), yp(0.0), Env(0.0), sx(0.0), sy(0.0), sxp(0.0), syp(0.0), n(0), s(0.0) {}
  EMITT_DATA_LINE(const std::vector<Particle> &bunch, double _s=0.0 ) : n(1), s(_s) { init(bunch); }
  void init(const std::vector<Particle> &bunch );
  friend OStream &operator<<(OStream &stream, const EMITT_DATA_LINE &e ) { return stream << e.ex << e.ey << e.ex2 << e.ey2 << e.x << e.y << e.xp << e.yp << e.Env << e.sx << e.sy << e.sxp << e.syp << e.n << e.s; }
  friend IStream &operator>>(IStream &stream, EMITT_DATA_LINE &e ) { return stream >> e.ex >> e.ey >> e.ex2 >> e.ey2 >> e.x >> e.y >> e.xp >> e.yp >> e.Env >> e.sx >> e.sy >> e.sxp >> e.syp >> e.n >> e.s; }
};

#endif /* emitt_data_hh */
