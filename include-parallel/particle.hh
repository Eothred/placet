#ifndef particle_hh
#define particle_hh

#include <iostream>

#include "vector.hh"
#include "placet.h"

struct Particle {

  double x, xp, y, yp, z, energy, weight;
	
  Particle(const Vector<7> &v ) { x  = v[0]; xp = v[1]; y  = v[2]; yp = v[3]; z  = v[4]; energy = v[5]; weight = v[6]; }
  Particle(const Vector<6> &v ) { x  = v[0]; xp = v[1]; y  = v[2]; yp = v[3]; z  = v[4]; energy = v[5]; }

  Particle() {}

  inline double &operator[](size_t i) { return (&x)[i]; }
  inline const double &operator[](size_t i) const { return (&x)[i]; }
	
  inline operator const Vector<6>&() const { return reinterpret_cast<const Vector<6>&>(*this); }
  inline operator const Vector<7>&() const { return reinterpret_cast<const Vector<7>&>(*this); }
	
  inline double beta() const { double _t = energy * INV_EMASS; _t *= _t; return (_t - 0.5) / _t; }  // first order approximation
  inline double beta2() const { double _t = energy * INV_EMASS; _t *= _t; return (_t - 1) / _t; }

  inline double gamma() const { return energy * INV_EMASS; }
  inline double gamma2() const { double _t = energy * INV_EMASS; return _t*_t; }

  inline double energy_deviation(double energy0 ) const { return energy > 0. ? (energy-energy0)/energy0 : (-energy-energy0)/energy0; }
  inline double momentum_deviation(double energy0 ) const
  {
    double _t = energy0 * INV_EMASS; _t *= _t;
    double e0b0 = energy0 * (_t - 0.5) / _t;
    return (energy * beta() - e0b0) / e0b0;
  }

  friend OStream &operator<<(OStream &stream, const Particle &p ) {
    stream.write((const char*)(&p), sizeof(Particle));
    return stream;
  }
  friend IStream &operator>>(IStream &stream, Particle &p ) { 
    stream.read((char*)(&p), sizeof(Particle)); 
    return stream;
  }

  friend std::ostream &operator<<(std::ostream &stream, const Particle &particle )
  {
    return stream << particle.energy << ' ' << particle.x <<  ' ' << particle.y << ' ' << particle.z << ' ' << particle.xp << ' ' << particle.yp;
  }

	
};

#endif /* particle_hh */
