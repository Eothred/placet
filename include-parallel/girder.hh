#ifndef girder_hh
#define girder_hh

#include <list>

#include "element.hh"
#include "object.hh"
#include "error.hh"

class Girder : public Object {

  double	dist;
	
  std::list<Element*>	elements;
	
public:

  TYPEDEF_ERROR(error);	// defines type Girder::error

  typedef std::list<Element*>::const_iterator	element_const_itr;
  typedef std::list<Element*>::iterator		element_itr;

public:
	
  Girder(const Girder &girder ) : Object(girder), dist(girder.dist)
  {
    for (element_const_itr itr=girder.elements.begin(); itr!=girder.elements.end(); ++itr) {
      elements.push_back(Element::duplicate(*itr));
    }
  }
  Girder() {}

  ~Girder() { clear(); }

  Girder *duplicate() const {	return new Girder(*this); }

  void clear()
  {
    for (element_itr itr=elements.begin(); itr!=elements.end(); ++itr) {
      delete (*itr);
    }
    elements.clear();
  }

  const Girder &operator=(const Girder &girder )
  {
    if (this!=&girder)
      {
	Object::operator=(girder);
	dist=girder.dist;
	clear();
	for (element_const_itr itr=girder.elements.begin(); itr!=girder.elements.end(); ++itr) {
	  elements.push_back(Element::duplicate(*itr));
	}
      }
    return *this;
  }

  const std::list<Element*> &get_elements() const { return elements; }
  std::list<Element*> &get_elements() { return elements; }

  element_const_itr begin() const { return elements.begin(); }
  element_const_itr end() const { return elements.end(); }

  element_itr begin()  { return elements.begin(); }
  element_itr end()  { return elements.end(); }

  void add_element(Element *element ) { elements.push_back(element); }

  double get_length() const
  {
    double l=0;
		
    element_const_itr itr;
		
    for (itr=elements.begin(); itr!=elements.end(); ++itr)
      {
	l += (*itr)->get_length();
      }
		
    return l;
  }

  friend OStream &operator<<(OStream &stream, const Girder &girder )
  {	
    stream 	<< static_cast<const Object &>(girder)
		<< girder.dist
		<< girder.elements.size();

    for (Girder::element_const_itr itr=girder.elements.begin(); itr!=girder.elements.end(); ++itr) {
      Element::insert_element_ptr(stream, *itr);
    }
		
    return stream;
  }
	
  friend IStream &operator>>(IStream &stream, Girder &girder )
  {
    size_t size;

    stream	>> static_cast<Object &>(girder)
		>> girder.dist
		>> size;

    girder.clear();
		
    for(size_t i=0; i<size; i++) {
      if (Element *element_ptr=Element::extract_element_ptr(stream)) {

	girder.elements.push_back(element_ptr);

	/*			} else {

				throw error("unknown element on input stream");
	*/
      }
    }

    return stream;
  }

};

#endif /* girder_hh */
