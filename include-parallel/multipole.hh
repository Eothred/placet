#ifndef multipole_hh
#define multipole_hh

#include <cmath>
#include <complex>
#include "element.hh"

class Multipole : public Element {

  int	type;
  int	nstep;
	
  std::complex<double> 	strength;	// GeV / m
  double	tilt;		// rad
		
public:

  Multipole(const std::string &name, double length, int _type, int _nstep, double _strength, double _tilt, double ref_energy=0.0 ) : Element(name, length, ref_energy), type(_type), nstep(_nstep), strength(_strength), tilt(_tilt) {}
  
  Multipole(double length, int _type, size_t _nstep, double _strength, double _tilt, double ref_energy=0.0 ) : Element(length, ref_energy), type(_type), nstep(_nstep), strength(_strength), tilt(_tilt) {}

  Multipole() {}

  int get_id() const { return ELEMENT_ID::MULTIPOLE; }

  void transport(std::vector<Particle> &beam ) const;
  void transport_synrad(std::vector<Particle> &beam ) const;

  friend OStream &operator<<(OStream &stream, const Multipole &mpole )
  {
    return stream << static_cast<const Element&>(mpole) 
		  << mpole.type
		  << mpole.nstep
		  << mpole.strength
		  << mpole.tilt;
  }
  
  friend IStream &operator>>(IStream &stream, Multipole &mpole )
  {
    return stream >> static_cast<Element&>(mpole) 
		  >> mpole.type
		  >> mpole.nstep
		  >> mpole.strength
		  >> mpole.tilt;
  }

};

#endif /* multipole_hh */
