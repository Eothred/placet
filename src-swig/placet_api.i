/* placet_api.i */

%{
#include "placet_api.hh"
#include "placet_api_client.hh"
%}

%include "std_string.i"

%rename("%(regex:/placet_Tcl(.*)/Tcl\\1/)s") "";

#if defined(SWIGOCTAVE)
%include "octave/matrixnd.i"
%include "octave/attribute.i"
%include "octave/vector_size_t.i"
%include "octave/vector_string.i"
%include "octave/vector_matrixnd.i"
%include "octave/vector_attribute.i"
%include "octave/vector_map_attribute.i"

%typemap(in, numinputs=0) std::string &out_value (std::string temp ) {
  $1 = &temp;
}

%typemap(argout) std::string &out_value {
  $result->append(octave_value(*$1));
}

%typemap(in, numinputs=0) double &out_position (double temp ) {
  $1 = &temp;
}

%typemap(argout) double &out_position {
  $result->append(octave_value(*$1));
}

#endif

#if defined(SWIGPYTHON)
%{
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#define is_array(a) ((a) && PyArray_Check((PyArrayObject *)a))
%}

%init %{
  import_array();
%}

%rename("%(strip:[placet_])s") "";

%include "python/matrixnd.i"
%include "python/attribute.i"
%include "python/vector_size_t.i"
%include "python/vector_string.i"
%include "python/vector_matrixnd.i"
%include "python/vector_attribute.i"
%include "python/vector_map_attribute.i"

%typemap(in, numinputs=0) std::string &out_value (std::string temp ) {
  $1 = &temp;
}

%typemap(argout) std::string &out_value {
  $result = SWIG_Python_AppendOutput($result, PyString_FromString((*$1).c_str()));
}

%typemap(in, numinputs=0) double &out_position (double temp ) {
  $1 = &temp;
}

%typemap(argout) double &out_position {
  $result = SWIG_Python_AppendOutput($result, PyFloat_FromDouble(*$1));
}
#endif

%include "placet_api.hh"
%include "placet_api_client.hh"

