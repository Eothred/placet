/* matrixnd.i */

%{
#include "matrixnd.hh"
%}

#if defined(SWIGPYTHON)

// T_ijk as an output argument
%typemap(in, numinputs=0) std::vector<MatrixNd> & (std::vector<MatrixNd> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<MatrixNd> & {
  npy_intp dimensions[3] = { 6, 6, 6 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(3, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (size_t i=0; i<dimensions[0]; i++)
    for (size_t j=0; j<dimensions[1]; j++)
      for (size_t k=0; k<dimensions[2]; k++)
        *(double*)(data + i*strides[0] + j*strides[1] + k*strides[2]) = (*$1)[i][j][k];
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res));
}

#endif
