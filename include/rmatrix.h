#ifndef rmatrix_h
#define rmatrix_h

#include <algorithm>

#include "element.h"

struct R_MATRIX {

  double r11,r12,r21,r22;

  inline R_MATRIX():r11(0), r12(0), r21(0), r22(0) {}
  inline explicit R_MATRIX(double a ) :  r11(a), r12(0), r21(0), r22(a){}
  inline R_MATRIX(double a, double b, double c, double d ) : r11(a), r12(b), r21(c), r22(d) {}
  
  inline void transpose() { std::swap(r12, r21); }
  
  friend inline R_MATRIX transpose(const R_MATRIX &a ) { return R_MATRIX(a.r11, a.r21, a.r12, a.r22); }

  inline const R_MATRIX &operator += (const R_MATRIX &m ) { r11+=m.r11; r12+=m.r12; r21+=m.r21; r22+=m.r22; return *this; }
  inline const R_MATRIX &operator -= (const R_MATRIX &m ) { r11-=m.r11; r12-=m.r12; r21-=m.r21; r22-=m.r22; return *this; }
  inline const R_MATRIX &operator *= (const R_MATRIX &m ) { return *this = *this * m; }
  
  friend inline R_MATRIX operator * (const R_MATRIX &a, double b ) { return R_MATRIX(a.r11*b, a.r12*b, a.r21*b, a.r22*b); }
  friend inline R_MATRIX operator * (double b, const R_MATRIX &a ) { return R_MATRIX(a.r11*b, a.r12*b, a.r21*b, a.r22*b); }
  friend inline R_MATRIX operator / (const R_MATRIX &a, double b ) { return R_MATRIX(a.r11/b, a.r12/b, a.r21/b, a.r22/b); }
  friend inline R_MATRIX operator + (const R_MATRIX &a, const R_MATRIX &b ) { return R_MATRIX(a.r11+b.r11, a.r12+b.r12, a.r21+b.r21, a.r22+b.r22); }
  friend inline R_MATRIX operator - (const R_MATRIX &a, const R_MATRIX &b ) { return R_MATRIX(a.r11-b.r11, a.r12-b.r12, a.r21-b.r21, a.r22-b.r22); }
  friend inline R_MATRIX operator - (const R_MATRIX &a ) { return R_MATRIX(-a.r11, -a.r12, -a.r21, -a.r22); }
  
  friend inline R_MATRIX operator * (const R_MATRIX &a, const R_MATRIX &b )
  {
    return R_MATRIX(a.r11*b.r11+a.r12*b.r21,
                    a.r11*b.r12+a.r12*b.r22,
                    a.r21*b.r11+a.r22*b.r21,
                    a.r21*b.r12+a.r22*b.r22);
  }

};

struct R_VECTOR{
  double y,yp;
};

class BEAM;

class RMATRIX : public ELEMENT {
  
  int correct_offset;
  
  double matrix[16];
    
  void step_in(BEAM *beam ) { if (correct_offset) ELEMENT::step_in(beam); }
  void step_out(BEAM *beam ) { if (correct_offset) ELEMENT::step_out(beam); }

  void step_4d_0(BEAM * );
  void step_4d(BEAM * );
  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0, int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));

public:
 
  explicit RMATRIX(double l=0.0, double *r=NULL );
  
  bool is_rmatrix() const { return true; }
  
  void set_matrix(double *r )
  {
    memcpy(matrix,r,sizeof(double)*16);
  }

};

#endif /* rmatrix_h */
