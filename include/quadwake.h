#ifndef quadwake_h
#define quadwake_h

QUAD_KICK_DATA* quad_kick_data(BEAM *beam,int n_cell);
void quad_kick_data_delete(QUAD_KICK_DATA *quad_kick_data);
void quad_kick_data_fill(QUAD_KICK_DATA *quad_kick_data,BEAM *beam,int i_cell,
			 double lambda,double loss,double q,double charge);
double quadrupole_moment(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
			 R_MATRIX *sigma_xy);
void quadrupole_step_kick(double k,PARTICLE *particle,R_MATRIX *sigma,
			  R_MATRIX *sigma_xx,R_MATRIX *sigma_xy);
void slice_rotate(PARTICLE *particle,R_MATRIX *sigma,R_MATRIX *sigma_xx,
		  R_MATRIX *sigma_xy,double theta);
void quadrupole_wake_prepare(BEAM *beam,double charge);
void quadrupole_kick(BEAM *beam,double length,int mode);
void quadrupole_wake_prepare_const(BEAM *beam,double charge);
void quadrupole_kick_const(BEAM *beam,double length,int mode);

#endif // quadwake_h
