#ifndef spectrum_h
#define spectrum_h

int tk_TestSpectrum(ClientData ,Tcl_Interp *,int ,char **);
int tk_TestSpectrumSimpleCorrection(ClientData ,Tcl_Interp *,int ,char **);

#endif
