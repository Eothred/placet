#ifndef graph_h
#define graph_h

void element_position_plot(BEAMLINE *beamline, char *fname, bool bpm, bool quad, bool position);
void bpm_plot_all(BEAMLINE *,char *);
void bpm_position_plot(BEAMLINE *,char *);
void quadrupole_position_plot(BEAMLINE *,char *);
int testshow(BEAM *,int ,Tcl_Interp *,char *);
int testshow2(BEAM *,int ,Tcl_Interp *,char *);
int tk_BeamlinePositionPlot(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamlineShow(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamShow(ClientData ,Tcl_Interp *,int ,char **);
int tk_BpmPlot(ClientData ,Tcl_Interp *,int ,char **);
int tk_BpmPositionPlot(ClientData ,Tcl_Interp *,int ,char **);
int tk_QuadrupolePositionPlot(ClientData ,Tcl_Interp *,int ,char **);

#endif // graph_h
