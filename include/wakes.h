#ifndef _h_wakes
#define _h_wakes

class BEAM;

/// prepare cavity wakes, use default name "" 
void wakes_prepare(BEAM*,const char* name="", int iw=0);

#endif
