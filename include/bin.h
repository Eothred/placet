#ifndef bin_h
#define bin_h

void beamline_bin_divide_tcl(Tcl_Interp *,BEAMLINE* ,int,int);
int tk_BeamlineBinDivide(ClientData ,Tcl_Interp* ,int ,char **);
void bin_define(BEAMLINE *,int ,int ,int *,int *,int *,int *);
void bin_define_limit_correctors(BEAMLINE *,int ,int ,int *,int ,int ,int *,int *,int *,int *);
void bin_define_limit(BEAMLINE *,int ,int ,int ,int *,int *,int *,int *);
void bin_define_dipole(BEAMLINE *,int ,int ,int *,int *,int *,int *);
void bin_define_1(BEAMLINE *,int ,int ,int *,int *,int *,int *);
void bin_define_2(BEAMLINE *,int ,int ,int ,int *,int *,int *,int *);
void bin_define_one_to_one(BEAMLINE *,int ,int *,int *,int *,int *);
void bin_define_dipole_one_to_one(BEAMLINE *,int ,int *,int *,int *,int *);
void bin_define_feedback(BEAMLINE *,int ,int ,int *,int *,int *,int *);
void bin_set_elements_x(BIN *,int *,int ,int *,int ,int *,int ,int *,int ,int ,int);
void bin_set_elements(BIN *,int *,int ,int *,int ,int);
void bin_set_gain(BIN *,double);
void bin_set_dtau(BIN *,double);
void bin_delete(BIN *);
BIN *bin_make_x(int ,int ,int ,int);
BIN *bin_make(int ,int);
void binarray_write_file(char*,BIN**,int);
int binarray_read_file(char*,BIN***);

#endif // bin_h
