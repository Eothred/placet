#ifndef dl_element_defs_h
#define dl_element_defs_h

class DLELEMENT;
class BEAM;

extern "C" {

extern bool dl_element_init(DLELEMENT &_this, int &argc, char **argv );
extern void dl_element_fini(DLELEMENT &_this );

extern void dl_element_step_in(DLELEMENT &_this, BEAM *beam );
extern void dl_element_step_out(DLELEMENT &_this, BEAM *beam );

extern void dl_element_step_4d(DLELEMENT &_this, BEAM *beam );
extern void dl_element_step_4d_0(DLELEMENT &_this, BEAM *beam );
extern void dl_element_step_4d_sr(DLELEMENT &_this, BEAM *beam );
extern void dl_element_step_4d_sr_0(DLELEMENT &_this, BEAM *beam );

extern void dl_element_step_6d(DLELEMENT &_this, BEAM *beam );
extern void dl_element_step_6d_0(DLELEMENT &_this, BEAM *beam );
extern void dl_element_step_6d_sr(DLELEMENT &_this, BEAM *beam );
extern void dl_element_step_6d_sr_0(DLELEMENT &_this, BEAM *beam );

}

#endif /* dl_element_defs_h */
