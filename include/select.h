#ifndef select_h
#define select_h
#include "rmatrix.h"

/** 
    class RandomSelect
    only needed by HTGEN
    Possibly should be moved to there.
 */
class RandomSelect {
  double *wgt;
  int n0;
public:
  RandomSelect(int n) {wgt=new double[n];n0=n;};
  void set_wgt(int i,double wgt0) {wgt[i]=wgt0;};
  void init() {
    int i;
    for(i=1;i<n0;i++){wgt[i]+=wgt[i-1];} // calculate progressive weights
    for(i=0;i<n0-1;i++){wgt[i]/=wgt[n0-1];} // normalise vector with total weight
    wgt[n0-1]=1.0; // normalise last one element as well
  };
  int choose(double w) { // find the element number that is closest to weight w
    int nl=0,nh=n0-1,nt;
    if (wgt[0]>w) return 0;
    while (nh-nl>1) {
      nt=(nh+nl)/2;
      if (wgt[nt]<w) {
	nl=nt;
      }
      else {
	nh=nt;
      }
    }
    return nh;
  };
  ~RandomSelect() {delete[] wgt;};
};

void one_particle(R_MATRIX *sxx,R_MATRIX *sxy,R_MATRIX *syy,PARTICLE *p0, PARTICLE *p);
void StoreParticles_Init(Tcl_Interp *interp);
void one_particle_ellipse(double betax,double alphax,double rx,
		     double betay,double alphay,double ry,
		     double phasex,double phasey,PARTICLE *p);
void one_particle_from_twiss(double betax,double alphax,double rx,
			double betay,double alphay,double ry,
			double e0,double se,
			PARTICLE *p);

#endif
