#ifndef option_set_h
#define option_set_h

#include <string>
#include <vector>
#include <map>

#include "option_info.hh"
#include "placet_cout.hh"
#include "stream.hh"

class option_set {

  std::vector<option_info> set;

  static option_info option_null;

public:

  option_set(const option_set &o );
  option_set() {}  
  ~option_set();
  
  const option_info &operator[](const char *attribute ) const
  {
    if (const option_info *option=find(attribute))
      return *option;
    return option_null;
  }

  const option_info &operator[](int i ) const { return set[i]; }
  option_info &operator[](int i ) { return set[i]; }
  
  size_t size() const { return set.size(); }
  void reserve(size_t n) { set.reserve(n); }

  option_info *find(const char *attribute );
  const option_info *find(const char *attribute ) const;
  
  option_set &operator = (const option_set &o );
  
  void add(const char *name, const char *description, option_type type, void *_object, Set _set, Get _get ) { add(option_info(name, description, type, (option_helper*)_object, Set(_set), Get(_get))); }
  void add(const char *name, const char *description, option_type type, void *dest, double_unary_function func, double_unary_function ifunc=NULL );
  void add(const char *name, const char *description, option_type type, void *dest = NULL );
  void add(const char *name, option_type type, void *_object, Set _set, Get _get ) { add(option_info(name, type, (option_helper*)_object, Set(_set), Get(_get))); }
  void add(const char *name, option_type type, void *dest, double_unary_function func, double_unary_function ifunc=NULL );
  void add(const char *name, option_type type, void *dest = NULL );
  void add(option_info option );
  void remove(const char *name );

  bool parse_args(int &argc, char **argv, bool purge_argv = true );
  
  double      get_value_double(const char *attribute ) const { 
    if (const option_info *option = find(attribute)) 
      return option->get_double(); 
    else {		  
      placet_cout << WARNING << "attribute not found: " << attribute << endmsg;
      return 0.0; }
  }
  int         get_value_int(const char *attribute ) const { 
    if (const option_info *option = find(attribute)) 
      return option->get_int(); 
    else {
      placet_cout << WARNING << "attribute not found: " << attribute << endmsg;
      return 0; 
    }
  }
  bool        get_value_bool(const char *attribute ) const { 
    if (const option_info *option = find(attribute)) 
      return option->get_bool(); 
    else {
      placet_cout << WARNING << "attribute not found: " << attribute << endmsg;
      return false;
    }
  }
  std::string get_value_string(const char *attribute ) const { 
    if (const option_info *option = find(attribute)) {
      return option->get_string();
    } else {
      placet_cout << WARNING << "attribute not found: " << attribute << endmsg;
      return ""; 
    }
  }

  std::string get_value_as_string(const char *attribute ) const {
    if (const option_info *option = find(attribute)) {
      return option->get_value_as_string();
    } else {
      placet_cout << WARNING << "attribute not found: " << attribute << endmsg;
      return "";
    }
  }
  
  template <typename T> bool set_value(const char *attribute, T value )
  {
    if (option_info *option = find(attribute)) {
      option->set(value);
      return true;
    } else {
      placet_cout << WARNING << "attribute not found: " << attribute << endmsg;
      return false;
    }
  }

  bool set_value_from_string(const char *attribute, const std::string &value )
  {
    if (option_info *option = find(attribute)) {
      option->set_value_from_string(value);
      return true;
    } else {
      placet_cout << WARNING << "attribute not found: " << attribute << endmsg;
      return false;
    }
  }


  friend std::ostream &operator<<(std::ostream &stream, const option_set &set );
  
  friend OStream &operator << (OStream &stream, const option_set &o );
  friend IStream &operator >> (IStream &stream, option_set &o );

};

#endif /* option_set_h */
