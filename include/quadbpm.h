#ifndef quadbpm_h
#define quadbpm_h

#include "quadrupole.h"
#include "bpm.h"

class QUADBPM : public QUADRUPOLE, public BPM {  

  void step_in(BEAM *beam ) { QUADRUPOLE::step_in(beam); }
  void step_out(BEAM *beam ) { QUADRUPOLE::step_out(beam); }

  void step_4d_0(BEAM*);
  void step_4d(BEAM*);
  void step_4d_sr_0(BEAM *);

  void step_6d_0(BEAM*);

public:

  struct {
    double x;
    double y;
  } bpm_offset;

  QUADBPM(int &argc, char **argv ) : QUADRUPOLE(0.,0.), BPM() {set_attributes(argc, argv);}

  //  QUADBPM(double length, double strength ) : QUADRUPOLE(length, strength)
  //  {
  //   enable_hcorr("x", 0.0);
  //   enable_vcorr("y", 0.0);
  // }

  double get_bpm_x_reading() const { return get_x_reading(); }
  double get_bpm_y_reading() const { return get_y_reading(); }
  double get_bpm_offset_x() const { return bpm_offset.x; }
  double get_bpm_offset_y() const { return bpm_offset.y; }

  void set_bpm_offset_x(double x ) { bpm_offset.x = x; }
  void set_bpm_offset_y(double y ) { bpm_offset.y = y; }

  bool is_quadbpm() const { return true; }
  
  QUADBPM *quadbpm_ptr() { return this; }
  const QUADBPM *quadbpm_ptr() const { return this; }

  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback0)(FILE*,BEAM*,int,double,int,int)) { QUADRUPOLE::step_twiss(beam,file,step,j,s0,n1,n2,callback0); }
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback0)(FILE*,BEAM*,int,double,int,int)) { QUADRUPOLE::step_twiss_0(beam,file,step,j,s0,n1,n2,callback0); }
  
};

int tk_BpmSetToOffset(ClientData ,Tcl_Interp *,int,char **);

#endif /* quadbpm_h */
