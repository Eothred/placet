#ifndef dipole_kick_h
#define dipole_kick_h

class BEAM;
class ELEMENT;
struct WAKE_DATA;

extern void dipole_kick_n(ELEMENT *element, WAKE_DATA *wake_data, BEAM *beam );
extern void dipole_kick(ELEMENT *element, WAKE_DATA *wake_data, BEAM *beam );

#endif /* dipole_kick_h */
