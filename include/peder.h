#ifndef PEDER_H
#define PEDER_H

int tk_ApplyJitter(ClientData clientdata,Tcl_Interp *interp,int argc,
		   char *argv[]);
int tk_DynamicEffectsSet(ClientData clientdata,Tcl_Interp *interp,int argc,
			 char *argv[]);
int tk_GetResponseMatrix(ClientData clientdata,Tcl_Interp *interp,int argc,
			 char *argv[]);
int tk_CalcEmitt(ClientData clientdata,Tcl_Interp *interp,
		 int argc,char *argv[]);
int tk_CalcSquareFit(ClientData clientdata,Tcl_Interp *interp,
		     int argc,char *argv[]);
int tk_CavityNumberList(ClientData clientdata,Tcl_Interp *interp,int argc,
			char *argv[]);
int tk_ElementGetOffset(ClientData clientdata,Tcl_Interp *interp,
			int argc,char *argv[]);
int tk_Svd(ClientData clientdata, Tcl_Interp *interp, int argc,
	   char *argv[]);
int tk_BpmSetResolution(ClientData clientdata,Tcl_Interp *interp,int argc,
			char *argv[]);
int tk_BumpCorrection(ClientData clientdata,Tcl_Interp *interp,int argc,
		      char *argv[]);
int tk_BumpSetup(ClientData clientdata,Tcl_Interp *interp,int argc,
		      char *argv[]);
int tk_KnobReferenceSet(ClientData clientdata,Tcl_Interp *interp,int argc,
			char *argv[]);
int tk_KnobReferenceZero(ClientData clientdata,Tcl_Interp *interp,int argc,
			 char *argv[]);
int tk_KnobSave(ClientData clientdata,Tcl_Interp *interp,int argc,
		char *argv[]);
int Peder_Init(Tcl_Interp*);

#endif /* PEDER_H */
