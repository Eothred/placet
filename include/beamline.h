#ifndef beamline_h
#define beamline_h

#include <string>

#include "element.h"
#include "latex_picture.h"
#include "placet.h"
#include "stream.hh"

class GIRDER;
class QUADRUPOLE;
class CAVITY;
class BPM;
class GROUND_DATA;

/**
 * @brief a beamline definition
 * 
 * This class keeps information about the beamline.
 * class owns its pointers, but not the elements or girders
 * 
 */

class BEAMLINE {

 public:
  /// constructor
  BEAMLINE();
  /// destructor, 
  // deletes whole beamline including elements and girders
  ~BEAMLINE();
  void unset(); // frees pointers and ground motion, elements and girders are not deleted

  /** set beamline, sets element numbers
      allocates memory for arrays
      file output option is used for suppressing output (NULL) */
  void set(FILE *file = stdout);
  /** set/get name */
  void set_name(const char* n){name = std::string(n);}
  const char* get_name()const{return name.c_str();}
  /** add girder to the end of line with a distance dist to last element */
  void girder_add(GIRDER *girder,double dist);
  /** returns length of BEAMLINE */
  double get_length()const{return length;}
  /** calculates length of BEAMLINE, can give slightly different number as precalculated length  */
  //double calc_length()const;
  /** prints BEAMLINE to FILE */
  void list(FILE *file)const;
  /** set all offset to zero */
  void set_zero();
  /** return girder number for an element number */
  int girder_number(int element_nr)const;
  /** check if element number is in beamline range, prints ERROR if not */
  bool element_in_range(int element_nr)const;
  /** print element names and lengths to FILE */
  void print(FILE *file)const;

 private:
  /// private copy constructor (not implemented)
  BEAMLINE(const BEAMLINE&);
  /// private assign operator (not implemented)
  BEAMLINE& operator=(const BEAMLINE&);

  std::string name;
  double length;
 public:

  // TODO: beamline elements, put in vectors
  int n_girders,n_elements;
  /// pointers to first and last girder
  GIRDER *first,*last;
  ELEMENT **element;
  QUADRUPOLE **quad;
  CAVITY **cav;
  BPM  **bpm;
  int n_quad,n_bpm,n_cav;
  /** ground motion object for both planes, owned by beamline */
  GROUND_DATA *gx, *gy;

  friend LaTeX_Picture &operator<<(LaTeX_Picture &stream, const BEAMLINE &beamline )
  {
    ELEMENT **element=beamline.element;
    for (int i=0;i<beamline.n_elements;i++)
      stream << *element[i];
    return stream;
  }

  void save_footprint(std::ostream &stream, int start=0, int end=-1 );

};

#endif /* beamline_h */
