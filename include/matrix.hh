#ifndef matrix_hh
#define matrix_hh

#include <iostream>

#include "vector.hh"

template <size_t M, size_t N> class Matrix {

  double data[M*N];

public:
  
  inline explicit Matrix(double arg ) { for (size_t i = 0; i < M*N; i++) data[i] = arg; }
  inline explicit Matrix(const double m[][N] )
  {
    for (size_t i=0; i<M; i++) {
      for (size_t j=0; j<N; j++) {
	data[N*i+j]=m[i][j];
      }
    }
  }
  
  Matrix(double a11, double a12, ... ) 
  {
    data[0]=a11;
    data[1]=a12;
    va_list ap;
    va_start(ap, a12);
    for (size_t j=2; j<N; j++) {
      data[j]=va_arg(ap, double);
    }
    for (size_t i=1; i<M; i++) {
      for (size_t j=0; j<N; j++) {
	data[N*i+j]=va_arg(ap, double);
      }
    }
    va_end(ap);
  }
  
  template <size_t I, size_t J>
  inline explicit Matrix(const Matrix<I,J> &m )
  {
    for (size_t i=0;i<(I<=M?I:M);i++) {
      for (size_t j=0;j<(J<=N?J:N);j++) {
        data[N*i+j]=m[i][j];
      }
    }
  }
  
  inline Matrix() {zero();}
  
  inline void zero() { for (size_t i = 0; i < M*N; i++)	data[i] = 0.0; }
  
  inline void identity()
  {
    for (size_t i=0;i<M*N;i++)  data[i]=0.0;
    for (size_t i=0;i<(N<=M?N:M);i++) data[(N+1)*i]=1.0;
  }
  
  inline size_t size() const { return M; }
  
  Matrix<M-1,N-1> get_submatrix(size_t i, size_t j ) const
  {
    Matrix<M-1, N-1> result;
    size_t __i=0;
    for (size_t _i=0; _i<M; _i++) {
      if (_i!=i) {
	size_t __j=0;
	for (size_t _j=0; _j<N; _j++) {
	  if (_j!=j) {
	    result[__i][__j]=(*this)[_i][_j];
	    __j++;
	  }
	}
	__i++;
      }
    }
    return result;
  }
  
  inline const Matrix &operator = (const double m[][N] )
  {
    for (size_t i=0; i<M; i++) {
      for (size_t j=0; j<N; j++) {
	data[N*i+j]=m[i][j];
      }
    }
    return *this;
  }
  inline const Matrix &operator += (const Matrix &m )			{ if (&m != this)	for (size_t i = 0; i < M*N; i++)	*data[i] += m.data[i]; return *this; }
  inline const Matrix &operator -= (const Matrix &m )			{ if (&m != this)	for (size_t i = 0; i < M*N; i++)	*data[i] -= m.data[i]; return *this; }
  inline const Matrix &operator *= (const Matrix &m )			{ return *this = *this * m; }
  
  inline const double *operator [] (size_t i ) const { return &data[N*i]; }
  inline double *operator [] (size_t i ) { return &data[N*i]; }
  
  friend Matrix operator * (const Matrix &a, double b )			{ Matrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] * b; return result; }
  friend Matrix operator * (double b, const Matrix &a )			{ Matrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] * b; return result; }
  friend Matrix operator / (const Matrix &a, double b )			{ Matrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] / b; return result; }
  friend Matrix operator + (const Matrix &a, const Matrix &b )		{ Matrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] + b.data[i]; return result; }
  friend Matrix operator - (const Matrix &a, const Matrix &b )		{ Matrix<M,N> result;	for (size_t i = 0; i < M*N; i++)	result.data[i] = a.data[i] - b.data[i]; return result; }
  friend Matrix operator - (Matrix a )					{ for (size_t i = 0; i < M*N; i++)	a.data[i] = -a.data[i]; return a; }
  
  template <size_t _I, size_t _J, size_t _K> 	
  friend Matrix<_I,_K> operator * (const Matrix<_I,_J> &a, const Matrix<_J,_K> &b );
  
  friend Vector<M> operator * (const Matrix &a, const Vector<N> &b )
  {
    Vector<M> r;
    for (size_t i = 0; i < M; i++) {
      r[i] = a.data[N*i] * b[0];
      for (size_t j = 1; j < N; j++)	r[i] += a.data[N*i+j] * b[j];
    }
    return r;
  }
  
};

template <size_t M, size_t N> static inline Matrix<N, M> transpose(const Matrix<M,N> &a )
{
  Matrix<N, M> result;
  for (size_t i = 0; i < M; i++)
    for (size_t j = 0; j < N; j++)	result.data[M*j+i] = a.data[N*i+j];
  return result;
}

template <size_t M, size_t N, size_t O> static inline Matrix<M,O> operator * (const Matrix<M,N> &a, const Matrix<N,O> &b )
{ 
  Matrix<M, O> result;	
  result.zero();
  for (size_t i = 0; i < M; i++)	
    for (size_t j = 0; j < O; j++) {
      result.data[O*i+j] = a.data[i*N] * b.data[j];
      for (size_t k = 1; k < N; k++)	result.data[O*i+j] += a.data[N*i+k] * b.data[O*k+j];
    }
  
  return result;
}

template <size_t M, size_t N> static inline std::ostream &operator << (std::ostream &stream, const Matrix<M,N> &m )
{ 
  stream << M << 'x' << N << ":\n";
  for (size_t i = 0; i < M; i++)	stream << Vector<N>(m[i]) << std::endl;	
  return stream; 
}

template <size_t M, size_t N> static inline Matrix<M,N> Identity()
{
  Matrix<M,N> m;
  m.identity();
  return m;
}

#endif /* matrix_hh */
