#ifndef tclcall_h
#define tclcall_h

#include "element.h"

class TCLCALL : public ELEMENT {

  std::string script;
  int correct_offset;

  void step_in(BEAM *beam ) { if (correct_offset) ELEMENT::step_in(beam); }
  void step_out(BEAM *beam ) { if (correct_offset) ELEMENT::step_out(beam); }

public:

  explicit TCLCALL(const char * = NULL );
  TCLCALL(int &argc, char **argv ) : ELEMENT(), correct_offset(0)
  {
    attributes.add("script_name", OPT_STRING, &script);
    attributes.add("correct_offset", OPT_INT, &correct_offset);
    set_attributes(argc, argv);
  }
  
  void step_4d_0(BEAM* );
  void step_4d(BEAM *beam ) { step_4d_0(beam); }

  void step_6d_0(BEAM *beam ) { step_4d_0(beam); }
  void step_6d(BEAM *beam ) { step_4d_0(beam); }

  void set_script(const char *str) { script = str; }
  bool is_tclcall() const { return true; }
 
  
};

#endif /* tclcall_h */
