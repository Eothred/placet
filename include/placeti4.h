#ifndef placeti4_h
#define placeti4_h

void train_bin_fill(BEAMLINE *beamline,
		    BEAM *bunch0,BEAM *workbunch,BIN **bin,int bin_number);
extern int bump_long;
void bump_calculate(BUMP *bump,BEAM *bunch,double s[]);
void simple_correct_emitt(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			  BEAM *workbunch);
void simple_correct_dipole_emitt(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
				 BEAM *workbunch);
void simple_correct(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		    BEAM *workbunch);
void simple_correct_range(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			  BEAM *workbunch,int ipos,int end);
void simple_correct_nlc(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			BEAM *workbunch,BEAM *probe);
void simple_correct_nlc_0(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			  BEAM *workbunch);
void simple_correct_nlc_jitter(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *beam0,
			       BEAM *beam1,BEAM *workbeam,BEAM* probe);
void simple_correct_0(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		      BEAM *workbunch);
void simple_correct_dipole_0(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			     BEAM *workbunch);
void test_quadrupole_jitter(BEAMLINE *beamline,BEAM *bunch,double a0,double a1,
			    int nstep,int nk,char *name);
void test_quadrupole_sens(BEAMLINE *beamline,BEAM *bunch,char *name);
void free_bin_fill(BEAMLINE *beamline,double *quad0,double *quad1,double *quad2,
		   BEAM *bunch0,BEAM *workbunch,BIN **bin,int bin_number);
void free_correct(BEAMLINE *beamline,double *quad0,double *quad1,double *quad2,
		  BIN **bin,int bin_number,BEAM *bunch0,BEAM *workbunch,
		  int do_emitt);
void free_correct_jitter(BEAMLINE *beamline,double *quad0,double *quad1,
			 double *quad2,BIN **bin,int bin_number,BEAM *bunch0,
			 BEAM *bunch1,BEAM *workbunch,int do_emitt);
void
measured_bin_fill_new(BEAMLINE *beamline,double *quad0,double *quad1,
		      double *quad2, BEAM *b0,BEAM *b1,BEAM *b2,BEAM *workbunch,
		      double gl0[],double gl1[],double gl2[],
		      BIN **bin,int bin_number,double g1,double g2);
void measured_correct(BEAMLINE *beamline,double *quad0,double *quad1,
		      double *quad2,
		      BIN **bin,int bin_number,BEAM *b0,BEAM *b1,BEAM *b2,
		      BEAM *workbunch,int do_emitt,double jitter);
void measured_correct_n(BEAMLINE *beamline,double *quad0,double *quad1,
			double *quad2,BIN **bin,int bin_number,BEAM *b0,BEAM *b1,
			BEAM *b2,BEAM *workbunch,int do_emitt,double sigma);
void measured_correct_jitter(BEAMLINE *beamline,double *quad0,double *quad1,
			     double *quad2,BIN **bin,int bin_number,BEAM *b0,
			     BEAM *b0_1,BEAM *b1,BEAM *b1_1,BEAM *b2,
			     BEAM *b2_1,BEAM *workbunch,int do_emitt,
			     double g1,double g2);
void measured_correct_jitter_n(BEAMLINE *beamline,double *quad0,double *quad1,
			       double *quad2,BIN **bin,int bin_number,BEAM *b0,
			       BEAM *b0_1,BEAM *b1,BEAM *b1_1,BEAM *b2,
			       BEAM *b2_1,BEAM *workbunch,int do_emitt,
			       double sigma);
void bpm_reset(BEAMLINE *beamline,BEAM *bunch);
void simple_correct_jitter(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
			   BEAM *bunch1,BEAM *workbunch,int do_emitt);
void simple_correct_dipole_jitter(BEAMLINE *beamline,BIN **bin,int nbin,
				  BEAM *bunch0,BEAM *bunch1,BEAM *workbunch,
				  int do_emitt);
void simple_correct_jitter_0(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
			     BEAM *bunch1,BEAM *workbunch);
void simple_correct_dipole_jitter_0(BEAMLINE *beamline,BIN **bin,int nbin,
				    BEAM *bunch0,BEAM *bunch1,BEAM *workbunch);
void test_no_spectrum(BEAMLINE *beamline,BEAM *bunch0,double l0,double l1,
		      int nstep,char name[],int do_girder);
void test_simple_spectrum(BEAMLINE *beamline,BIN **bin,int nbin,
			  BEAM *bunch0,double lambda1,double lambda2,int nstep,
			  char *file_name);
void errors_init();
void align_rf(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,BEAM *tb,
	      BEAM *workbunch,BEAM *probe);
void beamline_set_zero(BEAMLINE *beamline);
void beamline_survey_earth(BEAMLINE *beamline);
void beamline_survey_clic(BEAMLINE *,int,int);
void beamline_survey_none(BEAMLINE *beamline);
void beamline_survey_inject(BEAMLINE *);
void beamline_survey_inject2(BEAMLINE *);
void survey_init_atl(double);
void beamline_survey_atl(BEAMLINE *);
void beamline_survey_atl_move(BEAMLINE *);
void beamline_survey_atl_2(BEAMLINE *);
void beamline_survey_atlzero_2(BEAMLINE *);
void beamline_survey_nlc(BEAMLINE *);
void beamline_survey_test_clic(BEAMLINE *);
void beamline_survey_test_nlc(BEAMLINE *);
void bunch_test_plot(BEAM *bunch,char *name);
double test_simple_correction_indep(BEAMLINE *beamline,BIN **bin,int nbin,
				    BEAM *bunch0,BEAM *probe,int niter,
				    void (*survey)(BEAMLINE*),char *name,const char *format,
				    char *name2);
double test_simple_correction_indep_list(BEAMLINE* beamline,BIN** bin,int nbin,
					 BEAM* beam0,BEAM** probe,double*,
					 int nbeam,int niter,
					 void (*survey)(BEAMLINE*),char *name,const char *format,
					 char *name2);
double test_simple_correction(BEAMLINE *beamline,BIN **bin,int nbin,
			      BEAM *bunch0,BEAM *probe,int niter,
			      void (*survey)(BEAMLINE*),char *name,const char *format);
double test_simple_correction_dipole(BEAMLINE *beamline,BIN **bin,int nbin,
				     BEAM *bunch0,BEAM *probe,int niter,
				     void (*survey)(BEAMLINE*),char *name, const char *format);
void bump_correct(BEAMLINE *beamline,BIN *bin[],int nbin,BUMP *bump[],int loop[],
		  BEAM *tb,BEAM *workbunch,BEAM *testbunch,int do_emitt);
void bump_prepare(BEAMLINE *beamline,BIN *bin[],int nbin,BUMP **bump,int loop[],
		  int btype,BEAM *bunch,BEAM *tb,BEAM *workbunch,
		  BEAM *testbunch);
void bump_prepare_types(BEAMLINE *beamline,BIN *bin[],int nbin,BUMP **bump,
			int loop[],int btype[],BEAM *bunch,BEAM *tb,BEAM *workbunch,
			BEAM *testbunch);
void test_simple_alignment(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
			   BEAM* testbeam,
			   int niter,void (*survey)(BEAMLINE*),char *name,const char *format,
			   double w0,double w1,int do_rf,int loop[]);
double test_no_correction(BEAMLINE *beamline,BEAM *bunch0,BEAM *probe,
			  int niter,void (*survey)(BEAMLINE*),char *name,const char *format);
double test_no_correction_2(BEAMLINE *beamline,BEAM *bunch0,BEAM *probe,
			    int niter,int init_emit,void (*survey)(BEAMLINE*),
			    char *name,const char *format, char *name2);
double test_measured_correction(BEAMLINE *beamline,int start,int end,
				double quad0[],double quad1[],
				double quad2[],BIN **bin,
				int nbin,BEAM *bunch0,
				BEAM *bunch1,BEAM *bunch2,
				BEAM *cbunch0,BEAM *cbunch1,BEAM *cbunch2,
				double gl0[],double gl1[],double gl2[],int nc,
				double g1,double g2,
				int niter,int beamline_niter,int bin_iter,
				void (*survey)(BEAMLINE*),
				double field_error,
				double offset_y,char *name, const char *format );
void bpm_freeze(BEAMLINE *beamline,double sx,double sy);
void bpm_freeze_bunch(BEAMLINE *beamline,double sx,double sy,int ib);
void cav_scatter(BEAMLINE *beamline,double y,double x);
void test_free_correction(BEAMLINE *beamline,
			  double quad0[],double quad1[],
			  double quad2[],BIN **bin,int nbin,
			  BEAM *bunch0,
			  BIN **rf_bin,int nrf,double offset_y,
			  int niter,void (*survey)(BEAMLINE*),
			  char *name,const char *format);
double check_autophase(BEAMLINE *beamline,BEAM *bunch0,char *name,const char *format);
void rf_init();
void test_free_correction_new(BEAMLINE *beamline,double quad0[],double quad1[],
			      double quad2[],BIN **bin,int nbin,BEAM *bunch0,
			      BIN **bin2,int nbin2,BEAM *testbeam,double jitter_y,
			      double shift,int niter,int step_iter0,
			      void (*survey)(BEAMLINE*),char *name,const char *format);
void bump_correct_long(BEAMLINE *beamline,BIN *bin[],int nbin,BUMP *bump[],
		       int loop[],BEAM *tb,BEAM *workbunch,BEAM *testbunch,
		       int do_emitt);
void test_free_correction_dipole(BEAMLINE *beamline,BIN **bin,int nbin,
				 BEAM *bunch0,BEAM *bunch1,BEAM *testbeam,
				 int niter,void (*survey)(BEAMLINE*),
				 char *name,const char *format);
void test_train_correction(BEAMLINE *beamline,
			   BIN **bin,int nbin,
			   BEAM *bunch0,
			   BIN **rf_bin,int nrf,double offset_y,
			   int niter,void (*survey)(BEAMLINE*),
			   char *name,const char *format);

#endif // placeti4_h
