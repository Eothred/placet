#ifndef ground_h
#define ground_h

#include <map>
#include <string>
#include <vector>

//class FILTER;
class GIRDER;
class GROUND_DATA;

/** Ground motion file based on work and a FORTRAN file by A. Seryi


-----------------------------------------------------------------------
                                  MODEL                           
                         version October 1999                      
                       Seryi@SLAC.Stanford.EDU                   
-----------------------------------------------------------------------

  conversion to C:   f2c -r8 model.f    (-r8 promote real to double)

-----------------------------------------------------------------------
        Computes horizontal x(t,s) and vertical y(t,s)       
        position of the ground at a given time t and in a given 
        longitudinal position s, assuming that at time t=0 we had
        x(0,s)=0 and y(0,s)=0.  The values x(t,s) and y(t,s) will 
        be computed using the same power spectrum P(w,k), however
        they are independent. Parameters of approximation of 
        the P(w,k) (PWK) can be chosen to model quiet or noisy
        place.
        Units are seconds and meters.
       
-----------------------------------------------------------------------
                   The program needs the next files              
       "model.data"  (if it does not exist, it will create it)    
        for explanations of parameters see Report DAPNIA/SEA 95-04 
        (should also appear in Phys.Rew.E, in 1 April 1997 issue)  
                                                                  
        Also needs one column file "positions.data" that contains  
        longitudinal positions s of points where we will want to 
        find x(t,s) and y(t,s). Number of lines in this file is 
        therefore the numbers of elements. In FORTRAN version
        this number is limited.

-----------------------------------------------------------------------
                      How it works.
      We assumed that we know power spectrum of ground motion P(w,k).
      Then we assume that using only finite number of  
      harmonics we can model ground motion in some limited range of
      t and s. As one of inputs we have these ranges: minimum and 
      maximum time Tmin and Tmax and minimum and maximum distance
      Smin and Smax. We will define then the range of important
      frequencies wmin to wmax and wave-numbers kmin to kmax.
      Our harmonics we will distribute equidistantly in logarithmic
      sense, that is, for example, k_{i+1}/k_{i} is fixed.
      Number of steps defined Np from the input file, for
      example Np=50. A single harmonic characterized by 
      its amplitude am_{ij}, frequency w_{i}, wave number k_{j}
      and phase phi_{ij}. Total number of harmonics is NpNp.
      The amplitudes of harmonics are defined once at the beginning 
      from integral over surface (w_{i+1}:w_{i})(k_{j+1}:k_{j}) 
      on P(w,k). Phases phi_{ij} are also defined once at the 
      beginning by random choice. The resulting x(t,s) will be 
      given by double sums:
      x(t,s) = 0.5 sum_{i}^{Np} sum_{j}^{Np} am_{ij} * sin(w_{i} t) 
                                                * sin(k_{j} s + phi_{ij})
             + 0.5 sum_{i}^{Np} sum_{j}^{Np} am_{ij} * (cos(w_{i} t)-1)
                                                * sin(k_{j} s + psi_{ij})
      This choise of formula ensures x(t,s) = 0 at t=0.
      The last sinus is presented in the program in other form
      sin(k_{j} s + phi_{ij}) = sin(k_{j} s) cos(phi_{ij}) 
                                     + cos(k_{j} s) sin(phi_{ij})
      So, we store not phases phi_{ij}, but cos(phi_{ij}) and 
      sin(phi_{ij})
      The same for y(t,s) but with different phases phi_{ij}.
*/

class GROUND_DATA_VEC 
{
  // class that holds several GROUND_DATA which have different filters
  // will be changed later, therefore not integrated with GROUND_DATA and no private members

 public: 
  void add_filter(GROUND_DATA* gm, int start, int end){ // girder numbers
    if (start > end || end >= (int)gm_vec.size()) end = gm_vec.size()-1; // gm_vec has #girders +1
    for (; start<=end; start++) {
      gm_vec.at(start)=gm;
    }
  }
  GROUND_DATA_VEC(){};
  void init(GROUND_DATA* gm, int n) {gm_vec=std::vector<GROUND_DATA*>(n,gm);}
  GROUND_DATA* get_ground_data(int i){return gm_vec.at(i);}
 private:
  std::vector <GROUND_DATA*> gm_vec;
};

class GROUND_DATA {

 public:
  GROUND_DATA(char* file,bool do_syst,double t_st,double tstep,double s_st,double l_st,double s_sgn,int s_ab);
 private:
  /// copy constructor (not implemented)
  GROUND_DATA(const GROUND_DATA&);
  /// assignment operator (not implemented)
  GROUND_DATA& operator=(const GROUND_DATA&);
 public:
  ~GROUND_DATA();

  double xpos(double t0,double s0); /// calculate ground position at time=t0 and s=s0 in [m]
  void xpos(double t0,double s0,double ds,double *x,int n); // should be adapted to include filter in [m]
  // // double xpos(double t_e,double s_e,FILTER *filter);
  // // void xpos(double t_e,double s_e,FILTER *filter,double dt,int n,double *x);
  void init(GIRDER* girder, char* filter_file=0,GROUND_DATA* nominal=0);
  double dxpos(int pos, bool filter=true); /// calculate position change at s=pos in [um]; use filtered or unfiltered
  double dxpos_0(); /// calculate position change at s=0.0 in [um]
  int next_iteration(); /// increase iteration
  void move_girder_ends(GIRDER* girder, bool vertical, double scale=1.); // move girder according to GM
  void spectrum(char* name,double dz,double dt)const; /// produce a spectrum output file
  /** methods used by standalone script (therefore public) */
  void start(GROUND_DATA* nominal=0);
  void start_filter(char* filter_file, GROUND_DATA* nominal=0);

  /** PREISOLATOR description
      Large mass to mitigate ground motion for last two quads, QF1 and QD0 (in CLIC) 
      
      Each ground object can only have one PREISOLATOR */

  // could be separate class in future, but for now too much integrated
 private:
  bool preisolator;
  int preisolator_girder;
  std::pair<double*,double*> tf_mnt1QD0;
  std::pair<double*,double*> tf_mnt2QD0;
  std::pair<double*,double*> tf_mnt1QF1;
  std::pair<double*,double*> tf_mnt2QF1;
  /** precalculated amplitudes (k,w), real and imaginary */
  double *cqd0, *sqd0, *cqf1, *sqf1;
  double xpos_qd0_prev,xpos_qf1_prev;

 public:
  /** special methods for preisolator, positions from IP */
  void preisolator_filter(int g, double pos_mnt1, double pos_mnt2, char * mnt1QD0, char * mnt2QD0, char * mnt1QF1, char * mnt2QF1);
  /** change of position of QF1 and QD0 in [um] */
  std::pair<double,double> preisolator_dxpos();
  bool has_preisolator()const{return preisolator;}

 private:
  void constructor_init(); /// method for constructors

  void init_phases();
  void copy_phases(const GROUND_DATA* nominal);
  void set_startpos();
  void apply_freq_cut(double low=0,double high=1e20);

  std::pair<double*,double*> read_filter_file(char* name, bool logscale_x=true, bool logscale_y=false)const;

  /* void reset() {t=0.0; s=0.0; reset0=1;}; */
  void set_gs0(GIRDER *girder); /// store longitudinal girder positions
  void update_time(double time); /// update some constants for new t=time

  double *f,*k,*a; /// list of frequencies, wave-numbers and amplitudes
  double *s0,*c0; /// list of sin/cos of starting random phases for all frequencies and wave-numbers
  double *s1,*c1; /// list of sin/cos of starting random phases for standing waves for all frequencies and wave-numbers
  double *sf,*cf; /// list of sin/cos values of frequency wave for current time (t*f[j])
  double *sk0,*ck0; /// list of sin/cos starting values
  double t; /// current time stored in cf and sf
  double *sk,*ck; /// list of sin/cos values of frequency wave for current time (t*f[j]) for standing waves, stored to reduce cpu
  /// parameters for systematic dominated region (larger time scales)
  /// the harm.*.300 files have only values for the shorter time scales
  bool do_syst; /// (default:false)
  int n_f,n_k,n_ks;  /// number of frequencies (250), wave-numbers (250), wave-numbers for systematic dominated region (larger time scales) (0)
  double *as,*ph_s,*ks;

  bool standing; /// add standing wave part
  /* int reset0; */

  std::vector<double> gs0,x0; /// (girder) beamline longitudinal positions in and its current vertical (horizontal) offset
  /// starting variables, see GroundMotionInit for explanation
  double t_start,t_step,s_start,last_start,s_sign; 
  int s_abs;
  //  std::pair<int,double> iter_x1; // stored x-value at s=0.0, for iter i, stored to reduce cpu
  int iter; /// current iteration number

 public:
  const double * get_c0()const{return c0;}
  const double * get_s0()const{return s0;}
  const double * get_c1()const{return c1;}
  const double * get_s1()const{return s1;}
  const double * get_ph_s()const{return ph_s;}
  double get_gs0(int n)const{return gs0.at(n);}

  void add_filter(char * file_name, GIRDER* girder, char* filter_name, int start, int end, double low_freq=-1.0, double high_freq=-1.0); /// use initial phases of *this
 private:
  GROUND_DATA_VEC gm_filters;
  std::map<std::string, GROUND_DATA*> gm_map; /// map of used filters, key is filename
};

#endif // ground_h
