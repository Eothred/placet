#ifndef andrea_h
#define andrea_h

#include <sys/types.h>
#include <tcl.h>

class BEAM;

namespace Andrea {
  
  extern bool UseDispersionFreeEmittance;
  extern double ReferenceEnergy;
  
  //  extern TrackingModule TrackingCode;
  
  extern double emitt_x_disp_free(const BEAM *bunch );
  extern double emitt_y_disp_free(const BEAM *bunch );
  
  extern int Init(Tcl_Interp *interp );
  
  extern Tcl_Interp *Placet_interp;

}

#endif /* andrea_h */
