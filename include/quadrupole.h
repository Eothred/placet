#ifndef quadrupole_h
#define quadrupole_h

#include "element.h"
#include "function.h"
#include "conversion.h"

class QUADRUPOLE : public virtual ELEMENT {

  QUADRUPOLE() {}

protected:

  double strength;
  double tilt;
  double kn;
  int field;

  void step_in(BEAM *beam );
  void step_out(BEAM *beam );

  void step_4d_sr_0(BEAM*);
  void step_4d_0(BEAM*);
  void step_4d(BEAM*);

  void step_4d_tl(BEAM *beam );
  
  void step_6d_0(BEAM*);
  void step_6d_tl(BEAM *beam );
  
  void step_partial(BEAM* beam, double l, void (ELEMENT::*step_function)(BEAM*));

  KICK get_kick(const PARTICLE &particle ) const
  {
    KICK kick;
    if (geometry.length==0.0) {
      double KL = strength / particle.energy / flags.thin_lens;
      kick.xp = +KL * particle.x;
      kick.yp = -KL * particle.y;
    } else { 
      double k = strength / particle.energy / geometry.length;
      if (k>0.) {
	double ksqrt = sqrt(k);
	kick.xp = sinh(ksqrt*geometry.length/flags.thin_lens) * ksqrt * particle.x;
	kick.yp = -sin(ksqrt*geometry.length/flags.thin_lens) * ksqrt * particle.y;
      } else {
	double ksqrt = sqrt(-k);
	kick.xp = -sin(ksqrt*geometry.length/flags.thin_lens) * ksqrt * particle.x;
	kick.yp = sinh(ksqrt*geometry.length/flags.thin_lens) * ksqrt * particle.y;
      }
    }
    return kick;
  }
  
public:

  QUADRUPOLE(double l, double s ) : ELEMENT(l), strength(s), tilt(0.0), kn(0.0), field(0)
  {
    attributes.add("strength", "Integrated quadrupole strength [GeV/m]", OPT_DOUBLE, &strength, opposite);
    attributes.add("tilt", "Tilt angle [rad]", OPT_DOUBLE, &tilt);
    attributes.add("tilt_deg", "Tilt angle [deg]", OPT_DOUBLE, &tilt, deg2rad, rad2deg);
    attributes.add("Kn", "Multipolar error strength [GeV/m^(type-1)]", OPT_DOUBLE, &kn);
    attributes.add("type", "Multipolar error order (3: sextupole, 4: octupole, ...) [INT]", OPT_INT, &field);
    enable_hcorr("x", 0.0);
    enable_vcorr("y", 0.0);
  }

  QUADRUPOLE(int &argc, char **argv ) : ELEMENT(), strength(0.0), tilt(0.0), kn(0.0), field(0)
  {
    attributes.add("strength", "Integrated quadrupole strength [GeV/m]", OPT_DOUBLE, &strength, opposite);
    attributes.add("tilt", "Tilt angle [rad]", OPT_DOUBLE, &tilt);
    attributes.add("tilt_deg", "Tilt angle [deg]", OPT_DOUBLE, &tilt, deg2rad, rad2deg);
    attributes.add("Kn", "Multipolar error strength [GeV/m^(type-1)]", OPT_DOUBLE, &kn);
    attributes.add("type", "Multipolar error order (3: sextupole, 4: octupole, ...) [INT]", OPT_INT, &field);
    enable_hcorr("x", 0.0);
    enable_vcorr("y", 0.0);
    set_attributes(argc, argv);
  }

  void step_half(BEAM *beam );

  void set_strength(double s ) { strength=s; }
  void scale_strength(double f ) { strength*=f; }
  double get_strength() const { return strength; }

  void set_tilt(double r) { tilt=r; }
  double get_tilt() const { return tilt; }

  void set_kn(double K ) { kn=K; }
  double get_kn() const { return kn; }
  void set_type(int type ) { field=type; }
  int get_type() const { return field; }

  void multipolar_error(BEAM *beam);
  
  Matrix<6,6> get_transfer_matrix_6d(double _energy ) const;

  bool is_quad() const { return true; }

  QUADRUPOLE *quad_ptr() { return this; }
  const QUADRUPOLE *quad_ptr() const { return this; }

  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  
  bool track_with_error(BEAM *beam);

  /**
   * Get quadrupole magnetic field By + iBx = k(x + iy)
   */
  void GetMagField(PARTICLE *particle, double *bfield);

  friend OStream &operator<<(OStream &stream, const QUADRUPOLE &quad )
  {
    return stream << static_cast<const ELEMENT &>(quad)
		  << quad.tilt
		  << quad.strength;
  }
 
  friend IStream &operator>>(IStream &stream, QUADRUPOLE &quad )
  {
    return stream >> static_cast<ELEMENT &>(quad)
                  >> quad.tilt
                  >> quad.strength;
  }

};

#endif /* quadrupole_h */
