#ifndef PLACET_PRINT_H
#define PLACET_PRINT_H
#include <cstdio>
/**
 * @brief Verbosity levels that can be used in placet_fprintf and placet_printf.
 * 
 * Default level is INFO. You can set the verbosity level using input
 * argument "-s, -v, -vv, or -dbg", or with the Verbosity command in a
 * script.
 */
enum VERBOSITY { 
  ALWAYS,
  ERROR,
  WARNING,
  INFO, 
  VERBOSE, 
  VERYVERBOSE, 
  DEBUG 
};

extern VERBOSITY get_verbosity();
extern void set_verbosity(VERBOSITY verbosity_new);

struct PRINT_DATA{
  VERBOSITY verbosity;
  int num_warnings;
  int num_errors;
  bool exit_on_error; /// exists after an error message is printed
};

//void print_char(FILE *,char *,int);

extern int print_copy();
/**
 * @brief fprintf() with verbosity level
 * 
 * Use this instead of fprintf(). If you want to print to normal screen,
 * use verbosity INFO. Other levels include ERROR, WARNING, VERBOSE, 
 * VERYVERBOSE, and DEBUG.
 * @author Andrea Latina
 * @return 0 if string isn't printed, otherwise returns value from fputs().
 * @see VERBOSITY
 */
extern int placet_fprintf(VERBOSITY verbosity, FILE *stream, const char *format, ... );
/**
 * @brief printf() with verbosity level
 * 
 * Use this instead of printf(). If you want to print to normal screen,
 * use verbosity INFO. Other levels include ERROR, WARNING, VERBOSE, 
 * VERYVERBOSE, and DEBUG.
 * @author Andrea Latina
 * @return 0 if string isn't printed, otherwise returns value from fputs().
 * @see VERBOSITY
 */
extern int placet_printf(VERBOSITY verbosity, const char *format, ... );

extern int print_help(char* prog_name);

char *print_double(const char *,double);
char *print_int(const char *,int);

void my_error(char* err_rout);

#endif
