#ifndef histogram_h
#define histogram_h

typedef struct
{
  const char *name;
  int nc,ncmax;
  int type;
  float xmin,xmax,dx_i;
  float* x;
  double* y;
  double *y2;
  long* count;
} HISTOGRAM;

//unused functions in src/obsolete/histogram.cc
void histogram_make(HISTOGRAM *,int ,float ,float ,int ,const char *);
//void histogram_inquire(HISTOGRAM *,int *,float *,float *,int *);
//void histogram_set_channel_y(HISTOGRAM *,int ,double);
//void histogram_set_channel_y2(HISTOGRAM *,int ,double);
//void histogram_set_channel_count(HISTOGRAM *,int ,int);
//double histogram_y(HISTOGRAM *,int);
//double histogram_y2(HISTOGRAM *,int);
//long histogram_count(HISTOGRAM *,int);
//void histogram_compare(HISTOGRAM *,HISTOGRAM *,HISTOGRAM *);
//void histogram_type_copy(HISTOGRAM *,HISTOGRAM *);
//void histogram_reduce(HISTOGRAM *,int);
//void histogram_sub(HISTOGRAM *,HISTOGRAM *,HISTOGRAM *);
//void histogram_store(FILE *,HISTOGRAM *);
//double histogram_sum(HISTOGRAM *);
//void histogram_scale(HISTOGRAM *,double);
//void histogram_density(HISTOGRAM *);
void histogram_integrate(HISTOGRAM *,int);
void histogram_fprint(FILE *,HISTOGRAM *);
//void histogram_print(HISTOGRAM *);
//void histogram_fprint_2(FILE *,HISTOGRAM *,HISTOGRAM *);
//void histogram_make_error(HISTOGRAM *,HISTOGRAM *);
void histogram_add(HISTOGRAM *,float ,float);
//void histogram_fitprint(FILE *,HISTOGRAM *);
int Tcl_Histogram_add(ClientData ,Tcl_Interp *,int ,char **);
int Tcl_Histogram_integrate(ClientData ,Tcl_Interp *,int ,char **);
int Tcl_Histogram_print(ClientData ,Tcl_Interp *,int ,char **);
int Tcl_Histogram_eval(ClientData ,Tcl_Interp *,int ,char **);
int Tcl_Histogram(ClientData ,Tcl_Interp *,int ,char **);
int Histogram_Init(Tcl_Interp *);

#endif // histogram_h
