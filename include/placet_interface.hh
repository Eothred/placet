#ifndef placet_interface_hh
#define placet_interface_hh

#include <tcl.h>

extern int tk_Octave(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,char *argv[]);
extern int tk_Python(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,char *argv[]);

#endif /* placet_interface_hh */
