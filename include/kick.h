#ifndef kick_h
#define kick_h

// A kick, in urad

#include "photon_spectrum.h"

struct KICK {
  double xp; // urad
  double yp; // urad
  double angle0; // the bending angle the reference particle is subjected to in bends [urad]
  KICK() : xp(0.), yp(0.), angle0(0.) {}
  KICK(double _xp, double _yp, double _angle0=0. ) : xp(_xp), yp(_yp), angle0(_angle0) {}
  
  double get_synrad_average_energy_loss(double length, double particle_energy ) const
  {
    double kick = sqrt((xp+angle0)*(xp+angle0)+yp*yp) * 1e-6;
    return SYNRAD::get_average_energy_loss(kick, length, particle_energy);
  }
  
  double get_synrad_mean_free_path(double length, double particle_energy ) const 
  {
    double kick = sqrt((xp+angle0)*(xp+angle0)+yp*yp) * 1e-6;
    return SYNRAD::get_mean_free_path(kick, length, particle_energy);
  }

  double get_synrad_free_path(double length, double particle_energy ) const 
  {
    double kick = sqrt((xp+angle0)*(xp+angle0)+yp*yp) * 1e-6;
    return SYNRAD::get_free_path(kick, length, particle_energy);
  }
  
  double get_synrad_energy_loss(double length, double particle_energy ) const 
  {
    double kick = sqrt((xp+angle0)*(xp+angle0)+yp*yp) * 1e-6;
    return SYNRAD::get_energy_loss(kick, length, particle_energy);
  }
  
  friend KICK operator-(const KICK &a ) { return KICK(-a.xp, -a.yp, -a.angle0); }
  friend KICK operator-(const KICK &a, const KICK &b ) { return KICK(a.xp-b.xp, a.yp-b.yp, a.angle0-b.angle0); }
  friend KICK operator+(const KICK &a, const KICK &b ) { return KICK(a.xp+b.xp, a.yp+b.yp, a.angle0+b.angle0); }

};

#endif
