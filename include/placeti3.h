#ifndef placeti3_h
#define placeti3_h

#include <utility>
#include <vector>

class BEAM;
class BEAMLINE;
class BIN;
struct BUMP;
class ELEMENT;
struct FIELD;
class GIRDER;
struct MAIN_BEAM_PARAM;
struct PARTICLE;
struct PLACET_SWITCH_STRUCT;

double resist_wall_transv(double s);
double resist_wall_long(double s);
double bns(double n,double kick,double l,double mu,double e);
void mkquad(double rk,double rl,double r[]);
void mkdrift(double rl,double r[]);
void matmul(double r1[],double r2[],double r3[]);
int infodo(double rk1,double rl1,double rk2,double rl2,double d,
	   double *alpha1,double *alpha2,double *beta1,double *beta2,
	   double *rmu1,double *rmu2);
int intripl(double rk1,double rk2,double rl1,double rl2,double rl3,double d,
	    double *alpha1,double *alpha2,double *beta1,double *beta2,
	    double *rmu1,double *rmu2);
void field_delete(FIELD *field);
void beam_delete(BEAM *);
FIELD *field_make(int ,int);
void field_set_kick(FIELD *,int ,double);
BEAM *bunch_make_2(int n_bunch,int n_slice,int n_macro,int n_field,int n_max, int n_halo=0);
BEAM *bunch_make(int ,int ,int ,int,int, int n_halo=0);
BEAM *bunch_remake(BEAM *bunch);
double envelope_y(BEAM *beam);
double envelope_x(BEAM *beam);
double beam_rms_y(BEAM *bunch);
double beam_rms_x(BEAM *bunch);
double sigma_y(BEAM *bunch);
double sigma_x(BEAM *bunch);
void bunch_set_sigma_yy(BEAM *bunch,int nb,int slice,int part,
			double beta,double alpha,double eps);
void bunch_set_sigma_yy(BEAM *bunch,int index,
			double beta,double alpha,double eps);
void bunch_set_sigma_xx(BEAM *bunch,int nb,int slice,int part,
			double beta,double alpha,double eps);
void bunch_set_sigma_xx(BEAM *bunch,int index,
			double beta,double alpha,double eps);
void bunch_set_sigma_xy(BEAM *bunch,int nb,int slice,int part);
void bunch_set_sigma_xy(BEAM *bunch,int index);
void bunch_set_slice_energy(BEAM *,int ,int ,int ,double);
void bunch_set_slice_wgt(BEAM *,int ,int ,int ,double);
void bunch_set_slice_y(BEAM *,int ,int ,int ,double);
void bunch_set_slice_yp(BEAM *,int ,int ,int ,double);
void bunch_set_slice_x(BEAM *,int ,int ,int ,double);
void bunch_set_slice_xp(BEAM *,int ,int ,int ,double);
void bunch_rotate_0(BEAM *bunch,double theta);
void bunch_rotate(BEAM *bunch,double theta);
void element_add_offset(ELEMENT *element,double y,double yp);
#ifdef TWODIM
void element_add_offset_x(ELEMENT *element,double x,double xp);
#endif
void element_set_offset_to(ELEMENT *element,double y,double yp);
void bpm_init(int cav_bpm,int all);
GIRDER* make_drift_girder_new(int field);
GIRDER* make_cav_girder_new(double phase);
GIRDER* make_bpm_girder_new(double phase);
GIRDER* make_quad_girder_1_new(double qstr,double phase);
GIRDER* make_quad_girder_2_new(double qstr,double phase);
GIRDER* make_quad_girder_3_new(double qstr,double phase);
GIRDER* make_quad_girder_4_new(double qstr,double phase);
GIRDER* make_quad_girder_1_start(double qstr,double phase);
GIRDER* make_quad_girder_end_new(double qstr);
void quadrupoles_set(BEAMLINE *beamline,double strength[]);
void quadrupoles_bin_set(BEAMLINE *beamline,BIN *bin,double strength[]);
void quadrupoles_bin_set_b(BEAMLINE *beamline,BIN *bin,double strength[],
			   double strength0[]);
void errors_fill(double s[],int n,double sigma,double cut);
void quadrupoles_bin_set_position_error(BEAMLINE *beamline,BIN *bin,
					double position[],int direction);
void quadrupoles_bin_set_position_error_b(BEAMLINE *beamline,BIN *bin,
					  double position[],int direction);
void quadrupoles_bin_set_position_error_b_x(BEAMLINE *beamline,BIN *bin,
				       double position_x[],double position_y[],
					    int direction);
void quadrupoles_bin_set_error(BEAMLINE *beamline,BIN *bin,double strength[],
			       double sigma);
void quadrupoles_bin_set_error_b(BEAMLINE *beamline,BIN *bin,double strength[],
				 double strength0[],double sigma);
double emitt_y_scale(const BEAM *bunch,double scale);
double emitt_y(const BEAM *bunch);
double emitt_y_range(const BEAM *bunch,int n1,int n2,double *off);
double emitt_y_axis_range(const BEAM *bunch,int n1,int n2,double *off);
double emitt_y_axis(const BEAM *bunch);
#ifdef TWODIM
double emitt_x(const BEAM *bunch);
double emitt_x_range(const BEAM *bunch,int n1,int n2,double *off);
double emitt_x_axis_range(const BEAM *bunch,int n1,int n2,double *off);
double emitt_x_axis(const BEAM *bunch);
#endif
void beam_add_offset_y(BEAM *bunch,double y);
void beam_add_offset_x(BEAM *bunch,double x);
void longrange_fill_band_new(BEAM *bunch,double lambda0[],int nfreq);
BEAM *make_multi_bunch(BEAMLINE *beamline,char *name,MAIN_BEAM_PARAM *param);
int bunchinfo_cmp_z(const void *,const void *);
double move_atl(double a,double t,double l);
void move_atl_element(double a,double t,double length,double& y2,double &y,double&yp);
void beamline_move_atl(BEAMLINE *beamline,double a,double t,int move_girders,int apply_x=true,
		       int apply_y=true,int end_fixed=false,int start=-1,int end=-1);
void beamline_move_sine(BEAMLINE *beamline,double a,double k,double phase);
void beamline_move_sine2(BEAMLINE *beamline,double a,double k,double phase);
void beamline_move_sine_list(BEAMLINE *beamline,int nel[],
			     double a,double k,double phase);
void field_read(FIELD **field,double phase[],double scale[],int n,char *string);
void check_kscal(int *kscal,double estep[],double e);
void lattice_data_init(int nsect,int nquad[],int nlong[],int ngirder[]);
void setup_TeV(char *name,char *beam_name,BEAMLINE *beamline);
void smooth_quad(BEAMLINE *beamline,double f,double l,double e);
void smooth_drift(BEAMLINE *beamline,double l);
void smooth_bpm(BEAMLINE *beamline,double l);
void smooth_cav(BEAMLINE *beamline,double l,double phase);
void setup_smooth(char *name,BEAMLINE *beamline);
void beam_copy_0(BEAM *bunch1,BEAM *bunch2);
void bunch_copy_0(BEAM *bunch1,BEAM *bunch2);
void beam_copy(BEAM *,BEAM *);
void beam_copy_sigma(BEAM *,BEAM *);
void bunch_join(BEAM *bunch0,BEAM *bunch1,double scal,BEAM *bunch2);
void bunch_join_0(BEAM *bunch0,BEAM *bunch1,double scal,
		  BEAM *bunch2);
void bunch_join_x(BEAM *bunch0,BEAM *bunchx,BEAM *bunchy,double scal_x,
		  double scal_y,BEAM *bunch2);
void bunch_join_0_x(BEAM *bunch0,BEAM *bunchx,BEAM *bunchy,double scal_x,
		    double scal_y,BEAM *bunch2);
void bns_plot(BEAMLINE *beamline,BEAM *beam,char *name,double charge,double kick);
void bin_response_target(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
			 BIN *bin);
void bin_response(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
		  BIN *bin);
void bin_measure_one_beam(BEAMLINE *beamline,BEAM *bunch,BIN *bin);
void bin_response_train(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
			BIN *bin);
void bin_response_dipole(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
			 BIN *bin);
void bin_response_b(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch1,BEAM *bunch,
		    int flag,BIN *bin);
void bin_response_b_calc(BEAMLINE *beamline,BEAM *beam0,BEAM *beam1,BEAM *beam,
			 int flag,BIN *bin);
void bin_response_b_calc_x(BEAMLINE *beamline,BEAM *beam0,BEAM *beamx,BEAM *beamy,
			   BEAM *beam,int flag,BIN *bin);
void bin_response_zero(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
		       BIN *bin);
void bin_measure(BEAMLINE *beamline,BEAM *bunch,int flag,BIN *bin, bool track=true, bool noacc=false);
void bin_read_measure(BEAMLINE *beamline,BEAM *bunch,int flag,BIN *bin);
void twiss_write_header(FILE *file);
void twiss_plot(BEAMLINE *beamline,BEAM *bunch0,char *name,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int), int *list=NULL, int nlist=0 );
void dispersion_plot(BEAMLINE *beamline,BEAM *bunch,char *name);
double energy_spread(BEAM *bunch);
double bunch_energy(BEAM *bunch);
double energy_spread_rms(BEAM *bunch);
void energy_spread_plot_header(FILE *file);
void energy_spread_plot(BEAMLINE *beamline,BEAM *beam,char *name,int header);
int ludcmp(double a[],int n,int indx[],double *d);
int lubksb(double a[],int n,int indx[],double b[]);
void hessian_0(double a0[],int nq,int nbpm,double pwgt,double a[]);
void right_side_new_0(double a0[],int nq,int nbpm,double bs0[],double t0[], double xr[]);
void hessian(double a0[],double a1[],double a2[],int nq,int nbpm,double pwgt,
	     double w0,double w,double a[]);
void correct_0(double a[],int indx[],double a0[],int nq,int nbpm,double bpm0[],
	       double pwgt,double qcorr[]);
void corr_init();
void correct(double a[],int indx[],double a0[],double a1[],double a2[],int nq,
	int nbpm,double w,double bpm0[],double bpm1[],double bpm2[],
	     double pwgt,double qcorr[]);
void bin_correct(BEAMLINE *beamline,int flag,BIN *bin);
void bin_correct_step(BEAMLINE *beamline,int flag,BIN *bin,double gain);
void bin_correct_nlc(BEAMLINE *beamline,int flag,BIN *bin);
void bin_finish(BIN *bin,int flag);
void bin_finish_nlc(BIN *bin,int flag);
void print_position_bin(FILE *file,BEAMLINE *beamline,BIN *bin);
void bin_print(FILE *file,BEAMLINE *beamline,BIN *bin,int n);
void beamline_bin_divide(BEAMLINE *beamline,int nq,int interleave,
			 BIN **bin,int *bin_number);
void beamline_bin_divide_limit(BEAMLINE *beamline,int start,int end,int nq,
			       int interleave,BIN **bin,int *bin_number);
void beamline_bin_divide_limit_correctors(BEAMLINE *beamline,int start,int end,
					  int *correctors, int ncorrector, int nq,
					  int interleave,BIN **bin,int *bin_number);
void beamline_bin_divide_dipole(BEAMLINE *beamline,int nq,int interleave,
				BIN **bin,int *bin_number);
void beamline_bin_divide_ballistic(BEAMLINE *beamline,int nq,int interleave,
				   BIN **bin,int *bin_number);
void beamline_bin_divide_1(BEAMLINE *beamline,int nq,int interleave,
			   BIN **bin,int *bin_number);
void beamline_bin_divide_interleave(BEAMLINE *beamline,BIN **bin,int *bin_number);
void beamline_bin_divide_dipole_interleave(BEAMLINE *beamline,BIN **bin,
					   int *bin_number);
void beamline_bpm_switch_off(BEAMLINE *beamline,int sign);
void simple_bin_fill(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		     BEAM *workbunch);
void simple_bin_fill_dipole(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			    BEAM *workbunch);
void simple_bin_fill_b(BEAMLINE *beamline,double quad[],double quad0[],BIN **bin,
		       BIN **bin1,int nbin,BEAM *bunch,BEAM *workbunch);
void simple_bin_fill_nlc(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			 BEAM *workbunch);
void feedback_correct_3(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			BEAM *workbunch,double dist);
void bump_init(int i,int quad_dist,int emitt_dist);
BUMP* bump_make(int n_slices);
void bump_set(BEAMLINE *beamline,BUMP *bump,int which,double val);
void bump_step(BEAMLINE *beamline,BUMP *bump,BEAM *bunch,BEAM *workbunch);
void bump_step_emitt(BEAMLINE *beamline,BUMP *bump,BEAM *bunch,BEAM *workbunch);
void bump_fill(BEAMLINE *beamline,BUMP *bump,BEAM *bunch0,BEAM *workbunch,
	       BEAM *testbunch);
void bump_define_cav(BEAMLINE *beamline,BUMP *bump,int cav);
void bump_define_quad(BEAMLINE *beamline,BUMP *bump,int cav);
void bump_define(BEAMLINE *beamline,BUMP *bump,int cav,int flag);
int indoublet(double rk1,double rk2,double rl1,double rl2,double d1,double d2,
	      double *alpha1,double *alpha2,double *beta1,double *beta2,
	      double *rmu1,double *rmu2);
void bin_finish_new(BIN *bin,int flag);
void bump_step_meas(BEAMLINE *beamline,BUMP *bump,BIN **bin,int nbin,
		    BEAM *bunch,BEAM *workbunch);
void bump_fill_long(BEAMLINE *beamline,BUMP *bump,BIN **bin,int nbin,
		    BEAM *bunch0,BEAM *workbunch,BEAM *testbunch);

extern PLACET_SWITCH_STRUCT placet_switch;

#endif
