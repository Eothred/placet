#ifndef slices_to_particles_h
#define slices_to_particles_h

#include <tcl.h>

#include "element.h"

class BEAM;

class S2P : public ELEMENT {

  //  int seed; // Seed to be used for transforming slices to rays. ?->If not set no reset will be performed, to change the seed use RandomReset, streamname Select
  int dist; // If 1 the particles distribution in energy and z comes from continuous slices

  void step_in(BEAM *) {}
  void step_out(BEAM *) {}

public:
  S2P(int &argc, char **argv) : ELEMENT(),dist(1) {
    /* attributes.add("seed", "Seed to be used for transforming slices to rays. If not set no reset will be performed, to change seed use RandomReset, streamname Select", OPT_INT, &seed); */
    attributes.add("type", "If 1 the particles distribution in energy and z comes from continuous slices", OPT_INT, &dist);
    set_attributes(argc,argv);
  }
  
  void step_4d(BEAM*); 
  void step_4d_0(BEAM*);
  void step_6d(BEAM *beam ) { step_4d(beam); }
  void step_6d_0(BEAM *beam ) { step_4d_0(beam); }

  void list(FILE*)const;
  
  void set_dist(int d) { dist=d; }
};

void slices_to_particles(BEAM *b,int dist);

int
tk_SlicesToParticles(ClientData clientdata,Tcl_Interp *interp,int argc,
		     char *argv[]);

#endif /* slices_to_particles_h */
