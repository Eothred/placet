#ifndef girder_h
#define girder_h

#include "placet.h"
#include "beamline.h"

class ELEMENT;

/**
 * @class GIRDER
 * Definition of the GIRDER class on which ELEMENTS are put.
 *
 * 'classified' by combining several standalone functions
 * @author Jochem Snuverink
 */

class GIRDER{
 public:
  explicit GIRDER(double length=0); /// constructor

  /// returns the length of the GIRDER
  double get_length() const {return length=calc_length();}
  /// returns a pointer to the first element
  ELEMENT* element()const{return first_element;}
  /// returns a pointer to the next girder in the beamline
  GIRDER* next()const{return next_girder;}
  
  /// add element at the end
  void add_element(ELEMENT *element);
  /// add element at the end at length z from begin
  void add_element(ELEMENT *element,double z);
  /// insert element at position z
  void insert_element(ELEMENT *element,double z);
  /// offset its elements
  void move_ends(double x1,double x2,double y1 ,double y2)const;
  /// offset its cavities, but not its other elements
  void move_ends_cav(double x1,double x2,double y1,double y2)const;

  /// offsets all elements in y
  void move(double dy,double dyp)const;
  /// offsets only the cavities in y
  void move_cav(double dy,double dyp)const;
#ifdef TWODIM
  /// offsets all elements in x
  void move_x(double dx,double dxp)const;
  /// offsets only the cavities in x
  void move_cav_x(double dx,double dxp)const;
#endif

  /// set first element
  void set_first_element(ELEMENT* element){first_element=element;} // preferably not needed
  /// returns distance to previous girder (there can be (drift) space in between girders)
  double distance_to_prev_girder()const{return dist;}
  /// prints girder length to FILE
  void print(FILE *file)const;

 private:
  /// calculates length of girder, used internally
  double calc_length() const;
  /// set distance to previous girder
  void set_distance_to_prev_girder(double d){dist=d;}
  /// offset elements on the girder,internally used by move_ends and move_ends_cav
  void move_ends(double x1,double x2,double y1 ,double y2,bool cav_only)const;
  void move(double dy,double dyp, bool cav_only)const;
#ifdef TWODIM
  void move_x(double dx,double dxp, bool cav_only)const;
#endif

  /// data members:

  /// length of GIRDER
  mutable double length;
  /// pointer to next GIRDER
  GIRDER *next_girder;
  /// pointer to first ELEMENT
  ELEMENT *first_element;
  /// distance to previous girder
  double dist;

  /// friend method that adds girder to beamline
  friend void BEAMLINE::girder_add(GIRDER*,double dist);
};

#endif /* girder_h */
