#ifndef step_h
#define step_h

class BEAM;
class BEAMLINE;

void
fprint_twiss(FILE *file,BEAM *beam,int j,double s,int n1,int n2);
void
twiss_plot_step(BEAMLINE *beamline,BEAM *beam0,char *name,double step,int n1,int n2,
		void(*fprint_twiss)(FILE*,BEAM*,int,double,int,int),int *list=NULL, int nlist=0);

#endif //step_h
