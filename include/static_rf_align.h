#ifndef align_rf_gider_h
#define align_rf_gider_h

class BEAMLINE;
class BIN;
class BEAM;

enum GIRDER_ALIGNMENT {
  NONE,
  PER_GIRDER,
  PER_BIN
};

extern void align_rf_girder(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0, BEAM *tb, BEAM *workbunch,BEAM *probe, int girder_alignment=PER_GIRDER);
extern void inter_girder_move(BEAMLINE * /*beamline*/,double ampl_y,double ampl_x, double flo_y,double flo_x,int cav_only);
extern void scatter_girder(BEAMLINE *beamline,double ampl_y,double ampl_yp, double ampl_x,double ampl_xp,int cav_only);

extern int tk_TestRfAlignment(ClientData clientdata, Tcl_Interp *interp, int argc, char *argv[]);

#endif /* static_rf_align_h */
