#ifndef particles_to_slices_h
#define particles_to_slices_h

#include <cstdio>
#include <tcl.h>

#include <limits>

#include "element.h"

class BEAM;

struct P2S : public ELEMENT {

  bool read_structure;
  bool center; 
 
  P2S(bool _read_structure=false, bool _center=false, double e0=-1)
  {
    read_structure=_read_structure;
    center=_center;
    if (e0!=-1) ref_energy=e0;
  }
  
  void list(FILE* f)const { fprintf(f,"ParticlesToSlices\n"); }
  
  void step_4d_0(BEAM* beam);
  void step_4d(BEAM* beam)
  {
    puts("Warning ParticlesToSlices used on a sliced beam\n");
    step_4d_0(beam);
  }

};

extern int tk_ParticlesToSlices(ClientData clientdata, Tcl_Interp *interp, int argc, char *argv[]);

#endif /* particles_to_slices_h */
