#ifndef powell_h
#define powell_h

class POWELL {

 private:
  
  struct {
    double pcom[50], xicom[50];
    int ncom;
    /// function that will be minimised
    double (*func)(double *);
  } f1com;
  
  /** Table of constant values */
  const double c_b13;
  
 public:
  /// constructor
  explicit POWELL(double (*powell_func)(double*)):f1com(),c_b13(1e-4){
    f1com.func=powell_func;
  }
    
  void powell(double *,double *,int ,int ,double ,int *,double *);
    
 private:

  double r_sign(double,double);
  double brent(double,double,double,double(POWELL::*)(double),double,double *);
  double f1dim(double);
  void mnbrak(double *,double *,double *,double *,double *,double *,
	      double (POWELL::*)(double));
  int linmin(double *,double *,int *,double *);
};

#endif
