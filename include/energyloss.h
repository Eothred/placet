#ifndef energyloss_h
#define energyloss_h

#include <tcl.h>

#include <gsl/gsl_rng.h>

#include "element.h"

class ENERGYLOSS : public ELEMENT {

	double sigma;
	gsl_rng *rng;

public:

	explicit ENERGYLOSS(double _sigma = 0 );
	~ENERGYLOSS() { gsl_rng_free(rng); }

	void step_4d_0(BEAM *beam );
	void step_4d(BEAM *beam) { step_4d_0(beam); }
	
};

extern int tk_EnergyLoss(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[]);

#endif /* energyloss_h */
