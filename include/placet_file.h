#ifndef PLACET_FILE_H
#define PLACET_FILE_H
#include <cstdio>

/** 
    open file function
    If a file with the same name already exists, its contents are discarded and the file is treated as a new empty file.
    option for binary file format (false by default)
    if name==NULL, stdout is used
 */
FILE* open_file (const char* name, bool binary=false);
/** 
    append file function
    If a file with the same name already exists, its contents are kept and new contents is stored at the end
    option for binary file format (false by default)
    if name==NULL, stdout is used
 */
FILE* append_file (const char* name, bool binary=false);
/** 
    read file method
    option for binary file format (false by default)
    if name==NULL, stdin is used
*/
FILE* read_file (const char* name, bool binary=false);
/** 
    close file function
    safety check on file!=stdout,stdin,stderr included
*/
void close_file (FILE* file);

/* methods used in peder.cc */

/** skip rest of word (until space, tab or end of line) */
void skipword(FILE *f);
/** skip rest of current line */
void skipcurrentline(FILE *f);

/* methods used in position.cc */

/** close file f with name fname and print error message */
void file_too_short(FILE* f, char* fname);
/** read next n doubles into array d from file f with name fname */
bool file_read_doubles(FILE* f, char* fname, double* d, unsigned int n);

#endif
