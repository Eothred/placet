#ifndef help_h
#define help_h

#include <vector>
#include <string>
#include <cstring>
#include <tcl.h>

#include "manual.h"

struct COMMAND_HELPER {

  std::string name;
  MANUAL::CATEGORY category;
  MANUAL::SUBCATEGORY subcategory;
  std::string texinfo_description;
  
  COMMAND_HELPER() {}
  COMMAND_HELPER(Tcl_Interp *_interp, const char *_name, MANUAL::CATEGORY _category, MANUAL::SUBCATEGORY _subcategory, const std::string &texi_desc = "" );
  COMMAND_HELPER(Tcl_Interp *_interp, const char *_name, const std::string &texi_desc = "" );
  COMMAND_HELPER(const char *_name ) : name(_name) {}
  
  static Tcl_Interp *interp;
  static std::string texi_header() 
  {
    return "\\input texinfo   @c -*-texinfo-*-";
  }
  
  static std::string texi_footer()
  {
    return "@bye";
  }
    
  void print_help_message() const;

  std::string get_help_texinfo() const;

  void display_help_text() const;
  

  bool operator == (const char *str ) const
  {
    return strcmp(name.c_str(), str) == 0;
  }

  MANUAL::CATEGORY_KEY get_category_key() const { return MANUAL::CATEGORY_KEY(category,subcategory); }

};

extern std::vector<COMMAND_HELPER> help_head;

extern void display_help_text (std::ostream& os, const std::string& msg);

#endif /* help_h */
