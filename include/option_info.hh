#ifndef option_info_h
#define option_info_h

#include <limits>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <map>
#include <string>

#include "matrixnd.hh"

#include <getopt.h> // see e.g. www.opensource.apple.com/source/bc/bc-21/bc/h/getopt.h

class option_set;

enum option_type {
  OPT_INT,
  OPT_UINT,
  OPT_BOOL,
  OPT_DOUBLE,
  OPT_STRING,
  OPT_CHAR_PTR,
  OPT_COMPLEX,
  OPT_MATRIX,
  OPT_NONE
};

struct option_helper {};

typedef void (option_helper::*Set)();
typedef void (option_helper::*Set_int)(int );
typedef void (option_helper::*Set_uint)(size_t );
typedef void (option_helper::*Set_bool)(bool );
typedef void (option_helper::*Set_double)(double );
typedef void (option_helper::*Set_matrix)(MatrixNd );
typedef void (option_helper::*Set_string)(const char * );
typedef void (option_helper::*Set_char_ptr)(const char * );
typedef void (option_helper::*Set_complex)(std::complex<double> );

typedef void (option_helper::*Get)() const;
typedef int (option_helper::*Get_int)() const;
typedef size_t (option_helper::*Get_uint)() const;
typedef bool (option_helper::*Get_bool)() const;
typedef double (option_helper::*Get_double)() const;
typedef MatrixNd (option_helper::*Get_matrix)() const;
typedef std::string (option_helper::*Get_string)() const;
typedef const char *(option_helper::*Get_char_ptr)() const;
typedef std::complex<double> (option_helper::*Get_complex)() const;

typedef double (*double_unary_function)(double );

/* 
   The option_info class gives set and get access from outside to variables inside a class.
   In PLACET it is used for the members (attributes) of the ELEMENT classes.

   It inherits from the GNU option struct from getopt.h 
   // see e.g. www.opensource.apple.com/source/bc/bc-21/bc/h/getopt.h
 */

class option_info : private option { 
  // private inheritance, as 'name' should not be changed!

  // only option_set needs access to the internals of the base class (when parsing the args)
  friend class option_set;

  union { // destination, to variable or object
    void* var_ptr;
    option_helper* obj_ptr;
  };

  // struct definition that holds the information how to access and set the variable
  struct option_access {
    const char* name; // string that holds the name of the variable, currently copy of option::name, but could be changed into a mangled name
    const char* description; // string which stores the description of the variable
    option_type type; // type of the variable (int, bool, string, etc.)

    // definition of structs for different access methods (no union as object_access twice as large)
    struct variable_access {
      double_unary_function set;
      double_unary_function get;
    };
    struct object_access {
      Set set;
      Get get;
    };

    union {
      variable_access* var_access; // stored as pointer as set/get functions often not stored (pointer is 0 in that case)
      object_access* obj_access;
    };
    bool variable_ptr; // is the info accessible directly on the variable (true) or via its object (false)
  };

   // option_access: store pointer or do lookup in map? cpu (? % increase) vs memory (20% increase) -> depends how often we need the information --> lookup for a map with key 'name' should be fast (O(log n)). In principle lookup could be benificial, but needs quite some work to avoid duplication of names (relying on pointer values is not a good idea in general, so name mangling should be introduced. For now simply storing the pointer is chosen.
  const option_access* access;
  //  const option_access* access()const {return ((*option_access_map.find(name)).second);} // pointer should always valid

  // Definition of static map that checks if a new option_access object should be built, or an existing one can be used. It counts the number of instances (useful for garbage collection)

  // order function for map
  struct keyCompare{
    // function to order option_access pointers, by name, type, variable_ptr, set, get methods
    bool operator ()(const option_access& a, const option_access& b) const {
      int compare = strcmp(a.name,b.name);
      if (compare!=0) {return compare<0;} // returns true if a < b
      if (a.type != b.type) {return a.type<b.type;}
      if (a.variable_ptr != b.variable_ptr) {return a.variable_ptr;}
      if (a.variable_ptr) {
	if (a.var_access && b.var_access) {
	  if (a.var_access->set != b.var_access->set) {return a.var_access->set<b.var_access->set;}
	  if (a.var_access->get != b.var_access->get) {return a.var_access->get<b.var_access->get;}      
	}
	else if (a.var_access) {return true;}
      }
      else {
	if (a.obj_access && b.obj_access) {
	  // no < operator defined for option_helper::Set/Get, check on function pointer
	  if (a.obj_access->set != b.obj_access->set) {return &(a.obj_access->set) < &(b.obj_access->set);}
	  if (a.obj_access->get != b.obj_access->get) {return &(a.obj_access->get) < &(b.obj_access->get);}
	}
	else if (a.obj_access) {return true;}
      }
      return false; // if equal return false
    }
  };

  typedef std::map<const option_access, int, keyCompare> AccessMap;
  typedef std::map<const option_access, int, keyCompare>::iterator AccessMapIt;
  static AccessMap option_access_map; 

  // add / remove methods to map
  // like map::insert add() returns an iterator to the mapped element and a bool to check if the element was added (true) or already present (false)
  std::pair<AccessMapIt,bool> add(const option_access&)const;
  void remove(const option_access&)const;

 public:
  // read-only access to name
  const char* get_name()const{return name;}

  const char* description()const{return access->description;}
  option_type type()const{return access->type;}

  std::string get_type_name() const
  {
    switch(access->type) {
    case OPT_INT:      return "integer"; 
    case OPT_BOOL:     return "boolean";
    case OPT_UINT:     return "unsigned integer";
    case OPT_DOUBLE:   return "double";
    case OPT_MATRIX:   return "matrix";
    case OPT_STRING:   return "string";
    case OPT_COMPLEX:  return "complex";
    default:           return "[undefined]";
    }
  }

  explicit
  option_info(const char *_name=NULL, option_type _type=OPT_NONE, void *_dest=NULL, double_unary_function func=NULL, double_unary_function ifunc=NULL );
  option_info(const char *_name, option_type _type, option_helper *_object, Set _set, Get _get);
  option_info(const char *_name, const char *_description, option_type _type=OPT_NONE, void *_dest=NULL, double_unary_function func=NULL, double_unary_function ifunc=NULL );
  option_info(const char *_name, const char *_description, option_type _type, option_helper *_object, Set _set, Get _get);
  
  // copy constructor
  option_info(const option_info&);
  // assignment operator
  option_info& operator=(const option_info&); 
  // destructor
  ~option_info(){if(access) remove(*access);} // decreases option_access counter from map

  int get_int() const;
  size_t get_uint() const;
  bool get_bool() const;
  double get_double() const;
  MatrixNd get_matrix() const;
  std::string get_string() const;
  const char *get_char_ptr() const;
  std::complex<double> get_complex() const;
  std::string get_value_as_string() const;
  
  void set(int value );
  void set(size_t value );
  void set(bool value );
  void set(double value );
  void set(const char *ptr );
  void set(const MatrixNd &matrix );
  void set(const std::string &string );
  void set(const std::complex<double> &value );
  void set_value_from_string(const std::string &value );
};

#endif /* option_info */
