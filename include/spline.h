#ifndef _h_spline
#define _h_spline

#include <tcl.h>

/**
 * @brief a cubic spline definition
 *  
 * This class is a cubic spline interpolation class

 * see e.g.
 * http://en.wikipedia.org/wiki/Spline_(mathematics)
 * 
 * The implemented method is from Numerical Recipes by W.H. Press et al.
 * 3.3 Cubic Spline Interpolation
 * It is a 'natural' spline, which has zero derivates at the end points.
 *
 * @author Daniel Schulte <Daniel.Schulte@cern.ch>
 *
 * object-oriented by Jochem Snuverink.
 */

class SPLINE {

  /// helper class that stores one spline point
  class spline_tab_entry {
  public:
    /// input value
    double x;
    /// output value
    double y;
    /// second derivative
    double y2;
  };

 public:
  /** 
   * @brief constructor
   *
   * @param x array of input values
   * @param y array of output values
   * @param xscal scale (0: linear, 1: log)
   * @param yscal scale (0: linear, 1: log)
   */
  SPLINE(double x[],int xscal,double y[],int yscal,int n);
  /// destructor
  ~SPLINE();

  /// eval function
  double eval(double);

  /**
   * @brief get table of evaluations with a linear range
   *
   * @param[out] y   list of evaluations
   * @param[in]  x0  start point of table
   * @param[in]  dx  step
   * @param[in]  n   number of evaluations
   */
  
  void get_table(double x0,double dx,double y[],int n);

 private:
  /// number of points, and scale (0: linear, 1: log)
  int n,xscal,yscal;
  /// spline implementation: array of spline tab entries
  spline_tab_entry *tab;

  /// set second derivative, called by constructor
  void sety2();

  /// private default constructor (not implemented)
  SPLINE();
  /// private copy constructor (not implemented)
  SPLINE(const SPLINE&);
  /// private assign operator (not implemented)
  SPLINE& operator=(const SPLINE&);
};

/// get spline from Tcl_HashTable SplineTable
SPLINE* get_spline(const char * splinename);
/// construct spline from Tcl list
SPLINE* spline_list(char **,int ,int ,int);
/// Tcl command for spline evaluation
int tk_SplineEval(ClientData ,Tcl_Interp *,int ,char **);
/// Tcl command for spline creation
int tk_SplineCreate(ClientData ,Tcl_Interp *,int ,char **);
/// Tcl command for initialisation of Tcl spline commands
int Spline_Init(Tcl_Interp*);

#endif
