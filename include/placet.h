#ifndef PLACET_H
#define PLACET_H

#include <cstring>
#include <cmath>
#include <tcl.h>

#include "defaults.h"

/** physical constants from http://physics.nist.gov/cuu/Constants/index.html */

#define C_LIGHT 299792458 /** velocity of light [m/s] */
#define EMASS 0.000510998928 /** electron mass [GeV] */
#define INV_EMASS 1956.9514
#define RE 2.8179403267e-15 /** classical electron radius [m] */
#define ECHARGE 1.602176565e-19 /** electron charge [C] */
#define Z0 (C_LIGHT*4e-7*M_PI) /** characteristic impedance [Ohm] */
#define ALPHA_EM (1.0/137.035999074) /** fine structure constant */
#define HBAR 6.58211928e-25 /** reduced Planck constant [GeV s] */
#define ENERGY_LOSS 1.4079285720036e-05 /// e * e / 6 / pi / epsilon0 / m / ((electronmass * c * c / GeV)**4)

/** mathematical constants */

#ifndef PI
#define PI 3.141592653589793
#endif

#ifndef TWOPI
#define TWOPI 6.283185307179586
#endif

#define SQRT3 1.73205080756887719
#define SQRT_PI 1.7724538509055

/** preprocessor flags */

#define END_FIELDS
#define CAV_PRECISE
//#define LONGITUDINAL

#define TWODIM

/// earth magnetic field (default zero field, set value with EarthField)
//#define EARTH_FIELD

/** PLACET sizes and constants */

//#define CAVSTEP drive_data.steps
#define NSTEP data_nstep

/*#define MULTI 200*/
/** select random number generator */
#define rndm() rndm5()

#define BIN_MAX_BPM 1000
#define BIN_MAX_QUAD 2000
#define MAX_QUAD 20010
#define MAX_BIN 2500
//#define EMITT_LINES 10

#define EMITT_UNIT 1e-7

extern void interp_append(char*);

#if (SINCOS_EXISTS == 0)
static inline void sincos(double x, double* s, double *c)
{
  *s=sin(x); *c=cos(x);
}
#endif

#if (STRNSTR_EXISTS == 0)
static inline char *strnstr(const char *s1, const char *s2, size_t len)
{
  size_t l2 = strlen(s2);
  if (!l2)
    return (char *)s1;
  while (len >= l2) {
    len--;
    if (!memcmp(s1, s2, l2))
      return (char *)s1;
    s1++;
  }
  return NULL;
}
#endif

#include "placet_cout.hh"
#include "placet_print.h"
#include "placet_file.h"
#endif /* PLACET_H */
