#ifndef photon_h
#define photon_h

#include <vector>
#include <string>
#include <fstream>
#include <tcl.h>

struct PARTICLE;
class ELEMENT;
class BEAM;

// ----------------------------------------------------
// Photon class
// ----------------------------------------------------

class PHOTON
{
 public:
  PHOTON(const char* n, double r=1.);
  ~PHOTON();
  int nphot;
  int add_photon(double, double, double, double, double, double, ELEMENT*);
  int add_zelem(double);
  double rmax;
  std::ofstream of;
  int track(ELEMENT*);
  int full_tracking(bool);
  int set_samplingfraction(double);
 private:
  double fsample;
  double zcur;
  double icur;
  int track_elem(PARTICLE&, ELEMENT*, double z=0.);
  bool is_lost(PARTICLE p, ELEMENT* elem);
  std::vector<PARTICLE> vphoton;
  std::vector<double> vzpos;
  PARTICLE fill_part(double, double, double, double, double);
  bool fulltracking;
  std::string outDir;
};

// ----------------------------------------------------
// Interpreter functions
// ----------------------------------------------------
int TrackPhoton_Init(Tcl_Interp *interp);

#endif /* photon_h */
