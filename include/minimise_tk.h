#ifndef minimise_tk_h
#define minimise_tk_h

#include <tcl.h>
#include <tk.h>

namespace {
struct FuncObj {
  int n;
  Tcl_Obj **arg,**arg2;
  Tcl_CmdInfo info;
  Tcl_Interp *interp;
};

double func(double *x);
double func2(double *x);
FuncObj* function_eval_init(Tcl_Interp *interp,char *f,int n);
FuncObj* function_eval_init2(Tcl_Interp *interp,char *f,int n);
int function_eval2(FuncObj *p,double *x,double *res);
int function_eval(FuncObj *p,double *x,double *res);
}

int
tk_MinimiseFunction(ClientData clientdata,Tcl_Interp *interp,int argc,
		    char *argv[]);

int Minimise_Init(Tcl_Interp *interp);

#endif
