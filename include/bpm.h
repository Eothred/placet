#ifndef bpm_h
#define bpm_h

#include <vector>

#include "element.h"

class BEAMLINE;

class BPM : public virtual ELEMENT {
protected:

  void step_4d(BEAM *beam);
  void step_4d_0(BEAM *beam);
  void step_4d_sr(BEAM *beam) { step_4d(beam); }
  void step_4d_sr_0(BEAM *beam) { step_4d_0(beam); }

  void step_6d_0(BEAM *beam);
  void step_6d_sr_0(BEAM *beam) {step_6d_0(beam);}

  /// calculate BPM reading, does not affect BEAM
  void calculate_bpm_reading(BEAM *beam);

  double resolution;

  struct XY {
    double x;
    double y;
  } scale, err;

  struct XYW {
    double x;
    double y;
    double w;
  } pos;

  std::vector<XYW> store_bunches;

  void setup_attributes_table();

public:

  explicit BPM(double l=0.0, int n=0 );
  BPM(int &argc, char **argv );

  bool is_bpm() const { return true; }

  BPM *bpm_ptr() { return this; }
  const BPM *bpm_ptr() const { return this; }
  
  int display(Tcl_Interp*,int);
  
  double bunch_signal(int i) { return store_bunches[i].y; }
  double bunch_signal_x(int i) { return store_bunches[i].x; }
 
  void set_resolution(double r ) { resolution=r; }

  void set_x_position(double x ) { pos.x=x; }
  void set_y_position(double y ) { pos.y=y; }

  void add_x_position(double x ) { pos.x+=x; }
  void add_y_position(double y ) { pos.y+=y; }

  void set_store_bunches(int n);
  int get_store_bunches() const { return store_bunches.size(); }

  double get_resolution() const { return resolution; }

  double get_x_reading_exact() const { return pos.x; }
  double get_y_reading_exact() const { return pos.y; }
  double get_x_reading() const { return pos.x+err.x; }
  double get_y_reading() const { return pos.y+err.y; }

  double get_transmitted_charge() const { return pos.w; }

  double get_bpm_x_reading_exact() const { return pos.x; }
  double get_bpm_y_reading_exact() const { return pos.y; }
  double get_bpm_x_reading() const { return pos.x+err.x; }
  double get_bpm_y_reading() const { return pos.y+err.y; }
  double get_bpm_offset_x() const { return offset.x; }
  double get_bpm_offset_y() const { return offset.y; }
  
  void set_err_x(double x ) { err.x=x; }  
  void set_err_y(double x ) { err.y=x; }

  double get_err_x() const { return err.x; }
  double get_err_y() const { return err.y; }
  
  void set_scale_y(double x ) { scale.y=x; }
  void set_scale_x(double x ) { scale.x=x; }

  friend OStream &operator<<(OStream &stream, const BPM &bpm )
  {
    return stream << static_cast<const ELEMENT &>(bpm)
		  << size_t(bpm.get_store_bunches())
		  << double(bpm.get_resolution())
		  << double(bpm.get_err_x())
		  << double(bpm.get_err_y())
		  << double(bpm.pos.x)
		  << double(bpm.pos.y)
		  << double(1.0);
  }
};

int tk_BpmDisplay(ClientData ,Tcl_Interp *,int ,char **);
int tk_BpmReadings(ClientData ,Tcl_Interp *,int ,char **);

#endif
