#ifndef collimator_h
#define collimator_h

#include "element.h"

class SPLINE;

class COLLIMATOR : public ELEMENT {


  int correct_offset,nmax;
  double dz,dzi,zmin,zmax;
  double *wtx,*wty,*rhox,*rhoy;
  SPLINE *spx,*spy;

  double in_height,fin_height,width,taper_length,flat_length;
  double sigma,tau;
  double Nb;
 
  bool giovanni;
  bool vertical;


  void step_in(BEAM *beam ) { if (correct_offset) ELEMENT::step_in(beam); }
  void step_out(BEAM *beam ) { if (correct_offset) ELEMENT::step_out(beam); }


public:
  
  COLLIMATOR(double bb, double gg, double ww, double LT, double Lflat, double sig_ch, double tau_ch, double _charge, int corr=1, bool vert=true);
  COLLIMATOR(SPLINE*);
  COLLIMATOR(SPLINE*,SPLINE*);
  void set_spline(SPLINE*);
  void set_spline_x(SPLINE*);
  void set_spline_y(SPLINE*);
  void set_correct_offset(int i) {
    correct_offset=i;
  };
  void set_bin(double z1,double z2,int n0) {
    zmin=z1; zmax=z2; nmax=n0; dz=(zmax-zmin)/nmax; dzi=1.0/dz;
  };
  void set_wake_meth(int wake_method) { wake_meth=wake_method; }
  void set_wake_table(const char *wake_table) { wake_tname = wake_table;}
  int get_wake_meth() { return wake_meth; }
  const char* get_wake_table() const {return wake_tname.c_str(); }
  void adina_step_4d_0(BEAM*);
  void giovanni_step_4d_0(BEAM*);
  void giovanni_step(BEAM*);
  void daniel_step_4d_0(BEAM*);
  void daniel_step(BEAM*);
  void step_4d_0(BEAM *beam ) { if (giovanni && wake_meth==0) giovanni_step_4d_0(beam); else if (giovanni && wake_meth!=0) adina_step_4d_0(beam); else daniel_step_4d_0(beam); }
  void step_4d(BEAM *beam ) { if (giovanni) giovanni_step(beam); else daniel_step(beam); }
  //virtual int list(Tcl_Interp *interp);
  //virtual void list(FILE *file);
 private:
  int wake_meth;
  std::string wake_tname;
  
};

int tk_Collimator(ClientData clientdata,Tcl_Interp *interp,int argc,
		  char *argv[]);

#endif /* collimator_h */
