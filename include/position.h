#ifndef position_h
#define position_h

int tk_SaveAllPositions(ClientData ,Tcl_Interp *,int ,char **);
int tk_ReadAllPositions(ClientData ,Tcl_Interp *,int ,char **);
int tk_ReadAllMotion(ClientData ,Tcl_Interp *,int ,char **);
int tk_ReadQuadMotion(ClientData ,Tcl_Interp *,int ,char **);
int tk_WriteGirderLength(ClientData ,Tcl_Interp *,int ,char **);
int tk_MoveGirder(ClientData ,Tcl_Interp *,int ,char **);
int tk_GroundMotionATL(ClientData ,Tcl_Interp *,int ,char **);
int tk_GroundMotionInit(ClientData ,Tcl_Interp *,int ,char **);
int tk_GroundMotion(ClientData ,Tcl_Interp *,int ,char **);
int tk_AddPreIsolator(ClientData ,Tcl_Interp *,int ,char **);
int tk_AddGMFilter(ClientData ,Tcl_Interp *,int ,char **);
#endif // position_h
