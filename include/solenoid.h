#ifndef solenoid_h
#define solenoid_h

#include "element.h"

class BEAM;
struct PARTICLE;
struct R_MATRIX;

class SOLENOID : public ELEMENT {

  double strength;

public:

  explicit SOLENOID(double l=0.0, double s=0.0 ) : ELEMENT(l), strength(s) {}

  void set_strength(double s ) { strength=s; }
  double get_strength() const { return strength; }

  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_4d_0(BEAM*);
  void step_4d(BEAM*);

  bool is_solenoid() const { return true; }

  Matrix<6,6> get_transfer_matrix_6d(double _energy ) const;

  SOLENOID *solenoid_ptr() { return this; }
  const SOLENOID *solenoid_ptr() const { return this; }

  void GetMagField(PARTICLE *particle, double *bfield) {};


 private:
  void particle_step_end(PARTICLE* particle, double bz) const;
  void particle_step(PARTICLE* particle, double length, double bz) const;
  void sigma_step(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,double energy,double length,double bz) const;
  void sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,double energy,double bz) const;
};

#endif /* solenoid_h */
