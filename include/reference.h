#ifndef reference_h
#define reference_h

#include "element.h"

class REFERENCE : public ELEMENT {

  int sens, angle;

public:

  REFERENCE(int _sens=0, int _angle=0 ) : sens(_sens), angle(_angle) {}
  
  void step_4d_0(BEAM*);
  void step_4d(BEAM*); 

  void list(FILE*)const;
  
  void set_sens(int s ) { sens=s; }
  void set_angle(int a ) { angle=a; }

  int get_sens() { return sens; }

};

int tk_ReferencePoint(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[]);

#endif /* reference_h */
