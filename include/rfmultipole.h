#ifndef rfmultipole_h
#define rfmultipole_h

#include <complex>
#include <vector>

#include "element.h"
#include "particle.h"
#include "conversion.h"

class RFMULTIPOLE : public ELEMENT {

  double tilt;
  std::vector<std::complex<double> > strength_list;
  std::vector<std::complex<double> > phase_list;
  double lambda;

  mutable std::vector<std::complex<double> > Kn_list;

  void init_kick()
  {
    if (!strength_list.empty()) {
      Kn_list.resize(strength_list.size());
      for (size_t i=0; i<strength_list.size(); i++) {
	std::complex<double> strength_n = strength_list[i] * 1e6 / double(flags.thin_lens);
	int _field=i+1;
	for (int j=2;j<_field;j++)
	  strength_n/=j;
	Kn_list[i] = strength_n * std::polar(1.0, tilt);
      }
    }
  }
  
  KICK get_kick(const PARTICLE &particle ) const
  {
    KICK kick;
    kick.xp = 0.0;
    kick.yp = 0.0;
    if (Kn_list.empty()) return kick;

    const size_t Kn_length = Kn_list.size();
    const size_t Pl_length = phase_list.size();
    double kz = 2.*M_PI/(lambda*1e6)*particle.z;
    // dipole kick
    const std::complex<double> &K1 = Kn_list[0];
    if ((fabs(K1.real())>std::numeric_limits<double>::epsilon()) ||
	(fabs(K1.imag())>std::numeric_limits<double>::epsilon())) {
      std::complex<double> _K1 = K1;
      if (Pl_length>0) {
	_K1 = std::complex<double> (_K1.real() * cos(kz+phase_list[0].real()),  _K1.imag()*cos(kz+phase_list[0].imag()));
      }
      std::complex<double> Kick = _K1 / particle.energy;
      kick.xp -= real(Kick);
      kick.yp += imag(Kick);
    }
    if (Kn_length<=1) return kick;
    
    // quadrupole kick
    const std::complex<double> &K2 = Kn_list[1];
    if ((fabs(K2.real())>std::numeric_limits<double>::epsilon()) ||
	(fabs(K2.imag())>std::numeric_limits<double>::epsilon())) {
      std::complex<double> _K2 = K2;
      if (Pl_length>1) {
	_K2 = std::complex<double> (_K2.real() * cos(kz+phase_list[1].real()),  _K2.imag()*cos(kz+phase_list[1].imag()));
      }
      std::complex<double> Kick = _K2 / particle.energy * std::complex<double>(particle.x, particle.y) * 1e-6;
      kick.xp -= real(Kick);
      kick.yp += imag(Kick);
    }
    // higher order kicks
    for (size_t n=2; n<Kn_length; n++) {
      const std::complex<double> &Kn = Kn_list[n];
      if ((fabs(Kn.real())>std::numeric_limits<double>::epsilon()) ||
	  (fabs(Kn.imag())>std::numeric_limits<double>::epsilon())) {
	std::complex<double> _Kn = Kn;
	if (Pl_length>n) {
	  _Kn = std::complex<double> (_Kn.real() * cos(kz+phase_list[n].real()),  _Kn.imag()*cos(kz+phase_list[n].imag()));
	}
	std::complex<double> _kick = _Kn / particle.energy * std::pow(std::complex<double>(particle.x, particle.y) * 1e-6, n);
	kick.xp -= real(_kick);
	kick.yp += imag(_kick);
      }
    }
    return kick;
  }
  
public:
  
  RFMULTIPOLE(int &argc, char **argv );

  bool is_rf_multipole() const { return true; }
 
  RFMULTIPOLE *rf_multipole_ptr() { return this; }
  const RFMULTIPOLE *rf_multipole_ptr() const { return this; }

  void set_nstep(int n) { flags.thin_lens=n; }
  void set_tilt(double t) { tilt=t; }
  int get_nstep() const { return flags.thin_lens; }

  double get_tilt() const { return tilt; }

  std::string get_strength_list_str() const
  {
    std::ostringstream buf;
    for (size_t i=0; i<strength_list.size(); i++)
      buf << ' ' << strength_list[i];
    return buf.str();
  }
  
  std::string get_phase_list_deg_str() const
  {
    std::ostringstream buf;
    for (size_t i=0; i<strength_list.size(); i++) {
      std::complex<double> k = phase_list[i];
      k = std::complex<double>(rad2deg(k.real()),rad2deg(k.imag()));
      buf << ' ' << k;
    }
    return buf.str();
  }
  
  std::string get_phase_list_rad_str() const
  {
    std::ostringstream buf;
    for (size_t i=0; i<strength_list.size(); i++) {
      buf << ' ' <<phase_list[i];
    }
    return buf.str();
  }
   
  void set_strength_list_str(const char *strength_list_str )
  {
    strength_list.resize(0);
    {
      std::istringstream buf(strength_list_str);
      std::complex<double> k;
      while(buf>>k) {
	strength_list.push_back(k);
      }
    }
  }

  void set_phase_list_deg_str(const char *phase_list_str )
  {
    phase_list.resize(0);
    {
      std::istringstream buf(phase_list_str);
      std::complex<double> k;
      while(buf>>k) {
	k = std::complex<double>(deg2rad(k.real()),deg2rad(k.imag()));
        phase_list.push_back(k);
      }
    }
  }

  void set_phase_list_rad_str(const char *phase_list_str )
  {
    phase_list.resize(0);
    {
      std::istringstream buf(phase_list_str);
      std::complex<double> k;
      while(buf>>k) {
      	phase_list.push_back(k);
      }
    }
  }
  void list(FILE*) const;
  
  /*
    friend OStream &operator<<(OStream &stream, const RFMULTIPOLE &multipole )
    {
    return stream << static_cast<const ELEMENT &>(multipole)
    << multipole.field
    << multipole.strength
    << multipole.tilt;
    }

    friend IStream &operator>>(IStream &stream, RFMULTIPOLE &multipole )
    {
    return stream >> static_cast<ELEMENT &>(multipole)
    >> multipole.field
    >> multipole.strength
    >> multipole.tilt;
    }
  */

  Matrix<6,6> get_transfer_matrix_6d(double _energy ) const;

};

#endif
