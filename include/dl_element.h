#ifndef dl_element_h
#define dl_element_h

#include "element.h"

#include "dl_element_defs.h"

class DLELEMENT : public ELEMENT {

  std::string filename;

  void *handle;
  
  void *data;
  
  typedef bool (*dl_init_t)(DLELEMENT &_this, int &argc, char **argv );
  typedef void (*dl_fini_t)(DLELEMENT &_this );
  typedef void (*dl_step_t)(DLELEMENT &_this, BEAM *beam );

  dl_init_t dl_init;
  dl_fini_t dl_fini;

  dl_step_t dl_step_4d;
  dl_step_t dl_step_4d_0;
  dl_step_t dl_step_4d_sr;
  dl_step_t dl_step_4d_sr_0;

  dl_step_t dl_step_6d;
  dl_step_t dl_step_6d_0;
  dl_step_t dl_step_6d_sr;
  dl_step_t dl_step_6d_sr_0;

  dl_step_t dl_step_in;
  dl_step_t dl_step_out;

  void step_in(BEAM *beam )	  { if (dl_step_in)       (*dl_step_in)(*this, beam); else ELEMENT::step_in(beam); }
  void step_out(BEAM *beam )	  { if (dl_step_out)      (*dl_step_out)(*this, beam); else ELEMENT::step_out(beam); }

  void step_4d(BEAM *beam )	  { if (dl_step_4d)       (*dl_step_4d)(*this, beam); else ELEMENT::step_4d(beam); }
  void step_4d_0(BEAM *beam )	  { if (dl_step_4d_0)     (*dl_step_4d_0)(*this, beam); else ELEMENT::step_4d_0(beam); }
  void step_4d_sr(BEAM *beam )	  { if (dl_step_4d_sr)    (*dl_step_4d_sr)(*this, beam); else ELEMENT::step_4d_sr(beam); }
  void step_4d_sr_0(BEAM *beam )  { if (dl_step_4d_sr_0)  (*dl_step_4d_sr_0)(*this, beam); else ELEMENT::step_4d_sr_0(beam); }
  
  void step_6d(BEAM *beam )	  { if (dl_step_6d)       (*dl_step_6d)(*this, beam); else ELEMENT::step_6d(beam); }
  void step_6d_0(BEAM *beam )	  { if (dl_step_6d_0)     (*dl_step_6d_0)(*this, beam); else ELEMENT::step_6d_0(beam); }
  void step_6d_sr(BEAM *beam )	  { if (dl_step_6d_sr)    (*dl_step_6d_sr)(*this, beam); else ELEMENT::step_6d_sr(beam); }
  void step_6d_sr_0(BEAM *beam )  { if (dl_step_6d_sr_0)  (*dl_step_6d_sr_0)(*this, beam); else ELEMENT::step_6d_sr_0(beam); }
  
  DLELEMENT() {}

public:

  DLELEMENT(int &argc, char **argv );
  ~DLELEMENT();
  
  // void GetMagField(double * pos, double *bfield) {};
  
  friend bool dl_element_init(DLELEMENT &_this, int &argc, char **argv );
  friend void dl_element_fini(DLELEMENT &_this );

  friend void dl_element_step_in(DLELEMENT &_this, BEAM *beam );
  friend void dl_element_step_out(DLELEMENT &_this, BEAM *beam );

  friend void dl_element_step_4d(DLELEMENT &_this, BEAM *beam );
  friend void dl_element_step_4d_0(DLELEMENT &_this, BEAM *beam );
  friend void dl_element_step_4d_sr(DLELEMENT &_this, BEAM *beam );
  friend void dl_element_step_4d_sr_0(DLELEMENT &_this, BEAM *beam );

  friend void dl_element_step_6d(DLELEMENT &_this, BEAM *beam );
  friend void dl_element_step_6d_0(DLELEMENT &_this, BEAM *beam );
  friend void dl_element_step_6d_sr(DLELEMENT &_this, BEAM *beam );
  friend void dl_element_step_6d_sr_0(DLELEMENT &_this, BEAM *beam );

};

#endif /* dl_element_h */
