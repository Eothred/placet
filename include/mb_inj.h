#ifndef mb_inj_h
#define mb_inj_h

class BEAMLINE;
class BEAM;
struct INJECT_BEAM_PARAM;
class SPLINE;

BEAM *make_multi_bunch_inject(BEAMLINE *,char *,SPLINE *,INJECT_BEAM_PARAM *,int,int);

#endif // mb_inj_h
