#ifndef beamenergy_h
#define beamenergy_h

double beamenergy(BEAM *);
void beam_energy_plot(BEAMLINE *,BEAM *,char *);
void beam_energy_profile_plot(BEAM *,char *);
int tk_BeamEnergyPlot(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamEnergyPrint(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamEnergyProfilePlot(ClientData ,Tcl_Interp *,int ,char **);

#endif // beamenergy_h
