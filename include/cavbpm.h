#ifndef cavbpm_h
#define cavbpm_h

#include "bpm.h"

struct FIELD;
struct WAKE_DATA;

/**
 * @brief class for cavity BPM, BPM with short range wakefield
 * 
 * @author Francis Cullinan <Francis.Cullinan.2010@live.rhul.ac.uk>
 * @author Jochem Snuverink <Jochem.Snuverink@rhul.ac.uk>
 */

class CAVBPM : public BPM {
  
 private:
  /// private default constructor (not implemented)
  CAVBPM();
  /// private copy constructor (not implemented)
  CAVBPM(const CAVBPM&);
  /// private assign operator (not implemented)
  CAVBPM& operator=(const CAVBPM&);

  /** 
      array of FIELDS in [urad GeV / um]
   */
  /// wakefield array
  FIELD* wakefield;
  /// current size of wakefield array
  unsigned int kicks;

  /// apply wakefield kick for field representation
  void apply_wakefield_field(BEAM* beam);

  /// long range wakefield for multi bunches, so far only applied in field representation
  WAKE_DATA* wake_data;

protected:
  
  /// tracking method for sliced beam
  void step_4d(BEAM*);
  /// tracking method for particle beam
  void step_4d_0(BEAM*);
  /// overwrite ELEMENT::apply_short_range_wakefield method
  virtual void apply_short_range_wakefield(BEAM* beam);
  
public:
  /// constructor, number of kicks (ki) and kick array (ch)
  CAVBPM(int &argc, char **argv, unsigned int ki=0, double * ch=NULL);
  /// destructor
  ~CAVBPM();  
  /// set long range wakefield name
  void set_wake_long(const char *name );
  /// get long range wakefield name
  std::string get_wake_long() const;
  /// get wakefield
  FIELD* get_field()const { return wakefield; }
  /// methods to provide class type
  bool is_cavbpm() const { return true; }
  CAVBPM *cavbpm_ptr() { return this; }
  const CAVBPM *cavbpm_ptr() const { return this; }
};

#endif /* cavbpm_h */
