#ifndef _placet_tk_h
#define _placet_tk_h

#include <string>
#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"
#include "manual.h"

class BEAM;
class BEAMLINE;

void interp_append(char *);
void check_error(Tcl_Interp *,char *,int);
int check_beamline_ready(Tcl_Interp *,char *,int);
Tcl_Command Placet_CreateCommand _ANSI_ARGS_((Tcl_Interp *,const char *,Tcl_CmdProc *, ClientData ,Tcl_CmdDeleteProc *, std::string texinfo_description = ""));
Tcl_Command Placet_CreateCommand _ANSI_ARGS_((MANUAL::CATEGORY category, MANUAL::SUBCATEGORY subcategory, Tcl_Interp *,const char *,Tcl_CmdProc *,ClientData ,Tcl_CmdDeleteProc *, std::string texinfo_description = ""));
                                               
void beam_set_range_offset_y(BEAM *,int ,int ,double);
void beam_set_range_offset_yp(BEAM *,int ,int ,double);
void beam_set_to_range_offset_y(BEAM *,int ,int ,double);
void beam_set_to_range_offset_yp(BEAM *,int ,int ,double);
void beam_set_range_offset_sine_y(BEAM *,int ,int ,double ,double ,double);
void beam_set_range_offset_sine_yp(BEAM *,int ,int ,double ,double ,double);
void beam_set_range_offset_x(BEAM *,int ,int ,double);
void beam_set_range_offset_xp(BEAM *,int ,int ,double);
void beam_set_to_range_offset_x(BEAM *,int ,int ,double);
void beam_set_to_range_offset_xp(BEAM *,int ,int ,double);
void beam_set_range_offset_sine_x(BEAM *,int ,int ,double ,double ,double);
void beam_set_range_offset_sine_xp(BEAM *,int ,int ,double ,double ,double);
void beam_set_to_macro_offset_xy(BEAM *beam, double sigma_offset_x, double sigma_offset_y, double* e_list, double* /*w_list*/);
void beam_set_real_last_weight(BEAM *beam, double real_last_wgt);
int eval_beam_position(Tcl_Interp *,BEAM *,char *,int *);
BEAM* get_beam(const char * beamname);
BEAMLINE* get_beamline(const char * beamlinename);
WAKE_DATA* get_wake(const char * wakename);
int Tcl_Survey(ClientData ,Tcl_Interp *,int ,char **);
void survey_add(Tcl_Interp *,char *,void (*)(BEAMLINE*));
void beamline_survey_name(BEAMLINE *);
void *survey_find(const char *);
void survey_init(Tcl_Interp *);
double wake_eval(WAKE_DATA *,double);
/* int tk_ApertureLosses(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_Beam(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamAddOffset(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamGetPosition(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamGetSize(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamlineInfo(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamlineList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamlineSetSine(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamLoad(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamLoadAll(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamModulateEnergy(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamSave(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamSaveAbsolute(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamSaveAll(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamSaveMad(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamSaveGp(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamSetOffsetSine(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamSetToOffset(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BeamWriteRms(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BnsPlot(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_Bookshelf(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BpmNumberList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BpmPositionListZ(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BpmRealign(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BpmToCavScatter(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_BunchesSetOffsetRandom(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_CavityDefine(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_CavityGetGradientList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_CavityGetPhaseList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_CavityGetTypeList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_CavitySetGradientList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_CavitySetPhaseList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_CavitySetTypeList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_CheckAutophase(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_Clic(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DipoleGetStrengthList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DipoleNumberList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DipoleSetStrength(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DipoleSetStrengthList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DipoleSetZero(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DispersionPlot(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DivideBeamline(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DivideBeamline2(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_DriveBeam(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_EarthField(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_ElementAddOffset(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_ElementInfo(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_ElementSetLength(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_ElementSetToOffset(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_Emitt_0(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_EnergySpreadPlot(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_EnvelopeCut(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_FirstOrder(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_Help(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_Help2(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_InjectorBeam(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_InjectorCavityDefine(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_InterGirderMove(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_IPFeedback_0(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_LongBumps(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_Lumi_0(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_MainBeam(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_MainGradient(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_MatchDoublet(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_MatchFodo(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_MatchTriplet(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_PhaseAdvance(ClientData, Tcl_Interp *, int, char**, char**); */
/* int tk_PlotBeamSpectrum(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_PlotFillFactor(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupoleGetStrength(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupoleGetStrengthList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupoleNumberList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupolePositionListZ(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupoleSetStrength(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupoleSetStrengthList(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupoleSetting(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupoleSettingCmd(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadrupoleStepsize(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_QuadWake(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_RandomReset(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_ScatterGirder(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_SetRfGradient(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_SetRfGradientSingle(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_SimpleFeedback(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_SurveyErrorSet(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TempView(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TempView2(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestBallisticCorrection(ClientData,Tcl_Interp *,int ,char **);  */
/* int tk_TestBeam(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestFeedback(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestFreeCorrection(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestFreeCorrection2(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestFreeCorrectionDipole(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestIndependentFeedback(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestNoCorrection(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestNoCorrection2(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestQuadrupoleJitter(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestQuadrupoleSensitivity(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestSimpleAlignment(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestSimpleCorrection(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestSimpleCorrectionDipole(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TestTrainCorrection(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TwissMain(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TwissMainSpread(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TwissPlot(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_TwissPlotStep(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_ViewAll(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_WakeEval(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_WakefieldSet(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_WakeSet(ClientData ,Tcl_Interp *,int ,char **); */
/* int tk_WireSurvey(ClientData ,Tcl_Interp *,int ,char **); */
void placet_init();
void placet_init2(Tcl_Interp *);
int beamline_bin_divide_list(Tcl_Interp *,BEAMLINE *,BIN **,int *,char *);
/* int tk_TestFreeCorrectionDipole(ClientData clientdata, */
/* 				Tcl_Interp *interp,int argc, */
/* 				char *argv[]); */

extern int data_nstep;
extern double zero_point;

extern INJECTOR_DATA injector_data;
extern BUMP_DATA bump_data;
extern RF_DATA rf_data;
#ifdef EARTH_FIELD
extern EARTH_FIELD_STRUCT earth_field;
#endif
extern SWITCHES_STRUCT switches;
extern QUAD_WAKE_STRUCT quad_wake;
extern ERRORS errors;
extern CORR corr;
extern BALLISTIC_DATA  ballistic_data;
extern double quad_set0[MAX_QUAD],quad_set1[MAX_QUAD],quad_set2[MAX_QUAD];
extern WAKEFIELD_DATA_STRUCT wakefield_data;
extern SURVEY_ERRORS_STRUCT survey_errors;

extern Tcl_Interp *beamline_survey_hook_interp;

#ifdef __cplusplus

extern "C" {

int Placet_Init(Tcl_Interp * );
int Tcl_AppInit(Tcl_Interp * );

}

#endif

#endif /* _placet_tk_h */
