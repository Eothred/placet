#include <limits>
#include <cmath>

#include "sbend.hh"

#define M_SQRT3 1.7320508075688772935274463415058723669428

void SBend::transport(std::vector<Particle> &beam ) const
{
  //size_t photons = 0;
  const double h0 = angle0/length*ref_energy;
  const double t1 = fabs(e1)>std::numeric_limits<double>::epsilon() ? tan(e1)*h0 : 0.0;
  const double t2 = fabs(e2)>std::numeric_limits<double>::epsilon() ? tan(e2)*h0 : 0.0;
  for (int i=0; i<beam.size(); i++) {
    // reference to a particle 
    Particle &particle = beam[i];
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      // entrance angle e1
      if (fabs(t1)>std::numeric_limits<double>::epsilon()) {
	double fx=t1/particle.energy;
	double fy=t1/particle.energy;
	particle.xp+=fx*particle.x;
	particle.yp-=fy*particle.y;
      }
      // calculate the energy loss by synrad emission
      const double h = angle0/length;
      const double kx_ = h*ref_energy/particle.energy;
      const double kyy = -k/particle.energy/length;
      const double kxx = h*kx_ - kyy;
      const double delta = particle.momentum_deviation(ref_energy)*1e6; // micro
      if (fabs(kxx)<std::numeric_limits<double>::epsilon()) {
	particle.z += particle.x*angle0 + particle.xp*angle0*length;
	double __x = particle.xp*length + delta*angle0*length;
	particle.xp += delta*angle0;
	particle.x += __x;
      } else if (kxx>0.) {
	const double sqrt_kxx = sqrt(kxx);
	double cx, sx;
	sincos(sqrt_kxx*length, &sx, &cx);
	double sx_kx = sx/sqrt_kxx;
	particle.z += particle.x*h*sx_kx + particle.xp*h*(1-cx)/kxx + delta*(length-sx_kx);
	double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
	particle.xp = -particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
	particle.x = __x;
      } else {
	const double sqrt_kxx = sqrt(-kxx);
	double cx = sinh(sqrt_kxx*length);
	double sx = cosh(sqrt_kxx*length);
	double sx_kx = sx/sqrt_kxx;
	particle.z += particle.x*h*sx_kx + particle.xp*h*(1-cx)/kxx + delta*(length-sx_kx);
	double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
	particle.xp = particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
	particle.x = __x;
      }
      if (fabs(kyy)<std::numeric_limits<double>::epsilon()) {
	particle.y  += particle.yp*length;
      } else if (kyy > 0.) {
	const double sqrt_kyy = sqrt(kyy);
	double cy, sy;
	sincos(sqrt_kyy*length, &sy, &cy);
	double sy_ky = sy/sqrt_kyy;
	double __y = particle.y*cy + particle.yp*sy_ky;
	particle.yp = -particle.y*sqrt_kyy*sy + particle.yp*cy;
	particle.y = __y;
      } else {
	const double sqrt_kyy = sqrt(-kyy);
	double sy = sinh(sqrt_kyy*length);
	double cy = cosh(sqrt_kyy*length);
	double sy_ky = sy/sqrt_kyy;
	double __y = particle.y*cy + particle.yp*sy_ky;
	particle.yp = particle.y*sqrt_kyy*sy + particle.yp*cy;
	particle.y = __y;
      }
      // exit angle e2
      if (fabs(t2)>std::numeric_limits<double>::epsilon()) {
	double fx=t2/particle.energy;
	double fy=t2/particle.energy;
	particle.xp+=fx*particle.x;
	particle.yp-=fy*particle.y;
      }
    }
  }
}

void SBend::transport_synrad(std::vector<Particle> &beam ) const
{
  //size_t photons = 0;
  const double h0 = angle0/length*ref_energy;
  const double t1 = fabs(e1)>std::numeric_limits<double>::epsilon() ? tan(e1)*h0 : 0.0;
  const double t2 = fabs(e2)>std::numeric_limits<double>::epsilon() ? tan(e2)*h0 : 0.0;
  for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
    // reference to particle
    Particle &particle = *i;
    if (fabs(particle.energy)>std::numeric_limits<double>::epsilon()) {
      // entrance angle e1
      if (fabs(t1)>std::numeric_limits<double>::epsilon()) {
	double fx=t1/particle.energy;
	double fy=t1/particle.energy;
	particle.xp+=fx*particle.x;
	particle.yp-=fy*particle.y;
      }
      // calculate the energy loss by synrad emission
      for (double z = 0.0;;) {
	const double radius = length/fabs(angle0)*particle.energy/ref_energy;
	const double gamma = particle.gamma();
	const double meanfreepath = M_SQRT3*radius/(2.5*ALPHA_EM*gamma);
	double step = gsl_ran_exponential(rng, meanfreepath);
	const bool done = (z+step>=length);
	if (done) step = length-z;
	if (step>std::numeric_limits<double>::epsilon()) {
	  const double iangle = angle0 * step / length;
	  const double ilength = step;
	  const double h = angle0/length;
	  const double kx_ = h*ref_energy/particle.energy;
	  const double kyy = -k/particle.energy/length;
	  const double kxx = h*kx_ - kyy;
	  const double delta = particle.momentum_deviation(ref_energy)*1e6; // micro
	  if (fabs(kxx)<std::numeric_limits<double>::epsilon()) {
	    particle.z += particle.x*iangle + particle.xp*iangle*ilength;
	    double __x = particle.xp*ilength + delta*iangle*ilength;
	    particle.xp += delta*iangle;
	    particle.x += __x;
	  } else if (kxx>0.) {
	    const double sqrt_kxx = sqrt(kxx);
	    double cx, sx;
	    sincos(sqrt_kxx*ilength, &sx, &cx);
	    double sx_kx = sx/sqrt_kxx;
	    particle.z += particle.x*h*sx_kx + particle.xp*h*(1-cx)/kxx + delta*(ilength-sx_kx);
	    double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
	    particle.xp = -particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
            particle.x = __x;
	  } else {
	    const double sqrt_kxx = sqrt(-kxx);
	    double cx = sinh(sqrt_kxx*length);
	    double sx = cosh(sqrt_kxx*length);
	    double sx_kx = sx/sqrt_kxx;
	    particle.z += particle.x*h*sx_kx + particle.xp*h*(1-cx)/kxx + delta*(ilength-sx_kx);
	    double __x = particle.x*cx + particle.xp*sx_kx + (1-cx)*(h-kx_)/kxx*1e6;
	    particle.xp = particle.x*sqrt_kxx*sx + particle.xp*cx + (h-kx_)*sx_kx*1e6;
	    particle.x = __x;
	  }
	  if (fabs(kyy)<std::numeric_limits<double>::epsilon()) {
	    particle.y  += particle.yp*ilength;
	  } else if (kyy > 0.) {
	    const double sqrt_kyy = sqrt(kyy);
	    double cy, sy;
	    sincos(sqrt_kyy*ilength, &sy, &cy);
	    double sy_ky = sy/sqrt_kyy;
	    double __y = particle.y*cy + particle.yp*sy_ky;
	    particle.yp = -particle.y*sqrt_kyy*sy + particle.yp*cy;
	    particle.y = __y;
	  } else {
	    const double sqrt_kyy = sqrt(-kyy);
	    double sy = sinh(sqrt_kyy*length);
	    double cy = cosh(sqrt_kyy*length);
	    double sy_ky = sy/sqrt_kyy;
	    double __y = particle.y*cy + particle.yp*sy_ky;
	    particle.yp = particle.y*sqrt_kyy*sy + particle.yp*cy;
	    particle.y = __y;
	  }
	  z += step;
	}
	if (done) break;
	// nphotons++;
	double energy_critical = 1.5*HBAR*C_LIGHT*gamma*gamma*gamma/radius;
	double energy_loss = syngen()*energy_critical;
	particle.energy -= energy_loss;
      }
      // exit angle e2
      if (fabs(t2)>std::numeric_limits<double>::epsilon()) {
	double fx=t2/particle.energy;
	double fy=t2/particle.energy;
	particle.xp+=fx*particle.x;
	particle.yp-=fy*particle.y;
      }
    }
  }
}
