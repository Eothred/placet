#include <typeinfo>
#include <cstring>
#include <string>

#include "quadrupole.hh"
#include "multipole.hh"
#include "special.hh"
#include "dipole.hh"
#include "drift.hh"
#include "sbend.hh"
#include "bpm.hh"

Element *Element::duplicate(const Element *ptr )
{
#define TEST_AND_ALLOCATE(PTR, TYPE)					\
  if (const TYPE *t_ptr=dynamic_cast<const TYPE *>(PTR)) return new TYPE(*t_ptr);

  TEST_AND_ALLOCATE(ptr, Quadrupole)
  TEST_AND_ALLOCATE(ptr, Multipole)
  TEST_AND_ALLOCATE(ptr, Special)
  TEST_AND_ALLOCATE(ptr, Dipole)
  TEST_AND_ALLOCATE(ptr, Drift)
  TEST_AND_ALLOCATE(ptr, SBend)
  TEST_AND_ALLOCATE(ptr, Bpm)

#undef TEST_AND_ALLOCATE

    return NULL;
}

void Element::insert_element_ptr(OStream &stream, const Element *ptr )
{
#define TEST_AND_INSERT(PTR, ID, TYPE)					\
  if (const TYPE *t_ptr=dynamic_cast<const TYPE *>(PTR)) { stream << int(ID) << *t_ptr; return; }

  TEST_AND_INSERT(ptr, ELEMENT_ID::QUADRUPOLE, Quadrupole)
  TEST_AND_INSERT(ptr, ELEMENT_ID::MULTIPOLE, Multipole)
  TEST_AND_INSERT(ptr, ELEMENT_ID::SPECIAL, Special)
  TEST_AND_INSERT(ptr, ELEMENT_ID::DIPOLE, Dipole)
  TEST_AND_INSERT(ptr, ELEMENT_ID::DRIFT, Drift)
  TEST_AND_INSERT(ptr, ELEMENT_ID::SBEND, SBend)
  TEST_AND_INSERT(ptr, ELEMENT_ID::BPM, Bpm)

#undef TEST_AND_INSERT
}

Element *Element::extract_element_ptr(IStream &stream )
{
  int element_id; stream >> element_id;

#define TEST_AND_EXTRACT(PTR, ID, TYPE)					\
  if (element_id==ID) { TYPE *ptr = new TYPE; stream >> *ptr; return ptr; }

  TEST_AND_EXTRACT(ptr, ELEMENT_ID::QUADRUPOLE, Quadrupole)
  TEST_AND_EXTRACT(ptr, ELEMENT_ID::MULTIPOLE, Multipole)
  TEST_AND_EXTRACT(ptr, ELEMENT_ID::SPECIAL, Special)
  TEST_AND_EXTRACT(ptr, ELEMENT_ID::DIPOLE, Dipole)
  TEST_AND_EXTRACT(ptr, ELEMENT_ID::DRIFT, Drift)
  TEST_AND_EXTRACT(ptr, ELEMENT_ID::SBEND, SBend)
  TEST_AND_EXTRACT(ptr, ELEMENT_ID::BPM, Bpm)
    
#undef TEST_AND_EXTRACT
    
    return NULL;
}

PhotonSpectrum	 Element::syngen(0.0);
gsl_rng		*Element::rng;

void Element::transport_in(std::vector<Particle> &beam ) const
{
  double offsetx1=0.5*length*xp-x;
  double offsety1=0.5*length*yp-y;
  for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
    Particle &particle = *i;
    particle.x += offsetx1;
    particle.y += offsety1;
    particle.xp -= xp;
    particle.yp -= yp;
  }
  if (fabs(roll)>std::numeric_limits<double>::epsilon()) {
    double c,s;
    sincos(roll,&s,&c);
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      double tmp=c*particle.x-s*particle.y;
      particle.y=s*particle.x+c*particle.y;
      particle.x=tmp;
      tmp=c*particle.xp-s*particle.yp;
      particle.yp=s*particle.xp+c*particle.yp;
      particle.xp=tmp;
    }
  }
}

void Element::transport_out(std::vector<Particle> &beam ) const
{
  if (fabs(roll)>std::numeric_limits<double>::epsilon()) {
    double c,s;
    sincos(-roll,&s,&c);
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      double tmp=c*particle.x-s*particle.y;
      particle.y=s*particle.x+c*particle.y;
      particle.x=tmp;
      tmp=c*particle.xp-s*particle.yp;
      particle.yp=s*particle.xp+c*particle.yp;
      particle.xp=tmp;
    } 
  }
  double offsetx2=0.5*length*xp+x;
  double offsety2=0.5*length*yp+y;
  for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
    Particle &particle = *i;
    particle.x += offsetx2;
    particle.y += offsety2;
    particle.xp += xp;
    particle.yp += yp;
  }
}

namespace {
  struct __Init_Module {
    __Init_Module()	{ ::Element::rng = gsl_rng_alloc(gsl_rng_mt19937); }
    ~__Init_Module()	{ gsl_rng_free(::Element::rng); }
  } dummy;
}
