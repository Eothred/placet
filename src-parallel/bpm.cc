#include <limits>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "bpm.hh"

Bpm::Bpm(const std::string &name, double length, size_t _n_bunches, double _resolution ) : Element(name, length), n_bunches(_n_bunches), resolution(_resolution)
{
  x_err=y_err=x_sum=y_sum=0.;
  sum=1.;
}

Bpm::Bpm(double length, size_t _n_bunches, double _resolution ) : Element(length), n_bunches(_n_bunches), resolution(_resolution)
{
  x_err=y_err=x_sum=y_sum=0.;
  sum=1.;
}

void Bpm::transport(std::vector<Particle> &beam ) const
{
  x_err = gsl_ran_gaussian(rng, resolution);
  y_err = gsl_ran_gaussian(rng, resolution);
  x_sum = 0.;
  y_sum = 0.;
  sum = 0.;
  if (length > std::numeric_limits<double>::epsilon()) {
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      particle.x += length*particle.xp * 0.5;
      particle.y += length*particle.yp * 0.5;
    }
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      if (fabs(particle.weight)> std::numeric_limits<double>::epsilon()) {
	double wgt = fabs(1.0);
	x_sum += (particle.x-x)*wgt;
	y_sum += (particle.y-y)*wgt;
	sum += wgt;
      }
    }
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      particle.x += length*particle.xp * 0.5;
      particle.y += length*particle.yp * 0.5;
      //particle.z += length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
    }
  } else {
    for (std::vector<Particle>::iterator i=beam.begin(); i!=beam.end(); ++i) {
      Particle &particle = *i;
      if (fabs(particle.weight)> std::numeric_limits<double>::epsilon()) {
	double wgt = fabs(1.0);
	x_sum += (particle.x-x)*wgt;
	y_sum += (particle.y-y)*wgt;
	sum += wgt;
      }
    }
  }
}
