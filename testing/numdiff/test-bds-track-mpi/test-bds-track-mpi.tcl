#################################################################################################################################
#                                                                                                                                
# main.tcl
#
#################################################################################################################################

set e_initial 1500
set e0 $e_initial
set base_dir .
set script_dir ../../script_dir/

set bpmres 0.010
set deltae 0.001

source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl

# synchrotron radiation on
set sr 1

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr 
set sbend_synrad $sr

# six dimensional step functions:
set six_dim 1

set six_dim_sbend $six_dim
# this is not implemented yet
set six_dim_quad 0

SetReferenceEnergy $e0
Girder
MPI_Begin
source $script_dir/lattices/bds.match.linac4b_v_10_10_11
MPI_End
TclCall -script { 
  Octave {
    B = placet_get_beam();
    printf("Beam distr: %.6e %.6e %.6e %.6e %.6e %.6e\n",mean(B))
    printf("Beam std: %.6e %.6e %.6e %.6e %.6e %.6e\n",std(B))
  }
}
BeamlineSet -name test

array set match {
    alpha_x 0.59971622
    alpha_y -1.93937335
    beta_x  18.382571
    beta_y  64.450775
    emitt_x 6.6
    emitt_y 0.2
    charge  4.0e9
    sigma_z 44.0
    phase   0.0
    e_spread -1.0
}

#emittances unit is e-7
set charge $match(charge)
set e0 $e_initial

set n_slice 100
set n 5000
set n_total [expr $n_slice*$n]

make_beam_particles $e0 $match(e_spread) $n_total
make_beam_many beam0 $n_slice $n
BeamRead -file particles.in -beam beam0

FirstOrder 1

puts "\nMPI tracking:"
Octave tic
MPI_TestNoCorrection -beam beam0 -survey None -emitt /dev/null -mpirun "mpiexec -np 8"
Octave toc
BpmReadings -file mpi_reading.dat

puts "\nSingle-thread tracking:"
Octave tic
TestNoCorrection -beam beam0 -survey None -emitt /dev/null
Octave toc
BpmReadings -file single_reading.dat
