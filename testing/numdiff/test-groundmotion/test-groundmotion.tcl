Girder
Drift -length 10
Girder
Drift -length 0
Girder
Drift -length 0
Girder
Drift -length 10
Girder
Drift -length 10
Girder
Drift -length 10
Girder
Drift -length 10
Girder
Drift -length 10
Girder
Drift -length 10
Girder
Drift -length 10
BeamlineSet
Zero

set time_step 5
set ground_motion_x 1
set ground_motion_y 1
set start_element 3
set end_element 7
set end_fixed 0

GroundMotionATL -t $time_step -x $ground_motion_x -y $ground_motion_y -end_fixed $end_fixed -start_element $start_element -end_element $end_element
SaveAllPositions -file "pos.txt"