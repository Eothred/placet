//--- Hello, World! for CppUnit

#include <iostream>
#include <stdlib.h>

#include "placet_unit_test.h"

#include "bessel.h"

class Test : public CPPUNIT_NS::TestCase
{
  CPPUNIT_TEST_SUITE(Test);
  CPPUNIT_TEST(testBesselI0);
  CPPUNIT_TEST(testBesselI1);
  CPPUNIT_TEST(testBesselY0);
  CPPUNIT_TEST(testBesselY1);
  CPPUNIT_TEST_SUITE_END();

public:
  void setUp(void) {}
  void tearDown(void) {} 
protected:
  void testBesselI0(void) { CPPUNIT_ASSERT_DOUBLES_EQUAL( bessel_i0(1.5), 1.64672,  0.00002); }
  void testBesselI1(void) { CPPUNIT_ASSERT_DOUBLES_EQUAL( bessel_i1(2.5), 2.51672,  0.00002); }
  void testBesselY0(void) { CPPUNIT_ASSERT_DOUBLES_EQUAL( bessel_y0(3.5), 0.189022, 0.00002); }
  void testBesselY1(void) { CPPUNIT_ASSERT_DOUBLES_EQUAL( bessel_y1(4.5), 0.300997, 0.00002); }

};

CPPUNIT_TEST_SUITE_REGISTRATION(Test);

// This includes the main function,
// which I think is equal in all
// tests..?
#include "placet_unit_test_main.icc"