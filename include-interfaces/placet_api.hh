#ifndef placet_api_hh
#define placet_api_hh

#include <vector>
#include <string>
#include <map>

#include "attribute.hh"
#include "matrixnd.hh"

extern void placet_test_no_correction(MatrixNd          &out_emitt,
				      MatrixNd          &out_beam,
				      const std::string &in_beamline,
				      const std::string &in_beam,
				      const std::string &in_survey,
				      int                in_nmachines = 1,
				      int                in_start = 0,
				      int                in_end = -1,
				      const std::string &in_format = "%s %ex %sex %x 0.0 %ey %sey %Env 0.0 %n" );

extern void placet_test_no_correction(MatrixNd          &out_emitt,
				      MatrixNd          &out_beam,
				      const std::string &in_beamline,
				      const std::string &in_beam,
				      const std::string &in_survey,
				      const std::string &in_format );

extern void placet_get_bpm_readings(MatrixNd                  &out_readings,
				    MatrixNd                  &out_positions,
				    std::vector<size_t>       &out_indexes,
				    MatrixNd                  &out_transmission,
				    const std::string         &in_beamline,
				    const std::vector<size_t> &in_bpms = std::vector<size_t>(),
				    bool                       in_exact = false );

extern void placet_get_number_list(std::vector<size_t>            &out_indexes,
				   const std::string              &in_beamline,
				   const std::vector<std::string> &in_types );

extern void placet_get_name_number_list(std::vector<size_t>  &out_indexes,
					const std::string    &in_beamline,
					const std::string    &in_pattern );

extern void placet_element_set_attribute(const std::string              &in_beamline,
					 const std::vector<size_t>      &in_indexes,
					 const std::vector<std::string> &in_attributes,
					 const std::vector<Attribute>   &in_value );

extern void placet_element_get_attribute(std::vector<Attribute>         &out_value,
					 const std::string              &in_beamline,
					 const std::vector<size_t>      &in_indexes,
					 const std::vector<std::string> &in_attributes );

extern void placet_element_get_attributes(std::vector<std::map<std::string,Attribute> > &out_attributes,
					  const std::string         &in_beamline,
					  const std::vector<size_t> &in_indexes = std::vector<size_t>());

extern void placet_vary_corrector(const std::string         &in_beamline,
				  const std::vector<size_t> &in_indexes,
				  const MatrixNd            &in_delta );

extern void placet_get_beam(MatrixNd          &out_beam,
			    double            &out_position,
			    const std::string &in_name = std::string());

extern void placet_set_beam(const std::string &in_name,
			    const MatrixNd    &in_beam );

extern void placet_set_beam(const MatrixNd &in_beam );

extern void placet_beamline_refresh(const std::string &in_beamline );

extern void placet_get_response_matrix_fit(MatrixNd                  &out_Rxx,
					   MatrixNd                  &out_Rxy,
					   MatrixNd                  &out_Ryx,
					   MatrixNd                  &out_Ryy,
					   MatrixNd                  &out_Rxxx,
					   MatrixNd                  &out_Rxxy,
					   MatrixNd                  &out_Rxyy,
					   MatrixNd                  &out_Ryxx,
					   MatrixNd                  &out_Ryyx, 
					   MatrixNd                  &out_Ryyy,
					   const std::string         &in_beamline,
					   const std::string         &in_beam,
					   const std::string         &in_survey,
					   const std::vector<size_t> &in_bpms,
					   const std::vector<size_t> &in_correctors );

extern void placet_get_transfer_matrix_fit(MatrixNd               &out_R,
					   std::vector<MatrixNd>  &out_T,
					   const std::string      &in_beamline,
					   const std::string      &in_beam,
					   const std::string      &in_survey,
					   int                     in_start = 0,
					   int                     in_end = -1 );

extern void placet_get_transfer_matrix(MatrixNd               &out_R,
				       const std::string      &in_beamline,
				       int                     in_start = 0,
				       int                     in_end = -1 );

extern void placet_get_transfer_matrix_elements_fit(MatrixNd                       &out_elements,
						    const std::string              &in_beamline,
						    const std::string              &in_beam,
						    const std::string              &in_survey,
						    const std::vector<std::string> &in_elements,
						    int                             in_start = 0,
						    int                             in_end = -1 );

extern void placet_girder_get_length(MatrixNd          &out_lengths,
				     const std::string &in_beamline );

extern void placet_girder_move(const std::string &in_beamline,
			       const MatrixNd    &in_delta );

extern void placet_Tcl_GetVar(std::string       &out_value,
			      const std::string &in_varname1,
			      const std::string &in_varname2 = std::string());

extern void placet_Tcl_SetVar(const std::string &in_varname1,
			      const std::string &in_varname2,
			      const Attribute   &in_value );

extern void placet_Tcl_SetVar(const std::string &in_varname1,
			      const Attribute   &in_value );

extern void placet_Tcl_Eval(const std::string &in_script );

extern void placet_Tcl_EvalFile(const std::string &in_filename );

#endif /* placet_api_hh */
