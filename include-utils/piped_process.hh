#ifndef piped_process_hh
#define piped_process_hh

#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <cstring>

class Piped_process {
protected:

  pid_t PID;
  int _stdin;    // parent -> child
  int _stdout;   // child -> parent
  int _stderr;   // child -> parent
  int _ifiledes; // parent -> child
  int _ofiledes; // child -> parent

  Piped_process() {}

public:
  
  Piped_process(const char *command, char *const argv[] );
  ~Piped_process();
  
  int get_stdin() const  { return _stdin; }
  int get_stdout() const { return _stdout; }
  int get_stderr() const { return _stderr; }
  int ifiledes() const   { return _ifiledes; }
  int ofiledes() const   { return _ofiledes; }

  const pid_t &pid() const { return PID; }
  int wait() { int status; waitpid(pid(),&status,0); return status; }
  int kill(int sig ) { return ::kill(PID, sig); }

  ssize_t send(const void *buf, size_t nbyte ) { return write(_stdin, buf, nbyte); }
  ssize_t send(const char *str ) { return write(_stdin, str, strlen(str)); }

  ssize_t recv(void *buf, size_t nbyte ) { return read(_stdout, buf, nbyte); }

};

#endif /* piped_process_hh */
