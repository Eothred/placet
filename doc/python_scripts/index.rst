.. PLACET Python Scripts documentation master file, created by
   sphinx-quickstart2 on Wed Feb 12 10:10:19 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation of PLACET Python Scripts's
========================================

This is automatically generated documentation of the python scripts
available for PLACET.

Contents:

.. toctree::
   :maxdepth: 2

   guinea
   grid
   tools
   BeamPlot
   OrbitPlot
   genetic_algorithm
   yaml_lattice


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

