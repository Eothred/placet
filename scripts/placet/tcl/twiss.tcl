#Load gnuplot procedures 
source gnuplot_procs.tcl

namespace eval twiss_plot {
    array set range [list .c.x "*:*"  .c "*:*"]
    variable first 1
    
   proc eval_plot_beta_x {} {
        variable namebetax 
        eval [exec gnuplot << "set term tkcanvas
	set xlabel \"s \[m\]\"
	set ylabel \"beta_x \[m\]\"
                 set xrange\[$twiss_plot::range(.c.x)\]
                  set yrange\[$twiss_plot::range(.c)\]
	plot \"$namebetax\" u 2:4 notitle w lines
	exit
	"]
}

  proc eval_plot_beta_y {} {
        variable namebetay 
        eval [exec gnuplot << "set term tkcanvas
	set xlabel \"s \[m\]\"
	set ylabel \"beta_y \[m\]\"
                 set xrange\[$twiss_plot::range(.c.x)\]
                  set yrange\[$twiss_plot::range(.c)\]
	plot \"$namebetay\" u 2:8 notitle w lines
	exit
	"]
}

    proc beta_x {name fname} {
	variable first
                 variable namebetax $name 
	if {$fname!="1"} {
	    set file [open $fname w]
	    puts $file [exec gnuplot << "set term post eps enh color 24
			                 set xlabel \"s \[m\]\"
	                                 set ylabel \"\{/Symbol b\}_x \[m\]\"
	                                 plot \"$name\" u 2:4 notitle w lines
	                                 exit
	                                "]
	    close $file
	} {
	    if {$first} {
		canvas .c
		pack .c
		set first 0

	    }
                     my_gnuplot .c twiss_plot::eval_plot_beta_x twiss_plot
	}
    }

  

    proc beta_y {name fname} {
	variable first
                  variable namebetay $name 
	if {$fname!="1"} {
	    set file [open $fname w]
	    puts $file [exec gnuplot << "set term post eps enh color 24
                	                 set xlabel \"s \[m\]\"
	                                 set ylabel \"\{/Symbol b\}_y \[m\]\"
	                                 plot \"$name\" u 2:8 notitle w lines
	                                 exit
	                                "]
	    close $file
	} {
	    if {$first} {
		canvas .c
		pack .c
		set first 0
                	    }
	    my_gnuplot .c twiss_plot::eval_plot_beta_y twiss_plot
	}
    }
}

#twiss_plot::beta_x "testt"    1
