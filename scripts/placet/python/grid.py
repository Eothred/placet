'''
 A python implementation of grid
 (this is slower..)
'''


def loadtxt(filelist,ncols=1,dtype=float,sep=' '):
    '''
      Similar to numpy.loadtxt, but this is much faster
      filelist is a list of filenames (str) instead of just one file
    '''
    from numpy import fromstring

    fstring=''
    for f in filelist:
        fstring+=file(f).read()

    if ncols==1:
        return fromstring(fstring,dtype=dtype,sep=sep)
    ret=fromstring(fstring,dtype=dtype,sep=sep)
    return ret.reshape((len(ret)/ncols,ncols))

def grid(efile,pfile,toscreen=True):
    '''
    Calculate the grid, and optionally print it to screen

    :param efile: name of electron distribution file
    :param pfile: name of positron distribution file
    :param toscreen: print result to stdout
    '''
    from numpy import array

    arr  = loadtxt([efile,pfile],ncols=6)

    l =len(arr)
    x,y=arr[:,1],arr[:,2]
    x.sort()
    y.sort()

    ret=1e3*array([
       [x[l/100],x[-l/100]],
       [y[l/100],y[-l/100]]
       ])
    if toscreen:
        for r in ret:
            print r[0],r[1]

    return ret

if __name__=="__main__": 
    import argparse
    parser = argparse.ArgumentParser(description='Calculate the grid containing the distributions.')
    parser.add_argument('-e',  type=str, help='Name of electron distribution file [electron.ini]', default='electron.ini')
    parser.add_argument('-p',  type=str, help='Name of positron distribution file [positron.ini]', default='positron.ini')
    args = parser.parse_args()
    grid(args.e,args.p)

