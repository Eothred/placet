#  This file is part of PLACET
#
# Copyright (C) 2012 Yngve Inntjore Levinsen <yngve.inntjore.levinsen@cern.ch>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this library; see the file COPYING.LIB.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

def grid(el_file='',pos_file='',stdout=None):
    '''
    Run grid in a subprocess
    '''
    import subprocess

    cmd=['grid']
    if el_file and pos_file:
        cmd.extend([el_file,pos_file])
    result=subprocess.check_output(cmd).split()
    res_final=[[float(result[i*2+j]) for j in range(2)] for i in xrange(3)]
    return res_final

def get_offset(res):
    '''
    Give the result from grid()
    to this function, and in return
    you get the offset horizontally and vertically
    (tuple)
    '''
    offset_x =-0.5*(res[0][0]+res[0][1])
    offset_y =-0.5*(res[1][0]+res[1][1])
    return offset_x,offset_y

def get_xycut(res):
    '''
    Estimate the horizontal and vertical cuts
    from grid()
    '''
    xcut=2*(res[0][1]-res[0][0])
    ycut=2*(res[1][1]-res[1][0])
    return xcut,ycut


def shuffled_array(length,arrname):
    '''
    Generate a randomly shuffled
    array.

    Returns the python list of
    the shuffled array

    Example::

      Python {
      import tools
      tools.shuffled_array(16,"myarr")
      }
      puts $myarr

    '''
    import random,sys
    import placet_python as placet

    # generate a shuffled array:
    arr=range(length)
    random.shuffle(arr)

    if sys.flags.debug:
        print "array generated:", arr

    # make a string for tcl..
    arr_str=' '.join([str(i) for i in arr])
    placet.Tcl_SetVar(arrname,arr_str)

    return arr


