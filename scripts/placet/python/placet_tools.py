

def load_dict(json_file='result.json'):
    '''
    Loads the dictionary (of results) from file...
    '''
    import json
    return json.load(file(json_file,'r'))

def save_dict(result_dict,json_file='result.json'):
    '''
    Saves the dictionary (of results) to file...
    Note, result_dict can also be a list, as long as it can be sent to
    json.dumps()
    '''
    import json
    file(json_file,'w').write(json.dumps(result_dict,sort_keys=True,indent=2))

def get_array(varname,vartype=float):
    '''
    Retrieve an array from the TCL environment
    '''
    import placet_python as placet
    _ret=placet.Tcl_GetVar(varname).strip('[]')
    if ',' in _ret:
        return [vartype(i) for i in _ret.split(',')]
    else:
        return [vartype(i) for i in _ret.split()]

def get_knobs_list(beamline="test",NAMES = [ "SF6", "SF5", "SD4", "SF1", "SD0" ]):
    '''
    Returns list of knobs indices (by default, sextupoles in CLIC BDS)
    '''
    import numpy,placet_python as placet

    N = len(NAMES)
    KBI = []
    for i in xrange(N):
        ret=placet.get_name_number_list(beamline, NAMES[i])
        if len(ret) != 1:
            print "Tried to get list id for %s, but got %i ID numbers\n" % (NAMES[i],length(ret));
            raise ValueError("Wrong number of elements with name")
        KBI.extend(ret)
    return KBI

def subtract_avg_yp(fin,fout=None):
    '''
    Subtracts the average yp from the distribution in the file fin,
    and writes the resulting distribution to fout.

    If fout is not given, writes to fin directly
    '''
    import numpy

    data=numpy.loadtxt(fin)
    yp_avg=numpy.average(data[:,5])
    print "Average yp subtracted:",yp_avg
    data[:,5]-=yp_avg
    if fout==None:
        fout=fin
    numpy.savetxt(fout,data)

