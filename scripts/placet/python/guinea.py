#  This file is part of PLACET
#
# Copyright (C) 2012 Yngve Inntjore Levinsen <yngve.inntjore.levinsen@cern.ch>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this library; see the file COPYING.LIB.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

import subprocess

def guinea(
        calc_time=False,
        result_file='result.out',
        elfile='',
        posfile='',
        accfile='',
        accelerator='clic',
        parameter_set='default',
        print_cmd=True,
        stdout=None):
    '''
    Run GUINEA-PIG in a subprocess...
    '''
    cmd=['guinea']

    if elfile:
        cmd.append('--el_file='+elfile)
    if posfile:
        cmd.append('--pos_file='+elfile)
    if accfile:
        cmd.append('--acc_file='+accfile)

    cmd.extend([accelerator,parameter_set,result_file])
    if calc_time:
        import time
        start=time.time()
    if print_cmd:
      print ' '.join(cmd)
    subprocess.call(cmd,stdout=stdout)
    if calc_time:
        return time.time()-start


def read_guinea_result(result_file):
    '''
    Read the result from GUINEA-PIG.
    Returns a dictionary containing the
    keys 'total' and 'peak', corresponding
    to the luminosity found in the output file.
    '''
    ret={}
    for l in file(result_file):
        lsp=l.split()
        if len(lsp)>0:
            if lsp[0]=='lumi_ee_high':
                ret['peak']=float(lsp[2])
            elif lsp[0]=='lumi_ee':
                ret['total']=float(lsp[2])
        # lines below are for when old c version is used..
        lsp=l.split('=')
        if len(lsp)>0:
            if lsp[0]=='lumi_ee_high':
                ret['peak']=float(lsp[1].split(';')[0])
            elif lsp[0]=='lumi_ee':
                ret['total']=float(lsp[1].split(';')[0])
    return ret

def get_lumi( nslice, nperslice, distr_file='electron.ini', accelerator='clic', force_run=False):
    '''
    Runs GRID, GUINEA-PIG and then returns (l_total, l_peak)

    :param nslice: number of slices
    :param nperslice: number of particles per slice
    :param distr_file: particle distribution file
    :param accelerator: name of the accelerator (must be defined in the macro!)
    :param force_run: run even if distr_file is not found.
    '''
    import tools
    import numpy
    import os

    if not os.path.isfile(distr_file):
        print "Not found:",distr_file
        if not force_run:
            return 0.0,0.0
        else:
            print "Forcing grid/guineapig"

    # below follows some flags for checking guinea-pig..
    # use old guinea-pig..
    old=True
    # charge_sign in acc.dat
    charge_sign=-1.0
    # load_beam in acc.dat
    load_beam=3
    # GUINEA-PIG cuts (suggestion by Barbara x: 200, y: 25):
    cut_x=400
    cut_y=35
    # number of timesteps per slice
    n_t=1
    # include coherent:
    do_coherent=1
    # include radiation:
    do_photons=0
    # Total number of particles..
    ntotal=nperslice*nslice

    ## check that input parameters are sane:

    # no coherent without radiation..
    if not do_photons:
        do_coherent=0

    # Running grid...
    g=tools.grid(distr_file, distr_file)
    offset_x, offset_y = tools.get_offset(g)
    cut_x, cut_y = tools.get_xycut(g)

    # keep grid size approx. constant for the cut but not too small:
    # (this is optimized for CLIC, maybe not general enough)
    n_x=max(32,  2**(int(numpy.log2(64.*cut_x/200))))
    n_y=max(128, 2**(int(numpy.log2(256.*cut_y/25))))
    # and not too long simulation..
    n_x=min(n_x,128)
    n_y=min(n_y,512)

    if os.path.isfile('acc.macro.dat'):
        fin=file('acc.macro.dat')
    else:
        fin=file(os.path.join(os.path.dirname(__file__),'acc.macro.dat'))
    acc='acc_'+distr_file
    file(acc,'w').write(fin.read() % locals())

    out_file='gpresult_'+distr_file

    guinea(old,
        elfile=distr_file,
        posfile=distr_file,
        result_file=out_file,
        accfile=acc,
        accelerator=accelerator,
        print_cmd=False,
        stdout=file(os.devnull,'w'))
    res=read_guinea_result(out_file)

    # remove temporary files..
    os.remove(out_file)
    os.remove(acc)

    return res['peak'], res['total']
