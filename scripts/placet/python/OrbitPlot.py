'''
Plotting the orbit inside a PLACET script..
'''

def OrbitPlot(beamline,
              ymax=None,
              xmax=None,
              smin=None,
              smax=None,
              filename='orbit.png'):
    '''
    Plots the orbit in two subplots, y above.

    :param beamline: name of beamline to get BPM signal from
    :param ymax: Define a max for vertical orbit such that plot goes from [-ymax,ymax]
    :param xmax: Define a max for horizontal orbit such that plot goes from [-xmax,xmax]
    :param smin: Plot the orbit starting from smin
    :param smax: Plot the orbit only until smax
    :param filename: name of file
    '''
    import placet_python as placet
    import pylab

    # get bpm readings:
    bpm_meas,bpm_s,bpm_i=placet.get_bpm_readings(beamline)
    bpm_x=bpm_meas[:,0]
    bpm_y=bpm_meas[:,1]
    bpm_s=bpm_s[0]

    # create a simple plot..
    f = pylab.figure()

    # Reduce the whitespace between the two plots to a minimum:
    pylab.subplots_adjust(hspace=0.1)

    # Plot horizontal orbit (this is the upper plot)
    ax0 = pylab.subplot(211)
    ax0.plot(bpm_s,bpm_x)
    ax0.set_ylabel('x [um]')
    if not xmax==None:
        pylab.ylim((-xmax,xmax))

    # Plot vertical orbit (this is the lower plot)
    ax1 = pylab.subplot(212,sharex=ax0)
    ax1.plot(bpm_s,bpm_y)
    ax1.set_ylabel('y [um]')
    ax1.set_xlabel('Longitudinal position [m]')
    if not ymax==None:
        pylab.ylim((-ymax,ymax))

    # in case the user wants to set longitudinal ranges:
    pylab.xlim((smin,smax))

    # remove xtics in the upper plot..
    pylab.setp(ax0.get_xticklabels(), visible=False)

    # save the figure..
    pylab.savefig(filename)
