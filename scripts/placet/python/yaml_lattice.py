# PyYAML is available at: http://pyyaml.org/
import yaml


'''
 These are convenience tools to store lattices in an alternative
 YAML format. The convert function should work on most TCL files that 
 have only lattice elements and e.g. set commands to update energy etc.
 
 The benefit of YAML over TCL is that the lattices should be easier to maintain,
 as you have e.g. object inheritance easily available. The files are also readable
 by several languages, including Python, Octave, Matlab, C/C++....

 Last edited: Feb. 2014

 @author: Yngve Inntjore Levinsen
'''


def convert_lattice_to_yaml(filename,yaml_filename):
    '''
     Reads a TCL scripted lattice file into a yaml data file
     which can easily be processed in many different languages...
    '''
    _base=[]
    for l in file(filename,'r'):
        if len(l.strip())==0 or l.strip()[0]=='#': # comment line..
            continue
        if l.strip()[0]!=l.strip()[0].upper(): # tcl command..
            element={'TCL':l.strip()}
        else:
            lsp=l.split()
            if len(lsp)==1: # usually Girder..
                element={lsp[0]:dict(distance=0.0)}
            else:
                i=1
                _dict={}
                while(i<len(lsp)):
                    if lsp[i][0]=='-':
                        j=i+1
                        if lsp[j][0]=="[":
                            while lsp[j][-1] !="]":
                                j+=1
                        _val=' '.join(lsp[i+1:j+1])
                        try:
                            _dict[lsp[i][1:]]=int(_val)
                        except ValueError:
                            try:
                                _dict[lsp[i][1:]]=float(_val)
                            except ValueError:
                                _dict[lsp[i][1:]]=_val.strip('"').strip("'")
                        i=j+1
                element={lsp[0]:_dict}

        for b in _base:
            if element==b:
                element=b
                print element
                break
        _base.append(element)

    
    yaml.safe_dump({"Lattice":_base},file(yaml_filename,'w'), default_flow_style=False)

def _flatten_list(test_list):
    '''
    Flattens a list of lists (of lists..) recursively
    '''
    if isinstance(test_list, list):
        if len(test_list) == 0:
            return []
        first, rest = test_list[0], test_list[1:]
        return _flatten_list(first) + _flatten_list(rest)
    else:
        return [test_list]

def _expand_lattice(lattice):
    '''
     The lattice object should be a list of dictionaries..
     But in order to allow for repeating segments, we flatten lists of lists recursively..
    '''

    return _flatten_list(lattice)


def get_yaml_lattice_commands(filename):
    '''
     Reads in a yaml lattice file and converts to a list of TCL commands.

     Can then easily be written to a TCL file

     Example:
       >>> lst=get_yaml_lattice_commands('clic.yml')
       >>> file('clic.tcl','w').write('\\n'.join(lst))

    '''

    lattice=yaml.safe_load(file(filename,'r'))['Lattice']

    lattice=_expand_lattice(lattice)
    cmds=[]

    for _dict in lattice:
        _type=_dict.keys()[0]
        if _type=='TCL': # generic TCL command
            cmds.append(_dict['TCL'])
        else:
            cmd=_type+' '
            for key,attr in _dict[_type].items():
                cmd+=' -'+key+' '+str(attr)
            cmds.append(cmd)

    return cmds

def exec_yaml_lattice(filename):
    '''
     Reads in a yaml formatted lattice file and executes the commands
    '''

    import placet_python as placet

    cmds=get_yaml_lattice_commands(filename)
    for cmd in cmds:
        placet.Tcl_Eval(cmd)

