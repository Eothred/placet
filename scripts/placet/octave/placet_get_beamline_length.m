## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_beamline_length (@var{beamline}, [ START, END ])
## Return the length of 'beamline' (optionally, from the entrance of START to the exit of END).
## @end deftypefn

function s=placet_get_beamline_length(beamline, START, END)
	error("Sorry, function 'placet_get_beamline_length' has been renamed 'placet_beamline_get_length' for consistency.");
endfunction
