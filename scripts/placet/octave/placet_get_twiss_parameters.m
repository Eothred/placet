## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_twiss_parameters (@var{B})
## Return twiss parameters and emittance of beam `B'; where `B' is a 6-columns matrix, in guinea-pig format, or a 17-columns matrix in sliced-beam format.
## @end deftypefn

function [beta_x, beta_y, alpha_x, alpha_y, emitt_x, emitt_y]=placet_get_twiss_parameters(B)
  if nargin==1 && ismatrix(B)
		M=placet_get_twiss_matrix(B);
		beta_x=M(1,1);
		beta_y=M(3,3);
		alpha_x=-M(1,2);
		alpha_y=-M(3,4);
                E=placet_get_emittance(B);
                emitt_x = E(1);
                emitt_y = E(2);
  else
    help placet_get_twiss_parameters
  endif
endfunction
