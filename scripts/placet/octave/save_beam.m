## -*- texinfo -*-
## @deftypefn {Function File} {} save_beam (@var{filename}, @var{beam})
## Save 'beam' in file 'filename'.
## @end deftypefn

function save_beam(filename, beam)
  if nargin==2 && ischar(filename) && ismatrix(beam) && columns(beam)==6
    file=fopen(filename, 'wt');
    for i=1:rows(beam)
      fprintf(file, "%.15g %.15g %.15g %.15g %.15g %.15g\n", beam(i,1), beam(i,2), beam(i,3), beam(i,4), beam(i,5), beam(i,6));
    endfor
    fclose(file);
  else
    help save_beam
  endif
endfunction
