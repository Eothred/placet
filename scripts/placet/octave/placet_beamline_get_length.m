## -*- texinfo -*-
## @deftypefn {Function File} {} placet_beamline_get_length (@var{beamline}, [ START, END ])
## Return the length of 'beamline' (optionally, from the entrance of START to the exit of END).
## @end deftypefn

function s=placet_beamline_get_length(beamline, START, END)
  if nargin==1 && ischar(beamline)
    I=placet_get_name_number_list(beamline, "*");
    s=placet_element_get_attribute(beamline, I(end), "s");
  elseif nargin==3 && ischar(beamline) && isscalar(START) && isscalar(END)
    s0=placet_element_get_attribute(beamline, START, "s");
    l0=placet_element_get_attribute(beamline, START, "length");
    s1=placet_element_get_attribute(beamline, END, "s");
    s=s1-s0+l0;
  else 
    help placet_beamline_get_length
  endif
endfunction
